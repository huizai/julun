<?php

return [
    'apikey' => env('SMS_APIKEY'),
    'apisecret' => env('SMS_APISECRET'),
];