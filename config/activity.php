<?php

return [
    //满减活动字段配置
    'full_reduction' => [
        'name'              => '满减',
        'title'             => '标题',
        'full_amount'       => '满金额',
        'discount_amount'   => '减金额',
        'start_time'        => '开始时间',
        'end_time'          => '结束时间',
    ],
    'full_discount' => [
        'name'              => '满减',
        'title'             => '标题',
        'full_amount'       => '满金额',
        'discount_amount'   => '折扣',
        'start_time'        => '开始时间',
        'end_time'          => '结束时间',
    ],
];
