<?php
return [
    'name'   => '梦想商区',
    'ios' => [
        'bundle'         => env('IOS_BUNDLE'),
        'last_version'   => env('IOS_LAST_VERSION')
    ],
    'android' => [
        'last_version'   => env('ANDROID_LAST_VERSION')
    ],
    'level' => [
        'level_1' => [
            'name'  => '普通用户',
        ],
        'level_2' => [
            'name'  => '代理商',
        ],
        'level_3' => [
            'name'  => '分公司',
        ]
    ]
];
