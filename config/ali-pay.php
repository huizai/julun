<?php

return [

    'config' => [
        'app_id' => env('ALIPAY_APPID'),
        'notify_url' => env('APP_URL').'/pluto/order/pay/ali/notify',
        'return_url' => 'http://192.168.0.104:10086/#/pages/pay/paySuccess',
        'ali_public_key' => env('ALIPAY_PUBLIC_KEY'),
        // 加密方式： **RSA2**
        'private_key' => env('ALIPAY_PRIVATE_KEY'),
        'log' => [ // optional
            'file' => '../storage/logs/alipay.log',
            'level' => 'debug', // 建议生产环境等级调整为 info，开发环境为 debug
            'type' => 'single', // optional, 可选 daily.
            'max_file' => 30, // optional, 当 type 为 daily 时有效，默认 30 天
        ],
        'http' => [ // optional
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
            // 更多配置项请参考 [Guzzle](https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html)
        ],
//        'mode' => 'dev', // optional,设置此参数，将进入沙箱模式
    ],

    'recharge' => [
        'app_id' => env('ALIPAY_APPID'),
        'notify_url' => env('APP_URL').'/pluto/user/pay/ali/recharge/notify',
        'return_url' => 'http://192.168.0.104:10086/#/pages/pay/paySuccess',
        'ali_public_key' => env('ALIPAY_PUBLIC_KEY'),
        'private_key' => env('ALIPAY_PRIVATE_KEY'),
        'log' => [
            'file' => '../storage/logs/alipay.log',
            'level' => 'debug',
            'type' => 'single',
            'max_file' => 30,
        ],
        'http' => [
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
        ],
    ],

    'lansiu' => [
        'app_id' => '2019081266215154',
        'notify_url' => env('APP_URL').'/pluto/user/pay/ali/recharge/notify',
        'ali_public_key' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkRtsdW5eSH+Ip2l8dIWyQ1f8H0iLMXVxszlF6fEh9x7txlhiN/m9dDuMPwgWAn0GlL1yEmEhkuhdDLWislxEXm8H9PX6FaayUcq5s1seD0fzqSS6gcz3p1Qs71+s9ZD5vs/9Hkh+jx9vQ5Dl1r6FIf+t324oV0i2T3cAHtJ2pM3/E4QSZyISIi0UX7N7jo5bf4YTYH1sOv7DijVjCsHzwsX/t6/5ahAhbqrHAEBT9FPhqX0Yvn1t1aL2qbm3yjtA5VerDNqk/kNJZ667aF0PjIRY6oR8CYU2F6THBy1uRIqlzgkhbguDiouAkt8myyHo5c6P2vYwJp1P5OuWgm+I/QIDAQAB',
        'private_key' => 'MIIEogIBAAKCAQEAkc0l/AplsJU/o4WQ0/rG3LQlKimHE28FJ6w7t0VV5tCHDkr3Y+m5x7j16syIzswcM4c1elEZh+RR0LEfYynusKnr2nr0p37VvNZyI3abPUCXtWLzlXmQ9KIc+krgLviHOfOTwnG13FOoAEyBsTwQc1xXHMklmJVgAgfRRJ9qcKBwTxb9kU6PWMtaekqJcO2PvzlqN/S2deFjrSl6e1CsvBwRxBuFBt0ZbaMN0Sgn1DuVG/3ZWEy9WUsefmF6wMC4xQUt1ZdVLiR9wtlkvLVaG7fKbLRojtZURFzmgnyyA/pjswH/QAGhyQt2JBYl1yQ06vuzKQMBJex8JR9Pv6RKtQIDAQABAoIBACjPa1po7p3GqNF0iV1MTG1qXxy2EwD1ilPYeb6KwcihhawSf7yPp1ioBO174q8/ooUpy2Y5XRQUbHIloct36T+50DKPR/u6HMmGnNwgTsOOWOiEkfm81T4kQkgsvSNonTEzZKCAg47wO44Z3BNn5JLlvdxo0QUsinsHBSRYGsr7MUzXPKZejr5IjRYitAuGhUC/qTb6WOCmBLosMS4KR7TpcLah0S+RrzDxslw4khO6AE/olSp6OM1o/b0NHWWZ3iY0MrozXEeoMJVnrUzClCR8mzoikWkptFhXKWdzSUHG7GgPGq9yb+CFaP/J7hLbLHw5+yHvCFRDO4aS4yrC+gECgYEAwYBuPOCMKWL7U3vKmjY+SjJFY0d/ugd3zZ4lKtyZzCKYqFywvfOln9sspFEwje4oyjZlxFC5NsVS4yDcZolmvAyqMLdgh2Co78DCaHyRw3SqSlykc0YRemhednMbULJn8D2PPrYETAUik2bbj7FW/f25SI/5mg565SuW9fhgSRECgYEAwOSlRVtrIgNU1jlW0asTEhfFG/KHTDcNZ5yuL8Uu7R1175ll+r84YqX85a1djlbjgr+CzIekANVeOw7FWhFKhyhp0wzvmJ4x3cFxttuMog2kq58b91p6FY93/mz7/1YJCtzIHx8vsipTQudjs44PUSvpx6eFLzk44KJ462UoB2UCgYAO/vwMHb7fCju+CY73CpMjPugfvI/Kg/+l4MaynqwQpPG9wokz/2jrjZnVFjG0HivtF/AlGbhw5TP6RDawf5+FnVmnMt75PuV4xzCJ1nWZdzYnUyyvflOmb+Y6Oct5IopplOvACW0l0xQN5l/HsA+02W/9jqLibJLbpdgqMdiMcQKBgHr2xuyp1GfAW+qcbt4GAog8Iukn+Fv7lE4CsX++YaXCbBYrYbkxAQ7R5Z+8nrFIboxA7W7Y4RGu8v0O83vxHeN/PcbOrkZKQlutH4KD/uinbdwB99/9C2qHJOVKMHKbM/R6vhAo30zicmVqAELMQrNqHQOrpNlvzvRUXmUg/R2JAoGAb3OF1aWSqgIBu1xDap5LbLYLaHCTUFpdZH80ukykngQ0QZdOhg4zx9DAwrb5NXzIunY263m/39FLotMxayqakQ/QX2A03OViIUEBHvQ5i+8dA7aaNGqpQvfLTOu+EHAPJZNGszS2Tm1PAjrYr0RTOdnNjwoz1sm9fF2x0cisvEA=',
        'log' => [
            'file' => '../storage/logs/alipay.log',
            'level' => 'debug',
            'type' => 'single',
            'max_file' => 30,
        ],
        'http' => [
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
        ],
    ],

    'parking' => [
        'app_id' => env('ALIPAY_APPID'),
        'notify_url' => env('APP_URL').'/pluto/parking/pay/ali/notify',
        'ali_public_key' => env('ALIPAY_PUBLIC_KEY'),
        'private_key' => env('ALIPAY_PRIVATE_KEY'),
        'log' => [
            'file' => '../storage/logs/alipay.log',
            'level' => 'debug',
            'type' => 'single',
            'max_file' => 30,
        ],
        'http' => [
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
        ],
    ],

    'book'  => [
        'app_id' => env('ALIPAY_APPID'),
        'notify_url' => env('APP_URL').'/pluto/store/book/pay/alipay/notify',
        'ali_public_key' => env('ALIPAY_PUBLIC_KEY'),
        'private_key' => env('ALIPAY_PRIVATE_KEY'),
        'log' => [
            'file' => '../storage/logs/alipay.log',
            'level' => 'debug',
            'type' => 'single',
            'max_file' => 30,
        ],
        'http' => [
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
        ],
    ],

    'group'  => [
        'app_id' => env('ALIPAY_APPID'),
        'notify_url' => env('APP_URL').'/pluto/order/group/alipay/notify',
        'ali_public_key' => env('ALIPAY_PUBLIC_KEY'),
        'private_key' => env('ALIPAY_PRIVATE_KEY'),
        'log' => [
            'file' => '../storage/logs/alipay.log',
            'level' => 'debug',
            'type' => 'single',
            'max_file' => 30,
        ],
        'http' => [
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
        ],
    ]

];
