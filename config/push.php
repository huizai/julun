<?php

return [
    'app_key' => env('JPush_AppKey', ''),
    'secret' => env('JPush_SECRET', ''),
];
