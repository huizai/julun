<?php

return [
    'mini' => [
        'url'=>'https://pay.swiftpass.cn/pay/gateway',
        'mchId'=>'102515899840',
        'key'=>'',  /* MD5密钥 */
        'version'=>'2.0',
        'sign_type'=>'RSA_1_256',
        'public_rsa_key'=>'/data/code/xhlmxcPem/xhlmxc_mini_public.pem',   /* 平台RSA公钥*/
        'private_rsa_key'=> '/data/code/xhlmxcPem/xhlmxc_mini_private.pem'   /* 商户自行生成的RSA私钥 */
    ],

    'app' => [
        'app_id'             => 'wxcaa34c40613f4178',
        'mch_id'             => '1552391181',
        'key'                => 'eca2482835c4fb5be91be034a03f5699',   // API 密钥
        'cert_path'          => '/data/code/xhlmxcPem/app_xhlmxc_cert.pem',
        'key_path'           => '/data/code/xhlmxcPem/app_xhlmxc_key.pem',
    ],

    'pub' => [
        'app_id'             => 'wxda61a3b58bb9983c',
        'mch_id'             => '1552391181',
        'key'                => 'eca2482835c4fb5be91be034a03f5699',   // API 密钥
        'cert_path'          => '/data/code/xhlmxcPem/app_xhlmxc_cert.pem',
        'key_path'           => '/data/code/xhlmxcPem/app_xhlmxc_key.pem',
    ]

];
