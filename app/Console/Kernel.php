<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CreateModelFromDBTable::class,
        Commands\GetCityCode::class,
        Commands\OrderCancel::class,
        Commands\ManageRemind::class,
        Commands\GoodsVolume::class,
        Commands\ConfirmGoods::class,
        Commands\UserArouse::class,
        Commands\DeleteUserCoupon::class,
        Commands\StoreLease::class,
        Commands\ClearUserLottery::class,
        Commands\Test::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
