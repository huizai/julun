<?php

namespace App\Console\Commands;

use App\Models\District;
use Illuminate\Console\Command;

class GetCityCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:city:code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '获取城市编码';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $amapCityCode = new District();
        $data = file_get_contents('https://restapi.amap.com/v3/config/district?keywords=&subdistrict=4&key=c8149a3da34eaa5a1de69a34e1a42412');
        $data = json_decode($data, true);
        $area_arr = $data['districts'];
        foreach ($area_arr as $country) {
            $countryId = $amapCityCode->saveData([
                'name'      => $country['name'],
                'adcode'    => $country['adcode'],
                'center'    => $country['center'],
                'city_code' => empty($country['citycode'])?null:$country['citycode'],
                'level'     => $country['level']
            ]);

            echo $country['name']."\n";
            foreach ($country['districts'] as $province) {
                $provinceId = $amapCityCode->saveData([
                    'name'      => $province['name'],
                    'adcode'    => $province['adcode'],
                    'center'    => $province['center'],
                    'city_code' => empty($province['citycode'])?null:$province['citycode'],
                    'level'     => $province['level'],
                    'parent_id' => $countryId
                ]);
                echo $province['name']."\n";
                foreach ($province['districts'] as $city) {
                    $cityId = $amapCityCode->saveData([
                        'name'      => $city['name'],
                        'adcode'    => $city['adcode'],
                        'center'    => $city['center'],
                        'city_code' => empty($city['citycode'])?null:$city['citycode'],
                        'level'     => $city['level'],
                        'parent_id' => $provinceId
                    ]);
                    echo $city['name']."\n";
                    foreach ($city['districts'] as $district) {
                        $districtId = $amapCityCode->saveData([
                            'name'      => $district['name'],
                            'adcode'    => $district['adcode'],
                            'center'    => $district['center'],
                            'city_code' => empty($district['citycode'])?null:$district['citycode'],
                            'level'     => $district['level'],
                            'parent_id' => $cityId
                        ]);
                        echo $district['name']."\n";
                        foreach ($district['districts'] as $street) {
                            $amapCityCode->saveData([
                                'name'      => $street['name'],
                                'adcode'    => $street['adcode'],
                                'center'    => $street['center'],
                                'city_code' => empty($street['citycode'])?null:$street['citycode'],
                                'level'     => $street['level'],
                                'parent_id' => $districtId
                            ]);
                        }
                    }
                    echo $district['name']."\n";
                }
            }
        }
    }
}