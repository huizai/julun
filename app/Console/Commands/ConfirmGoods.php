<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Orders;
use App\Models\Stores;
use App\Models\OrderCoupons;
use App\Models\StoreIncomeLog;
use App\Models\Goods;
use App\Models\OrderGoods;
use App\Models\StoreVolume;

class ConfirmGoods extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'confirm:goods';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '确认收货';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orderModel = new Orders();
        $orderCouponModel       = new OrderCoupons();
        $storeModel             = new Stores();
        $storeIncomeLogModel    = new StoreIncomeLog();
        $orderGoodsModel        = new OrderGoods();
        $goodsModel             = new Goods();
        $storeVolumeModel       = new StoreVolume();

        $orderData = DB::table($orderModel->getTable().' as o')
            ->select(
                'o.id',
                'o.stores_id',
                'o.pay_amount',
                'oc.coupon_type',
                'oc.amount'
            )
            ->leftJoin($orderCouponModel->getTable().' as oc', 'oc.orders_id', '=', 'o.id')
            ->whereRaw("TIMESTAMPDIFF(DAY,FROM_UNIXTIME(UNIX_TIMESTAMP(create_time) , '%Y-%m-%d'),'2019-10-01') >= 15")
            ->where('o.status', 2)
            ->get()->toArray();

        if (empty($orderData)){
            exit;
        }

        try{
            DB::beginTransaction();

            $incomeLogData  = [];
            $orderId        = [];
            $storeId        = [];
            foreach ($orderData as $key => $value) {
                $orderId[] = $value->id;
                $storeId[] = $value->stores_id;
                if ($value->coupon_type == 0 && !empty($value->coupon_type)){
                    $value->pay_amount += $value->amount;
                }

                $incomeLogData[$key]['stores_id'] = $value->stores_id;
                $incomeLogData[$key]['orders_id'] = $value->id;
                $incomeLogData[$key]['money'] = $value->pay_amount;

                if(!DB::table($storeModel->getTable())
                    ->where('id', $value->stores_id)
                    ->increment('balance', $value->pay_amount)
                ){
                    DB::rollBack();
                    \Log::debug('添加商户余额失败');
                    exit;
                }
            }

            if(!DB::table($storeIncomeLogModel->getTable())->insert($incomeLogData)){
                DB::rollBack();
                \Log::debug('添加商户余额日志失败');
                exit;
            }

            if(!DB::table($orderModel->getTable())->whereIn('id', $orderId)->update(['status' => 3])){
                DB::rollBack();
                \Log::debug('修改订单状态失败');
                exit;
            }

            $orderGoods = DB::table($orderGoodsModel->getTable().' as og')
                ->select('og.goods_id', 'og.quantity', 'o.stores_id')
                ->leftJoin($orderModel->getTable().' as o', 'o.id', '=', 'og.orders_id')
                ->where('orders_id', $orderId)->get();


            foreach ($orderGoods as $value) {
                if(!DB::table($goodsModel->getTable())
                    ->where('id', $value->goods_id)
                    ->update([
                        'sales_volume'          => DB::raw("sales_volume + {$value->quantity}"),
                        'mouth_sales_volume'    => DB::raw("mouth_sales_volume + {$value->quantity}"),
                        'people_volume'         => DB::raw("people_volume + 1")
                    ]))
                {
                    DB::rollBack();
                    \Log::debug('添加商品销量失败');
                    exit;
                }

                if(!DB::table($storeVolumeModel->getTable())
                    ->where('stores_id', $value->stores_id)
                    ->where('year', date('Y', time()))
                    ->where('month', date('m', time()))
                    ->first()
                ){
                    if(!DB::table($storeVolumeModel->getTable())
                        ->insert([
                            'stores_id' => $value->stores_id,
                            'year'      => date('Y', time()),
                            'month'     => date('m', time()),
                            'volume'    => $value->quantity
                        ]))
                    {
                        DB::rollBack();
                        \Log::debug('添加商户月销量日志失败');
                        exit;
                    }
                }else{
                    if(!DB::table($storeVolumeModel->getTable())
                        ->where('stores_id', $value->stores_id)
                        ->where('year', date('Y', time()))
                        ->where('month', date('m', time()))
                        ->increment('volume', $value->quantity))
                    {
                        DB::rollBack();
                        \Log::debug('添加商户月销量日志失败');
                        exit;
                    }
                }
            }

            DB::commit();
            return 1;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            exit;
        }

    }
}
