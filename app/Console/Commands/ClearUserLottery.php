<?php

namespace App\Console\Commands;

use App\Models\Users;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearUserLottery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:user:lottery';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '初始化用户抽奖次数';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userModel          = new Users();

        DB::table($userModel->getTable())
            ->update(['lottery' => 1]);
    }
}
