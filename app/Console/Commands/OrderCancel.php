<?php

namespace App\Console\Commands;

use App\Models\Alipay;
use App\Models\GoodsBargain;
use App\Models\OrderBargain;
use App\Models\OrderGoods;
use App\Models\OrderRefund;
use App\Models\Stores;
use App\Models\Users;
use App\Models\UserWalletLog;
use App\Models\WechatPay;
use EasyWeChat\Factory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use App\Models\Orders;
use App\Models\StoreBooks;
use App\Models\StoreBookOrder;
use Illuminate\Support\Facades\DB;
use Yansongda\Pay\Pay;

class OrderCancel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '取消超时订单';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('default_socket_timeout',-1);
        $cachedb = 1;
        $pattern = '__keyevent@'.$cachedb.'__:expired';
        Redis::psubscribe([$pattern], function($msg){

            $expiredKey = trim(strstr($msg, ':'), ':');
            $key_type = str_before($msg, ':');

            if ($key_type == 'order-cancel'){
                //普通订单15分钟内未支付自动取消
                $orderData = Orders::where('batch', $expiredKey)->get();
                $orderId = [];

                foreach ($orderData as $value) {
                    if ($value->status == 0){
                        $orderId[] = $value->id;
                    }
                }
                if (!empty($orderId)){
                    Orders::whereIn('id', $orderId)->update(['status' => 5]);
                }

            }else if ($key_type == 'order-book-cancel'){
                //预约订单取消
                try{
                    DB::beginTransaction();
                    $order = StoreBookOrder::where('book_id', $expiredKey)->first();
                    if ($order && $order->status === 0){
                        if(StoreBooks::where('id', $expiredKey)->update([
                                'status'        => 2
                            ]) === false
                        ){
                            DB::rollBack();
                        }

                        if(StoreBookOrder::where('book_id', $expiredKey)->update([
                                'status'        => 5
                            ])  === false
                        ){
                            DB::rollBack();
                        }
                    }

                    DB::commit();
                }catch (\Exception $exception){
                    DB::rollBack();
                    \Log::error($exception);
                }
            }
//            else if ($key_type == 'orderTypeFour-cancel'){
//                //聚南珍订单5分钟内商家未接单自动原路退款
//                $orderModel         = new Orders();
//                $storeModel         = new Stores();
//                $userModel          = new Users();
//                $orderGoodsModel    = new OrderGoods();
//                $orderRefundModel   = new OrderRefund();
//
//                $expiredKey = json_decode($expiredKey, true);
//                try{
//                    $orderInfo      = DB::table($orderModel->getTable().' as o')
//                        ->select('o.*', 's.store_type_id', 'u.balance')
//                        ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'o.stores_id')
//                        ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'o.users_id')
//                        ->whereIn('o.id', $expiredKey)
//                        ->get();
//                    \Log::debug(json_encode($orderInfo));
//
//                    $refundData = [];
//                    $i = 0;
//                    foreach ($orderInfo as $item) {
//                        if ($item->status != 1){
//                            continue;
//                        }
//
//                        //修改订单状态为取消
//                        DB::table($orderModel->getTable())
//                            ->where('id', $item->id)
//                            ->update([
//                                'status'        => 5
//                            ]);
//                        //微信支付退款
//                        if ($item->pay_type == 1){
//                            $refundType         = 1;
//                            $weChatPayModel     = new WechatPay();
//                            $transactionId      = DB::table($weChatPayModel->getTable())
//                                ->where('orders_id', $item->id)
//                                ->value('transaction_id');
//                            \Log::debug('商户订单号：'.$transactionId);
//                            \Log::debug('商户退款单号：'.md5($item->batch.time()));
//                            \Log::debug('金额：'.$item->pay_amount);
//                            $app                = Factory::payment(\Config::get('zhongxin-pay.app'));
//                            $result             = $app->refund->byTransactionId(
//                                $transactionId,
//                                md5($item->batch.time()),
//                                $item->pay_amount,
//                                $item->pay_amount, [
//                                'refund_desc' => '商家未接单，取消订单，金额退回',
//                            ]);
//                            \Log::debug('微信退款信息'.json_encode($result));
//
//                            if (!empty($result)){
//                                if ($result['return_code'] == 'SUCCESS'){
//                                    if ($result['result_code'] == 'SUCCESS'){
//                                        $message        = '成功';
//                                        $refundStatus   = 3;
//                                    }else{
//                                        $message        = $result['err_code'].'【'.$result['err_code_des'].'】';
//                                        $refundStatus   = 5;
//                                    }
//
//                                }else{
//                                    $message        = '失败';
//                                    $refundStatus   = 5;
//                                }
//                            }else{
//                                $message        = '失败';
//                                $refundStatus   = 5;
//                            }
//
//                        }
//                        //支付宝退款
//                        if ($item->pay_type == 2){
//                            $refundType         = 2;
//                            $aliPayModel        = new Alipay();
//                            $outTradeNo         = DB::table($aliPayModel->getTable())
//                                ->where('orders_id', $item->id)
//                                ->value('out_trade_no');
//                            $result             = Pay::alipay(config('ali-pay.config'))->refund([
//                                'out_trade_no'          => $outTradeNo,
//                                'refund_amount'         => priceIntToFloat($item->pay_amount),
//                                'refund_reason'         => '商家未接单，取消订单，金额退回'
//                            ]);
//
//                            if ($result['msg'] == 'Success' && $result['code'] == 10000){
//                                $message        = '成功';
//                                $refundStatus   = 3;
//                            }else{
//                                $message        = '失败';
//                                $refundStatus   = 5;
//                            }
//                        }
//                        //余额退款
//                        if ($item->pay_type == 3){
//                            $refundType         = 3;
//                            $userWalletModel    = new UserWalletLog();
//
//                            if(!DB::table($userWalletModel->getTable())
//                                ->insert([
//                                    'users_id'      => $item->users_id,
//                                    'money'         => $item->pay_amount,
//                                    'balance'       => $item->balance,
//                                    'wallet_type'   => 1,
//                                    'stores_id'     => $item->stores_id,
//                                    'type'          => 5,
//                                    'remake'        => '商家未接单，取消订单，金额退回'
//                                ])
//                            ){
//                                $message        = '失败';
//                                $refundStatus   = 5;
//                            }
//
//                            if(!DB::table($userModel->getTable().' as u')
//                                ->where('id', $item->users_id)
//                                ->increment('balance', $item->pay_amount)
//                            ){
//                                $message        = '失败';
//                                $refundStatus   = 5;
//                            }else{
//                                $message        = '成功';
//                                $refundStatus   = 3;
//                            }
//                        }
//
//                        $refundOrderGoods = DB::table($orderGoodsModel->getTable())
//                            ->where('orders_id', $item->id)
//                            ->get()->toArray();
//
//
//                        foreach ($refundOrderGoods as $value) {
//                            $refundData[$i]['users_id']                 = $item->users_id;
//                            $refundData[$i]['stores_id']                = $item->stores_id;
//                            $refundData[$i]['orders_id']                = $value->orders_id;
//                            $refundData[$i]['refund_transaction_id']    = md5($item->batch.time());
//                            $refundData[$i]['order_goods_num']          = count($refundOrderGoods);
//                            $refundData[$i]['order_goods_id']           = $value->id;
//                            $refundData[$i]['goods_amount']             = $value->selling_price;
//                            $refundData[$i]['goods_sku']                = $value->sku;
//                            $refundData[$i]['goods_quantity']           = $value->quantity;
//                            $refundData[$i]['pay_amount']               = $item->pay_amount;
//                            $refundData[$i]['refund_amount']            = $item->pay_amount;
//                            $refundData[$i]['cause']                    = '商家未接单，取消订单，金额退回';
//                            $refundData[$i]['type']                     = $refundType;
//                            $refundData[$i]['message']                  = $message;
//                            $refundData[$i]['status']                   = $refundStatus;
//                            $i++;
//                        }
//                    }
//                    \Log::debug($refundData);
//                    if (isset($refundData) && !empty($refundData)){
//                        if(!DB::table($orderRefundModel->getTable())
//                            ->insert($refundData)
//                        ){
//                            \Log::debug('添加退款信息失败');
//                        }
//                    }
//
//
//                    DB::commit();
//                }catch (\Exception $exception){
//                    \Log::error($exception);
//                }
//
//            }
            else if($key_type == 'bargain-order-cancel'){
                //砍价失败订单修改订单状态
                $orderBargainModel      = new OrderBargain();
                $goodsBargainModel      = new GoodsBargain();

                $bargain = DB::table($orderBargainModel->getTable())
                    ->where('id', $expiredKey)
                    ->first();

                if ($bargain->status == 0){
                    try{
                        DB::beginTransaction();

                        if(!DB::table($orderBargainModel->getTable())
                            ->where('id', $expiredKey)
                            ->update([
                                'status'    => 2
                            ])
                        ){
                            \Log::error('取消砍价订单失败');
                            DB::rollBack();
                        }

                        if(!DB::table($goodsBargainModel->getTable())
                            ->where('goods_id', $bargain->goods_id)
                            ->increment('total_sum')
                        ){
                            \Log::error('修改砍价商品可下单数量失败');
                            DB::rollBack();
                        }

                        DB::commit();
                    }catch(\Exception $exception){
                        \Log::error('取消砍价订单失败'.$exception);
                        DB::rollBack();
                    }

                }
            }

        });

    }
}
