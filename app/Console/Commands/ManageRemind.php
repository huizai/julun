<?php

namespace App\Console\Commands;

use App\Models\StoreContractLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\StoreContract;

class ManageRemind extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manage:remind';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '定时提醒商户需缴纳费用';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $storeContractModel     = new StoreContract();
        $storeContractLogModel  = new StoreContractLog();
        $store = DB::table($storeContractModel->getTable())
            ->where('remind_time', '<=', date('Y-m-d', time()))
            ->where('is_remind', 1)
            ->get();


        $storeId  = [];
        $response = '';
        $logData  = [];
        if (!$store->isEmpty()){
            foreach ($store as $item) {
                $response = \App\Libs\Aliyun\Sms::manageRemindSms($item->mobile);
                $logData['stores_id'] = $item->id;
                $logData['mobile'] = $item->mobile;
                if (!empty($response) && $response->Code === 'OK'){
                    $storeId[] = $item->id;
                    $logData['sms_status'] = '成功';
                }else if (!empty($response) && $response->Code !== 'OK'){
                    $logData['sms_status'] = '失败';
                }
            }


            /**
             * 发送的商户修改提醒状态
             */
            if (!empty($response) && $response->Code === 'OK'){
                try{
                    DB::beginTransaction();

                    if(
                        !DB::table($storeContractModel->getTable())
                            ->whereIn('id', $storeId)
                            ->update(['is_remind' => 0]))
                    {
                        DB::rollBack();
                        \Log::debug('发送成功，修改商户是否需要提醒失败');
                    }
                    if(
                        !DB::table($storeContractLogModel->getTable())
                            ->insert($logData))
                    {
                        DB::rollBack();
                        \Log::debug('发送成功，添加商户提醒日志失败');
                    }

                    DB::commit();
                }catch (\Exception $exception){

                    \Log::error('商家提交缴写入失败，原因：'.$exception);
                }

            }
        }



    }
}
