<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\UserCoupons;

class DeleteUserCoupon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:coupon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '删除用户过期优惠券';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userCouponModel        = new UserCoupons();
        $expiredCoupon = DB::table($userCouponModel->getTable())
            ->select('id')
            ->where('end_time', '<', date('Y-m-d H:i:s', time()))
            ->whereNull('delete_time')
            ->where('is_use', 0)
            ->get();

        $couponId = [];
        foreach ($expiredCoupon as $item) {
            $couponId[] = $item->id;
        }

        DB::table($userCouponModel->getTable())
            ->whereIn('id', $couponId)
            ->update(['delete_time' => date('Y-m-d H:i:s', time())]);
    }
}
