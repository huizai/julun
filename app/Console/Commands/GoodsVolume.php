<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Goods;

class GoodsVolume extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'goods:volume';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '清除商品月销量，写入日志';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $goodsModel         = new Goods();

        DB::table($goodsModel->getTable())->update(['mouth_sales_volume' => 0]);

    }
}
