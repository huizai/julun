<?php

namespace App\Console\Commands;


use App\Models\Arouse;
use App\Models\SmsContent;
use App\Models\Users;
use Illuminate\Console\Command;

class UserArouse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:arouse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '唤醒长时间未登录的用户';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $arouseModel        = new Arouse();
//        $userModel          = new Users();
//        $smsContentModel    = new SmsContent();
//
//        $day                = $arouseModel->getArouseDay();
//        $userData           = $userModel->getNotLoginUser($day);
//
//
//        $userMobile = [];
//        foreach ($userData as $item) {
//            if (!empty($item->mobile)){
//                $userMobile[] = $item->mobile;
//            }
//        }
//        $content = $smsContentModel->getContent();

        $userMobile = ['15673396137', '17711768276'];

        $content = [
            [
                'content' => '100元'
            ],
            [
                'content' => '100元'
            ]
        ];
        $sign = [
            '梦想商区','梦想商区'
        ];

        $response = \App\Libs\Aliyun\Sms::notLoginUser($sign, $userMobile, $content);
        \Log::debug(json_encode($response));
    }
}
