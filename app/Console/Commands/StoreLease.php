<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
USE App\Models\StoreLease as StoreLeaseModel;

class StoreLease extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:lease';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '修改商户租赁过期信息状态';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $storeLeaseModel        = new StoreLeaseModel();
        $data = DB::table($storeLeaseModel->getTable())
            ->select('id')
            ->where('lease_end_time', '<=', date('Y-m-d'))
            ->get()->toArray();

        if (!empty($data)){
            $leaseId = [];
            foreach ($data as $value) {
                $leaseId[] = $value->id;
            }

            $result = DB::table($storeLeaseModel->getTable())
                ->whereIn('id', $leaseId)
                ->update(['status' => 2]);
        }

    }
}
