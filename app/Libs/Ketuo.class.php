<?php

namespace App\Libs;

class Ketuo {
    //正式
    private $uri = "http://pay1.keytop.cn:8099/api/wec";
    //测试
    // private $uri="http://220.160.111.118:8099/api/wec";

    private function getSign($secret, $params){
        $date = date('Ymd', time());
        $str = '';
        foreach($params as $p){
            $str .= $p;
        }
        $str .= $date.$secret;

        return md5($str);
    }

    /**
     * 车场信息相关接口
     */
    public function getParkingLotList(){
        $secret = env('KETUO_SECRET');
        $url = $this->uri. '/GetParkingLotList';
        $params = [];
        $sign = $this->getSign($secret, $params);

        $params['appId'] = env('KETUO_APPID');
        $params['key'] = $sign;

        $res = curlPost($url, $params);

        $res = json_decode($res);
        if($res->resCode == 0){
            return $res->data;
        }

        return $res;
    }


    /**
     * 停车场信息查询接口
     * @return mixed
     */
    public function getParkingLotInfo(){
        $secret = env('KETUO_SECRET');
        $url = $this->uri. '/GetParkingLotInfo';

        $params = [];

        $params['parkId'] = env('KETUO_PARK_ID');
        $params['appId'] = env('KETUO_APPID');
        $params['key'] = $this->getSign($secret, [$params['parkId']]);

        $res = curlPost($url, $params);
        return json_decode($res)->data;
    }


    /**
     * 停车场设备状态查询接口
     * @return mixed
     */
    public function getDeviceStatusList(){
        $secret = env('KETUO_SECRET');
        $url = $this->uri. '/GetDeviceStatusList';

        $params = [
            'parkId' => env('KETUO_PARK_ID')
        ];
        $sign = $this->getSign($secret, $params);

        $params['appId'] = env('KETUO_APPID');
        $params['key'] = $sign;

        $res = curlPost($url, $params);

        return json_decode($res);
    }

    /**
     * 停车记录查询接口
     * @return mixed
     */
    public function carParkingInfo(){
          $secret = env('KETUO_SECRET');

          $url = $this->uri. '/CarParkingInfo';

          $params = [
              'parkId'      => env('KETUO_PARK_ID'),
              'plateNo'     => '新ADM606',
              'pageIndex'   => 1,
              'pageSize'    => 20
          ];

          $sign = $this->getSign($secret, $params);

          $params['appId'] = env('KETUO_APPID');
          $params['key'] = $sign;

          $res = curlPost($url, $params);

          return json_decode($res);
    }

    /**
     * 查询车牌费用
     * @return mixed
     */
    public function getParkingPaymentInfo($number){
        $secret = env('KETUO_SECRET');
        $url = $this->uri. '/GetParkingPaymentInfo';
        $params = [
              'parkId'      => env('KETUO_PARK_ID'),
              'plateNo'     => $number //"新ADM606"
        ];
        $sign = $this->getSign($secret, $params);

        $params['appId'] = env('KETUO_APPID');
        $params['key'] = $sign;

        $res = curlPost($url, $params);
        return json_decode($res, true)['data'][0];
    }

    /**
     *  账单减免计费
     */
    public function getPaymentRecharge($ordeNo, $freeMoney = 0, $freeTime = 0){
        $secret = env('KETUO_SECRET');
        $url = $this->uri. '/GetPaymentRecharge';
        $params = [
            'orderNo'       => $ordeNo,
            'freeMoney'     => $freeMoney,
            'freeTime'      => $freeTime
        ];
        $sign = $this->getSign($secret, $params);

        $params['appId'] = env('KETUO_APPID');
        $params['key'] = $sign;

        $res = curlPost($url, $params);

        \Log::debug($res);
        return json_decode($res, true);
    }

    /**
     * 支付订单同步
     * @param $orderNo      //订单号
     * @param $amount       //支付金额
     * @param $freeMoney    //减免金额
     * @param $payType      //支付方式
     * @param $userId       //用户ID
     * @return mixed
     */
    public function payParkingFee2($orderNo, $amount, $freeMoney, $payType, $userId, $freeTime){
        $secret = env('KETUO_SECRET');
        $url = $this->uri. '/PayParkingFee2';

        $freeDetail = [
            [
                'type'      => 2,           //抵扣来源 0：积分 1：抵扣券 2：购物小票
                'money'     => $freeMoney,  //抵扣金额
                'time'      => $freeTime,           //抵扣时间
                'code'      => $userId      //会员id/抵扣券编号/购物小票号
            ]
        ];
        $params = [
            'orderNo'       => $orderNo,    //订单号
            'amount'        => $amount,     //支付金额（不包含减免）
            'payType'       => $payType,    //收费终端 4：微信 5：App
            'payMethod'     => 3,           //付款方式 1：现金 2：银行卡 3：电子现金
            'freeMoney'     => $freeMoney,  //减免总金额 单位：分
            'freeTime'      => $freeTime,           //减免总时间 单位：秒
            'freeDetail'    => json_encode($freeDetail)
        ];

        \Log::debug('停车场订单同步数据---------------------------'.json_encode($params));
        $sign = $this->getSign($secret, $params);

        $params['appId'] = env('KETUO_APPID');
        $params['key'] = $sign;

        $res = curlPost($url, $params);


        return json_decode($res);
    }
}
