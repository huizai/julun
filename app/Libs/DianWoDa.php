<?php
namespace App\Libs;

use App\Models\StoreTakeaway;
use Illuminate\Support\Facades\DB;

class DianWoDa{

    function __construct() {
        $this->gateway = 'https://open-test.dianwoda.com/gateway';
        $this->appkey = env('DIANWODA_APPKEY');
        $this->app_secret = env('DIANWODA_SECRET');
        $this->access_token = env('DIANWODA_ACCESS_TOKEN');
    }

    /**
     * 生成签名
     * @param $appkey
     * @param $timestamp
     * @param $nonce
     * @param $access_token
     * @param $api
     * @param $secret
     * @param $biz_params
     * @return string
     */
    private function sign( $timestamp, $nonce, $api, $biz_params ) {
        $unsignRequestParams = array(
            'appkey' => $this->appkey,
            'timestamp' => $timestamp,
            'nonce' => $nonce,
            'api' => $api,
            'access_token' => $this->access_token
        );
//        if($access_token != NULL) {
//            $unsignRequestParams["access_token"] = $access_token;
//        }

        ksort($unsignRequestParams);
        $unsignStr = '';
        foreach ($unsignRequestParams as $key => $value){
            $unsignStr = $unsignStr.$key.'='.$value.'&';
        }
        $unsignStr = $unsignStr.'body='.$biz_params;
        $unsignStr = $unsignStr.'&secret='.$this->app_secret;
        return sha1($unsignStr);
    }

    function getMillisecond() {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }

    function request($api, $apiParam) {
        $bodyStr = $apiParam == NULL ? "" : json_encode($apiParam);
        $timestamp = $this->getMillisecond();
        $nonce = mt_rand(0, 1000000);
        $sign = $this->sign($timestamp, $nonce, $api, $bodyStr);

        $url = $this->gateway.'?appkey='.$this->appkey.'&timestamp='.$timestamp.'&nonce='.$nonce.'&access_token='.$this->access_token.'&api='.$api.'&sign='.$sign;

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/json; charset=utf-8",
                'method'  => 'POST',
                'content' => $bodyStr
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if($result === FALSE){
            throw new Exception("请求gateway异常");
        }

        return json_decode($result, true);
    }

    /**
     * 门店运力审核请求
     */
    public function sellerTransportationConfirm($data){
        $storeTakeawayModel         = new StoreTakeaway();

        $paramsData = [
            'city_code'             => '430100',                        //城市编码，通过1.4行政区代码接口获取
            'seller_id'             => env('DIANWODA_APPKEY').$data['stores_id'],     //点我达APP_KEY和商户系统的商户ID链接
            'seller_name'           => $data['store_name'],             //商户名称
            'seller_mobile'         => $data['mobile'],                 //商户手机号码
            'seller_address'        => $data['address'],                //商户地址
            'seller_lat'            => $data['lat'],                    //经度
            'seller_lng'            => $data['lng'],                    //纬度
            'expect_active_time'    => getMillisecond()                 //运力生效时间，单位unix毫秒级时间戳
        ];
        $resultConfirm  = $this->request('dianwoda.seller.transportation.confirm', $paramsData);
        \Log::debug('审核请求返回结果'.json_encode($resultConfirm));
        $resultData     = [];
        $saveData       = [];
        $saveData['stores_id']      = $data['stores_id'];
        $resultData['stores_id']    = $data['stores_id'];
        if ($resultConfirm['code'] == 'success'){
            $resultData['code']     = 1;
        }else{
            $resultData['code']     = 2;

            $saveData['code']           = $resultConfirm['code'];
            $saveData['message']        = $resultConfirm['message'];
            DB::table($storeTakeawayModel->getTable())
                ->insert($saveData);
            return 'failed';
        }

        $confirmResult = $this->sellerTransportationConfirmResult($resultData);
        \Log::debug('审核结果返回结果'.json_encode($confirmResult));

        if ($confirmResult['code'] == 'success'){
            $createResult               = $this->sellerBatchCreate([$data]);
            $code                       = $createResult['code'];
            $saveData['code']           = $createResult['code'];
            \Log::debug('创建门店返回结果'.json_encode($createResult));

            if ($createResult['code'] == 'success'){
                $saveData['message']        = $createResult['message'];
            }else{
                $saveData['message']        = '创建店铺失败'.$createResult['message'];
            }
        }else{
            $saveData['code']           = $confirmResult['code'];
            $saveData['message']        = '门店运力审核失败，失败原因：'.$confirmResult['message'];
        }


        DB::table($storeTakeawayModel->getTable())
            ->insert($saveData);

        return isset($code) ? $code : 'failed';
    }

    /**
     * 门店运力审核结果【消息】
     * @param $result
     * @return mixed
     */
    public function sellerTransportationConfirmResult($result){
        $paramsData = [
            'delivery_active_time'      => getMillisecond(),                    //运力生效时间，单位unix毫秒级时间戳
            'seller_id'                 => env('DIANWODA_APPKEY').$result['stores_id'],     //客户系统中的门店id，门店的唯一性标识
            'result'                    => $result['code'],                     //审核结果：1、通过；2、不通过
            'city_code'                 => '430100'                             //城市编码，文档上没有体现出来，需要根据接口返回值添加
        ];
        return $this->request('dianwoda.seller.transportation.confirm-result', $paramsData);
    }

    /**
     * 创建或更新门店信息
     * @return mixed
     */
    public function sellerBatchCreate($data){
        $sellers                        = [];
        foreach ($data as $key => $value) {
            $sellers[$key]['city_code']        = '430100';
            $sellers[$key]['seller_id']        = env('DIANWODA_APPKEY').$value['stores_id'];
            $sellers[$key]['seller_name']      = $value['store_name'];
            $sellers[$key]['seller_mobile']    = $value['mobile'];
            $sellers[$key]['seller_lng']       = $value['lng'];
            $sellers[$key]['seller_lat']       = $value['lat'];
            $sellers[$key]['seller_address']   = $value['address'];
        }
        $paramsData = ['sellers' => $sellers];

        return $this->request('dianwoda.seller.batch-create', $paramsData);
    }

    /**
     * 推送订单
     * @return mixed
     */
    public function orderCreate($data, $userAddress, $goodsData){
        $goods                  = [];
        foreach ($goodsData as $key => $good) {
            $goods[$key]['item_name']          = $good['goods_name'];
            $goods[$key]['unit']               = '份';
            $goods[$key]['quantity']           = $good['quantity'];
            $goods[$key]['unit_price']         = $good['selling_price'];
            $goods[$key]['discount_price']     = $good['selling_price'];
        }

        \Log::debug($data);
        $paramsData             = [
            'order_original_id'             => env('DIANWODA_APPKEY').'_'.$data['batch'],
            'order_create_time'             => getMillisecond(),
            'order_remark'                  => $data['order_remark'],
            'serial_id'                     => 1,
            'waybillno'                     => getSalt(32),
            'order_price'                   => $data['pay_amount'],
            'city_code'                     => '410100',
            'seller_id'                     => env('DIANWODA_APPKEY').$data['stores_id'],
            'seller_name'                   => $data['store_name'],
            'seller_mobile'                 => $data['mobile'],
            'seller_address'                => $data['address'],
            'seller_lat'                    => $data['lat'],
            'seller_lng'                    => $data['lng'],
            'consignee_name'                => $userAddress['name'],
            'consignee_mobile'              => $userAddress['mobile'],
            'consignee_address'             => $userAddress['address'],
            'consignee_lat'                 => $userAddress['lat'],
            'consignee_lng'                 => $userAddress['lng'],
            'cargo_weight'                  => '0',
            'cargo_num'                     => '1',
            'items'                         => $goods
        ];
        return $this->request('dianwoda.order.create', $paramsData);

    }

    /**
     * 获取配送费
     * @param $data
     * @return mixed
     */
    public function orderCostEstimate($storeAddress, $userAddress){
        $params = [
            'city_code'         => '410100',
            'seller_id'         => env('DIANWODA_APPKEY').$storeAddress['stores_id'],
            'seller_name'       => $storeAddress['stores_name'],
            'seller_mobile'     => $storeAddress['mobile'],
            'seller_address'    => $storeAddress['address'],
            'seller_lat'        => $storeAddress['lat'],
            'seller_lng'        => $storeAddress['lng'],
            'consignee_address' => $userAddress['address'],
            'consignee_lat'     => $userAddress['lat'],
            'consignee_lng'     => $userAddress['lng'],
            'cargo_weight'      => 0
        ];
        return $this->request('dianwoda.order.cost.estimate', $params);
    }

    /**
     * 查询账户余额
     * @return mixed
     */
    public function accountBalanceQuery(){
        $paramsData         = [
            'account_type'  => 'seller',
            'seller_id'     => env('DIANWODA_APPKEY').'1'
        ];
        return $this->request('dianwoda.account-balance.query', $paramsData);

    }
}