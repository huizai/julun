<?php

/**
 * 快递鸟
 */
namespace App\Libs;

class ExpressDelivery
{
    private $EBusinessID    = '1581764';
    private $AppKey         = 'eda28739-8b42-4b61-9e96-459ff4cd7e14';
    //查询快递
    private $ReqURL         = 'http://api.kdniao.com/Ebusiness/EbusinessOrderHandle.aspx';
    //查询快递公司
    private $IdentificationNumberURL = 'http://api.kdniao.com/Ebusiness/EbusinessOrderHandle.aspx';

    /**
     * 查询快递
     * @param $ShipperCode      //快递公司缩写（大写）
     * @param $LogisticCode     //快递单号
     * @return array|bool
     */
    public function getOrderTracesByJson($ShipperCode, $LogisticCode){
        $requestData= "{'OrderCode':'', 'ShipperCode': '{$ShipperCode}', 'LogisticCode': {$LogisticCode}}";

        $data = array(
            'EBusinessID' => $this->EBusinessID,
            'RequestType' => '1002',
            'RequestData' => urlencode($requestData),
            'DataType' => '2',
        );
        $data['DataSign'] = $this->encrypt($requestData, $this->AppKey);
        $express = \Qiniu\json_decode($this->sendPost($this->ReqURL, $data));


        if ($express->Success === true){
            $expressData = [];

            $count = count($express->Traces);
            $k = 0;
            for ($i = $count-1; $i >= 0; $i--){
                $expressData[$k]['AcceptStation'] = $express->Traces[$i]->AcceptStation;
                $expressData[$k]['AcceptTime'] = $express->Traces[$i]->AcceptTime;
                $time = strtotime($express->Traces[$i]->AcceptTime);
                $expressData[$k]['day'] = date('m-d', $time);
                $expressData[$k]['hour'] = date('H:i', $time);
                $k++;
            }

            $result = $expressData;
        }else{
            $result = false;
        }
        return $result;
    }

    /**
     * 根据订单号查询快递公司
     * @param $LogisticCode     //快递单号
     * @return string
     */
    public function IdentificationNumber($LogisticCode){
        $requestData= "{'LogisticCode': $LogisticCode}";
        $data = array(
            'EBusinessID' => $this->EBusinessID,
            'RequestType' => '2002',
            'RequestData' => urlencode($requestData),
            'DataType' => '2',
        );
        $data['DataSign'] = $this->encrypt($requestData, $this->AppKey);
        $result = $this->sendPost($this->IdentificationNumberURL, $data);

        /**
         * 返回数据转数组格式
         */
//        [
//            'LogisticCode'  => $LogisticCode,
//            'Shippers'      => [
//                [
//                    'ShipperName'       => 'EMS',
//                    'ShipperCode'       => 'EMS'
//                ],
//                [
//                    'ShipperName'       => '邮政快递',
//                    'ShipperCode'       => 'YZPY'
//                ]
//            ],
//            'EBusinessID'   => $this->EBusinessID,
//            'Code'          => 100,
//            'Success'       => 1
//        ]


        return $result;
    }

    private function sendPost($url, $data) {
        $temps = array();
        foreach ($data as $key => $value) {
            $temps[] = sprintf('%s=%s', $key, $value);
        }
        $post_data = implode('&', $temps);
        $url_info = parse_url($url);
        if(empty($url_info['port']))
        {
            $url_info['port']=80;
        }
        $httpHeader = "POST " . $url_info['path'] . " HTTP/1.0\r\n";
        $httpHeader.= "Host:" . $url_info['host'] . "\r\n";
        $httpHeader.= "Content-Type:application/x-www-form-urlencoded\r\n";
        $httpHeader.= "Content-Length:" . strlen($post_data) . "\r\n";
        $httpHeader.= "Connection:close\r\n\r\n";
        $httpHeader.= $post_data;
        $fd = fsockopen($url_info['host'], $url_info['port']);
        fwrite($fd, $httpHeader);
        $gets = "";
        $headerFlag = true;
        while (!feof($fd)) {
            if (($header = @fgets($fd)) && ($header == "\r\n" || $header == "\n")) {
                break;
            }
        }
        while (!feof($fd)) {
            $gets.= fread($fd, 128);
        }
        fclose($fd);

        return $gets;
    }

    private function encrypt($data, $appKey) {
        return urlencode(base64_encode(md5($data.$appKey)));
    }

    /**
     * 查询快递（不能查询顺丰京东）
     * @return array|bool|int
     */
    public function checkExpress($express){
        $host = "https://kuaidid.market.alicloudapi.com";
        $path = "/danhao";
        $method = "POST";
        $appcode = "8eab4dd2d10d49f69a8f8e464d116ecb";
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        //根据API的要求，定义相对应的Content-Type
        array_push($headers, "Content-Type".":"."application/x-www-form-urlencoded; charset=UTF-8");
        $querys = "";
        $bodys = "src=$express";
        $url = $host . $path;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $bodys);
        $content = \Qiniu\json_decode(curl_exec($curl), true);
        curl_close($curl);

        if ($content['status'] == 200 && isset($content['msg']['context'])){
            $expressData = [];

            $count = count($content['msg']['context']);
            for ($i = 0; $i < $count; $i++){
                if ($i-1 >=0 && $content['msg']['context'][$i]['time'] > $content['msg']['context'][$i-1]['time']){
                    $expressData[$i]['AcceptStation'] = $content['msg']['context'][$i-1]['desc'];
                    $expressData[$i]['AcceptTime'] = date('Y-m-d H:i:s', $content['msg']['context'][$i-1]['time']);
                    $time = $content['msg']['context'][$i-1]['time'];
                    $expressData[$i]['day'] = date('m-d', $time);
                    $expressData[$i]['hour'] = date('H:i', $time);

                    $expressData[$i-1]['AcceptStation'] = $content['msg']['context'][$i]['desc'];
                    $expressData[$i-1]['AcceptTime'] = date('Y-m-d H:i:s', $content['msg']['context'][$i]['time']);
                    $time = $content['msg']['context'][$i]['time'];
                    $expressData[$i-1]['day'] = date('m-d', $time);
                    $expressData[$i-1]['hour'] = date('H:i', $time);

                }else{
                    $expressData[$i]['AcceptStation'] = $content['msg']['context'][$i]['desc'];
                    $expressData[$i]['AcceptTime'] = date('Y-m-d H:i:s', $content['msg']['context'][$i]['time']);
                    $time = $content['msg']['context'][$i]['time'];
                    $expressData[$i]['day'] = date('m-d', $time);
                    $expressData[$i]['hour'] = date('H:i', $time);
                }

            }

            $result = $expressData;
        }else if($content['status'] == 200 && !isset($content['msg']['context'])) {
            $result = -1;
        }else  if ($content['status'] == 500){
            $result = -2;
        }else{
            $result = false;
        }
        return $result;

    }

    /**
     * 查询快递（可查询顺丰、京东）
     * @param $company
     * @param $express
     * @return array|int
     */
    public function getExpress($company, $express){
        $host = "https://ali-deliver.showapi.com";
        $path = "/showapi_expInfo";
        $method = "GET";
        $appcode = "8eab4dd2d10d49f69a8f8e464d116ecb";
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = "com=$company&nu=$express";
        $bodys = "";
        $url = $host . $path . "?" . $querys;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        $content = \GuzzleHttp\json_decode(curl_exec($curl), true);
        if ($content['showapi_res_code'] == 0 && $content['showapi_res_body']['ret_code'] == 0){
            $count          = count($content['showapi_res_body']['data']);
            $expressData    = [];
            for ($i = 0; $i < $count; $i++){
                $time = strtotime($content['showapi_res_body']['data'][$i]['time']);
                $expressData[$i]['AcceptStation'] = $content['showapi_res_body']['data'][$i]['context'];
                $expressData[$i]['AcceptTime'] = date('Y-m-d H:i:s', $time);
                $expressData[$i]['day'] = date('m-d', $time);
                $expressData[$i]['hour'] = date('H:i', $time);

            }
            $result = $expressData;
        }else{
            $result = -1;
        }

        return $result;
    }

    /**
     * 根据快递订单号查询快递公司
     * @param $number
     * @return string
     */
    public function getExpressOrderCompany($number){
        $host = "https://ali-deliver.showapi.com";
        $path = "/fetchCom";
        $method = "GET";
        $appcode = "8eab4dd2d10d49f69a8f8e464d116ecb";
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = "nu=$number";
        $bodys = "";
        $url = $host . $path . "?" . $querys;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }

        $content = \Qiniu\json_decode(curl_exec($curl), true);
        if ($content['showapi_res_code'] === 0 && $content['showapi_res_body']['msg'] == '操作成功'){
            $result = $content['showapi_res_body']['data'][0]['simpleName'];
        }else{
            $result = 'auto';
        }

        return $result;
    }
}
