<?php

namespace App\Libs;

class ResponseMessage {
    /**
     * 定义通用报错列表
     *
     * @return	array
     */
    private $failedCodeList;

    private $status;
    private $failedCode;
    private $failedMsg;
    private $data;
    static private $instance;

    public function __construct(){
        self::intFailedCodeList();
        $this->status = 'success';
        $this->data = [];
    }

    private function intFailedCodeList(){
        $this->failedCodeList = [
            'FAILED'                        => '操作失败',
            'ERROR'                         => '未知错误',
            'NO_LOGIN'                      => '没有登录',
            'NO_USER'                       => '没有此用户',
            'PASSWORD_ERROR'                => '密码错误',
            'VALIDATE_CODE_ERROR'           => '验证码错误',
            'PARAM_ERROR'                   => '参数错误',
            'LOGIN_FAILED'                  => '登录失败',
            'CHOOSE_ADDRESS'                => '请选择地址',
            'USER_EXIST'                    => '用户已存在',
            'CREATE_EXIST'                  => '添加失败',
            'DATA_EXIST'                    => '该记录不存在',
            'UPDATE_EXIST'                  => '修改失败',
            'NO_MOBILE'                     => '手机号不存在',
            'MOBILE_EXIST'                  => '手机号已存在',
            'MOBILE_FORMAT_ERROR'           => '手机号格式错误',
            'ADD_ERROR'                     => '添加失败',
            'UPDATE_ERROR'                  => '修改失败',
            'DELETE_ERROR'                  => '删除失败',
            'PROHIBIT'                      => '禁止登陆',
            'MOBILE_BIND'                   => '手机号已经被绑定',
            'COMMENT_ERROR'                 => '评论失败'
        ];
    }

    static public function getInstance(){
        if(!(self::$instance instanceof ResponseMessage)){
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function success($data = []){
        $this->status = 'success';
        $this->failedCode = '';
        $this->failedMsg = '';
        $this->data = $data;
        return $this;
    }

    public function failed($failedCode = null){
        $this->status = 'failed';
        if(empty($this->failedCode) && empty($this->failedMsg)){
            $this->failedCode = isset($this->failedCodeList[$failedCode])?$failedCode:'ERROR';
            $this->failedMsg = isset($this->failedCodeList[$failedCode])?$this->failedCodeList[$failedCode]:'未知错误';
        }
        return $this;
    }

    public function setStatus($status){
        $this->status = $status;
        return $this;
    }

    public function setFailedCode($failedCode, $failedMsg){
        $this->failedCode = $failedCode;
        $this->failedMsg = $failedMsg;
        return $this;
    }

    public function setData($data){
        $this->data = $data;
        return $this;
    }

    public function response(){
        $response = ['status' => $this->status, 'failedCode' => $this->failedCode, 'failedMsg' => $this->failedMsg, 'data' => $this->data];
        return $response;
    }
}
