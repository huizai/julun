<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserKeyword extends Model{

    protected $table = 'user_keyword';
    public $timestamps = false;

    public function saveKeyword($userId, $keyword, $type){
        $keyWord = DB::table($this->table)
            ->where('users_id', $userId)
            ->where('keyword', $keyword)
            ->where('type', $type)
            ->first();
        if (!$keyWord){
            DB::table($this->table)->insert([
                'users_id'      => $userId,
                'keyword'       => $keyword,
                'type'          => $type
            ]);
        }
    }

    /**
     * 历史搜索关键字
     * @param $userId
     * @return mixed
     */
    public function keywordList($userId, $type){
        $sql = DB::table($this->table)
            ->select('keyword')
            ->where('users_id', $userId);

        if (!empty($type)){
            $sql->where('type', $type);
        }

        return $sql->get();
    }

    /**
     * 搜索热词
     * @return mixed
     */
    public function keywordHot($type){
        $sql = DB::table($this->table)
            ->select(
                'keyword',
                DB::raw('count(*) as count')
            );

        if (!empty($type)){
            $sql->where('type', $type);
        }

        return $sql->groupBy('keyword')
                ->orderBy('count', 'desc')
                ->limit(5)
                ->get();
    }
}
