<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSignLog extends Model{

    protected $table = 'user_sign_log';
    public $timestamps = false;
}
