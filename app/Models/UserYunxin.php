<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserYunxin extends Model{

    protected $table = 'user_yunxin';
    public $timestamps = false;

    /**
     * 添加用户云信信息
     * @param $userId
     * @param $accId
     * @param $token
     * @return bool
     */
    public function saveUserYunXinInfo($userId, $accId, $token){
        return DB::table($this->table)->insert([
            'users_id'      => $userId,
            'accid'         => $accId,
            'token'         => $token
        ]);
    }

    /**
     * 获取用户云信信息
     * @param $userId
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getUserYunXinInfo($userId){
        return DB::table($this->table)->where('users_id', $userId)->first();
    }

    /**
     * 刷新云信token
     * @param $userId
     * @param $accId
     * @param $token
     * @return int
     */
    public function updateUserYunXinInfo($userId, $accId, $token){
        return DB::table($this->table)->where('users_id', $userId)->update([
            'accid'         => $accId,
            'token'         => $token
        ]);
    }
}
