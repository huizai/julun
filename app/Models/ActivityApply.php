<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityApply extends Model{

    protected $table = 'activity_apply';
    public $timestamps = false;


    protected $fillable = [
        'users_id',
        'act_id'
    ];
}
