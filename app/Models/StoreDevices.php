<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreDevices extends Model{

    protected $table = 'store_devices';
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'stores_id',
        'name',
        'sort'
    ];
}
