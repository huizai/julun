<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Integral extends Model{

    protected $table = 'integral';
    public $timestamps = false;

    /**
     * 积分列表
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function index(){
        return DB::table($this->table)
            ->whereNull('delete_time')
            ->get();
    }




    /**
     * 修改积分
     * @param $id
     * @param $integral
     * @return mixed
     */
    public function updateIntegral($id, $integral){
        return DB::table($this->table)->where('id', $id)->update([
            'integral'      => $integral
        ]);
    }


    /**
     * 删除该操作积分
     * @param $id
     * @return mixed
     */
    public function deleteIntegral($id){
        return DB::table($this->table)->where('id', $id)->delete();
    }


}
