<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsEstimatesTag extends Model{

    protected $table = 'goods_estimates_tag';
    public $timestamps = false;

    protected $fillable = [
        'store_type_id',
        'title'
    ];
}
