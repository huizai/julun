<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class SmsContent extends Model{

    protected $table = 'sms_content';
    public $timestamps = false;

    public function getContent(){
        return DB::table($this->table)
            ->where('type', 1)
            ->value('content');
    }
}
