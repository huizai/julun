<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalancePay extends Model{

    protected $table = 'balance_pay';
    public $timestamps = false;
}
