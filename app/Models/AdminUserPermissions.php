<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdminUserPermissions extends Model{

    protected $table = 'admin_user_permissions';
    public $timestamps = false;

    public function getUserPermissions($id){
        return DB::table($this->table)->select('pid')->where('uid',$id)->get();
    }
}
