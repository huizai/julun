<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class StoreWithdraw extends Model{

    protected $table = 'store_withdraw';
    public $timestamps = false;

    public function getStoreWithdraw($storeId){
        return DB::table($this->table)
            ->where('stores_id', $storeId)
            ->Where('status', 0)
            ->first();
    }

    public function getStoreWithdrawStatus($id){
        return DB::table($this->table)->where('id', $id)->value('status');
    }
}
