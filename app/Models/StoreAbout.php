<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreAbout extends Model{

    protected $table = 'store_about';
    public $timestamps = false;


    protected $fillable = [
        'store_id',
        'title',
        'content',
        'image',
    ];
}
