<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StoreSeatLineup extends Model{

    protected $table = 'store_seat_lineup';
    public $timestamps = false;

    protected $fillable = ['users_id', 'stores_id', 'lineup_number', 'people'];

    public function userLineup($userId, $storeId){
        $storeModel             = new Stores();
        $storeSeatLineupModel   = new StoreSeatLineup();
        return DB::table($storeSeatLineupModel->getTable().' as sel')
            ->select('sel.*', 's.name')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'sel.stores_id')
            ->where('sel.stores_id', $storeId)
            ->where('sel.users_id', $userId)
            ->whereNull('sel.delete_time')
            ->where('sel.is_use', 0)
            ->orderBy('sel.create_time', 'desc')
            ->first();
    }

    public function getLineupTime($storeId, $people){
        return DB::table($this->table)->where('stores_id', $storeId)
            ->where('is_use', 0)
            ->whereNull('delete_time')
            ->where('people', '<=', $people)
            ->count();
    }

    public function userStoreLineup($userId){
        return DB::table($this->table)
            ->where('users_id', $userId)
            ->whereNull('delete_time')
            ->where('is_use', 0)
            ->orderBy('create_time', 'desc')
            ->get();
    }

    public function createLineup($userId, $storeId, $people, $lineupNumber){
        return DB::table($this->table)
            ->insertGetId([
                'users_id'      => $userId,
                'stores_id'     => $storeId,
                'people'        => $people,
                'lineup_number' => $lineupNumber
            ]);
    }
}
