<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PlantingGrass extends Model{

    protected $table = 'planting_grass';
    public $timestamps = false;

    /**
     * 通过种草ID获取用户ID
     * @param $id
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getUserInfo($id){
        $userModel          = new Users();
        $grassModel         = new PlantingGrass();
        return DB::table($grassModel->getTable().' as g')
            ->select('u.*')
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'g.users_id')
            ->where('g.id', $id)
            ->first();
    }
}
