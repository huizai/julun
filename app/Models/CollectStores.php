<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectStores extends Model{

    protected $table = 'collect_stores';
    public $timestamps = false;

    protected $fillable = ['users_id', 'stores_id'];
}
