<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOfflineCoupons extends Model{

    protected $table = 'user_offline_coupons';
    public $timestamps = false;
}
