<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWalletLog extends Model{

    protected $table = 'user_wallet_log';
    public $timestamps = false;


    protected $fillable = [
        'users_id'
    ];
}
