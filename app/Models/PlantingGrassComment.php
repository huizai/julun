<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlantingGrassComment extends Model{

    protected $table = 'planting_grass_comment';
    public $timestamps = false;
}
