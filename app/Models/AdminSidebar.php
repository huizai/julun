<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdminSidebar extends Model{

    protected $table = 'admin_sidebar';
    public $timestamps = false;

    /**
     * 菜单查询sql
     * @param $search
     * @return mixed
     */
    private function getSidebarSql($search){
        $sql =  DB::table($this->table)->whereNull('delete_time');

        if (isset($search['title'])){
            $sql->where('title', 'like', '%'.$search['title'].'%');
        }

        if (isset($search['id'])){
            $sql->where('id', $search['id']);
        }

        if (isset($search['sidebar_id'])){
            $sql->whereIn('id', $search['sidebar_id']);
        }

        return $sql;
    }

    /**
     * 菜单列表
     * @param $page
     * @param $pageSize
     * @param $search
     * @return mixed
     */
    public function getSidebarList($page, $pageSize, $search){
        $sql = $this->getSidebarSql($search);

        return $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();
    }

    /**
     * 菜单分页
     * @param $page
     * @param $pageSize
     * @param $search
     * @return array
     */
    public function getSidebarPagination($page, $pageSize, $search){
        $sql = $this->getSidebarSql($search);

        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    public function sidebarInfo($search){
        return $this->getSidebarSql($search)->first();
    }

    /**
     * 修改菜单基本信息
     * @param $data
     * @return mixed
     */
    public function sidebarUpdate($data){
        return DB::table($this->table)->where('id', $data['id'])->update($data);
    }

    /**
     * 删除菜单
     * @param $id
     * @return mixed
     */
    public function sidebarDelete($id){
        return DB::table($this->table)->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s',time())
        ]);
    }

    /**
     *添加菜单
     * @param $data
     * @return mixed
     */
    public function sidebarAdd($data){
        return DB::table($this->table)->insert($data);
    }
}
