<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GoodsImages extends Model{

    protected $table = 'goods_images';
    public $timestamps = false;

    protected $fillable = [
        'image',
        'goods_id',
    ];


    public function del($goodsId){
        $GoodsImages = new GoodsImages();
        return DB::table($GoodsImages->getTable())->where('goods_id',$goodsId)->delete();

    }
}
