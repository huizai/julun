<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StoreInfo extends Model
{
    protected $table = 'store_info';
    public $timestamps = false;

    public function getStoreInfo($storeId, $field){
        return DB::table($this->table)
            ->select($field)
            ->where('store_id', $storeId)
            ->first();
    }
}
