<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Stores;
use Illuminate\Support\Facades\DB;

class StoreCoupons extends Model{

    protected $table = 'store_coupons';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'term_of_validity',
        'quantity',
        'full_amount',
        'discount_amount',
        'type',
        'stores_id',
        'company_id',
        'effective_time',
        'scope',
        'selling_price',
        'create_time',
        'address',
        'belong',
        'virtual',
        'odds',
        'code',
        'recharge_amount',
        'rule',
        'admins_id'
    ];

    /**
     * 领券中心，所有商户的未过期优惠券
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function storeCouponsCenter($page, $pageSize, $userId, $search){
        $storeModel         = new Stores();
        $userCouponModel    = new UserCoupons();
        if (!empty($userId)){
            $userCoupon = DB::table($userCouponModel->getTable())
                ->where('users_id', $userId)
                ->where('is_use', 0)
                ->get();
            $userCouponId = [];
            foreach ($userCoupon as $item) {
                $userCouponId[] = $item->coupon_id;
            }
        }


        $sql = DB::table($this->table.' as sc')
            ->select(
                's.id as store_id',
                's.name as store_name',
                'sc.id as coupons_id',
                'sc.quantity',
                DB::raw("convert(full_amount/100, decimal(15,2)) as full_amount"),
                'discount_amount',
                'sc.type',
                'sc.create_time',
                'sc.term_of_validity',
                'sc.name as coupon_name',
                'sc.selling_price'
            )
            ->leftJoin($storeModel->getTable().' as s','sc.stores_id', '=', 's.id')
            ->where('quantity', '>', 0)
            ->whereNull('sc.delete_time');

        if (empty($search)){
            $sql->where('sc.belong', 2);
        }

        if (isset($userCouponId) && !empty($userCouponId)){
            $sql->whereNotIn('sc.id', $userCouponId);
        }

        if (isset($search['code'])){
            $sql->where('sc.code', $search['code'])
                ->where('sc.scope', '余额');
        }

        if (isset($search['scope'])){
            $sql->where('sc.scope', $search['scope']);
        }

        if (isset($search['belong'])){
            $sql->where('sc.belong', $search['belong']);
        }

        $total = $sql->count();
        $list  = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get()->toArray();




        foreach ($list as $value) {
            if ( !empty($userId) && in_array($value->coupons_id, $userCouponId)){
                $value->is_get = 1;
            }else{
                $value->is_get = 0;
            }

            if ($value->type == 0){
                $value->discount_amount = priceIntToFloat($value->discount_amount);
            }else{
                $value->discount_amount = ($value->discount_amount / 10).'折';
            }
        }

        return [
            'list'          => $list,
            'pagination'    => [
                'total'     => $total,
                'pageSize'  => $pageSize,
                'page'      => $page
            ]
        ];
    }


    /**
     * 根据优惠券ID获取优惠券信息
     * @param $couponId
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getCouponInfo($couponId){
        return DB::table($this->table)->where('id', $couponId)->first();
    }

    /**
     * 根据优惠券用途获取优惠券信息
     * @param $scope
     */
    public function getScopeCoupon($scope){
        return DB::table($this->table)->where('scope', $scope)
            ->select('id', 'name')
            ->whereNull('delete_time')
            ->get()
            ->toArray();
    }

    public function updateCoupon($data){
        return DB::table($this->table)->where('id', $data['id'])->update($data);
    }
    

}
