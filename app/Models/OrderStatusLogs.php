<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatusLogs extends Model{

    protected $table = 'order_status_logs';
    public $timestamps = false;
}
