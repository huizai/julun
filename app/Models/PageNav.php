<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PageNav extends Model{

    protected $table = 'page_nav';
    public $timestamps = false;
    protected $fillable = [
        'name',
        'url',
        'icon',
        'position'
    ];

    public function getPageConfig($position){
        return DB::table($this->table)
            ->select(
            'name as title', 'url', 'icon'
            )->where('position', $position)
                ->whereNull('delete_time')
                ->get();
    }
}
