<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AdminUserPermissions;
use DB;

class AdminPermissions extends Model{

    protected $table = 'admin_permissions';
    public $timestamps = false;

    /**
     * 角色权限信息
     * @param $id
     * @return mixed
     */
    public function rolesPermissions($id){
        return DB::table($this->table)->select('id','pid','name','rule')
            ->whereNull('deleted_at')
            ->whereIn('id',$id)
            ->get();
    }

    /**
     * 权限列表
     * @return mixed
     */
    public function permissionsList($name, $pid, $page, $pageSize){

        $sql = DB::table($this->table)->select('id','pid','name','created_at','updated_at')
            ->whereNull('deleted_at')
            ->where('pid',$pid);

        if (empty($nickname)){
            $sql->where('name','like','%'.$name.'%');
        }

        $total = $sql->count();

        $list = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();

        return [
            'list' => $list,
            'pagination' => [
                'current' => $page,
                'pageSize' => $pageSize,
                'total' => $total
            ]
        ];
    }

    public function permissionsTreeList(){
        return DB::table($this->table)->select('id','name as label','pid')->whereNull('deleted_at')->get();
    }

    /**
     * 权限路由ID
     * @param $id
     * @return mixed
     */
    public function permissionsUrl($id){
        return DB::table($this->table)->select('pid','rule','name')->where('id',$id)->first();
    }

    public function getPermissionsID(){
        return DB::table($this->table)->select('id')->get();
    }

    /**
     * 添加权限
     * @param $data
     * @return mixed
     */
    public function addPermissions($data){
        return DB::table($this->table)->insert($data);
    }

    /**
     * 修改权限信息
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updatePermissions($id, $data){
        return DB::table($this->table)->where('id',$id)->update($data);
    }

    /**
     * 删除权限
     * @param $id
     * @return mixed
     */
    public function deletePermissions($id){

        try{
            DB::beginTransaction();
            if (
                !DB::table($this->table)->where('id',$id)->update([
                    'deleted_at' => date('Y-m-d H:i:s',time())
                ])
            ){
                DB::rollBack();
                return false;
            }

            $userPermissionsModel = new AdminUserPermissions();
            if (
                !DB::table($userPermissionsModel->getTable())->where('pid',$id)->delete()
            ){
                DB::rollback();
                return false;
            }

            DB::commit();
            return false;

        }catch (\Exception $exception){
            \Log::error('deletePermissions:'.$exception);
            return false;
        }

    }

    /**
     * 权限无分页列表
     * @return mixed
     */
    public function PermissionsNoPageList(){
        return DB::table($this->table)->select('id','name')->get();
    }
}
