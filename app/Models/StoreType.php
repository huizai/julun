<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StoreType extends Model{

    protected $table = 'store_type';
    public $timestamps = false;

    public function getStoreType(){
        return DB::table($this->table)->get();
    }
}
