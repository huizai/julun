<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectMillGoods extends Model{

    protected $table = 'collect_mill_goods';
    public $timestamps = false;
}
