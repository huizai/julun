<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Message extends Model{

    protected $table = 'message';
    public $timestamps = false;

    public function messageList($storeId){
        return DB::table($this->table)->where('store_id', $storeId)
            ->where('is_read', 0)
            ->get();
    }

    public function messageRead($id){
        return DB::table($this->table)->whereIn('id', $id)->update(['is_read' => 1]);
    }
}
