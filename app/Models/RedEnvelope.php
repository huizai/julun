<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RedEnvelope extends Model{

    protected $table = 'red_envelope';
    public $timestamps = false;

    /**
     * 获取红包列表
     * @return \Illuminate\Support\Collection
     */
    public function getRedEnvelopeList(){
        return DB::table($this->table)
            ->select(
                'id',
                'title',
                DB::raw("convert(full_amount/100, decimal(15,2)) as full_amount"),
                DB::raw("convert(discount_amount/100, decimal(15,2)) as discount_amount"),
                'type',
                'effective_time'
            )
            ->whereNull('delete_time')->get();
    }

    /**
     * 添加或者修改红包
     * @param $id
     * @param $data
     * @return bool|int
     */
    public function redEnvelopeSave($id, $data){
        if (empty($id)){
            if ($data['type'] == 1){
                if(DB::table($this->table)
                    ->where('type', 1)
                    ->whereNull('delete_time')
                    ->first()){
                    return -1;
                }
            }
            return DB::table($this->table)->insert($data);
        }else{
            return DB::table($this->table)->where('id', $id)->update($data);
        }
    }

    /**
     * 删除红包
     * @param $id
     * @return int
     */
    public function redEnvelopeRemove($id){
        return DB::table($this->table)->where('id', $id)->update([
            'delete_time'       => date('Y-m-d H:i:s', time())
        ]);
    }
}
