<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreIncomeLog extends Model{

    protected $table = 'store_income_log';
    public $timestamps = false;
}
