<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectGoods extends Model{

    protected $table = 'collect_goods';
    public $timestamps = false;

    protected $fillable = ['users_id', 'goods_id'];
}
