<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUserMenu extends Model{

    protected $table = 'admin_user_menu';
    public $timestamps = false;
}
