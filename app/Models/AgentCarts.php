<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentCarts extends Model{

    protected $table = 'agent_carts';
    public $timestamps = false;
    protected $fillable = ['company_id', 'p_id', 'mill_id', 'agent_id', 'quantity', 'goods_id', 'sku_path', 'sku_value'];
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
}
