<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectMill extends Model{

    protected $table = 'collect_mill';
    public $timestamps = false;
}
