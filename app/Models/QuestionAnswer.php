<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class QuestionAnswer extends Model{

    protected $table = 'question_answer';
    public $timestamps = false;

    /**
     * 获取问答列表
     * @return \Illuminate\Support\Collection
     */
    public function getQuestionList(){
        return DB::table($this->table)
            ->whereNull('delete_time')
            ->get();
    }

    /**
     * 修改或者添加QA问答
     * @param $id
     * @param $data
     * @return bool|int
     */
    public function questionAnswerSave($id, $data){
        if (empty($id)){
            return DB::table($this->table)->insert($data);
        }else{
            return DB::table($this->table)->where('id', $id)->update($data);
        }
    }

    /**
     * 删除QA问答
     * @param $id
     * @return int
     */
    public function questionAnswerRemove($id)
    {
        return DB::table($this->table)->where('id', $id)->update([
            'delete_time'       => date('Y-m-d H:i:s', time())
        ]);
    }

    public function getAnswer($id){
        return DB::table($this->table)->where('id', $id)->value('answer');
    }

}
