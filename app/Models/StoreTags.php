<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreTags extends Model{

    protected $table = 'store_tags';
    public $timestamps = false;

    protected $fillable = [
        'stores_id',
        'tag_name'
    ];
}
