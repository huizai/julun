<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GoodsSkuName extends Model{

    protected $table = 'goods_sku_name';
    public $timestamps = false;


    protected $fillable = [
        'name',
        'goods_id',
    ];

    public function del($goodsId){
        $GoodsSkuName = new GoodsSkuName();

        return DB::table($GoodsSkuName->getTable())->where('goods_id',$goodsId)->delete();
    }
}
