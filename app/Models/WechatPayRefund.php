<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WechatPayRefund extends Model{

    protected $table = 'wechat_pay_refund';
    public $timestamps = false;
}
