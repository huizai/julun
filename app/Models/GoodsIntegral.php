<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsIntegral extends Model{

    protected $table = 'goods_integral';
    public $timestamps = false;
}
