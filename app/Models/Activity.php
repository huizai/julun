<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Activity extends Model{

    protected $table = 'activity';
    public $timestamps = false;


    protected $fillable = [
        'title'
    ];

    /**
     * 公益活动列表
     * @param $page
     * @param $pageSize
     * @param $title
     * @return array
     */
    public function activityList($page, $pageSize, $title){
        $sql = DB::table($this->table)
            ->whereNull('delete_time');

        if (!empty($title)){
            $sql->where('title', 'like', '%'.$title.'%');
        }

        $total = $sql->count();
        $list  = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();

        foreach ($list as $item) {
            $item->hour_time = explode('-', $item->hour_time);
        }
        return [
            'list'          => $list,
            'pagination'    => [
                'total'     => $total,
                'page'      => $page,
                'pageSize'  => $pageSize
            ]
        ];
    }


    public function addActivity($data){
        return DB::table($this->table)->insert($data);
    }


    public function updateActivity($data){
        return DB::table($this->table)->where('id', $data['id'])->update($data);
    }

    public function deleteActivity($id){
        return DB::table($this->table)->where('id', $id)->update([
            'delete_time'       => date('Y-m-d H:i:s', time())
        ]);
    }


}
