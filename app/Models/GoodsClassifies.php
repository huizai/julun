<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsClassifies extends Model{

    protected $table = 'goods_classifies';
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'name',
        'icon',
        'parent_id',
        'sort'
    ];
}
