<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParkingOrder extends Model{

    protected $table = 'parking_order';
    public $timestamps = false;
}
