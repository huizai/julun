<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNumberPlate extends Model{

    protected $table = 'user_number_plate';
    public $timestamps = false;
}
