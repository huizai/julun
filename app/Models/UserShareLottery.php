<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserShareLottery extends Model{

    protected $table = 'user_share_lottery';
    public $timestamps = false;
}
