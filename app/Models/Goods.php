<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Goods extends Model{

    protected $table = 'goods';
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'stores_id',
        'classify_id',
        'name',
        'slogan',
        'thumbnail',
        'selling_price',
        'purchase_price',
        'original_price',
        'sales_volume',
        'stock',
        'detail',
        'sort',
        'create_time',
    ];


    public function GoodsUpdate($data=[],$arr=[]){
        try {
            $Goods = new Goods();

            if (DB::table($Goods->getTable())->where('id','=',$arr['id'])->
                                                where('company_id','=',$arr['company_id'])->
                                                where('stores_id','=',$arr['stores_id'])->update($data) === false) {
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }


}
