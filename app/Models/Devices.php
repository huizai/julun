<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Devices extends Model{

    protected $table = 'devices';
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'name',
        'sort'
    ];
}
