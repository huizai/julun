<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreContractLog extends Model{

    protected $table = 'store_contract_log';
    public $timestamps = false;
}
