<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParkingRule extends Model{

    protected $table = 'parking_rule';
    public $timestamps = false;
}
