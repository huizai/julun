<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WechatPay extends Model{

    protected $table = 'wechat_pay';
    public $timestamps = false;
}
