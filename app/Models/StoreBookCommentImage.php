<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreBookCommentImage extends Model{

    protected $table = 'store_book_comment_image';
    public $timestamps = false;
}
