<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Stores extends Model{

    protected $table = 'stores';
    public $timestamps = false;

    public function saveStoreUser($data){
        return DB::table($this->table)->insertGetId($data);
    }


    /**
     * 商户详情
     * @param $id
     * @return object
     */
    public function adminUserInfo($id){
        $user = DB::table($this->table)->select(
            'id',
            'name',
            'logo',
            'status',
            'balance'
        )->where('id',$id)
            ->first();

        return $user;

    }
}

