<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlantingGrassCommentAwesome extends Model{

    protected $table = 'planting_grass_comment_awesome';
    public $timestamps = false;
}
