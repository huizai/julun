<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsEstimates extends Model{

    protected $table = 'goods_estimates';
    public $timestamps = false;
}
