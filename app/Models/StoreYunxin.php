<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StoreYunxin extends Model{

    protected $table = 'store_yunxin';
    public $timestamps = false;

    public function getStoreYunXinInfo($storeId){
        return DB::table($this->table)->where('stores_id', $storeId)
            ->first();
    }

    public function updateStoreYunXinInfo($store, $accId, $token){
        return DB::table($this->table)->where('stores_id',$store)
            ->update([
                'accid'         => $accId,
                'token'         => $token
            ]);
    }
}
