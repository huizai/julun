<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spike extends Model{

    protected $table = 'spike';
    public $timestamps = false;
}
