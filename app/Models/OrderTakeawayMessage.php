<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderTakeawayMessage extends Model{

    protected $table = 'order_takeaway_message';
    public $timestamps = false;

    public function saveMessage($data){
        DB::table($this->table)
            ->insert($data);
    }
}
