<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdminUserSidebar extends Model{

    protected $table = 'admin_user_sidebar';
    public $timestamps = false;

    public function getUserSidebar($id, $type){
        $sql = DB::table($this->table)->select('sidebar_id')->where('type', $type);

        if (isset($id)){
            $sql->where('user_id', $id);
        }

        return $sql->get();
    }

    /**
     * 修改用户菜单
     * @param $sidebarData
     * @param $type
     * @return bool
     */
    public function updateUserSidebar($sidebarData, $type){
        try{

            DB::beginTransaction();

            if (!DB::table($this->table)->where('type', $type)->delete()){
                DB::rollback();
                return false;
            }

            if(!DB::table($this->table)->insert($sidebarData)){
                DB::rollback();
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error($exception);
            return false;
        }
    }
}
