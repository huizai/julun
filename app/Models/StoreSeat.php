<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class StoreSeat extends Model{

    protected $table = 'store_seat';
    public $timestamps = false;
    protected $fillable = [
        'stores_id',
        'position',
        'seat_number',
        'min_capacity',
        'max_capacity',
        'capacity_range',
        'original_price',
        'selling_price',
        'period',
        'start_time',
        'ent_time'
    ];

    /**
     * 商户座位列表
     * @param $page
     * @param $pageSize
     * @param $storeId
     * @return array
     */
    public function seatList($page, $pageSize, $storeId){
        $sql = DB::table($this->table)
            ->select(
                'id',
                'stores_id',
                'position',
                'seat_number',
                'capacity_range',
                'min_capacity',
                'max_capacity',
                DB::raw("convert(original_price/100, decimal(15,2)) as original_price"),
                DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                'period',
                'start_time',
                'ent_time',
                'image'
            )
            ->where('stores_id', $storeId)
            ->whereNull('delete_time');

        $total = $sql->count();
        $list = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
                ->get();

        return [
            'list'  => $list,
            'pagination'    => [
                'page'      => $page,
                'pageSize'  => $pageSize,
                'total'     => $total
            ]
        ];
    }
}
