<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Turnover extends Model{

    protected $table = 'turnover';
    public $timestamps = false;
}
