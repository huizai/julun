<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;


class AdminUserRoles extends Model{

    protected $table = 'admin_user_roles';
    public $timestamps = false;

    public function userRoles($id){
        return DB::table($this->table)->where('uid',$id)->get();
    }
}
