<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserViewRecording extends Model{

    protected $table = 'user_view_recording';
    public $timestamps = false;
}
