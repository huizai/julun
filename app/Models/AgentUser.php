<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentUser extends Model{

    protected $table = 'agent_user';
    public $timestamps = false;

    protected $fillable = [
        'username',
        'password',
        'real_name',
        'mobile',
        'company',
        'company_id',
        'agent_type',
        'p_mobile',
        'avatar',
        'type'
    ];
}
