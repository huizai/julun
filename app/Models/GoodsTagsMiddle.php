<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsTagsMiddle extends Model{

    protected $table = 'goods_tags_middle';
    public $timestamps = false;
}
