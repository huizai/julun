<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ExpressDelivery extends Model{

    protected $table = 'express_delivery';
    public $timestamps = false;

    public function getExpressDelivery(){
        return DB::table($this->table)->select('name')->get();
    }
}
