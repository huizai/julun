<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class PlantingGrassClassify extends Model{

    protected $table = 'planting_grass_classify';
    public $timestamps = false;

    /**
     * 获取种草社区话题分类
     * @return \Illuminate\Support\Collection
     */
    public function getClassify(){
        return DB::table($this->table)
            ->select('id', 'name')
            ->whereNull('delete_time')
            ->get();
    }

    /**
     * 修改或者添加种草社区话题分类
     * @param $id
     * @param $name
     * @return bool|int
     */
    public function plantingClassifySave($id, $name){
        if (empty($id)){
            return DB::table($this->table)->insert(['name' => $name]);
        }else{
            return DB::table($this->table)->where('id', $id)->update(['name' => $name]);
        }
    }

    /**
     * 删除种草社区话题分类
     *
     * @param $id
     * @return int
     */
    public function plantingClassifyRemove($id){
        return DB::table($this->table)->where('id', $id)->update([
            'delete_time'  => date('Y-m-d H:i:s', time())
        ]);
    }
}
