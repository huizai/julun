<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlantingGrassCollection extends Model{

    protected $table = 'planting_grass_collection';
    public $timestamps = false;
}
