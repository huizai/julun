<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GoodsBargain extends Model{

    protected $table = 'goods_bargain';
    public $timestamps = false;

    public function getBargainList($page, $pageSize){
        return DB::table($this->table)
            ->where('total_sum', '>', 0)
            ->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();
    }
}
