<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlantingGrassAwesome extends Model{

    protected $table = 'planting_grass_awesome';
    public $timestamps = false;
}
