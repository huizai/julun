<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Users extends Model{

    protected $table = 'users';
    public $timestamps = false;

    protected $fillable = ['name', 'mobile', 'avater', 'password', 'create_time'];


    public function usesTotal($companyId){ //总人数//当天注册人数

        $userModel = new Users();
        $arr=array();
        $arr['total']=DB::table($userModel->getTable())
            ->where('company_id',$companyId)
            ->count();
        $arrs=DB::select("select count(*) as a  from `jl_users` where DATE_FORMAT(create_time,'%Y-%m-%d') = DATE_FORMAT(now(),'%Y-%m-%d') and company_id={$companyId}");
        $arr['currentTotal']=$arrs['0']->a;

        return $arr;
    }

    public function getNotLoginUser($day){
        return DB::table($this->table)
            ->select('id', 'mobile')
            ->whereRaw("TIMESTAMPDIFF(DAY,FROM_UNIXTIME(UNIX_TIMESTAMP(update_time) , '%Y-%m-%d'),'2019-10-01') >= {$day}")
            ->whereNull('delete_time')
            ->get();
    }

    public function findNum($str=''){
        $str=trim($str);
        if(empty($str)){return '';}
        $temp=array('1','2','3','4','5','6','7','8','9','0');
        $result='';
        for($i=0;$i<strlen($str);$i++){
            if(in_array($str[$i],$temp)){
                $result.=$str[$i];
            }
        }
        return $result;
    }

    /**
     * 获取用户名称
     * @param $id
     * @return mixed
     */
    public function getUserName($id){
        return DB::table($this->table)->where('id', $id)->value('name');
    }

    /**
     * 通过用户ID获取用户信息
     * @param $userId
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getUserInfo($userId){
        return DB::table($this->table)->where('id', $userId)->first();
    }

    /**
     * 升级会员
     * @param $userId
     * @return bool|int
     */
    public function userUpgrade($userId){
        $userInfo = DB::table($this->table)
            ->select('level', 'integral')
            ->where('id', $userId)
            ->first();

        if ($userInfo->level < 2 && $userInfo->integral >= 5000){
            return DB::table($this->table)
                ->where('id', $userId)
                ->update([
                    'level'     => 2,
                    'integral'  => DB::raw('integral + 200')
                ]);
        }

        if ($userInfo->level < 3 && $userInfo->integral >= 20000){
            return DB::table($this->table)
                ->where('id', $userId)
                ->update([
                    'level'     => 3,
                    'integral'  => DB::raw('integral + 500')
                ]);
        }

        return true;
    }

    /**
     * 设置用户VIP等级
     * @param $userId
     * @param $vip
     * @return int
     */
    public function settingUserVip($userId, $vip){
        return DB::table($this->table)
            ->where('id', $userId)
            ->update(['is_vip' => $vip]);
    }

}
