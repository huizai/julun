<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WechatSubIds extends Model{

    protected $table = 'wechat_sub_ids';
    public $timestamps = false;


    protected $fillable = [
        'save_id',
        'type',

    ];

}
