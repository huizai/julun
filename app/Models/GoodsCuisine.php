<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class GoodsCuisine extends Model{

    protected $table = 'goods_cuisine';
    public $timestamps = false;

    /**
     * 商品菜系列表
     * @param $page
     * @param $pageSize
     * @param $search
     * @return array
     */
    public function list($page, $pageSize, $search){
        $sql = DB::table($this->table);

        if (isset($search['name'])){
            $sql->where('name', 'like', '%'.$search['name'].'%');
        }

        $sql->whereNull('delete_time');

        $total  = $sql->count();
        $list   = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

        return [
            'list'          => $list,
            'pagination'    => [
                'total'     => $total,
                'page'      => $page,
                'pageSize'  => $pageSize
            ]
        ];
    }

    /**
     * 修改商品菜系
     * @param array $id
     * @param array $name
     * @return bool
     */
    public function cuisineUpdate($id, $name){
        return DB::table($this->table)
            ->where('id', $id)
            ->update([
                'name'  => $name
            ]);
    }

    /**
     * 删除商品菜系
     * @param $id
     * @return bool|null
     */
    public function cuisineDelete($id){
        return DB::table($this->table)
            ->where('id', $id)
            ->update([
                'delete_time'  => date('Y-m-d H:i:s', time())
            ]);
    }


    /**
     * 添加商品菜系
     * @param $name
     * @return mixed
     */
    public function cuisineAdd($name){
        return DB::table($this->table)->insert(['name' => $name]);
    }
}
