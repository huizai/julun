<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreNavs extends Model{

    protected $table = 'store_navs';
    public $timestamps = false;

    protected $fillable = [
        'stores_id',
        'sort',
        'name'
    ];
}
