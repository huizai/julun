<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GoodsGroupBuy extends Model{

    protected $table = 'goods_group_buy';
    public $timestamps = false;

    public function getGroupBuyList($page, $pageSize){
        return DB::table($this->table)
            ->where('total_sum', '>', 1)
            ->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();
    }
}
