<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreLeaseLog extends Model{

    protected $table = 'store_lease_log';
    public $timestamps = false;
}
