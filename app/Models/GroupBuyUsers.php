<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupBuyUsers extends Model{

    protected $table = 'group_buy_users';
    public $timestamps = false;
}
