<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreClassifity extends Model{

    protected $table = 'store_classifity';
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'type_id',
        'name',
        'icon',
        'parent_id',
        'sort'
    ];
}
