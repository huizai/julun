<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreBookOrder extends Model{

    protected $table = 'store_book_order';
    public $timestamps = false;
}
