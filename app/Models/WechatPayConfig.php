<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WechatPayConfig extends Model{

    protected $table = 'wechat_pay_config';
    public $timestamps = false;


    private function __weChatsql(){
        $wechat_pay_config = new WechatPayConfig();
       $sql= DB::table($wechat_pay_config->getTable())
            ->select(
                'id',
                        'mch_id',
                        'mch_key',
                        'cert_path',
                        'key_path',
                        'appid',
                        'update_time'
                    );
            return $sql;
    }
    public function weChat_confInfo($companyId){
        $sql = $this->__weChatsql();
        $info = $sql->where('company_id','=',$companyId)->get();
        return $info;
    }
}
