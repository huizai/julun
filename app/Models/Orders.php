<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Orders extends Model{

    protected $table = 'orders';
    public $timestamps = false;

    public function orderList(array $search = [], int $page = 1, int $pageSzie = 20){
        $Good = new Goods();
        $User = new Users();
        $OrderGoods = new OrderGoods();
        $OrderAddress = new OrderAddress();
        $districtModel = new District();
        $storeModel     = new Stores();
        $sql = DB::table($this->getTable() . ' as o')
            ->select(
                'o.*',
                'u.name',
                'u.mobile',
                'u.avatar',
                'od.contact',
                'pd.name as province_name',
                'cd.name as city_name',
                'dd.name as district_name',
                'od.address',
                's.name as store_name'
            )
            ->leftJoin($User->getTable() . ' as u', 'o.users_id', '=', 'u.id')
            ->leftJoin($OrderAddress->getTable() . ' as od', 'od.batch', '=', 'o.batch')
            ->leftJoin($districtModel->getTable() . ' as pd', 'pd.id', '=', 'od.province')
            ->leftJoin($districtModel->getTable() . ' as cd', 'cd.id', '=', 'od.city')
            ->leftJoin($districtModel->getTable() . ' as dd', 'dd.id', '=', 'od.district')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'o.stores_id')
            ->whereNull('o.delete_time');
        if(isset($search['order_sn'])){
            $sql->where('o.order_sn', 'like', "%".$search['order_sn']."%");
        }

        if(isset($search['company'])){
            $sql->where('o.company_id', $search['company']);
        }
        if(isset($search['store_id']) && $search['store_id'] != 0){
            $sql->where('o.stores_id', $search['store_id']);
        }
        if (isset($search['search_store_id'])){
            $sql->where('o.stores_id', $search['search_store_id']);
        }
        if (isset($search['status']) && $search['status'] != null){
            $sql->where('o.status', $search['status']);
        }
        if (isset($search['online']) && $search['online'] == 1){
            $sql->where('o.is_online', 1);
        }else if (isset($search['online']) && $search['online'] == 0){
            $sql->where('o.is_online', 0);
        }
        if (isset($search['time']) && !empty($search['time'])){
            $sql->whereBetween('o.create_time', $search['time']);
        }
        if (isset($search['pay_type'])){
            $sql->where('o.pay_type', $search['pay_type']);
        }
        $total = $sql->count();
        $list = $sql->skip(($page - 1) * $pageSzie)
            ->take($pageSzie)
            ->orderBy('o.create_time', 'desc')
            ->get()->toArray();
        foreach ($list as $k=>$v){
            $v->goods_price = priceIntToFloat($v->goods_price);
            $v->pay_amount = priceIntToFloat($v->pay_amount);
            $v->discount_amount = priceIntToFloat($v->discount_amount);
            $v->goods_data = DB::table($OrderGoods->getTable() . ' as og')
                ->select(
                'g.name as title',
                        'g.thumbnail',
                        'og.quantity',
                        'og.sku'
                    )
                ->leftJoin($Good->getTable() . ' as g', 'g.id', '=', 'og.goods_id')
                ->where('og.orders_id',$v->id)->get();
            $v->goods_sum = 0;
            foreach ($v->goods_data as $item) {
                $v->goods_sum += $item->quantity;
            }
        }


        return [
            'list'              => $list,
            'pagination'        => [
                'total'         => $total,
                'pageSize'      => $pageSzie,
                'current'       => $page
            ]
        ];
    }

    public function orderListPagination(array $search, int $page = 1, int $pageSize = 20){
        $User = new Users();
        $OrderAddress = new OrderAddress();
        $sql = DB::table($this->getTable() . ' as o')
            ->select(
                'o.*',
                'u.name',
                'u.mobile',
                'od.province',
                'od.city',
                'od.district',
                'od.contact'
            )
            ->leftJoin($User->getTable() . ' as u', 'o.users_id', '=', 'u.id')
            ->leftJoin($OrderAddress->getTable() . ' as od', 'od.batch', '=', 'o.batch')
            ->whereNull('o.delete_time');
        if(isset($search['order_sn'])){
            $sql->where('o.order_sn', 'like', "%".$search['order_sn']."%");
        }

        if(isset($search['company'])){
            $sql->where('o.company_id', $search['company']);
        }
        if(isset($search['store_id'])){
            $sql->where('o.stores_id', $search['store_id']);
        }
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }



    public function ordersTotal($companyId,$storeId=null,$status=null){  //总订单//当天订单   1/今日 2/昨日 3/最近七日 4/本月
        $ordersModel = new Orders();
        $arr=array();
        $arrs=array();
       if(empty($storeId)){
           if(empty($status)){
               $arrs=DB::select("select count(*) as a,sum(pay_amount) as price,avg(pay_amount) as priceAvg from jl_orders where DATE_FORMAT(create_time,'%Y-%m-%d')=DATE_FORMAT(now(),'%Y-%m-%d') and company_id={$companyId} ");

           }else{
               if($status=='1'){ // 1/今日
                   $arrs=DB::select("select count(*) as a,sum(pay_amount) as price,avg(pay_amount) as priceAvg from jl_orders where DATE_FORMAT(create_time,'%Y-%m-%d')=DATE_FORMAT(now(),'%Y-%m-%d') and company_id={$companyId} ");

               }elseif ($status=='2'){  //2/昨日
                   $arrs=DB::select(" SELECT count(*) as a,sum(pay_amount) as price,avg(pay_amount) as priceAvg FROM `jl_orders` WHERE TO_DAYS(NOW( )) - TO_DAYS(create_time) <= 1 and company_id={$companyId} ");

               }elseif ($status=='3'){  //3/最近七日
                   $arrs=DB::select(" SELECT count(*) as a,sum(pay_amount) as price,avg(pay_amount) as priceAvg FROM  `jl_orders` WHERE YEARWEEK( date_format(  create_time,'%Y-%m-%d' ) ) = YEARWEEK( now() ) and company_id={$companyId} ");

               }elseif ($status=='4'){ //3/本月
                   $arrs=DB::select(" SELECT count(*) as a,sum(pay_amount) as price,avg(pay_amount) as priceAvg FROM  jl_orders WHERE DATE_FORMAT( create_time, '%Y%m' ) = DATE_FORMAT( CURDATE( ) ,'%Y%m' ) and company_id={$companyId} ");

               }else{
                   return false;
               }

           }

           $arr['currentTotal']=$arrs['0']->a;
           $arr['price']=empty($arrs['0']->price)?0:$arrs['0']->price;
           $arr['priceAvg']=empty($arrs['0']->priceAvg)?0:$arrs['0']->priceAvg;
       }else{

           if(empty($status)){
               $arrs=DB::select("select count(*) as a,sum(pay_amount) as price,avg(pay_amount) as priceAvg from jl_orders where DATE_FORMAT(create_time,'%Y-%m-%d')=DATE_FORMAT(now(),'%Y-%m-%d') and company_id={$companyId} and stores_id={$storeId}");

           }else{
                    if($status=='1'){ // 1/今日
                        $arrs=DB::select("select count(*) as a,sum(pay_amount) as price,avg(pay_amount) as priceAvg from jl_orders where DATE_FORMAT(create_time,'%Y-%m-%d')=DATE_FORMAT(now(),'%Y-%m-%d') and company_id={$companyId} and stores_id={$storeId}");

                    }elseif ($status=='2'){  //2/昨日
                        $arrs=DB::select(" SELECT count(*) as a,sum(pay_amount) as price,avg(pay_amount) as priceAvg FROM `jl_orders` WHERE TO_DAYS(NOW( )) - TO_DAYS(create_time) <= 1 and company_id={$companyId} and stores_id={$storeId}");

                    }elseif ($status=='3'){  //3/最近七日
                        $arrs=DB::select(" SELECT count(*) as a,sum(pay_amount) as price,avg(pay_amount) as priceAvg FROM  `jl_orders` WHERE YEARWEEK( date_format(  create_time,'%Y-%m-%d' ) ) = YEARWEEK( now() ) and company_id={$companyId} and stores_id={$storeId}");

                    }elseif ($status=='4'){ //3/本月
                        $arrs=DB::select(" SELECT count(*) as a,sum(pay_amount) as price,avg(pay_amount) as priceAvg FROM  jl_orders WHERE DATE_FORMAT( create_time, '%Y%m' ) = DATE_FORMAT( CURDATE( ) ,'%Y%m' ) and company_id={$companyId} and stores_id={$storeId}");

                    }else{
                        return false;
                    }

           }

           $arr['currentTotal']=$arrs['0']->a;
           $arr['price']=empty($arrs['0']->price)?0:$arrs['0']->price;
           $arr['priceAvg']=empty($arrs['0']->priceAvg)?0:$arrs['0']->priceAvg;



       }
        return $arr;
    }






    public function ordersStatus($companyId,$storeId){  //状态；1待发货，4售后
        $ordersModel = new Orders();
        $arr=array();
        $arr['待发货']=DB::table($ordersModel->getTable())
            ->where('status','1')
            ->where('company_id',$companyId)
            ->where('stores_id',$storeId)
            ->count();
        $arr['售后']=DB::table($ordersModel->getTable())
            ->where('status','2')
            ->where('company_id',$companyId)
            ->where('stores_id',$storeId)
            ->count();
        return $arr;
    }


    /**
     * 获取订单剩余支付时间
     * @param $batch
     * @return int
     */
    public function orderPatTime($batch, $orderId){
        $sql    = DB::table($this->table);
        if (!empty($batch)){
            $sql->where('batch', $batch);
        }else{
            $sql->where('id', $orderId);
        }
        $time   = $sql->where('status', 0)
            ->whereNull('delete_time')
            ->value('create_time');

        if (empty($time)){
            return -1;
        }

        return naturalNumber(strtotime($time) + 900 - time());
    }


    /**
     *
     * @param $id
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function orderInfo($id){
        $edModel        = new ExpressDelivery();
        return DB::table($this->getTable().' as o')
            ->select('o.*', 'ed.abbreviation')
            ->leftJoin($edModel->getTable().' as ed', 'o.express', '=', 'ed.name')
            ->where('o.id', $id)
            ->first();
    }

    /**
     * 通过batch订单批次号获取订单详情
     * @param $batch
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function orderInfoByBatch($batch){
        return DB::table($this->table)
            ->where('batch', $batch)
            ->first();
    }
}
