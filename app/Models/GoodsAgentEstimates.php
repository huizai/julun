<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsAgentEstimates extends Model{

    protected $table = 'goods_agent_estimates';
    public $timestamps = false;
}
