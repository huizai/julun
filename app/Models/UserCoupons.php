<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserCoupons extends Model{

    protected $table = 'user_coupons';
    public $timestamps = false;

    public function getUserCoupon($couponId, $userId){
        return DB::table($this->table)->where('coupon_id', $couponId)
            ->where('users_id', $userId)
            ->where('is_use', 0)
            ->first();
    }

    /**
     * 获取优惠金额最高的停车场优惠券
     * @param $userId
     * @param $amount
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getUserParkingCoupon($userId, $amount, $couponId = null){
        $storeCouponModel       = new StoreCoupons();

        $time = date('Y-m-d H:i:s', time());
        $sql = DB::table($this->table.' as uc')
            ->select('uc.id', 'sc.*')
            ->leftJoin($storeCouponModel->getTable().' as sc', 'sc.id', '=', 'uc.coupon_id')
            ->where('sc.scope', '停车场')
            ->where('uc.users_id', $userId)
            ->where('uc.start_time', '<=', $time)
            ->where('uc.end_time', '>=', $time)
            ->where('uc.is_use', 0)
            ->whereNull('uc.delete_time')
            ->orderByDesc('sc.discount_amount');

        if (!empty($couponId)){
            $sql->where('uc.coupon_id', $couponId);
        }

        if ($amount > 0){
            $sql->where('sc.full_amount', '>=', $amount);
        }

        return $sql->first();
    }

    public function getUserCouponInfo($couponId, $userId){
        $storeCouponModel       = new StoreCoupons();
        $storeModel             = new Stores();
        return DB::table($this->table.' as uc')
            ->select(
                'sc.name as coupon_name',
                DB::raw("convert(discount_amount/100, decimal(15,2)) as discount_amount"),
                'uc.code',
                'uc.qr_code',
                'sc.address',
                's.name as store_name',
                'sc.rule'
            )
            ->leftJoin($storeCouponModel->getTable().' as sc', 'sc.id', '=', 'uc.coupon_id')
            ->leftJoin($storeModel->getTable().' as s','s.id','=','sc.stores_id')
            ->where('uc.id', $couponId)
            ->where('uc.users_id', $userId)
            ->where('uc.is_use', 0)
            ->first();
    }

    /**
     * 获取用户已使用的优惠券
     * @param $search
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getUserUseCoupon($search, $page, $pageSize){
        $userModel          = new Users();
        $couponModel        = new StoreCoupons();
        $userCouponModel    = new UserCoupons();
        $storeModel         = new Stores();

        $sql = DB::table($userCouponModel->getTable().' as uc')
            ->select(
                'u.name as user_name',
                'u.mobile',
                'sc.name as coupon_name',
                'uc.update_time',
                's.name as store_name',
                'uc.code',
                'uc.end_time',
                'uc.id'
            )
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'uc.users_id')
            ->leftJoin($couponModel->getTable().' as sc', 'sc.id', '=', 'uc.coupon_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'sc.stores_id')
            ->where('sc.belong', 1)
            ->where('uc.is_check', $search['is_check'])
            ->where('uc.is_use', $search['is_use'])
            ->whereNull('uc.delete_time');

        if (!empty($search['stores_id']) && $search['stores_id'] != 0){
            $sql->where('sc.stores_id', $search['stores_id']);
        }

        if (!empty($search['mobile'])){
            $sql->where('u.mobile', $search['mobile']);
        }

        $total = $sql->count();
        $list  = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

        return [
            'list'              => $list,
            'pagination'        => [
                'total'         => $total,
                'current'       => $page,
                'pageSize'      => $pageSize
            ]
        ];
    }

    /**
     * 获取当前店铺用户未使用的线下优惠券
     * @param $search
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getUserKindCoupon($search, $page, $pageSize){
        $storeCouponModel       = new StoreCoupons();
        $userModel              = new Users();
        $sql = DB::table($this->table.' as uc')
            ->select(
                'u.mobile',
                'u.name as user_name',
                'sc.name as coupon_name',
                'uc.code',
                'uc.end_time',
                'uc.start_time',
                'uc.id'
            )
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'uc.users_id')
            ->leftJoin($storeCouponModel->getTable().' as sc', 'sc.id', '=', 'uc.coupon_id')
            ->where('sc.virtual', 2)
            ->where('sc.stores_id', $search['stores_id'])
            ->where('uc.is_use', 0)
            ->orderBy('uc.create_time', 'desc');

        if (!empty($search['code'])){
            $sql->where('uc.code', $search['code']);
        }

        if (!empty($search['mobile'])){
            $sql->where('u.mobile', $search['mobile']);
        }

        $total = $sql->count();
        $list  = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

        return [
            'list'          => $list,
            'pagination'    => [
                'total'     => $total,
                'current'   => $page,
                'pageSize'  => $pageSize
            ]
        ];
    }

    /**
     * 兑换线下优惠券
     * @param $id
     * @return int
     */
    public function userUserKindCoupon($id, $code){
        $sql = DB::table($this->table);
        if (!empty($id)){
            $sql->where('id', $id);
        }
        if (!empty($code)){
            $sql->where('code', $code);
        }
        $coupon = $sql->first();

        if (!$coupon){
            return -1;
        }

        if ($coupon->end_time < date('Y-m-d H:i:s', time())){
            return -2;
        }

        if ($coupon->is_use == 1){
            return -3;
        }

        return DB::table($this->table)
            ->where('id', $id)
            ->update(['is_use' => 1]);
    }

    /**
     * 核销优惠券
     * @param $id
     * @return array|int
     */
    public function checkUserCoupon($id){
        if (is_array($id)){
            $coupon = DB::table($this->table)
                ->whereIn('id', $id)
                ->get()->toArray();

            if (empty($coupon)){
                return [
                    'code'      => 'field',
                    'message'   => '未找到该优惠券'
                ];
            }
            foreach ($coupon as $item) {
                if ($item->is_use == 0){
                    return [
                        'code'      => 'field',
                        'message'   => '请使用该优惠券后再进行核销'
                    ];
                }
            }

            $result = DB::table($this->table)
                ->where('id', $id)
                ->update(['is_check' => 1]);
        }else{

            $coupon = DB::table($this->table)
                ->where('id', $id)
                ->first();
            if (!$coupon){
                return [
                    'code'      => 'field',
                    'message'   => '未找到该优惠券'
                ];
            }

            if ($coupon->is_use == 0){
                return [
                    'code'      => 'field',
                    'message'   => '请使用该优惠券后再进行核销'
                ];
            }

            $result = DB::table($this->table)
                ->whereIn('id', $id)
                ->update(['is_check' => 1]);
        }




        if (is_array($id)){

        }else{

        }


        if ($result){
            return [
                'code'      => 'success'
            ];
        }

        return [
            'code'      => 'field',
            'message'   => '未知错误'
        ];
    }
}
