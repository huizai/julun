<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FreightCompany extends Model{

    protected $table = 'freight_company';
    public $timestamps = false;
}
