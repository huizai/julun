<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreBookComment extends Model{

    protected $table = 'store_book_comment';
    public $timestamps = false;
}
