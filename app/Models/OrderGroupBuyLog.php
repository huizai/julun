<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderGroupBuyLog extends Model{

    protected $table = 'order_group_buy_log';
    public $timestamps = false;
}
