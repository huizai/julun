<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreAddress extends Model{

    protected $table = 'store_address';
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'store_id',
        'consignee',
        'contact',
        'province',
        'city',
        'district',
        'address',
        'longitude',
        'latitude',
        'default',
        'create_time'
    ];
}
