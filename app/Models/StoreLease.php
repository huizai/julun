<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StoreLease extends Model{

    protected $table = 'store_lease';
    public $timestamps = false;

    public function saveStoreLease($data){
        return DB::table($this->table)->insert($data);
    }
}
