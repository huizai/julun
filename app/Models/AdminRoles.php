<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;


class AdminRoles extends Model{

    protected $table = 'admin_roles';
    public $timestamps = false;

    /**
     * 角色列表
     * @return mixed
     */
    public function adminRolesList($page, $pageSize, $name){
        $sql = DB::table($this->table)->select('id','name','created_at')
            ->whereNull('deleted_at');

        if (empty($nickname)){
            $sql->where('name','like','%'.$name.'%');
        }

        $total = $sql->count();

        $list = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();

        return [
            'list' => $list,
            'pagination' => [
                'current' => $page,
                'pageSize' => $pageSize,
                'total' => $total
            ]
        ];
    }

    /**
     * 获取角色权限ID
     * @param $id
     * @return array
     */
    public function getRolesPermissions($id){
        return  DB::table($this->table)->select('permission')->whereIn('id',$id)->get();
    }

    public function rolesPermissions($id){
        return  DB::table($this->table)->select('permission')->whereIn('id',$id)->get();
    }

    /**
     * 添加角色
     * @param $data
     * @return mixed
     */
    public function addRoles($data){
        return DB::table($this->table)->insert($data);
    }

    /**
     * 修改角色信息
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateRoles($id, $data){
        return DB::table($this->table)->where('id',$id)->update($data);
    }

    /**
     * 删除角色
     * @param $id
     * @return mixed
     */
    public function deleteRoles($id){
        return DB::table($this->table)->where('id',$id)->update([
            'deleted_at' => date('Y-m-d H:i:s',time())
        ]);
    }
}
