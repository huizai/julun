<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserRedEnvelope extends Model{

    protected $table = 'user_red_envelope';
    public $timestamps = false;

    public function getUserRedEnvelope($page, $pageSize, $userId){
        $redEnvelopeModel       = new RedEnvelope();
        $sql = DB::table($this->getTable().' as ure')
            ->select(
                'ure.start_time',
                'ure.end_time',
                're.id',
                're.title',
                DB::raw("convert(jl_re.full_amount/100, decimal(15,2)) as full_amount"),
                DB::raw("convert(jl_re.discount_amount/100, decimal(15,2)) as discount_amount")
            )
            ->leftJoin($redEnvelopeModel->getTable().' as re', 're.id','=','ure.red_envelope_id')
            ->where('ure.users_id', $userId)
            ->where('ure.is_use', 0)
            ->whereNull('re.delete_time');

        $total = $sql->count();
        $list  = $sql->orderBy('end_time', 'asc')
            ->skip(($page -1) * $pageSize)->take($pageSize)
            ->get();

        return [
            'list'          => $list,
            'pagination'    => [
                'total'     => $total,
                'current'   => $page,
                'pageSize'  => $pageSize
            ]
        ];
    }
}
