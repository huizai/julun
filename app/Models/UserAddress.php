<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model{

    protected $table = 'user_address';
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'users_id',
        'consignee',
        'contact',
        'province',
        'city',
        'district',
        'address',
        'longitude',
        'latitude',
        'create_time'
    ];
}
