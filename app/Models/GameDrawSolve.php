<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GameDrawSolve extends Model{

    protected $table = 'game_draw_solve';
    public $timestamps = false;

    public function getGameDrawSolve($id){
        return DB::table($this->table)
            ->where('game_id', $id)
            ->whereNull('delete_time')
            ->get();
    }

    public function saveGameDrawSolve($id, $gameId, $content){
        if (empty($id)){
            return DB::table($this->table)->insert(['content' => $content, 'game_id' => $gameId]);
        }else{
            return DB::table($this->table)->where('id', $id)->update(['content' => $content]);
        }
    }

    public function removeGameDrawSolve($id){
        return DB::table($this->table)->where('id', $id)->update([
            'delete_time'       => date('Y-m-d H:i:s', time())
        ]);
    }

    public function getRandomDrawSolve($id){
        $sql = "select * from `jl_game_draw_solve` where game_id = {$id} and delete_time is null order by rand( ) limit 1";
        return DB::select($sql);
    }
}
