<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentTurnoverGoods extends Model{

    protected $table = 'agent_turnover_goods';
    public $timestamps = false;
}
