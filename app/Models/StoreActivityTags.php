<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class StoreActivityTags extends Model{

    protected $table = 'store_activity_tags';
    public $timestamps = false;

    public function getStoreActivityTagsList($stores_id){
        return DB::table($this->table)->where('stores_id', $stores_id)->get();
    }

    public function addStoresActivityTags($data){
        return DB::table($this->table)->insert($data);
    }

    public function updateStoreActivityTags($data){
        return DB::table($this->table)->where('id', $data['id'])->update($data);
    }

    public function deleteStoreActivityTags($id){
        return DB::table($this->table)->where('id', $id)->delete();
    }
}
