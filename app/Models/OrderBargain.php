<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderBargain extends Model{

    protected $table = 'order_bargain';
    public $timestamps = false;
}
