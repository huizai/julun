<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsDevices extends Model{

    protected $table = 'goods_devices';
    public $timestamps = false;
}
