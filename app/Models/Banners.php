<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model{

    protected $table = 'banners';
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'images',
        'url',
        'sort',
        'position',
        'uptime',
        'downtime',
        'update_time',
    ];
}
