<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alipay extends Model{

    protected $table = 'alipay';
    public $timestamps = false;
}
