<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GameDraw extends Model{

    protected $table = 'game_draw';
    public $timestamps = false;

    public function getGameDraw(){
        return DB::table($this->table)
            ->whereNull('delete_time')
            ->get();
    }

    public function saveGameDraw($id, $content){
        if (empty($id)){
            return DB::table($this->table)->insert(['content' => $content]);
        }else{
            return DB::table($this->table)->where('id', $id)->update(['content' => $content]);
        }
    }

    public function removeGameDraw($id){
        return DB::table($this->table)->where('id', $id)
            ->update(['delete_time' => date('Y-m-d H:i:s', time())]);
    }

    public function getRandomDraw(){
        $sql = "select * from jl_game_draw where delete_time is null order by rand( ) limit 1";
        return DB::select($sql);
    }
}
