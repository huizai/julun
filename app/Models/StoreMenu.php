<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreMenu extends Model{

    protected $table = 'store_menu';
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'store_id',
        'title',
        'price',
        'image',
    ];
}
