<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlantingGrassGoods extends Model{

    protected $table = 'planting_grass_goods';
    public $timestamps = false;
}
