<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupBuy extends Model{

    protected $table = 'group_buy';
    public $timestamps = false;
}
