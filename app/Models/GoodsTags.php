<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsTags extends Model{

    protected $table = 'goods_tags';
    public $timestamps = false;
    protected $fillable = [
        'stores_id',
        'mr_name',
        'tag_name'
    ];
}
