<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreOfflineCoupons extends Model{

    protected $table = 'store_offline_coupons';
    public $timestamps = false;
}
