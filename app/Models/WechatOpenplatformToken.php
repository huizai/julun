<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WechatOpenplatformToken extends Model{

    protected $table = 'wechat_openplatform_token';
    public $timestamps = false;

    protected $fillable = [
        'appid','refresh_token'
    ];
}
