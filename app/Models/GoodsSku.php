<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GoodsSku extends Model{

    protected $table = 'goods_sku';
    public $timestamps = false;

    protected $fillable = [
        'value_path',
        'goods_id',
        'purchase_price',
        'original_price',
        'selling_price',
        'stock',
    ];

    public function del($goodsId){
        $GoodsSku = new GoodsSku();
        return DB::table($GoodsSku->getTable())->where('goods_id',$goodsId)->delete();

    }
}
