<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Users;

class UserRechargeLog extends Model{

    protected $table = 'user_recharge_log';
    public $timestamps = false;

    /**
     * 用户充值记录
     * @param $page
     * @param $pageSize
     * @param $search
     * @return \Illuminate\Database\Query\Builder
     */
    public function __listSql($search){
        $userModel      = new Users();
        $prefix = env('DB_PREFIX');

        $sql = DB::table($this->getTable().' as ur')
            ->select(
                'ur.id',
                'ur.create_time',
                DB::raw("convert({$prefix}ur.total_fee/100, decimal(15,2)) as total_fee"),
                'ur.remake',
                DB::raw("if({$prefix}ur.pay_type= 1, '微信支付', '支付宝支付') as pay_type"),
                'u.name',
                'u.mobile'
            )
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'ur.users_id')
            ->where('ur.status', 1)
            ->orderBy('ur.create_time', 'desc');


        if (isset($search['time'])){
            $sql->whereBetween('ur.create_time', $search['time']);
        }

        if (isset($search['pay_type']) && !empty($search['pay_type'])){
            $sql->where('ur.pay_type', $search['pay_type']);
        }

        if (isset($search['mobile']) && !empty($search['mobile'])){
            $sql->where('u.mobile', $search['mobile']);
        }
        return $sql;
    }

    public function list($page, $pageSize, $search){
        $model=new Users();
        $sql = $this->__listSql($search);
        $total              = $sql->count();
        $data               = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();
//        $payType            = array();
//        $payType[1]         = '微信支付';
//        $payType[2]         = '支付宝支付';


        return [
            'data'          => $data,
            'pagination'    => [
                'page'      => $page,
                'pageSize'  => $pageSize,
                'total'     => $total
            ]
        ];
    }

    public function getUserRechargeCountMoney($time, $payType){
        \Log::debug($time);
        $totalFeeSql = "select SUM(total_fee) as total_fee from jl_user_recharge_log where status = 1";

        $totalUserSql = "select  COUNT(DISTINCT (users_id)) as users_id from  jl_user_recharge_log where status = 1";


        if (!empty($time)){
            $totalFeeSql .= " and create_time >= '{$time[0]}"." 00:00:00' and create_time <= '{$time[1]}"." 00:00:00'";
            $totalUserSql .= " and create_time >= '{$time[0]}"." 00:00:00' and create_time <= '{$time[1]}"." 00:00:00'";
        }

        if (!empty($payType)){
            $totalFeeSql .= " and pay_type = {$payType}";
            $totalUserSql .= " and pay_type = {$payType}";
        }

        $totalFee = DB::selectOne($totalFeeSql);
        $totalUser = DB::selectOne($totalUserSql);


        return ['totalFee' => priceIntToFloat($totalFee->total_fee), 'totalUser' => $totalUser->users_id];
    }

//    public function exportExcelData($time){
//        return $this->__listSql(['time' => $time])->get();
//    }

    //商户列表新
    public function exportExcelData(array $search){

        $model=new Users();
        $userModel      = new Users();
        $prefix = env('DB_PREFIX');
        $sql = DB::table($this->getTable().' as ur')
            ->select(
                'ur.id',
                'ur.create_time',
                DB::raw("convert({$prefix}ur.total_fee/100, decimal(15,2)) as total_fee"),
                'ur.remake',
                DB::raw("if({$prefix}ur.pay_type= 1, '微信支付', '支付宝支付') as pay_type"),
                'u.name',
                'u.mobile'
            )
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'ur.users_id')
            ->where('ur.status', 1)
            ->orderBy('ur.create_time', 'desc');

        if (isset($search['time'])&&!empty($search['time'])){
            $sql->whereBetween('ur.create_time', $search['time']);
        }

        if (isset($search['pay_type']) && !empty($search['pay_type'])){
            $sql->where('ur.pay_type', $search['pay_type']);
        }
        if (isset($search['mobile']) && !empty($search['mobile'])){
            $sql->where('u.mobile', $search['mobile']);
        }
        $data               = $sql->get()->toArray();
       // print_r($data);die;
        if(!empty($data)){
            foreach ($data as$k=>&$v) {
                $data[$k]->remake=intval($model->findNum($v->remake));
                $data[$k]->remake=sprintf("%.2f", $data[$k]->remake/100);
                //$data[$k]->remake    =  sprintf("%.2f", $model->findNum($v->remake)/100);
            }
        }

        return $data;

    }

}

