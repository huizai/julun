<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserExchangeGoods extends Model{

    protected $table = 'user_exchange_goods';
    public $timestamps = false;
}
