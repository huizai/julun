<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RechargeActivity extends Model{

    protected $table = 'recharge_activity';
    public $timestamps = false;

    public function list(){
        return DB::table($this->table)
            ->select(
                'id',
                DB::raw("convert(`condition`/100, decimal(15,2)) as `condition`"),
                DB::raw("convert(send/100, decimal(15,2)) as send"),
                'start_time',
                'end_time',
                'create_time',
                'update_time'
            )
            ->get();
    }

    public function modification($data){
       return DB::table($this->table)->where('id', $data['id'])->update($data);
    }

    public function addition($data){
        return DB::table($this->table)->insert($data);
    }

    public function remove($id){
        return DB::table($this->table)->where('id', $id)->delete();
    }
}
