<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class UserMessage extends Model{

    protected $table = 'user_message';
    public $timestamps = false;

    public function getUserMessage($userId){
        return DB::table($this->table)
            ->select('content', 'create_time')
            ->where('users_id', $userId)
            ->get();
    }

    public function saveUserMessage($data){
        DB::table($this->table)->insert($data);
    }
}
