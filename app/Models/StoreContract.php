<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreContract extends Model{

    protected $table = 'store_contract';
    public $timestamps = false;
}
