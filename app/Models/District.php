<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class District extends Model{

    protected $table = 'district';
    public $timestamps = false;

    public function saveData($data){
        return \DB::table($this->table)->insertGetId($data);
    }

    public function getAddress($province = '', $city = '', $district = ''){
        $sql = DB::table($this->table)
            ->select('name');

        if (!empty($province)){
            $sql->orWhere('id', $province);
        }

        if (!empty($city)){
            $sql->orWhere('id', $city);
        }

        if (!empty($district)){
            $sql->orWhere('id', $district);
        }



        return $sql->get();
    }
}
