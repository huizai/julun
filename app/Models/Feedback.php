<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Feedback extends Model{

    protected $table = 'feedback';
    public $timestamps = false;


    protected $fillable = [
        'content',
        'contact',
        'contact_method',
        'images'
    ];

    public function getUserFeedBack($page, $pageSize){
        $sql = DB::table($this->table);

        $total = $sql->count();
        $list = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

        return [
            'list'          => $list,
            'pagination'    => [
                'total'     => $total,
                'current'   => $page,
                'pageSize'  => $pageSize
            ]
        ];

    }
}
