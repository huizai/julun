<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Version extends Model{

    protected $table = 'version';
    public $timestamps = false;

    /**
     * 修改版本信息
     * @return mixed
     */
    public function getVersionInfo(){
        return DB::table($this->table)->get();
    }

    /**
     * 修改版本信息
     * @param $data
     * @param $id
     * @return mixed
     */
    public function saveVersion($data, $id){
        if (!empty($id)){
            return DB::table($this->table)->where('id', $id)->update($data);
        }

        return DB::table($this->table)->insert($data);

    }
}
