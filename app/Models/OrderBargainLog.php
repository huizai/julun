<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderBargainLog extends Model{

    protected $table = 'order_bargain_log';
    public $timestamps = false;
}
