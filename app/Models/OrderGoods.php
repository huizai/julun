<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderGoods extends Model{

    protected $table = 'order_goods';
    public $timestamps = false;

    public function getOrderGoods($orderId){
        return DB::table($this->table)
            ->select(
                DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                'name',
                'thumbnail',
                'quantity',
                'sku'
            )
            ->where('orders_id', $orderId)
            ->get();
    }
}
