<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentTurnover extends Model{

    protected $table = 'agent_turnover';
    public $timestamps = false;
}
