<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class StoreRecommendLeisure extends Model{

    protected $table = 'store_recommend_Leisure';
    public $timestamps = false;

    /**
     * 获取推荐店铺ID
     * @return mixed
     */
    public function getStoreId(){
        return DB::table($this->table)->get();
    }

    /**
     * 新增推荐店铺
     * @param $storeId
     * @return int
     */
    public function saveStore($storeId){
        if ( count($this->getStoreId()) >= 4){
            return -1;
        }

        return DB::table($this->table)->insert([
            'store_id' => $storeId
        ]);
    }

    /**
     * 删除推荐店铺
     * @param $id
     * @return mixed
     */
    public function deleteStore($id){
        return DB::table($this->table)->where('store_id', $id)->delete();
    }


}
