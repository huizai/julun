<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreBookRefund extends Model{

    protected $table = 'store_book_refund';
    public $timestamps = false;
}
