<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreTurnover extends Model{

    protected $table = 'store_turnover';
    public $timestamps = false;
}
