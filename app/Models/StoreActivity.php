<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StoreActivity extends Model{

    protected $table = 'store_activity';
    public $timestamps = false;

    public function getStoreActivity($storeId){
        return DB::table($this->table)
            ->where('stores_id', $storeId)
            ->whereNull('delete_time')
            ->get();
    }
}
