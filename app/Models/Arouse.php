<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Arouse extends Model{

    protected $table = 'arouse';
    public $timestamps = false;

    public function getArouseDay(){
        return DB::table($this->table)->value('day');
    }
}
