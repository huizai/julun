<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderCoupons extends Model{

    protected $table = 'order_coupons';
    public $timestamps = false;
}
