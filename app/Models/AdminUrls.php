<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdminUrls extends Model{

    protected $table = 'admin_urls';
    public $timestamps = false;

    /**
     * 节点列表Sql
     * @return mixed
     */
    private function adminUrlListSql($search){
        $sql = DB::table($this->table)->whereNull('deleted_at');

        if (isset($search['url'])){
            $sql->where('url', 'like', '%'.$search['url'].'%');
        }

        if (isset($search['method'])){
            $sql->where('method', 'like', '%'.$search['method'].'%');
        }

        return $sql;
    }

    /**
     * 节点列表
     * @param $page
     * @param $pageSize
     * @param $search
     * @return mixed
     */
    public function adminUrlList($page, $pageSize, $search){
        $sql = $this->adminUrlListSql($search);

        return $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get()->toArray();
    }

    /**
     * 节点分页
     * @param $page
     * @param $pageSize
     * @param $search
     * @return array
     */
    public function adminUrlPagination($page, $pageSize, $search){
        $sql = $this->adminUrlListSql($search);

        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    public function adminUrlInfo($id){
        return DB::table($this->table)->select('url','method')->where('id',$id)->first();
    }

    public function getUrl($id){
        return DB::table($this->table)->whereIn('id',$id)->get();
    }

    /**
     * 添加权限节点
     * @param $data
     * @return mixed
     */
    public function addUrl($data){
        return DB::table($this->table)->insert($data);
    }

    /**
     * 修改权限节点
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateUrl($id, $data){
        return DB::table($this->table)->where('id',$id)->update($data);
    }

    /**
     * 删除权限节点
     * @param $id
     * @return mixed
     */
    public function deleteUrl($id){
        return DB::table($this->table)->where('id',$id)->update([
            'deleted_at' => date('Y-m-d H:i:s',time())
        ]);
    }
}
