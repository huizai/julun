<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AdminUserPermissions;
use App\Models\AdminUserSidebar;
use DB;

class AdminUser extends Model{

    protected $table = 'admin_user';
    public $timestamps = false;

//    public function adminUser($page, $pageSize, $nickname){
//        $sql = DB::table($this->table)
//            ->select(
//                'id',
//                'nickname',
//                'last_login_ip',
//                'last_login_time',
//                'sort',
//                'create_time',
//                'status'
//            )
//            ->whereNull('delete_time')
//            ->orderBy('sort','desc');
//
//        if (empty($nickname)){
//            $sql->where('nickname','like','%'.$nickname.'%');
//        }
//
//        $total = $sql->count();
//
//        $list = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();
//
//        return [
//            'list' => $list,
//            'pagination' => [
//                'current' => $page,
//                'pageSize' => $pageSize,
//                'total' => $total
//            ]
//        ];
//    }
//
//
//    /**
//     * 管理员详情
//     * @param $id
//     * @return object
//     */
//    public function adminUserInfo($id){
//        $user = DB::table($this->table)->select(
//            'id',
//            'username',
//            'avatar',
//            'nickname',
//            'sort',
//            'status'
//        )->where('id',$id)
//        ->first();
//
//        return $user;
//
//    }
//
//    public function addAdminUser($data, $sidebar_id, $type){
//
//        try{
//            DB::beginTransaction();
//
//            $insertID = DB::table($this->table)->insertGetId($data);
//            if (!$insertID){
//                DB::rollBack();
//                return false;
//            }
//
//            $insertData = [];
//            foreach ($sidebar_id as $key => $value) {
//                $insertData[$key]['type']           = $type;
//                $insertData[$key]['user_id']        = $insertID;
//                $insertData[$key]['sidebar_id']     = $value;
//            }
//
//            $adminUserSidebarModel = new AdminUserSidebar();
//            if (
//            !DB::table($adminUserSidebarModel->getTable())->insert($insertData)
//            ) {
//                DB::rollback();
//                return false;
//            }
//
//            DB::commit();
//            return $insertID;
//        } catch (\Exception $exception) {
//            \Log::error('addAdminUser：' . $exception);
//            return false;
//        }
//
//    }
//
//    public function updateAdminUser($id, $data, $sidebar_id, $type){
//
//        try{
//            DB::beginTransaction();
//
//            if(!empty($data)){
//
//                if(
//                !DB::table($this->table)->where('id',$id)->update($data)
//                ){
//                    DB::rollBack();
//                    return false;
//                }
//
//            }
//
//
//            if (!empty($sidebar_id)){
//
//                $sidebarModel = new AdminUserSidebar();
//                if (
//                  DB::table($sidebarModel->getTable())
//                      ->where('user_id',$id)
//                      ->where('type', $type)
//                      ->delete() === false
//                ){
//                    DB::rollBack();
//                    return false;
//                }
//
//                $updateSidebarData = [];
//                foreach ($sidebar_id as $key => $value) {
//                    $updateSidebarData[$key]['type'] = $type;
//                    $updateSidebarData[$key]['user_id'] = $id;
//                    $updateSidebarData[$key]['sidebar_id'] = $value;
//                }
//
//                if (
//                !DB::table($sidebarModel->getTable())->insert($updateSidebarData)
//                ){
//                    DB::rollBack();
//                    return false;
//                }
//
//            }
//
//            DB::commit();
//            return true;
//        }catch (\Exception $exception){
//            \Log::error('updateAdminUser：'.$exception);
//            return false;
//        }
//    }
//
//    public function deleteAdminUser($id){
//
//        return DB::table($this->table)->where('id',$id)->update([
//            'delete_time' => date('Y-m-d H:i:s',time())
//        ]);
//    }
//
//    public function getAdminUserInfo($userId){
//        return DB::table($this->getTable())->where('id', $userId)->first();
//    }

    /**
     * 新增管理员
     * @param $data
     * @return mixed
     */
    public function registerAdminUser($data){
        return DB::table($this->table)->insert($data);
    }

    /**
     * 通过账号获取管理员信息
     * @param $userName
     * @return mixed
     */
    public function getUserInfo($userName){
        return DB::table($this->table)->where('username', $userName)->whereNull('delete_time')->first();
    }
}
