<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WechatUsers extends Model{

    protected $table = 'wechat_users';
    public $timestamps = false;

    protected $guarded = [

    ];
}
