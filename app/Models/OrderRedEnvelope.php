<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderRedEnvelope extends Model{

    protected $table = 'order_red_envelope';
    public $timestamps = false;
}
