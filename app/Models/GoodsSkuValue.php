<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GoodsSkuValue extends Model{

    protected $table = 'goods_sku_value';
    public $timestamps = false;

    protected $fillable = [
        'value',
        'goods_id',
        'name_id',
    ];

    public function del($goodsId){
        $GoodsSkuValue = new GoodsSkuValue();

        return DB::table($GoodsSkuValue->getTable())->where('goods_id',$goodsId)->delete();
    }
}
