<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreTakeaway extends Model{

    protected $table = 'store_takeaway';
    public $timestamps = false;
}
