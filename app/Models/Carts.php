<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carts extends Model{

    protected $table = 'carts';
    protected $fillable = ['users_id', 'goods_id', 'quantity', 'company_id', 'create_time', 'store_id', 'sku_path', 'sku_value'];
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
}
