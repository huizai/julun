<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsEstimatesImages extends Model{

    protected $table = 'goods_estimates_images';
    public $timestamps = false;
}
