<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderActivity extends Model{

    protected $table = 'order_activity';
    public $timestamps = false;
}
