<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserClicks extends Model{

    protected $table = 'user_clicks';
    public $timestamps = false;
}
