<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopSet extends Model{

    protected $table = 'shop_set';
    public $timestamps = false;
    protected $fillable = [
        'company_id',
        'tel',
//        'store_pwd',
        'logo',
        'title',
        'description',
        'create_time',
    ];
}
