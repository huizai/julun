<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentUserInfo extends Model{

    protected $table = 'agent_user_info';
    public $timestamps = false;
}
