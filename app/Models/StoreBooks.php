<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreBooks extends Model{

    protected $table = 'store_books';
    public $timestamps = false;
    protected $fillable = [
        'users_id',
        'stores_id',
        'position',
        'people',
        'book_time',
        'mobile',
        'name',
        'sex',
        'remark',
        'pay_amount',
        'seat_id',
        'coupon_id',
        'activity_id'
    ];
}
