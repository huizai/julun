<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreSeatPosition extends Model{

    protected $table = 'store_seat_position';
    public $timestamps = false;
}
