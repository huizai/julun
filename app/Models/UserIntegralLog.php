<?php
namespace App\Models;
use App\Models\Users;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class UserIntegralLog extends Model{

    protected $table = 'user_integral_log';
    public $timestamps = false;


    //积分列表
    public function lists(array $search, int $page = 1, int $pageSize = 20){
        $prefix     = env('DB_PREFIX');
        $userModel=new Users();
        $userIntegralModel=new UserIntegralLog();
        $sql=DB::table($userModel->getTable() . ' as s')->whereNull('s.delete_time')
            ->leftJoin($userIntegralModel->getTable() . ' as sc', 'sc.users_id', '=', 's.id')
            ->select(
                's.id',
                's.name',
                's.mobile',
                DB::raw("convert({$prefix}s.integral, decimal(15,2)) as totalintegral")
            )->groupBy('s.id');
        if(isset($search['name'])){
            $sql->where('s.name', 'like', "%".$search['name']."%");
        }
        if(isset($search['mobile'])){
            $sql->where('s.mobile', $search['mobile']);
        }
        if (isset($search['create_time'])){
            $sql->whereBetween('sc.create_time', $search['create_time']);
        }
        $total  = $sql->get()->count();
        $store = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get()->toArray();
        return [
            'list'          => $store,
            'pagination'    => [
                'total'     => $total,
                'current'   => $page,
                'pageSize'  => $pageSize
            ]
        ];
    }

    //积分明细
    public function index(array $search, int $page = 1, int $pageSize = 20){
        $prefix     = env('DB_PREFIX');
       $userModel=new Users();
       $userIntegralModel=new UserIntegralLog();
        $sql=DB::table($userIntegralModel->getTable() . ' as sc')->whereNull('s.delete_time')
            ->leftJoin($userModel->getTable() . ' as s', 'sc.users_id', '=', 's.id')
            ->select(
                's.name',
                's.mobile',
                DB::raw("convert({$prefix}sc.integral, decimal(15,2)) as integral"),
                'sc.vary',
                'sc.source',
                'sc.create_time'
            )->where('s.id',$search['userid']);
        if (isset($search['create_time'])){
            $sql->whereBetween('sc.create_time', $search['create_time']);
        }
        $total  = $sql->get()->count();
        $store = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get()->toArray();
        return [
            'list'          => $store,
            'pagination'    => [
                'total'     => $total,
                'current'   => $page,
                'pageSize'  => $pageSize
            ]
        ];
    }

    public function getPage(){

    }
}
