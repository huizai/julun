<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentTurnoverAddress extends Model{

    protected $table = 'agent_turnover_address';
    public $timestamps = false;
}
