<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsAgentEstimatesImages extends Model{

    protected $table = 'goods_agent_estimates_images';
    public $timestamps = false;
}
