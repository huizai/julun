<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreBrand extends Model{

    protected $table = 'store_brand';
    public $timestamps = false;
}
