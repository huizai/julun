<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentApply extends Model{

    protected $table = 'agent_apply';
    public $timestamps = false;
}
