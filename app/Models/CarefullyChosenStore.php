<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarefullyChosenStore extends Model{

    protected $table = 'carefully_chosen_store';
    public $timestamps = false;
}
