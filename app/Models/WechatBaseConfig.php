<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WechatBaseConfig extends Model{

    protected $table = 'wechat_base_config';
    public $timestamps = false;


    private function __weChatsql(){
        $wechat_base_config = new WechatBaseConfig();

       $sql = DB::table($wechat_base_config->getTable())
            ->select('*');

       return $sql;
    }

    public function weChat_confBase($companyId){ //基本信息
       $sql = $this->__weChatsql();
       $arr = $sql->where('company_id','=',$companyId)->get();

       return $arr;
    }


/*    public function weChat_confUpdate($id=null,$searchData=[]){ //修改
            if(empty($id)){
                $this->weChat_confInsert($searchData);
            }else{

            }
    }
    public function weChat_confInsert($searchData=[]){  //新增
        $wechat_base_config = new WechatBaseConfig();
        $searchData
        $userId = DB::table($wechat_base_config->getTable())->insertGetId($searchData);
        if (!$userId) {
            return false;
        }

        return true;
    }*/
}
