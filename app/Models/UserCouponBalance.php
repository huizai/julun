<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCouponBalance extends Model{

    protected $table = 'user_coupon_balance';
    public $timestamps = false;
}
