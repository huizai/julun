<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderGroupBuy extends Model{

    protected $table = 'order_group_buy';
    public $timestamps = false;
}
