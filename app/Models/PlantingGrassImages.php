<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlantingGrassImages extends Model{

    protected $table = 'planting_grass_images';
    public $timestamps = false;
}
