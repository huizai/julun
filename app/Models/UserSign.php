<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSign extends Model{

    protected $table = 'user_sign';
    public $timestamps = false;
}
