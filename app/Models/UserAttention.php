<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserAttention extends Model{

    protected $table = 'user_attention';
    public $timestamps = false;

    public function defaultAttentionUser($userId, $beUserId){
        DB::table($this->table)->insert([
            'users_id'      => $userId,
            'be_users_id'   => $beUserId
        ]);
    }
}
