<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceLog extends Model{

    protected $table = 'balance_log';
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'user_id',
        'number',
        'type',
        'money_type',
        'credit_sn',
        'operator',
        'remake',
        'create_time',
    ];
}
