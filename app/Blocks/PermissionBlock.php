<?php
namespace App\Blocks;


class PermissionBlock{
    /**
     * 获取中信微信支付的对象
     * @param $sidebar
     * @return \array
     */
    public function getRole($sidebar){
//        将重复的节点去除
        $result_01 = array_flip($sidebar);
        $result_02 = array_flip($result_01);
        $sidebar   = array_merge($result_02);
//        查询所有节点
        $data = $authorityModel->getUserRoute($admin_id);
        $data = json_decode(json_encode($data), true);
        $roteId             = [];

        foreach ($data as $key => &$value) {
            if ($value['pid'] == 0){
                $roteId[] = $value['id'];
            }

            $data[$key] = array_filter($value, function ($item){
                if($item === '' || $item === null){
                    return false;
                }
                return true;
            });

            if (isset($value['meta'])){
                $value['meta'] = json_decode($value['meta'], true);
            }


            if (isset($value['alwaysShow']) && $value['alwaysShow'] == 1){
                $value['alwaysShow'] = true;
            }else if (isset($value['alwaysShow']) && $value['alwaysShow'] == 0){
                $value['alwaysShow'] = false;
            }

            if (isset($value['hidden']) && $value['hidden'] == 1){
                $value['hidden'] = true;
            }else if (isset($value['hidden']) && $value['hidden'] == 0){
                $value['hidden'] = false;
            }
        }
//        弄成树状
        return [12];
    }

}
