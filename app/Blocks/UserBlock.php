<?php
namespace App\Blocks;

use App\Libs\YunXin\ServerApi;
use App\Models\Activity;
use App\Models\ActivityApply;
use App\Models\AdminUser;
use App\Models\BalanceLog;
use App\Models\CollectGoods;
use App\Models\CollectStores;
use App\Models\Feedback;
use App\Models\Goods;
use App\Models\OrderGoods;
use App\Models\Orders;
use App\Models\StoreCoupons;
use App\Models\StoreIncomeLog;
use App\Models\StoreOfflineCoupons;
use App\Models\Stores;
use App\Models\StoreTurnover;
use App\Models\Turnover;
use App\Models\UserAttention;
use App\Models\UserCouponBalance;
use App\Models\UserCoupons;
use App\Models\UserIntegralLog;
use App\Models\UserOfflineCoupons;
use App\Models\UserRechargeLog;
use App\Models\UserShareLottery;
use App\Models\UserViewRecording;
use App\Models\UserWalletLog;
use App\Models\UserYunxin;
use App\Models\WechatUsers;
use Illuminate\Support\Facades\DB;
use App\Models\Users;
use App\Models\UserAddress;
use App\Models\District;
use QrCode;
use Illuminate\Support\Facades\Redis;
use App\Models\RechargeActivity;

class UserBlock{
    /**
     * 注册成为新用户
     * @param null $mobile
     * @param null $name
     * @param null $password
     * @param null $avatar
     * @param null $inviteCode
     * @param int $wechatUserId
     * @param null $regId
     * @return bool|int
     */
    public function registerUser(
        $mobile=null,
        $name=null,
        $password=null,
        $avatar=null,
        $inviteCode=null,
        $wechatUserId=0,
        $regId=null
    ){
        $userModel                  = new Users();
        $storeCouponModel           = new StoreCoupons();
        $userCouponModel            = new UserCoupons();

        $parentId   = 0;
        $integral   = 0;

        if (!empty($mobile)){
            $user = DB::table($userModel->getTable())->where('mobile', $mobile)->first();
            if ($user){
                return -1;
            }
        }


        if(!empty($inviteCode)) {
            /**
             * 查找上级
             */
            $parent = DB::table($userModel->getTable())
                ->where('invite_code', $inviteCode)
                ->whereNull('delete_time')
                ->first();

            if ($parent) {
                $parentId = $parent->id;
                $integral = 10;
            }
        }

        //在中间件 GetCompanyId获取
        global $companyId;

        $saveData = [
            'parent_id'         => $parentId,
            'mobile'            => $mobile,
            'name'              => $name,
            'avatar'            => $avatar,
            'wechat_user_id'    => $wechatUserId,
            'company_id'        => $companyId,
            'reg_id'            => $regId,
            'create_time'       => date('Y-m-d H:i:s', time())
        ];
        if($password) {
            $saveData['password'] = bcrypt($password);
        }

        try {
            DB::beginTransaction();
            $userId = DB::table($userModel->getTable())->insertGetId($saveData);
            if (!$userId) {
                return false;
            }

            $myInviteCode = $this->createCode($userId);

            if (DB::table($userModel->getTable())->where('id', $userId)->update([
                    'invite_code' => $myInviteCode
                ]) === false) {
                DB::rollBack();
                return false;
            }
            if($integral > 0 && $parentId > 0){
                if (DB::table($userModel->getTable())->where('id', $parentId)->increment('integral', $integral) === false) {
                    DB::rollBack();
                    return false;
                }
            }

            $coupon = DB::table($storeCouponModel->getTable())
                ->where('belong', 1)
                ->where('scope', '新人首单红包')
                ->whereNull('delete_time')
                ->first();

            if ($coupon){
                $endTime = time()+($coupon->term_of_validity*60*60);
                if(!DB::table($userCouponModel->getTable())
                    ->insert([
                        'users_id'          => $userId,
                        'coupon_id'         => $coupon->id,
                        'start_time'        => date('Y-m-d H:i:s', time()),
                        'end_time'          => date('Y-m-d H:i:s', $endTime)
                    ])
                ){
                    \Log::debug('添加新人首单红包');
                }
            }

            //注册礼包
            $offlineCoupon = $this->registerGiftPackage($userId);
            \Log::debug('offlineCoupon'.$offlineCoupon);
            if(!$offlineCoupon){
                \Log::debug('添加注册礼包失败');
                DB::rollBack();
                return false;
            }

            $userYunxinModel    = new UserYunxin();
            $accId              = md5($userId.$name);
            $yunXin             = new ServerApi();
            $yunXinInfo         = $yunXin->createUserId($accId);
            if ($yunXinInfo['code'] == 200){
                $userYunxinModel->saveUserYunXinInfo($userId, $accId, $yunXinInfo['info']['token']);
            }



            DB::commit();
            return $userId;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }

    public function saveWechatUser($data){
        $inviteCode = $data['invite_code'];
        unset($data['invite_code']);

        try {
            DB::beginTransaction();

            $wechatUser = WechatUsers::updateOrCreate(['openid' => $data['openid']],$data);
            if($wechatUser === false){
                \Log::debug('添加微信用户数据失败'.json_encode($data));
                return false;
            }

            $user = Users::where('wechat_user_id', $wechatUser->id)->first();

            if(!$user) {
                $userId = $this->registerUser(null, $data['nickname'], null, $data['avatar'], $inviteCode, $wechatUser->id);
            }else{
                $userId = $user->id;
            }

            if($userId === false){
                DB::rollBack();
                return false;
            }
            DB::commit();
            return $userId;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::debug($exception);
            return false;
        }
    }

    /**
     * 生成邀请码
     * @param $userId
     * @return string
     */
    private function createCode($userId) {
        static $source_string = 'E5FCDG3HQA4B1NOPIJ2RSTUV67MWX89KLYZ';
        $num = $userId;
        $code = '';
        while ( $num > 0) {
            $mod = $num % 35;
            $num = ($num - $mod) / 35;
            $code = $source_string[$mod].$code;
        }

        if(empty($code[6]))
            $code = str_pad($code,6,'0',STR_PAD_LEFT);
        return $code;
    }

    /**
     * 根据openid获取用户信息
     * @param $openId
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function getUserInfoByOpenid($openid){
        $userModel          = new Users();
        $wechatUserModel    = new WechatUsers();

        $user = DB::table($userModel->getTable() . ' as u')
            ->select('u.*', 'wu.openid')
            ->leftJoin($wechatUserModel->getTable() . ' as wu', 'u.wechat_user_id', '=', 'wu.id')
            ->where('wu.openid', $openid)
            ->whereNull('u.delete_time')
            ->first();

        return $user;
    }

    public function wechatMiniLogin($data,$inviteCode){
        $wechatUserModel    = new WechatUsers();

        try {
            DB::beginTransaction();
            $wechatUserId = DB::table($wechatUserModel->getTable())->insertGetId($data);

            if(!$wechatUserId){
                return false;
            }

            $userId = $this->registerUser(null, null, null, null, $inviteCode, $wechatUserId);
            if($userId === false){
                DB::rollBack();
                return false;
            }

            DB::commit();
            return $userId;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }

    public function wechatMiniAuthorize($userId, $openid, $data){
        $wechatUserModel    = new WechatUsers();
        $userModel          = new Users();

        try{
            if(
                DB::table($wechatUserModel->getTable())
                    ->where('openid', $openid)
                    ->update($data) === false
            ){
                return false;
            }

            //如果有昵称等信息
            //但是用户信息表里面的字段没有数据
            //才更新用户信息表
            $user = DB::table($userModel->getTable())
                ->where('id', $userId)
                ->first();
            $userUpdateData = [];
            if(isset($data['nickname']) && !$user->name){
                $userUpdateData['name'] = $data['nickname'];
            }
            if(isset($data['avatar']) && !$user->avatar){
                $userUpdateData['avatar'] = $data['avatar'];
            }

            if(!empty($userUpdateData)) {
                if (
                    DB::table($userModel->getTable())
                        ->where('id', $userId)
                        ->update([
                            'name' => $data['nickname'],
                            'avatar' => $data['avatar']
                        ]) === false
                ) {
                    DB::rollBack();
                    return false;
                }
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }

    private function __userAddressSql(int $userId, array $search=[]){
        $userAddressModel = new UserAddress();
        $districtModel = new District();

        $sql = DB::table($userAddressModel->getTable() . ' as ua')
            ->select(
                'ua.id',
                'ua.company_id',
                'ua.users_id',
                'ua.consignee',
                'ua.contact',
                'ua.province',
                'ua.city',
                'ua.district',
                'ua.address',
                'ua.longitude',
                'ua.latitude',
                'ua.create_time',
                'ua.default',
                'pd.name as province_name',
                'cd.name as city_name',
                'dd.name as district_name'
            )
            ->leftJoin($districtModel->getTable() . ' as pd', 'pd.id', '=', 'ua.province')
            ->leftJoin($districtModel->getTable() . ' as cd', 'cd.id', '=', 'ua.city')
            ->leftJoin($districtModel->getTable() . ' as dd', 'dd.id', '=', 'ua.district')
            ->where('users_id', $userId)
            ->whereNull('delete_time');

        if(isset($search['id'])){
            $sql->where('ua.id', $search['id']);
        }
        if(isset($search['default'])){
            $sql->where('ua.default', $search['default']);
        }
        return $sql;
    }

    public function getUserAddressList(int $userId, array $search=[]){
        $sql = $this->__userAddressSql($userId, $search);

        $address = $sql->get();

        return $address;
    }

    public function getUserAddressInfo(int $userId, array $search=[]){
        $sql = $this->__userAddressSql($userId, $search);

        $address = $sql->first();

        return $address;
    }
    //修改用户积分或余额
    public function updateBalanceIntegral($data,$operator=0){
//        $data = [
//            'company_id'    => 1,
//            'user_id'       => 9,
//            'number'        => 30,
//            'type'          => 0,
//            'money_type'    => 1,
//            'credit_sn'     => "RC".date("YmdHis").mt_rand(1000,9999),
//            'operator'      => $operator,
//            'remake'        => "后台充值/减少多少多少钱",
//            'create_time'   => date('Y-m-d H:i:s', time()),
//        ];


        try{
            $userModel          = new Users();
            $balanceLog         = new BalanceLog();
            $sql = $user = DB::table($userModel->getTable())
                ->where('id',$data['user_id'])
                ->where('company_id',$data['company_id']);

            if ($data['money_type']==1){
                $field = 'integral';
            }else{
                $field = 'balance';
                $data['number'] = $data['number']*100;
            }
            if ($data['type']==1){
                $user = $sql->increment($field, $data['number']);
            }else{
                $user = $sql->where($field, '>=', $data['number'])->decrement($field, $data['number']);
            }
            if ($user){
                $balance = DB::table($balanceLog->getTable())->insertGetId(
                    $data
                );
            }else{
                $balance = 1;
            }
            return $balance;
        }catch (\Exception $exception){
            \Log::error($exception);
            return false;
        }

    }


    private function userSql($search)
    {
        $userMode           = new Users();
        $wechatUserModel    = new WechatUsers();
        $sql = DB::table($userMode->getTable() . ' as u')
            ->select(
                'u.*',
                DB::raw("convert(balance/100, decimal(15,2)) as balance"),
                'w.openid',
                'w.avatar as wx_avatar',
                'w.mobile as wx_mobile',
                'w.origin'
            )
            ->leftJoin($wechatUserModel->getTable() . ' as w', 'u.wechat_user_id', '=', 'w.id')
            ->whereNull('u.delete_time');
        if (isset($search['name'])) {
            $sql->where('u.name', 'like', "%" . $search['name'] . "%");
        }
        if (isset($search['mobile'])) {
            $sql->where('u.mobile', 'like', "%" . $search['mobile'] . "%");
        }
        if (isset($search['id'])) {
            $sql->where('u.id', $search['id']);
        }

        return $sql;
    }

    /**
     * 获取用户列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function userList($search, $page, $pageSize, $asc = [], $desc = [])
    {
        $sql = $this->userSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $user = $sql->orderBy('id', 'desc')->get();

        return $user;
    }

    /**
     * 获取用户列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function userListPagination(array $search, int $page = 1, int $pageSize = 20)
    {
        $sql = $this->userSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取用户详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function userInfo($search)
    {
        $sql = $this->userSql($search);
        $user = $sql->first();
        return $user;
    }



    private  function userBalance_sql(){
        $userModel = new Users();
        $adminModel = new AdminUser();
        $sql =\Illuminate\Support\Facades\DB::table('balance_log as a')
            ->select(
                'a.id',
                'a.number',
                'a.type',
                'a.money_type',
                'a.credit_sn',
                'a.remake',
                'b.name',
                'b.avatar',
                'c.username'
            )
            ->leftJoin($userModel->getTable().' as b','a.user_id','=','b.id')
            ->leftJoin($adminModel->getTable().' as c','a.operator','=','c.id');
        return $sql;
    }

    public function userBalance_log(array $search = [], int $page = 1, int $pageSzie = 20){
        $sql= $this->userBalance_sql();
        if(!empty($search['username'])){
            $sql->where('c.username', 'like',"%{$search['username']}%");
        }
        if(!empty($search['name'])){
            $sql->where('b.name', 'like',"%{$search['name']}%");
        }

        $cart= $sql->skip(($page - 1) * $pageSzie)
            ->take($pageSzie)
            ->get();

        return $cart;

    }
    public function userBalance_logPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->userBalance_log($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    /**
     * 通过手机号获取用户信息
     */
    public function getUserInfoByMobile($mobile){
        $userModel = new Users();

        $user = DB::table($userModel->getTable())
            ->where('mobile', $mobile)
            ->whereNull('delete_time')
            ->first();

        return $user;
    }

    /**
     * 通过用户ID获取用户信息
     */
    public function getUserInfoById($id){
        $userModel = new Users();

        $user = DB::table($userModel->getTable())
            ->where('id', $id)
            ->whereNull('delete_time')
            ->first();

        return $user;
    }

    /**
     * 领取优惠券
     */
    public function receiveCoupon($userId, $couponId){
        $storeCouponModel = new StoreCoupons();
        $userCouponModel  = new UserCoupons();
        $userModel        = new Users();

        $userIntegral     = DB::table($userModel->getTable())->where('id', $userId)->value('integral');

        $coupon = DB::table($storeCouponModel->getTable())
            ->where('id', $couponId)
            ->whereNull('delete_time')
            ->first();

        $userCoupon = DB::table($userCouponModel->getTable())
            ->where('users_id', $userId)
            ->where('coupon_id', $couponId)
            ->where('is_use', 0)
            ->where('end_time', '>', date('Y-m-d H:i:s', time()))
            ->whereNull('delete_time')
            ->first();

        if ($coupon->quantity == 0){
            return -4;
        }

        if ($coupon->selling_price > 0 && $userIntegral < $coupon->selling_price){
            return -3;
        }

        if(!$coupon){
            //优惠券不存在或者已经领取完了
            \Log::info("优惠券已经领取完了或者优惠券ID不正确;优惠券ID{$couponId},用户ID:{$userId}");
            return -1;
        }

        if ($userCoupon){
            //已领取该优惠券
            \Log::info("已领取该优惠券;优惠券ID{$couponId},用户ID:{$userId}");
            return -2;
        }

        try{
            DB::beginTransaction();

            //更新用户优惠券记录
            $userCouponData = [
                'users_id'      => $userId,
                'coupon_id'     => $couponId,
                'start_time'    => date('Y-m-d H:i:s', time()),
                'end_time'      => date('Y-m-d H:i:s', time()+($coupon->term_of_validity*60*60))
            ];
            if(
                DB::table($userCouponModel->getTable())
                    ->insert($userCouponData) === false
            ){
                return false;
            }

            //更新优惠券数量
            if(
                DB::table($storeCouponModel->getTable())
                    ->where('id', $couponId)
                    ->decrement('quantity') === false
            ){
                DB::rollBack();
                \Log::error("更新优惠券数量失败;优惠券ID{$couponId},用户ID:{$userId}");
                return false;
            }

            if ($coupon->selling_price > 0){
                if(!DB::table($userModel->getTable())->where('id', $userId)
                    ->decrement('integral', $coupon->selling_price)
                ){
                    DB::rollBack();
                    \Log::error("修改用户积分失败");
                    return false;
                }
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }

    public function myCoupon($userId, $scope, $virtual){
        $storeCouponModel   = new StoreCoupons();
        $userCouponModel    = new UserCoupons();
        $storeModel         = new Stores();

        $sql  = DB::table($userCouponModel->getTable() . ' as uc')
            ->select(
                'uc.start_time',
                'uc.end_time',
                'uc.users_id',
                'uc.coupon_id',
                'uc.id',
                'uc.create_time',
                'sc.name',
                'sc.company_id',
                'sc.stores_id',
                'sc.term_of_validity',
                'sc.type',
                'sc.scope',
                DB::raw("convert(full_amount/100, decimal(15,2)) as full_amount"),
                'discount_amount',
                's.name as store_name',
                's.logo as store_logo',
                's.store_type_id',
                'sc.address'
            )
            ->leftJoin($storeCouponModel->getTable() . ' as sc', 'sc.id','=', 'uc.coupon_id')
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'sc.stores_id')
            ->where('uc.users_id', $userId)
            ->where('uc.is_use', 0)
            ->where('uc.delete_time')
            ->whereNull('sc.delete_time');

        if (!empty($scope)){
            $sql->where('sc.scope', $scope)
                ->where('sc.virtual', 1);
        }

        if (!empty($virtual)){
            $sql->where('sc.virtual', $virtual);
        }

        $coupon = $sql->get();

        foreach ($coupon as $item) {
            if ($item->type == 0){
                $item->discount_amount = priceIntToFloat($item->discount_amount);
            }else{
                $item->discount_amount = ($item->discount_amount / 10).'折';
            }

            if (!empty($scope)){
                $item->store_name = '新华联梦想城';
            }
        }

        return $coupon;
    }

    /**
     * 微信小程序绑定手机号
     */
    public function wxMiniBindMobile($userId, $mobile, $invitationCode){
        $userModel          = new Users();
        $wechatUserModel    = new WechatUsers();

        $wechatUser = DB::table($userModel->getTable() . ' as u')
            ->leftJoin($wechatUserModel->getTable() . ' as wu', 'u.wechat_user_id', '=', 'wu.id')
            ->where('u.id', $userId)
            ->first();


        $mobileUser = DB::table($userModel->getTable())
            ->where('mobile', $mobile)
            ->first();

        if(!empty($invitationCode)){
            $invitationUser = DB::table($userModel->getTable())
                ->where('invite_code', $invitationCode)
                ->value('id');
        }


        //手机号没有绑定
        if(!$mobileUser){
            $saveData                   = [];
            $saveData['mobile']         = $mobile;
            if (!empty($invitationUser)){
                $saveData['parent_id']  = $invitationUser;
            }

            if(
                DB::table($userModel->getTable())
                    ->where('id', $userId)
                    ->update($saveData) !== false
            ){
                return true;
            }
        }

        //手机号被绑定了
        if($mobileUser->wechat_user_id == 0){
//        if($mobileUser->wechat_user_id == 0){
            //如果手机用户没有绑定微信小程序
            //需要将微信小程序授权创建的用户删除
            try {
                DB::beginTransaction();
                if (
                    DB::table($userModel->getTable())
                        ->where('id', $mobileUser->id)
                        ->update([
                            'wechat_user_id' => $wechatUser->wechat_user_id
                        ]) === false
                ) {
                    return false;
                }

                /**
                 * 这步操作只适应于小程序登录的时候强制绑定手机的情况
                 * 不然有可能已经产生用户数据了
                 * 直接删除掉就不合适了
                 */

                if (
                    DB::table($userModel->getTable())
                        ->where('id', $userId)
                        ->delete() === false
                ) {
                    DB::rollBack();
                    return false;
                }

                DB::commit();
                return true;
            }catch (\Exception $exception){
                DB::rollBack();
                \Log::error($exception);
                return false;
            }
        }else{
            //手机号被绑定了
            return -1;
        }
    }

    /**
     * 收藏商品
     * @param $data
     * @return bool
     */
    public function collectGoods($userId, $goodsId){
        $collectGoodsModel = new CollectGoods();
        $data = [];
        if(is_array($goodsId)){
            foreach ($goodsId as $key => $datum) {
                $goods = DB::table($collectGoodsModel->getTable())
                    ->where('users_id', $userId)
                    ->where('goods_id', $datum)
                    ->first();

                if ($goods){
                    return -1;
                }

                $data[$key] = [
                    'users_id' => $userId,
                    'goods_id' => $datum
                ];
            }
        }else{
            $goods = DB::table($collectGoodsModel->getTable())
                ->where('users_id', $userId)
                ->where('goods_id', $goodsId)
                ->first();

            if ($goods){
                return -1;
            }

            $data = [
                'users_id' => $userId,
                'goods_id' => $goodsId
            ];
        }

        return DB::table($collectGoodsModel->getTable())->insert($data);
    }

    /**
     * 收藏店铺
     * @param $data
     * @return bool
     */
    public function collectStore($data){
        $collectStoreModel = new CollectStores();
        $store = DB::table($collectStoreModel->getTable())
            ->where('users_id', $data['users_id'])
            ->where('stores_id', $data['stores_id'])
            ->first();

        if ($store){
            return -1;
        }
        return DB::table($collectStoreModel->getTable())->insert($data);
    }

    /**
     * 意见反馈添加
     * @param $data
     * @return bool
     */
    public function feedbackAdd($data){
        $FeedbackModel = new Feedback();
        return DB::table($FeedbackModel->getTable())->insert($data);
    }

    /**
     * 公益活动报名
     * @param $data
     * @return bool
     */
    public function activityApply($data){
        $activityApplyModel = new ActivityApply();
        $activityInfo = DB::table($activityApplyModel->getTable())
            ->where('users_id', $data['users_id'])
            ->where('act_id', $data['act_id'])
            ->first();
        if ($activityInfo){
            return -1;
        }
        return DB::table($activityApplyModel->getTable())->insert($data);
    }

    /**
     * 报名的公益活动
     * @param $data
     * @return bool
     */
    public function activityMyApply($uid,$type){
        $activityModel = new Activity();
        $activityApplyModel = new ActivityApply();
        $sql = DB::table($activityApplyModel->getTable().' as a')
            ->select('b.*')
            ->leftJoin($activityModel->getTable() . ' as b', 'a.act_id', '=', 'b.id')
            ->where('a.users_id',$uid);
        if($type == 0){
            $sql->where('start_time','>=',date("Y-m-d",time()));
        }else{
            $sql->where('start_time','<',date("Y-m-d",time()));
        }
        $data = $sql->orderBy('a.create_time','desc')->get();
        return $data;
    }


    private function __userStreamSql($search){
        $storeModel = new Stores();
        $walletModel = new UserWalletLog();
        $prefix = env('DB_PREFIX');
        $sql = DB::table($walletModel->getTable().' as o')->select(
            DB::raw("convert(money/100, decimal(15,2)) as pay_amount,convert({$prefix}o.balance/100, decimal(15,2)) as balance"),
            'o.wallet_type',
            'o.create_time as pay_time',
            's.name as store_name',
            'o.remake',
            'o.type'
        )
            ->leftJoin($storeModel->getTable().' as s','o.stores_id','=','s.id')
            ->where('o.users_id', $search['user_id'])
            ->orderBy('o.create_time','desc');
        return $sql;
    }

    public function userStream($page, $pageSize, $search){
        $sql = $this->__userStreamSql($search);

        return $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();
    }

    public function userStreamPagination($page, $pageSize, $search){
        $sql = $this->__userStreamSql($search);

        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    /**
        充值订单记录
     **/
    public function recharge_log(int $userId, $batch){
        $userRechargeLogModel = new UserRechargeLog();
        return DB::table($userRechargeLogModel->getTable())
            ->where('users_id',$userId)
            ->where('out_trade_no',$batch)
            ->first();
    }


    public function initRecharge(int $userId, $companyId, $totalFee, $remake, $type){
        $userModel   = new Users();
        $userData = DB::table($userModel->getTable())
            ->where('id', $userId)
            ->first();
        if(!$userData){
            return false;
        }

        if (empty($userData->mobile)){
            return -1;
        }

        //充值单号
        $batch = $userId.(microtime(true) * 10000).mt_rand(1000, 9999);
        //执行sql
        try {
            DB::beginTransaction();

                //组合订单基本数据
                $order = [
                    'out_trade_no'      => $batch,
//                    'company_id'        => $companyId,
                    'users_id'          => $userId,
                    'total_fee'         => $totalFee*100,
                    'status'            => 0,
                    'remake'            => $remake,
                    'pay_type'          => $type == 'wx' ? 1 : 2,
                    'create_time'       => date('Y-m-d H:i:s', time())
                ];
                $userRechargeLogModel = new UserRechargeLog();
                $rechargeId = DB::table($userRechargeLogModel->getTable())
                    ->insertGetId($order);

                if(!$rechargeId){
                    \Log::error("创建充值订单失败:".json_encode($order)."");
                    return false;
                }

            DB::commit();

            //返回充值单号
            return $batch;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::debug($exception);
        }
    }

    /**
     * 发送模板消息
     */
    public function smallWxMessage($userId,$formid,$tempid,$kws)
    {
        $userModel          = new Users();
        $wechatUserModel    = new WechatUsers();
        DB::connection()->enableQueryLog();
        $user = DB::table($userModel->getTable() . ' as u')
            ->leftJoin($wechatUserModel->getTable() . ' as wu', 'u.wechat_user_id', '=', 'wu.id')
            ->where('u.id', $userId)
            ->first();
        $data = <<<END
{
  "touser": "{$user->openid}",
  "template_id": "{$tempid}",
  "page": "index",
  "form_id": "{$formid}",
  "data": {
      "keyword1": {
          "value": "{$kws[0]}"
      },
      "keyword2": {
          "value": "{$kws[1]}"
      },
      "keyword3": {
          "value": "{$kws[2]}"
      } ,
      "keyword4": {
          "value": "{$kws[3]}"
      } ,
      "keyword5": {
          "value": "{$kws[4]}人"
      }
  },
  "emphasis_keyword": "keyword1.DATA"
}
END;
        $access = $this->getToken();  //获取token
        $access_token= $access['access_token'];
        $url = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=" . $access_token;
        $data = $this->getHttpArray($url,$data);  //post请求url
//        \Log::error("消息通知:".$data."");
        return $data;
    }


    public function getQrcode($g_uid,$scene,$page,$savePath){

        $expires_in = 7200;

        if(!is_dir(public_path().'/'.$savePath)){
            @mkdir(public_path().'/'.$savePath, 0775, true);
        }
        $fileUrl = \Config::get('app.url').'/'.$savePath."/{$g_uid}.png";
        $savePath  =public_path().'/'.$savePath."/{$g_uid}.png";
        if(file_exists($savePath)){
            //当前时间-文件创建时间<过期时间
            if( (time()-filemtime($savePath)) < $expires_in ) return $fileUrl;
        }

        $access = $this->getToken();  //获取token
        $access_token= $access['access_token'];
        $url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token='.$access_token;
        $qrcode = array(
            'scene'			=> $scene,
            'width'			=> 200,
            'page'			=> $page,
            'auto_color'	=> true
        );
        $result = $this->is_request($url,true,'POST',json_encode($qrcode));
        $errcode = json_decode($result,true)['errcode'];
        $errmsg = json_decode($result,true)['errmsg'];
        if($errcode) return array('status'=>0,'info'=>$errmsg);
        $res = file_put_contents($savePath,$result);
        return $fileUrl; //返回本地图片地址
    }

    private function getToken()
    {
//        $appid = 'wx10cb0441bd039cfb';
//        $secret = 'dd9393d41ae166ff316414d466acebcb';
        $appid = 'wx10cb0441bd039cfb';
        $secret = 'dd9393d41ae166ff316414d466acebcb';
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$appid}&secret={$secret}";
        return $data = $this->curlGet($url);
    }

    private function getHttpArray($url,$post_data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   //没有这个会自动输出，不用print_r();也会在后面多个1
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $output = curl_exec($ch);
        curl_close($ch);
        $out = json_decode($output);
        return $out;
    }

    private function curlGet($url) {
        //1.初始化Curl
        $curl = curl_init();
        //设置curl传输选项
        curl_setopt($curl, CURLOPT_URL, $url);//访问ip地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//设置为FALSE 禁止 cURL 验证对等证书
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);//设置为1或true时，获取的信息以字符串返回
        $data = curl_exec($curl); //返回值
        curl_close($curl);
        $out = json_decode($data,true);
        return $out;
    }

    /**
     * 发送request请求
     * @param $url
     * @param bool $ssl
     * @param string $type
     * @param null $data
     * @return bool|mixed
     */
    public function is_request($url, $ssl = true, $type = 'GET', $data = null)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $user_agent = isset($_SERVER['HTTP_USERAGENT']) ? $_SERVER['HTTP_USERAGENT'] : 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36';
        curl_setopt($curl, CURLOPT_USERAGENT, $user_agent);//请求代理信息
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);//referer头 请求来源
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);//请求超时
        curl_setopt($curl, CURLOPT_HEADER, false);//是否处理响应头
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);//curl_exec()是否返回响应
        if ($ssl) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//禁用后curl将终止从服务端进行验证
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);//检查服务器ssl证书中是否存在一个公用名（common name）
        }
        if ($type == "POST") {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        //发出请求
        $response = curl_exec($curl);
        if ($response === false) {
            return false;
        }
        return $response;
    }


    public function viewRecordingList($page, $pageSize, $type, $userId){
        $goodsModel             = new Goods();
        $viewRecordingModel     = new UserViewRecording();

        $sql = DB::table($viewRecordingModel->getTable().' as uvr');


        if ($type == 1){

            $sql->leftJoin($goodsModel->getTable(). ' as g', 'g.id', '=', 'uvr.type_id')
                ->addSelect(
                    'g.name',
                    'g.id',
                    'g.sales_volume',
                    DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                    'g.thumbnail'
                )
                ->where('users_id', $userId)
                ->where('type', 1)
                ->groupBy('uvr.type_id')
                ->orderBy('uvr.create_time');
        }

        $total  = $sql->count();
        $list   = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();

        return [
            'list'          => $list,
            'pagination'    => [
                    'current'       => $page,
                    'pageSize'      => $pageSize,
                    'total'         => $total
            ]
        ];

    }

    /**
     * 设置支付密码
     * @param $password
     * @param $userId
     * @return mixed
     */
    public function settingPayPassword($password, $userId){
        $userModel      = new Users();
        return DB::table($userModel->getTable())->where('id', $userId)->update([
            'pay_password'  => $password
        ]);
    }

    /**
     * 检测用户是否设置支付密码
     * @param $userId
     * @return mixed
     */
    public function checkPayPassword($userId){
        $userModel      = new Users();
        return DB::table($userModel->getTable())->where('id', $userId)->value('pay_password');
    }

    /**
     * 获取用户个人信息
     * @param $userId
     * @return mixed
     */
    public function getUserInfo($userId){
        $userModel      = new Users();
        return DB::table($userModel->getTable())->where('id', $userId)->first();
    }

    /**
     * 修改用户极光注册ID
     * @param $userId
     * @param $regId
     */
    public function saveRegId($userId, $regId, $type){
        $userModel      = new Users();
        $data           = [
            'update_time'   => date('Y-m-d H:i:s', time())
        ];
        if ($type == 'app'){
            $data['reg_id']         = $regId;
        }
        $result = DB::table($userModel->getTable())->where('id', $userId)->update($data);
        \Log::debug($result);
    }

    /**
     * 获取用户极光注册ID
     * @param $userId
     * @return mixed
     */
    public function getUserRegId($userId){
        $userModel          = new Users();
        $sql = DB::table($userModel->getTable())
            ->select('reg_id');

        if (is_array($userId)){
            $sql->whereIn('id', $userId);
        }else{
            $sql->where('id', $userId);
        }

        return $sql->first();
    }

    /**
     * 绑定邮箱地址
     * @param $data
     * @return bool
     */
    public function bindingEmail($data){
        $userModel      = new Users();
        $userIntegral   = new UserIntegralLog();

        try{
            DB::beginTransaction();

            if(!DB::table($userModel->getTable())
                ->where('id', $data['userId'])
                ->update([
                    'email'         => $data['email'],
                    'integral'      => DB::raw('integral + 10')
                ])
            ){
                \Log::debug('修改用户邮箱失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($userIntegral->getTable())
                ->insert([
                    'users_id'      => $data['userId'],
                    'vary'          => 1,
                    'integral'      => 10,
                    'source'        => '绑定邮箱',
                    'create_time'   => date('Y-m-d', time())
                ])
            ){
                \Log::debug('添加用户积分日志失败');
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error('绑定邮箱失败原因:'.$exception);
            DB::rollBack();
            return false;
        }

    }

    /**
     * 根据邮箱地址获取用户信息
     * @param $email
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getUserInfoByEmail($email){
        $userModel      = new Users();
        return DB::table($userModel->getTable())
            ->where('email', $email)
            ->first();
    }

    /**
     * 填写邀请码
     * @param $userId
     * @param $inviteCode
     * @return int
     */
    public function inviteUser($userId, $inviteCode){
        $userModel      = new Users();
        $parentId = DB::table($userModel->getTable())
            ->where('invite_code', $inviteCode)
            ->value('id');
        if($parentId){
            return DB::table($userModel->getTable())
                ->where('id', $userId)
                ->update([
                    'parent_id'    => $parentId
                ]);
        }else{
            return -1;
        }

    }

    public function userUpgrade($userId){
        $userModel      = new Users();
        $userLevel = DB::table($userModel->getTable())
            ->where('id', $userId)
            ->value('level');

        if ($userLevel === 0){
            return DB::table($userModel->getTable())
                ->where('id', $userId)
                ->update([
                    'level'     => 1,
                    'integral'  => DB::raw('integral + 100')
                ]);
        }
    }

    /**
     * 添加注册礼包
     * @param $id
     * @return bool
     */
    public function registerGiftPackage($id){
        $offlineCouponModel         = new StoreOfflineCoupons();
        $userOfflineCouponModel     = new UserOfflineCoupons();

        $offlineCoupons = DB::table($offlineCouponModel->getTable())
            ->select('id', 'effective_time')
            ->whereNull('delete_time')
            ->get()->toArray();

        if (empty($offlineCoupons)){
            return true;
        }

        try{
            $userOfflineCoupon = [];
            foreach ($offlineCoupons as $key => $value) {
                $path = '/qrcode/useroffline/'.$id.$value->id.time().'.png';
                $userOfflineCoupon[$key]['users_id']            = $id;
                $userOfflineCoupon[$key]['coupons_id']          = $value->id;
                $userOfflineCoupon[$key]['expire_time']         = date('Y-m-d H:i:s',($value->effective_time*60*60)+time());
                $userOfflineCoupon[$key]['cancel_after']        = getSalt();
                $userOfflineCoupon[$key]['cancel_after_code']   = env('APP_URL').$path;
                QrCode::format('png')
                    ->size(500)
                    ->encoding('UTF-8')
                    ->generate(
                        env('APP_URL')."/pluto/user/offline?id={$id}&coupons_id={$value->id}",
                        public_path().$path
                    );
            }

            return DB::table($userOfflineCouponModel->getTable())
                ->insert($userOfflineCoupon);
        }catch (\Exception $exception){
            \Log::error($exception);
            return false;
        }
    }

    public function myOfflineCoupon($page, $pageSize, $userId){
        $offlineCouponModel             = new StoreOfflineCoupons();
        $userOfflineCouponModel         = new UserOfflineCoupons();

        $sql = DB::table($userOfflineCouponModel->getTable().' as uoc')
            ->select(
                'so.id',
                'so.name',
                DB::raw("convert(discount_amount/100, decimal(15,2)) as discount_amount"),
                'so.rule',
                'so.effective_time',
                'uoc.coupons_id',
                'uoc.expire_time',
                'uoc.cancel_after',
                'uoc.cancel_after_code',
                'uoc.create_time'
            )
            ->leftJoin($offlineCouponModel->getTable().' as so', 'so.id', '=', 'uoc.coupons_id')
            ->where('uoc.is_use', 0)
            ->where('uoc.users_id', $userId);

        $total      = $sql->count();
        $list       = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
                        ->get();

        return [
            'list'          => $list,
            'pagination'    => [
                'total'     => $total,
                'current'   => $page,
                'pageSize'  => $pageSize
            ]
        ];

    }

    /**
     * 余额二维码付款
     * @param $userId
     * @param $storeId
     * @param $money
     * @return bool|int
     */
    public function balanceQrCodePay($userId, $storeId, $money){
        $userModel          = new Users();
        $userIntegralModel  = new UserIntegralLog();
        $storeModel         = new Stores();
        $userWalletModel    = new UserWalletLog();
        $orderModel         = new Orders();
        $orderGoodsModel    = new OrderGoods();
        $storeIncomeModel   = new StoreIncomeLog();
        $storeTurnoverModel = new StoreTurnover();
        $turnoverModel      = new Turnover();

        try{
            DB::beginTransaction();

            $userInfo = $userModel->getUserInfo($userId);
            if ($userInfo->balance < $money){
                return -1;
            }

            $integral = floor($money);

            //修改用户余额，积分
            $saveUserData = [
                'balance'       => DB::raw('balance - '.$money),
                'integral'      => DB::raw('integral + '.$integral),
                'is_shop'       => 1
            ];
            if (isset($level)){
                $saveUserData['level']  = $level;
            }
            if(!DB::table($userModel->getTable())
                ->where('id', $userId)
                ->update($saveUserData)
            ){
                \Log::debug('线下余额付款码支付，扣款失败');
                DB::rollBack();
                return false;
            }

            //商户名称
            $storeName  = DB::table($storeModel->getTable())
                ->where('id', $storeId)
                ->value('name');

            //添加钱包明细
            if(!DB::table($userWalletModel->getTable())
                ->insert([
                    'users_id'          => $userId,
                    'money'             => $money,
                    'balance'           => $userInfo->balance,
                    'wallet_type'       => 1,
                    'stores_id'         => $storeId,
                    'type'              => 1,
                    'remake'            => "线下余额二维码付款给{$storeName}商家"
                ])
            ){
                \Log::debug('添加余额支付记录失败');
                DB::rollBack();
                return false;
            }



            //添加积分明细
            $saveIntegralData = [];
            $saveIntegralData[0] = [
                'users_id'          => $userId,
                'vary'              => 1,
                'integral'          => $integral,
                'source'            => "线下余额二维码付款给{$storeName}商家",
                'create_time'       => date('Y-m-d H:i:s', time())
            ];

            //给邀请人添加积分
            if ($userInfo->parent_id != 0 && $userInfo->is_shop == 0){
                //计算上级所得积分
                $parentIntegral = floor($integral * 0.8);
                if ($parentIntegral > 0){
                    if(!DB::table($userModel->getTable())
                        ->where('id', $userInfo->parent_id)
                        ->increment('integral', $parentIntegral)
                    ){
                        \Log::debug('上级用户添加积分失败');
                        DB::rollBack();
                        return false;
                    }

                    $saveIntegralData[1] = [
                        'users_id'          => $userInfo->parent_id,
                        'vary'              => 1,
                        'integral'          => $parentIntegral,
                        'source'            => "邀请的用户消费返还{$parentIntegral}积分",
                        'create_time'       => date('Y-m-d H:i:s', time())
                    ];
                }

            }

            if(!DB::table($userIntegralModel->getTable())
                ->insert($saveIntegralData)
            ){
                \Log::debug('添加积分明细失败');
                DB::rollBack();
                return false;
            }

            //添加订单
            $orderId    = DB::table($orderModel->getTable())
                ->insertGetId([
                    'batch'         => $userId.(microtime(true) * 10000).mt_rand(1000, 9999),
                    'order_sn'      => $storeId . (microtime(true) * 10000) . $userId,
                    'company_id'    => 1,
                    'users_id'      => $userId,
                    'stores_id'     => $storeId,
                    'goods_price'   => $money,
                    'pay_amount'    => $money,
                    'pay_time'      => date('Y-m-d H:i:s', time()),
                    'pay_type'      => 3,
                    'status'        => 3,
                    'type'          => 1,
                    'pay_mode'      => 1
                ]);

            if (!$orderId){
                \Log::debug('添加订单失败');
                DB::rollBack();
                return false;
            }

            //添加订单商品
            if(!DB::table($orderGoodsModel->getTable())
                ->insert([
                    'orders_id'     => $orderId,
                    'goods_id'      => 1,
                    'name'          => '商家客户端扫码支付',
                    'selling_price' => $money,
                    'quantity'      => 1
                ])
            ){
                \Log::debug('添加商品失败');
                DB::rollBack();
                return false;
            }

            //添加商户余额
            if(!DB::table($storeModel->getTable())
                ->where('id', $storeId)
                ->update([
                    'balance'       => DB::raw('balance + '.$money),
                    'turnover'      => DB::raw('turnover + '.$money)
                ])
            ){
                \Log::debug('添加商户余额失败');
                DB::rollBack();
                return false;
            }

            //添加商户收入日志
            if(!DB::table($storeIncomeModel->getTable())
                ->insert([
                    'stores_id'     => $storeId,
                    'orders_id'     => $orderId,
                    'money'         => $money,
                    'type'          => 1
                ])
            ){
                \Log::debug('添加商户收入日志失败');
                DB::rollBack();
                return false;
            }

            //添加商户营业额
            $turnover = DB::table($storeTurnoverModel->getTable())
                ->where('stores_id', $storeId)
                ->where('date', date('Y-m-d', time()))
                ->first();
            if ($turnover){
                if(!DB::table($storeTurnoverModel->getTable())
                    ->where('stores_id', $storeId)
                    ->where('date', date('Y-m-d', time()))
                    ->update([
                        'turnover'      => DB::raw('turnover + '.$money)
                    ])
                ){
                    \Log::debug('修改商户每日营业额日志失败');
                    DB::rollBack();
                    return false;
                }
            }else{
                if(!DB::table($storeTurnoverModel->getTable())
                    ->insert([
                        'stores_id'     => $storeId,
                        'date'          => date('Y-m-d', time()),
                        'turnover'      => $money
                    ])
                ){
                    \Log::debug('添加商户每日营业额日志失败');
                    DB::rollBack();
                    return false;
                }

            }

            //添加平台营业额
            $turnover = DB::table($turnoverModel->getTable())
                ->where('date', date('Y-m-d', time()))
                ->first();
            if ($turnover){
                if(!DB::table($turnoverModel->getTable())
                    ->where('date', date('Y-m-d', time()))
                    ->increment('turnover', $money)
                ){
                    DB::rollBack();
                    \Log::debug('添加平台营销额失败');
                    return false;
                }
            }else{
                if(!DB::table($turnoverModel->getTable())
                    ->insert([
                        'turnover'  => $money,
                        'date'      => date('Y-m-d', time())
                    ])
                ){
                    DB::rollBack();
                    \Log::debug('修改平台营销额失败');
                    return false;
                }
            }


            Redis::del('payQrCode:'.$userId);
            DB::commit();
            return true;
        }catch(\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }

    public function getOfflineCoupon($userId){
        $userOfflineStoreModel      = new UserOfflineCoupons();
        $storeOfflineCouponModel    = new StoreOfflineCoupons();
        $storeModel                 = new Stores();

        return DB::table($userOfflineStoreModel->getTable().' as uo')
            ->select(
                'uo.expire_time',
                'uo.cancel_after',
                'uo.cancel_after_code',
                'so.name',
                DB::raw("convert(discount_amount/100, decimal(15,2)) as discount_amount"),
                'so.rule',
                's.name as store_name',
                'so.address'
            )
            ->leftJoin($storeOfflineCouponModel->getTable().' as so', 'so.id', '=', 'uo.coupons_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'so.stores_id')
            ->where('uo.users_id', $userId)
            ->where('uo.is_use', 0)
            ->get();

    }

    /**
     * 抽奖
     * @param $userId
     * @return array|bool|int|mixed|string
     */
    public function userLottery($userId){
        $userModel          = new Users();
        $couponModel        = new StoreCoupons();
        $userCouponModel    = new UserCoupons();

        $lottery = DB::table($userModel->getTable())
            ->where('id', $userId)
            ->value('lottery');

        if ($lottery < 1){
            return -1;
        }

        try{
            DB::beginTransaction();

            $coupon = DB::table($couponModel->getTable())
                ->select('id', 'odds', 'name', 'term_of_validity', 'quantity')
                ->where('scope','抽奖')
                ->whereNull('delete_time')
                ->get()->toArray();

            if (empty($coupon)){
                //减少抽奖次数
                if(!DB::table($userModel->getTable())
                    ->where('id', $userId)
                    ->decrement('lottery')
                ){
                    \Log::debug('减少抽奖次数失败');
                    DB::rollBack();
                    return false;
                }
                return  ['id' => 0, 'name' => '很遗憾，未中奖'];
            }

            //拿到抽奖奖品
            $proSum = 0;
            foreach ($coupon as $value) {
                $proSum += $value->odds;
            }
            $result = '';
            foreach ($coupon as $key => $value) {
                $randNum = mt_rand(1, 10000);
                if ($randNum <= $value->odds) {
                    $result = $value;
                    break;
                } else {
                    $proSum -= $value->odds;
                }
            }




            //减少抽奖次数
            if(!DB::table($userModel->getTable())
                ->where('id', $userId)
                ->decrement('lottery')
            ){
                \Log::debug('减少抽奖次数失败');
                DB::rollBack();
                return false;
            }

            //返回中奖记录
            if (!empty($result)){
                //奖品数量不足
                if ($result->quantity < 1){
                    DB::rollBack();
                    return -2;
                }

                if(!DB::table($couponModel->getTable())
                    ->where('id', $result->id)
                    ->decrement('quantity')
                ){
                    \Log::debug('修改奖品数量失败');
                    DB::rollBack();
                    return false;
                }

                $saveResult = DB::table($userCouponModel->getTable())
                    ->insert([
                        'users_id'      => $userId,
                        'coupon_id'     => $result->id,
                        'start_time'    => date('Y-m-d H:i:s', time()),
                        'end_time'      => date('Y-m-d H:i:s', time() + $result->term_of_validity * 60 * 60)
                    ]);

                if ($saveResult){
                    DB::commit();
                    return $result;
                }else{
                    \Log::debug('添加中奖优惠券失败');
                    DB::rollBack();
                    return false;
                }

            }else{
                DB::commit();
                return ['id' => 0, 'name' => '很遗憾，未中奖'];
            }

        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }

    }

    /**
     * 用户兑换余额
     * @param $userId
     * @param $couponId
     * @return bool|int
     */
    public function exchangeBalanceCoupon($userId, $couponId){
        $storeCouponModel       = new StoreCoupons();
        $userModel              = new Users();
        $userWalletModel        = new UserWalletLog();
        $userCouponModel        = new UserCoupons();
        $userCouponBalanceModel = new UserCouponBalance();

        $coupon = DB::table($storeCouponModel->getTable())
            ->where('id', $couponId)
            ->where('belong', 1)
            ->where('scope', '余额')
            ->whereNull('delete_time')
            ->first();



        if (!$coupon){
            return -1;
        }

        if ($coupon->quantity < 1){
            return -2;
        }

        \Log::debug('兑换码：'.$coupon->code);
        \Log::debug('用户：'.$userId);
        try{
            DB::beginTransaction();
            $userBalance = DB::table($userModel->getTable())->where('id', $userId)->value('balance');

            if(!DB::table($userModel->getTable())
                ->where('id', $userId)
                ->update([
                    'balance'       => DB::raw('balance + '.$coupon->discount_amount),
                    'integral'      => DB::raw('integral + '.priceIntToFloat($coupon->discount_amount))
                ])
            ){
                \Log::debug('添加用户余额失败');
                DB::rollBack();
                return false;
            }
            $exchangeBalance = $coupon->discount_amount;

            if(!DB::table($userCouponBalanceModel->getTable())
                ->insert([
                    'users_id'          => $userId,
                    'coupon_id'         => $couponId,
                    'code'              => $coupon->code,
                    'balance'           => $coupon->discount_amount
                ])
            ){
                \Log::debug('添加用户余额优惠券失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($userWalletModel->getTable())
                ->insert([
                    'users_id'      => $userId,
                    'money'         => $coupon->discount_amount,
                    'balance'       => $userBalance,
                    'wallet_type'   => 0,
                    'type'          => 6,
                    'remake'        => '余额优惠券兑换金额'.priceIntToFloat($coupon->discount_amount)
                ])
            ){
                \Log::debug('添加用户余额日志失败');
                DB::rollBack();
                return false;
            }

            $coupon = DB::table($storeCouponModel->getTable())
                ->where('scope', '充值')
                ->where('recharge_amount', '<=', $coupon->discount_amount)
                ->where('quantity', '>', 0)
                ->whereNull('delete_time')
                ->get()->toArray();

            $userCoupon = DB::table($userCouponModel->getTable().' as uc')
                ->select('uc.coupon_id')
                ->leftJoin($storeCouponModel->getTable().' as sc', 'sc.id', '=', 'uc.coupon_id')
                ->where('sc.scope', '充值')
                ->where('uc.users_id', $userId)
                ->whereNull('sc.delete_time')
                ->get();
            $userCouponId = [];
            foreach ($userCoupon as $item) {
                $userCouponId[] = $item->coupon_id;
            }

            $couponsId = [];
            $couponsId[] = $couponId;
            if (!empty($coupon)){
                $saveUserCoupon = [];
                foreach ($coupon as $key => $value) {
                    if (in_array($value->id, $userCouponId)){
                        continue;
                    }
                    $couponsId[] = $value->id;
                    $saveUserCoupon[$key]['users_id']         = $userId;
                    $saveUserCoupon[$key]['coupon_id']        = $value->id;
                    $saveUserCoupon[$key]['start_time']       = date('Y-m-d H:i:s', time());
                    $saveUserCoupon[$key]['end_time']         = date('Y-m-d H:i:s', time() + $value->term_of_validity * 60 *60);
                    $saveUserCoupon[$key]['code']             = null;
                    $saveUserCoupon[$key]['qr_code']          = null;
                    if ($value->virtual == 2){
                        $path = public_path().'/qrcode/coupon/user';
                        if (!is_dir(public_path().'/qrcode/coupon')){
                            @mkdir(public_path().'/qrcode/coupon', 0755);
                        }

                        if (!is_dir($path)){
                            @mkdir($path, 0755);
                        }

                        $code = getSalt(12);
                        $imageName = $userId.time().$value->id.".png";
                        \SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')
                            ->size(500)
                            ->encoding('UTF-8')
                            ->generate(
                                $code,
                                $path."/".$imageName
                            );

                        $saveUserCoupon[$key]['code']           = $code;
                        $saveUserCoupon[$key]['qr_code']        = env('APP_URL').'/qrcode/coupon/user/'.$imageName;
                    }
                }

                if (!empty($saveUserCoupon)){
                    if(!DB::table($userCouponModel->getTable())
                        ->insert($saveUserCoupon)
                    ){
                        \Log::debug('添加充值优惠券失败');
                    }
                }

            }
            if(!DB::table($storeCouponModel->getTable())
                ->whereIn('id', $couponsId)
                ->decrement('quantity')
            ){
                \Log::debug('减少优惠券数量失败');
            }


            \Log::info("余额兑换成功====>>>>>>>>>>>>>用户id：{$userId}===>>>>>>>>>>金额：{$exchangeBalance}");
            DB::commit();
            return true;
        }catch(\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }

    }

    /**
     * 扫码领券
     * @param $code
     * @param $userId
     * @return bool|int
     */
    public function receiveScanCodeCoupon($code, $userId){
        $storeCouponModel       = new StoreCoupons();
        $userCouponModel        = new UserCoupons();

        $coupon = DB::table($storeCouponModel->getTable())
            ->where('code', $code)
            ->where('belong', 1)
            ->whereNull('delete_time')
            ->first();

        if (!$coupon){
            return -1;
        }

        if ($coupon->quantity < 1){
            return -2;
        }

        $userCoupon = DB::table($userCouponModel->getTable())
            ->where('coupon_id', $coupon->id)
            ->where('users_id', $userId)
            ->whereNull('delete_time')
            ->first();

        if ($userCoupon){
            return -3;
        }

        try{
            $saveData = [];
            $saveData['users_id']       = $userId;
            $saveData['coupon_id']      = $coupon->id;
            $saveData['start_time']     = date('Y-m-d H:i:s', time());
            $saveData['end_time']       = date('Y-m-d H:i:s', time() + $coupon->term_of_validity * 60 * 60);
            if ($coupon->virtual == 2){
                $saveCode               = getSalt(12);
                $saveData['code']       = $saveCode;

                if (!is_dir(public_path().'/qrcode/coupon')){
                    @mkdir(public_path().'/qrcode/coupon', 0755);
                }
                $path = public_path().'/qrcode/coupon/user';
                if (!is_dir($path)){
                    @mkdir($path, 0755);
                }
                $imageName = $userId.time().$coupon->id.".png";
                $data = [
                    'redirect_type'     => 'native',
                    'path'              => 'DiscountCoupon',
                    'params'            => [
                        'code'          => $saveCode
                    ]
                ];
                QrCode::format('png')
                    ->size(500)
                    ->encoding('UTF-8')
                    ->generate(
                        json_encode($data),
                        $path.'/'.$imageName
                    );

                $saveData['qr_code']    = env('APP_URL')."/qrcode/coupon/user/{$imageName}";
            }

            if(!DB::table($storeCouponModel->getTable())
                ->where('id', $coupon->id)
                ->decrement('quantity')
            ){
                \Log::debug('修改优惠券数量失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($userCouponModel->getTable())
                ->insert($saveData)
            ){
                \Log::debug('添加用户优惠券失败');
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        }catch(\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }

    }


    public function shareLottery($userId){
        $userShareLotteryModel      = new UserShareLottery();
        $userModel                  = new Users();
        $userShare      = DB::table($userShareLotteryModel->getTable())
            ->where('users_id', $userId)
            ->where('day', date('y-m-d', time()))
            ->first();

        if ($userShare && $userShare->share >= 5 ){
            return -1;
        }
        try{
            if ($userShare && $userShare->share < 5){

                if(!DB::table($userModel->getTable())
                    ->where('id', $userId)
                    ->increment('lottery')
                ){
                    \Log::debug('增加用户抽奖次数失败');
                    DB::rollBack();
                    return false;
                }

                if(!DB::table($userShareLotteryModel->getTable())
                    ->where('id', $userShare->id)
                    ->increment('share'))
                {
                    \Log::debug('修改分享日志失败');
                    DB::rollBack();
                    return false;
                }

            }else if (!$userShare){
                if(!DB::table($userModel->getTable())
                    ->where('id', $userId)
                    ->increment('lottery')
                ){
                    \Log::debug('增加用户抽奖次数失败');
                    DB::rollBack();
                    return false;
                }

                if(!DB::table($userShareLotteryModel->getTable())
                    ->insert([
                        'users_id'      => $userId,
                        'share'         => 1,
                        'day'           => date('y-m-d', time())
                    ]))
                {
                    \Log::debug('添加分享日志失败');
                    DB::rollBack();
                    return false;
                }
            }

            DB::commit();
            return true;
        }catch(\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }

    }

    /**
     * 获取余额10元以上的用户数量
     * @return int
     */
    public function getUserBalanceIsTen(){
        $userModel      = new Users();
        return DB::table($userModel->getTable())
            ->where('balance', '>=', 10)
            ->whereNull('delete_time')
            ->count();
    }

    public function getUserInfoByIdAttention($userId, $beUserId){
        $userModel              = new Users();
        $userAttentionModel     = new UserAttention();

        $userInfo = DB::table($userModel->getTable())
            ->where('id', $beUserId)
            ->first();
        $userAttention = DB::table($userAttentionModel->getTable())
            ->where('users_id', $userId)
            ->where('be_users_id', $beUserId)
            ->whereNull('delete_time')
            ->first();

        if ($userAttention){
            $userInfo->is_attention = 1;
        }else{
            $userInfo->is_attention = 0;
        }

        return $userInfo;
    }

    /**
     * 获取充值活动赠送金额
     * @param $money
     * @return mixed
     */
    public function getRechargeActivity($money){
        $rechargeActivityModel      = new RechargeActivity();
        $currentTime                = date('Y-m-d H:i:s', time());
        $rechargeSend               = DB::table($rechargeActivityModel->getTable())
            ->where('condition', '<=', $money)
            ->where('start_time', '<=', $currentTime)
            ->where('end_time', '>=', $currentTime)
            ->orderBy('send', 'desc')
            ->first();

        return $rechargeSend;
    }
}
