<?php
namespace App\Blocks;


use App\Models\StoreDevices;
use DB;


class StoreDevicesBlock{
    private function devicesSql($search){
        $storeDevicesModel = new StoreDevices();
        $sql = DB::table($storeDevicesModel->getTable())
            ->whereNull('delete_time');
        if(isset($search['name'])){
            $sql->where('name', 'like', "%".$search['name']."%");
        }
        if(isset($search['id'])){
            $sql->where('id', $search['id']);
        }
        if(isset($search['company'])){
            $sql->where('company_id', $search['company']);
        }
        if(isset($search['store_id'])){
            $sql->where('stores_id', $search['store_id']);
        }

        return $sql;
    }
    /**
     * 获取商户服务列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeDevicesList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->devicesSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeDevices = $sql->orderBy('id', 'desc')->get();

        return $storeDevices;
    }

    /**
     * 获取商户服务列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeDevicesListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->devicesSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户服务详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeDevicesInfo($search){
        $sql = $this->devicesSql($search);
        $storeDevices = $sql->first();
        return $storeDevices;
    }

}