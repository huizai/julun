<?php
namespace App\Blocks;

use App\Models\GoodsClassifies;
use Illuminate\Support\Facades\DB;


class GoodClassifiesBlock{
    private function classSql($search){
        $goodClassModel = new GoodsClassifies;
        $sql = DB::table($goodClassModel->getTable())
            ->whereNull('delete_time');
        if(isset($search['name'])){
            $sql->where('name', 'like', "%".$search['name']."%");
        }
        if(isset($search['id'])){
            $sql->where('id', $search['id']);
        }
        if(isset($search['company'])){
            $sql->where('company_id', $search['company']);
        }
        if(isset($search['type'])){
            $sql->where('type', $search['type']);
        }


        return $sql;
    }
    /**
     * 获取商品分类列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function goodClassList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->classSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);
        $goodClass = $sql->orderBy('id', 'desc')->get();

        return $goodClass;
    }

    /**
     * 获取商品分类列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function goodClassListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->classSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商品详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function goodClassInfo($search){
        $sql = $this->classSql($search);
        $goodClass = $sql->first();
        return $goodClass;
    }

}