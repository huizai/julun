<?php
namespace App\Blocks;


use App\Models\Devices;
use DB;


class DevicesBlock{
    private function devicesSql($search){
        $DevicesModel = new Devices();
        $sql = DB::table($DevicesModel->getTable())
            ->whereNull('delete_time');
        if(isset($search['name'])){
            $sql->where('name', 'like', "%".$search['name']."%");
        }
        if(isset($search['id'])){
            $sql->where('id', $search['id']);
        }
        if(isset($search['company'])){
            $sql->where('company_id', $search['company']);
        }

        return $sql;
    }
    /**
     * 获取商户服务列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function devicesList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->devicesSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $devices = $sql->orderBy('id', 'desc')->get();

        return $devices;
    }

    /**
     * 获取商户服务列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function devicesListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->devicesSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户服务详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function devicesInfo($search){
        $sql = $this->devicesSql($search);
        $devices = $sql->first();
        return $devices;
    }

}