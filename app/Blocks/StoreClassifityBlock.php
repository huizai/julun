<?php
namespace App\Blocks;

use App\Models\StoreClassifity;
use App\Models\StoreType;
use DB;


class StoreClassifityBlock{
    private function classSql($search){
        $storeClassModel = new StoreClassifity;
        $storeTypeModel  = new StoreType();
        $sql = DB::table($storeClassModel->getTable(). ' as sc')
            ->select(
                'sc.*',
                'psc.name as parent_name',
                'st.name as type_name'
            )
            ->leftJoin($storeTypeModel->getTable() . ' as st', 'st.id', '=', 'sc.type_id')
            ->leftJoin($storeClassModel->getTable() . ' as psc', 'psc.id', '=', 'sc.parent_id')
            ->whereNull('sc.delete_time');
        if(isset($search['name'])){
            $sql->where('sc.name', 'like', "%".$search['name']."%");
        }
        if(isset($search['id'])){
            $sql->where('sc.id', $search['id']);
        }
        if(isset($search['company'])){
            $sql->where('sc.company_id', $search['company']);
        }


        return $sql;
    }
    /**
     * 获取商户分类列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeClassList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->classSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeClass = $sql->orderBy('sc.id', 'desc')->get();

        return $storeClass;
    }

    /**
     * 获取商户分类列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeClassListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->classSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商品详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeClassInfo($search){
        $sql = $this->classSql($search);
        $storeClass = $sql->first();
        return $storeClass;
    }

    public function getStoreClass(){
        $storeClassModel = new StoreClassifity();
        return DB::table($storeClassModel->getTable())
            ->select('id','parent_id as parentId','name')
            ->get();
    }

}
