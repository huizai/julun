<?php
namespace App\Blocks;

use App\Models\Alipay;
use App\Models\StoreBookOrder;
use App\Models\StoreBookRefund;
use App\Models\StoreBooks;
use App\Models\Stores;
use App\Models\StoreSeatPosition;
use App\Models\UserCoupons;
use App\Models\Users;
use App\Models\UserWalletLog;
use App\Models\WechatPay;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Yansongda\Pay\Pay;
use function EasyWeChat\Kernel\Support\get_server_ip;


class StoreBooksBlock{
    private function bookSql($search){
        $storeBookModel             = new StoreBooks();
        $storeSeatPositionModel     = new StoreSeatPosition();
        $userModel                  = new Users();
        $sql = DB::table($storeBookModel->getTable(). ' as sbk')
            ->select(
                'sbk.*',
                'ssp.name as position_name',
                'u.name as user_nickname'
            )
            ->leftJoin($storeSeatPositionModel->getTable() . ' as ssp', 'ssp.id', '=', 'sbk.position')
            ->leftJoin($userModel->getTable() . ' as u', 'u.id', '=', 'sbk.users_id')
            ->whereNull('sbk.delete_time');
        if(isset($search['mobile'])){
            $sql->where('sbk.mobile', 'like', "%".$search['mobile']."%");
        }
        if(isset($search['id'])){
            $sql->where('sbk.id', $search['id']);
        }
        if(isset($search['store_id'])){
            $sql->where('sbk.stores_id', $search['store_id']);
        }



        return $sql;
    }
    /**
     * 获取预订信息列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeBookList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->bookSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeBooks = $sql->orderBy('sbk.id', 'desc')->get();

        return $storeBooks;
    }

    /**
     * 获取预订信息列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeBookListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->bookSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户预订信息详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeBooksInfo($search){
        $sql = $this->bookSql($search);
        $storeBooks = $sql->first();
        return $storeBooks;
    }


    /**
     * 获取用户预定商户信息
     * @param $uid
     * @param $store_id
     * @return mixed
     */
    public function getUserBook($uid, $store_id){
        $storeBooksModel = new StoreBooks();
        return DB::table($storeBooksModel->getTable())
            ->where('users_id',$uid)
            ->where('stores_id', $store_id)
            ->where('status', 0)
            ->whereNull('delete_time')
            ->first();
    }

    /**
     * 预定余额支付
     * @param $bookId
     * @param $balance
     * @return bool|int
     */
    public function storeBookBalancePay($bookId, $balance){
        $bookOrderModel     = new StoreBookOrder();
        $bookModel          = new StoreBooks();
        $userModel          = new Users();
        $wallerLogModel     = new UserWalletLog();
        $userCouponModel    = new UserCoupons();
        $storeModel         = new Stores();

        $bookInfo = DB::table($bookOrderModel->getTable().' as bo')
            ->select('b.pay_amount', 'b.users_id', 'b.coupon_id', 'bo.*', 's.store_type_id', 'b.stores_id')
            ->leftJoin($bookModel->getTable().' as b', 'b.id', '=', 'bo.book_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'b.stores_id')
            ->where('bo.book_id', $bookId)
            ->first();




        if ($bookInfo->status > 0){
            return -1;
        }

        if ($bookInfo->pay_amount > $balance){
            return -2;
        }

        if ($bookInfo->store_type_id !== 3){
            return -3;
        }

        try{
            DB::beginTransaction();

            $qrcodeRedirectUrl = 'https://www.baidu.com'; 
            if (!is_dir(public_path() . '/qrcode/storebook')) {
                mkdir(public_path() . '/qrcode/storebook', 0775, true);
            }
            $pathName   = md5(time().$bookInfo->users_id);
            QrCode::format('png')->size(200)->margin(1)
                ->generate($qrcodeRedirectUrl, public_path("qrcode/storebook/{$pathName}.png"));

            if(!DB::table($bookModel->getTable())
                ->where('id', $bookInfo->book_id)
                ->update([
                    'qrcode'        => "/qrcode/storebook/{$pathName}.png"
                ])
            ){
                DB::rollBack();
                return false;
            }

            if (DB::table($bookOrderModel->getTable())
                ->where('book_id', $bookId)
                ->update([
                    'pay_type'      => 3,
                    'status'        => 1,
                    'pay_time'      => date('Y-m-d H:i:s', time())
                ]) === false
            ){
                DB::rollBack();
                return false;
            }

            if (!DB::table($userModel->getTable())
                ->where('id', $bookInfo->users_id)
                ->decrement('balance', $bookInfo->pay_amount)
            ){
                DB::rollBack();
                return false;
            }

            if(!DB::table($wallerLogModel->getTable())
                ->insert([
                    'users_id'      => $bookInfo->users_id,
                    'money'         => $bookInfo->pay_amount,
                    'balance'       => $balance,
                    'wallet_type'   => 1,
                    'stores_id'     => $bookInfo->stores_id,
                    'type'          => 3
                ])
            ){
                DB::rollBack();
                return false;
            }

            if (!empty($bookInfo->coupon_id)){
                if(DB::table($userCouponModel->getTable())
                        ->where('coupon_id', $bookInfo->coupon_id)
                        ->where('users_id', $bookInfo->users_id)
                        ->update(['is_use' => 1]) === false
                ){
                    DB::rollBack();
                    return false;
                }
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }

    public function storeBookWeChatPay($bookId){
        $weChatModel        = new WechatPay();
        $bookOrderModel     = new StoreBookOrder();
        $bookModel          = new StoreBooks();
        $storeModel         = new Stores();

        $outTradeNo = md5($bookId. time());
        $bookInfo = DB::table($bookModel->getTable().' as b')
            ->select('b.*', 'bo.status', 's.store_type_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'b.stores_id')
            ->leftJoin($bookOrderModel->getTable().' as bo', 'bo.book_id', '=', 'b.id')
            ->where('b.id', $bookId)
            ->first();

        if ($bookInfo->status > 0){
            return -1;
        }

        if ($bookInfo->store_type_id !== 3){
            return  -2;
        }
        $app = Factory::payment(\Config::get('zhongxin-pay.app'));
        $result = $app->order->unify([
            'body' => '新华联梦想城福街消费',
            'out_trade_no' => $outTradeNo,
            'total_fee' => $bookInfo->pay_amount,
            'spbill_create_ip' => get_server_ip(),
            'notify_url' => env('APP_URL').'/pluto/store/book/pay/wechat/notify',
            'trade_type' => 'APP'
        ]);
        \Log::debug($result);
        try{
            if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS'){
                DB::beginTransaction();

                $weChatInfo = DB::table($weChatModel->getTable())->where('orders_id', $bookId)->first();

                if (!$weChatInfo){
                    if(!DB::table($weChatModel->getTable())->insert([
                        'orders_id'       => $bookId,
                        'out_trade_no'  => $outTradeNo,
                        'type'          => 3
                    ])
                    ){
                        DB::rollBack();
                        return false;
                    }
                }else{
                    if(!DB::table($weChatModel->getTable())
                        ->where('book_id', $bookId)
                        ->update([
                            'out_trade_no'  => $outTradeNo,
                        ])
                    ){
                        DB::rollBack();
                        return false;
                    }
                }



                if(DB::table($bookOrderModel->getTable())
                        ->where('book_id', $bookId)
                        ->update([
                            'pay_type'      => 1
                        ]) === false
                ){
                    DB::rollBack();
                    return false;
                }

                DB::commit();
                return $app->jssdk->appConfig($result['prepay_id']);
            }else{
                return false;
            }
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }
    }

    /**
     * APP预订微信支付回调
     * @param $app
     * @return mixed
     */
    public function storeBookWeChatPayNotify($app){
        $response = $app->handlePaidNotify(function ($message, $fail) {

            if ($message['return_code'] === 'SUCCESS') {
                if (array_get($message, 'result_code') === 'SUCCESS') {

                    $weChatModel            = new WechatPay();
                    $bookOrderModel         = new StoreBookOrder();
                    $userCouponModel        = new UserCoupons();
                    $bookModel              = new StoreBooks();

                    $bookInfo = DB::table($weChatModel->getTable().' as w')
                        ->select('w.*', 'b.users_id', 'b.coupon_id')
                        ->leftJoin($bookModel->getTable().' as b','b.id', '=', 'w.orders_id')
                        ->where('out_trade_no', $message['out_trade_no'])
                        ->first();

                    try{
                        DB::beginTransaction();

                        $qrcodeRedirectUrl = 'https://www.baidu.com';
                        if (!is_dir(public_path() . '/qrcode/storebook')) {
                            mkdir(public_path() . '/qrcode/storebook', 0775, true);
                        }
                        $pathName   = md5(time().$bookInfo->users_id);
                        QrCode::format('png')->size(200)->margin(1)
                            ->generate($qrcodeRedirectUrl, public_path("qrcode/storebook/{$pathName}.png"));

                        if(!DB::table($bookModel->getTable())
                            ->where('id', $bookInfo->orders_id)
                            ->update([
                                'qrcode'        => "/qrcode/storebook/{$pathName}.png"
                            ])
                        ){
                            DB::rollBack();
                            return false;
                        }

                        if(DB::table($weChatModel->getTable())
                                ->where('out_trade_no', $message['out_trade_no'])
                                ->update(['out_trade_no' => $message['out_trade_no']]) === false
                        ){
                            DB::rollBack();
                            return false;
                        }

                        if(!DB::table($bookOrderModel->getTable())
                            ->where('book_id', $bookInfo->orders_id)
                            ->update([
                                'status'        => 1,
                                'pay_time'      => date('Y-m-d H:i:s', time())
                            ])
                        ){
                            DB::rollBack();
                            return false;
                        }

                        if (!empty($bookInfo->coupon_id)){
                            if(DB::table($userCouponModel->getTable())
                                    ->where('coupon_id', $bookInfo->orders_id)
                                    ->where('users_id', $bookInfo->users_id)
                                    ->update(['is_use' => 1]) === false
                            ){
                                DB::rollBack();
                                return false;
                            }
                        }

                        DB::commit();
                        return true;
                    }catch(\Exception $exception){
                        DB::rollBack();
                        \Log::error($exception);
                        return false;
                    }
                }elseif (array_get($message, 'result_code') === 'FAIL') {
                    return $fail('支付失败，请稍后再通知我');
                }

            }else {
                return $fail('通信失败，请稍后再通知我');
            }

        });

        return $response;

    }

    /**
     * 预定支付宝支付
     * @param $bookId
     * @return bool|int|\Symfony\Component\HttpFoundation\Response
     */
    public function storeBookAliPay($bookId){
        $aliPayModel        = new Alipay();
        $bookOrderModel     = new StoreBookOrder();
        $bookModel          = new StoreBooks();
        $storeModel         = new Stores();

        $outTradeNo = md5($bookId. time());
        $bookInfo = DB::table($bookModel->getTable().' as b')
            ->select('b.*', 'bo.status', 's.store_type_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'b.stores_id')
            ->leftJoin($bookOrderModel->getTable().' as bo', 'bo.book_id', '=', 'b.id')
            ->where('b.id', $bookId)
            ->first();

        if ($bookInfo->status > 0){
            return -1;
        }

        if ($bookInfo->store_type_id !== 3){
            return  -2;
        }

        try{
            DB::beginTransaction();

            $aliPayInfo = DB::table($aliPayModel->getTable())->where('orders_id', $bookId)->first();

            if (!$aliPayInfo){
                if(!DB::table($aliPayModel->getTable())->insert([
                        'orders_id'         => $bookId,
                        'out_trade_no'      => $outTradeNo,
                        'type'              => 3
                    ])
                ){
                    DB::rollBack();
                    return false;
                }
            }else{
                if(!DB::table($aliPayModel->getTable())
                    ->where('book_id', $bookId)
                    ->update([
                        'out_trade_no'  => $outTradeNo,
                    ])
                ){
                    DB::rollBack();
                    return false;
                }
            }



            if(DB::table($bookOrderModel->getTable())
                ->where('book_id', $bookId)
                ->update([
                    'pay_type'      => 2
                ]) === false
            ){
                DB::rollBack();
                return false;
            }


            $data = [
                'subject'      => '新华联梦想城福街消费',
                'out_trade_no' => $outTradeNo,
                'total_amount' => priceIntToFloat($bookInfo->pay_amount),
            ];

            DB::commit();
            return Pay::alipay(config('ali-pay.book'))->app($data)->getContent();
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }

    /**
     * 预定支付宝支付回调
     * @param $notifyData
     * @return bool
     */
    public function storeBookAliPayNotify($notifyData){
        \Log::debug('支付宝回调信息：'.json_encode($notifyData));

        if (
            $notifyData->trade_status == 'TRADE_SUCCESS'
            ||
            $notifyData->trade_status == 'TRADE_FINISHED ')
        {
            $bookOrderModel         = new StoreBookOrder();
            $userCouponModel        = new UserCoupons();
            $bookModel              = new StoreBooks();
            $aliPayModel            = new Alipay();

            $bookInfo = DB::table($aliPayModel->getTable().' as ba')
                ->select('ba.*', 'b.users_id', 'b.coupon_id')
                ->leftJoin($bookModel->getTable().' as b','b.id', '=', 'ba.orders_id')
                ->where('out_trade_no', $notifyData->out_trade_no)
                ->first();
            try{
                DB::beginTransaction();

                $qrcodeRedirectUrl = 'https://www.baidu.com';
                if (!is_dir(public_path() . '/qrcode/storebook')) {
                    mkdir(public_path() . '/qrcode/storebook', 0775, true);
                }
                $pathName   = md5(time().$bookInfo->users_id);
                QrCode::format('png')->size(200)->margin(1)
                    ->generate($qrcodeRedirectUrl, public_path("qrcode/storebook/{$pathName}.png"));

                if(!DB::table($bookModel->getTable())
                    ->where('id', $bookInfo->orders_id)
                    ->update([
                        'qrcode'        => "/qrcode/storebook/{$pathName}.png"
                    ])
                ){
                    DB::rollBack();
                    return false;
                }

                if(DB::table($aliPayModel->getTable())
                    ->where('out_trade_no', $notifyData->out_trade_no)
                    ->update(['trade_no' => $notifyData->trade_no]) === false
                ){
                    DB::rollBack();
                    return false;
                }

                if(!DB::table($bookOrderModel->getTable())
                    ->where('book_id', $bookInfo->orders_id)
                    ->update([
                        'status'        => 1,
                        'pay_time'      => date('Y-m-d H:i:s', time())
                    ])
                ){
                    DB::rollBack();
                    return false;
                }

                if (!empty($bookInfo->coupon_id)){
                    if(DB::table($userCouponModel->getTable())
                        ->where('coupon_id', $bookInfo->orders_id)
                        ->where('users_id', $bookInfo->users_id)
                        ->update(['is_use' => 1]) === false
                    ){
                        DB::rollBack();
                        return false;
                    }
                }

                DB::commit();
                return true;
            }catch(\Exception $exception){
                DB::rollBack();
                \Log::error($exception);
                return false;
            }

        }
    }


    public function getBookRefund($storeId, $status, $page, $pageSize){
        $bookOrderModel         = new StoreBookOrder();
        $bookRefundModel        = new StoreBookRefund();
        $bookModel              = new StoreBooks();

        $sql = DB::table($bookRefundModel->getTable().' as br')
            ->select(
                'b.id',
                'b.book_time',
                'b.mobile',
                'b.name',
                DB::raw("convert(pay_amount/100, decimal(15,2)) as pay_amount"),
                DB::raw("convert(coupon_discount_amount/100, decimal(15,2)) as coupon_discount_amount"),
                DB::raw("convert(activity_discount_amount/100, decimal(15,2)) as activity_discount_amount"),
                'br.cause',
                'br.image',
                'br.status'
            )
            ->leftJoin($bookOrderModel->getTable().' as bo', 'br.book_id', '=', 'bo.book_id')
            ->leftJoin($bookModel->getTable().' as b', 'b.id', '=', 'br.book_id')
            ->where('b.stores_id', $storeId);

        if (isset($status)){
            $sql->where('br.status', $status);
        }

        $total = $sql->count();
        $data = $sql->get();
        foreach ($data as $item) {
            $item->image = explode(',', $item->image);
        }

        return [
            'list'          => $data,
            'pagination'    => [
                'total'     => $total,
                'current'   => $page,
                'pageSize'  => $pageSize
            ]
        ];
    }


    public function updateBookRefund($id, $status, $refundAmount, $remark){
        $storeBookRefundModel               = new StoreBookRefund();

        return DB::table($storeBookRefundModel->getTable())
            ->where('id', $id)
            ->update([
                'refund_amount'         => $refundAmount,
                'remark'                => $remark,
                'status'                => $status
            ]);

    }

}
