<?php
namespace App\Blocks;

use App\Models\AgentTurnover;
use App\Models\AgentTurnoverAddress;
use App\Models\AgentUser;
use App\Models\CollectGoods;
use App\Models\Goods;
use App\Models\GoodsClassifies;
use App\Models\GoodsImages;
use App\Models\GoodsSku;
use App\Models\GoodsSkuName;
use App\Models\GoodsSkuValue;
use App\Models\GoodsTagsMiddle;
use App\Models\GroupBuyUsers;
use App\Models\StoreActivity;
use App\Models\StoreAddress;
use App\Models\StoreCoupons;
use App\Models\StoreNavs;
use App\Models\GroupBuy;
use App\Models\UserCoupons;
use App\Models\Users;
use DB;
use App\Models\Spike;
use App\Models\Stores;

class AgentGoodsBlock{
    private function __goodsSql($search){
        $goodsModel             = new Goods();
        $goodsClassifyModel     = new GoodsClassifies();
        $agentModel             = new AgentUser();
//        $collectGoodsModel      = new CollectGoods();


        $prefix = env('DB_PREFIX');
        $sql = DB::table($goodsModel->getTable() . ' as g')
            ->select(
                'g.id',
                'g.classify_id',
                'g.agent_id',
                'g.name',
                'g.slogan',
                'g.enable_sku',
                'g.service',
                'g.params',
                'g.status',
                'g.thumbnail',
                DB::raw("convert({$prefix}g.selling_price/100, decimal(15,2)) as selling_price"),
                DB::raw("convert({$prefix}g.purchase_price/100, decimal(15,2)) as purchase_price"),
                DB::raw("convert({$prefix}g.original_price/100, decimal(15,2)) as original_price"),
                'g.sales_volume',
                'g.mouth_sales_volume',
                'g.stock',
                'g.user_volume',
                'g.create_time',
                'gc.name as class_name',
                'gc.icon as class_icon',
                'au.agent_type',
                'au.company'

            )
            ->leftJoin($goodsClassifyModel->getTable() . ' as gc', 'g.classify_id', '=', 'gc.id')
            ->leftJoin($agentModel->getTable() . ' as au', 'au.id', '=', 'g.agent_id')
            ->whereNull('g.delete_time')
            ->where('au.agent_type', 0)
            ->where('g.agent_id', '<>', 0);
//        if(isset($search['collect']) && $search['collect'] === 1){
//            $sql->leftJoin($collectGoodsModel->getTable() . ' as cg', 'cg.goods_id', '=', 'g.id');
//            if(isset($search['user_id'])) {
//                $sql->where('cg.users_id', $search['user_id']);
//            }
//            if(isset($search['stype'])){
//                $sql->where('s.store_type_id', $search['stype']);
//            }
//        }


//        if (isset($search['operate'])){
//            $sql->where('s.operate', $search['operate']);
//        }

        if(isset($search['name'])){
            $sql->where('g.name', 'like', "%".$search['name']."%");
        }

        if(isset($search['cid'])){
            $sql->where('g.classify_id', $search['cid']);
        }

        if(isset($search['id'])){
            $sql->where('g.id', $search['id']);
            $sql->addSelect('g.detail');
        }

        if(isset($search['company'])){
            $sql->where('g.company_id', $search['company']);
        }
        if(isset($search['agent_id'])){
            $sql->where('g.agent_id', $search['agent_id']);
        }
        return $sql;
    }

    /**
     * 获取商品列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function goodsList($search, $page = 1, $pageSize = 20, $asc = [], $desc = []){

        $sql = $this->__goodsSql($search);
        if (isset($search['field']) && isset($search['sequence'])){
            $sql->orderBy('g.'.$search['field'], $search['sequence']);
        }

        if($page > 0){
            $sql->skip(($page - 1) * $pageSize)->take($pageSize);
        }
        $goods = $sql->groupBy('g.id')
            ->get();

        return $goods;
    }

    public function goodsListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__goodsSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    public function goodsGroupUser($group_id){
        $groupUserModel = new GroupBuyUsers();
        $usersModel = new Users();

        $userInfo = DB::table($groupUserModel->getTable().' as gu')
            ->select('u.avatar')
            ->leftJoin($usersModel->getTable().' as u','u.id','=','gu.users_id')
            ->where('group_buy_id', $group_id)
            ->whereNull('u.delete_time')
            ->get();

        return json_decode(json_encode($userInfo),true);
    }

    /**
     * 获取商品分类列表
     * @param $pid
     * @param $companyId
     */
    public function goodsClassify($pid=null,$companyId){
        if (!$pid){
            $pid = 0;
        }
        $classify = GoodsClassifies::where('parent_id', $pid)
            ->where('company_id', $companyId)
            ->whereNull('delete_time')
            ->get();
        return $classify;
    }

    /**
     * 获取商品详情
     * @param $id
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function goodsInfo($id, $userId){
        $sql = $this->__goodsSql(['id' => $id]);
        $goods = $sql->first();

        if($goods){
            //获取商品图片
            $goods->images = [];
            $goodsImage = GoodsImages::where('goods_id', $id)->get();
            foreach ($goodsImage as $image){
                $goods->images[] = $image;
            }

            $goodsSkuNameModel = new GoodsSkuName();
            $goodsSkuValueModel = new GoodsSkuValue();
            $godosSkuModel = new GoodsSku();
            $groupModel = new GroupBuy();
            $spikeModel = new Spike();

            //获取商品规格
            $skuName = DB::table($goodsSkuNameModel->getTable())
                ->select(
                    'id',
                    'name',
                    'goods_id',
                    'has_thumbnail'
                )->where('goods_id', $id)->get()->toArray();
            $skuValue = DB::table($goodsSkuValueModel->getTable())->where('goods_id', $id)->get()->toArray();

            $goods->sku = [];
            foreach ($skuName as $name){
                $name->skuValue = [];
                foreach ($skuValue as $value){
                    if($value->name_id == $name->id) {
                        $value->selected = 0;
                        $name->skuValue[] = $value;
                    }
                }
                $goods->sku[] = $name;
            }

            $goodsSkuData = DB::table($godosSkuModel->getTable())
                ->select(
                    'id',
                    'value_path',
                    'sales_volume',
                    'stock',
                    'thumbnail',
                    DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                    DB::raw("convert(purchase_price/100, decimal(15,2)) as purchase_price"),
                    DB::raw("convert(original_price/100, decimal(15,2)) as original_price")
                )
                ->where('goods_id', $id)->get();

            $goods->skuData = [];
            foreach ($goodsSkuData as $gSku){
                $gSku->value_path = rtrim($gSku->value_path, ',');
                $goods->skuData[$gSku->value_path] = $gSku;
            }

            $goods->service = json_decode( $goods->service, true);

            $goods->tags = [];
            $goodsTags = GoodsTagsMiddle::where('goods_id', $id)->get();
            foreach ($goodsTags as $tag){
                $goods->tags[] = $tag;
            }

            if ($userId){
                $collectGoodsModel = new CollectGoods();
                $isCollect = DB::table($collectGoodsModel->getTable())
                    ->where('goods_id',$id)
                    ->where('users_id',$userId)->first();

                if (isset($isCollect->id)){
                    $goods->is_collect=1;
                }else{
                    $goods->is_collect=0;
                }
            }else{
                $goods->is_collect=0;
            }
//            商品参数
            if ($goods->params){
                $goods->good_params = json_decode($goods->params, true);
            }else{
                $goods->good_params[] = [];
            }
        }

        return $goods;
    }

    /**
     * 获取商品详情(houtai)
     * @param $id
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function goodsInfoAdmin($id, $userId){
        $sql = $this->__goodsSql(['id' => $id]);
        $goods = $sql->first();

        if($goods){
            //获取商品图片
            $goods->images = [];
            $goodsImage = GoodsImages::where('goods_id', $id)->get();
            foreach ($goodsImage as $image){
                $goods->images[] = $image;
            }

            $goodsSkuNameModel = new GoodsSkuName();
            $goodsSkuValueModel = new GoodsSkuValue();
            $godosSkuModel = new GoodsSku();
            $groupModel = new GroupBuy();
            $spikeModel = new Spike();

            //获取商品规格
            $skuName = DB::table($goodsSkuNameModel->getTable())
                ->select(
                    'id',
                    'name',
                    'goods_id',
                    'has_thumbnail'
                )->where('goods_id', $id)->get()->toArray();
            $skuValue = DB::table($goodsSkuValueModel->getTable())->where('goods_id', $id)->get()->toArray();

            $goods->sku = [];
            foreach ($skuName as $name){
                $name->skuValue = [];
                foreach ($skuValue as $value){
                    if($value->name_id == $name->id) {
                        $value->selected = 0;
                        $name->skuValue[] = $value;
                    }
                }
                $goods->sku[] = $name;
            }

            $goodsSkuData = DB::table($godosSkuModel->getTable())
                ->select(
                    'id',
                    'value_path',
                    'sales_volume',
                    'stock',
                    'thumbnail',
                    DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                    DB::raw("convert(purchase_price/100, decimal(15,2)) as purchase_price"),
                    DB::raw("convert(original_price/100, decimal(15,2)) as original_price")
                )
                ->where('goods_id', $id)->get();

            $goods->skuData = [];
            foreach ($goodsSkuData as $gSku){
                $gSku->value_path = rtrim($gSku->value_path, ',');
                $goods->skuData[] = $gSku;
            }

            $group = DB::table($groupModel->getTable())->where('goods_id',$id)->whereNull('delete_time')
                ->value('people');
            if ($group){
                $goods->is_group = true;
                $goods->people = $group;
            }

            $spike = DB::table($spikeModel->getTable())->where('goods_id',$id)
                ->whereNull('delete_time')->get();

            if (!$spike->isEmpty()){
                $goodsSpike = [];
                foreach ($spike as $item) {
                    $goodsSpike['spike_selling_price'] = priceIntToFloat($item->selling_price);
                    $goodsSpike['day'][] = $item->day;
                    $goodsSpike['hour'][] = $item->hour;
                    $goodsSpike['stock'][] = $item->stock;
                }

                $goods->is_spike = true;
                $goods->spike_selling_price = $goodsSpike['spike_selling_price'];
                $goods->day = array_values(array_unique($goodsSpike['day']));
                $goods->day = [$goods->day[0],end($goods->day)];
                $goods->hour = array_values(array_unique($goodsSpike['hour']));
                $goods->spike_stock = array_unique($goodsSpike['stock']);
            }

            $goods->service = json_decode($goods->service, true);

            $goods->tags = [];
            $goodsTags = GoodsTagsMiddle::where('goods_id', $id)->get();
            foreach ($goodsTags as $tag){
                $goods->tags[] = $tag;
            }

            if ($userId){
                $collectGoodsModel = new CollectGoods();
                $isCollect = DB::table($collectGoodsModel->getTable())
                    ->where('goods_id',$id)
                    ->where('users_id',$userId)->first();

                if (isset($isCollect->id)){
                    $goods->is_collect = 1;
                }else{
                    $goods->is_collect = 0;
                }
            }else{
                $goods->is_collect = 0;
            }
            if ($goods->params){
                $goods->good_params = json_decode($goods->params, true);
            }else{
                $goods->good_params[] = [];
            }


        }

        return $goods;
    }

    /**
     * 添加商品
     */
    public function addGoods(array $data, array $sku=[], array $skuData=[], array $images=[], $group, $spike, $tags){
        $goodsModel             = new Goods();
        $goodsSkuModel          = new GoodsSku();
        $goodsSkuValueModel     = new GoodsSkuValue();
        $goodsSkuNameModel      = new GoodsSkuName();
        $goodsImagesModel       = new GoodsImages();
        $groupModel             = new GroupBuy();
        $spikeModel             = new Spike();
        $goodsTagsMiddleModel   = new GoodsTagsMiddle();

        try{
            DB::beginTransaction();
            //添加商品的基本数据
            $goodsId = DB::table($goodsModel->getTable())->insertGetId($data);
            if(!$goodsId){
                \Log::error('添加商品基本数据失败');
                return false;
            }

            //添加商品的图片数据
            if(!empty($images)) {
                $goodsImagesData = [];
                foreach ($images as $img) {

                    $goodsImagesData[] = [
                        'goods_id' => $goodsId,
                        'image' => $img,
                        'thumbnail' => $img,
                    ];
                }
                if (
                !DB::table($goodsImagesModel->getTable())->insert($goodsImagesData)
                ) {
                    DB::rollBack();
                    \Log::error('添加商品图片失败');
                    return false;
                }
            }

            if(!empty($sku) && !empty($skuData)) {
                //添加规格
                $combinationSku = $this->combinationSku($goodsId, $sku, $skuData);
                if (
                    !DB::table($goodsSkuNameModel->getTable())->insert($combinationSku['name'])
                    ||
                    !DB::table($goodsSkuValueModel->getTable())->insert($combinationSku['value'])
                    ||
                    !DB::table($goodsSkuModel->getTable())->insert($combinationSku['sku'])
                ) {
                    DB::rollBack();
                    \Log::error('添加商品规格失败');
                    return false;
                }
            }

            if ($group['is_group']){
                if (
                    !DB::table($groupModel->getTable())->insert([
                    'goods_id' => $goodsId,
                    'people' => $group['people']
                ])){
                    DB::rollBack();
                    return false;
                }
            }

            if ($spike['is_spike']){
                foreach ($spike['spikeData'] as &$value) {
                    $value['goods_id'] = $goodsId;
                    $value['create_time'] = date('Y-m-d H:i:s',time());
                    $value['selling_price'] = $value['selling_price'] * 100;
                }

                if(
                    !DB::table($spikeModel->getTable())->insert($spike['spikeData'])
                ){
                    DB::rollBack();
                    return false;
                }
            }

            //添加商品的标签数据
            $goodsTagsData = [];
            foreach ($tags as $v){
                $goodsTagsData[] = [
                    'goods_id'      => $goodsId,
                    'tag_id'        => $v,
                ];
            }
            if(
            !DB::table($goodsTagsMiddleModel->getTable())->insert($goodsTagsData)
            ){
                DB::rollBack();
                \Log::error('添加商品标签失败');
                return false;
            }

            DB::commit();
            return $goodsId;

        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }


    /**
     * 修改商品
     */
    public function updateGoods(array $data, array $sku=[], array $skuData=[], array $images=[], array $tags=[], $spike=[], $group=[]){
        $goodsModel             = new Goods();
        $goodsSkuModel          = new GoodsSku();
        $goodsSkuValueModel     = new GoodsSkuValue();
        $goodsSkuNameModel      = new GoodsSkuName();
        $goodsImagesModel       = new GoodsImages();
        $goodsTagsMiddleModel   = new GoodsTagsMiddle();
        $spikeModel             = new Spike();
        $groupBuyModel          = new GroupBuy();

        DB::beginTransaction();
        try{
            //修改商品的基本数据
            $res = DB::table($goodsModel->getTable())
                ->where('id',$data['id'])
                ->update($data);
            if(!$res){
                \Log::error('添加商品基本数据失败');
                return false;
            }

            //修改商品的图片数据
            //删除旧的图片
            $delImages= $goodsImagesModel->del($data['id']);

            if ($delImages===false){
                DB::rollBack();
                \Log::error('删除旧商品图片失败');
                return false;
            }
            //添加新的图片
            $goodsImagesData = [];
            foreach ($images as $img){
                $goodsImagesData[] = [
                    'goods_id'      => $data['id'],
                    'image'         => $img,
                    'thumbnail'     => $img,
                ];
            }
            if(
            !DB::table($goodsImagesModel->getTable())->insert($goodsImagesData)
            ){
                DB::rollBack();
                \Log::error('添加商品图片失败');
                return false;
            }

            //修改规格
            //删除旧的规格
            $delSku         = GoodsSku::where('goods_id', $data['id'])->delete();
            $delSkuName     = GoodsSkuName::where('goods_id', $data['id'])->delete();
            $delSkuValue    = GoodsSkuValue::where('goods_id', $data['id'])->delete();
            if(
                $delSkuValue ===false
                ||
                $delSkuName===false
                ||
                $delSku===false
            ){
                DB::rollBack();
                \Log::error('添加商品规格失败');
                return false;
            }
            //添加新规格
            $combinationSku = $this->combinationSku($data['id'], $sku, $skuData);
            if(
                !DB::table($goodsSkuNameModel->getTable())->insert($combinationSku['name'])
                ||
                !DB::table($goodsSkuValueModel->getTable())->insert($combinationSku['value'])
                ||
                !DB::table($goodsSkuModel->getTable())->insert($combinationSku['sku'])
            ){
                DB::rollBack();
                \Log::error('添加商品规格失败');
                return false;
            }


            //修改商品的标签数据
            //删除旧的标签
            $delTags = DB::table($goodsTagsMiddleModel->getTable())->where('goods_id', $data['id'])->delete();

            if ($delTags===false){
                DB::rollBack();
                \Log::error('删除商品旧标签失败');
                return false;
            }
            //添加新的标签
            $goodsTagsData = [];
            foreach ($tags as $v){
                $goodsTagsData[] = [
                    'goods_id'      => $data['id'],
                    'tag_id'        => $v,
                ];
            }
            if(
            !DB::table($goodsTagsMiddleModel->getTable())->insert($goodsTagsData)
            ){
                DB::rollBack();
                \Log::error('添加商品标签失败');
                return false;
            }

            //秒杀
            $isSpike = DB::table($spikeModel->getTable())->where('goods_id',$data['id'])->get();
            if (!$isSpike->isEmpty()){
                if (
                !DB::table($spikeModel->getTable())->where('goods_id', $data['id'])->update([
                    'delete_time' => date('Y-m-d H:i:s', time())
                ])
                ) {
                    DB::rollBack();
                    \Log::debug(1);
                    return false;
                }
            }



            if ($spike['is_spike']) {


                foreach ($spike['spikeData'] as &$value) {
                    $value['create_time'] = date('Y-m-d H:i:s', time());
                    $value['selling_price'] = $value['selling_price'] * 100;
                }

                if (
                !DB::table($spikeModel->getTable())->insert($spike['spikeData'])
                ) {
                    DB::rollBack();
                    \Log::debug(2);
                    return false;
                }

            }

            $isGroup = DB::table($groupBuyModel->getTable())->where('goods_id',$data['id'])
                ->whereNull('delete_time')
                ->get();
            //团购
            if ($group['is_group'] == true){

                if (!$isGroup->isEmpty()){

                    if (
                    DB::table($groupBuyModel->getTable())->where('goods_id',$data['id'])->update([
                        'people' => $group['people']
                    ]) === false
                    ){
                        DB::rollBack();
                        \Log::debug(3);
                        return false;
                    }
                }else{
                    if (!DB::table($groupBuyModel->getTable())->insert([
                        'goods_id'  => $data['id'],
                        'people'    => $group['people']
                    ])){
                        DB::rollBack();
                        \Log::debug(5);
                        return false;
                    }

                }


            }else{

                if (!$isGroup->isEmpty()){
                    if (
                    !DB::table($groupBuyModel->getTable())->where('goods_id',$data['id'])->update([
                        'delete_time' => date('Y-m-d H:i:s',time())
                    ])
                    ){
                        DB::rollBack();
                        \Log::debug(4);
                        return false;
                    }
                }


            }


            DB::commit();
            return $data['id'];
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }

    /**
     * 组合sku的name 和 value
     * @param $goodsId
     * @param array $sku
     * @param array $skuData
     */
    private function combinationSku($goodsId, array $sku, array $skuData){
        $skuNames   = [];
        $skuValues  = [];
        $skus       = [];
        for ($i = 0; $i < count($skuData); $i++){
            $nameId = $goodsId.'-'.($i+1);
            $skuNames[] = [
                'goods_id'      => $goodsId,
                'name'          => $skuData[$i]['sku'],
                'id'            => $nameId,
                'has_thumbnail' => $this->JudgeHasThumbnail($skuData[$i]['thumbnail'])
            ];

            for ($j = 0; $j < count($skuData[$i]['name']); $j++){
                $valueId = $nameId.'-'.($j+1);
                $skuValues[] = [
                    'id'        => $valueId,
                    'goods_id'  => $goodsId,
                    'name_id'   => $nameId,
                    'value'     => $skuData[$i]['name'][$j],
                    'thumbnail' => $skuData[$i]['thumbnail'][$j]
                ];
            }
        }


        //组合sku
        $skuItemNum = 0;
        foreach ($sku as $skuItem) {
            $skus[$skuItemNum] = [
                'purchase_price'    => $skuItem['purchase_price']*100,
                'original_price'    => $skuItem['original_price']*100,
                'selling_price'     => $skuItem['selling_price']*100,
                'stock'         => $skuItem['stock'],
                'value_path'    => '',
                'goods_id'      => $goodsId
            ];

            foreach ($skuItem['value'] as $skuItemValue) {
                foreach ($skuValues as $skuValue) {
                    if ($skuItemValue === $skuValue['value']) {
                        $skus[$skuItemNum]['value_path'] .= $skuValue['id'].',';

                    }
                }
            }

            $skus[$skuItemNum]['value_path'] = substr($skus[$skuItemNum]['value_path'],0,strlen($skus[$skuItemNum]['value_path'])-1);

            $skuItemNum += 1;
        }

        return [
            'name'      => $skuNames,
            'value'     => $skuValues,
            'sku'       => $skus
        ];
    }

    private function JudgeHasThumbnail($data){
        foreach ($data as $value) {
            if ($value != null && $value != ''){
                return 1;
            }else{
                return 0;
            }
        }
    }

    /**
     * 后台获取商品分类
     * @param $classify
     * @return array
     */
    public function classifyList($classify){
        $pid = 0;
        $tree = $this->permissionsTree($classify, $pid);
        return $tree;
    }
    public function permissionsTree($data, $pid){

        $tree = [];
        foreach($data as $key => $value){
            if($value->parent_id == $pid){
                $value->children = $this->permissionsTree($data, $value->id);
                $tree[] = $value;

                unset($data[$key]);
            }
        }
        return $tree;
    }

    /**
     * 一键复制商品
     * @param $type
     * @param $id
     * @param $goodsId
     * @return bool
     */
    public function copyGoods($type, $id, $goodsId){
        $goodsModel             = new Goods();
        $goodsSkuModel          = new GoodsSku();
        $goodsSkuValueModel     = new GoodsSkuValue();
        $goodsSkuNameModel      = new GoodsSkuName();
        $goodsImagesModel       = new GoodsImages();
        //商品基本信息
        $goods = DB::table($goodsModel->getTable())->where('id', $goodsId)->first();
        //商品规格信息
        $goodsSku = DB::table($goodsSkuModel->getTable())->where('goods_id', $goodsId)->get()->toArray();
        $goodsSkuName = DB::table($goodsSkuNameModel->getTable())->where('goods_id', $goodsId)->get()->toArray();
        $goodsSkuValueName = DB::table($goodsSkuValueModel->getTable())->where('goods_id', $goodsId)->get()->toArray();
        $skuData = [];

        foreach ($goodsSkuName as $k=>$v){
            $skuData[$k]['sku'] = $v->name;
            $skuData[$k]['name'] = [];
            $skuData[$k]['thumbnail'] = [];
            foreach ($goodsSkuValueName as $key=>$val){
                if ($val->name_id == $v->id){
                    $skuData[$k]['name'][] = $val->value;
                    $skuData[$k]['thumbnail'][] = $val->thumbnail;
                }

            }
        }
        $skuValueData = [];
        foreach ($goodsSku as $k=>$v){
            $skuValueData[$k]['value'] = [];
            $skuValueData[$k]['original_price'] = $v->original_price;
            $skuValueData[$k]['selling_price'] = $v->selling_price;
            $skuValueData[$k]['purchase_price'] = $v->purchase_price;
            $skuValueData[$k]['stock'] = $v->stock;
            foreach ($goodsSkuValueName as $key=>$val){
                if (strpos($v->value_path, $val->id) !== false){
                    $skuValueData[$k]['value'][] = $val->value;

                }

            }
        }

        //商品图片信息
        $goodsImage = DB::table($goodsImagesModel->getTable())->where('goods_id', $goodsId)->get()->toArray();
        $images = [];
        foreach ($goodsImage as $key=>$val){
            $images[] = $val->image;
        }
        $data = [
            'name' => $goods->name,
            'slogan' => $goods->slogan,
            'thumbnail' => $goods->thumbnail,
            'selling_price' => $goods->selling_price,
            'purchase_price' => $goods->purchase_price,
            'original_price' => $goods->original_price,
            'stock' => $goods->stock,
            'detail' => $goods->detail,
            'sort' => $goods->sort,
            'classify_id' => $goods->classify_id,
            'status' => 2,
            'enable_sku' => $goods->enable_sku,
            'store_nav_id' => 0,
            'company_id' => $goods->company_id,
            'create_time' => date('Y-m-d H:i:s', time()),
            'params' => $goods->params,
            'service' => $goods->service
        ];
        //查看是什么身份的人复制商品
        if ($type == 'agent'){
            $data['agent_id'] = $id;
            $data['p_id'] = $goods->agent_id;
            $data['mill_id'] = $goods->agent_id;
            $data['stores_id'] = 0;

        }elseif ($type == 'store'){
            $data['agent_id'] = 0;
            $data['p_id'] = 0;
            $data['mill_id'] = 0;
            $data['stores_id'] = $id;
        }
        $goodsId    = $this->addGoods($data, $skuValueData, $skuData, $images, false, false, []);
        return $goodsId;

    }



    /**
     * 订单支付
     * @param $data
     */
    public function turnoverPay($data){
        $agentTurnoverModel = new AgentTurnover();

        try{
//          修改订单状态
            $res = DB::table($agentTurnoverModel->getTable())
                ->where('id', $data['turnover_id'])
                ->update([
                    'pay_type'  => $data['pay_type'],
                    'pay_time'  => $data['pay_time'],
                    'pay_image' => $data['pay_image'],
                    'status'    => 1,
                ]);
            if(!$res){
                DB::rollBack();
                \Log::error('修改订单状态失败');
                return false;
            }

            DB::commit();
            return $res;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }
}
