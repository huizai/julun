<?php
namespace App\Blocks;

use App\Exceptions\BlockException;
use App\Libs\DianWoDa;
use App\Models\AgentApply;
use App\Models\AgentTurnover;
use App\Models\Banners;
use App\Models\CarefullyChosenStore;
use App\Models\CollectMill;
use App\Models\CollectMillGoods;
use App\Models\CollectStores;
use App\Models\Goods;
use App\Models\GoodsClassifies;
use App\Models\GoodsEstimates;
use App\Models\GoodsTags;
use App\Models\GoodsTagsMiddle;
use App\Models\Message;
use App\Models\Orders;
use App\Models\PageNav;
use App\Models\StoreActivity;
use App\Models\StoreActivityTags;
use App\Models\StoreBookComment;
use App\Models\StoreBookOrder;
use App\Models\StoreBookRefund;
use App\Models\StoreBooks;
use App\Models\StoreBrand;
use App\Models\StoreClassifity;
use App\Models\StoreContract;
use App\Models\StoreCoupons;
use App\Models\StoreDevices;
use App\Models\StoreImage;
use App\Models\StoreInfo;
use App\Models\StoreLease;
use App\Models\StoreLeaseLog;
use App\Models\StoreNavs;
use App\Models\Stores;
use App\Models\StoreSeat;
use App\Models\StoreSeatLineup;
use App\Models\StoreSeatPosition;
use App\Models\StoreTags;
use App\Models\StoreTurnover;
use App\Models\StoreYunxin;
use App\Models\UserCoupons;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Libs\YunXin\ServerApi;


class StoreBlock{
    private function __storeSql(array $search, array $field = []){
        $storeModel             = new Stores();
        $storeInfoModel         = new StoreInfo();
        $storeClassifyModel     = new StoreClassifity();
        $ccStoreModel           = new CarefullyChosenStore();
        $collectStoreModel      = new CollectStores();
        $activityModel          = new StoreActivity();
        $goodsModel             = new Goods();
        $prefix = env('DB_PREFIX');
        $baseField = [
            's.id',
            's.classifity_id',
            's.name',
            's.logo',
            's.rating',
            's.user_volume',
            's.sales_volume',
            DB::raw("convert({$prefix}si.per_capita/100, decimal(15,2)) as per_capita"),
            'si.take_out',
            'si.longitude',
            'si.latitude',
            'sc.name as class_name',
            'sc.icon',
            's.create_time',
            's.is_lineup',
            's.cuisine_name as cuisine',
            's.store_type_id',
            DB::raw("convert({$prefix}s.delivery_fee/100, decimal(15,2)) as delivery_fee"),
            DB::raw("convert({$prefix}s.init_delivery_price/100, decimal(15,2)) as init_delivery_price")

        ];

        $field = array_merge($baseField, $field);

        $sql = DB::table($storeModel->getTable() . ' as s')
            ->select(
                $field
            )
            ->leftJoin($storeClassifyModel->getTable() . ' as sc', 's.classifity_id', '=', 'sc.id')
            ->leftJoin($storeInfoModel->getTable() . ' as si', 's.id', '=', 'si.store_id');


        if(isset($search['recommend'])){
            if($search['recommend'] == '精选好店') {
                $sql->join($ccStoreModel->getTable() . ' as cc', 'cc.stores_id', '=', 's.id');
                $sql->addSelect('cc.cover_image')
                    ->orderBy('cc.sort', 'asc');

            }
        }

        if(isset($search['collect'])){
            if($search['collect'] === 1) {
                $sql->leftJoin($collectStoreModel->getTable() . ' as cs', 'cs.stores_id', '=', 's.id');
                if(isset($search['user_id'])) {
                    $sql->where('cs.users_id', $search['user_id']);
                }
                if(isset($search['stype']) && $search['stype'] !== 3){
                    $sql->where('s.store_type_id', $search['stype']);
                }

                if (isset($search['stype']) && $search['stype'] === 3){
                    $sql->where('si.take_out', 1);
                }
            }
        }

        if (isset($search['activity']) && !empty($search['activity'])){
            $sql->leftJoin($activityModel->getTable() . ' as sa', 'sa.stores_id', '=', 's.id');
        }

        if (isset($search['goods']) && !empty($search['goods'])){
            $sql->leftJoin($goodsModel->getTable().' as g', 'g.stores_id', '=', 's.id');
        }

        if(isset($search['id'])){
            if(is_array($search['id'])){
                $sql->whereIn('s.id', $search['id']);
            }else {
                $sql->where('s.id', $search['id']);
            }
        }

        if(isset($search['stores_id'])){
            $sql->where('s.id', $search['id']);
        }



        $sql->whereNull('s.delete_time');

        if (isset($search['operate'])){
            $sql->where('s.operate', $search['operate']);
        }

        if(isset($search['cid'])){
            $sql->where('s.classifity_id', $search['cid']);
        }

        if(isset($search['name'])){
            $sql->where('s.name', 'like', "%".$search['name']."%");
        }

        if(isset($search['store_type'])){
            $sql->where('s.store_type_id', $search['store_type']);
        }

        if(isset($search['is_book'])){
            $sql->where('s.is_book', $search['is_book']);
        }
        if(isset($search['status'])){
            $sql->where('s.status', $search['status']);
        }

        if(isset($search['choice_store']) && $search['choice_store'] == 1){
            $sql->where('s.rating', 5);
        }

        if (isset($search['is_lineup'])){
            $sql->where('s.is_lineup', 1);
        }

        if (isset($search['cuisine']) && !empty($search['cuisine'])){
            $sql->where('s.cuisine_name', $search['cuisine']);
        }

        if (isset($search['field']) && isset($search['sequence'])){
            if ($search['field'] == 'rating' || $search['field'] == 'user_volume'){
                $sql->orderBy('s.'.$search['field'], $search['sequence']);
            }

            if ($search['field'] == 'id'){
                $sql->orderBy('s.sort', 'asc');
            }

            if ($search['field'] == 'sales_volume'){
                $sql->orderBy('s.user_volume', 'desc');
            }

        }

        if (
            isset($search['long'])
            &&
            !empty($search['long'])
            &&
            isset($search['lat'])
            &&
            !empty($search['lat'])
        ){
            $sql->addSelect(
                DB::raw("SQRT(
                    POW(111.2 * (latitude - {$search['lat']}), 2) 
                    +
                    POW(111.2 * ({$search['long']} - longitude) * COS(latitude / 57.3), 2))
                    
                    as distance
                ")
            );
        }


        if (isset($search['init_delivery_price']) && !empty($search['init_delivery_price'])){
            $sql->orderBy('s.init_delivery_price', 'desc');
        }

        if (isset($search['delivery_fee']) && !empty($search['delivery_fee'])){
            $sql->orderBy('s.delivery_fee', 'asc');
        }




        return $sql;
    }

    /**
     * 获取店铺列表
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return \Illuminate\Support\Collection
     */
    public function storeList(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__storeSql($search);


        if (isset($search['long']) && isset($search['lat'])){
            $sql->having('distance', '<', '10')->orderBy('distance', 'desc');
        }


        $sql->whereNull('s.delete_time')
            ->skip(($page - 1) * $pageSize)->take($pageSize);


        $store = $sql->get();

        $storeIds = [];
        foreach ($store as $s){
            $s->rating = sprintf("%.1f", ($s->rating));
            $storeIds[] = $s->id;
        }

        if (isset($search['is_lineup'])){
            $storeSearLineupModel = new StoreSeatLineup();
            $lineup = DB::table($storeSearLineupModel->getTable())
                ->where('is_use', 0)
                ->whereNull('delete_time')
                ->get();
        }

        //获取标签
        $storeTagModel = new StoreTags();
        $tags = DB::table($storeTagModel->getTable())->whereIn('stores_id', $storeIds)->get();

        //获取店铺优惠券
        $storeCoupon = new StoreCoupons();
        $coupons = DB::table($storeCoupon->getTable())->whereIn('stores_id', $storeIds)->get();

        //促销
        $storeActivityTagsModel = new StoreActivityTags();
        $promotion = DB::table($storeActivityTagsModel->getTable())->whereIn('stores_id', $storeIds)->get();

        //促销icon
        //减
        $lessImage    = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153708411.png';
        //买
        $buyImage     = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153713144.png';
        //特
        $bargainImage = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153716328.png';
        //新
        $newImage     = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153722068.png';

        //组装数据
        foreach ($store as $keys => $s){



            //标签
            $s->tags = [];
            $s->coupons = [];
            foreach ($tags as $t){
                if($t->stores_id == $s->id){
                    $s->tags[] = $t;
                }
            }
            foreach ($coupons as $c){
                if($c->stores_id == $s->id){
                    $s->coupons[] = $c;
                }
            }

            //促销
            $s->promotion = [];
            foreach ($promotion as $key => $item) {
                if ($s->id == $item->stores_id){
                    $s->promotion[$key]['title'] = $item->title;
                    switch ($item->type)
                    {
                        case 0:
                            $s->promotion[$key]['icon'] = $lessImage;
                            break;
                        case 1:
                            $s->promotion[$key]['icon'] = $buyImage;
                            break;
                        case 2:
                            $s->promotion[$key]['icon'] = $bargainImage;
                            break;
                        case 3:
                            $s->promotion[$key]['icon'] = $newImage;
                            break;
                    }
                }
            }

            $s->promotion = array_values($s->promotion);

            //排队
            if (isset($search['is_lineup']) && $search['is_lineup'] == 1 ){
                $lineupNumber = 0;
                foreach ($lineup as $item) {
                    if ($s->id == $item->stores_id){
                        $lineupNumber += 5;
                    }
                }
                $s->lineupMinute = $lineupNumber;
            }else if (isset($search['is_lineup']) && $search['is_lineup'] == 0){
                foreach ($lineup as $item) {
                    if ($s->id == $item->stores_id){
                        unset($store[$keys]);
                    }else{
                        $s->lineupMinute = 0;
                    }
                }
            }
        }


        return $store;
    }

    /**
     * 获取店铺列表的分页信息
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function storeListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__storeSql($search);
        if (isset($search['long']) && isset($search['lat'])){
            $sql->having('distance', '<', '10')->orderBy('distance', 'desc');
        }
        $data = $sql->get();
        $total = count($data);

        if (isset($search['is_lineup'])){
            $storeSearLineupModel = new StoreSeatLineup();
            $lineup = DB::table($storeSearLineupModel->getTable())
                ->where('is_use', 0)
                ->whereNull('delete_time')
                ->get();

            foreach ($data as $s) {
                if (isset($search['is_lineup']) && $search['is_lineup'] == 0){
                    foreach ($lineup as $item) {
                        if ($s->id == $item->stores_id){
                            $total -=1;
                        }
                    }
                }
            }
        }

        return [
            'total' => $total,
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    /**
     * 获取店铺详情
     * @param $id
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeInfo($id, $userId){
        $field = [
            'si.introduce',
            'si.address',
            'si.business_license',
            'si.food_license',
            'si.am',
            'si.pm',
            'si.contact',
            'si.take_out'
        ];
        $sql = $this->__storeSql(['id' => $id], $field);
        $store = $sql->first();
        if ($store){
            //获取商家提供的服务
            $storeDevicesModel      = new StoreDevices();
            $goodsModel             = new Goods();
            $storeEstimatesModel    = new GoodsEstimates();
            $storeModel             = new Stores();
            $device = DB::table($storeDevicesModel->getTable())
                ->select('name')
                ->where('stores_id', $id)
                ->whereNull('delete_time')
                ->get();
            $store->device = $device;
            $store->rating = sprintf("%.1f", $store->rating);
            $store->introduce_cut_str = julun_cut_str($store->introduce, 35);
            $store->take_image = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store/156497612073.png';
            //获取店铺优惠券
            $storeCoupon = new StoreCoupons();
            $userCoupon  = new UserCoupons();
            $coupons = DB::table($storeCoupon->getTable())->where('stores_id', $id)->get();
            foreach ($coupons as $k=>$v){
                if ($userId){
                    $isGet = DB::table($userCoupon->getTable())
                        ->where('users_id', $userId)
                        ->where('coupon_id', $v->id)
                        ->where('is_use', 0)
                        ->whereNull('delete_time')
                        ->first();
                    if (isset($isGet->id)){
                        $v->is_get=1;
                    }else{
                        $v->is_get=0;
                    }
                }else{
                    $v->is_get=0;
                }

            }
            $store->coupon = $coupons;

            //获取店铺活动
            $storeActivityModel = new StoreActivity();
            $storeActivity = DB::table($storeActivityModel->getTable())->where('stores_id', $id)->get();
            foreach ($storeActivity as $k=>$v){
                $v->activity = [];
                $data = json_decode($v->data, true);
                $data['full_amount']        = priceIntToFloat($data['full_amount']);
                $data['discount_amount']    = priceIntToFloat($data['discount_amount']);
                $v->activity[] = $data;
            }
            $store->store_activity = $storeActivity;
            if ($userId){
                $orderModel         = new Orders();
                $collectStoresModel = new CollectStores();
                //是否收藏该店铺
                $isCollect = DB::table($collectStoresModel->getTable())
                    ->where('stores_id',$id)
                    ->where('users_id',$userId)->first();

                if (isset($isCollect->id)){
                    $store->is_collect=1;
                }else{
                    $store->is_collect=0;
                }

                /**
                 * 当前是否有外卖订单
                 */
                $takeOrder = DB::table($orderModel->getTable())
                    ->where('users_id', $userId)
                    ->where('stores_id', $id)
                    ->where('status', 0)
                    ->where('is_online', 1)
                    ->orderBy('create_time', 'desc')
                    ->first();


                if ($takeOrder){
                    $store->take_orders_id  = $takeOrder->id;
                    $store->is_take_out     = 1;
                }else{
                    $store->is_take_out = 0;
                }

            }else{
                $store->is_collect = 0;
                $store->is_take_out = 0;
            }

            $storeBookModel = new StoreBooks();
            $isBook = DB::table($storeBookModel->getTable())
                ->where('users_id', $userId)
                ->where('stores_id', $id)
                ->where('status', 0)
                ->whereNull('delete_time')
                ->first();

            if ($isBook){
                $store->is_book = 1;
                $store->book_id = $isBook->id;
            }

            $storeLineupModel   = new StoreSeatLineup();
            $store->lineup_people = DB::table($storeLineupModel->getTable())
                ->where('stores_id', $id)
                ->where('is_use', 1)
                ->count();


            //促销
            $storeActivityTagsModel = new StoreActivityTags();
            $promotion = DB::table($storeActivityTagsModel->getTable())->where('stores_id', $id)->get();

            //促销icon
            //减
            $lessImage    = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153708411.png';
            //买
            $buyImage     = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153713144.png';
            //特
            $bargainImage = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153716328.png';
            //新
            $newImage     = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153722068.png';

            $store->promotion = [];
            foreach ($promotion as $key => $item) {
                if ($store->id == $item->stores_id){
                    $store->promotion[$key]['title'] = $item->title;
                    switch ($item->type)
                    {
                        case 0:
                            $store->promotion[$key]['icon'] = $lessImage;
                            break;
                        case 1:
                            $store->promotion[$key]['icon'] = $buyImage;
                            break;
                        case 2:
                            $store->promotion[$key]['icon'] = $bargainImage;
                            break;
                        case 3:
                            $store->promotion[$key]['icon'] = $newImage;
                            break;
                    }
                }
            }

            $store->promotion = array_values($store->promotion);

            $store->estimates_total = DB::table($storeEstimatesModel->getTable().' as se')
                ->select('se.id')
                ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'se.goods_id')
                ->where('g.stores_id', $store->id)
                ->count();


            DB::table($storeModel->getTable())->where('id', $id)
                ->increment('user_volume');
        }

        return $store;
    }

    /**
     * 我的排号信息
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function storeSeatLineUpList($userId){
        $storeSeatLineUpModel = new StoreSeatLineup();
        $lineup = DB::table($storeSeatLineUpModel->getTable())
            ->where('users_id', $userId)
            ->whereNull('delete_time')
            ->where('is_use', 0)
            ->get();

        $storeId = [];
        foreach ($lineup as $l){
            $storeId[] = $l->stores_id;
        }

        $store = $this->storeList(['id' => $storeId], 1, count($storeId));

        foreach ($lineup as $l){
            $l->store = null;
            foreach ($store as $s){
                if($l->stores_id === $s->id){
                    $l->store = $s;
                }
            }
        }

        return $lineup;
    }

    /**
     * 确认预定包厢
     * @param $seatId
     * @return mixed
     */
    public function enterBoxBook($seatId, $couponId, $userId){
        $storeModel         = new Stores();
        $seatModel          = new StoreSeat();
        $activityModel      = new StoreActivity();
        $userCouponModel    = new UserCoupons();
        $couponModel        = new StoreCoupons();
        $data = DB::table($seatModel->getTable().' as ss')
            ->select(
                'ss.id',
                'ss.seat_number',
                'ss.period',
                'ss.position',
                'ss.selling_price',
                'ss.start_time',
                'ss.ent_time',
                'ss.image',
                'ss.sales_volume',
                's.id as store_id',
                's.name as store_name'
            )->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'ss.stores_id')
            ->where('ss.id', $seatId)
            ->where('s.status', 2)
            ->where('s.store_type_id', 3)
            ->whereNull('s.delete_time')
            ->first();
        if (!$data){
            return false;
        }

        $data->create_time = date('Y-m-d H:i:s', time());

        /**
         * 优惠活动
         */
        $activity = DB::table($activityModel->getTable())->where('stores_id', $data->store_id)->get();

        $time = date('Y-m-d H:i:s', time());
        $activityDiscountAmount = 0;
        $activityData = '';
        foreach ($activity as $item) {
            $item->data = json_decode($item->data);
            if ($item->data->start_time < $time && $item->data->end_time > $time){

                if ($item->type == 'full_reduction'){
                    if(
                        $data->selling_price >= $item->data->full_amount
                        &&
                        $activityDiscountAmount < $item->data->discount_amount
                    ){
                        $activityDiscountAmount = $item->data->discount_amount;
                        $activityData = $item;
                    }

                    $item->data->full_amount = priceIntToFloat($item->data->full_amount);
                    $item->data->discount_amount = priceIntToFloat($item->data->discount_amount);

                }else{

                    if ($data->selling_price >= $item->data->full_amount){
                        $discountAmount = $data->selling_price - (int)($data->selling_price * ($item->data->discount_amount / 100));
                        if ($discountAmount > $activityDiscountAmount){
                            $activityDiscountAmount = $discountAmount;
                            $item->data->full_amount = priceIntToFloat($item->data->full_amount);
                            $item->data->discount_amount = ($item->data->discount_amount / 10).'折';
                            $activityData = $item;
                        }
                    }

                }


            }

        }

        /**
         * 优惠券
         */
        $couponDiscountAmount = 0;
        $couponData = '';
        if(!empty($couponId)){


            $coupon = DB::table($userCouponModel->getTable().' as uc')
                ->select(
                    'uc.*',
                    'sc.full_amount',
                    'sc.discount_amount',
                    'sc.type',
                    'sc.name'
                )
                ->leftJoin($couponModel->getTable().' as sc', 'uc.coupon_id', '=', 'sc.id')
                ->where('uc.users_id', $userId)
                ->where('uc.coupon_id', $couponId)
                ->where('uc.is_use', 0)
                ->first();


            if (!$coupon){
                return -1;
            }

            if ($coupon->end_time < $time){
                return -2;
            }


            if ($data->selling_price >=  $coupon->full_amount){
                if ($coupon->type == 0){
                    $couponDiscountAmount = $coupon->discount_amount;
                    $couponData = $coupon;
                }else{
                    $couponDiscountAmount = (int)($data->selling_price * (1 - ($coupon->discount_amount / 100)));
                    $couponData = $coupon;
                }
            }else{
                return -3;
            }
        }



        $data->discount_amount = priceIntToFloat($activityDiscountAmount + $couponDiscountAmount);
        $data->activity_discount_amount = priceIntToFloat($activityDiscountAmount);
        $data->coupon_discount_amount = priceIntToFloat($couponDiscountAmount);
        $data->pay_amount = priceIntToFloat($data->selling_price - $activityDiscountAmount - $couponDiscountAmount);
        $data->activyty = $activityData;
        $data->coupon = $couponData;
        $data->selling_price = priceIntToFloat($data->selling_price);
        return $data;
    }

    /**
     * 预订
     * @param $data
     * @return mixed
     */
    public function storeBook($data){
        $storeBookModel     = new StoreBooks();
        $storeSeatModel     = new StoreSeat();
        $bookOrderModel     = new StoreBookOrder();
        $storeModel         = new Stores();
        $storeActivityModel = new StoreActivity();
        $storeCouponModel   = new StoreCoupons();
        $userCouponModel    = new UserCoupons();

        if (isset($data['seat_id'])){
            $store = DB::table($storeSeatModel->getTable().' as ss')
                ->select('s.store_type_id', 'ss.selling_price')
                ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'ss.stores_id')
                ->where('ss.id', $data['seat_id'])
                ->first();
        }else{
            $store = DB::table($storeModel->getTable())->select('store_type_id')->first();
            $store->selling_price = 0;
        }

        try{
            DB::beginTransaction();
            $data['create_time'] = date('Y-m-d H:i:s', time());
            if ($store->store_type_id != 3){
                $qrcodeRedirectUrl = 'https://www.baidu.com';
                if (!is_dir(public_path() . '/qrcode/storebook')) {
                    mkdir(public_path() . '/qrcode/storebook', 0775, true);
                }
                $pathName   = md5(time().$data['users_id']);
                QrCode::format('png')->size(200)->margin(1)->generate($qrcodeRedirectUrl, public_path("qrcode/storebook/{$pathName}.png"));
                $data['qrcode'] = "/qrcode/storebook/{$pathName}.png";

            }
            $data['pay_amount'] = $store->selling_price;

            if (isset($data['activity_id']) && !empty($data['activity_id'])){
                $activity = DB::table($storeActivityModel->getTable())
                    ->where('id', $data['activity_id'])
                    ->whereNull('delete_time')
                    ->first();
                $activity->data = json_decode($activity->data);
                if ($activity->type == 'full_reduction' ){
                    $data['pay_amount'] -= $activity->data->discount_amount;
                    $data['activity_discount_amount'] = $activity->data->discount_amount;
                }else{
                    $data['pay_amount'] = $store->selling_price *  ($activity->data->discount_amount / 100);
                    $data['activity_discount_amount'] = $store->selling_price - $data['pay_amount'];
                }
            }

            if (isset($data['coupon_id']) && !empty($data['coupon_id'])){
                $coupon = DB::table($storeCouponModel->getTable())
                    ->where('id', $data['coupon_id'])
                    ->first();
                if ($coupon->type === 0){
                    $data['pay_amount'] -= $coupon->discount_amount;
                    $data['coupon_discount_amount'] = $coupon->discount_amount;
                }else{
                    $payAmount = $data['pay_amount'] * ($coupon->discount_amount / 100);
                    $data['coupon_discount_amount'] = $data['pay_amount'] - $payAmount;
                    $data['pay_amount'] = $payAmount;
                }
            }

            $bookId = DB::table($storeBookModel->getTable())->insertGetId($data);
            if (!$bookId){
                DB::rollBack();
                return false;
            }

            if ($store->store_type_id == 3){
                if(!DB::table($bookOrderModel->getTable())->insert([
                    'book_id'       => $bookId,
                    'status'        => 0
                ])){
                    DB::rollBack();
                    return false;
                }
            }

            DB::commit();
            Redis::setex('order-book-cancel:'.$bookId, 900, 'order-book-cancel');
            return $bookId;
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }


    }

    /**
     * 预约详细信息
     */
    public function storeBookInfo($bookId){
        $storeBookModel         = new StoreBooks();
        $storeSeatModel         = new StoreSeat();
        $storeCouponModel       = new StoreCoupons();
        $storeActivityModel     = new StoreActivity();
        $storeSeatPositionModel = new StoreSeatPosition();
        $orderBookModel         = new StoreBookOrder();
        $storeModel             = new Stores();

        $book = DB::table($storeBookModel->getTable() . ' as b')
            ->select(
                'b.*',
                'sp.name as position_name',
                'ob.status as pay_status'
            )
            ->leftJoin($storeSeatPositionModel->getTable() . ' as sp', 'sp.id', '=', 'b.position')
            ->leftJoin($orderBookModel->getTable().' as ob', 'ob.book_id', '=', 'b.id')
            ->where('b.id', $bookId)
            ->whereNull('b.delete_time')
            ->first();
        if (!$book){
            return false;
        }
        if ($book->qrcode){
            $book->qrcode = env('APP_URL').$book->qrcode;
        }

        $book->pay_amount = priceIntToFloat($book->pay_amount);
        $book->coupon_discount_amount = priceIntToFloat($book->coupon_discount_amount);
        $book->activity_discount_amount = priceIntToFloat($book->activity_discount_amount);
        /**
         * 座位信息
         */
        $book->seat = [];
        if (!empty($book->seat_id)){
            $book->seat = DB::table($storeSeatModel->getTable().' as ss')
                ->select('ss.*', 's.name as store_name')
                ->leftJoin($storeModel->getTable().' as s', 'ss.stores_id', '=', 's.id')
                ->where('ss.id', $book->seat_id)->first();
        }
        /**
         * 优惠券信息
         */
        $book->coupon = [];
        if (!empty($book->coupon_id)){
            $coupon = DB::table($storeCouponModel->getTable())->where('id', $book->coupon_id)->first();
            $coupon->full_amount = priceIntToFloat($coupon->full_amount);
            $coupon->discount_amount = priceIntToFloat($coupon->discount_amount);
            $book->coupon = $coupon;
        }
        /**
         * 活动信息
         */
        $book->activity = [];
        if (!empty($book->activity_id)){
            $activity = DB::table($storeActivityModel->getTable())
                ->select('type', 'data')
                ->where('id', $book->activity_id)
                ->first();
            $data = json_decode($activity->data, true);
            $data['full_amount'] = priceIntToFloat($data['full_amount']);
            if ($activity->type == 'full_reduction'){
                $data['discount_amount'] = priceIntToFloat($data['discount_amount']);
            }else{
                $data['discount_amount'] = ($data['discount_amount'] / 10).'折';
            }
            $book->activity = $data;
        }

        $book->refund = '';
        if ($book->pay_status == 4){
            $bookRefundModel        = new StoreBookRefund();
            $refund = DB::table($bookRefundModel->getTable())
                ->select(
                    'cause',
                    'image',
                    DB::raw("convert(money/100, decimal(15,2)) as money"),
                    'status',
                    'remark'
                )
                ->where('book_id', $bookId)
                ->first();

            $refund->image = empty($refund->image) ? [] : explode(',', $refund->image);
            $book->refund = $refund;
        }

        return $book;
    }

    /**
     * 我的预约信息
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function storeBookList($userId, $type, $page, $pageSize, $status){
        $storeBookModel     = new StoreBooks();
        $storeModel         = new Stores();
        $storeCouponModel   = new StoreCoupons();
        $bookOrderModel     = new StoreBookOrder();
        $seatModel          = new StoreSeat();
        $bookRefundModel    = new StoreBookRefund();

        $sql = DB::table($storeBookModel->getTable().' as sb')
            ->select('sb.*')
            ->where('sb.users_id', $userId)
            ->whereNull('sb.delete_time');
        if (!empty($type) && $type == 'leisure'){
            $sql->leftJoin($storeModel->getTable().' as s','sb.stores_id', '=', 's.id')
                ->leftJoin($seatModel->getTable().' as ss', 'ss.id', '=', 'sb.seat_id')
                ->leftJoin($bookOrderModel->getTable().' as bo', 'sb.id', '=', 'bo.book_id')
                ->leftJoin($bookRefundModel->getTable().' as bf', 'bf.book_id', '=', 'sb.id')
                ->addSelect(
                    'ss.image as seat_image',
                    'ss.seat_number',
                    'ss.start_time',
                    'ss.period',
                    'bo.pay_type',
                    'bo.status as pay_status',
                    'bo.pay_time',
                    'bf.status as refund_status'
                )
            ->where('s.store_type_id', 3);
            if (!empty($status) || $status === 0){
                $sql->where('bo.status', $status);
            }
        }else{
            $sql->leftJoin($storeModel->getTable().' as s','sb.stores_id', '=', 's.id')
                ->where('s.store_type_id', 1);
        }


        $book = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();



        $storeIds = [];
        $couponId = [];
        foreach ($book as $b){
            $storeIds[] = $b->stores_id;
            $couponId[] = $b->coupon_id;
            if (isset($b->pay_status) && !empty($b->pay_status)){
                switch ($b->pay_status){
                    case 0:
                        $b->status_text = '待付款';
                        break;
                    case 1:
                        $b->status_text = '待使用';
                        break;
                    case 2:
                        $b->status_text = '待评价';
                        break;
                    case 3:
                        $b->status_text = '已评价';
                        break;
                    case 4:
                        if ($b->refund_status == 0){
                            $b->status_text = '退款申请中';
                        }else if ($b->refund_status == 1){
                            $b->status_text = '退款成功';
                        }else{
                            $b->status_text = '退款失败';
                        }
                        break;
                    case 5:
                        $b->status_text = '已取消';
                        break;
                }
            }

        }

        $store = $this->storeList(['id' => $storeIds], 1, count($storeIds));
        $coupon = DB::table($storeCouponModel->getTable())->whereIn('id', $couponId)->get();

        foreach ($book as $l){
            $l->qrcode = env('APP_URL').$l->qrcode;
            $l->pay_amount = priceIntToFloat($l->pay_amount);
            $l->store = null;
            foreach ($store as $s){
                if($l->stores_id === $s->id){
                    $l->store = $s;
                }
            }

            foreach ($coupon as $item) {
                $item->full_amount = priceIntToFloat($item->full_amount);
                $item->discount_amount = priceIntToFloat($item->discount_amount);
                if($l->stores_id === $item->stores_id){
                    $l->coupon = $item;
                }
            }
        }


        return $book;
    }

    /**
     * 获取餐厅座位的位置信息
     */
    public function storeSeatPosition($storeId){
        $storeSeatModel         = new StoreSeat();
        $storeSeatPositionModel = new StoreSeatPosition();

        $seat = DB::table($storeSeatModel->getTable())
            ->select('position')
            ->where('stores_id', $storeId)
            ->groupBy('position')
            ->get();

        $positionIds = [];
        foreach ($seat as $s){
            $positionIds[] = $s->position;
        }

        $position = DB::table($storeSeatPositionModel->getTable())
            ->whereIn('id', $positionIds)
            ->get();

        return $position;

    }

    /**
     * 添加店铺信息
     * @param $store_id
     * @param $data
     * @return array|int|mixed
     */
    public function storeInfoMsg($store_id,$data)
    {
        try {
            if (!isset($store_id)) {
                return ['error' => '参数错误'];
            }

            $storeImage = new StoreImage();
            $storeInfo = new \App\Models\StoreInfo();
            DB::beginTransaction();
            try {
                $storeInfoData = [
                    'store_id' => $store_id,
                    'address' => $data['address'],
                    'long' => $data['long'],
                    'business_license' => $data['business_license'],
                    'lat' => $data['lat'],
                    'food_license' => $data['food_license'],
                    'am' => $data['am'],
                    'pm' => $data['pm'],
                    'introduce' => $data['introduce'],
                    'tags' => $data['tags'],
                ];
                $storeInfoMsg = DB::table($storeInfo->getTable())
                    ->where('store_id', $store_id)
                    ->first();
                if ($storeInfoMsg) {
                    $storeInfoId = $storeInfoMsg->id;
                    DB::table($storeInfo->getTable())
                        ->where('store_id', $store_id)
                        ->update($storeInfoData);
                } else {
                    $storeInfoId = DB::table($storeInfo->getTable())->insertGetId($storeInfoData);
                }
                $srore_image = DB::table($storeImage->getTable())
                    ->where('store_id', $store_id)
                    ->first();
                if ($srore_image) {
                    DB::table($storeImage->getTable())->where('store_id', $store_id)->delete();
                    foreach ($data['image'] as $v) {
                        $img_data = ['img_url' => $v, 'store_id' => $store_id];
                        DB::table($storeImage->getTable())->insertGetId($img_data);
                    }
                }else{
                    foreach ($data['image'] as $v) {
                        $img_data = ['img_url' => $v, 'store_id' => $store_id];
                        DB::table($storeImage->getTable())->insertGetId($img_data);
                    }
                }
                DB::commit();
                return $storeInfoId;
            } catch (\Exception $error) {
                DB::rollBack();
                \Log::debug($error);
                throw new BlockException($error);
            }
        } catch (\Exception $exception) {
            \Log::error($exception);
            throw new BlockException($exception);
        }
    }

    public function __getYunXinObject(){
        $yunXin = new ServerApi();
        return $yunXin;
    }

    /**
     * 添加商户
     * @param $data
     * @return bool
     */
    public function addStoreBasic($data){
        $storeModel         = new Stores();
        $storeInfoModel     = new StoreInfo();
        $storeYunXinModel   = new StoreYunxin();

        try{
            DB::beginTransaction();
            $name = DB::table($storeModel->getTable())
                ->where('name', $data['name'])
                ->whereNull('delete_time')
                ->first();
            if($name){
                return -1;
            }

            $mobile = DB::table($storeModel->getTable())
                ->where('mobile', $data['mobile'])
                ->whereNull('delete_time')
                ->first();
            \Log::debug(json_encode($mobile));
            if($mobile){
                return -2;
            }

            $storeId = DB::table($storeModel->getTable())->insertGetId($data);
            if (!$storeId){
                DB::rollBack();
                return false;
            }

            if(!DB::table($storeInfoModel->getTable())->insert(['store_id' => $storeId])){
                DB::rollBack();
                return false;
            }

            $yunXin             = $this->__getYunXinObject();
            $accId              = md5($storeId.$data['name']);
            $yunXinInfo         = $yunXin->createUserId($accId);
            if ($yunXinInfo['code'] == 200){
                DB::table($storeYunXinModel->getTable())->insert([
                    'stores_id'     => $storeId,
                    'accid'         => $accId,
                    'token'         => $yunXinInfo['info']['token']
                ]);
            }


            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error('商户添加失败：'.$exception);
            return false;
        }

    }

    /**
     * 平台添加商户
     * @param $data
     * @param $tag
     * @return array|int|mixed
     */
    public function addStore($data, $tag)
    {

        try {
            $store = new Stores();
            $storeInfo = new StoreInfo();
            DB::beginTransaction();
            try {
                $storeData = [
                    'classifity_id'         => $data['classifity_id'],
                    'name'                  => $data['name'],
                    'mobile'                => $data['mobile'],
                    'password'              => $data['password'],
                    'logo'                  => $data['logo'],
                    'store_type_id'         => $data['store_type_id'],
                    'company_id'            => $data['company_id'],
                    'create_time'           => $data['create_time'],
                ];
                if (isset($data['is_lineup'])){
                    $storeData['is_lineup'] = $data['is_lineup'];
                }

                if (isset($data['is_book'])){
                    $storeData['is_book'] = $data['is_book'];
                }

                if (isset($data['delivery_fee'])){
                    $storeData['delivery_fee'] = $data['delivery_fee'];
                }

                if (isset($data['init_delivery_price'])){
                    $storeData['init_delivery_price'] = $data['init_delivery_price'];
                }

                $storeInfoData = [
                    'address'           => $data['address'],
                    'long'              => $data['long'],
                    'lat'               => $data['lat'],
                    'business_license'  => $data['business_license'],
                    'am'                => $data['am'],
                    'pm'                => $data['pm'],
                    'introduce'         => $data['introduce'],
                    'contact'           => $data['contact'],
                ];

                if (isset($data['take_out'])){
                    $storeInfoData['take_out'] = $data['take_out'];
                }

                if (isset($data['food_license'])){
                    $storeInfoData['food_license'] = $data['food_license'];
                }

                if (isset($data['per_capita'])){
                    $storeInfoData['per_capita'] = $data['per_capita'];
                }

                $storeId = DB::table($store->getTable())
                    ->where('mobile', $data['mobile'])
                    ->first();

                if (!$storeId){
                    $storeId = DB::table($store->getTable())->insertGetId($storeData);

                }else{
                    DB::rollBack();
                    return '此商户手机号已存在';
                }

                if ($storeId){
                    $storeInfoData['store_id'] = $storeId;
                    if(!DB::table($storeInfo->getTable())->insertGetId($storeInfoData)){
                        DB::rollBack();
                        return '添加商户信息失败';
                    }
                }else{
                    DB::rollBack();
                    return '添加失败';
                }


                $tagData = [];
                if(!empty($tag)){
                    $storeTagModel = new StoreTags();
                    //添加新的标签
                    foreach ($tag as $k=>$v){
                        $tagData[$k]['stores_id'] = $storeId;
                        $tagData[$k]['tag_name'] = $v['tag_name'];
                        $tagData[$k]['tags_id'] = $v['id'];
                    }

                    $tagRes =  DB::table($storeTagModel->getTable())->insert($tagData);
                    if (!$tagRes){
                        DB::rollBack();
                        return '商户标签添加失败';
                    }
                }
                DB::commit();
                return $storeId;
            }catch (\Exception $error) {
                DB::rollBack();
                \Log::error($error);
                return '添加失败';
            }
        } catch (\Exception $exception) {
            \Log::error($exception);
            return '添加失败';
        }
    }


    /**
     * 平台修改店铺信息
     * @param $data
     * @return array|int|mixed
     */
    public function adminStoreInfoMsg($data, $tag)
    {

        if (!isset($data['id'])) {
            return ['error' => '参数错误'];
        }
        try {
            $store = new \App\Models\Stores();
            $storeInfo = new \App\Models\StoreInfo();
            DB::beginTransaction();
            try {
                $storeData = [
                    'name'                  => $data['name'],
                    'mobile'                => $data['mobile'],
                    'logo'                  => $data['logo'],
                    'user_volume'           => $data['user_volume'],
                    'is_lineup'             => $data['is_lineup'],
                    'is_book'               => $data['is_book'],
                    'delivery_fee'          => $data['delivery_fee'],
                    'init_delivery_price'   => $data['init_delivery_price'],
                    'status'                => $data['status']
                ];
                if (isset($data['password'])){
                    $storeData['password'] = $data['password'];
                }
                $storeInfoData = [
                    'address'           => $data['address'],
                    'long'              => $data['long'],
                    'lat'               => $data['lat'],
                    'business_license'  => $data['business_license'],
                    'food_license'      => $data['food_license'],
                    'am'                => $data['am'],
                    'pm'                => $data['pm'],
                    'introduce'         => $data['introduce'],
                    'per_capita'        => $data['per_capita'],
                    'contact'           => $data['contact'],
                    'take_out'          => $data['take_out'],
                    'store_id'          => $data['id']
                ];

                $store = DB::table($store->getTable())
                    ->where('id', $data['id'])
                    ->update($storeData);
                if ($store === false) {
                    \Log::debug($store);
                    DB::rollBack();
                    return false;
                }

                $storeInfoMsg = DB::table($storeInfo->getTable())
                    ->where('store_id', $data['id'])
                    ->first();

                if ($storeInfoMsg) {
                    DB::table($storeInfo->getTable())
                        ->where('store_id', $data['id'])
                        ->update($storeInfoData);
                    $storeInfoId = $storeInfoMsg->id;
                } else {
                    $storeInfoId = DB::table($storeInfo->getTable())->insertGetId($storeInfoData);
                }

                if ($storeInfoId === false) {
                    \Log::debug($storeInfoId);
                    DB::rollBack();
                    return false;
                }

                $tagData = [];
                if (!empty($tag)) {
                    $storeTagModel = new StoreTags();
                    //删除旧的标签
                    $storeTagdel = $storeTagModel::where('stores_id', $data['id'])->delete();

                    if ($storeTagdel === false) {
                        DB::rollBack();
                        \Log::error('删除旧标签失败');
                        return false;
                    }
                    //添加新的标签


                    foreach ($tag as $k => $v) {
                        $tagData[$k]['stores_id'] = $data['id'];
                        $tagData[$k]['tag_name'] = $v['tag_name'];
                        $tagData[$k]['tags_id'] = $v['id'];

                    }

                    $tagRes = DB::table($storeTagModel->getTable())->insert($tagData);
                    if (!$tagRes) {
                        DB::rollBack();
                        return false;
                    }
                }

                DB::commit();
                return $data['id'];
            } catch (\Exception $error) {
                DB::rollBack();
                \Log::error($error);
                return false;
            }
        } catch (\Exception $exception) {
            \Log::error($exception);
            return false;
        }

    }

    //平台商户列表
    private function __storeAdminSql(array $search){
        $storeModel             = new Stores();
        $storeClassifyModel     = new StoreClassifity();
        $storeInfo              = new \App\Models\StoreInfo();
        $ccStoreModel           = new CarefullyChosenStore();
        $collectMillModel       = new CollectMill();
        $sql = DB::table($storeModel->getTable() . ' as s')
            ->select(
                's.id',
                's.account',
                's.name',
                's.mobile',
                's.logo',
                's.background',
                's.synopsis',
                's.rating',
                's.user_volume',
                's.store_type_id',
                's.is_lineup',
                's.is_book',
                's.freight_type as freight',
                's.classifity_id',
                's.create_time',
                's.status',
                's.operate',
                's.agency',
                's.cuisine_name',
                DB::raw("convert(balance/100, decimal(15,2)) as balance"),
                DB::raw("convert(delivery_fee/100, decimal(15,2)) as delivery_fee"),
                DB::raw("convert(init_delivery_price/100, decimal(15,2)) as init_delivery_price"),
                'sc.name as class_name',
                'sc.icon',
                's.sort',
                'si.address',
                'si.latitude',
                'si.longitude',
                'si.business_license',
                'si.food_license',
                'si.am',
                'si.pm',
                'si.introduce',
                DB::raw("convert(per_capita/100, decimal(15,2)) as per_capita"),
                'si.take_out',
                'si.contact',
                'si.ali_pay',
                'si.wechat_pay',
                'si.bank_card',
                DB::raw("convert(turnover/100, decimal(15,2)) as turnover"),
                's.qr_code'
            )
            ->leftJoin($storeClassifyModel->getTable() . ' as sc', 's.classifity_id', '=', 'sc.id')

            ->leftJoin($storeInfo->getTable() . ' as si', 's.id', '=', 'si.store_id');

        if(isset($search['recommend'])){
            if($search['recommend'] == '精选好店') {
                $sql->leftJoin($ccStoreModel->getTable() . ' as cc', 'cc.stores_id', '=', 's.id');
                $sql->addSelect('cc.cover_image','cc.sort as cover_sort');
            }
        }

        $sql->whereNull('s.delete_time');

        if(isset($search['id'])){
            $sql->where('s.id', $search['id']);
        }

        if(isset($search['cid'])){
            $sql->where('s.classifity_id', $search['cid']);
        }

        if(isset($search['company'])){
            $sql->where('s.company_id', $search['company']);
        }

        if(isset($search['name'])){
            $sql->where('s.name', 'like', "%".$search['name']."%");
        }

        if (isset($search['status'])){
            $sql->where('s.status', $search['status']);
        }

        if (isset($search['store_type'])){
            $sql->where('s.store_type_id', $search['store_type']);
        }

        if (isset($search['create_time'])){
            $sql->whereBetween('s.create_time', $search['create_time']);
        }

        if(isset($search['collect'])){
            $sql->where('cm.store_id', $search['store_id'])
                ->leftJoin($collectMillModel->getTable() . ' as cm', 'cm.mill_id', '=', 's.id');
        }

        if (isset($search['orderColumn'])){
            $sql->orderBy($search['orderColumn'][0], $search['orderColumn'][1]);
        }else{
            $sql->orderBy('s.turnover', 'desc');
        }



        return $sql;
    }

    //商户列表新
    public function getStore(array $search, int $page = 1, int $pageSize = 20){
        $prefix     = env('DB_PREFIX');
        $storeModel             = new Stores();
        $storeTurnoverModel=new StoreTurnover();
        $storeClassifyModel     = new StoreClassifity();
        $sql=DB::table($storeModel->getTable() . ' as s')
            ->leftJoin($storeTurnoverModel->getTable() . ' as si', 's.id', '=', 'si.stores_id')
            ->leftJoin($storeClassifyModel->getTable() . ' as sc', 's.classifity_id', '=', 'sc.id')
            ->whereNull('s.delete_time')
            ->select(
                's.id',
                's.account',
                's.name',
                's.mobile',
                's.logo',
                's.background',
                's.synopsis',
                's.rating',
                's.user_volume',
                's.store_type_id',
                's.is_lineup',
                's.is_book',
                's.freight_type as freight',
                's.classifity_id',
                's.create_time',
                's.status',
                's.operate',
                's.agency',
                's.cuisine_name',
                DB::raw("convert(balance/100, decimal(15,2)) as balance"),
                DB::raw("convert(delivery_fee/100, decimal(15,2)) as delivery_fee"),
                DB::raw("convert(init_delivery_price/100, decimal(15,2)) as init_delivery_price"),
                'sc.name as class_name',
                'sc.icon',
                's.sort',
                's.qr_code',
                 DB::raw("sum(convert({$prefix}si.turnover/100, decimal(15,2))) as turnover")
            )->groupBy('s.id');

        if(isset($search['cid'])){
            $sql->where('s.classifity_id', $search['cid']);
        }
        if(isset($search['name'])){
            $sql->where('s.name', 'like', "%".$search['name']."%");
        }

        if (isset($search['status'])){
            $sql->where('s.status', $search['status']);
        }

        if (isset($search['store_type'])){
            $sql->where('s.store_type_id', $search['store_type']);
        }

        if (isset($search['create_time'])){
            $sql->whereBetween('si.date', $search['create_time']);
        }

        if (isset($search['orderColumn'])){
            $sql->orderBy($search['orderColumn'][0], $search['orderColumn'][1]);
        }else{
            $sql->orderBy('si.turnover', 'desc');
        }
        $total=$sql->get()->count();
        $store = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

        return [
            'list'          => $store,
            'pagination'    => [
                'total'     => $total,
                'current'   => $page,
                'pageSize'  => $pageSize
            ]
        ];

    }





//


    /**
     * 获取店铺列表
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return \Illuminate\Support\Collection
     */
    public function storeAdminList(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__storeAdminSql($search);
        $store = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();
        return $store;
    }


    /**
     * 获取店铺列表的分页信息
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function storeAdminListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__storeAdminSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }
    /**
     * 后台获取店铺分类
     * @param $companyId
     * @param $typeId
     * @return array
     */
    public function classifyList($companyId,  $typeId){
        $storeClassifyModel = new StoreClassifity();

        $sql = $storeClassifyModel::where('company_id', $companyId);
        if ($typeId !=0 ){
            $sql->where('type_id', $typeId);
        }
        $data = $sql->get();
        $pid = 0;
        $tree = $this->permissionsTree($data, $pid);
        return $tree;
    }
    public function permissionsTree($data, $pid){

        $tree = [];
        foreach($data as $key => $value){
            if($value->parent_id == $pid){
                $value->children = $this->permissionsTree($data, $value->id);
                $tree[] = $value;

                unset($data[$key]);
            }
        }
        return $tree;
    }

    /**
     * 后台获取店铺详情
     * @param $storeId
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeAdminInfo($storeId, $id){
        $sql = $this->__storeAdminSql(['id' => $storeId]);

        $res = $sql->addSelect('s.platform_rating', 's.user_rating')->first();
        if($res){
            //获取商品图片
            $res->tag = [];
            $tags = StoreTags::where('stores_id', $storeId)->get();
            foreach ($tags as $v){
                $res->tag[] = $v->tags_id;
            }
        }

        //是否收藏该店铺
        if ($id){
            $collectStoresModel = new CollectMill();
            $isCollect = DB::table($collectStoresModel->getTable())
                ->where('store_id',$id)
                ->where('mill_id',$storeId)->first();

            if (isset($isCollect->id)){
                $res->is_collect=1;
            }else{
                $res->is_collect=0;
            }

            //                是否申请了代理
            $agentApplyModel = new AgentApply();
            $isAgent = DB::table($agentApplyModel->getTable())
                ->where('mill_id', $storeId)
                ->where('store_id', $id)
                ->first();
            if (isset($isAgent->id) && $res->agency == 1){
                if ($isAgent->status == 0){
                    $res->p_id = null;
                }else{
                    $res->p_id = $isAgent->p_id;
                }
                $res->apply_status = $isAgent->status;
            }elseif($res->agency == 1){
                $res->p_id = null;
                $res->apply_status = 3;
            }else{
                $res->p_id = null;
                $res->apply_status = 4;
            }
        }else{
            $res->is_collect=0;
            $res->p_id = null;
        }

        return $res;
    }

    public function getStoreImage($id){
        $storeImageModel = new StoreImage();
        return DB::table($storeImageModel->getTable())->where('store_id', $id)->get();
    }

    /**
     * 修改商户状态
     * @param $id
     * @param $status
     * @return mixed
     */
    public function updateStoreStatus($id, $status){

        $storeModel     = new Stores();
        DB::beginTransaction();
        $result         = DB::table($storeModel->getTable())
                            ->where('id',$id)
                            ->update(['status'=>$status]);
        \Log::debug($result);

        if ($result && $status == 2){
            $storeInfoModel = new StoreInfo();
            $storeInfo      = DB::table($storeModel->getTable().' as s')
                ->select(
                    's.name as store_name',
                    's.mobile',
                    'si.address',
                    'si.longitude as lng',
                    'si.latitude as lat',
                    's.id as stores_id',
                    's.store_type_id'
                )
                ->leftJoin($storeInfoModel->getTable().' as si', 'si.store_id', '=', 's.id')
                ->where('s.id', $id)
                ->first();
//            if ($storeInfo->store_type_id == 1){
//                $dianWoDa       = new DianWoDa();
//                $createResult   = $dianWoDa->sellerTransportationConfirm(json_decode(json_encode($storeInfo), true));
//                \Log::debug($createResult);
//                if ($createResult == 'success'){
//                    DB::commit();
//                    $result = true;
//                }else{
//                    DB::rollBack();
//                    $result = false;
//                }
//            }else{
//                DB::commit();
//            }
            DB::commit();

        }else if ($result){
                DB::commit();
        }else{
            DB::rollBack();
        }

        return $result;
    }

    /**
     * 修改商户基本信息
     * @param $data
     * @param $infoData
     * @return bool
     */
    public function updateBasicInfo($data, $infoData, $devices, $companyId, $storeImage, $tag){

        try{

            DB::beginTransaction();

            $storeModel = new Stores();
            $storeInfoModel = new StoreInfo();
            $storeDeviceModel = new StoreDevices();
            $storeImageModel = new StoreImage();

            $devicesData = [];
            if (!empty($devices)){

                //删除旧的服务
                $storeDevicesdel = DB::table($storeDeviceModel->getTable())->where('stores_id', $data['id'])->update([
                    'delete_time' => date('Y-m-d H:i:s', time())
                ]);

                if ($storeDevicesdel === false) {
                    DB::rollBack();
                    \Log::error('删除旧服务失败');
                    return false;
                }
                foreach ($devices as $key => $value) {
                    $devicesData[$key]['company_id'] = $companyId;
                    $devicesData[$key]['stores_id'] = $data['id'];
                    $devicesData[$key]['name'] = $value;
                }

                $devicesRes = DB::table($storeDeviceModel->getTable())->insert($devicesData);
                if (!$devicesRes) {
                    DB::rollBack();
                    return false;
                }
            }

            $tagData = [];
            if (!empty($tag)) {
                $storeTagModel = new StoreTags();
                //删除旧的标签
                $storeTagdel = $storeTagModel::where('stores_id', $data['id'])->delete();

                if ($storeTagdel === false) {
                    DB::rollBack();
                    \Log::error('删除旧标签失败');
                    return false;
                }
                //添加新的标签
                foreach ($tag as $k => $v) {
                    $tagData[$k]['stores_id'] = $data['id'];
                    $tagData[$k]['tag_name'] = $v['tag_name'];
                    $tagData[$k]['tags_id'] = $v['id'];

                }
                $tagRes = DB::table($storeTagModel->getTable())->insert($tagData);
                if (!$tagRes) {
                    DB::rollBack();
                    return false;
                }
            }

            $store = $storeModel::where('id', $data['id'])->first();
            \Log::debug($data);
            if(isset($store->id)){
                if (
                DB::table($storeModel->getTable())->where('id', $data['id'])->update($data) === false
                ){
                    DB::rollBack();
                    return false;
                }
            }
//            else{
//                DB::table($storeModel->getTable())->insert($data);
//            }
//            DB::table($storeModel->getTable())->where('id', $data['id'])->update($data);

            $storeInfo = $storeInfoModel::where('store_id', $infoData['store_id'])->first();
            \Log::debug($infoData);
            if(isset($storeInfo->id)){
                if (
                DB::table($storeInfoModel->getTable())
                    ->where('store_id', $infoData['store_id'])
                    ->update($infoData) === false
                ){
                    DB::rollBack();
                    return false;
                }

            }else{

                if (
                    DB::table($storeInfoModel->getTable())->insert($infoData) === false
                ){
                    DB::rollBack();
                    return false;
                }
            }



            $imageData = [];
            if (!empty($storeImage)){
                //删除旧的图片
                $storeImagesdel = $storeImageModel::where('store_id', $infoData['store_id'])->delete();
                if ($storeImagesdel === false) {
                    DB::rollBack();
                    \Log::error('删除旧图片失败');
                    return false;
                }
                foreach ($storeImage as $key => $item) {
                    $imageData[$key]['store_id']    = $infoData['store_id'];
                    $imageData[$key]['img_url']   = $item;
                }

                //添加新图片
                $storeImagesRes = DB::table($storeImageModel->getTable())->insert($imageData);
                if (!$storeImagesRes) {
                    DB::rollBack();
                    return false;
                }
            }
            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }
    }

    public function storeGoodsTags($search){
        $goodsModel             = new Goods();
        $goodsClassifyModel     = new GoodsClassifies();
        $storeModel             = new Stores();
        $goodsTagsMiddle        = new GoodsTagsMiddle();
        $goodsTags              = new GoodsTags();

        //查询商户所有标签
        $tagsSql    = $goodsTags::where('stores_id', $search['store_id']);
        if(isset($search['tag_id'])){
            $tagsSql->whereIn('id',$search['tag_id']);
        }
        $tags       = $tagsSql->get();

        $prefix     = env('DB_PREFIX');
        $field = [
            'g.id',
            'g.stores_id',
            'g.classify_id',
            'g.name',
            'g.slogan',
            'g.enable_sku',
            'g.service',
            'g.status',
            'g.thumbnail',
            DB::raw("convert({$prefix}g.selling_price/100, decimal(15,2)) as selling_price"),
            DB::raw("convert({$prefix}g.purchase_price/100, decimal(15,2)) as purchase_price"),
            DB::raw("convert({$prefix}g.original_price/100, decimal(15,2)) as original_price"),
            'g.sales_volume',
            'g.stock',
            'g.user_volume',
            'g.store_nav_id',
            'g.create_time',
            'gc.name as class_name',
            'gc.icon as class_icon',
            's.name as stores_name',
            's.store_type_id'
        ];
        if (!$tags->isEmpty()){
            //查询所有标签下面的商品
            foreach ($tags as $k=>&$v) {
                $tagsId[] = $v->id;
                $sql = DB::table($goodsModel->getTable() . ' as g')
                    ->select($field)
                    ->leftJoin($goodsClassifyModel->getTable() . ' as gc', 'g.classify_id', '=', 'gc.id')
                    ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'g.stores_id')
                    ->leftJoin($goodsTagsMiddle->getTable() . ' as gtm', 'gtm.goods_id', '=', 'g.id')
                    ->whereNull('g.delete_time')
                    ->where('g.status', 2);
                $v->goods = null;
                $v->goods = $sql->where('gtm.tag_id', $v->id)
                    ->groupBy('g.id')->get();

            }
        }else{
            $goods = DB::table($goodsModel->getTable() . ' as g')
                ->select($field)
                ->leftJoin($goodsClassifyModel->getTable() . ' as gc', 'g.classify_id', '=', 'gc.id')
                ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'g.stores_id')
                ->whereNull('g.delete_time')
                ->where('s.id', $search['store_id'])
                ->get();

            $tags = [[
                'stores_id' => $search['store_id'],
                'tag_name'  => '所有商品',
                'goods'     => $goods
            ]];
        }

        return $tags;
        //查询所有的商品
    }

    /**
     * 删除精选好店
     * @param $store_id
     * @return mixed
     */
    public function deleteChosenStore($id){
        $chosenStoreModel = new CarefullyChosenStore();
        return DB::table($chosenStoreModel->getTable())->where('stores_id', $id)->delete();
    }

    /**
     * 修改精选好店信息
     * @param $id
     * @param $image
     * @param $sort
     * @return mixed
     */
    public function updateChosenStore($id, $image, $sort){
        $chosenStoreModel = new CarefullyChosenStore();
        return DB::table($chosenStoreModel->getTable())->where('stores_id', $id)->update([
            'sort'          => $sort,
            'cover_image'   => $image
        ]);
    }

    /**
     * 添加精选好店信息
     * @param $store_id
     * @param $image
     * @param $sort
     * @return mixed
     */
    public function addChosenStore($store_id, $image, $sort){
        $chosenStoreModel = new CarefullyChosenStore();
        return DB::table($chosenStoreModel->getTable())->insert([
            'stores_id'      => $store_id,
            'sort'          => $sort,
            'cover_image'   => $image
        ]);
    }


    /**
     * 批发商城首页信息
     */
    public function indexPage($position){
        $bannerModel = new Banners();
        $pageNavModel = new PageNav();
        $storeModel = new Stores();
        $goodsModel = new Goods();
        $date = date('Y-m-d H:i:s', time());
        $banner = DB::table($bannerModel->getTable())->where('uptime', '<=', $date)
            ->where('downtime', '>=', $date)
            ->where('position', $position)
            ->orderBy('sort', 'asc')
            ->get();
        $recommend = [
            ['title' => "采购社圈", 'image' => "https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/app/156449461874.png", 'url' => ""],
            ['title' => "付费功能", 'image' => "https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/app/156455375666.png", 'url' => ""],
            ['title' => "新人攻略", 'image' => "https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/app/156449481245.png", 'url' => ""],
            ['title' => "小程序", 'image' => "https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/app/156455385428.png", 'url' => ""]
        ];
        $nav = DB::table($pageNavModel->getTable())
            ->where('position', $position)
            ->whereNull('delete_time')
            ->get();
        foreach ($nav as $k=>$v){
            $v->url = json_decode($v->url, true);
        }
        $hotBrand = DB::table($storeModel->getTable())
            ->select(['id', 'name', 'logo'])
            ->where('agent_type', 0)
            ->where('agency', 1)
            ->where('status', 2)
            ->whereNull('delete_time')
            ->get();

        $search['goodids'] = [];
        $goodList = DB::table($goodsModel->getTable() . ' as g')
            ->select(
                'g.id',
                's.agency'
            )
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'g.stores_id')
            ->whereNull('g.delete_time')
            ->where('g.status', 2)
            ->where('g.stores_id', '!=', 0)
            ->where('s.agency', 0)
            ->where('s.agent_type', 0)
            ->where('g.mill_id', '!=', 0)
            ->where('g.company_id', 1)
            ->get();

        $goodList = json_decode(json_encode($goodList), true);
        $numReq = ceil(count($goodList)/2);
        if ($numReq>10){
            $numReq = 10;
        }
        if ($numReq>0){
            $goodIds = array_rand($goodList, $numReq);

            $search['goodids'] = [];
            foreach ($goodIds as $k=>$v){

                $search['goodids'][] = $goodList[$v]['id'];
            };

            $goodsBlock = new GoodsBlock();
            $sellLike = $goodsBlock->goodsList($search, 1, 10);
        }else{
            $sellLike = [];
        }


        return [
            'banner'        => $banner,
            'recommend'     => $recommend,
            'nav'           => $nav,
            'hotBrand'      => $hotBrand,
            'sellLike'      => $sellLike,

        ];
    }

    /**
     * 收藏店铺(蓝丝羽app端)
     * @param $data
     * @param $type
     * @return bool
     */
    public function collectStore($data, $type){
        $collectMillModel = new CollectMill();
        if ($type==0){
            $res = DB::table($collectMillModel->getTable())
                ->where('mill_id', $data['mill_id'])
                ->where('store_id', $data['store_id'])
                ->delete();

        }else{
            $res = DB::table($collectMillModel->getTable())->insert($data);
        }
        return $res;
    }


    /**
     * 个人中心
     * @param $storeId
     * @return array
     */
    public function personal($storeId){
//        收藏数量
        $collectMillGoodsModel = new CollectMillGoods();
        $collectNum = DB::table($collectMillGoodsModel->getTable())->where('store_id', $storeId)->count();
//        新订单
        $agentTurnoverModel = new AgentTurnover();
        $orderNum = DB::table($agentTurnoverModel->getTable())->where('p_id', $storeId)->count();
//        下级代理
        $agentApplyModel = new AgentApply();
        $agentNum = DB::table($agentApplyModel->getTable())->where('p_id', $storeId)->count();
//        消息
        $messaegModel = new Message();
        $unReadMessageNum = DB::table($messaegModel->getTable())->where('is_read', 0)->where('store_id', $storeId)->count();
        $messageNum = DB::table($messaegModel->getTable())->where('store_id', $storeId)->count();
//        销售额
//        $agentTurnoverModel = new AgentTurnover();
        $saleNum = DB::table($agentTurnoverModel->getTable())->where('p_id', $storeId)->sum('total_price');
        return [
          'collect_num'             =>  $collectNum,
          'order_num'               =>  $orderNum,
          'agent_num'               =>  $agentNum,
          'message_num'             =>  $messageNum,
          'un_read_message_num'     =>  $unReadMessageNum,
          'sale_num'                =>  $saleNum/100,
        ];
    }

    /**
     * 找回密码（商户/厂家）
     * @param $data
     * @return bool
     */
    public function passwordByVc($data){
//        查询商户信息
        $StoreModel = new Stores();
        $store = $StoreModel::where('mobile', $data['mobile'])->first();
        if(!isset($store->id)){
            \Log::error("商户信息有误");
            return false;
        }
//        修改密码
        $res = DB::table($StoreModel->getTable())->where('id', $store->id)->update([
            'password' => md5($data['password'])
        ]);
        return $res;
    }

    /**
     * 上传商户合同信息
     * @param $data
     * @param $id
     * @return mixed
     */
    public function uploadStoreContract($data, $id){
        $storeContractModel = new StoreContract();
        if (empty($id)){
            return DB::table($storeContractModel->getTable())->insert($data);
        }else{
            return DB::table($storeContractModel->getTable())
                ->where('id', $id)
                ->update($data);
        }

    }

    /**
     * 获取商户合同
     * @param $storeId
     * @return mixed
     */
    public function getStoreContract($storeId){
        $storeContractModel = new StoreContract();
        return DB::table($storeContractModel->getTable())->where('stores_id', $storeId)->first();
    }


    public function getStoreType($id){
        $storeModel     = new Stores();
        return DB::table($storeModel->getTable())
            ->where('id', $id)
            ->value('store_type_id');
    }

    public function bookComment($usersId, $bookId, $content, $image, $incognito){
        $storeBookModel             = new StoreBooks();
        $bookCommentModel           = new StoreBookComment();
        $storeBookOrderModel        = new StoreBookOrder();

        $storeId = DB::table($storeBookModel->getTable())
            ->where('id', $bookId)
            ->value('stores_id');

        try{
            DB::beginTransaction();
            $commentId = DB::table($bookCommentModel->getTable())->insertGetId([
                'users_id'          => $usersId,
                'stores_id'         => $storeId,
                'book_id'           => $bookId,
                'content'           => $content,
                'image'             => implode(',', $image),
                'is_incognito'      => $incognito
            ]);
            if (!$commentId){
                DB::rollBack();
                return false;
            }

            if(!DB::table($storeBookOrderModel->getTable())
                ->where('id', $bookId)
                ->update(['status' => 3])
            ){
                DB::rollBack();
                return false;
            }


            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }

    /**
     * 预约退款
     * @param $usersId
     * @param $bookId
     * @param $cause
     * @param $image
     * @return bool|int
     */
    public function bookRefund($usersId, $bookId, $cause, $image){
        $bookOrderModel         = new StoreBookOrder();
        $bookModel              = new StoreBooks();
        $bookRefundModel        = new StoreBookRefund();

        $book = DB::table($bookModel->getTable().' as b')
            ->select('b.*', 'bo.status as pay_status')
            ->leftJoin($bookOrderModel->getTable().' as bo', 'bo.book_id', '=', 'b.id')
            ->where('b.users_id', $usersId)
            ->where('b.id', $bookId)
            ->first();

        if (!$book){
            return -1;
        }

        if ($book->pay_status === 0){
            return -2;
        }

        $refund = DB::table($bookRefundModel->getTable())
            ->where('users_id', $usersId)
            ->where('book_id', $bookId)
            ->first();

        if ($refund){
            return -3;
        }

        try{
            if(!DB::table($bookRefundModel->getTable())->insert([
                'cause'     => $cause,
                'image'     => implode(',', $image),
                'book_id'   => $bookId,
                'users_id'  => $usersId,
                'money'     => $book->pay_amount,
                'status'    => 0
            ])){
                DB::rollBack();
                return false;
            }

            if(!DB::table($bookOrderModel->getTable())
                ->where('book_id', $bookId)
                ->update(['status' => 4])
            ){
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }


    }


    public function getStoreComment($storeId, $page, $pageSize){
        $commentModel           = new StoreBookComment();
        $userModel              = new Users();

        $sql = DB::table($commentModel->getTable().' as c')
            ->select('c.*', 'u.name', 'u.avatar')
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'c.users_id')
            ->where('c.stores_id', $storeId);
        $total  = $sql->count();
        $data   = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();

        foreach ($data as $item) {
            $item->image = explode(',', $item->image);
            if ($item->is_incognito === 1){
                $item->avatar   = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/avatar/157102430871.png';
                $item->name     = '匿名用户';
            }
        }

        return [
            'list'          => $data,
            'pagination'    => [
                'total'     => $total,
                'current'   => $page,
                'pageSize'  => $pageSize
            ]
        ];

    }

    /**
     * 获取品牌商家
     * @return \Illuminate\Support\Collection
     */
    public function getStoreBrand(){
        $storeBrandModel        = new StoreBrand();
        $storeModel             = new Stores();

        return DB::table($storeBrandModel->getTable().' as sb')
            ->select('sb.id','s.name', 's.id as store_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'sb.stores_id')
            ->whereNull('s.delete_time')
            ->get();
    }

    /**
     * 通过ID获取品牌商户
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getStoreBrandById($id){
        $storeBrandModel        = new StoreBrand();
        return DB::table($storeBrandModel->getTable())->where('stores_id', $id)->first();
    }

    /**
     * 添加商家品牌
     * @param $id
     * @return bool
     */
    public function saveStoreBrand($id){
        $storeBrandModel        = new StoreBrand();
        return DB::table($storeBrandModel->getTable())->insert(['stores_id' => $id]);
    }

    /**
     * 删除商家品牌
     * @param $id
     * @return int
     */
    public function removeStoreBrand($id){
        $storeBrandModel        = new StoreBrand();
        return DB::table($storeBrandModel->getTable())->where('id', $id)->delete();
    }

    /**
     * 获取店铺租赁信息列表
     * @param $page
     * @param $pageSize
     * @param $search
     * @return array
     */
    public function getStoreLeaseList($page, $pageSize, $search){
        $storeLessModel             = new StoreLease();

        $sql = DB::table($storeLessModel->getTable());

        if (isset($search['status']) && !empty($search['status'])){
            $sql->where('status', $search['status']);
        }

        if (isset($search['name']) && !empty($search['name'])){
            $sql->where('store_name', 'like', "%{$search['name']}%");
        }

        $total = $sql->count();
        $list  = $sql->skip(($page - 1) * $pageSize)
            ->take($pageSize)
            ->get();

        return [
            'list'          => $list,
            'pagination'    => [
                'current'   => $page,
                'pageSize'  => $pageSize,
                'total'     => $total
            ]
        ];
    }

    /**
     * 添加或者修改店铺租赁信息
     * @param $id
     * @param $data
     * @return bool|int
     */
    public function saveStoreLease($id, $data){
        $storeLeaseModel        = new StoreLease();
        if (!empty($id)){
            return DB::table($storeLeaseModel->getTable())
                ->where('id', $id)
                ->update($data);
        }else{
            if(DB::table($storeLeaseModel->getTable())
                ->where('store_name', 'like', "%{$data['store_name']}%")
                ->first()
            ){
                return -1;
            }
            return DB::table($storeLeaseModel->getTable())->insert($data);
        }

    }

    public function storePayment($data, $id){
        $storeLeaseLogModel         = new StoreLeaseLog();
        $storeLeaseModel            = new StoreLease();

        try{

            DB::beginTransaction();

            if(!DB::table($storeLeaseLogModel->getTable())
                ->insert($data)
            ){
                DB::rollBack();
                \Log::debug('添加商户缴费日志失败');
                return false;
            }

            if(!DB::table($storeLeaseModel->getTable())
                ->where('id', $id)
                ->update([
                    'lease_end_time'        => $data['payment_time'],
                    'status'                => 1,
                    'payment_time'          => date('Y-m-d')
                ])
            ){
                DB::rollBack();
                \Log::debug('修改商户租赁信息失败');
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }


    }

    /**
     * 获取商家租赁缴费记录
     * @param $page
     * @param $pageSize
     * @param $name
     * @return array
     */
    public function paymentRecording($page, $pageSize, $name){
        $leaseLogModel      = new StoreLeaseLog();

        $sql = DB::table($leaseLogModel->getTable())
            ->select(
                'type',
                'type',
                'create_time',
                DB::raw("convert(pay_amount/100, decimal(15,2)) as pay_amount"),
                'payment_time',
                'pay_type',
                'remark',
                'store_name'
            );

        if (!empty($name)){
            $sql->where('store_name', 'like', "%{$name}%");
        }

        $total  = $sql->count();
        $list   = $sql->skip(($page - 1) * $pageSize)
            ->take($pageSize)
            ->get();

        return [
            'list'          => $list,
            'pagination'    => [
                'current'   => $page,
                'pageSize'  => $pageSize,
                'total'     => $total
            ]
        ];

    }


    public function getStoreTurnover($storeId){
        $orderModel     = new Orders();
        $storeModel     = new Stores();
        $time           = [
            date('Y-m-d', strtotime("+1 day")),
            date('Y-m-d', strtotime("-16 day"))
        ];

        $sevenTurnover  = DB::table($orderModel->getTable())
            ->select(
                DB::raw('CONVERT(SUM(pay_amount)/100, decimal(15,2))  as amount'),
                DB::raw("str_to_date(create_time, '%Y-%m-%d') as date")
            )
            ->where('stores_id', $storeId)
            ->where('create_time', '<', $time[0])
            ->where('create_time', '>', $time[1])
            ->where('status', 3)
            ->groupBy('date')
            ->get();

        $timeData = range(
            time(),
            strtotime("-15 day"),
            86400
        );
        $dateData = array_map(function ($value){
            return date('Y-m-d', $value);
        }, $timeData);

        $data = [];
        foreach ($dateData as $key => $value) {
            $data[$key]['amount'] = priceIntToFloat(0);
            $data[$key]['date'] = $value;
        }

        foreach ($data as &$value) {
            foreach ($sevenTurnover as $item) {
                if ($value['date'] == $item->date){
                    $value['amount'] = $item->amount;
                }
            }
        }

        $totalTurnover = DB::table($orderModel->getTable())
            ->select(DB::raw('CONVERT(SUM(pay_amount)/100, decimal(15,2))  as amount'))
            ->where('status', 3)
            ->where('stores_id', $storeId)
            ->get()->toArray();


        $balance = DB::table($storeModel->getTable())
            ->select(DB::raw('CONVERT(balance/100, decimal(15,2))  as balance'))
            ->where('id', $storeId)
            ->first();

        $dayTurnover = DB::table($orderModel->getTable())
            ->select(DB::raw('CONVERT(SUM(pay_amount)/100, decimal(15,2))  as amount'))
            ->where('status', 3)
            ->where('stores_id', $storeId)
            ->where('pay_time', 'like', date('Y-m-d' ,time()).'%')
            ->get()->toArray();

        return [
            'sevenTurnover'         => $data,
            'totalTurnover'         => $totalTurnover[0]->amount,
            'dayTurnover'           => $dayTurnover[0]->amount == null ? priceIntToFloat(0) : $dayTurnover[0]->amount,
            'balance'               => $balance->balance,
            'day'                   => 15
        ];

    }

    public function downStore(){
        $goodsModel     = new Goods();
        $storeModel     = new Stores();

        $store = DB::table($goodsModel->getTable())
            ->select('stores_id')
            ->groupBy('stores_id')
            ->get();
        $storeArray = [];

        foreach ($store as $item) {
            $storeArray[] = $item->stores_id;
        }

        return DB::table($storeModel->getTable())
            ->whereNotIn('id', $storeArray)
            ->update(['operate' => 0]);
    }

    public function generateQrCode(){
        $storeModel     = new Stores();
        $store = DB::table($storeModel->getTable())
            ->select('name', 'id', 'store_type_id', 'logo')
            ->where('store_type_id', 4)
            ->get()->toArray();

        $result = [];
        if (!empty($store)){
            if (!is_dir(public_path().'/images')){
                @mkdir(public_path().'/images', 0755);
            }

            if (!is_dir(public_path().'/images/store')){
                @mkdir(public_path().'/images/store', 0755);
            }

            foreach ($store as $item) {
                $path = public_path().'/images/store';
                $imageName = $item->name.".png";
                $data = [
                    'redirect_type'     => 'native',
                    'path'              => 'ShopFoodOrderDetail',
                    'params'            => [
                        'id'            => $item->id,
                        'type'          => $item->store_type_id
                    ]
                ];
                QrCode::format('png')
                    ->size(500)
                    ->encoding('UTF-8')
                    ->merge('/public/images/logo.png',.15)
                    ->generate(
                        json_encode($data),
                        $path.'/'.$imageName
                    );

                $result[] = DB::table($storeModel->getTable())
                    ->where('id', $item->id)
                    ->update(['qr_code' => env('APP_URL').'/images/store/'.$imageName]);
            }
        }
        return $result;

    }


    public function addStoreGoods(){
        $storeModel     = new Stores();
        $goodsModel     = new Goods();

//        $store = DB::table($storeModel->getTable())
//            ->select('id')
//            ->whereIn('id', [221])
//            ->get();
        $storeId = [223];
//        foreach ($store as $item) {
//            $storeId[] = $item->id;
//        }
        $goodsData = [
            [
                'company_id'            => 1,
                'stores_id'             => 0,
                'classify_id'           => 0,
                'name'                  => '0.1元',
                'slogan'                => '0.1元',
                'thumbnail'             => 'https://api.hnxhlmxc.com/images/logo.png',
                'selling_price'         => '10',
                'purchase_price'        => '10',
                'original_price'        => '10',
                'stock'                 => 99999,
                'params'                => json_encode([]),
                'service'               => json_encode([]),
                'status'                => 2
            ],
            [
                'company_id'            => 1,
                'stores_id'                => 0,
                'classify_id'           => 0,
                'name'                  => '0.5元',
                'slogan'                => '0.5元',
                'thumbnail'             => 'https://api.hnxhlmxc.com/images/logo.png',
                'selling_price'         => '50',
                'purchase_price'        => '50',
                'original_price'        => '50',
                'stock'                 => 99999,
                'params'                => json_encode([]),
                'service'               => json_encode([]),
                'status'                => 2
            ],
            [
                'company_id'            => 1,
                'stores_id'             => 0,
                'classify_id'           => 0,
                'name'                  => '1元',
                'slogan'                => '1元',
                'thumbnail'             => 'https://api.hnxhlmxc.com/images/logo.png',
                'selling_price'         => '100',
                'purchase_price'        => '100',
                'original_price'        => '100',
                'stock'                 => 99999,
                'params'                => json_encode([]),
                'service'               => json_encode([]),
                'status'                => 2
            ],
            [
                'company_id'            => 1,
                'stores_id'                => 0,
                'classify_id'           => 0,
                'name'                  => '5元',
                'slogan'                => '5元',
                'thumbnail'             => 'https://api.hnxhlmxc.com/images/logo.png',
                'selling_price'         => '500',
                'purchase_price'        => '500',
                'original_price'        => '500',
                'stock'                 => 99999,
                'params'                => json_encode([]),
                'service'               => json_encode([]),
                'status'                => 2
            ],
            [
                'company_id'            => 1,
                'stores_id'             => 0,
                'classify_id'           => 0,
                'name'                  => '10元',
                'slogan'                => '10元',
                'thumbnail'             => 'https://api.hnxhlmxc.com/images/logo.png',
                'selling_price'         => '1000',
                'purchase_price'        => '1000',
                'original_price'        => '1000',
                'stock'                 => 99999,
                'params'                => json_encode([]),
                'service'               => json_encode([]),
                'status'                => 2
            ],
            [
                'company_id'            => 1,
                'stores_id'                => 0,
                'classify_id'           => 0,
                'name'                  => '20元',
                'slogan'                => '20元',
                'thumbnail'             => 'https://api.hnxhlmxc.com/images/logo.png',
                'selling_price'         => '2000',
                'purchase_price'        => '2000',
                'original_price'        => '2000',
                'stock'                 => 99999,
                'params'                => json_encode([]),
                'service'               => json_encode([]),
                'status'                => 2
            ],
            [
                'company_id'            => 1,
                'stores_id'                => 0,
                'classify_id'           => 0,
                'name'                  => '50元',
                'slogan'                => '50元',
                'thumbnail'             => 'https://api.hnxhlmxc.com/images/logo.png',
                'selling_price'         => '5000',
                'purchase_price'        => '5000',
                'original_price'        => '5000',
                'stock'                 => 99999,
                'params'                => json_encode([]),
                'service'               => json_encode([]),
                'status'                => 2
            ],
            [
                'company_id'            => 1,
                'stores_id'                => 0,
                'classify_id'           => 0,
                'name'                  => '100元',
                'slogan'                => '100元',
                'thumbnail'             => 'https://api.hnxhlmxc.com/images/logo.png',
                'selling_price'         => '10000',
                'purchase_price'        => '10000',
                'original_price'        => '10000',
                'stock'                 => 99999,
                'params'                => json_encode([]),
                'service'               => json_encode([]),
                'status'                => 2
            ]

        ];

        DB::beginTransaction();
        $saveGoods          = [];
        $i = 0;
        $storeNavModel      = new StoreNavs();
        foreach ($storeId as $storeValue) {
            $navId = DB::table($storeNavModel->getTable())
                ->insertGetId([
                    'stores_id'     => $storeValue,
                    'name'          => '自定义价格专区'
                ]);
            foreach ($goodsData as $goodsValue) {
                $saveGoods[$i]                      = $goodsValue;
                $saveGoods[$i]['stores_id']         = $storeValue;
                $saveGoods[$i]['store_nav_id']      = $navId;
                \Log::debug($saveGoods);
                $result = DB::table($goodsModel->getTable())
                    ->insert($saveGoods[$i]);


                if (!$result) {
                    DB::rollBack();
                    return false;
                }

                $i++;
            }
        }

        DB::commit();


        dd($result);

    }

    /**
     * 生成商家二维码
     * @param $storeId
     * @return bool
     */
    public function generateStoreQrCode($storeId){
        $storeModel     = new Stores();
        $store          = DB::table($storeModel->getTable())
            ->select('name', 'store_type_id')
            ->where('id', $storeId)
            ->first();

        try{
            if (!is_dir(public_path().'/images')){
                @mkdir(public_path().'/images', 0755);
            }

            if (!is_dir(public_path().'/images/store')){
                @mkdir(public_path().'/images/store', 0755);
            }

            $path = public_path().'/images/store';
            $data = [
                'redirect_type'     => 'native',
                'path'              => 'ShopFoodOrderDetail',
                'params'            => [
                    'id'            => $storeId,
                    'type'          => $store->store_type_id
                ]
            ];
            $imageName = $store->name.".png";
            \SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')
                ->size(500)
                ->encoding('UTF-8')
                ->merge('/public/images/logo.png',.15)
                ->generate(
                    json_encode($data),
                    $path.'/'.$imageName
                );

            if(!DB::table($storeModel->getTable())
                ->where('id', $storeId)
                ->update(['qr_code' => env('APP_URL').'/images/store/'.$imageName])
            ){
                return false;
            }

            return true;
        }catch(\Exception $exception){
            \Log::error($exception);
            return false;
        }

    }

}
