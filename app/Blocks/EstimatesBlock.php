<?php

namespace App\Blocks;


use App\Models\AgentTurnover;
use App\Models\Goods;
use App\Models\GoodsAgentEstimates;
use App\Models\GoodsAgentEstimatesImages;
use App\Models\GoodsEstimates;
use App\Models\GoodsEstimatesImages;
use App\Models\GoodsEstimatesTag;
use App\Models\OrderGoods;
use App\Models\Orders;
use App\Models\Stores;
use DB;
use App\Models\Users;

class EstimatesBlock
{

    private function estimatesSql($search)
    {
        $goodsModel = new Goods();
        $storeModel = new Stores();
        $EstimatesModel = new GoodsAgentEstimates();
        $prefix = env('DB_PREFIX');
        $sql = DB::table($EstimatesModel->getTable() . ' as e')
            ->select(
                'e.id',
                'e.goods_id',
                'e.store_id',
                'e.sku_value',
//                'e.users_id',
                'e.number',
                'e.content',
                'e.star',
                'e.create_time',
                'g.name as goods_name',
                's.name as store_name',
                's.logo as store_logo'
            )
            ->leftJoin($goodsModel->getTable() . ' as g', 'e.goods_id', '=', 'g.id')
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'e.store_id');
        if (isset($search['goods_id'])) {
            $sql->where('e.goods_id', $search['goods_id']);
        }
        if (isset($search['id'])) {
            $sql->where('e.id', $search['id']);
        }

        return $sql;
    }

    /**
     * 获取商品评论列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function estimatesList($search, $page, $pageSize)
    {
        $sql = $this->estimatesSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $goodsEstimates = $sql->orderBy('id', 'desc')->get();
        $goodsEstimatesImagesModel = new GoodsAgentEstimatesImages();

        foreach ($goodsEstimates as $k => &$v) {
            $v->images = [];
            $goodsImage = $goodsEstimatesImagesModel::where('estimates_id', $v->id)->skip(0)->take(3)->get();
            foreach ($goodsImage as $image) {
                $v->images[] = $image;
            }
        }

        return $goodsEstimates;
    }

    /**
     * 获取商品评论列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function estimatesListPagination(array $search, int $page = 1, int $pageSize = 20)
    {
        $sql = $this->estimatesSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    /**
     * 获取商品评论列表评分信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function estimatesScore(array $search)
    {
        $sql = $this->estimatesSql($search);
        $score = $sql->get();
        $star = [];
        $i = 0;
        foreach ($score as $k=>$v){
            $star[] = $v->star;
            if ($v->star >= 4){
                $i++;
            }
        }
//        dd($star);
        if(count($star) == 0){
            $res = [
                'store' => sprintf("%.1f", 5),
                'feed_back' => '100%'
            ];
        }else{
            $iss = array_sum($star)/count($star);
            $feedBack = $i/count($star);
            $res = [
                'store' => sprintf("%.1f", $iss),
                'feed_back' => ($feedBack*100).'%'
            ];
        }

        return $res;
    }

    /**
     * 获取商品评论详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function estimatesInfo($search)
    {
        $goodsEstimatesImagesModel = new GoodsEstimatesImages();
        $sql = $this->estimatesSql($search);
        $goodsEstimates = $sql->first();
        if ($goodsEstimates) {
            //获取评论图片
            $goodsEstimates->images = [];
            $goodsImage = $goodsEstimatesImagesModel::where('estimates_id', $search['id'])->get();
            foreach ($goodsImage as $image) {
                $goodsEstimates->images[] = $image;
            }
        }
        return $goodsEstimates;
    }


    /**
     * 添加评论
     * @param $data
     * @param $image
     * @param $turnoverId
     * @return int
     */
    public function addEstimatesGoods($data, $image, $turnoverId)
    {
        $goodsEstimatesModel = new GoodsAgentEstimates();
        $goodsEstimatesImageModel = new GoodsAgentEstimatesImages();
        $agentTurnoverModel = new AgentTurnover();
//        验存订单是否存在
        $turnover = DB::table($agentTurnoverModel->getTable())
            ->select('id')
            ->where('id', $turnoverId)
            ->first();
        if (!isset($turnover->id)) {
            return 0;
        }
        try {

            DB::beginTransaction();
//            修改订单状态
            if (DB::table($agentTurnoverModel->getTable())->where('id', $turnoverId)->update(['status' => 4]) === false) {
                DB::rollBack();
                \Log::error('修改订单已评价状态失败');
                return false;
            }

//            添加评论
            foreach ($data as $k => $v) {
                $estimatesId = DB::table($goodsEstimatesModel->getTable())->insertGetId($v);;
                if (!$estimatesId) {
                    DB::rollBack();
                    \Log::error('添加评价失败');
                    return false;

                } else {
                    $imageData[$k]['estimates_id'] = $estimatesId;
                    foreach ($image as $item){
                        if ($item['goods_id'] == $v['goods_id']){
                            $imageData[$k]['images'] = $item['image'];
                        }
                    }

                }
            }
            $image = [];
            foreach ($imageData as $k=>$v){
                if ($v['images'] == ""){
                    unset($imageData[$k]);
                }
                foreach ($v['images'] as $item){
                    $image[] = ['estimates_id' => $v['estimates_id'], 'images' => $item];
                }
            }
            if (DB::table($goodsEstimatesImageModel->getTable())->insert($image) === false) {
                DB::rollBack();
                \Log::error('添加评价图片失败');
                return false;
            }


            DB::commit();
            return true;
        } catch (\Exception $exception) {
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }

}
