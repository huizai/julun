<?php

namespace App\Blocks;
use DB;
use App\Models\Users;
use App\Models\StoreSeatLineup;

class StoreSeatLineupBlock{

    private function __lineupSql($search){
        $userModel          = new Users();
        $storeLineupModel   = new StoreSeatLineup();

        $sql = DB::table($storeLineupModel->getTable().' as sl')->select(
            'sl.*',
            'u.name'
        )->leftJoin($userModel->getTable().' as u','sl.users_id','=','u.id');

        if (isset($search['stores_id'])){
            $sql->where('sl.stores_id',$search['stores_id']);
        }

        if (isset($search['lineup_number'])){
            $sql->where('sl.lineup_number',$search['lineup_number']);
        }

        return $sql->whereNull('sl.delete_time')->orderBy('sl.create_time','desc');
    }

    public function storeLineupList($page, $pageSize, $search){

        $sql = $this->__lineupSql($search);

       return $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();
    }

    public function storeLineupPagination($page, $pageSize, $search){

        $sql = $this->__lineupSql($search);

        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    /**
     * 删除排队信息
     * @param $id
     */
    public function storeLineupDelete($id){
        $storeLineupModel   = new StoreSeatLineup();
        return DB::table($storeLineupModel->getTable())->where('id',$id)->update([
            'delete_time' => date('Y-m-d H:i:s',time())
        ]);
    }

    /**
     * 修改排队状态
     * @param $id
     * @return mixed
     */
    public function storeLineupUpdate($id){
        $storeLineupModel   = new StoreSeatLineup();
        return DB::table($storeLineupModel->getTable())->where('id',$id)->update([
            'is_use' => 1
        ]);
    }
}
