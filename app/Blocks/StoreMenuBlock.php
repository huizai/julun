<?php
namespace App\Blocks;

use App\Exceptions\BlockException;
use App\Models\StoreMenu;
use DB;

class StoreMenuBlock{
    private function __goodsSql($search){
        $storeMenu             = new StoreMenu();
        $sql = DB::table($storeMenu->getTable() . ' as m')
            ->select(
                'm.id',
                'm.store_id',
//                'm.classify_id',
                'm.title',
                DB::raw("convert(price/100, decimal(15,2)) as price"),
                'm.image',
                'm.create_time'
            )
//            ->leftJoin($goodsClassifyModel->getTable() . ' as gc', 'g.classify_id', '=', 'gc.id')
            ->whereNull('m.delete_time');

        if(isset($search['title'])){
            $sql->where('m.title', 'like', "%".$search['title']."%");
        }
        if(isset($search['company'])){
            $sql->where('m.company_id', $search['company']);
        }
        if(isset($search['store_id'])){
            $sql->where('m.store_id', $search['store_id']);
        }
        return $sql;
    }

    /**
     * 获取菜单列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function menusList($search, $page, $pageSize){
        $sql = $this->__goodsSql($search);

        $goods = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
//            ->orderBy('m.sort', 'asc')
            ->orderBy('m.create_time', 'desc')
            ->get();

        return $goods;
    }

    public function menusListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__goodsSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }
}