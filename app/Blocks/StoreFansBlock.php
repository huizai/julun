<?php

namespace App\Blocks;

use App\Models\CollectStores;
use App\Models\StoreTags;
use App\Models\Users;
use DB;


class StoreFansBlock
{
    private function fansSql($search)
    {
        $userModel          = new Users();
        $collectStoresModel = new CollectStores();
        $sql = DB::table($collectStoresModel->getTable() . ' as c')
            ->select(
                'c.id',
                'u.name',
                'u.avatar',
                'u.mobile',
                'u.remarks'
            )
            ->leftJoin($userModel->getTable() . ' as u', 'c.users_id', '=', 'u.id')
            ->whereNull('u.delete_time');
        if (isset($search['name'])) {
            $sql->where('u.name', 'like', "%" . $search['name'] . "%");
        }
        if (isset($search['id'])) {
            $sql->where('c.id', $search['id']);
        }
        if (isset($search['store_id'])) {
            $sql->where('c.stores_id', $search['store_id']);
        }

        return $sql;
    }

    /**
     * 获取商户粉丝列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeFansList($search, $page, $pageSize, $asc = [], $desc = [])
    {
        $sql = $this->fansSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeTags = $sql->orderBy('id', 'desc')->get();

        return $storeTags;
    }

    /**
     * 获取商户粉丝列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeFansListPagination(array $search, int $page = 1, int $pageSize = 20)
    {
        $sql = $this->fansSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户粉丝详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeFansInfo($search)
    {
        $sql = $this->fansSql($search);
        $storeTags = $sql->first();
        return $storeTags;
    }


}
