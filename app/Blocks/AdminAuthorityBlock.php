<?php
namespace App\Blocks;

use App\Models\Admin;
use App\Models\AdminMenu;
use App\Models\AdminRoles;
use App\Models\AdminUser;
use App\Models\AdminUserMenu;
use App\Models\Stores;
use DB;

class AdminAuthorityBlock{

    /**
     * 管理员列表
     * @param $page
     * @param $pageSize
     * @param $username
     * @return array
     */
    public function adminUserList($page, $pageSize, $username){

        $adminModel         = new AdminUser();
        $sql = DB::table($adminModel->getTable())
            ->select('id', 'username', 'status', 'avatar', 'nickname', 'roles_id', 'create_time');

        if (!empty($username)){
            $sql->where('username', "%{$username}%");
        }

        $sql->whereNull('delete_time');

        $total = $sql->count();
        $list = $sql->orderBy('create_time', 'desc')
                ->skip(($page - 1) * $pageSize)
                ->take($pageSize)
                ->get();

        foreach ($list as $value) {
            $value->roles_id = explode(',', $value->roles_id);

            foreach ($value->roles_id as &$item) {
                $item = (int)$item;
            }

            $menu = $this->getRolesMenu($value->roles_id);
            $menuId = '';
            foreach ($menu as $menuValue) {
                $menuId .= $menuValue->menu_id.',';
            }
            $rolesMenu = array_unique(explode(',', rtrim($menuId, ',')));
            foreach ($rolesMenu as &$roles_menu) {
                $roles_menu = (int)$roles_menu;
            }
            $value->roles_menu = $rolesMenu;
        }

        return [
            'list'          => $list,
            'pagination'    => [
                'current'   => $page,
                'pageSize'  => $pageSize,
                'total'     => $total
            ]
        ];
    }


    /**
     * 删除管理员
     * @param $id
     * @return mixed
     */
    public function deleteAdminUser($id){
        $adminModel         = new AdminUser();
        return DB::table($adminModel->getTable())->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * 修改管理员信息
     * @param $id
     * @param $updateData
     * @return mixed
     */
    public function updateAdminUser($id, $updateData){
        $adminModel         = new AdminUser();
        return DB::table($adminModel->getTable())->where('id', $id)->update($updateData);
    }


    /**
     * 获取当前管理员路由
     * @param $adminId
     * @param $type
     * @return mixed
     */
    public function getUserRoute($adminId, $type){
        $adminModel         = new AdminUser();
        $storeModel         = new Stores();
        $rolesModel         = new AdminRoles();
        $userMenuModel      = new AdminUserMenu();
        $adminMenuModel     = new AdminMenu();

        /**
         * 获取角色
         */
        if ($type == 'company'){
            $adminInfo = DB::table($adminModel->getTable())->where('id', $adminId)->value('roles_id');
        }elseif($type == 'store'){
            $adminInfo = DB::table($storeModel->getTable())->where('id', $adminId)->value('roles_id');
        }elseif($type == 'mill'){
            $adminInfo = DB::table($storeModel->getTable())->where('id', $adminId)->value('roles_id');
        }else{
            $adminInfo = 0;
        }

        $rolesId = explode(',', $adminInfo);

        /**
         * 获取角色菜单
         */
        $rolesData = DB::table($rolesModel->getTable())
            ->select('menu_id')
            ->whereIn('id', $rolesId)->get();
        $menu = '';
        foreach ($rolesData as $rolesDatum) {
            $menu .= $rolesDatum->menu_id.',';
        }

        $userMenu = DB::table($userMenuModel->getTable())
            ->where('admin_user_id', $adminId)
            ->where('type', $type)
            ->value('menu_id');

        $menu .= $userMenu;

        $menuId = array_unique(explode(',',$menu));

        return DB::table($adminMenuModel->getTable())
            ->select('id', 'pid', 'alwaysShow', 'path', 'name', 'hidden', 'redirect', 'meta')
            ->whereNull('delete_time')
            ->whereIn('id', $menuId)->get();

    }


    /**
     * 菜单列表
     * @return object
     */
    public function menuList(){
        $menuModel          = new AdminMenu();
        return DB::table($menuModel->getTable())
            ->select(
                'id',
                'pid',
                'alwaysShow',
                'name',
                'hidden',
                'redirect',
                'meta',
                'path',
                'create_time'
            )->whereNull('delete_time')
            ->orderBy('create_time', 'desc')
            ->get();
    }

    /**
     * 添加菜单
     * @param $data
     * @return mixed
     */
    public function menuAdd($data){
        $menuModel          = new AdminMenu();
        return DB::table($menuModel->getTable())->insert($data);
    }

    /**
     * 修改菜单
     * @param $id
     * @param $data
     * @return mixed
     */
    public function menuUpdate($id, $data){
        $menuModel          = new AdminMenu();
        return DB::table($menuModel->getTable())->where('id', $id)->update($data);
    }

    /**
     * 删除菜单
     * @param $id
     * @return mixed
     */
    public function menuDelete($id){
        $menuModel          = new AdminMenu();
        return DB::table($menuModel->getTable())->where('id', $id)->update([
            'delete_time'   => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * 角色列表
     * @return mixed
     */
    public function rolesList(){
        $rolesModel         = new AdminRoles();
        return DB::table($rolesModel->getTable())->whereNull('delete_time')->get();
    }

    /**
     * 角色详情
     * @param $id
     * @return mixed
     */
    public function rolesInfo($id){
        $rolesModel         = new AdminRoles();
        return DB::table($rolesModel->getTable())
            ->where('id', $id)
            ->whereNull('delete_time')->first();
    }

    /**
     * 添加角色
     * @param $name
     * @param $menuId
     * @return mixed
     */
    public function rolesAdd($name, $menuId){
        $rolesModel         = new AdminRoles();
        return DB::table($rolesModel->getTable())
            ->insert([
                'name'      => $name,
                'menu_id'   => $menuId
            ]);
    }

    /**
     * 修改角色
     * @param $id
     * @param $name
     * @param $menuId
     * @return mixed
     */
    public function rolesUpdate($id, $name, $menuId){
        $rolesModel         = new AdminRoles();
        return DB::table($rolesModel->getTable())
            ->where('id', $id)
            ->update([
                'name'      => $name,
                'menu_id'   => $menuId
            ]);
    }

    /**
     * 删除角色
     * @param $id
     * @return mixed
     */
    public function rolesDelete($id){
        $rolesModel         = new AdminRoles();
        return DB::table($rolesModel->getTable())
            ->where('id', $id)
            ->update([
                'delete_time'   => date('Y-m-d H:i:s', time())
            ]);
    }

    /**
     * 获取角色菜单
     * @param $rolesId
     * @return mixed
     */
    public function getRolesMenu($rolesId){
        $adminRolesModel        = new AdminRoles();
        return DB::table($adminRolesModel->getTable())
            ->whereIn('id', $rolesId)
            ->select('menu_id')->get();
    }

    /**
     * 修改或添加管理员所在角色之外的菜单
     * @param $id
     * @param $menuId
     * @return mixed
     */
    public function updateUserRoute($id, $menuId){
        $userMenuModel  = new AdminUserMenu();
        $userMenu       = DB::table($userMenuModel->getTable())->where('admin_user_id', $id)->first();
        if (!$userMenu){
            return DB::table($userMenuModel->getTable())->insert([
                'admin_user_id' => $id,
                'menu_id'       => $menuId,
                'type'          => 'company'
            ]);
        }else{
            return DB::table($userMenuModel->getTable())->where('admin_user_id', $id)->update([
                'menu_id' => $menuId
            ]);
        }

    }
}
