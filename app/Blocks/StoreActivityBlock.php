<?php

namespace App\Blocks;

use App\Models\StoreActivity;
use DB;

class StoreActivityBlock
{


    public function activityList($page, $pageSize, $store_id)
    {

        $storeActivityModel = new StoreActivity();

        $sql = DB::table($storeActivityModel->getTable())
            ->whereNull('delete_time');

        if (!empty($store_id)) {
            $sql->where('stores_id', $store_id);
        }

        $total = $sql->count();
        $list = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();

        foreach ($list as $item) {
            $item->data             = json_decode($item->data, true);
            $item->title            = $item->data['title'];
            $item->start_time       = $item->data['start_time'];
            $item->end_time         = $item->data['end_time'];
            $item->full_amount      = priceIntToFloat($item->data['full_amount']);
            $item->discount_amount  = priceIntToFloat($item->data['discount_amount']);
        }

        return [
            'list' => $list,
            'pagination' => [
                'total' => $total,
                'current' => $page,
                'pageSize' => $pageSize
            ]
        ];
    }


    public function activityUpdate($id, $data, $type)
    {
        $storeActivityModel = new StoreActivity();

        return DB::table($storeActivityModel->getTable())->where('id', $id)->update([
            'type' => $type,
            'data' => $data
        ]);
    }

    public function activityAdd($store_id, $data, $type)
    {
        $storeActivityModel = new StoreActivity();

        return DB::table($storeActivityModel->getTable())->insert([
            'type'              => $type,
            'stores_id'         => $store_id,
            'data'              => $data,
            'create_time'       => date('Y-m-d H:i:s', time())
        ]);
    }

    public function activityDelete($id)
    {
        $storeActivityModel = new StoreActivity();

        return DB::table($storeActivityModel->getTable())->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
    }
}
