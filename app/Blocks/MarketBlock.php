<?php

namespace App\Blocks;


use App\Models\AgentTurnover;
use App\Models\AgentTurnoverGoods;
use App\Models\Goods;
use App\Models\GoodsClassifies;
use App\Models\PageNav;
use App\Models\StoreDevices;
use App\Models\StoreNavs;
use App\Models\StoreTags;
use DB;


class MarketBlock
{
    /**
     * 交易数据统计
     * @param array $data
     * @param int $storeId
     * @return array
     */
    public function turnoverMarket($data, $storeId){
        $turnoverModel = new AgentTurnover();
        $sql = DB::table($turnoverModel->getTable())
            ->select(
                DB::raw("sum(total_price) as money")
            )
            ->whereIn('status', [1, 2, 3, 4])
            ->where('p_id', $storeId);
        if (isset($data['year']) && !isset($data['month'])){
            $sql->addSelect(DB::raw("DATE_FORMAT(create_time, '%c') as time"))
                ->whereBetween('create_time', [$data['year'].'-01-01 00:00:00', ($data['year']+1).'-01-01 00:00:00']);;
        }

        if (isset($data['year']) && isset($data['month'])){
            $sql->addSelect(DB::raw("DATE_FORMAT(create_time, '%e') as time"))
                ->whereBetween('create_time', [$data['year'].'-'.$data['month'].'-01 00:00:00', $data['year'].'-'.($data['month']+1).'-01 00:00:00']);
        }
        $res = $sql->groupBy('time')->get();
//        默认十二月
        $m=range(1,12);
        if (isset($data['month'])){
            $m=range(1, cal_days_in_month(CAL_GREGORIAN, $data['month'], $data['year']));
        }
        $arr = [];
        $totalMoney = 0;
        foreach($m as $v){
            $arr[]=array('money' =>priceIntToFloat(0), 'time' =>$v);
        }
        foreach ($res as $k=>$v){
            foreach ($arr as $key=>$value){
                if($v->time == $value['time']){
                    $arr[$key]['time'] = $v->time;
                    $arr[$key]['money'] = priceIntToFloat($v->money);
                }
            }
            $totalMoney += $v->money;
        }

        foreach ($arr as $key=>&$value){
            $value['total_money'] = priceIntToFloat($totalMoney);
            if ($totalMoney == 0){
                $value['proportion'] = '100%';
            }else{
                $value['proportion'] = sprintf("%.1f",($value['money']/$value['total_money']*100)).'%';
            }

        }
        return $arr;
    }
    /**
     * 商品交易数据排行
     * @param array $data
     * @param int $storeId
     * @return array|string
     */
    public function goodsMarket($data, $page, $pageSize, $storeId){
        $sqlPage = ($page-1)*$pageSize;
        $where = "";
        if (isset($data['cid'])){
            $where .= " and g.classify_id = {$data['cid']}";
        }
        if(isset($data['name'])){
            $where .= " and g.name like '%{$data['name']}%'";
        }
        $orderWhere = "";
        if (isset($data['start_time']) && isset($data['end_time'])){
            $orderWhere .= " and at.create_time between '{$data['start_time']}' and '{$data['end_time']}'";
        }elseif(isset($data['start_time']) && !isset($data['end_time'])){
            $endTime = date('Y-m-d H:i:s', time());
            $orderWhere .= " and at.create_time between '{$data['start_time']}' and '{$endTime}'";
        }elseif (!isset($data['start_time']) && !isset($data['end_time'])){
            $startTime = date('Y-m-d H:i:s', strtotime(date("Y",time())."-1"."-1"));
            $endTime = date('Y-m-d H:i:s', time());
            $orderWhere .= " and at.create_time between '{$startTime}' and '{$endTime}'";
        }
        $list = \Illuminate\Support\Facades\DB::select("
select 
g.id, 
g.name, 
g.stores_id, 
gc.name as class_name, 
g.mill_id, 
g.classify_id, 
(select 
sum(atg.total_price) 
from 
jl_agent_turnover_goods as atg 
left join jl_agent_turnover as at on atg.turnover_id = at.id 
where atg.goods_id = g.id {$orderWhere}
) as total_price,
(select 
sum(atg.number) 
from 
jl_agent_turnover_goods as atg 
left join jl_agent_turnover as at on atg.turnover_id = at.id 
where atg.goods_id = g.id {$orderWhere}
) as number 
from jl_goods as g 
left join jl_goods_classifies as gc on g.classify_id = gc.id 
where g.stores_id = {$storeId} {$where}
order by  number desc
limit {$sqlPage},{$pageSize}");
        $count = \Illuminate\Support\Facades\DB::select("
select 
COUNT(g.id) as total
from jl_goods as g 
left join jl_goods_classifies as gc on g.classify_id = gc.id 
where g.stores_id = {$storeId} {$where}");
        $sqlPage++;
        foreach ($list as $k=>$v){
            $v->rownum = $sqlPage++;
            if ($v->number==null){
                $v->number = 0;
            }
            if ($v->total_price==null){
                $v->total_price = 0;
            }
            $v->total_price = priceIntToFloat($v->total_price);
        }
        return [
            'list' => $list,
            'pagination' => [
                'total' => $count[0]->total,
                'pageSize' => $pageSize,
                'current' => $page
            ]
        ];
    }

    /**
     * 商户交易数据排行
     * @param array $data
     * @param int $storeId
     * @return array|string
     */
    public function storeMarket($data, $page, $pageSize, $storeId){
        $sqlPage = ($page-1)*$pageSize;
        $where = "";
        if(isset($data['name'])){
            $where .= " and s.name like '%{$data['name']}%'";
        }
        $list = \Illuminate\Support\Facades\DB::select("
select 
s.id, 
s.name, 
s.logo,  
(select 
sum(at.total_price) 
from 
jl_agent_turnover as at 
where at.store_id = s.id
) as total_price,
(select 
count(at.id) 
from 
jl_agent_turnover as at 
where at.store_id = s.id and at.p_id = {$storeId}
) as number 
from jl_stores as s 
where s.id != {$storeId} {$where }
order by  number desc
limit {$sqlPage},{$pageSize}");
        $count = \Illuminate\Support\Facades\DB::select("
select 
COUNT(s.id) as total
from jl_stores as s ");
        $sqlPage++;
        foreach ($list as $k=>$v){
            $v->rownum = $sqlPage++;
            if ($v->number==null){
                $v->number = 0;
            }
            if ($v->total_price==null){
                $v->total_price = 0;
            }
            $v->total_price = priceIntToFloat($v->total_price);
        }
        return [
            'list' => $list,
            'pagination' => [
                'total' => $count[0]->total,
                'pageSize' => $pageSize,
                'current' => $page
            ]
        ];
    }
}
