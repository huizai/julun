<?php
namespace App\Blocks;

use App\Models\AgentApply;
use App\Models\AgentTurnover;
use App\Models\AgentTurnoverAddress;
use App\Models\AgentTurnoverGoods;
use App\Models\AgentUser;
use App\Models\Banks;
use App\Models\Goods;
use App\Models\GoodsSku;
use App\Models\GoodsSkuValue;
use App\Models\StoreClassifity;
use App\Models\StoreInfo;
use App\Models\Stores;
use DB;
use App\Models\AgentUserInfo;
use App\Models\District;

class AgentBlock{
//    认证信息填写
    public function setAgentInfo($data, $sotreId){
        try{
            DB::beginTransaction();
            //判断参数
            if (!$sotreId){
                \Log::error('参数错误');
                return false;
            }
//            数据验存
            $storeModel = new Stores();

            $sotre = DB::table($storeModel->getTable())
                ->where('id', $sotreId)
                ->first();
            if (!$sotre){
                \Log::error('商户不存在');
                return false;
            }

            $storeInfoModel = new StoreInfo();

            $sotreInfo = DB::table($storeInfoModel->getTable())
                ->where('store_id', $sotreId)
                ->first();
            if ($sotreInfo){
                if ($sotre->status==1){
                    return ['申请信息正在审核无法修改'];
                }
                if ($sotre->status==2){
                    return ['信息已通过认证无法修改'];
                }

                $sotreInfoId = DB::table($storeInfoModel->getTable())
                    ->where('store_id', $sotreId)
                    ->update($data);

                if ($sotreInfoId ===false){
                    DB::rollBack();
                    \Log::error('认证申请失败');
                    return false;
                }

            }else{
                $sotreInfoId = DB::table($storeInfoModel->getTable())->insertGetId($data);
                if (!$sotreInfoId){
                    DB::rollBack();
                    \Log::error('认证申请失败');
                    return false;
                }
            }
            $storeUsave = DB::table($storeModel->getTable())
                ->where('id', $sotreId)
                ->update(['status' => 1]);
            if (!$storeUsave){
                DB::rollBack();
                \Log::error('商户状态修改失败');
                return false;
            }

            $messageBlock = new MessageBlock();
            $content = "您的认证申请已提交";
            $messageId = $messageBlock->messageAdd($sotreId, $content);

            if (!$messageId){
                DB::rollBack();
                \Log::error('消息添加失败');
                return false;
            }
            DB::commit();
            return $sotreId;

        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }
    //    认证信息审核
    public function examAgent($data){
        try{
            DB::beginTransaction();
            $storeModel = new Stores();
            $store = DB::table($storeModel->getTable())
                ->where('id', $data['id'])
                ->first();
            if (!isset($store->id)){
                DB::rollBack();
                \Log::error('商户信息有误');
                return false;
            }

            if ($store->agent_type == 0){
                $role = 2;
            }else{
                $role = 3;
            }

            //判断参数

            $store = DB::table($storeModel->getTable())
                ->where('id', $data['id'])
                ->update(['status' => $data['status'], 'roles_id' => $role]);
            if ($store === false){
                DB::rollBack();
                \Log::error('审核失败');
                return false;
            }
            $storeInfoModel = new StoreInfo();
            $storeInfo = DB::table($storeInfoModel->getTable())
                ->where('store_id', $data['id'])
                ->update(['remake' => $data['remake']]);
            if ($storeInfo === false){
                DB::rollBack();
                \Log::error('审核失败');
                return false;
            }

            $messageBlock = new MessageBlock();
            if ($data['status']==2){
                $content = "您的认证申请信息已通过，请重新登录";
            }else{
                $content = "您的认证申请信息已被拒绝，拒绝原因：".$data['remake'];
            }

            $messageId = $messageBlock->messageAdd($data['id'], $content);

            if (!$messageId){
                DB::rollBack();
                \Log::error('消息添加失败');
                return false;
            }
            DB::commit();
            return $storeInfo;

        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }
    //    认证信息审核--同意  （废弃）
    public function consentAgent($data){
        $agentModel = new AgentUserInfo();
        $res = DB::table($agentModel->getTable())
            ->where('id', $data['id'])
            ->update(['status'=>1]);
        return $res;
    }

//    代理信息详情
    public function agentInfo($agentId){
        $sql = $this->agentSql(['id' => $agentId]);
        $res = $sql->first();
//        if ($res->status === null){
//            $res->status = 3;
//        }
        return $res;
    }

    private function agentSql($search){
        $storeModel = new Stores();
        $storeInfoModel = new StoreInfo();
        $bankModel = new Banks();
        $districtModel = new District();
        $sql = DB::table($storeModel->getTable() . ' as s')
            ->select(
                'si.id',
                'si.store_id',
                'si.province',
                'si.city',
                'si.district',
                'si.street',
                'si.address',
                'si.organ_type',
                'si.license_num',
                'si.scope',
                'si.longitude',
                'si.latitude',
                'si.business_license',
                'si.intelligence_img',
                'si.trademark_img',
                'si.bank_num',
                'si.bank_username',
                'si.person_name',
                'si.person_number',
                'si.person_id_card_z',
                'si.person_id_card_f',
                'si.remake',
                'si.bank_name as bank_id',
                's.mobile',
                's.account',
                's.real_name',
                's.logo',
                's.main_type',
                's.name',
                's.agent_type',
                's.status',
                'b.bank_name',
                'pd.name as province_name',
                'cd.name as city_name',
                'dd.name as district_name',
                'sd.name as street_name'
            )
            ->leftJoin($storeInfoModel->getTable() . ' as si', 'si.store_id', '=', 's.id')
            ->leftJoin($bankModel->getTable() . ' as b', 'b.id', '=', 'si.bank_name')
            ->leftJoin($districtModel->getTable() . ' as pd', 'pd.id', '=', 'si.province')
            ->leftJoin($districtModel->getTable() . ' as cd', 'cd.id', '=', 'si.city')
            ->leftJoin($districtModel->getTable() . ' as dd', 'dd.id', '=', 'si.district')
            ->leftJoin($districtModel->getTable() . ' as sd', 'sd.id', '=', 'si.street')
            ->whereNull('s.delete_time');

        if(isset($search['id'])){
            $sql->where('s.id', $search['id']);
        }

        if(isset($search['name'])){
            $sql->where('s.name', 'like', "%".$search['name']."%");
        }
        if(isset($search['mobile'])){
            $sql->where('s.mobile', 'like', "%".$search['mobile']."%");
        }
        if(isset($search['main_type'])){
            $sql->where('s.main_type', 'like', "%".$search['main_type']."%");
        }
        if(isset($search['status'])){
            $sql->where('s.status',  $search['status']);
        }


        return $sql;
    }
    /**
     * 获取代理审核列表
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return \Illuminate\Support\Collection
     */
    public function agentAdminList(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->agentSql($search);


        $store = $sql->where('si.id', '<>', null)
            ->skip(($page - 1) * $pageSize)->take($pageSize)
            ->orderBy('s.id', 'desc')
            ->get();
        return $store;
    }


    /**
     * 获取代理审核列表的分页信息
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function agentAdminListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->agentSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    //    代理基本信息
    public function agentMsg($data, $storeId){
        $storeModel = new Stores();
        if (!isset($storeId)){
            return '登录信息有误请重新登录';
        }
        $res = DB::table($storeModel->getTable())
            ->where('id', $storeId)
            ->update($data);
        if ($res !== false){
            return $storeId;
        }else{
            return '修改失败';
        }

    }

//厂家列表
    private function millSql($search, $id=null){
        $storeModel = new Stores();
        $applyModel = new AgentApply();
        $storeInfoModel = new StoreInfo();
        $sql = DB::table($storeModel->getTable() . ' as s')
            ->select(
                's.id',
                's.name',
                's.logo',
                'aa.status',
                'aa.p_id',
                'si.scope'
            )
            ->leftJoin($storeInfoModel->getTable() . ' as si', 'si.store_id', '=', 's.id')
            ->leftJoin($applyModel->getTable() . ' as aa', function ($join ) use ($id){
                $join->on('aa.mill_id', '=', 's.id')
                    ->where('aa.store_id', $id);
            })
            ->whereNull('s.delete_time')
            ->where('s.agent_type', 0)
            ->where('s.agency', 1)
            ->where('s.status', 2);
        if(isset($search['id'])){
            $sql->where('s.id', $search['id']);
        }
        if(isset($search['name'])){
            $sql->where('s.name', 'like', "%".$search['name']."%");
        }
        if(isset($search['status'])){
            if ($search['status']==3){
                $search['status'] = null;
                $sql->whereNull('aa.status');
            }else{
                $sql->where('aa.status', $search['status']);
            }

        }
        return $sql;
    }

    /**
     * 获取厂家列表
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return \Illuminate\Support\Collection
     */
    public function millList(array $search, int $page = 1, int $pageSize = 20, $id){
        $sql = $this->millSql($search, $id);
        $store = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->orderBy('s.id', 'desc')
            ->get();
        $store = json_decode(json_encode($store),true);
        foreach ($store as $k=>&$v){
            if ($v['status'] === null){
                $v['status'] = 3;
            }
        }
        return $store;
    }


    /**
     * 获取厂家列表的分页信息
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @param int $id
     * @return array
     */
    public function millListPagination(array $search, int $page = 1, int $pageSize = 20, $id){
        $sql = $this->millSql($search, $id);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    public function applySql(){
        $storeModel = new Stores();
        $agentApplyModel = new AgentApply();
        $storeInfoModel = new StoreInfo();
        $sql = DB::table($agentApplyModel->getTable(). ' as aa')
            ->select(
                'aa.store_id'
            )
            ->leftJoin($storeModel->getTable() . ' as s', 'aa.store_id', '=', 's.id')
            ->leftJoin($storeInfoModel->getTable() . ' as si', 'si.store_id', '=', 's.id')
            ->where('aa.status', 1)
            ->whereNull('s.delete_time');
        return $sql;
    }

    /**
     * 申请代理
     * @param array $data
     * @param object $agent
     * @return string|int
     */
    public function agentApply(array $data,$agent){
        $agentApplyModel = new AgentApply();

        $agentSql = $this->applySql();//是否存在此地区的代理
        $pSql = $this->applySql();//确认上级

        //判断这个等级的代理是否存在
        if ($data['level']==3){
            $agentSql->where('si.province',$agent->province)
                ->where('si.city',$agent->city)
                ->where('si.district',$agent->district)
                ->where('aa.mill_id', $data['mill_id']);
        }elseif ($data['level']==2){
            $agentSql->where('si.province',$agent->province)
                ->where('si.city',$agent->city)
                ->where('aa.mill_id', $data['mill_id']);
        }elseif ($data['level']==1){
            $agentSql->where('si.province',$agent->province)
                ->where('aa.mill_id', $data['mill_id']);
        }elseif ($data['level']==4){
            $agentSql->where('si.province',$agent->province)
                ->where('si.city',$agent->city)
                ->where('si.district',$agent->district)
                ->where('si.street',$agent->district)
                ->where('aa.mill_id', $data['mill_id']);
        }
        $issetAgent = $agentSql ->where('aa.level', $data['level'])->first();
        if ($issetAgent){
            return '此地区已有代理';
        }

        //判断是否申请过这个厂家的代理
        $applyInfo = DB::table($agentApplyModel->getTable())
            ->where('store_id', $data['store_id'])
            ->where('mill_id', $data['mill_id'])
            ->first();
        if ($applyInfo){
            if ($applyInfo->status==0){
                return '已申请该厂家代理，请勿重复申请';
            }
            if ($applyInfo->status==1){
                return '信息已通过审核无法修改';
            }
            $applyInfoId = $applyInfo->id;
            $data['status'] = 0;
            $data['remake'] = '';
            DB::table($agentApplyModel->getTable())
                ->where('id', $applyInfo->id)
                ->update($data);
            return $applyInfoId;
        }

        //获取上级的id
        if ($data['level']==4){
            $pSql->where('si.province',$agent->province)
                ->where('si.city',$agent->city)
                ->where('si.district',$agent->district)
                ->where('aa.mill_id', $data['mill_id'])
                ->where('aa.level', 3);
        }elseif ($data['level']==3){
            $pSql->where('si.province',$agent->province)
                ->where('si.city',$agent->city)
                ->where('aa.mill_id', $data['mill_id'])
                ->where('aa.level', 2);
        }elseif ($data['level']==2){
            $pSql->where('si.province',$agent->province)
                ->where('aa.mill_id', $data['mill_id'])
                ->where('aa.level', 1);
        }elseif ($data['level']==1){
            $pSql->where('s.id', $data['mill_id']);
        }
        $pAgent = $pSql->first();
        if ($pAgent){
            $data['p_id'] = $pAgent->store_id;
        }
        if (!isset($data['p_id'])){
            $data['p_id'] = $data['mill_id'];
        }

        try{

            DB::beginTransaction();
            //插入数据表
            $res = DB::table($agentApplyModel->getTable())->insertGetId(
                $data
            );
            if (!$res){
                DB::rollBack();
                \Log::error('申请信息添加失败');
                return false;
            }
//            查询厂家信息
            $storeModel = new Stores();
            $mill = DB::table($storeModel->getTable())->where('id', $data['mill_id'])->first();
            if (!$mill){
                DB::rollBack();
                \Log::error('厂家信息有误');
                return false;
            }
            $messageBlock = new MessageBlock();
            $content = "您向【".$mill->name."】厂家的代理申请已提交";
            $messageId = $messageBlock->messageAdd($data['store_id'], $content);

            if (!$messageId){
                DB::rollBack();
                \Log::error('消息添加失败');
                return false;
            }
            DB::commit();
            return $res;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error('申请失败，原因：'.$exception);
            return false;
        }
    }

    //申请代理列表
    private function agentExamSql($search){
        $storeModel = new Stores();
        $agentApplyModel = new AgentApply();
        $storeInfoModel = new StoreInfo();
        $sql = DB::table($agentApplyModel->getTable() . ' as aa')
            ->select(
                'aa.id',
                's.id as store_id',
                's.mobile',
                's.agent_type',
                's.name',
                'si.scope',
                'si.province',
                'si.district',
                'si.city',
                'si.street',
                'aa.level',
                'aa.mill_id',
                'aa.status'
            )
            ->leftJoin($storeModel->getTable() . ' as s', 'aa.store_id', '=', 's.id')
            ->leftJoin($storeInfoModel->getTable() . ' as si', 'si.store_id', '=', 's.id')
            ->whereNull('s.delete_time');
        if (isset($search['id'])){
            $sql->where('s.id', $search['id']);
        }
        if (isset($search['name'])){
            $sql->where('s.name', 'like', "%".$search['name']."%");
        }
        if (isset($search['p_id'])){
            $sql->where('aa.p_id', $search['p_id']);
        }

        if (isset($search['status'])){
            $sql->where('aa.status', $search['status']);
        }

        if (isset($search['mill_id'])){
            $sql->where('aa.mill_id', $search['mill_id']);
        }
        return $sql;
    }

    /**
     * 获取申请代理列表
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return \Illuminate\Support\Collection
     */
    public function agentExamList(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->agentExamSql($search);
        $store = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->orderBy('s.create_time', 'desc')
            ->get();
        return $store;
    }


    /**
     * 获取申请代理列表的分页信息
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function agentExamListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->agentExamSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取申请信息详情
     * @param int $storeId
     * @param int $millId
     * @return object
     */
    public function applyInfo($storeId, $millId){
        $storeModel         = new Stores();
        $agentApplyModel    = new AgentApply();
        $storeInfoModel     = new StoreInfo();
        $districtModel      = new District();
        $bankModel          = new Banks();
        $agentApplyInfo = DB::table($agentApplyModel->getTable(). ' as aa')
            ->select(
                'aa.id',
                'aa.mill_id',
                'aa.store_id',
                'aa.level',
                'aa.status',
                'aa.remake',
                'pd.name as province_name',
                'cd.name as city_name',
                'dd.name as district_name',
                'sd.name as street_name',
                'si.address',
                's.real_name',
                's.name',
                's.mobile',
                's.agent_type',
                'si.organ_type',
                'si.license_num',
                'si.scope',
                'si.province',
                'si.city',
                'si.district',
                'si.street',
                'si.business_license',
                'si.intelligence_img',
                'si.trademark_img',
                'si.bank_num',
                'si.bank_username',
                'b.bank_name',
                'si.person_name',
                'si.person_number',
                'si.person_id_card_z',
                'si.person_id_card_f',
                's.main_type'
            )
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'aa.store_id')
            ->leftJoin($storeInfoModel->getTable() . ' as si', 'si.store_id', '=', 's.id')
            ->leftJoin($bankModel->getTable() . ' as b', 'si.bank_name', '=', 'b.id')
            ->leftJoin($districtModel->getTable() . ' as pd', 'pd.id', '=', 'si.province')
            ->leftJoin($districtModel->getTable() . ' as cd', 'cd.id', '=', 'si.city')
            ->leftJoin($districtModel->getTable() . ' as dd', 'dd.id', '=', 'si.district')
            ->leftJoin($districtModel->getTable() . ' as sd', 'sd.id', '=', 'si.street')
            ->where('aa.mill_id', $millId)
            ->where('aa.store_id', $storeId)
            ->whereNull('s.delete_time')
            ->first();
        return $agentApplyInfo;
    }


    //代理厂家列表
    private function applyMillSql($search){
        $storeModel = new Stores();
        $agentApplyModel = new AgentApply();
        $storeInfoModel = new StoreInfo();

        $sql = DB::table($agentApplyModel->getTable() . ' as aa')
            ->select(
                's.logo',
                's.name',
                'aa.level',
                'aa.mill_id',
                'aa.p_id'
            )
            ->leftJoin($storeModel->getTable() . ' as s', 'aa.mill_id', '=', 's.id')
            ->leftJoin($storeInfoModel->getTable() . ' as si', 'si.store_id', '=', 's.id')
            ->where('s.agent_type', 0)
            ->whereNull('s.delete_time');

        if (isset($search['store_id'])){
            $sql->where('aa.store_id', $search['store_id'])->where('aa.status', 1);
        }
        return $sql;
    }
    /**
     * 获取代理已代理的厂家列表
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return \Illuminate\Support\Collection
     */
    public function applyMillList(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->applyMillSql($search);

        $store = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->orderBy('s.create_time', 'desc')
            ->get();
        return $store;
    }


    /**
     * 获取代理已代理的厂家列表的分页信息
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function applyMillListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->applyMillSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }



    private function turnoverSql($search){
        $agentTurnoverModel         = new AgentTurnover();
        $storeModel                 = new Stores();
        $storeInfoModel             = new StoreInfo();
        $bankModel                  = new Banks();
        $districtModel              = new District();
        $agentTurnoverAddressModel  = new AgentTurnoverAddress();
//        $goodSukModel = new GoodsSku();
        $prefix = env('DB_PREFIX');
        $sql = DB::table($agentTurnoverModel->getTable() . ' as at')
            ->select(
                'at.id',
                'at.mill_id',
                'at.p_id',
                'at.turnover_no',
                'at.number',
                'at.ture_number',
                'at.pay_type',
                'at.pay_image',
                'at.send_company',
                'at.send_member',
                'at.send_mobile',
                'at.send_image',
                'at.send_no',
                'at.remake',
                DB::raw("convert({$prefix}at.total_price/100, decimal(15,2)) as total_price"),
//                'at.total_price',
                'at.status',
                'at.create_time',
                's.real_name',
                's.mobile',
                's.logo',
                's.name',
                'p.real_name as p_real_name',
                'p.mobile as p_mobile',
                'p.logo as p_logo',
                'p.name as p_name',
                'p.freight_type as freight',
                'pi.bank_num as p_bank_num',
                'pi.bank_username as p_bank_username',
                'pi.bank_name as p_bank_id',
                'b.bank_name',
                'm.real_name as m_real_name',
                'm.mobile as m_mobile',
                'm.logo as m_logo',
                'm.name as m_name',
                'ata.consignee',
                'ata.contact',
                'ata.address',
                'pd.name as province_name',
                'cd.name as city_name',
                'dd.name as district_name',
                'at.fahuo_time'
            )
            ->leftJoin($storeModel->getTable() . ' as s', 'at.store_id', '=', 's.id')
            ->leftJoin($storeModel->getTable() . ' as p', 'at.p_id', '=', 'p.id')
            ->leftJoin($storeInfoModel->getTable() . ' as pi', 'pi.store_id', '=', 'p.id')
            ->leftJoin($bankModel->getTable() . ' as b', 'pi.bank_name', '=', 'b.id')
            ->leftJoin($storeModel->getTable() . ' as m', 'at.mill_id', '=', 'm.id')
            ->leftJoin($agentTurnoverAddressModel->getTable() . ' as ata', 'ata.turnover_id', '=', 'at.id')
            ->leftJoin($districtModel->getTable() . ' as pd', 'pd.id', '=', 'ata.province')
            ->leftJoin($districtModel->getTable() . ' as cd', 'cd.id', '=', 'ata.city')
            ->leftJoin($districtModel->getTable() . ' as dd', 'dd.id', '=', 'ata.district');
        if (isset($search['turnover_type'])){
            if ($search['turnover_type']==0){
                $sql->where('at.store_id', $search['store_id']);
            }else{
                $sql->where('at.p_id', $search['store_id']);
            }
        }

        if (isset($search['status'])){
            $sql->where('at.status', $search['status']);
        }
        if (isset($search['id'])){
            $sql->where('at.id', $search['id']);
        }
        if (isset($search['turnover_no'])){
            $sql->where('at.turnover_no', $search['turnover_no']);
        }

        return $sql;

    }

    /**
     * 获取出入库订单列表
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return \Illuminate\Support\Collection
     */
    public function turnoverList(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->turnoverSql($search);
        $turnover = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->orderBy('at.create_time', 'desc')
            ->get();
//        组合商品数据
        if ($turnover){
            $agentTurnoverGoodsModel = new AgentTurnoverGoods();
            $goodsModel = new Goods();
            $sukModel = new GoodsSku();
            $prefix = env('DB_PREFIX');
            foreach ($turnover as $k=>$v){
                $v->goods = [];
                $turnoverGoods = DB::table($agentTurnoverGoodsModel->getTable().' as atg')
                    ->select(
                        'g.id',
                        'g.name',
                        'g.thumbnail',
                        DB::raw("convert({$prefix}atg.price/100, decimal(15,2)) as price"),
                        DB::raw("convert({$prefix}atg.total_price/100, decimal(15,2)) as total_price"),
                        'atg.sku_value',
                        'atg.number'
                    )
                    ->leftJoin($goodsModel->getTable() . ' as g', 'g.id', '=', 'atg.goods_id')
                    ->where('turnover_id', $v->id)
                    ->get();
                foreach ($turnoverGoods as $key=>$val){
                    if (empty($data['sku'][$k]['sku_path']) && empty($data['sku'][$k]['sku_value'])){
//              没有规格取商品价格
                        $total = $val->price * $val->number;
                        $price = $val->price;
                    }else{
//              有规格时按规格价格
                        $price = $sukModel::where('value_path', $v->sku_path)->first();
                        $total = priceIntToFloat($price->selling_price*$data['sku'][$k]['quantity']);
                        $price = priceIntToFloat($price->selling_price);
                    }
                    $v->goods[$key]['id'] = $val->id;
                    $v->goods[$key]['name'] = $val->name;
                    $v->goods[$key]['sku_value'] = $val->sku_value;
                    $v->goods[$key]['price'] = $price;
                    $v->goods[$key]['total'] = $total;
                    $v->goods[$key]['quantity'] = $val->number;
                    $v->goods[$key]['thumbnail'] = $val->thumbnail;
                }
            }

        }


        return $turnover;
    }


    /**
     * 获取出入库订单列表的分页信息
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function turnoverListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->turnoverSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    /**
     * 出入库订单详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function turnoverInfo($search){
        $sql = $this->turnoverSql($search);
        $turnover = $sql->first();
        $agentTurnoverGoodsModel = new AgentTurnoverGoods();
        $goodsModel = new Goods();
        $goodSkuModel = new GoodsSku();
        if($turnover){
            //获取订单商品
            $prefix = env('DB_PREFIX');
            $turnoverGoods = DB::table($agentTurnoverGoodsModel->getTable().' as atg')
                ->select(
                    'g.id',
                    'g.name',
                    'g.thumbnail',
                    DB::raw("convert({$prefix}atg.price/100, decimal(15,2)) as price"),
                    DB::raw("convert({$prefix}atg.total_price/100, decimal(15,2)) as total_price"),
                    'atg.sku_value',
                    'atg.number'
                )
                ->leftJoin($goodsModel->getTable() . ' as g', 'g.id', '=', 'atg.goods_id')
                ->where('turnover_id', $turnover->id)
                ->get();
            $turnover->goods_species = 0;
                foreach ($turnoverGoods as $key=>$val) {

                    if (empty($turnover->sku_path) && empty($turnover->sku_value)) {
                        //              没有规格取商品价格
                        $total = $val->price * $val->number;
                        $price = $val->price;
                    } else {
                        //              有规格时按规格价格
                        $price = $goodSkuModel::where('value_path', $turnover->sku_path)->first();
                        $total = priceIntToFloat($price->selling_price * $val->quantity);
                        $price = priceIntToFloat($price->selling_price);
                    }
                    $goods[$key]['id'] = $val->id;
                    $goods[$key]['name'] = $val->name;
                    $goods[$key]['sku_value'] = $val->sku_value;
                    $goods[$key]['price'] = $price;
                    $goods[$key]['total'] = $total;
                    $goods[$key]['quantity'] = $val->number;
                    $goods[$key]['thumbnail'] = $val->thumbnail;

                    $turnover->goods_species++;
                }

            $turnover->goods = $goods;

//            剩余支付时间
            $remainingPaymentTime = strtotime($turnover->create_time )+1800;
            if (time()<$remainingPaymentTime){
                $turnover->remaining_payment_time = $remainingPaymentTime - time();
            }else{
                $turnover->remaining_payment_time = 0;
            }
        }

        return $turnover;
    }

//    首页信息展示
    public function main($agentId){
//        $agentModel         = new AgentUser();
//        $agentApplyModel         = new AgentApply();
//        $agentTurnoverModel = new AgentTurnover();
////        账号信息
//        $agent = DB::table($agentModel->getTable())->where('id', $agentId)->first();
////        下级代理数量
//        $agentNum = DB::table($agentApplyModel->getTable())
//            ->where('p_id', $agentId)
//            ->count();
////       待发货订单
//        $turnOrder = DB::table($agentTurnoverModel->getTable() )
//            ->where('p_id', $agentId)
//            ->where('status', 1)
//            ->count();
////       待审核订单
//        $examOrder = DB::table($agentTurnoverModel->getTable())
//            ->where('agent_id', $agentId)
//            ->where('status', 0)
//            ->count();
////       待收货订单
//        $overOrder = DB::table($agentTurnoverModel->getTable())
//            ->where('agent_id', $agentId)
//            ->where('status', 1)
//            ->count();
//       待审核代理
//        $examAgent = DB::table($agentModel->getTable())
//            ->where('agent_id', $agentId)
//            ->where('status', 1)
//            ->count();
//        $data =  [
//            'agent'           => [
//                'username'    => $agent->username,
//                'mobile'      => $agent->mobile,
//                'company'     => $agent->company,
//                'agent_num'   => $agentNum,
//                'agent_type'  => $agent->agent_type
//            ],
//            'turn'            => $turnOrder,
//            'exam'            => $examOrder,
//            'over'            => $overOrder,
//
//        ];
        $data =  [
            'agent'           => [
                'username'    => "测试",
                'mobile'      => "15074766120",
                'company'     => "华丰",
                'agent_num'   => "10",
                'agent_type'  => 0
            ],
            'turn'            => 2,
            'exam'            => 3,
            'over'            => 4,

        ];
        return $data;
    }

    //    基本信息详情
    public function agentMsgInfo($storeId){
        $storeModel         = new Stores();
        $storeClassModel    = new StoreClassifity();
        $sql = DB::table($storeModel->getTable() . ' as s')
            ->select(
                's.id',
                's.mobile',
                's.account',
                's.company_id',
                's.agent_type',
                's.status',
                's.classifity_id',
                's.freight_type as freight',
                's.logo',
                's.main_type',
                's.real_name',
                's.name',
                's.operate',
                's.agency',
                'sc.name as class_name'

            )
            ->leftJoin($storeClassModel->getTable() . ' as sc', 'sc.id', '=', 's.classifity_id')
            ->where('s.id', $storeId)
            ->whereNull('s.delete_time');
        $res = $sql->first();
        return $res;
    }


    /**
     * 下级代理收录列表
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return \Illuminate\Support\Collection
     */
    public function employAgentList(array $search, int $page = 1, int $pageSize = 20){
        $param = [
//            'name' => $search['name'],
            'mill_id' => $search['mill_id'],
        ];
        $sql = $this->agentExamSql($param);
//        获取自己所在位置与等级
        $store = $this->applyInfo($search['id'], $search['mill_id']);
//        获取where条件
        if ($store->level == 4){
            $sql->where('province', $store->province)
                ->where('city', $store->city)
                ->where('district', $store->district)
                ->where('street', $store->street)
                ->where('level', 5);
        }elseif ($store->level == 3){
            $sql->where('province', $store->province)
                ->where('city', $store->city)
                ->where('district', $store->district)
                ->where('level', 4);
        }elseif ($store->level == 2){
            $sql->where('province', $store->province)
                ->where('city', $store->city)
                ->where('level', 3);
        }elseif ($store->level == 1){
            $sql->where('province', $store->province)
                ->where('level', 2);
        }
        $store = $sql->where('p_id', '!=', $store->store_id)
            ->where('aa.status', 1)
            ->skip(($page - 1) * $pageSize)
            ->take($pageSize)
            ->orderBy('s.create_time', 'desc')
            ->get();
        return $store;
    }


    /**
     * 获取下级代理收录列表的分页信息
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function employAgentListPagination(array $search, int $page = 1, int $pageSize = 20){
        $param = [
//            'name' => $search['name'],
            'mill_id' => $search['mill_id'],
        ];
        $sql = $this->agentExamSql($param);
//        获取自己所在位置与等级
        $store = $this->applyInfo($search['id'], $search['mill_id']);
//        获取where条件
        if ($store->level == 4){
            $sql->where('province', $store->province)
                ->where('city', $store->city)
                ->where('district', $store->district)
                ->where('street', $store->street)
                ->where('level', 5);
        }elseif ($store->level == 3){
            $sql->where('province', $store->province)
                ->where('city', $store->city)
                ->where('district', $store->district)
                ->where('level', 4);
        }elseif ($store->level == 2){
            $sql->where('province', $store->province)
                ->where('city', $store->city)
                ->where('level', 3);
        }elseif ($store->level == 1){

            $sql->where('province', $store->province)
                ->where('level', 2);
        }
        return [
            'total' => $sql->where('p_id', '!=', $store->store_id)->where('aa.status', 1)->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

}