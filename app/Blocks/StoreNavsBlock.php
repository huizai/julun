<?php

namespace App\Blocks;


use App\Models\StoreDevices;
use App\Models\StoreNavs;
use App\Models\StoreTags;
use DB;


class StoreNavsBlock
{
    private function navSql($search)
    {
        $storeNavsModel = new StoreNavs();
        $sql = DB::table($storeNavsModel->getTable())
            ->whereNull('delete_time');
        if (isset($search['name'])) {
            $sql->where('name', 'like', "%" . $search['name'] . "%");
        }
        if (isset($search['id'])) {
            $sql->where('id', $search['id']);
        }
        if (isset($search['store_id'])) {
            $sql->where('stores_id', $search['store_id']);
        }

        return $sql;
    }

    /**
     * 获取商户导航列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeNavsList($search, $page, $pageSize, $asc = [], $desc = [])
    {
        $sql = $this->navSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeNavs = $sql->orderBy('id', 'desc')->get();

        return $storeNavs;
    }

    /**
     * 获取商户导航列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeNavsListPagination(array $search, int $page = 1, int $pageSize = 20)
    {
        $sql = $this->navSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }



}
