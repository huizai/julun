<?php
namespace App\Blocks;




use App\Models\Tags;
use DB;


class TagsBlock{
    private function tagSql($search){
        $tagsModel = new Tags();
        $sql = DB::table($tagsModel->getTable());
        if(isset($search['name'])){
            $sql->where('tag_name', 'like', "%".$search['name']."%");
        }
        if(isset($search['id'])){
            $sql->where('id', $search['id']);
        }

        return $sql;
    }
    /**
     * 获取商户服务列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function tagsList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->tagSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $tags = $sql->orderBy('id', 'desc')->get();

        return $tags;
    }

    /**
     * 获取商户服务列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function tagsListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->tagSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户服务详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function tagsInfo($search){
        $sql = $this->tagSql($search);
        $tags = $sql->first();
        return $tags;
    }

}
