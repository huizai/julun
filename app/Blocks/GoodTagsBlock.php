<?php

namespace App\Blocks;


use App\Models\GoodsTags;
use DB;


class GoodTagsBlock
{
    private function tagSql($search)
    {
        $storeTagsModel = new GoodsTags();
        $sql = DB::table($storeTagsModel->getTable());
        if (isset($search['name'])) {
            $sql->where('tag_name', 'like', "%" . $search['name'] . "%");
        }
        if (isset($search['id'])) {
            $sql->where('id', $search['id']);
        }
        if (isset($search['store_id'])) {
            $sql->where('stores_id', $search['store_id']);
        }

        return $sql;
    }

    /**
     * 获取商品标签列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function goodTagsList($search, $page, $pageSize, $asc = [], $desc = [])
    {
        $sql = $this->tagSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeTags = $sql->orderBy('id', 'desc')->get();

        return $storeTags;
    }

    /**
     * 获取商品标签列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function goodTagsListPagination(array $search, int $page = 1, int $pageSize = 20)
    {
        $sql = $this->tagSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商品标签详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function goodTagsInfo($search)
    {
        $sql = $this->tagSql($search);
        $storeTags = $sql->first();
        return $storeTags;
    }


}
