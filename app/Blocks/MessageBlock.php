<?php
namespace App\Blocks;




use App\Models\Message;
use DB;


class MessageBlock{
    private function messageSql($search){
        $messageModel = new Message();
        $sql = DB::table($messageModel->getTable());

        if(isset($search['is_read'])){
            $sql->where('is_read', 0);
        }
        if(isset($search['store_id'])){
            $sql->where('store_id', $search['store_id']);
        }

        return $sql;
    }
    /**
     * 获取消息列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function messageList($search, $page, $pageSize){

        $sql = $this->messageSql($search);

        $message = $sql->skip(($page - 1) * $pageSize)
            ->take($pageSize)
            ->orderBy('id', 'desc')
            ->get();

        return $message;
    }

    /**
     * 获取消息列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function messageListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->messageSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

//
//    /**
//     * 获取消息详情
//     * @param $search
//     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
//     */
//    public function messageInfo($search){
//        $sql = $this->messageSql($search);
//        $message = $sql->first();
//        return $message;
//    }

    /**
     * 消息添加
     * @param $storeId
     * @param $content
     * @param $type
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function messageAdd($storeId, $content){
        $messageModel = new Message();
        $messageId = DB::table($messageModel->getTable())
            ->insertGetId([
                'store_id'      => $storeId,
                'content'       => $content,
                'create_time'   => date('Y-m-d H:i:s', time())
            ]);
        return $messageId;
    }

    /**
     * 消息改为已读
     * @param $storeId
     * @param $content
     * @param $type
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function messageRead($storeId){
        $messageModel = new Message();
        $message = DB::table($messageModel->getTable())
            ->where('store_id', $storeId)
            ->update([
                'is_read' => 1,
            ]);

        return $message;
    }


}
