<?php
namespace App\Blocks;



use App\Models\Goods;
use App\Models\GoodsEstimates;
use App\Models\GoodsEstimatesImages;
use App\Models\GoodsEstimatesTag;
use App\Models\StoreImage;
use DB;
use App\Models\Users;

class StoreImageBlock{

    private function imageSql($search){
        $storeImageModel   = new StoreImage();
        $sql = DB::table($storeImageModel->getTable());


        if(isset($search['store_id'])){
            $sql->where('store_id', $search['store_id']);
        }
        if(isset($search['id'])){
            $sql->where('ge.id', $search['id']);
        }

        return $sql;
    }
    /**
     * 获取商户相册列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function imageList($search, $page, $pageSize){
        $sql = $this->imageSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeImage = $sql->orderBy('id', 'desc')->get();


        return $storeImage;
    }

    /**
     * 获取商户相册列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function imageListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->imageSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }




}