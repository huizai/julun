<?php
namespace App\Blocks;


use App\Models\Banners;
use Illuminate\Support\Facades\DB;

class BannerBlock{
    private function __bannerSql($search){
        $bannersModel             = new Banners();

        $sql = DB::table($bannersModel->getTable());

        if(isset($search['position'])){
            $sql->where('position',$search['position']);
        }
        if(isset($search['company'])){
            $sql->where('company_id', $search['company']);
        }
        return $sql;
    }

    /**
     * 获取商品列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function bannersList($search, $page, $pageSize){
        $sql = $this->__bannerSql($search);

        $goods = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->orderBy('sort', 'asc')
            ->get();

        return $goods;
    }

    public function bannersListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__bannerSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }
}