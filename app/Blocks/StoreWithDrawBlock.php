<?php
namespace App\Blocks;

use App\Models\StoreIncomeLog;
use App\Models\StoreInfo;
use App\Models\StoreTurnover;
use App\Models\StoreWithdraw;
use App\Models\Stores;
use Illuminate\Support\Facades\DB;

class StoreWithDrawBlock{

    private function __withdrawSql($searchData){
        $withdrawModel  = new StoreWithdraw();
        $storeModel     = new Stores();
        $storeInfoModel = new StoreInfo();
        $sql = DB::table($withdrawModel->getTable().' as w')
            ->select(
                'w.id',
                's.name',
                'w.status',
                'w.create_time',
                'w.stores_id',
                'w.remarks',
                DB::raw("convert(money/100, decimal(15,2)) as money"),
                DB::raw("convert(discount_money/100, decimal(15,2)) as discount_money"),
                'w.time'
            )
            ->leftJoin($storeModel->getTable().' as s','w.stores_id','=','s.id')
            ->leftJoin($storeInfoModel->getTable(). ' as si', 'si.store_id', '=', 'w.stores_id');

        if (isset($searchData['store_id'])){
            $sql->where('w.stores_id',$searchData['store_id']);
        }

        if (isset($searchData['status'])){
            $sql->where('w.status',$searchData['status']);
        }

        return $sql->orderByDesc('create_time');
    }

    public function withdrawList($page, $pageSize, $searchData){
        $sql = $this->__withdrawSql($searchData);
        return $sql->skip(($page - 1) * $pageSize)
            ->take($pageSize)
            ->get();
    }

    public function withdrawPagination($page, $pageSize, $searchData){
        $sql = $this->__withdrawSql($searchData);
        return [
            'total'     => $sql->count(),
            'page'      => $page,
            'pageSize'  => $pageSize
        ];
    }

    /**
     * 修改提现状态
     * @param $id
     * @param $status
     * @return mixed
     */
    public function updateWithdrawStatus($data){
        $withdrawModel      = new StoreWithdraw();
        $turnoverModel      = new StoreTurnover();
        $storeModel         = new Stores();
        $storeIncomeModel   = new StoreIncomeLog();

        try{

            DB::beginTransaction();
            $withdraw = DB::table($withdrawModel->getTable())
                ->where('id', $data['id'])
                ->first();

            $turnoverId = explode(',',$withdraw->turnover_id);

            $saveData = ['status' => $data['status']];
            if ($data['status'] == 2){
                $saveData['remarks'] = $data['remarks'];
            }
            if(!DB::table($withdrawModel->getTable())
                ->where('id', $data['id'])
                ->update($saveData)
            ){
                \Log::debug('修改提现状态失败');
                DB::rollBack();
                return false;
            }

            /**
             * 同意 || 打款
             */
            if ($data['status'] == 1){

                if (!DB::table($turnoverModel->getTable())
                    ->whereIn('id', $turnoverId)
                    ->update(['status' => 1, 'is_withdraw' => 1])
                ){
                    \Log::debug('修改商户营业额提现状态失败');
                    DB::rollBack();
                    return false;
                }

                if(!DB::table($storeIncomeModel->getTable())
                    ->insert([
                        'stores_id'     => $withdraw->stores_id,
                        'orders_id'     => 0,
                        'money'         => $withdraw->money,
                        'type'          => 2,
                        'remarks'       => '提现'
                    ])
                ){
                    \Log::debug('添加商户余额日志失败');
                    DB::rollBack();
                    return false;
                }
            }

            /**
             * 驳回
             */
            if ($data['status'] == 2){

                if (!DB::table($storeModel->getTable())
                    ->where('id', $withdraw->stores_id)
                    ->increment('balance', $withdraw->money)
                ){
                    \Log::debug('修改商户余额失败');
                    DB::rollBack();
                    return false;
                }

                if (!DB::table($turnoverModel->getTable())
                    ->whereIn('id', $turnoverId)
                    ->update(['status' => 2])
                ){
                    \Log::debug('修改商户营业额提现状态失败');
                    DB::rollBack();
                    return false;
                }
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }


    }

    public function getStoreTurnover($storeId, $time){
        $storeTurnoverModel     = new StoreTurnover();
        return DB::table($storeTurnoverModel->getTable())
            ->select(
                'id',
                DB::raw("convert(turnover/100, decimal(15,2)) as convert_turnover"),
                'turnover',
                'date',
                'is_withdraw',
                'status'
            )
            ->where('stores_id', $storeId)
            ->whereBetween('date', $time)
            ->orderByDesc('date')
            ->get();
    }

    public function applicationWithdraw($storeId, $data){
        $storeTurnoverModel     = new StoreTurnover();
        $storeWithdrawModel     = new StoreWithdraw();
        $storeModel             = new Stores();

        $money      = 0;
        $time       = '';
        $turnoverId = [];

        foreach ($data as $key => $item) {
            $money += $item['turnover'];
            $turnoverId[] = $item['id'];
            $time .=  $item['date'].',';
        }

        $time = rtrim($time, ',');
        $discountMoney = round($money * 0.84);

        try{
            DB::beginTransaction();

            if(!DB::table($storeWithdrawModel->getTable())
                ->insert([
                    'stores_id'         => $storeId,
                    'time'              => $time,
                    'money'             => $money,
                    'discount_money'    => $discountMoney,
                    'turnover_id'       => implode(',', $turnoverId)
                ])
            ){
                \Log::debug('添加提现记录失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($storeTurnoverModel->getTable())
                ->whereIn('id', $turnoverId)
                ->update([
                    'status'    => 0
                ])
            ){
                \Log::debug('修改商家每日营业额提现状态失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($storeModel->getTable())
                ->where('id', $storeId)
                ->decrement('balance', $money)
            ){
                \Log::debug('修改商户余额失败');
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        }catch(\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }

    }
}
