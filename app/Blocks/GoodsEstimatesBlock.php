<?php

namespace App\Blocks;


use App\Models\Goods;
use App\Models\GoodsEstimates;
use App\Models\GoodsEstimatesImages;
use App\Models\GoodsEstimatesTag;
use App\Models\OrderGoods;
use App\Models\Orders;
use App\Models\Stores;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Users;

class GoodsEstimatesBlock
{

    private function estimatesSql($search)
    {
        $goodsModel = new Goods();
        $usersModel = new Users();
        $goodsEstimatesModel = new GoodsEstimates();
        $orderGoodsModel = new OrderGoods();
        $prefix = env('DB_PREFIX');
        $sql = DB::table($goodsEstimatesModel->getTable() . ' as ge')
            ->select(
                'ge.id',
                'ge.goods_id',
                'ge.is_reply',
                'g.stores_id',
                'ge.estimate',
                'ge.users_id',
                'ge.score',
                'ge.like',
                'ge.comment',
                'ge.Browse',
                DB::raw("date_format({$prefix}ge.update_time,'%Y-%m-%d') as create_time"),
                'g.name as goods_name',
                'u.name as user_name',
                'u.avatar',
                'u.mobile',
                'og.sku',
                'ge.status'
            )
            ->leftJoin($goodsModel->getTable() . ' as g', 'ge.goods_id', '=', 'g.id')
            ->leftJoin($usersModel->getTable() . ' as u', 'ge.users_id', '=', 'u.id')
            ->leftJoin($orderGoodsModel->getTable() . ' as og', 'ge.order_goods_id', '=', 'og.id')
            ->where('ge.p_id', 0)
            ->whereNull('ge.delete_time');
        if (isset($search['name'])) {
            $sql->where('g.name', 'like', "%" . $search['name'] . "%");
        }
        if (isset($search['title'])) {
            $sql->where('ge.estimate', 'like', "%" . $search['title'] . "%");
        }

        if (isset($search['goods_id'])) {
            $sql->where('ge.goods_id', $search['goods_id']);
        }
        if (isset($search['id'])) {
            $sql->where('ge.id', $search['id']);
        }
        if (isset($search['store_id'])) {
            if ($search['store_id'] >= 0){
                $sql->where('g.stores_id', $search['store_id'])
                ->where('ge.status', 1);
            }

        }

        return $sql;
    }

    /**
     * 审核评论
     * @param $id
     * @return int
     */
    public function checkGoodsEstimate($id){
        $goodsEstimateModel     = new GoodsEstimates();
        return DB::table($goodsEstimateModel->getTable())
            ->where('id', $id)
            ->update(['status' => 1]);
    }

    /**
     * 获取商品评论列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function estimatesList($search, $page, $pageSize)
    {
        $sql = $this->estimatesSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $goodsEstimates = $sql->orderBy('id', 'desc')->get();
        $goodsEstimatesImagesModel = new GoodsEstimatesImages();

        foreach ($goodsEstimates as $k => &$v) {
            $v->images = [];
            $goodsImage = $goodsEstimatesImagesModel::where('estimates_id', $v->id)->get();
            foreach ($goodsImage as $image) {
                $v->images[] = $image;
            }
        }

        return $goodsEstimates;
    }

    /**
     * 获取商品评论列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function estimatesListPagination(array $search, int $page = 1, int $pageSize = 20)
    {
        $sql = $this->estimatesSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    /**
     * 获取商品评论列表评分信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function estimatesScore(array $search, int $page = 1, int $pageSize = 20)
    {

        return [
            'store' => 5,
            'taste' => 5,
            'packing' => 5,
            'delivery' => 5,
        ];
    }

    /**
     * @param $search
     * 获取商品评论列表标签信息
     */
    public function estimatesTags($search)
    {
        //查询所有标签
        $goodsEstimatesTagsSql = GoodsEstimatesTag::whereNull('delete_time');
        if (isset($search['store_type_id'])) {
            $goodsEstimatesTagsSql->where('store_type_id', $search['store_type_id']);
        }
        $goodsEstimatesTags = $goodsEstimatesTagsSql->get();
        //查询所有评论并且组成一个字符串
        $goodsEstimates = $this->estimatesSql($search)->get();
        $goodsEstimatesStr = '';
        foreach ($goodsEstimates as $k => $v) {
            $goodsEstimatesStr .= $v->estimate;
        }

        //查询标签出现的次数
        $tags = [];
        foreach ($goodsEstimatesTags as $k => $v) {

            $tags[] = [
                'title' => $v->title,
                'number' => substr_count($goodsEstimatesStr, $v->title)
            ];
        }

        //返回出现次数最高的三个
        foreach ($tags as $k => $v) {
            if ($v['number'] == 0) {//||$v['number']<=50
                unset($tags[$k]);
                continue;
            }

            $volume[$k] = $v['number'];
        }
        if (isset($volume)) {
            array_multisort($volume, SORT_DESC, $tags);
        } else {
            $tags = [];
        }

        return $tags;
    }


    /**
     * 获取商品评论详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function estimatesInfo($search)
    {
        $goodsEstimatesImagesModel = new GoodsEstimatesImages();
        $sql = $this->estimatesSql($search);
        $goodsEstimates = $sql->first();
        if ($goodsEstimates) {
            //获取评论图片
            $goodsEstimates->images = [];
            $goodsImage = $goodsEstimatesImagesModel::where('estimates_id', $search['id'])->get();
            foreach ($goodsImage as $image) {
                $goodsEstimates->images[] = $image;
            }
        }
        return $goodsEstimates;
    }


    /**
     * 回复评论
     * @param $data
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function estimatesReply($data)
    {
        $goodsEstimatesModel = new GoodsEstimates();
        try {
            DB::beginTransaction();
            //添加评论的回复

            $replyId = DB::table($goodsEstimatesModel->getTable())->insertGetId($data);
            if (!$replyId) {
                \Log::error('添加回复失败');
                return false;
            }

            //修改回复的评论是否评论状态
            $estimates = DB::table($goodsEstimatesModel->getTable())
                ->where('id', $data['p_id'])
                ->update(['is_reply' => 1]);
            if (!$estimates) {
                \Log::error('回复失败');
                return false;
            }

            DB::commit();
            return $replyId;

        } catch (\Exception $exception) {
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }

    public function userEstimatesGoods($data, $image, $orderGoodsId, $storeId)
    {
        $goodsEstimatesModel        = new GoodsEstimates();
        $goodsEstimatesImageModel   = new GoodsEstimatesImages();
        $orderModel                 = new Orders();
        $orderGoodsModel            = new OrderGoods();
        $storeModel                 = new Stores();
        $orderDataId = DB::table($orderGoodsModel->getTable())
            ->select('orders_id')
            ->whereIn('id', $orderGoodsId)
            ->groupBy('orders_id')
            ->get();

        $orderId = null;
        foreach ($orderDataId as $item) {
            $orderId = $item->orders_id;
        }
        $orderStatus = DB::table($orderModel->getTable())->where('id', $orderId)->value('status');


        if ($orderStatus < 3){
            return -1;
        }

        if ($orderStatus == 6){
            return -2;
        }

        try {

            DB::beginTransaction();

            $estimatesId = [];

            if ( !DB::table($orderModel->getTable())->where('id', $orderId)->update(['status' => 6]) ){
                DB::rollBack();
                \Log::error('修改订单已评价状态失败');
                return false;
            }

            foreach ($data as $key => $datum) {
                $getId = DB::table($goodsEstimatesModel->getTable())->insertGetId($datum);
                if (!$getId) {
                    DB::rollBack();
                    \Log::error('添加评论失败');
                    return false;
                }else{
                    $estimatesId[$key]['estimates_id'] = $getId;
                    $estimatesId[$key]['order_goods_id'] = $datum['order_goods_id'];
                }

            }

            if (!empty($storeId)){

                $platformRating = DB::table($storeModel->getTable())
                    ->where('id', $storeId)
                    ->value('platform_rating');

                $score = DB::table($goodsEstimatesModel->getTable())
                    ->where('stores_id', $storeId)
                    ->avg('score');

                $rating = ($platformRating + $score) / 2;

                if(!DB::table($storeModel->getTable())
                    ->where('id', $storeId)
                    ->update([
                        'user_rating'   => $score,
                        'rating'        => $rating
                    ])
                ){
                    DB::rollBack();
                    \Log::error('修改商户用户评分失败');
                    return false;
                }



            }

            if (!empty($image)){
                $imageData = [];
                $i = 0;
                foreach ($estimatesId as $key => $value) {
                    foreach ($image as $item) {
                        if ($value['order_goods_id'] == $item['order_goods_id']){
                            foreach ($item['image'] as $images) {
                                $imageData[$i]['estimates_id'] =  $value['estimates_id'];
                                $imageData[$i]['images'] =  $images;
                                $i++;
                            }
                        }
                    }
                }


                if(DB::table($goodsEstimatesImageModel->getTable())->insert($imageData) === false){
                    DB::rollBack();
                    \Log::error('添加评价图片失败');
                    return false;
                }
            }


            DB::commit();
            return true;
        } catch (\Exception $exception) {
            DB::rollBack();
            \Log::error($exception);
            return false;
        }


    }

}
