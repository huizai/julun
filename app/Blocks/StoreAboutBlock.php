<?php

namespace App\Blocks;


use App\Models\StoreAbout;
use App\Models\StoreTags;
use DB;


class StoreAboutBlock
{
    private function storeAboutSql($search)
    {
        $storeAboutModel = new StoreAbout();
        $sql = DB::table($storeAboutModel->getTable());
        if (isset($search['store_id'])) {
            $sql->where('store_id', $search['store_id']);
        }

        return $sql;
    }

    /**
     * 获取商户介绍列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeAboutList($search, $page, $pageSize, $asc = [], $desc = [])
    {
        $sql = $this->storeAboutSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeAbout = $sql->orderBy('id', 'desc')->get();

        return $storeAbout;
    }

    /**
     * 获取商户介绍列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeAboutListPagination(array $search, int $page = 1, int $pageSize = 20)
    {
        $sql = $this->storeAboutSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户介绍详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeAboutInfo($search)
    {
        $sql = $this->storeAboutSql($search);
        $storeAbout = $sql->first();
        return $storeAbout;
    }



}
