<?php

namespace App\Blocks;


use App\Models\OrderGoods;
use App\Models\Orders;
use App\Models\ParkingOrder;
use App\Models\StoreTurnover;
use App\Models\Turnover;
use Illuminate\Support\Facades\DB;
use App\Models\Goods;
use App\Models\Stores;
use App\Models\Users;
use App\Models\UserCoupons;
use App\Models\StoreCoupons;
use App\Models\OrderCoupons;

class StatisticsBlock{

    public function getGoodsSalesRanking($orderBy){

        $goodsModel     = new Goods();
        $storeModel     = new Stores();

        $sql = DB::table($goodsModel->getTable().' as g')
            ->select(
                'g.id',
                'g.name',
                'g.sales_volume',
                'g.mouth_sales_volume',
                DB::raw("convert(jl_g.selling_price/100, decimal(15,2)) as selling_price"),
                's.name as store_name'
            )
            ->leftJoin($storeModel->getTable().' as s','g.stores_id', '=', 's.id')
            ->orderBy('g.sales_volume', $orderBy)
            ->whereNull('g.delete_time');

        $data = $sql->get();

        $i = 1;
        if ($orderBy == 'asc'){
            $i = $sql->count();
        }

        foreach ($data as $datum) {
            $datum->ranking = $i;
            if ($orderBy == 'asc'){
                $i--;
            }else{
                $i++;
            }
        }

        return $data;

    }

    /**
     * 获取用户总数、已购买会员数、未购买会员数
     * @return array
     */
    public function getUserData(){
        $userModel      = new Users();

        $data = DB::table($userModel->getTable())
            ->select(
                DB::raw("COUNT(id) AS total"),
                DB::raw("SUM(CASE WHEN is_shop = 0 THEN 1 ELSE 0 END) AS 未消费用户数量"),
                DB::raw("SUM(CASE WHEN is_shop = 1 THEN 1 ELSE 0 END) AS 已消费用户数量")
            )->whereNull('delete_time')
            ->first();

        $countData = [];
        $i = 0;
        foreach ($data as $key => $value) {
            if ($key != 'total'){
                $countData[$i]['value'] = $value;
                $countData[$i]['name'] = $key;
                $i++;
            }
        }

        return [
            'countData' => $countData,
            'total'     => $data->total

        ];
    }

    /**
     * 获取每天用户注册数量（默认7天）
     * @param $time
     * @return array
     */
    public function getUserRegister($time){
        $userModel      = new Users();
        $data = DB::table($userModel->getTable())
            ->select(
                DB::raw("DATE_FORMAT( create_time, '%Y-%m-%d' ) AS day"),
                DB::raw("COUNT( id ) AS total ")
            )
            ->whereNull('delete_time')
            ->whereBetween(DB::raw("DATE_FORMAT( create_time, '%Y-%m-%d' )"), $time)
            ->groupBy('day')
            ->get()->toArray();

        $timeData = range(strtotime($time[0]), strtotime($time[1]), 86400);
        $date = array_map(function ($value){
            return date('Y-m-d', $value);
        }, $timeData);



        $registerData = [];
        foreach($date as $v){
            $registerData[] = array('total' => 0,'day' => $v);
        }

        $registerCount = 0;
        $totalData = [];

        foreach ($registerData as $key => $value){
            foreach ($data as $k => $v){
                if($v->day == $value['day']){
                    $registerData[$key]['day'] = $v->day;
                    $registerData[$key]['total'] = $v->total;
                }
            }
            $totalData[] = $registerData[$key]['total'];
            $registerCount += $registerData[$key]['total'];
        }

        return [
            'totalData'     => $totalData,
            'dateData'      => $date,
            'totalCount'    => $registerCount
        ];
    }

    /**
     * 优惠券发行统计
     * @param $time
     * @return array
     */
    public function getCouponIssue($time){
        $userCouponModel        = new UserCoupons();
        $storeCouponModel       = new StoreCoupons();


        $data = DB::table($userCouponModel->getTable().' as uc')
            ->select(
                DB::raw("DATE_FORMAT( jl_uc.create_time, '%Y-%m-%d' ) AS day"),
                DB::raw("COUNT( jl_uc.id ) AS total "),
                'sc.belong as type'
            )
            ->leftJoin($storeCouponModel->getTable().' as sc', 'uc.coupon_id', '=', 'sc.id')
            ->whereBetween(DB::raw("DATE_FORMAT( jl_uc.create_time, '%Y-%m-%d' )"), $time)
            ->groupBy('day')
            ->groupBy('type')
            ->get()->toArray();

        $timeData = range(strtotime($time[0]), strtotime($time[1]), 86400);
        $date = array_map(function ($value){
            return date('Y-m-d', $value);
        }, $timeData);

        $couponData = [];
        foreach($date as $value){
            $couponData[] = [
                ['total' => 0,'day' => $value, 'type' => 1 ],
                ['total' => 0,'day' => $value, 'type' => 2 ]
            ];
        }

        foreach ($couponData as $key => &$value){
            foreach ($data as $k => $v){
                if($v->day == $value[0]['day'] && $value[0]['type'] == $v->type){
                    $value[0]['total'] += $v->total;
                }else if($v->day == $value[1]['day'] && $value[1]['type'] == $v->type){
                    $value[1]['total'] += $v->total;
                }
            }


        }

        $totalData = [];
        foreach ($couponData as $item) {
            $totalData[0][] = $item[0]['total'];
            $totalData[1][] = $item[1]['total'];
        }

        return [
            'totalData'     => $totalData,
            'totalLabel'    => ['平台', '商家'],
            'totalDate'     => $date
        ];
    }

    /**
     * 优惠券每天使用量统计
     * @param $time
     * @return array
     */
    public function getCouponUse($time){
        $orderCouponModel        = new OrderCoupons();
        $orderModel              = new Orders();

        $data = DB::table($orderCouponModel->getTable().' as oc')
            ->select(
                DB::raw("DATE_FORMAT( jl_oc.update_time, '%Y-%m-%d' ) AS day"),
                DB::raw("COUNT( jl_oc.id ) AS total ")
            )
            ->leftJoin($orderModel->getTable().' as o', 'o.id', '=', 'oc.orders_id')
            ->where('o.status', '>', 0)
            ->whereBetween(DB::raw("DATE_FORMAT( jl_oc.update_time, '%Y-%m-%d' )"), $time)
            ->groupBy('day')
            ->get()->toArray();

        $timeData = range(strtotime($time[0]), strtotime($time[1]), 86400);
        $date = array_map(function ($value){
            return date('Y-m-d', $value);
        }, $timeData);



        $couponUseData = [];
        foreach($date as $v){
            $couponUseData[] = array('total' => 0,'day' => $v);
        }

        $registerCount = 0;
        $totalData = [];

        foreach ($couponUseData as $key => $value){
            foreach ($data as $k => $v){
                if($v->day == $value['day']){
                    $couponUseData[$key]['day'] = $v->day;
                    $couponUseData[$key]['total'] = $v->total;
                }
            }
            $totalData[] = $couponUseData[$key]['total'];
            $registerCount += $couponUseData[$key]['total'];
        }

        return [
            'totalData'     => $totalData,
            'dateData'      => $date,
            'totalCount'    => $registerCount
        ];

    }


    public function getParkingTotal(){
        $parkingModel       = new ParkingOrder();

        /**
         * 停车场当天营业总额
         */
        $payAmount = DB::table($parkingModel->getTable())
            ->select(
                DB::raw("CONVERT(SUM(pay_amount)/100, decimal(15,2))  as total")
            )
            ->whereBetween('payTime', [
                date('Y-m-d 00:00:00', time()),
                date('Y-m-d H:i:s', time()),
            ])
            ->where('pay_status', 2)
            ->value('total');

        if (!$payAmount){
            $payAmount = priceIntToFloat(0);
        }


        /**
         * 停车场优惠券抵扣总额 && 优惠券抵扣数量
         */
        $total = DB::table($parkingModel->getTable())
            ->select(
                DB::raw("CONVERT(SUM(discount_amount)/100, decimal(15,2))  as discountAmount"),
                DB::raw("COUNT(coupons_id)  as couponTotal")
            )
            ->where('pay_status', 2)
            ->whereNotNull('coupons_id')
            ->first();

        if (!$total->discountAmount){
            $total->discountAmount = priceIntToFloat(0);
        }
//        var_dump();

        $total->payAmount = $payAmount;
        return $total;
    }

    /**
     * 商家营业额排行
     * @return array
     */
    public function getManageTurnover(){
        $storeModel     = new Stores();
        $data = DB::table($storeModel->getTable())
            ->select(
                'name', 'turnover',
                DB::raw("CONVERT(turnover/100, decimal(15,2)) as turnover")
            )
            ->orderBy('turnover','desc')
            ->skip(0)->take(20)
            ->get()->toArray();

        $data = array_reverse($data);
        $totalData = [];
        $maxTurnover = 0;
        foreach ($data as $value) {
            $totalData[$value->name]    = $value->turnover;
            if ($value->turnover > $maxTurnover){
                $maxTurnover = $value->turnover;
            }
        }
        return [
            'totalData'     => $totalData,
            'maxTurnover'   => $maxTurnover+100
        ];
    }

    /**
     * 平台营业额统计
     * @param $time
     * @return array
     */
    public function platformTurnover($time){
        $turnoverModel      = new Turnover();

        $data = DB::table($turnoverModel->getTable())
            ->select(
                'date',
                DB::raw("CONVERT(turnover/100, decimal(15,2)) as turnover")
            )
            ->whereBetween('date', $time)
            ->get();


        $timeData = range(strtotime($time[0]), strtotime($time[1]), 86400);
        $date = array_map(function ($value){
            return date('Y-m-d', $value);
        }, $timeData);

        $totalData = [];
        foreach ($date as $item) {
            $totalData[$item] = 0;
        }

        $totalTurnover = 0;
        $maxTurnover = 0;
        foreach ($data as $value) {
            if (in_array($value->date, $date)){
                $totalData[$value->date] = $value->turnover;
            }
            $totalTurnover += $value->turnover;
            if ($value->turnover > $maxTurnover){
                $maxTurnover = $value->turnover;
            }
        }

        return [
            'totalData'     => $totalData,
            'totalTurnover' => $totalTurnover,
            'maxTurnover'   => $maxTurnover+100
        ];
    }

    public function getMerchantStatistics($storeId, $time, $type, $year, $month, $day){
        $storeTurnoverModel     = new StoreTurnover();
        $storeModel             = new Stores();

        $yearTurnover = DB::table($storeTurnoverModel->getTable())
            ->where('stores_id', $storeId)
            ->where('date', 'like', $year.'%')
            ->sum('turnover');

        $monthTurnover = DB::table($storeTurnoverModel->getTable())
            ->where('stores_id', $storeId)
            ->where('date', 'like', $month.'%')
            ->sum('turnover');

        $dayTurnover = DB::table($storeTurnoverModel->getTable())
            ->where('stores_id', $storeId)
            ->where('date', 'like', $day.'%')
            ->sum('turnover');

        $data = DB::table($storeTurnoverModel->getTable().' as  st')
            ->select(
                's.name as store_name',
                'st.date',
                DB::raw("convert(jl_st.turnover/100, decimal(15,2)) as turnover")
            )
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'st.stores_id')
            ->where('st.stores_id', $storeId)
            ->whereBetween('date', $time)
            ->orderByDesc('date')
            ->get();

        return [
            'yearTurnover'      => $yearTurnover  === false ? priceIntToFloat(0) : priceIntToFloat($yearTurnover),
            'monthTurnover'     => $monthTurnover === false ? priceIntToFloat(0) : priceIntToFloat($monthTurnover),
            'dayTurnover'       => $dayTurnover   === false ? priceIntToFloat(0) : priceIntToFloat($dayTurnover),
            'data'              => $data,
            'year'              => $year,
            'month'             => $month,
            'day'               => $day,
            'time'              => $time
        ];

    }


    public function getTimeOrder($storeId, $date, $page, $pageSize){
        $orderModel         = new Orders();
        $userModel          = new Users();
        $orderGoodsModel    = new OrderGoods();

        $data = DB::table($orderModel->getTable().' as o')
            ->select(
                'o.id',
                'o.order_sn',
                DB::raw("convert(jl_o.pay_amount/100, decimal(15,2)) as pay_amount"),
                'o.pay_type',
                'o.pay_time',
                'u.name',
                'u.mobile',
                'u.avatar',
                'o.status',
                'o.create_time'
            )
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'o.users_id')
            ->where('pay_time', 'like', $date.'%')
            ->where('stores_id', $storeId)
            ->whereIn('status', [3,6])
            ->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

        foreach ($data as $datum) {
            $datum->goods_data = DB::table($orderGoodsModel->getTable())
                ->select(
                    'name as title',
                    'thumbnail',
                    DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                    'quantity'
                )
                ->where('orders_id', $datum->id)
                ->get()
                ->toArray();
            $datum->goods_sum = 0;
            foreach ($datum->goods_data as $value) {
                $datum->goods_sum += $value->quantity;
            }

            switch($datum->pay_type){
                case 1:
                    $datum->pay_type_string = '微信支付';
                    break;
                case 2:
                    $datum->pay_type_string = '支付宝支付';
                    break;
                case 3:
                    $datum->pay_type_string = '余额支付';
                    break;
                case 4:
                    $datum->pay_type_string = '小程序微信支付';
                    break;
            }
        }

        $totalAmount  = priceIntToFloat(DB::table($orderModel->getTable())
            ->where('pay_time', 'like', $date.'%')
            ->where('stores_id', $storeId)
            ->whereIn('status', [3,6])
            ->sum('pay_amount'));

        return [
            'data'      => $data,
            'total'     => $totalAmount
        ];
    }

    public function getGoodsStatistics($year, $month, $day, $name, $storeId){
        $orderGoodsModel        = new OrderGoods();
        $storeModel             = new Stores();
        $orderModel             = new Orders();
        $requestTime            = $year.$month.$day;

        if (empty($requestTime)){
            $requestTime = date('Y-m-d', time());
        }

        $sql = DB::table($orderGoodsModel->getTable().' as og')
            ->select(
                DB::raw("COUNT(jl_og.id) as total"),
                'og.goods_id',
                'og.name as goods_name',
                DB::raw("convert(jl_og.selling_price/100, decimal(15,2)) as selling_price"),
                's.name as store_name'
            )
            ->leftJoin($orderModel->getTable().' as o', 'o.id', '=', 'og.orders_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'o.stores_id')
            ->whereIn('o.status', [3,6])
            ->where('o.pay_time', 'like', $requestTime.'%')
            ->groupBy('goods_id')
            ->orderByDesc('total');


        if (!empty($name)){
            $sql->where('og.name', 'like', '%'.$name.'%');
        }

        if (!empty($storeId)){
            $sql->where('o.stores_id', $storeId);
        }
        return [
            'data'          => $sql->get(),
            'year'          => !empty($year)    ? $year : date('Y'),
            'month'         => !empty($month)   ? $year : date('m'),
            'day'           => !empty($day)     ? $year : date('d')
        ];
    }

    public function getMerchantRating($year, $month, $day, $name){
        $storeTurnoverModel     = new StoreTurnover();
        $storeModel             = new Stores();
        $requestTime            = '';
        if (!empty($year)){
            $requestTime .= $year.'-';
        }
        if (!empty($month)){
            $requestTime .= $month.'-';
        }
        if (!empty($day)){
            $requestTime .= $day;
        }
        if (empty($year) || empty($requestTime)){
            $requestTime = date('Y-m-d', time());
        }

        $sql = DB::table($storeTurnoverModel->getTable().' as st')
            ->select(
                DB::raw("convert(sum(jl_st.turnover)/100, decimal(15,2)) as turnover"),
                's.name as store_name',
                'st.stores_id'
            )
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'st.stores_id')
            ->where('date', 'like', $requestTime.'%')
            ->groupBy('st.stores_id')
            ->orderByDesc('turnover');

        if (!empty($name)){
            $sql->where('s.name', 'like', '%'.$name.'%');
        }


        return $sql->get();
    }

    public function getUserConsumption($year, $month, $day){
        $orderModel             = new Orders();

        $yearConsumption        = [];
        $monthConsumption       = [];
        $dayConsumption         = [];
        \Log::debug('$year：'.$year);
        \Log::debug('$month：'.$month);
        \Log::debug('$day：'.$day);

        $yearConsumption['people'] = DB::table($orderModel->getTable())
            ->select(
                'users_id'
            )
            ->groupBy('users_id')
            ->whereIn('status', [3,6])
            ->where('pay_time', 'like', $year.'%')
            ->get()->count();
        $yearConsumption['consumption'] = priceIntToFloat(DB::table($orderModel->getTable())
            ->whereIn('status', [3,6])
            ->where('pay_time', 'like', $year.'%')
            ->sum('pay_amount'));

        $monthConsumption['people'] = DB::table($orderModel->getTable())
            ->select(
                'users_id'
            )
            ->groupBy('users_id')
            ->whereIn('status', [3,6])
            ->where('pay_time', 'like', $month.'%')
            ->get()->count();
        $monthConsumption['consumption'] = priceIntToFloat(DB::table($orderModel->getTable())
            ->whereIn('status', [3,6])
            ->where('pay_time', 'like', $month.'%')
            ->sum('pay_amount'));

        $dayConsumption['people'] = DB::table($orderModel->getTable())
            ->select(
                'users_id'
            )
            ->groupBy('users_id')
            ->whereIn('status', [3,6])
            ->where('pay_time', 'like', $day.'%')
            ->get()->count();
        $dayConsumption['consumption'] = priceIntToFloat(DB::table($orderModel->getTable())
            ->whereIn('status', [3,6])
            ->where('pay_time', 'like', $day.'%')
            ->sum('pay_amount'));

        return [
            'year'          => $yearConsumption,
            'month'         => $monthConsumption,
            'day'           => $dayConsumption
        ];
    }

    public function getStoreExportTurnover($time, $storeName){
        $storeTurnoverModel         = new StoreTurnover();
        $storeModel                 = new Stores();

        $data = [
            'code'     => 200,
            'message'  => '',
            'data'     => []
        ];
        $prefix = env('DB_PREFIX');
        $sql = DB::table($storeTurnoverModel->getTable().' as st')
            ->select(
                'st.date',
                DB::raw("convert({$prefix}st.turnover/100, decimal(15,2)) as turnover"),
                's.name'
            )
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'st.stores_id')
            ->whereBetween('date', $time);

        if (!empty($storeName)){
           $storeId =  DB::table($storeModel->getTable())
                ->where('name', $storeName)
                ->value('id');

           if (!$storeId){
               $data['code'] = 506;
               $data['message'] = '未找到该商家,请重新输入';
               return $data;
           }

            $sql->where('st.stores_id', $storeId);
        }

        $data['data'] = $sql->orderBy('date')->get();
        return $data;
    }

}
