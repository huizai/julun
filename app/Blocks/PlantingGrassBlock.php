<?php
namespace App\Blocks;

use App\Models\Goods;
use App\Models\PlantingGrassAwesome;
use App\Models\PlantingGrassCollection;
use App\Models\PlantingGrassComment;
use App\Models\PlantingGrassCommentAwesome;
use App\Models\PlantingGrassImages;
use App\Models\UserAttention;
use App\Models\UserMessage;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use App\Models\PlantingGrass;
use App\Models\PlantingGrassGoods;

class PlantingGrassBlock{

    public function addGrass($userId, $title, $content, $thumbnail, $goodsId, $image, $classify){

        try{

            DB::beginTransaction();
            $grassModel         = new PlantingGrass();
            $grassGoodsModel    = new PlantingGrassGoods();
            $grassImageModel    = new PlantingGrassImages();

            //种草信息
            $grassId = DB::table($grassModel->getTable())->insertGetId([
                'users_id'      => $userId,
                'title'         => $title,
                'content'       => $content,
                'thumbnail'     => $thumbnail,
                'classify'      => $classify
            ]);

            if (!$grassId){
                DB::rollBack();
                return false;
            }

            //种草商品
            $goodsData = [];
            foreach ($goodsId as $key => $item) {
                $goodsData[$key]['goods_id']            = $item;
                $goodsData[$key]['planting_grass_id']   = $grassId;
            }

            if( !DB::table($grassGoodsModel->getTable())->insert($goodsData) ){
                DB::rollBack();
                return false;
            }

            //种草图片
            if (!empty($image)){

                $imageData = [];
                foreach ($image as $key => $item) {
                    $imageData[$key]['planting_grass_id']   = $grassId;
                    $imageData[$key]['image']               = $item;
                }

                if (!DB::table($grassImageModel->getTable())->insert($imageData)){
                    DB::rollBack();
                    return false;
                }
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error('添加种草失败，原因：'.$exception);
            return false;
        }
    }


    private function __grassSql($search){
        $userModel          = new Users();
        $grassModel         = new PlantingGrass();
        $userAttentionModel = new UserAttention();
        $grassCollectionModel = new PlantingGrassCollection();

        $sql = DB::table($grassModel->getTable().' as g')
            ->select(
                'g.*',
                'u.avatar',
                'u.name',
                'u.attention',
                'u.awesome'
            )
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'g.users_id');

        //关注
        if (isset($search['type']) && $search['type'] == 'attention' && isset($search['users_id'])){
            $sql->leftJoin($userAttentionModel->getTable().' as ua', 'ua.be_users_id', '=', 'g.users_id')
                ->whereNull('ua.delete_time')
                ->where('ua.users_id', $search['users_id']);
        }

        //收藏
        if (isset($search['type']) && $search['type'] == 'collection' && isset($search['users_id'])){
            $sql->leftJoin($grassCollectionModel->getTable().' as gc', 'gc.planting_grass_id', '=', 'g.id')
                ->addSelect('gc.id as collection_id')
                ->where('gc.users_id', $search['users_id']);
        }

        //根据用户ID查询
        if (isset($search['user_id'])){
            $sql->where('g.users_id', $search['user_id']);
        }


        if (isset($search['title'])){
            $sql->where('g.title', 'like', '%' . $search['title'] . '%');
        }

        if (isset($search['id'])){
            $sql->where('g.id', $search['id']);
        }

        if (!empty($search['status'])){
            $sql->where('status', $search['status']);
        }

        if (isset($search['classify'])){
            if (is_array($search['classify'])){
                $sql->whereIn('classify', $search['classify']);
            }else{
                $sql->where('classify', $search['classify']);
            }
        }

        $sql->whereNull('g.delete_time')
            ->whereNull('u.delete_time');

        return $sql;
    }

    /**
     * 种草社区列表
     * @param $page
     * @param $pageSize
     * @param $search
     * @return mixed
     */
    public function grassList($page, $pageSize, $search){
        $grassAwesomeModel         = new PlantingGrassAwesome();
        $grassCollection           = new PlantingGrassCollection();
        $useAttentionModel         = new UserAttention();
        $sql = $this->__grassSql($search);

        $data = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->orderBy('g.create_time', 'desc')
            ->get();

        if (isset($search['users_id'])){

            /**
             * 点赞
             */
            $awesomeData = DB::table($grassAwesomeModel->getTable())
                ->select('planting_grass_id')
                ->where('users_id', $search['users_id'])
                ->whereNull('delete_time')
                ->get();

            $collectionData = DB::table($grassCollection->getTable())
                ->select('planting_grass_id')
                ->where('users_id', $search['users_id'])
                ->get();

            $attentionData = DB::table($useAttentionModel->getTable())
                ->select('be_users_id')
                ->where('users_id', $search['users_id'])
                ->get();


            foreach ($data as $datum) {
                /**
                 * 查看自己的贴子，修改状态int为字符串
                 */
                if (
                    isset($search['users_id'])
                    &&
                    isset($search['user_id'])
                    &&
                    $search['users_id'] == $search['user_id'])
                {
                    $datum->status = $datum->status == 0 ? '待审核' : ($datum->status == 1 ? '显示' : '隐藏');
                }

                /**
                 * 点赞
                 */
                if ($awesomeData->isEmpty()){
                    $datum->is_awesome = 0;
                }else{
                    foreach ($awesomeData as $value) {
                        if ($datum->id == $value->planting_grass_id){
                            $datum->is_awesome = 1;
                        }else{
                            $datum->is_awesome = 0;
                        }
                    }
                }


                /**
                 * 收藏
                 */
                if ($collectionData->isEmpty()){
                    $datum->is_collection = 0;
                }else{
                    foreach ($collectionData as $value) {
                        if ($datum->id == $value->planting_grass_id){
                            $datum->is_collection = 1;
                        }else{
                            $datum->is_collection = 0;
                        }
                    }
                }

                /**
                 * 关注
                 */
                if ($attentionData->isEmpty()){
                    $datum->is_attention = 0;
                }else{
                    foreach ($attentionData as $value) {
                        if ($datum->users_id == $value->be_users_id){
                            $datum->is_attention = 1;
                        }else{
                            $datum->is_attention = 0;
                        }
                    }
                }

            }

        }


        return $data;
    }

    public function grassPagination($page, $pageSize, $search){
        $sql = $this->__grassSql($search);

        $total = $sql->count();
        return [
            'total'     => $total,
            'page'      => $page,
            'pageSize'  => $pageSize
        ];
    }

    /**
     * 种草社区详情
     * @param $id
     * @param $userId
     * @return mixed
     */
    public function grassInfo($id, $userId, $page, $pageSize){
        $goodsModel             = new Goods();
        $grassGoodsModel        = new PlantingGrassGoods();
        $usersModel             = new Users();
        $grassCommentModel      = new PlantingGrassComment();
        $grassImageModel        = new PlantingGrassImages();
        $grassAwesomeModel      = new PlantingGrassAwesome();
        $grassCollection        = new PlantingGrassCollection();
        $useAttentionModel      = new UserAttention();
        $commentAwesomeModel    = new PlantingGrassCommentAwesome();

        $infoData = $this->__grassSql(['id' => $id])->first();

        if (!$infoData){
            return '';
        }
        //是否点赞
        $infoData->is_awesome           = 0;
        //是否收藏
        $infoData->is_collection        = 0;
        //是否关注
        $infoData->is_attention         = 0;
        if (!empty($userId)){
            /**
             * 点赞
             */
            $awesome = DB::table($grassAwesomeModel->getTable())
                ->where('users_id', $userId)
                ->where('planting_grass_id', $infoData->id)
                ->whereNull('delete_time')
                ->first();
            if ($awesome){
                $infoData->is_awesome = 1;
            }

            /**
             * 收藏
             */
            $collection = DB::table($grassCollection->getTable())
                ->where('users_id', $userId)
                ->where('planting_grass_id', $infoData->id)
                ->first();
            if ($collection){
                $infoData->is_collection = 1;
            }

            /**
             * 关注
             */
            $attention = DB::table($useAttentionModel->getTable())
                ->where('users_id', $userId)
                ->where('be_users_id', $infoData->users_id)
                ->first();
            if ($attention){
                $infoData->is_attention = 1;
            }
        }

        //获取种草图片
        $imageData = DB::table($grassImageModel->getTable())->select('image')->where('planting_grass_id', $id)->get();
        $image = [];
        foreach ($imageData as $imageValue) {
            $image[] = $imageValue->image;
        }
        $infoData->image = $image;

        //获取种草商品
        $goodsData = DB::table($grassGoodsModel->getTable())
            ->select('goods_id')
            ->where('planting_grass_id', $id)->get();

        $goodsId = [];
        foreach ($goodsData as $goodsValue) {
            $goodsId[] = $goodsValue->goods_id;
        }

        $goods = DB::table($goodsModel->getTable())
            ->select(
                'id',
                'stores_id',
                'name',
                'thumbnail',
                DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price")
            )->whereIn('id', $goodsId)
            ->get();
        $infoData->goods = $goods;

        //获取种草评论
        $commentSql = DB::table($grassCommentModel->getTable().' as gc')
            ->select(
                'gc.*',
                'u.avatar',
                'u.name'
            )
            ->leftJoin($usersModel->getTable().' as u', 'gc.users_id', '=', 'u.id')
            ->where('gc.planting_grass_id', $id)
            ->where('gc.status', 1)
            ->whereNull('gc.delete_time');
        $commentTotal               = $commentSql->count();
        $commentData                = $commentSql->skip(($page - 1) * $pageSize)->take($pageSize)->get();


        if (!empty($userId)){
            $commentAwesome = DB::table($commentAwesomeModel->getTable())
                ->where('users_id', $userId)
                ->whereNull('delete_time')
                ->get();
            $commentId = [];
            foreach ($commentAwesome as $value) {
                $commentId[] = $value->comment_id;
            }

            foreach ($commentData as $item) {
                if (in_array($item->id, $commentId)){
                    $item->is_awesome = 1;
                }else{
                    $item->is_awesome = 0;
                }
            }
        }else{
            foreach ($commentData as $item) {
                $item->is_awesome = 0;

            }
        }

        $infoData->comment          = $commentData;
        $infoData->comment_total    = $commentTotal;

        return $infoData;
    }

    /**
     * 发布种草社区评论
     * @param $data
     * @return mixed
     */
    public function grassCommentAdd($data){
        $grassCommentModel  = new PlantingGrassComment();
        return DB::table($grassCommentModel->getTable())->insert($data);
    }

    /**
     * 种草点赞
     * @param $data
     * @return bool
     */
    public function grassAwesomeSave($data){
        $userModel          = new Users();
        $grassModel         = new PlantingGrass();
        $grassAwesomeModel  = new PlantingGrassAwesome();

        $userId = DB::table($grassModel->getTable())->where('id', $data['planting_grass_id'])->value('users_id');
        if (!$userId){
            return false;
        }

        $awesome = DB::table($grassAwesomeModel->getTable())
            ->where('users_id', $data['users_id'])
            ->where('planting_grass_id', $data['planting_grass_id'])
            ->first();

        try{
            DB::beginTransaction();
            if (!$awesome){
                /**
                 * 没点过赞
                 */
                if ( DB::table($grassAwesomeModel->getTable())->insert($data) === false ){
                    DB::rollBack();
                    return false;
                }
                /**
                 * 种草点赞数增加
                 */
                if (
                    DB::table($grassModel->getTable())
                        ->where('id' ,$data['planting_grass_id'])
                        ->increment('awesome') === false
                ){
                    DB::rollBack();
                    return false;
                }
                /**
                 * 用户点赞数增加
                 */
                if ( DB::table($userModel->getTable())->where('id', $userId)->increment('awesome') === false ){
                    DB::rollBack();
                    return false;
                }

            }else{

                /**
                 * 判断当前点赞状态
                 */
                if ($awesome->delete_time == null){

                    /**
                     * 已点赞，取消点赞
                     */
                    if ( DB::table($grassAwesomeModel->getTable())
                        ->where('users_id', $data['users_id'])
                        ->where('planting_grass_id', $data['planting_grass_id'])
                        ->update(['delete_time' => date('Y-m-d H:i:s', time())]) === false ){
                        DB::rollBack();
                        return false;
                    }
                    /**
                     * 种草点赞数减少
                     */
                    if (
                        DB::table($grassModel->getTable())
                            ->where('id' ,$data['planting_grass_id'])
                            ->decrement('awesome') === false
                    ){
                        DB::rollBack();
                        return false;
                    }
                    /**
                     * 用户点赞数减少
                     */
                    if ( DB::table($userModel->getTable())->where('id', $userId)->decrement('awesome') === false ){
                        DB::rollBack();
                        return false;
                    }
                }else{

                    /**
                     * 已取消点赞，进行点赞
                     */
                    if ( DB::table($grassAwesomeModel->getTable())
                        ->where('users_id', $data['users_id'])
                        ->where('planting_grass_id', $data['planting_grass_id'])
                        ->update(['delete_time' => null])  === false){
                        DB::rollBack();
                        return false;
                    }
                    /**
                     * 种草点赞数增加
                     */
                    if (
                        DB::table($grassModel->getTable())
                            ->where('id' ,$data['planting_grass_id'])
                            ->increment('awesome') === false
                    ){
                        DB::rollBack();
                        return false;
                    }
                    /**
                     * 用户点赞数增加
                     */
                    if ( DB::table($userModel->getTable())->where('id', $userId)->increment('awesome') === false ){
                        DB::rollBack();
                        return false;
                    }
                }
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error('点赞或取消点赞失败，原因：'.$exception);
            return false;
        }



    }


    /**
     * 隐藏种草社区贴子（评论）
     * @param $id
     * @return mixed
     */
    public function grassHidden($id, $type, $status){
        $grassModel         = new PlantingGrass();
        $grassCommentModel  = new PlantingGrassComment();
        $userMessageModel   = new UserMessage();

        if ($type == 'grass'){

            if ($status == 1){
                $userId = DB::table($grassModel->getTable())
                    ->where('id', $id)
                    ->value('users_id');

                DB::table($userMessageModel->getTable())
                    ->insert([
                        'content'           => '您在种草社区发布的贴子已通过审核',
                        'users_id'          => $userId
                    ]);


            }

            return DB::table($grassModel->getTable())
                ->where('id', $id)
                ->update(['status' => $status]);

        }else if ($type == 'comment'){

            return DB::table($grassCommentModel->getTable())
                ->where('id', $id)
                ->update(['status' => $status]);

        }else{

            return false;
        }

    }


    /**
     * 删除种草社区贴子（评论）
     * @param $id
     * @param $userId
     * @param $type
     * @return bool
     */
    public function grassDelete($id, $type){
        $grassModel         = new PlantingGrass();
        $grassCommentModel = new PlantingGrassComment();

        if ($type == 'grass'){

            return DB::table($grassModel->getTable())
                ->where('id', $id)
                ->update(['delete_time' => date('Y-m-d H:i:s', time())]);

        }else if ($type == 'comment'){

            return DB::table($grassCommentModel->getTable())
                ->where('id', $id)
                ->update(['delete_time' => date('Y-m-d H:i:s', time())]);

        }else{

            return false;
        }
    }


    /**
     * 种草社区贴子评论点赞（取消点赞）
     * @param $data
     * @return bool
     */
    public function grassCommentAwesome($data){
        $userModel                   = new Users();
        $grassCommentModel           = new PlantingGrassComment();
        $grassCommentAwesomeModel    = new PlantingGrassCommentAwesome();

        $userId = DB::table($grassCommentModel->getTable())->where('id', $data['comment_id'])->value('users_id');
        if (!$userId){
            return false;
        }

        $awesome = DB::table($grassCommentAwesomeModel->getTable())
            ->where('users_id', $data['users_id'])
            ->where('comment_id', $data['comment_id'])
            ->first();

        try{
            DB::beginTransaction();
            if (!$awesome){
                /**
                 * 没点过赞
                 */
                if ( DB::table($grassCommentAwesomeModel->getTable())->insert($data) === false ){
                    DB::rollBack();
                    return false;
                }
                /**
                 * 种草点赞数增加
                 */
                if (
                    DB::table($grassCommentModel->getTable())
                        ->where('id' ,$data['comment_id'])
                        ->increment('awesome') === false
                ){
                    DB::rollBack();
                    return false;
                }
                /**
                 * 用户点赞数增加
                 */
                if ( DB::table($userModel->getTable())->where('id', $userId)->increment('awesome') === false ){
                    DB::rollBack();
                    return false;
                }

            }else{

                /**
                 * 判断当前点赞状态
                 */
                if ($awesome->delete_time == null){

                    /**
                     * 已点赞，取消点赞
                     */
                    if ( DB::table($grassCommentAwesomeModel->getTable())
                            ->where('users_id', $data['users_id'])
                            ->where('comment_id', $data['comment_id'])
                            ->update(['delete_time' => date('Y-m-d H:i:s', time())]) === false ){
                        DB::rollBack();
                        return false;
                    }
                    /**
                     * 种草点赞数减少
                     */
                    if (
                        DB::table($grassCommentModel->getTable())
                            ->where('id' ,$data['comment_id'])
                            ->decrement('awesome') === false
                    ){
                        DB::rollBack();
                        return false;
                    }
                    /**
                     * 用户点赞数减少
                     */
                    if ( DB::table($userModel->getTable())->where('id', $userId)->decrement('awesome') === false ){
                        DB::rollBack();
                        return false;
                    }
                }else{

                    /**
                     * 已取消点赞，进行点赞
                     */
                    if ( DB::table($grassCommentAwesomeModel->getTable())
                            ->where('users_id', $data['users_id'])
                            ->where('comment_id', $data['comment_id'])
                            ->update(['delete_time' => null])  === false){
                        DB::rollBack();
                        return false;
                    }
                    /**
                     * 种草点赞数增加
                     */
                    if (
                        DB::table($grassCommentModel->getTable())
                            ->where('id' ,$data['comment_id'])
                            ->increment('awesome') === false
                    ){
                        DB::rollBack();
                        return false;
                    }
                    /**
                     * 用户点赞数增加
                     */
                    if ( DB::table($userModel->getTable())->where('id', $userId)->increment('awesome') === false ){
                        DB::rollBack();
                        return false;
                    }
                }
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error('点赞或取消点赞失败，原因：'.$exception);
            return false;
        }
    }


    /**
     *种草社区添加收藏
     * @param $data
     * @return mixed
     */
    public function grassCollectionSave($data){
        $grassCollectionModel = new PlantingGrassCollection();

        return DB::table($grassCollectionModel->getTable())->insert($data);
    }

    /**
     *种草社区删除收藏
     * @param $data
     * @return mixed
     */
    public function grassCollectionDelete($data){
        $grassCollectionModel = new PlantingGrassCollection();


        return DB::table($grassCollectionModel->getTable())
            ->where('planting_grass_id', $data['id'])
            ->where('users_id', $data['users_id'])
            ->delete();
    }

    /**
     * 种草关注（取消关注）
     * @param $userId
     * @param $beUserId
     * @return mixed
     */
    public function userAttention($userId, $beUserId){
        $attentionModel     = new UserAttention();
        $userModel          = new Users();
        $data = DB::table($attentionModel->getTable())
            ->where('be_users_id', $beUserId)
            ->where('users_id', $userId)
            ->first();

        /**
         * 之前未关注
         */
        try{
            DB::beginTransaction();

            if (empty($data)){
                if(!DB::table($userModel->getTable())
                    ->where('id', $beUserId)
                    ->increment('attention')
                ){
                    \Log::debug('添加用户关注数量失败');
                    DB::rollBack();
                    return false;
                }

                if(!DB::table($attentionModel->getTable())->insert([
                    'be_users_id'   => $beUserId,
                    'users_id'      => $userId
                ])
                ){
                    \Log::debug('添加用户关注日志失败');
                    DB::rollBack();
                    return false;
                }

                DB::commit();
                return true;
            }
            /**
             * 取消关注
             */
            if(!DB::table($userModel->getTable())
                ->where('id', $beUserId)
                ->decrement('attention')
            ){
                \Log::debug('减少用户关注数量失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($attentionModel->getTable())
                ->where('be_users_id', $beUserId)
                ->where('users_id', $userId)
                ->delete()
            ){
                \Log::debug('删除用户关注日志失败');
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        }catch(\Exception $exception){
            \Log::debug($exception);
            DB::rollBack();
            return false;
        }

    }
}
