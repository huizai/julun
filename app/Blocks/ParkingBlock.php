<?php

namespace App\Blocks;
use App\Models\Turnover;
use App\Models\UserCoupons;
use App\Models\UserNumberPlate;
use App\Models\ParkingRule;
use App\Models\ParkingOrder;
use App\Models\Users;
use App\Libs\Ketuo;
use Illuminate\Support\Facades\DB;

class ParkingBlock{

    /**
     * 绑定车牌号
     * @param $userId
     * @param $data
     * @return int
     */
    public function bindingNumberPlate($userId, $number){
        $numberPlateModel   = new UserNumberPlate();
        $userNumber         = DB::table($numberPlateModel->getTable())->where('users_id', $userId)->get();
        if (count($userNumber) >= 3){
            return -1;
        }
        return DB::table($numberPlateModel->getTable())->insert([
            'users_id'  => $userId,
            'number'    => $number
        ]);
    }

    /**
     * 获取停车规则
     * @return mixed
     */
    public function getParkingRule(){
        $parkingRuleModel   = new ParkingRule();
        return DB::table($parkingRuleModel->getTable())->value('content');
    }

    /**
     * 获取用户车牌号
     */
    public function getUserNumberPlate($userId){
        $numberPlateModel   = new UserNumberPlate();
        return DB::table($numberPlateModel->getTable())
            ->select('id', 'number')
            ->where('users_id', $userId)->get();
    }

    /**
     * 获取用户停车缴费记录
     * @param $userId
     * @return mixed
     */
    public function parkingOrderList($userId, $page, $pageSize){
        $parkingOrderModel  = new ParkingOrder();
        $sql = DB::table($parkingOrderModel->getTable())
            ->select(
                'id',
                DB::raw("convert(pay_amount/100, decimal(15,2)) as pay_amount"),
                'number',
                'payTime',
                'elapsedTime',
                'pay_type',
                'orderNo',
                'entryTime',
                DB::raw("convert(discount_amount/100, decimal(15,2)) as discount_amount"),
                DB::raw("convert(payable/100, decimal(15,2)) as payable")

            )
            ->where('users_id', $userId)
            ->where('pay_status', 2)
            ->orderBy('create_time', 'desc');

        $count = $sql->count();

        $data = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();


        foreach ($data as $datum) {
            if ($datum->pay_type == 1){
                $datum->pay_type = '微信支付';
            }else if ($datum->pay_type == 2){
                $datum->pay_type = '支付宝支付';
            }else if ($datum->pay_type == 3){
                $datum->pay_type = '余额支付';
            }
        }

        return [
            'list'          => $data,
            'pagination'    => [
                'page'      => $page,
                'pageSize'  => $pageSize,
                'total'     => $count
            ]
        ];
    }

    /**
     * 创建停车场订单
     * @param $data
     */
    public function createParkingOrder($data){
        $parkingModel   = new ParkingOrder();
        unset($data['parkId']);
        unset($data['parkName']);
        unset($data['imgName']);
        unset($data['delayTime']);
        try{
            $parkingInfo = DB::table($parkingModel->getTable())
                ->where('users_id', $data['users_id'])
                ->where('entryTime', $data['entryTime'])
                ->where('pay_status', 1)
                ->first();
            if ($parkingInfo){
                return DB::table($parkingModel->getTable())
                    ->where('entryTime', $data['entryTime'])
                    ->where('users_id', $data['users_id'])
                    ->where('pay_status', 1)
                    ->update($data);
            }else{
                return DB::table($parkingModel->getTable())->insert($data);
            }
        }catch (\Exception $exception){
            \Log::error($exception);
        }



    }

    /**
     * 获取停车订单需要支付金额
     * @param $orderNo
     * @return mixed
     */
    public function getParkingPayAmount($orderNo){
        $parkingModel   = new ParkingOrder();
        return DB::table($parkingModel->getTable())
            ->where('orderNo', $orderNo)
            ->where('pay_status', 1)
            ->value('pay_amount');
    }

    /**
     * 删除车牌号
     * @param $id
     * @return mixed
     */
    public function deleteNumber($id){
        $userNumberModel    = new UserNumberPlate();
        return DB::table($userNumberModel->getTable())->where('id', $id)->delete();
    }

    /**
     * 停车场余额支付
     * @param $orderNo
     * @param $payAmount
     * @param $userId
     * @param $freeMoney
     * @param $freeTime
     * @return bool
     */
    public function parkingRechargePay($orderNo, $payAmount, $userId, $freeMoney, $freeTime){
        $userModel          = new Users();
        $parkingOrderModel  = new ParkingOrder();
        $turnoverModel      = new Turnover();
        $userCouponModel    = new UserCoupons();

        try{
            DB::beginTransaction();

            $orderDetail = DB::table($parkingOrderModel->getTable())
                ->where('orderNo', $orderNo)
                ->first();
            if (!$orderDetail){
                return false;
            }

            if (!empty($orderDetail->coupons_id) && $orderDetail->discount_amount > 0){
                if(DB::table($userCouponModel->getTable())
                        ->where('id', $orderDetail->coupons_id)
                        ->update(['is_use' => 1]) === false
                ){
                    DB::rollBack();
                    \Log::debug('修改用户优惠券信息失败');
                    return false;
                }
            }


            if(DB::table($userModel->getTable())->where('id', $userId)->decrement('balance', $payAmount) === false){
                \Log::debug('停车场余额支付用户余额扣款失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($parkingOrderModel->getTable())->where('orderNo', $orderNo)->update([
                'payTime'       => date('Y-m-d H:i:s', time()),
                'pay_status'    => 2,
                'pay_type'      => 3
            ])){
                \Log::debug('停车场余额支付修改记录失败');
                DB::rollBack();
                return false;
            }

            $turnover = DB::table($turnoverModel->getTable())
                ->where('date', date('Y-m-d', time()))
                ->first();
            if ($turnover){
                if(!DB::table($turnoverModel->getTable())
                    ->where('date', date('Y-m-d', time()))
                    ->increment('turnover', $payAmount)
                ){
                    DB::rollBack();
                    \Log::debug('修改平台营业额日志失败');
                    return false;
                }
            }else{
                if(!DB::table($turnoverModel->getTable())
                    ->insert([
                        'date'      => date('Y-m-d', time()),
                        'turnover'  => $payAmount
                    ])
                ){
                    DB::rollBack();
                    \Log::debug('添加平台营业额日志失败');
                    return false;
                }
            }

            $keTuo      = new Ketuo();
            $feeResult  = $keTuo->payParkingFee2(
                $orderNo,
                $payAmount,
                $freeMoney,
                5,
                $userId,
                $freeTime
            );
            if (!$feeResult){
                DB::rollBack();
                return false;
            }
            \Log::debug(json_encode($feeResult));

            DB::commit();
            return true;
        }catch(\Exception $exception){
            \Log::debug('停车场余额支付失败原因：'.$exception);
            DB::rollBack();
            return false;
        }

    }

    public function getParkingUserPayAmount($orderNo){
        $parkingOrderModel      = new ParkingOrder();
        return DB::table($parkingOrderModel->getTable())
            ->select('users_id', 'pay_amount', 'discount_amount', 'discount_time')
            ->where('orderNo', $orderNo)
            ->first();
    }

    public function getPaymentRecharge($orderNo, $freeMoney, $freeTime){
        $KeTuo          = new Ketuo();
        $data           = $KeTuo->getPaymentRecharge($orderNo, $freeMoney, $freeTime);
        if ($data['resCode'] == 0){
            return $data['data']['payable'];
        }else{
            return false;
        }
    }
}




