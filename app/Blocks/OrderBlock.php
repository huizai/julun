<?php
namespace App\Blocks;

use App\Libs\DianWoDa;
use App\Libs\ZhongxinPay\ClientResponseHandler;
use App\Libs\ZhongxinPay\PayHttpClient;
use App\Libs\ZhongxinPay\RequestHandler;
use App\Libs\ZhongxinPay\Utils;
use App\Models\Alipay;
use App\Models\Carts;
use App\Models\District;
use App\Models\Goods;
use App\Models\GoodsBargain;
use App\Models\GoodsGroupBuy;
use App\Models\GoodsSku;
use App\Models\GoodsSkuValue;
use App\Models\OrderBargain;
use App\Models\OrderBargainLog;
use App\Models\OrderGroupBuy;
use App\Models\OrderGroupBuyLog;
use App\Models\OrderRefund;
use App\Models\RedEnvelope;
use App\Models\StoreTurnover;
use App\Models\StoreVolume;
use App\Models\OrderActivity;
use App\Models\OrderAddress;
use App\Models\OrderCoupons;
use App\Models\OrderGoods;
use App\Models\OrderRedEnvelope;
use App\Models\Orders;
use App\Models\Spike;
use App\Models\StoreActivity;
use App\Models\StoreCoupons;
use App\Models\StoreIncomeLog;
use App\Models\StoreInfo;
use App\Models\Stores;
use App\Models\StoreSeat;
use App\Models\Turnover;
use App\Models\UserAddress;
use App\Models\UserCoupons;
use App\Models\UserIntegralLog;
use App\Models\UserRedEnvelope;
use App\Models\Users;
use App\Models\UserWalletLog;
use App\Models\WechatPay;
use App\Models\WechatUsers;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Yansongda\Pay\Pay;
use QrCode;
use function EasyWeChat\Kernel\Support\get_server_ip;

class OrderBlock{
    private function __cartStoreSql(array $search){
        $cartModel = new Carts();
        $storeModel = new Stores();

        $sql = DB::table($cartModel->getTable() . ' as c')
            ->select(
                'c.store_id',
                's.logo',
                's.name as store_name',
                DB::raw("convert(delivery_fee/100, decimal(15,2)) as delivery_fee"),
                DB::raw("convert(init_delivery_price/100, decimal(15,2)) as init_delivery_price")
            )
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'c.store_id');

        if(isset($search['users_id'])){
            $sql->where('c.users_id', $search['users_id']);
        }

        if(isset($search['company_id'])){
            $sql->where('c.company_id', $search['company_id']);
        }

        if(isset($search['store_id'])){
            $sql->where('c.store_id', $search['store_id']);
        }

        if(isset($search['type'])){
            $sql->where('c.type', $search['type']);
        }

        if(isset($search['cart_id'])){
            if(is_array($search['cart_id'])) {
                $sql->whereIn('c.id', $search['cart_id']);
            }else{
                $sql->where('c.id', $search['cart_id']);
            }
        }

        if(isset($search['store_type'])){
            $sql->whereIn('s.store_type_id', $search['store_type']);
        }

        return $sql;
    }
    private function __cartSql(array $search){
        $cartModel = new Carts();
        $goodsModel = new Goods();
        $goodsSkuModel = new GoodsSku();
        $storeModel = new Stores();
        $userModel = new Users();

        $sql = DB::table($cartModel->getTable() . ' as c')
            ->select(
                'c.*',
                'g.name',
                'g.thumbnail',
                'g.stock as stock',
                'g.selling_price as g_selling_price',
                'g.purchase_price as g_purchase_price',
                'g.original_price as g_original_price',
                'g.enable_sku',
                'gsku.selling_price as sku_selling_price',
                'gsku.purchase_price as sku_purchase_price',
                'gsku.original_price as sku_original_price',
                'gsku.thumbnail as sku_thumbnail',
                'gsku.sales_volume',
                'gsku.stock as sku_stock',
                's.logo',
                's.name as store_name',
                'u.name as user_name',
                'u.avatar'
            )
            ->leftJoin($goodsModel->getTable() . ' as g', 'c.goods_id', '=', 'g.id')
            ->leftJoin($goodsSkuModel->getTable() . ' as gsku', 'c.sku_path', '=', 'gsku.value_path')
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'c.store_id')
            ->leftJoin($userModel->getTable(). ' as u', 'u.id', '=', 'c.users_id');

        if(isset($search['users_id'])){
            $sql->where('c.users_id', $search['users_id']);
        }

        if(isset($search['company_id'])){
            $sql->where('c.company_id', $search['company_id']);
        }

        if(isset($search['seat_id'])){
            $sql->where('c.seat_id', $search['seat_id']);
        }

        if(isset($search['store_id'])){
            $sql->where('c.store_id', $search['store_id']);
        }

        if(isset($search['store_ids'])){
            $sql->whereIn('c.store_id', $search['store_ids']);
        }

        if (isset($search['type'])){
            $sql->where('c.type', $search['type']);
        }

        if(isset($search['cart_id'])){
            if(is_array($search['cart_id'])) {
                $sql->whereIn('c.id', $search['cart_id']);
            }else{
                $sql->where('c.id', $search['cart_id']);
            }
        }

        return $sql;
    }

    /**
     * 获取购物车列表
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @param null $couponId
     * @return int|\stdClass
     */
    public function cartList(array $search = [], int $page = 1, int $pageSize = 20,  $couponId = null){

        $userCouponModel    = new UserCoupons();
        $couponModel        = new StoreCoupons();

        $cartStoreSql = $this->__cartStoreSql($search);

        $cartStore = $cartStoreSql->groupBy('store_id')
            ->skip(($page - 1) * $pageSize)
            ->take($pageSize)
            ->get();

        $deliveryFee = 0;
        $storeIds = [];
        foreach ($cartStore as $cs){
            $deliveryFee += $cs->delivery_fee;
            if(!in_array($cs->store_id, $storeIds)) {
                $storeIds[] = $cs->store_id;
            }
        }

        $search['store_ids'] = $storeIds;

        $sql = $this->__cartSql($search);

        $cart = $sql->get();


        $skuIds = [];
        foreach ($cart as $c){
            $cSku = explode(',', $c->sku_path);
            $skuIds = array_merge($skuIds, $cSku);
        }


        foreach ($cartStore as $cs){
            $cs->goods = [];
            $cs->stores_id = $cs->store_id;
            foreach ($cart as $c){
                if($c->store_id === $cs->store_id){
                    $cs->goods[] = $c;
                }
            }
        }


        $cartStore = $this->activityDiscount($cartStore->toArray());

        $totalDiscountAmount    = 0;    //减免价格
        $totalPayAmount         = 0;    //支付价格
        $totalFullAmount        = 0;    //原价
        $goodsSum               = 0;    //商品数量
        $goodsNumber            = 0;    //商品总数

        foreach ($cartStore as $cs){
            $totalDiscountAmount += $cs->discount_amount;
            $totalPayAmount   += $cs->pay_amount;
            $goodsSum += $cs->goods_sum;
            $goodsNumber += $cs->goods_number;
            $totalFullAmount += $cs->full_amount;
        }

        //处理价格
        foreach ($cartStore as $cs){
            foreach ($cs->goods as $g){
                $g->selling_price   = priceIntToFloat($g->selling_price);
                $g->purchase_price  = priceIntToFloat($g->purchase_price);
                $g->original_price  = priceIntToFloat($g->original_price);
                unset($g->g_selling_price);
                unset($g->g_purchase_price);
                unset($g->g_original_price);
                unset($g->sku_selling_price);
                unset($g->sku_purchase_price);
                unset($g->sku_original_price);

            }
            $lack_amount = priceIntToFloat($cs->init_delivery_price-$cs->full_amount);
            $cs->lack_amount            = $lack_amount > 0 ? $lack_amount : 0;
            $cs->full_amount            = priceIntToFloat($cs->full_amount);
            $cs->pay_amount             = priceIntToFloat($cs->pay_amount);
            $cs->discount_amount        = priceIntToFloat($cs->discount_amount);
            $cs->useFRDiscountAmount    = priceIntToFloat($cs->useFRDiscountAmount);
            if(isset($cs->activity)){
                $cs->activity->data->full_amount        = priceIntToFloat($cs->activity->data->full_amount);
                $cs->activity->data->discount_amount    = priceIntToFloat($cs->activity->data->discount_amount);
            }
            $remainingPrice = $cs->init_delivery_price*100 - $cs->full_amount*100;
            $cs->remaining_price = $remainingPrice < 0 ? priceIntToFloat(0) : priceIntToFloat($remainingPrice);
        }



        /**
         * 优惠券
         */
        if (!empty($couponId)){

            $userCoupon = DB::table($userCouponModel->getTable())
                ->where('users_id', $search['users_id'])
                ->where('coupon_id', $couponId)
                ->where('is_use', 0)
                ->first();
            if (!$userCoupon){
                return -1;
            }

            $couponInfo = DB::table($couponModel->getTable())
                ->select()
                ->where('id', $couponId)
                ->where('stores_id', $search['store_id'])
                ->whereNull('delete_time')
                ->first();
            if (!$couponInfo){
                return -2;
            }

            if ($userCoupon->end_time < date('Y-m-d H:i:s', time())){
                return -3;
            }


            if ($couponInfo->full_amount <= $totalFullAmount){
                if ($couponInfo->type == 0){
                    $totalDiscountAmount += $couponInfo->discount_amount;
                    $totalPayAmount -= $couponInfo->discount_amount;
                    $couponInfo->full_amount        = priceIntToFloat($couponInfo->full_amount);
                    $couponInfo->discount_amount    = priceIntToFloat($couponInfo->discount_amount);
                }else{
                    $totalPayAmount -= (1-($couponInfo->discount_amount /100)) * $totalPayAmount;
                    $couponInfo->full_amount        = priceIntToFloat($couponInfo->full_amount);
                    $couponInfo->discount_amount    = ($couponInfo->discount_amount /10).'折';
                    $totalDiscountAmount += $couponInfo->discount_amount;

                }

            }else{
                return -4;
            }
        }


        $payAmount = $totalPayAmount < 0 ? 0 : $totalPayAmount;
        $payAmount += $deliveryFee*100;
        $obj = new \stdClass();
        $obj->cart                  = $cartStore;
        $obj->total_discount_amount = priceIntToFloat($totalDiscountAmount);
        $obj->total_pay_amount      = priceIntToFloat($payAmount);
        $obj->total_full_amount     = priceIntToFloat($totalFullAmount);
        $obj->coupon                = isset($couponInfo) ? $couponInfo : [];
        $obj->goods_sum             = $goodsSum;
        $obj->goods_number          = $goodsNumber;
        return $obj;
    }

    /**
     * 获取单个店铺购物车列表
     */
    public function cartListByOneStore(array $search = []){
        $sql = $this->__cartSql($search);
        $cart = $sql->get();
        //组合sku
        $skuIds = [];
        foreach ($cart as $c){
            $cSku = explode(',', $c->sku_path);
            $skuIds = array_merge($skuIds, $cSku);
        }

        //判断使用哪个价格
        foreach ($cart as $c){
            if($c->enable_sku){
                $c->selling_price = priceIntToFloat($c->sku_selling_price);
                $c->purchase_price = priceIntToFloat($c->sku_purchase_price);
                $c->original_price = priceIntToFloat($c->sku_original_price);
            }else{
                $c->selling_price = priceIntToFloat($c->g_selling_price);
                $c->purchase_price = priceIntToFloat($c->g_purchase_price);
                $c->original_price = priceIntToFloat($c->g_original_price);
            }
        }
        return $cart;
    }

    /**
     * 购物车分页
     * @param array $search
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function cartListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__cartStoreSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    public function initOrderBuyNow(int $userId, int $companyId, array $cart, int $orderType,$remark='',$address_id=0,$foodDeliveryTime,$deliveryTime){


        $goods_id = $cart[0]['goods_id'];
        $quantity = $cart[0]['quantity'];


        $goodsModel         = new Goods();
        $goodsSkuModel      = new GoodsSku();
        $goodsSkuValueModel = new GoodsSkuValue();
        $orderModel         = new Orders();
        $orderGoodsModel    = new OrderGoods();
        $orderAddressModel  = new OrderAddress();
        $userAddressModel   = new UserAddress();
        $userCouponModel    = new UserCoupons();
        $storeCouponModel   = new StoreCoupons();
        $orderCouponModel   = new OrderCoupons();
        $orderActivityModel = new OrderActivity();



        $goods = DB::table($goodsModel->getTable())
            ->where('id', $goods_id)
            ->whereNull('delete_time')
            ->first();


        if ($goods->enable_sku == 1){
            if(!isset($cart[0]['sku'])){
                return -2;
            }
        }
        $storeId = $goods->stores_id;
        if (isset($cart[0]['sku'])){
            $skuIds     = explode(',',$cart[0]['sku']);
            $sku = null;
            $skuValueText = [];
            //如果商品启用了sku
            if($goods->enable_sku){
                $skuValue = DB::table($goodsSkuValueModel->getTable())
                    ->whereIn('id', $skuIds)
                    ->get();

                foreach ($skuValue as $sv) {
                    $skuValueText[] = $sv->value;
                }

                $sku = implode(',', array_sort($skuIds));
                //判断sku是否存在
                $skuData = DB::table($goodsSkuModel->getTable())
                    ->where('goods_id', $goods_id)
                    ->where('value_path', $sku)
                    ->first();

                if (!$skuData) {
                    \Log::error("立即购买---商品{$goods_id}的规格{$sku}不存在");
                    return -1;
                }
            }
        }



        //商品信息
        $cartDataSql = DB::table($goodsModel->getTable() . ' as g')
            ->select(
                'g.id as  goods_id',
                'g.stores_id',
                'g.name',
                'g.selling_price as g_selling_price',
                'g.purchase_price as g_purchase_price',
                'g.original_price as g_original_price',
                'g.stock',
                'g.thumbnail',
                'g.enable_sku'
            )
            ->where('g.id', $goods_id);

        if (isset($cart[0]['sku'])){
            $cartDataSql->leftJoin($goodsSkuModel->getTable() . ' as sku', 'sku.goods_id', '=', 'g.id')
                ->addSelect(
                    'sku.selling_price as sku_selling_price',
                    'sku.purchase_price as sku_purchase_price',
                    'sku.original_price as sku_original_price',
                    'sku.thumbnail as sku_thumbnail',
                    'sku.stock as sku_stock',
                    'sku.value_path as sku_path'
                )
                ->where('sku.value_path', $sku);
        }


        $cartData = $cartDataSql->first();
        $cartData->quantity = intval($quantity);

        if (isset($cart[0]['sku'])){
            $cartData->sku_value = implode(';', $skuValueText);
        }



        //订单批次号
        /**
         * 多个商户会创建多个订单，但要属于同一批次
         */
        $batch = $userId.(microtime(true) * 10000).mt_rand(1000, 9999);
        /**
         * 根据店铺组合数据
         */

        $store = [];
        $seatId = null;
        foreach ([$cartData] as $cd){
            if(!isset($store[$cd->stores_id])) {
                $storeIds[] = $cd->stores_id;

                $store[$cd->stores_id] = new \stdClass();
                $store[$cd->stores_id]->stores_id =  0;
                $store[$cd->stores_id]->goods_price = 0;
                $store[$cd->stores_id]->goods = [];
                $store[$cd->stores_id]->sear_id = null;
            }

            if($cd->enable_sku == 1) {
                $store[$cd->stores_id]->goods_price += $cd->sku_selling_price;
            }else{
                $store[$cd->stores_id]->goods_price += $cd->g_selling_price;
            }

            $store[$cd->stores_id]->stores_id = $cd->stores_id;
            $store[$cd->stores_id]->goods[] = $cd;
        }


        /**
         * 设置秒杀的商品不能使用优惠券和参加活动
         */
        //订单活动
        $store = $this->activityDiscount($store);



        //订单优惠券
        //获取用户优惠券
        $userCoupon = DB::table($userCouponModel->getTable() . ' as uc')
            ->select(
                'uc.id as user_coupon_id',
                'uc.start_time',
                'uc.end_time',
                'uc.id',
                'sc.stores_id',
                'sc.name',
                'sc.term_of_validity',
                'sc.type',
                'sc.full_amount',
                'sc.discount_amount',
                'sc.id as coupon_id',
                'sc.belong',
                'sc.scope'
            )
            ->leftJoin($storeCouponModel->getTable() . ' as sc', 'sc.id', '=', 'uc.coupon_id')
            ->where('uc.users_id', $userId)
            ->where('uc.is_use', 0)
            ->where('sc.virtual', 1)
            ->where('sc.scope', '!=', '停车场')
            ->where('sc.scope', '!=', '余额')
            ->whereNull('uc.delete_time')
            ->get();


        $order = DB::table($orderModel->getTable())
            ->where('users_id', $userId)
            ->whereIn('status', ['1', '2', '3', '4', '6'])
            ->first();



        /**
         * 设置秒杀的商品不能使用优惠券和参加活动
         */
        foreach ($store as $s){
            //获取最大金额的优惠券
            $useCouponAmount = 0;
            $useCoupon = null;

            $s->pay_amount -= $s->spike_price;
            if ($s->pay_amount > 0){
                foreach ($userCoupon as $uc){
                    if ($uc->scope == '新人首单红包' && $uc->belong == 1){
                        if ($order){
                            continue;
                        }
                    }
                    //判断优惠券是否在生效时间内
                    if (date('Y-m-d H:i:s', time()) < $uc->end_time){
                        //coupon belong 1 平台优惠券 2 商家优惠券
                        if($uc->belong === 1 || $uc->stores_id === $s->stores_id){
                            if($uc->type === 0){
                                //满减券
                                /**
                                 * 如果满足满减条件
                                 * 并且减少的金额比上一个多
                                 */
                                if ($s->full_amount >= $uc->full_amount && $uc->discount_amount > $useCouponAmount) {
                                    $useCouponAmount = $uc->discount_amount;
                                    $useCoupon = $uc;
                                }
                            }else if($uc->type === 1){
                                //满折券
                                if ($s->full_amount >= $uc->full_amount && $uc->discount_amount * $s->full_amount / 100 > $useCouponAmount) {
                                    $useCouponAmount = $uc->discount_amount * (1- ($s->full_amount / 100));
                                    $uc->discount_amount = $useCouponAmount;
                                    $useCoupon = $uc;
                                }
                            }
                        }
                    }

                }
            }




            $s->coupon          = $useCoupon;
            $s->pay_amount      -= $useCouponAmount;
            $s->pay_amount      += $s->spike_price;
            $s->discount_amount += $useCouponAmount;
        }

        //执行sql
        try {
            DB::beginTransaction();
            foreach ($store as $s){
                //组合订单基本数据
                $order = [
                    'batch'             => $batch,
                    'company_id'        => $companyId,
                    'users_id'          => $userId,
                    'seat_id'           => $seatId,
                    'remark'            => $remark,
                    'stores_id'         => $s->stores_id,
                    'goods_price'       => $s->goods_price,
                    'pay_amount'        => $s->pay_amount,
                    'discount_amount'   => $s->discount_amount,
                    'order_sn'          => $s->stores_id . (microtime(true) * 10000) . $userId,
                    'type'              => $orderType,
                    'create_time'       => date('Y-m-d H:i:s', time())
                ];

                if (!empty($foodDeliveryTime) && !empty($deliveryTime)){
                    $order['food_delivery_time'] = $foodDeliveryTime;
                    $order['delivery_time'] = $deliveryTime;
                }

                $orderId = DB::table($orderModel->getTable())
                    ->insertGetId($order);

                if(!$orderId){
                    \Log::error("创建订单失败:".json_encode($order)."");
                    return false;
                }

                //订单优惠券信息
                if(!empty($s->coupon)) {
                    $orderCouponData = [
                        'orders_id'     => $orderId,
                        'coupon_id'     => $s->coupon->coupon_id,
                        'amount'        => $s->coupon->discount_amount,
                        'start_time'    => $s->coupon->start_time,
                        'end_time'      => $s->coupon->end_time,
                        'coupon_type'   => $s->coupon->belong
                    ];

                    if(
                        DB::table($orderCouponModel->getTable())
                            ->insert($orderCouponData) === false
                    ){
                        \Log::error("初始化订单----添加订单优惠券失败:" . json_encode($orderCouponData) . "");
                        DB::rollBack();
                        return false;
                    }

                }

                //订单活动信息
                if(!empty($s->activity)){
                    $orderActivityData = [
                        'orders_id'         => $orderId,
                        'store_activity_id' => $s->activity->id,
                        'type'              => $s->activity->type,
                        'data'              => json_encode($s->activity->data)
                    ];

                    if(
                        DB::table($orderActivityModel->getTable())
                            ->insert($orderActivityData) === false
                    ){
                        \Log::error("初始化订单----添加订单活动失败:" . json_encode($orderActivityData) . "");
                        DB::rollBack();
                        return false;
                    }
                }


                //组合订单商品数据
                $orderGoods = [];

                foreach ($s->goods as $key => $sg){
                    $orderGoods[$key] = [
                        'orders_id'         => $orderId,
                        'goods_id'          => $sg->goods_id,
                        'selling_price'     => $sg->selling_price,
                        'quantity'          => $sg->quantity,
                        'name'              => $sg->name,
                        'thumbnail'         => $sg->thumbnail
                    ];
                    if (isset($sg->sku_value)){
                        $orderGoods[$key]['sku'] = $sg->sku_value;
                    }
                    if (isset($sg->sku_path)){
                        $orderGoods[$key]['sku_path'] = $sg->sku_path;
                    }
                }
                \Log::debug($orderGoods);
                if(
                    DB::table($orderGoodsModel->getTable())
                        ->insert($orderGoods)
                    === false
                ){
                    \Log::error("插入订单商品失败:".json_encode($orderGoods)."");
                    DB::rollBack();
                    return false;
                }
            }


            /**
             * 选择订单的地址
             * 如果有默认的就是默认的
             * 没有就选择上一次使用的
             */
            $address = DB::table($userAddressModel->getTable())
                ->where('users_id', $userId)
                ->where('company_id', $companyId)
                ->whereNull('delete_time')
                ->orderBy('use_time', 'desc')
                ->get();

            if(!empty($address->toArray())){
                $userOrderAddress = $address[0];

                foreach ($address as $a){
                    if($a->default == 1){
                        $userOrderAddress = $a;
                    }
                }


                if($address_id>0){
                    $userOrderAddress = DB::table($userAddressModel->getTable())
                        ->where('users_id', $userId)
                        ->where('id', $address_id)
                        ->whereNull('delete_time')
                        ->orderBy('use_time', 'desc')
                        ->first();
                }

                $orderAddress = [
                    'batch'     => $batch,
                    'consignee' => $userOrderAddress->consignee,
                    'contact'   => $userOrderAddress->contact,
                    'province'  => $userOrderAddress->province,
                    'city'      => $userOrderAddress->city,
                    'district'  => $userOrderAddress->district,
                    'address'   => $userOrderAddress->address,
                    'longitude' => $userOrderAddress->longitude,
                    'latitude'  => $userOrderAddress->latitude
                ];


                if(
                    DB::table($orderAddressModel->getTable())
                        ->insert($orderAddress)
                    === false
                ){
                    \Log::error("插入订单收货地址失败:".json_encode($orderAddress)."");
                    DB::rollBack();
                    return false;
                }

                if(
                    DB::table($userAddressModel->getTable())
                        ->where('id', $userOrderAddress->id)
                        ->update([
                            'use_time' => date('Y-m-d H:i:s', time())
                        ])
                    === false
                ){
                    \Log::error("插入用户地址使用时间失败:".json_encode($orderAddress)."");
                    DB::rollBack();
                    return false;
                }
            }

            DB::commit();
            Redis::setex('order-cancel:'.$batch,900,'test');
            return $batch;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }

    public function initOrder(int $userId, int $companyId, array $cart, int $orderType,$remark,$address_id,$online){
        $cartIds = [];

        foreach ($cart as $c){
            $cartIds[] = $c->cart_id;
        }


        $cartModel          = new Carts();
        $goodsModel         = new Goods();
        $goodsSkuModel      = new GoodsSku();
        $orderModel         = new Orders();
        $orderGoodsModel    = new OrderGoods();
        $orderAddressModel  = new OrderAddress();
        $userAddressModel   = new UserAddress();
        $userCouponModel    = new UserCoupons();
        $storeCouponModel   = new StoreCoupons();
        $orderCouponModel   = new OrderCoupons();
        $orderActivityModel = new OrderActivity();
        $storeModel         = new Stores();
        $storeInfoModel     = new StoreInfo();

        //购物车商品信息
        $cartData = DB::table($cartModel->getTable() . ' as c')
            ->select(
                'g.id as  goods_id',
                'g.stores_id',
                'c.quantity',
                'c.sku_value',
                'c.sku_path',
                'c.seat_id',
                'sku.stock as sku_stock',
                'sku.thumbnail as sku_thumbnail',
                'sku.selling_price as sku_selling_price',
                'sku.purchase_price as sku_purchase_price',
                'sku.original_price as sku_original_price',
                'g.thumbnail',
                'g.stock',
                'g.name',
                'g.slogan',
                'g.classify_id',
                'g.selling_price as g_selling_price',
                'g.purchase_price as g_purchase_price',
                'g.original_price as g_original_price',
                'g.enable_sku',
                DB::raw("convert(delivery_fee/100, decimal(15,2)) as delivery_fee"),
                'si.longitude',
                'si.latitude',
                'si.address',
                's.store_type_id',
                's.mobile',
                's.name as store_name'
            )
            ->leftJoin($goodsModel->getTable() . ' as g', 'c.goods_id', '=', 'g.id')
            ->leftJoin($goodsSkuModel->getTable() . ' as sku', 'c.sku_path', '=', 'sku.value_path')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'c.store_id')
            ->leftJoin($storeInfoModel->getTable().' as si', 'si.store_id', '=', 'c.store_id')
            ->whereIn('c.id', $cartIds)
            ->where('c.users_id', $userId)
            ->where('c.company_id', $companyId)
            ->where('c.quantity', '>', 0)
            ->get();





        //订单批次号
        /**
         * 多个商户会创建多个订单，但要属于同一批次
         */
        $batch = $userId.(microtime(true) * 10000).mt_rand(1000, 9999);


        /**
         * 根据店铺组合数据
         */
        $store = [];
        $seatId = null;
        $goodsId = [];
        foreach ($cartData as $cd){
            $goodsId[] = $cd->goods_id;
            if(!isset($store[$cd->stores_id])) {
                $storeIds[] = $cd->stores_id;
                $store[$cd->stores_id] = new \stdClass();
                $store[$cd->stores_id]->stores_id       =  0;
                $store[$cd->stores_id]->goods_price     = 0;
                $store[$cd->stores_id]->delivery_fee    = $cd->delivery_fee;
                $store[$cd->stores_id]->goods           = [];
                $store[$cd->stores_id]->sear_id         = null;
                $store[$cd->stores_id]->address         = $cd->address;
                $store[$cd->stores_id]->longitude       = $cd->longitude;
                $store[$cd->stores_id]->latitude        = $cd->latitude;
                $store[$cd->stores_id]->mobile          = $cd->mobile;
                $store[$cd->stores_id]->store_name      = $cd->store_name;
                $store[$cd->stores_id]->store_type_id   = $cd->store_type_id;

            }

            if($cd->enable_sku) {
                $store[$cd->stores_id]->goods_price += $cd->sku_selling_price;
            }else{
                $store[$cd->stores_id]->goods_price += $cd->g_selling_price;
            }

            $store[$cd->stores_id]->stores_id = $cd->stores_id;
            $store[$cd->stores_id]->goods[] = $cd;
            $seatId = $cd->seat_id;
        }






        /**
         * 设置秒杀的商品不能使用优惠券和参加活动
         */
        //订单活动
        $store = $this->activityDiscount($store);

        //订单优惠券
        //获取用户优惠券
        $userCoupon = DB::table($userCouponModel->getTable() . ' as uc')
            ->select(
                'uc.id as user_coupon_id',
                'uc.start_time',
                'uc.end_time',
                'uc.id',
                'sc.stores_id',
                'sc.name',
                'sc.term_of_validity',
                'sc.type',
                'sc.full_amount',
                'sc.discount_amount',
                'sc.id as coupon_id',
                'sc.belong'
            )
            ->leftJoin($storeCouponModel->getTable() . ' as sc', 'sc.id', '=', 'uc.coupon_id')
            ->where('uc.users_id', $userId)
            ->where('uc.is_use', 0)
            ->where('sc.virtual', 1)
            ->where('sc.scope', '!=', '停车场')
            ->where('sc.scope', '!=', '余额')
            ->whereNull('uc.delete_time')
            ->get();




        foreach ($store as $s){
            //获取最大金额的优惠券
            $useCouponAmount = 0;
            $useCoupon = null;

            $s->pay_amount -= $s->spike_price;
            if ($s->pay_amount > 0){
                foreach ($userCoupon as $uc) {
                    //coupon store == 0 是平台发放的优惠券

                    if ($uc->stores_id === 0 || $uc->stores_id === $s->stores_id) {
                        if ($uc->type === 0) {
                            //满减券
                            /**
                             * 如果满足满减条件
                             * 并且减少的金额比上一个多
                             */
                            if ($s->full_amount >= $uc->full_amount && $uc->discount_amount > $useCouponAmount) {
                                $useCouponAmount = $uc->discount_amount;
                                $useCoupon = $uc;
                            }
                        } else if ($uc->type === 1) {
                            //满折券
                            if ($s->full_amount >= $uc->full_amount && ($s->pay_amount * (1 - ($uc->discount_amount / 100))) > $useCouponAmount) {
                                $useCouponAmount = $s->pay_amount * (1 - ($uc->discount_amount / 100));
                                $uc->discount_amount = $useCouponAmount;
                                $useCoupon = $uc;

                            }
                        }
                    }
                }
            }


            $s->pay_amount      -= $useCouponAmount;
            $s->coupon          = $useCoupon;
            $s->discount_amount += $useCouponAmount;

            if ($online == 0 && $s->store_type_id == 1){
                $s->pay_amount      += $s->spike_price + (($s->delivery_fee * 100) * 0.4);
            }else if($online == 0 && $s->store_type_id == 2) {
                $s->pay_amount      += $s->spike_price + ($s->delivery_fee * 100);
            }
        }


        //执行sql
        try {
            DB::beginTransaction();

            /**
             * 选择订单的地址
             * 如果有默认的就是默认的
             * 没有就选择上一次使用的
             */
            if($address_id > 0){
                $userOrderAddress = DB::table($userAddressModel->getTable())
                    ->where('users_id', $userId)
                    ->where('id', $address_id)
                    ->whereNull('delete_time')
                    ->first();
            }else{
                $address = DB::table($userAddressModel->getTable())
                    ->where('users_id', $userId)
                    ->where('company_id', $companyId)
                    ->whereNull('delete_time')
                    ->orderBy('use_time', 'desc')
                    ->get()->toArray();

                if (!empty($address)){
                    $userOrderAddress = $address[0];
                    foreach ($address as $a){
                        if($a->default == 1){
                            $userOrderAddress = $a;
                        }
                    }
                }

            }

            if(isset($userOrderAddress) && !empty($userOrderAddress)){

                $orderAddress = [
                    'batch'     => $batch,
                    'consignee' => $userOrderAddress->consignee,
                    'contact'   => $userOrderAddress->contact,
                    'province'  => $userOrderAddress->province,
                    'city'      => $userOrderAddress->city,
                    'district'  => $userOrderAddress->district,
                    'address'   => $userOrderAddress->address,
                    'longitude' => $userOrderAddress->longitude,
                    'latitude'  => $userOrderAddress->latitude
                ];


                if(
                    DB::table($orderAddressModel->getTable())
                        ->insert($orderAddress)
                    === false
                ){
                    \Log::error("插入订单收货地址失败:".json_encode($orderAddress)."");
                    DB::rollBack();
                    return false;
                }

                if(
                    DB::table($userAddressModel->getTable())
                        ->where('id', $userOrderAddress->id)
                        ->update([
                            'use_time' => date('Y-m-d H:i:s', time())
                        ])
                    === false
                ){
                    \Log::error("插入用户地址使用时间失败:".json_encode($orderAddress)."");
                    DB::rollBack();
                    return false;
                }
            }
            $dianWoDa       = new DianWoDa();


            foreach ($store as $s){
                $deliveryFee = $s->delivery_fee * 100;
                if ($s->store_type_id == 1){
                    //获取配送费
                    $deliveryFeeResult = $dianWoDa->orderCostEstimate(
                        [
                            'stores_id'     => $s->stores_id,
                            'stores_name'   => $s->store_name,
                            'mobile'        => $s->mobile,
                            'address'       => $s->address,
                            'lat'           => $s->latitude,
                            'lng'           => $s->longitude
                        ],
                        [
                            'address'       => $orderAddress['address'],
                            'lat'           => $orderAddress['latitude'],
                            'lng'           => $orderAddress['longitude']
                        ]
                    );

                    \Log::debug($deliveryFeeResult);

                    if ($deliveryFeeResult['code'] == 'success'){
                        $deliveryFee = $deliveryFeeResult['data']['total_price'];
                    }else if (isset($deliveryFeeResult['sub_code']) && !empty($deliveryFeeResult['sub_code'])){
                        if ($deliveryFeeResult['sub_code'] == 'unsupported_city'){
                            return -1;
                        }
                        if ($deliveryFeeResult['sub_code'] == 'seller_not_existed'){
                            return -2;
                        }
                        if ($deliveryFeeResult['sub_code'] == 'seller_unsettled'){
                            return -3;
                        }
                        if ($deliveryFeeResult['sub_code'] == 'seller_disabled'){
                            return -4;
                        }
                        if ($deliveryFeeResult['sub_code'] == 'out_of_distance'){
                            return -5;
                        }
                        if ($deliveryFeeResult['sub_code'] == 'no_enough_balance'){
                            return -6;
                        }
                        if ($deliveryFeeResult['sub_code'] == 'repeated_order'){
                            return -7;
                        }
                        DB::rollBack();
                        return false;
                    }else if($deliveryFeeResult['code'] != 'success'){
                        DB::rollBack();
                        return false;
                    }
                }
                //组合订单基本数据
                $order = [
                    'batch'             => $batch,
                    'company_id'        => $companyId,
                    'users_id'          => $userId,
                    'seat_id'           => $seatId,
                    'stores_id'         => $s->stores_id,
                    'goods_price'       => $s->goods_price,
                    'pay_amount'        => $s->pay_amount,
                    'discount_amount'   => $s->discount_amount,
                    'order_sn'          => $s->stores_id . (microtime(true) * 10000) . $userId,
                    'type'              => $s->store_type_id == 4 ? 1 : $orderType,
                    'remark'            => $remark,
                    'is_online'         => $online,
                    'create_time'       => date('Y-m-d H:i:s', time()),
                    'status'            => 0
                ];

                if ($online == 0 && $s->store_type_id == 1){
                    $order['delivery_fee']          = $deliveryFee * 0.4;
                    $order['total_delivery_fee']     = $deliveryFee;
                }else if($online == 0 && $s->store_type_id == 2){
                    $order['delivery_fee']          = $deliveryFee;
                    $order['total_delivery_fee']     = $deliveryFee;
                }else{
                    $order['delivery_fee'] = 0;
                    $order['total_delivery_fee']     = 0;
                }

//                if ($orderType == 1){
//                    $order['status'] = 2;
//                }

                $orderId = DB::table($orderModel->getTable())
                    ->insertGetId($order);
                \Log::debug('订单批次号：'.$batch.'，订单ID：'.$orderId);


                if(!$orderId){
                    DB::rollBack();
                    \Log::error("创建订单失败:".json_encode($order)."");
                    return false;
                }


                //订单优惠券信息
                if(!empty($s->coupon)) {
                    $orderCouponData = [
                        'orders_id'     => $orderId,
                        'coupon_id'     => $s->coupon->coupon_id,
                        'amount'        => $s->coupon->discount_amount,
                        'start_time'    => $s->coupon->start_time,
                        'end_time'      => $s->coupon->end_time,
                        'coupon_type'   => $s->coupon->belong,
                        'user_coupon'   => $s->coupon->user_coupon_id
                    ];

                    if(
                        DB::table($orderCouponModel->getTable())
                        ->insert($orderCouponData) === false
                    ){
                        \Log::error("初始化订单----添加订单优惠券失败:" . json_encode($orderCouponData) . "");
                        DB::rollBack();
                        return false;
                    }

                }


                //订单活动信息
                if(!empty($s->activity)){
                    $orderActivityData = [
                        'orders_id'         => $orderId,
                        'store_activity_id' => $s->activity->id,
                        'type'              => $s->activity->type,
                        'data'              => json_encode($s->activity->data)
                    ];

                    if(
                        DB::table($orderActivityModel->getTable())
                        ->insert($orderActivityData) === false
                    ){
                        \Log::error("初始化订单----添加订单活动失败:" . json_encode($orderActivityData) . "");
                        DB::rollBack();
                        return false;
                    }
                }


                //组合订单商品数据
                $orderGoods = [];

                foreach ($s->goods as $sg){
                    $orderGoods[] = [
                        'orders_id'         => $orderId,
                        'goods_id'          => $sg->goods_id,
                        'selling_price'     => $sg->selling_price,
                        'quantity'          => $sg->quantity,
                        'sku'               => $sg->sku_value,
                        'name'              => $sg->name,
                        'thumbnail'         => $sg->thumbnail,
                        'sku_path'          => $sg->sku_path
                    ];
                }
                if(
                    DB::table($orderGoodsModel->getTable())
                        ->insert($orderGoods)
                    === false
                ){
                    \Log::error("插入订单商品失败:".json_encode($orderGoods)."");
                    DB::rollBack();
                    return false;
                }
            }

            /**
             * 删除购物车已提交订单的数据
             */

            if(
                DB::table($cartModel->getTable())
                    ->whereIn('id',$cartIds)
                    ->delete()
                === false
            ){
                \Log::error("购物车数据删除失败:".json_encode($cartData)."");
                return false;
            }

            /**
             * 放入redis
             */
            Redis::setex('order-cancel:'.$batch, 900,'test');
            DB::commit();
            //返回批次号

            return $batch;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::debug($exception);
            return false;
        }
    }

    /**
     * 更具订单批次号获取订单信息
     * @param $batch
     * @return bool|\Illuminate\Support\Collection
     */
    public function orderByBatch($batch){
        $orderModel         = new Orders();
        $orderAddressModel  = new OrderAddress();
        $storeModel         = new Stores();
        $orderCouponModel   = new OrderCoupons();
        $storeCouponModel   = new StoreCoupons();
        $orderGoodsModel    = new OrderGoods();
        $orderActivityModel = new OrderActivity();
        $storeActivityModel = new StoreActivity();
        $storeSeatModel     = new StoreSeat();
        $storeInfoModel     = new StoreInfo();

        $prefix = env('DB_PREFIX');
        $order = DB::table($orderModel->getTable() . ' as o')
            ->select(
                'o.id',
                'o.batch',
                'o.company_id',
                'o.users_id',
                'o.stores_id',
                'o.order_sn',
                'o.create_time',
                'ss.seat_number',
                DB::raw("convert({$prefix}o.goods_price/100, decimal(15,2)) as goods_price"),
                DB::raw("convert({$prefix}o.pay_amount/100, decimal(15,2)) as pay_amount"),
                DB::raw("convert({$prefix}o.discount_amount/100, decimal(15,2)) as discount_amount"),
                'o.pay_time',
                'o.pay_type',
                'o.status',
                'o.remark',
                'o.food_delivery_time',
                'o.delivery_time',
                'o.create_time',
                's.id as store_id',
                's.name as store_name',
                's.logo as store_logo',
                'si.longitude',
                'si.latitude',
                'si.address as store_address',
                DB::raw("convert({$prefix}s.delivery_fee/100, decimal(15,2)) as delivery_fee")
            )
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'o.stores_id')
            ->leftJoin($storeInfoModel->getTable(). ' as si', 'si.store_id','=', 's.id' )
            ->leftJoin($orderAddressModel->getTable() . ' as oa', 'oa.batch', '=', 'o.batch')
            ->leftJoin($storeSeatModel->getTable() . ' as ss', 'ss.id', '=', 'o.seat_id')
            ->where('o.batch', $batch)
            ->whereNull('o.delete_time')
            ->get()->toArray();

        if(empty($order)){
            return false;
        }

        $orderIds = [];
        $payTime = '';
        $storeAddress = [];
        foreach ($order as $key => $o){
            $orderIds[] = $o->id;
            $payTime = naturalNumber(strtotime($o->create_time) + 900 - time());
            $storeAddress[$key]['store_id']     = $o->store_id;
            $storeAddress[$key]['address']      = $o->store_address;
            $storeAddress[$key]['store_name']   = $o->store_name;
            $storeAddress[$key]['longitude']    = $o->longitude;
            $storeAddress[$key]['latitude']     = $o->latitude;
        }

        //组合商品
        $goods = DB::table($orderGoodsModel->getTable())
            ->select(
                'id',
                'orders_id',
                'goods_id',
                'name',
                'thumbnail',
                DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                'quantity',
                'sku'
            )
            ->whereIn('orders_id', $orderIds)
            ->get();

        $i = 0;
        $payAmount = 0;
        $totalGoodsSum = 0;
        $totalGoodsNumber = 0;
        $totalDiscountAmount = 0;
        foreach ($order as $o){
            $payAmount = bcadd($payAmount, $o->pay_amount, 2);
            $totalDiscountAmount = bcadd($totalDiscountAmount, $o->discount_amount, 2);
            $o->goods = [];
            $o->goods_sum = 0;
            $o->goods_number = 0;
            foreach ($goods as $g){
                if($g->orders_id === $o->id){
                    $o->goods[] = $g;
                    $o->goods_sum += $g->quantity;
                    $o->goods_number++;

                    $totalGoodsSum += $g->quantity;
                    $totalGoodsNumber++;
                }
            }

            if(empty($o->goods)){
                unset($order[$i]);
            }
            $i++;
        }

        //订单参与的活动 && 优惠券
        $activity = DB::table($orderActivityModel->getTable() . ' as oa')
            ->leftJoin($storeActivityModel->getTable() . ' as sa', 'sa.id', '=', 'oa.store_activity_id')
            ->whereIn('oa.orders_id', $orderIds)
            ->get();

        $coupon = DB::table($orderCouponModel->getTable().' as oc')
            ->select('sc.*', 'oc.orders_id')
            ->leftJoin($storeCouponModel->getTable().' as sc', 'oc.coupon_id' , '=', 'sc.id')
            ->where('oc.orders_id', $orderIds)
            ->get();


        $activityConfig = \Config::get('activity');
        foreach ($order as $o){
            $o->activity = new \StdClass();
            foreach ($activity as $a){
                if($a->orders_id === $o->id){
                    $a->name = '活动';
                    if(isset($activityConfig[$a->type])){
                        $a->name = $activityConfig[$a->type]['name'];
                    }
                    $a->data = json_decode($a->data);
                    $o->activity = $a;
                }
            }


            foreach ($coupon as $item) {
                if ($item->orders_id == $o->id){
                    $o->coupon = $item;
                    $o->coupon->full_amount = priceIntToFloat($o->coupon->full_amount);
                    $o->coupon->discount_amount = priceIntToFloat($o->coupon->discount_amount);
                }else{
                    $o->coupon = [];
                }
            }
        }

        $address = DB::table($orderAddressModel->getTable())
            ->where('batch', $batch)
            ->first();

        $obj = new \stdClass();
        $obj->address                   = $address;
        $obj->order                     = $order;
        $obj->pay_amount                = $payAmount;
        $obj->totalGoodsSum             = $totalGoodsSum;
        $obj->totalGoodsNumber          = $totalGoodsNumber;
        $obj->totalDiscountAmount       = $totalDiscountAmount;
        $obj->remaining_payment_time    = $payTime;
        $obj->store_address             = $storeAddress;
        return $obj;
    }

    public function orderAliPayInfo($search){
        $orderModel         = new Orders();
        $aliPayModel        = new Alipay();
        $userModel          = new Users();
        $orderCouponModel   = new OrderCoupons();
        $userCouponModel    = new UserCoupons();
        $storeModel         = new Stores();
        $orderAddressModel  = new OrderAddress();
        $storeInfoModel     = new StoreInfo();

        $sql = DB::table($orderModel->getTable() . ' as o')
            ->select(
                'o.*',
                'wp.out_trade_no',
                'wp.orders_id',
                'uc.is_use',
                's.store_type_id',
                'oa.consignee',
                'oa.contact',
                'oa.address as user_address',
                'oa.longitude as user_longitude',
                'oa.latitude as user_latitude',
                'si.address as store_address',
                's.mobile as store_mobile',
                's.name as store_name',
                'si.longitude as store_longitude',
                'si.latitude as store_latitude',
                's.store_type_id'
            )
            ->leftJoin($orderCouponModel->getTable().' as oc', 'oc.orders_id', '=', 'o.id')
            ->leftJoin($userCouponModel->getTable().' as uc', 'uc.id', '=', 'oc.user_coupon')
            ->leftJoin($userModel->getTable() . ' as u', 'u.id', '=', 'o.users_id')
            ->leftJoin($aliPayModel->getTable() . ' as wp', 'wp.batch', '=', 'o.batch')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'o.stores_id')
            ->leftJoin($orderAddressModel->getTable().' as oa', 'oa.batch', '=', 'o.batch')
            ->leftJoin($storeInfoModel->getTable().' as si', 'si.store_id', '=', 'o.stores_id');

        if(isset($search['users_id'])){
            $sql->where('o.users_id', $search['users_id']);
        }
        if(isset($search['status'])){
            $sql->where('o.status', $search['status']);
        }
        if(isset($search['pay_amount'])){
            $sql->where('o.total_amount', $search['pay_amount']);
        }
        if(isset($search['out_trade_no'])){
            $order = $sql->where('wp.out_trade_no', $search['out_trade_no'])->first();
        }
        if(isset($search['batch'])){
            $order = $sql->where('o.batch', $search['batch'])->get();
        }
        if(isset($search['orders_id'])){
            $order = $sql->where('o.id', $search['orders_id'])->first();
        }

        return $order;
    }

    public function orderWechatPayInfo($search){
        $orderModel         = new Orders();
        $wechatPayModel     = new WechatPay();
        $userModel          = new Users();
        $wechatUserModel    = new WechatUsers();
        $userCouponModel    = new UserCoupons();
        $orderCouponModel   = new OrderCoupons();
        $storeModel         = new Stores();
        $orderAddressModel  = new OrderAddress();
        $storeInfoModel     = new StoreInfo();

        $sql = DB::table($orderModel->getTable() . ' as o')
            ->select(
                'o.*',
                'wu.openid',
                'wp.out_trade_no',
                'wp.transaction_id',
                'wp.refund_no',
                'wp.orders_id',
                'uc.is_use',
                's.store_type_id',
                'oa.consignee',
                'oa.contact',
                'oa.address as user_address',
                'oa.longitude as user_longitude',
                'oa.latitude as user_latitude',
                'si.address as store_address',
                's.mobile as store_mobile',
                's.name as store_name',
                'si.longitude as store_longitude',
                'si.latitude as store_latitude',
                's.store_type_id'
            )
            ->leftJoin($orderCouponModel->getTable().' as oc', 'oc.orders_id', '=', 'o.id')
            ->leftJoin($userCouponModel->getTable().' as uc', 'uc.id', '=', 'oc.user_coupon')
            ->leftJoin($userModel->getTable() . ' as u', 'u.id', '=', 'o.users_id')
            ->leftJoin($wechatUserModel->getTable() . ' as wu', 'wu.id', '=', 'u.wechat_user_id')
            ->leftJoin($wechatPayModel->getTable() . ' as wp', 'wp.batch', '=', 'o.batch')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'o.stores_id')
            ->leftJoin($orderAddressModel->getTable().' as oa', 'oa.batch', '=', 'o.batch')
            ->leftJoin($storeInfoModel->getTable().' as si', 'si.store_id', '=', 'o.stores_id');

        if(isset($search['users_id'])){
            $sql->where('o.users_id', $search['users_id']);
        }
        if(isset($search['status'])){
            $sql->where('o.status', $search['status']);
        }
        if(isset($search['pay_amount'])){
            $sql->where('o.pay_amount', $search['pay_amount']);
        }
        if(isset($search['out_trade_no'])){
            $order = $sql->where('wp.out_trade_no', $search['out_trade_no'])->first();
        }
        if(isset($search['batch'])){
            $order = $sql->where('o.batch', $search['batch'])->get();
        }
        if(isset($search['orders_id'])){
            $order = $sql->where('o.id', $search['orders_id'])->first();
        }

        return $order;
    }

    /**
     * @param $userId
     * @param $goodsId
     * @param $quantity
     * @param $skuIds
     * @return string
     */
    public function cart($userId, $goodsId, $quantity, $skuIds, $seatId=null, $type = 1){
        $goodsModel         = new Goods();
        $goodsSkuModel      = new GoodsSku();
        $goodsSkuValueModel = new GoodsSkuValue();
        $cartModel          = new Carts();

        //判断商品是否存在
        $goods = DB::table($goodsModel->getTable())
            ->where('id', $goodsId)
            ->whereNull('delete_time')
            ->first();
        if(!$goods){
            \Log::error("添加购物车---商品{$goodsId}不存在或软删除了");
            return -2;
        }

        $sku = null;
        $skuValueText = [];
        //如果商品启用了sku
        if($goods->enable_sku){

            if (!isset($skuIds)){
                return -3;
            }

            $skuValue = DB::table($goodsSkuValueModel->getTable())
                ->whereIn('id', $skuIds)
                ->get();


            foreach ($skuValue as $sv) {
                $skuValueText[] = $sv->value;
            }

            $sku = implode(',', array_sort($skuIds));
            //判断sku是否存在
            $skuData = DB::table($goodsSkuModel->getTable())
                ->where('goods_id', $goodsId)
                ->where('value_path', $sku)
                ->first();

            if (!$skuData) {
                \Log::error("添加购物车---商品{$goodsId}的规格{$sku}不存在");
                return -1;
            }
        }

        $storeId = $goods->stores_id;



        //添加购物车
        $cart = DB::table($cartModel->getTable())
            ->where('users_id', $userId)
            ->where('goods_id', $goodsId)
            ->where('store_id', $storeId)
            ->where('seat_id', $seatId)
            ->where('sku_path', $sku)
            ->where('type', $type)
            ->first();

        if($cart){
            if($quantity == 0){
                if (!DB::table($cartModel->getTable())
                    ->where('users_id', $userId)
                    ->where('store_id', $storeId)
                    ->where('sku_path', $sku)
                    ->where('seat_id', $seatId)
                    ->where('goods_id', $goodsId)
                    ->where('type', $type)
                    ->delete()){
                    return false;
                }
            }else{

                if(DB::table($cartModel->getTable())
                        ->where('users_id', $userId)
                        ->where('store_id', $storeId)
                        ->where('sku_path', $sku)
                        ->where('seat_id', $seatId)
                        ->where('type', $type)
                        ->where('goods_id', $goodsId)
                        ->update([
                            'quantity' => $quantity
                        ]) === false
                ){
                    return false;
                }
            }


        }else{
            if ($quantity != 0){
                if(DB::table($cartModel->getTable())
                        ->insert([
                            'company_id'    => $goods->company_id,
                            'store_id'      => $storeId,
                            'goods_id'      => $goodsId,
                            'users_id'      => $userId,
                            'quantity'      => $quantity,
                            'seat_id'       => $seatId,
                            'sku_path'      => $sku,
                            'sku_value'     => implode(';', $skuValueText),
                            'type'          => $type
                        ]) === false
                ){
                    return false;
                }
            }

        }


        //计算购物车最终的价格
        $cartData = DB::table($cartModel->getTable() . ' as c')
            ->select(
                'g.id as  goods_id',
                'g.stores_id',
                'c.quantity',
                'c.sku_value',
                'g.selling_price as g_selling_price',
                'g.purchase_price as g_purchase_price',
                'g.original_price as g_original_price',
                'g.stock',
                'g.thumbnail',
                'g.enable_sku',
                'sku.selling_price as sku_selling_price',
                'sku.purchase_price as sku_purchase_price',
                'sku.original_price as sku_original_price',
                'sku.thumbnail as sku_thumbnail',
                'sku.stock as sku_stock'
            )
            ->leftJoin($goodsModel->getTable() . ' as g', 'c.goods_id', '=', 'g.id')
            ->leftJoin($goodsSkuModel->getTable() . ' as sku', 'c.sku_path', '=', 'sku.value_path')
            ->where('c.users_id', $userId)
            ->where('c.store_id', $storeId)
            ->where('type', $type)
            ->get();

        $storeGoodsData = new \stdClass();
        $storeGoodsData->stores_id = $storeId;
        $storeGoodsData->goods = $cartData;

        $data = $this->activityDiscount([$storeGoodsData]);

        return $data[0];
    }

    /**
     * 立即购买提交
     * @param $userId
     * @param $goodsId
     * @param $quantity
     * @param $skuIds
     * @return string
     */
    public function countGoodgs($goodsId, $quantity, $skuIds, $userId, $couponId){
        $goodsModel         = new Goods();
        $goodsSkuModel      = new GoodsSku();
        $goodsSkuValueModel = new GoodsSkuValue();
        $storeModel         = new Stores();
        $storeInfoModel     = new StoreInfo();
        $storeCouponModel   = new StoreCoupons();
        $userCouponModel    = new UserCoupons();

        //判断商品是否存在
        $goods = DB::table($goodsModel->getTable())
            ->where('id', $goodsId)
            ->whereNull('delete_time')
            ->first();

        if(!$goods){
            \Log::error("立即购买---商品{$goodsId}不存在或软删除了");
            return -2;
        }


        $sku = null;
        $skuValueText = [];
        //如果商品启用了sku
        $skuData = '';
        if($goods->enable_sku){
            $skuValue = DB::table($goodsSkuValueModel->getTable())
                ->whereIn('id', $skuIds)
                ->get();

            foreach ($skuValue as $sv) {
                $skuValueText[] = $sv->value;
            }

            $sku = implode(',', array_sort($skuIds));
            //判断sku是否存在
            $skuData = DB::table($goodsSkuModel->getTable())
                ->where('goods_id', $goodsId)
                ->where('value_path', $sku)
                ->first();

            if (!$skuData) {
                \Log::error("添加购物车---商品{$goodsId}的规格{$sku}不存在");
                return -1;
            }
        }

        $storeId = $goods->stores_id;

        //计算最终的价格
        $cartDataSql = DB::table($goodsModel->getTable() . ' as g')
            ->select(
                'g.id as  goods_id',
                'g.name',
                'g.stores_id',
                'g.selling_price as g_selling_price',
                'g.purchase_price as g_purchase_price',
                'g.original_price as g_original_price',
                'g.stock',
                'g.thumbnail',
                'g.enable_sku',
                'sku.selling_price as sku_selling_price',
                'sku.purchase_price as sku_purchase_price',
                'sku.original_price as sku_original_price',
                'sku.thumbnail as sku_thumbnail',
                'sku.stock as sku_stock',
                'sku.value_path as sku'
            )
            ->leftJoin($goodsSkuModel->getTable() . ' as sku', 'sku.goods_id', '=', 'g.id')
            ->where('g.id', $goodsId);

        if ($sku){
            $cartDataSql->where('sku.value_path', $sku);
        }

        $cartData = $cartDataSql->first();




        $cartData->quantity = intval($quantity);

        $cartData->sku_value = implode(';', $skuValueText);

        $storeInfo = DB::table($storeModel->getTable().' as s')
            ->select(
                's.id',
                's.name',
                's.delivery_fee',
                'si.address'
            )
            ->leftJoin($storeInfoModel->getTable().' as si','s.id','=','si.store_id')
            ->where('s.id', $storeId)->first();
        $storeGoodsData = new \stdClass();
        $storeGoodsData->goods          = [$cartData];
        $storeGoodsData->user_id        = $userId;
        $storeGoodsData->stores_id      = $storeId;
        $storeGoodsData->store_name     = $storeInfo->name;
        $storeGoodsData->delivery_fee   = $storeInfo->delivery_fee;
        $storeGoodsData->store_address  = [[
            'store_id'  => $storeInfo->id,
            'address'  => $storeInfo->address,
            'store_name'  => $storeInfo->name
        ]];
        if ($skuData){
            $storeGoodsData->enable_sku = 1;
        }else{
            $storeGoodsData->enable_sku = 0;
        }

        $data = $this->activityDiscount([$storeGoodsData]);

        /**
         * 优惠券价格计算
         */
        if (!empty($couponId)){
            $coupon = DB::table($userCouponModel->getTable().' as uc')
                ->select('uc.end_time', 'sc.*')
                ->leftJoin($storeCouponModel->getTable().' as sc', 'sc.id', '=', 'uc.coupon_id')
                ->where('uc.coupon_id', $couponId)
                ->where('uc.users_id', $userId)
                ->where('uc.is_use', 0)
                ->where('uc.end_time', '>', date('Y-m-d H:i:s', time()))
                ->whereNull('uc.delete_time')
                ->first();
            if (!$coupon){
                return -3;
            }

            if ($coupon->end_time < date('Y-m-d H:i:s', time())){
                return -4;
            }

            if ($coupon->full_amount > $data[0]->full_amount){
                return -5;
            }

            if ($coupon->belong == 1 && $coupon->scope == '新人首单红包'){
                $orderModel             = new Orders();
                $order = DB::table($orderModel->getTable())
                    ->where('users_id', $userId)
                    ->whereIn('status', ['1', '2', '3', '4', '6'])
                    ->first();
                if ($order){
                    return -6;
                }
            }

            if ($coupon->type === 0){
                $discountAmount = $coupon->discount_amount;
                $payAmount = $data[0]->pay_amount - $discountAmount;
                $data[0]->pay_amount = $payAmount < 0 ? 0 : $payAmount;
                $data[0]->discount_amount += $discountAmount;
                $data[0]->useFRDiscountAmount += $discountAmount;
            }else{
                $discountAmount = (1 - ($coupon->discount_amount/100)) * $data[0]->full_amount;
                $payAmount = $data[0]->pay_amount - $discountAmount;
                $data[0]->pay_amount = $payAmount < 0 ? 0 : $payAmount;
                $data[0]->discount_amount += $discountAmount;
                $data[0]->useFRDiscountAmount += $discountAmount;
            }
            $coupon->full_amount = priceIntToFloat($coupon->full_amount);
            $coupon->discount_amount = priceIntToFloat($coupon->discount_amount);
            $data[0]->coupon = $coupon;
        }

        return $data[0];
//        return $this->orderCountGoods($data);
    }

    public function activityDiscount(array $data){
        $storeIds           = [];
        $spikeModel         = new Spike();
        $spikeAmount        = [];
        $discountAmount     = [];
        $fullAmountPrice    = [];


        foreach ($data as $key => $d){
            $storeIds[]         = $d->stores_id;
            $d->full_amount     = 0;
            $d->pay_amount      = 0;
            $d->goods_sum       = 0;
            $d->goods_number    = 0;
            $d->spike_price     = 0;
            foreach ($d->goods as $g){
                $d->goods_sum += $g->quantity;
                $d->goods_number++;
                /**
                 * 将设置秒杀的商品和没有设置秒杀的商品分离
                 * 满足设置秒杀的时间条件，跳出本次循环进入下一次循环
                 */
                $time   = intval(date("H", time()));
                $time   = intval(floor($time/2));
                $time   *=2;
                $spike = DB::table($spikeModel->getTable())
                    ->where('goods_id', $g->goods_id)
                    ->where('day', date('Y-m-d', time()))
                    ->where('hour', $time.':00')
                    ->whereNull('delete_time')
                    ->first();
                if ($spike){
                    $spikeAmount[$key] = 0;
                    $spikeAmount[$key] += $spike->selling_price * $g->quantity;

                    $d->spike_price += $spike->selling_price * $g->quantity;

                    /**
                     * 计算秒杀前的商品原价和秒杀后的减免价格
                     */
                    if ($g->enable_sku == 1){
                        $g->selling_price   = $g->sku_selling_price;
                        $g->purchase_price  = $g->sku_purchase_price;
                        $g->original_price  = $g->sku_original_price;
                        if(!empty($g->sku_thumbnail)){
                            $g->thumbnail       = $g->sku_thumbnail;
                        }
                        $g->stock           = $g->sku_stock;
                        $fullAmountPrice[$key]  = ($g->sku_selling_price * $g->quantity);
                        $discountAmount[$key]   = ($g->sku_selling_price * $g->quantity)
                                                -
                                                ($spike->selling_price * $g->quantity);
                    }else{
                        $g->selling_price   = $g->g_selling_price;
                        $g->purchase_price  = $g->g_purchase_price;
                        $g->original_price  = $g->g_original_price;
                        $fullAmountPrice[$key]  = ($g->g_selling_price  * $g->quantity);
                        $discountAmount[$key]   = ($g->g_selling_price * $g->quantity)
                                                -
                                                ($spike->selling_price * $g->quantity);
                    }

                    continue;
                }


                /**
                 * 如果需要排除折扣商品，可以在这里添加
                 */
                //如果启用规格，就必须使用规格的价格
                if($g->enable_sku === 1){
                    $d->full_amount     += $g->sku_selling_price * $g->quantity;
                    $d->pay_amount      += $g->sku_selling_price * $g->quantity;
                    $g->selling_price   = $g->sku_selling_price;
                    $g->purchase_price  = $g->sku_purchase_price;
                    $g->original_price  = $g->sku_original_price;
                    if(!empty($g->sku_thumbnail)){
                        $g->thumbnail       = $g->sku_thumbnail;
                    }
                    $g->stock           = $g->sku_stock;
                }else{
                    $d->full_amount     += $g->g_selling_price * $g->quantity;
                    $d->pay_amount      += $g->g_selling_price * $g->quantity;
                    $g->selling_price   = $g->g_selling_price;
                    $g->purchase_price  = $g->g_purchase_price;
                    $g->original_price  = $g->g_original_price;
                }
            }

        }


        //获取店铺优惠
        $storeActivityModel = new StoreActivity();
        $storeActivity = DB::table($storeActivityModel->getTable())
            ->select(
                'id',
                'stores_id',
                'type',
                'data'
            )
            ->whereIn('stores_id', $storeIds)
            ->whereNull('delete_time')
            ->get();


        //活动金额
        foreach ($data as $key => $d) {

            $d->useFRDiscountAmount = 0;
            $d->discount_amount = 0;
            $d->activity = null;
            foreach ($storeActivity as $sa) {
                if($sa->stores_id === $d->stores_id) {
                    $sa->data = json_decode($sa->data);
                    /**
                     * 如果满足满减条件
                     * 并且减少的金额比上一个多
                     */
                    if ($sa->type === 'full_reduction') {
                        if ($d->full_amount >= $sa->data->full_amount && $sa->data->discount_amount > $d->useFRDiscountAmount) {
                            if ($sa->data->discount_amount > $d->pay_amount){
                                $d->useFRDiscountAmount = $d->pay_amount;
                                $d->discount_amount = $d->pay_amount;
                            }else{
                                $d->useFRDiscountAmount = $sa->data->discount_amount;
                                $d->discount_amount = $sa->data->discount_amount;
                            }

                            $d->activity = $sa;
                        }

                    }

                }
            }

            /**
             * 支付价格减去优惠价格，加上没有算入优惠的秒杀价格和秒杀减少的价格
             */
            $payAmount = $d->pay_amount - $d->useFRDiscountAmount;
            $d->pay_amount = $payAmount < 0 ? 0 : $payAmount;
            if (isset($fullAmountPrice[$key])){
                $d->full_amount += $fullAmountPrice[$key];
            }
            if (isset($spikeAmount[$key])){
                $d->pay_amount += $spikeAmount[$key];
            }
            if (isset($discountAmount[$key])){
                $d->discount_amount += $discountAmount[$key];
            }



        }

        return $data;
    }

    public function orderCountGoods($data){

        $orderData['batch']             = $data[0]->user_id.(microtime(true) * 10000).mt_rand(1000, 9999);
        $orderData['company_id']        = $data[0]->company_id;
        $orderData['users_id']          = $data[0]->user_id;
        $orderData['stores_id']         = $data[0]->stores_id;
        $orderData['type']              = $data[0]->type;
        $orderData['goods_price']       = $data[0]->full_amount;
        $orderData['pay_amount']        = $data[0]->pay_amount;
        $orderData['discount_amount']   = $data[0]->full_amount - $data[0]->pay_amount;
        $orderData['status']            = 0;
        $orderData['create_time']       = date('Y-m-d H:i:s', time());
        $orderData['order_sn']          = $data[0]->stores_id.(microtime(true) * 10000).$data[0]->user_id;
        $orderData['remark']            = $data[0]->remark;


        $orderModel         = new Orders();
        $orderGoods         = new OrderGoods();
        $orderAddressModel  = new OrderAddress();
        $userAddressModel   = new UserAddress();
        $skuValueModel      = new GoodsSkuValue();
        try{
            DB::beginTransaction();

            $order_id = DB::table($orderModel->getTable())->insertGetId($orderData);
            if (!$order_id){
                DB::rollBack();
                return false;
            }

            $goods['orders_id']         = $order_id;
            $goods['goods_id']          = $data[0]->goods[0]->goods_id;
            $goods['name']              = $data[0]->goods[0]->name;
            $goods['thumbnail']         = $data[0]->goods[0]->thumbnail;
            $goods['selling_price']     = $data[0]->pay_amount;
            $goods['quantity']          = $data[0]->goods[0]->quantity;
            $goods['sku']               = '';
            $sku = DB::table($skuValueModel->getTable())
                ->select('value')
                ->whereIn('id', explode(',', $data[0]->goods[0]->sku))
                ->where('goods_id', $data[0]->goods[0]->goods_id)
                ->get();

            foreach ($sku as $item) {
                $goods['sku'] .= $item->value.';';
            }
            $goods['sku'] = substr($goods['sku'],0,strlen($goods['sku'])-1);

            if( !DB::table($orderGoods->getTable())->insert($goods) ){
                DB::rollBack();
                return false;
            }

            $address = DB::table($userAddressModel->getTable())
                ->select(
                    'consignee',
                    'contact',
                    'province',
                    'city',
                    'district',
                    'address',
                    'longitude',
                    'latitude'
                )
                ->where('id', $data[0]->address)->first();
            $address->batch = $orderData['batch'];
            $address = json_decode(json_encode($address) ,true);

            if( !DB::table($orderAddressModel->getTable())->insert($address) ){
                DB::rollBack();
                return false;
            }

            DB::commit();
            return $order_id;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error('立即购买失败，原因：'.$exception);
            return false;
        }
    }

    /**
     * 更新订单优惠券
     * @param $orderId
     * @param $userId
     * @param $couponId
     * @return bool|float|int|mixed
     * @throws \Exception
     */
    public function updateOrderCoupon($orderId,$userId, $couponId, $batch){
        $userCouponModel    = new UserCoupons();
        $storeCouponModel   = new StoreCoupons();
        $orderCouponModel   = new OrderCoupons();
        $orderModel         = new Orders();

        $coupon = DB::table($userCouponModel->getTable() . ' as uc')
            ->select('sc.*', 'uc.end_time', 'uc.start_time')
            ->leftJoin($storeCouponModel->getTable() . ' as sc', 'uc.coupon_id', '=', 'sc.id')
            ->where('sc.id', $couponId)
            ->where('uc.users_id', $userId)
            ->where('uc.is_use', 0)
            ->whereNull('sc.delete_time')
            ->first();

        if(!$coupon){
            //优惠券不存在
            return -1;
        }

        if ($coupon->end_time < date('Y-m-d H:i:s', time())){
            return -2;
        }



        $order = DB::table($orderModel->getTable() . ' as o')
            ->select(
                'o.goods_price',
                'o.pay_amount',
                'oc.amount as discount_amount'
            )
            ->leftJoin($orderCouponModel->getTable() . ' as oc', 'oc.orders_id', '=', 'o.id')
            ->where('o.id', $orderId)
            ->first();

        if ($order->pay_amount + $order->discount_amount < $coupon->full_amount){
            return -3;
        }

        $discountAmount = 0;
        if($coupon->type === 0){
            $discountAmount = $coupon->discount_amount;
        }else if($coupon->type === 1){
            //满折券
            $discountAmount = ($order->pay_amount + $order->discount_amount) * (1-($coupon->discount_amount/100));
//            $discountAmount = $coupon->discount_amount * $order->goods_price / 100;
        }

        $payAmount = $order->pay_amount + $order->discount_amount - $discountAmount;

        $payAmount = $payAmount > 0 ? $payAmount : 0;

        try{
            DB::beginTransaction();
            if(
                DB::table($orderModel->getTable())
                ->where('id', $orderId)
                ->update([
                    'pay_amount' => $payAmount
                ]) === false
            ){
                DB::rollBack();
                \Log::error("更新订单优惠券失败");
                return false;
            }

            $orderCouponData = [
                'orders_id' => $orderId,
                'coupon_id' => $coupon->id,
                'amount'    => $discountAmount,
                'start_time' => $coupon->create_time,
                'end_time' => $coupon->end_time
            ];

            if(
                DB::table($orderCouponModel->getTable())
                ->where('orders_id', $orderId)
                ->first()
            ) {
                if (
                    DB::table($orderCouponModel->getTable())
                        ->where('orders_id', $orderId)
                        ->update($orderCouponData) === false
                ) {
                    DB::rollBack();
                    \Log::error("初始化订单----修改订单优惠券失败:" . json_encode($orderCouponData) . "");
                    return false;
                }
            }else{
                if (
                    !DB::table($orderCouponModel->getTable())
                        ->insert($orderCouponData)
                ) {
                    DB::rollBack();
                    \Log::error("初始化订单----添加订单优惠券失败:" . json_encode($orderCouponData) . "");
                    return false;
                }
            }

            DB::commit();
            if (!empty($batch)){
                return $this->orderByBatch($batch);
            }else{
                return $this->orderList(['order_id' => $orderId], 1, 1)[0];
            }
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }

    /**
     * 更新订单备注
     * @param $orderId
     * @param $remark
     * @return int
     */
    public function updateOrderRemark($orderId, $remark){
        $orderModel = new Orders();
        return DB::table($orderModel->getTable())
            ->where('id', $orderId)
            ->update(['remark', $remark]);
    }

    /**
     * 订单地址信息
     * @param $orderId
     * @param $remark
     * @return int
     */
    public function getOrderAddress($batch){
        $orerAddressModel   = new OrderAddress();
        $districtModel      = new District();
        $orderModel         = new Orders();
        return DB::table($orerAddressModel->getTable() . ' as ua')
            ->select(
                'ua.*',
                'pd.name as province',
                'cd.name as city',
                'dd.name as district',
                'o.food_delivery_time'
            )
            ->leftJoin($districtModel->getTable() . ' as pd', 'pd.id', '=', 'ua.province')
            ->leftJoin($districtModel->getTable() . ' as cd', 'cd.id', '=', 'ua.city')
            ->leftJoin($districtModel->getTable() . ' as dd', 'dd.id', '=', 'ua.district')
            ->leftJoin($orderModel->getTable() . ' as o', 'o.batch', '=', 'ua.batch')
            ->where('ua.batch', $batch)
            ->orderBy('o.batch')
            ->first();
    }

    /**
     * 更新订单地址
     * @param $batch
     * @param $userId
     * @param $addressId
     * @return bool|int
     */
    public function updateOrderAddress($batch, $userId, $addressId){
        $userAddressModel = new UserAddress();
        $orderAddressModel = new OrderAddress();
        $userAddress = DB::table($userAddressModel->getTable())
            ->where('users_id', $userId)
            ->where('id', $addressId)
            ->whereNull('delete_time')
            ->first();

        if (!$userAddress) {
            //地址不存在
            return -1;
        }

        $orderAddress = [
            'batch'         => $batch,
            'consignee'     => $userAddress->consignee,
            'contact'       => $userAddress->contact,
            'province'      => $userAddress->province,
            'city'          => $userAddress->city,
            'district'      => $userAddress->district,
            'address'       => $userAddress->address,
            'longitude'     => $userAddress->longitude,
            'latitude'      => $userAddress->latitude
        ];

        if(
            DB::table($orderAddressModel->getTable())
            ->where('batch', $batch)
            ->first()
        ) {
            if (
                DB::table($orderAddressModel->getTable())
                    ->where('batch', $batch)
                    ->update($orderAddress)
                === false
            ) {
                \Log::error("插入订单收货地址失败:" . json_encode($orderAddress) . "");
                return false;
            }
        }else{
            if (
                DB::table($orderAddressModel->getTable())
                    ->insert($orderAddress)
                === false
            ) {
                \Log::error("插入订单收货地址失败:" . json_encode($orderAddress) . "");
                return false;
            }
        }

        return true;
    }

    private function _orderSql($search = []){
        $orderModel         = new Orders();
        $orderAddressModel  = new OrderAddress();
        $storeModel         = new Stores();
        $orderCouponModel   = new OrderCoupons();
        $storeCouponModel   = new StoreCoupons();
        $storeSeatModel     = new StoreSeat();
        $storeInfoModel     = new StoreInfo();

        $prefix = env('DB_PREFIX');
        $sql = DB::table($orderModel->getTable() . ' as o')
            ->select(
                'o.id',
                'o.batch',
                'o.company_id',
                'o.users_id',
                'o.stores_id',
                'o.order_sn',
                'o.create_time',
                DB::raw("convert({$prefix}o.goods_price/100, decimal(15,2)) as goods_price"),
                DB::raw("convert({$prefix}o.pay_amount/100, decimal(15,2)) as pay_amount"),
                'o.pay_time',
                'o.pay_type',
                'o.type',
                'o.status',
                'o.remark',
                'o.food_delivery_time',
                'o.delivery_time',
                'o.code',
                'o.qrcode',
                'o.express',
                'o.expresssn',
                'o.create_time',
                'o.pay_time',
                's.name as store_name',
                's.logo as store_logo',
                's.store_type_id',
                's.mobile as store_mobile',
                DB::raw("convert({$prefix}s.delivery_fee/100, decimal(15,2)) as delivery_fee"),
                'sc.id as coupon_id',
                'sc.name as coupon_name',
                DB::raw("convert({$prefix}sc.discount_amount/100, decimal(15,2)) as coupon_discount"),
                'ss.seat_number',
                'si.longitude',
                'si.latitude',
                'si.address as store_address',
                DB::raw("convert({$prefix}sc.discount_amount/100, decimal(15,2)) as discount_amount")
            )
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'o.stores_id')
            ->leftJoin($storeInfoModel->getTable().' as si', 'si.store_id', '=', 'o.stores_id')
            ->leftJoin($orderAddressModel->getTable() . ' as oa', 'oa.batch', '=', 'o.batch')
            ->leftJoin($orderCouponModel->getTable() . ' as oc', 'oc.orders_id', '=', 'o.id')
            ->leftJoin($storeCouponModel->getTable() . ' as sc', 'sc.id', '=', 'oc.coupon_id')
            ->leftJoin($storeSeatModel->getTable() . ' as ss', 'ss.id', '=', 'o.seat_id');

        if(isset($search['users_id'])){
            $sql->where('o.users_id', $search['users_id']);
        }
        if(isset($search['order_id'])){
            $sql->where('o.id', $search['order_id']);
        }
        if(isset($search['stores_id'])){
            $sql->where('o.stores_id', $search['stores_id']);
        }
        if(isset($search['batch'])){
            $sql->where('o.batch', $search['batch']);
        }
        if(isset($search['status'])){
            $sql->where('o.status', $search['status']);
        }
        if(isset($search['store_type'])){
            $sql->whereIn('s.store_type_id', $search['store_type']);
        }
        if (isset($search['online']) && !empty($search['online'])){
            $sql->where('o.is_online', $search['online']);
        }

        return $sql;
    }


    public function orderList($search = [], $page = 1, $pageSize = 20){
        $orderGoodsModel    = new OrderGoods();
        $orderActivityModel = new OrderActivity();
        $storeActivityModel = new StoreActivity();


        $sql = $this->_orderSql($search);

        $order = $sql->whereNull('o.delete_time')
            ->orderBy('o.create_time', 'desc')
            ->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();



        $orderIds = [];
        foreach ($order as $o){
            $orderIds[] = $o->id;
        }


        //组合商品
        if (!empty($orderIds)){
            $goods = DB::table($orderGoodsModel->getTable())
                ->select(
                    'id',
                    'orders_id',
                    'goods_id',
                    'name',
                    'thumbnail',
                    DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                    'quantity',
                    'sku'
                )
                ->whereIn('orders_id', $orderIds)
                ->get();

            $i = 0;

            foreach ($order as $k=>$o){
                $o->self_mention_code = $o->code;
                $o->self_mention_time = date('H:i:s', strtotime($o->pay_time));
                $o->self_mention_address = $o->store_address;

                $o->goods = [];
                $o->goods_sum = 0;
                $o->goods_num = 0;
                foreach ($goods as $g){
                    if($g->orders_id === $o->id){
                        $o->goods[] = $g;
                        $o->goods_sum += $g->quantity;
                        $o->goods_num ++;
                    }
                }

                if(empty($o->goods)){
                    unset($order[$i]);
                }
                $i++;
            }


            //订单参与的活动
            $activity = DB::table($orderActivityModel->getTable() . ' as oa')
                ->select('sa.*', 'oa.orders_id')
                ->leftJoin($storeActivityModel->getTable() . ' as sa', 'sa.id', '=', 'oa.store_activity_id')
                ->whereIn('oa.orders_id', $orderIds)
                ->get();

            $activityConfig = \Config::get('activity');
            foreach ($order as $o){
                $o->activity = new \StdClass();
                $o->orderInfo = [];
                if (!$activity->isEmpty()){
                    foreach ($activity as $a){
                        if($a->orders_id === $o->id){
                            $a->name = '活动';
                            if(isset($activityConfig[$a->type])){
                                $a->name = $activityConfig[$a->type]['name'];
                            }
                            $a->data = json_decode($a->data);
                            $a->data->full_amount = priceIntToFloat($a->data->full_amount);
                            $a->data->discount_amount = priceIntToFloat($a->data->discount_amount);
                            $o->activity = $a;
                        }
                    }
                    $o->activity->delivery_fee = $o->delivery_fee;
                    $o->activity->discount_amount = empty($o->discount_amount) ? priceIntToFloat(0) : $o->discount_amount;
                    $o->activity->pay_amount  = $o->pay_amount;
                    $o->orderInfo['order_sn'] = $o->order_sn;
                }


            }
        }



        return $order;
    }

    public function orderUseCoupons($storeId, $usersId, $couponsId){
        $storeCouponsModel      = new StoreCoupons();
        $userCouponsModel       = new UserCoupons();

        $search = [
            'store_id' => $storeId,
            'users_id' => $usersId
        ];

        $cart = $this->cartList($search);

        if (empty($couponsId)){
            return $cart;
        }
        $userCoupon = DB::table($userCouponsModel->getTable())
            ->where('users_id', $usersId)
            ->where('coupon_id', $couponsId)
            ->first();
        if (!$userCoupon){
            return -3;
        }
        /**
         * 优惠券信息
         */
        $couponsData = DB::table($storeCouponsModel->getTable())->where('id', $couponsId)->first();
        /**
         * 优惠券失效
         */
        if (strtotime($userCoupon->end_time)  < time()){
            return -1;
        }

        /**
         * 当前价格不满足优惠券条件价格
         */
        if ($cart->cart[0]->full_amount*100 < $couponsData->full_amount){
            return -2;
        }

        if ($couponsData->type == 0){
            /**
             * 满减
             */
            $payAmount = $cart->cart[0]->pay_amount*100 - $couponsData->discount_amount;
            $discountAmount = $couponsData->discount_amount;
        }else{
            /**
             * 满折
             */
            $payAmount = $cart->cart[0]->pay_amount*100 * $couponsData->discount_amount/100;
            $discountAmount = $cart->cart[0]->pay_amount*100 - $payAmount;
        }

        $cart->cart[0]->pay_amount = priceIntToFloat($payAmount);
        $cart->cart[0]->useFRDiscountAmount = priceIntToFloat($discountAmount) + $cart->cart[0]->useFRDiscountAmount;
        $cart->cart[0]->discount_amount = priceIntToFloat($discountAmount) +$cart->cart[0]->discount_amount;

        $couponsData->full_amount = priceIntToFloat($couponsData->full_amount);
        $couponsData->discount_amount = priceIntToFloat($couponsData->discount_amount);
        $cart->cart[0]->coupons = $couponsData;

        $cart->total_discount_amount = priceIntToFloat($cart->total_discount_amount*100 + $discountAmount);
        $cart->total_pay_amount = priceIntToFloat($cart->total_pay_amount*100 - $discountAmount);
        return $cart;


    }

    public function ordersRanking($companyId,$storeId){  //商品排行  本周数据
        $res= \Illuminate\Support\Facades\DB::select("
select count(*) as volume,b.name,sum(a.selling_price) as price from jl_order_goods as a
left join jl_goods as b on a.goods_id=b.id
left join jl_orders as c on a.orders_id=c.id
where b.name is not null
  and c.status <> 0
  and c.status <> 5
  and YEARWEEK(date_format(c.create_time,'%Y-%m-%d' ) ) = YEARWEEK( now() )
  and c.company_id={$companyId}
  and c.stores_id={$storeId}
group by a.goods_id
order by  count(*) desc
limit 0,10");

        $user=\Qiniu\json_decode(json_encode($res),true);
        $arr=array();
        foreach ($user as $key=>$v){
            $arr[$key+1]=$v;
        }
        return $arr;
    }

    /**
     * 确认订单
     * @param $user_id
     * @param $store_id
     * @param $coupon_id
     * @param $long
     * @param $lat
     * @return array|int
     */
    public function orderEnter($user_id, $store_id, $coupon_id, $long, $lat){
        $storeModel         = new Stores();
        $cartsModel         = new Carts();
        $goodsModel         = new Goods();
        $goodsSkuModel      = new GoodsSku();
        $userCouponModel    = new UserCoupons();
        $couponModel        = new StoreCoupons();
        $storeInfoModel     = new StoreInfo();

        $distance = DB::table($storeInfoModel->getTable())
            ->select(
                DB::raw("SQRT(
                    POW(111.2 * (latitude - {$lat}), 2) 
                    +
                    POW(111.2 * ({$long} - longitude) * COS(latitude / 57.3), 2))
                    
                    as distance
                ")
            )->where('store_id', $store_id)
            ->value('distance');


        if ($distance > 10){
            return -4;
        }

        if (!empty($coupon_id)){
            $userCoupon = DB::table($userCouponModel->getTable())
                ->where('users_id', $user_id)
                ->where('coupon_id', $coupon_id)
                ->where('is_use', 0)
                ->first();
            if (!$userCoupon){
                return -1;
            }

            $couponInfo = DB::table($couponModel->getTable().' as c')
                ->select('c.*', 'uc.end_time')
                ->leftJoin($userCouponModel->getTable().' as uc', 'uc.coupon_id', '=', 'c.id')
                ->where('uc.id', $coupon_id)
                ->where('c.stores_id', $store_id)
                ->whereNull('uc.delete_time')
                ->first();

            if (!$couponInfo){
                return -2;
            }

            if ($couponInfo->end_time < date('Y-m-d H:i:s', time())){
                return -3;
            }


        }




        $store = DB::table($storeModel->getTable())
            ->select(
                DB::raw("convert(delivery_fee/100, decimal(15,2)) as delivery_fee"),
                'name'
            )
            ->where('id', $store_id)->first();

        $goods = DB::table($cartsModel->getTable().' as c')
            ->select(
                'c.*',
                'g.name',
                'g.thumbnail',
                'gk.selling_price as sku_price',
                'g.selling_price as goods_price'
            )
            ->leftJoin($goodsModel->getTable().' as g','c.goods_id','=','g.id')
            ->leftJoin($goodsSkuModel->getTable().' as gk','c.sku_path','=','gk.value_path')
            ->where('store_id', $store_id)
            ->where('users_id', $user_id)
            ->get();

        $count_price = 0;
        foreach ($goods as $value) {

            if (!empty($value->sku_price)){
                $count_price += $value->sku_price * $value->quantity;
            }else{
                $count_price += $value->goods_price * $value->quantity;
            }

            $value->price = empty($value->sku_price)
                ? priceIntToFloat($value->goods_price) : priceIntToFloat($value->sku_price);
            unset($value->goods_price);
            unset($value->sku_price);
        }

        if (isset($couponInfo) && $couponInfo->full_amount <= $count_price){
            if ($couponInfo->type == 0){
                $count_price -= $couponInfo->discount_amount;
            }else{
                $count_price -= $count_price * ($couponInfo->discount_amount/100);
            }

            $couponInfo->discount_amount = priceIntToFloat($couponInfo->discount_amount);
            $couponInfo->full_amount     = priceIntToFloat($couponInfo->full_amount);
            $couponData = $couponInfo;
        }

        return [
            'delivery_fee'  => $store->delivery_fee,
            'store_name'    => $store->name,
            'count_price'   => priceIntToFloat($count_price),
            'goods'         => $goods,
            'coupon'        => isset($couponData) ? $couponData : []
        ];
    }


    public function orderDeliveryUpdate($ordersId, $batch, $type, $remark){
        $orderModel = new Orders();
        $sql = DB::table($orderModel->getTable())
            ->where('batch', $batch);

        if (isset($ordersId)){
            $sql->where('id', $ordersId);
        }

        if (isset($batch)){
            $sql->where('batch', $batch);
        }
        return $sql->update(['type' => $type, 'remark' => $remark]);
    }

    /**
     * 重新下单
     * @param $orderId
     * @param $userId
     * @param $companyId
     * @return bool|string
     */
    public function reorder($orderId, $userId, $companyId, $online){
        $cartModel           = new Carts();
        $orderModel          = new Orders();
        $orderGoodsModel     = new OrderGoods();
        $userAddressModel    = new UserAddress();

        $orderType  = DB::table($orderModel->getTable())->where('id', $orderId)->value('type');
        $orderGoods = DB::table($orderGoodsModel->getTable().' as og')
            ->select(
                'o.stores_id as store_id',
                'og.goods_id',
                'og.quantity',
                'og.sku_path',
                'og.sku as sku_value'
            )
            ->leftJoin($orderModel->getTable(). ' as o','o.id','=','og.orders_id')
            ->where('orders_id', $orderId)
            ->get();


        try{
            DB::beginTransaction();
            $cartData   = [];
            $cartIdData = [];
            foreach ($orderGoods as $key => $value) {
                $cartData[$key]['company_id']       = $companyId;
                $cartData[$key]['store_id']         = $value->store_id;
                $cartData[$key]['goods_id']         = $value->goods_id;
                $cartData[$key]['sku_path']         = $value->sku_path;
                $cartData[$key]['sku_value']        = $value->sku_value;
                $cartData[$key]['quantity']         = $value->quantity;
                $cartData[$key]['users_id']         = $userId;
                $cartIdData[$key]                    = new \StdClass();
                $cartIdData[$key]->quantity          = $value->quantity;
                $cartIdData[$key]->goods_id          = $value->goods_id;

                $cartId = DB::table($cartModel->getTable())->insertGetId($cartData[$key]);
                if($cartId === false){
                    DB::rollBack();
                    return false;
                }else{
                    $cartIdData[$key]->cart_id      = $cartId;
                    $cartIdData[$key]->quantity     = $value->quantity;
                }
            }

            $addressId = DB::table($userAddressModel->getTable())
                ->where('users_id', $userId)
                ->where('default', 1)
                ->value('id');


            $batch = $this->initOrder(
                $userId,
                $companyId,
                $cartIdData,
                $orderType,
                null,
                $addressId,
                $online
            );
            if (!$batch){
                \Log::debug('重新下单失败');
                DB::rollBack();
                return false;
            }


            DB::commit();
            Redis::setex('order-cancel:'.$batch,900,'test');
            return $batch;
        }catch (\Exception $exception){
            \Log::error('重新下单失败'.$exception);
            DB::rollBack();
            return false;
        }



    }

    /**
     *历史购买记录
     * @param $page
     * @param $pageSize
     * @param $search
     * @return mixed
     */
    public function historyBuy($page, $pageSize, $search){
        $sql = $this->_orderSql($search);

        $order = $sql
            ->where('o.status', 3)
            ->orWhere('o.status', 6)
            ->whereNull('o.delete_time')
            ->take(($page - 1) * $pageSize)->take($pageSize)
            ->orderBy('o.create_time', 'desc')
            ->get();

        return $order;
    }

    /**
     * 获取订单信息
     * @param $batch
     * @param $orderId
     * @return mixed
     */
    public function orderInfo($batch, $orderId){
        $orderModel         = new Orders();
        $orderCouponModel   = new OrderCoupons();
        $userCouponModel    = new UserCoupons();
        $storeModel         = new Stores();

        $sql = DB::table($orderModel->getTable().' as o')
            ->select('o.*', 'uc.is_use', 's.store_type_id')
            ->leftJoin($orderCouponModel->getTable().' as oc', 'oc.orders_id', '=', 'o.id')
            ->leftJoin($userCouponModel->getTable().' as uc', 'uc.id','=', 'oc.user_coupon')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=','o.stores_id');

        if (!empty($batch)){
            $sql->where('o.batch', $batch);
        }
        if (!empty($orderId)){
            $sql->where('o.id', $orderId);
        }
        return $sql->get()->toArray();
    }

    private function __getZhongXinWechatPayObj(){
        $payObject = new \stdClass();

        $payObject->resHandler = new ClientResponseHandler();
        $payObject->reqHandler = new RequestHandler();
        $payObject->pay = new PayHttpClient();

        $payObject->reqHandler->setGateUrl(\Config::get('zhongxin-pay.mini.url'));

        $sign_type = \Config::get('zhongxin-pay.mini.sign_type');

        if ($sign_type == 'MD5') {
            $payObject->reqHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
            $payObject->resHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
            $payObject->reqHandler->setSignType($sign_type);
        } else if ($sign_type == 'RSA_1_1' || $sign_type == 'RSA_1_256') {
            $payObject->reqHandler->setRSAKey(file_get_contents(\Config::get('zhongxin-pay.mini.private_rsa_key')));
            $payObject->resHandler->setRSAKey(file_get_contents(\Config::get('zhongxin-pay.mini.public_rsa_key')));
            $payObject->reqHandler->setSignType($sign_type);
        }

        return $payObject;
    }


    /**
     * 修改订单状态
     * @param $orderId
     * @param $status
     * @return bool
     */
    public function updateOrderStatus($orderId, $status, $Identity = ''){
        $orderModel         = new Orders();
        $storeModel         = new Stores();
        $orderCouponModel   = new OrderCoupons();
        $storeIncomeModel   = new StoreIncomeLog();
        $goodsModel         = new Goods();
        $orderGoodsModel    = new OrderGoods();
        $storeVolumeModel   = new StoreVolume();
        $turnoverModel      = new Turnover();
        $orderRefundModel   = new OrderRefund();
        $userModel          = new Users();
        $userIntegralModel  = new UserIntegralLog();
        $storeTurnoverModel = new StoreTurnover();

        try{
            DB::beginTransaction();

            $orderInfo = DB::table($orderModel->getTable().' as o')
                ->select('o.*', 's.store_type_id', 'o.pay_type', 'u.balance', 'u.parent_id')
                ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'o.stores_id')
                ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'o.users_id')
                ->where('o.id', $orderId)
                ->first();
            
            if (empty($orderInfo)){
                return -1;
            }

            if ($orderInfo->status == $status){
                return -4;
            }

            if ($orderInfo->store_type_id == 4 && $status == 5 && $Identity == ''){
                return -5;
            }

            //取消订单
            if ($status == 5){

                //不是聚南珍的商户，付款后不能取消订单
                if ($orderInfo->store_type_id != 4 && $orderInfo->status != 0){
                    return -2;
                }

                //聚南珍商家，status = 2 为制作中时才能进行取消
                if ($orderInfo->store_type_id == 4 && $orderInfo->status != 2 && $Identity != 'store'){
                    return -3;
                }

                //聚南珍商家，status = 2 并且是商家操作时才能取消
                if ($orderInfo->store_type_id == 4 && $orderInfo->status == 2 && $Identity == 'store'){
                    $refundTransactionId = md5($orderInfo->batch.time());
                    //微信支付退款
                    if ($orderInfo->pay_type == 1){
                        $refundType         = 1;
                        $weChatPayModel     = new WechatPay();
                        $transactionId      = DB::table($weChatPayModel->getTable())
                            ->where('orders_id', $orderId)
                            ->value('transaction_id');
                        $app                = Factory::payment(\Config::get('zhongxin-pay.app'));
                        $result             = $app->refund->byTransactionId(
                            $transactionId,
                            $refundTransactionId,
                            $orderInfo->pay_amount,
                            $orderInfo->pay_amount, [
                            'refund_desc' => '商家未接单，取消订单，金额退回',
                        ]);

                        if ($result['return_code'] == 'SUCCESS'){
                            if ($result['result_code'] == 'SUCCESS'){
                                $message        = '成功';
                                $refundStatus   = 3;
                            }else{
                                $message        = $result['err_code'].'【'.$result['err_code_des'].'】';
                                $refundStatus   = 5;
                            }

                        }else{
                            $message        = '失败';
                            $refundStatus   = 5;
                        }
                    }
                    //支付宝退款
                    if ($orderInfo->pay_type == 2){
                        $refundType         = 2;
                        $aliPayModel        = new Alipay();
                        $outTradeNo         = DB::table($aliPayModel->getTable())
                            ->where('orders_id', $orderId)
                            ->value('out_trade_no');
                        $result             = Pay::alipay(config('ali-pay.config'))->refund([
                            'out_trade_no'          => $outTradeNo,
                            'refund_amount'         => priceIntToFloat($orderInfo->pay_amount),
                            'refund_reason'         => '商家未接单，取消订单，金额退回'
                        ]);

                        if ($result['msg'] == 'Success' && $result['code'] == 10000){
                            $message        = '成功';
                            $refundStatus   = 3;
                        }else{
                            $message        = '失败';
                            $refundStatus   = 5;
                        }
                    }
                    //余额退款
                    if ($orderInfo->pay_type == 3){
                        $refundType         = 3;
                        $userWalletModel    = new UserWalletLog();

                        if(!DB::table($userWalletModel->getTable())
                            ->insert([
                                'users_id'      => $orderInfo->users_id,
                                'money'         => $orderInfo->pay_amount,
                                'balance'       => $orderInfo->balance,
                                'wallet_type'   => 2,
                                'stores_id'     => $orderInfo->stores_id,
                                'type'          => 5,
                                'remake'        => '商家未接单，取消订单，金额退回'
                            ])
                        ){
                            \Log::debug('商家未接单，取消订单，金额退回失败');
                            DB::rollBack();
                            return false;
                        }

                        if(!DB::table($userModel->getTable().' as u')
                            ->where('id', $orderInfo->users_id)
                            ->increment('balance', $orderInfo->pay_amount)
                        ){
                            DB::rollBack();
                            \Log::debug('退回商户余额失败');
                            return false;
                        }else{
                            $message        = '成功';
                            $refundStatus   = 3;
                        }
                    }

                    if ($orderInfo->pay_type == 4) {
                        $refundType = 4;
                        $weChatPayModel = new WechatPay();
                        $outTradeNo = DB::table($weChatPayModel->getTable())
                            ->where('orders_id', $orderId)
                            ->value('out_trade_no');
                        $pay = $this->__getZhongXinWechatPayObj();
                        $pay->reqHandler->setParameter('service', 'unified.trade.refund');
                        $pay->reqHandler->setParameter('version', \Config::get('zhongxin-pay.mini.version'));
                        $pay->reqHandler->setParameter('sign_type', \Config::get('zhongxin-pay.mini.sign_type'));
                        $pay->reqHandler->setParameter('mch_id', \Config::get('zhongxin-pay.mini.mchId'));
                        $pay->reqHandler->setParameter('out_trade_no', $outTradeNo);
                        $pay->reqHandler->setParameter('out_refund_no', $refundTransactionId);
                        $pay->reqHandler->setParameter('total_fee', 1); //$order->pay_amount
                        $pay->reqHandler->setParameter('refund_fee', 1); //退款金额
                        $pay->reqHandler->setParameter('op_user_id', \Config::get('zhongxin-pay.mini.mchId'));
                        $pay->reqHandler->setParameter('nonce_str', mt_rand());
                        $pay->reqHandler->createSign();

                        $data = Utils::toXml($pay->reqHandler->getAllParameters());
                        $pay->pay->setReqContent($pay->reqHandler->getGateURL(), $data);

                        if ($pay->pay->call()) {
                            $pay->resHandler->setContent($pay->pay->getResContent());
                            $pay->resHandler->setKey($pay->reqHandler->getKey());
                            $res = $pay->resHandler->getAllParameters();

                            \Log::info(date("Y-m-d H:i:s", time()) . '退款XML' . json_encode($res));

                            if ($pay->resHandler->isTenpaySign()) {
                                if ($pay->resHandler->getParameter('result_code') == 0) {
                                    $message = '成功';
                                    $refundStatus = 3;
                                    $payInfo = $pay->resHandler->getParameter('pay_info');
                                    \Log::debug($payInfo);
                                } else {
                                    $message = '失败';
                                    $refundStatus = 5;
                                    \Log::error('支付-生成微信支付配置数据-错误:' . $pay->resHandler->getParameter('message'));
                                }
                            } else {
                                $message = '失败';
                                $refundStatus = 5;
                                \Log::error('支付-生成微信支付配置数据-错误:' . $pay->resHandler->getParameter('message'));
                            }

                        }
                    }

                    $refundOrderGoods = DB::table($orderGoodsModel->getTable())
                        ->where('orders_id', $orderId)
                        ->get()->toArray();

                    $refundData = [];
                    $i = 0;
                    foreach ($refundOrderGoods as $item) {
                        $refundData[$i]['users_id']                 = $orderInfo->users_id;
                        $refundData[$i]['stores_id']                = $orderInfo->stores_id;
                        $refundData[$i]['orders_id']                = $orderId;
                        $refundData[$i]['refund_transaction_id']    = $refundTransactionId;
                        $refundData[$i]['order_goods_num']          = count($refundOrderGoods);
                        $refundData[$i]['order_goods_id']           = $item->id;
                        $refundData[$i]['goods_amount']             = $item->selling_price;
                        $refundData[$i]['goods_sku']                = $item->sku;
                        $refundData[$i]['goods_quantity']           = $item->quantity;
                        $refundData[$i]['pay_amount']               = $orderInfo->pay_amount;
                        $refundData[$i]['refund_amount']            = $orderInfo->pay_amount;
                        $refundData[$i]['cause']                    = '商家未接单，取消订单，金额退回';
                        $refundData[$i]['type']                     = $refundType;
                        $refundData[$i]['message']                  = $message;
                        $refundData[$i]['status']                   = $refundStatus;
                        $i++;
                    }

                    if(!DB::table($orderRefundModel->getTable())
                        ->insert($refundData)
                    ){
                        DB::rollBack();
                        \Log::debug('添加退款信息失败');
                        return false;
                    }
                }

            }

            //确认收货
            if ($status == 3){
                $orderCoupon = DB::table($orderCouponModel->getTable())
                    ->where('orders_id', $orderId)
                    ->first();

                //外卖商户 商户收取60%配送费，用户收取40%配送费
                $money = $orderInfo->pay_amount;
                if ($orderInfo->store_type_id == 1){
                    $money -= $orderInfo->delivery_fee;
                    $money -= $orderInfo->total_delivery_fee * 0.6;
                }

                if ($orderCoupon){
                    if ($orderCoupon->coupon_type == 0){
                        $money += $orderCoupon->amount;
                    }
                }
                //添加商余额
                if(!DB::table($storeModel->getTable())->where('id', $orderInfo->stores_id)
                    ->update(array(
                        'balance' => DB::raw("balance + {$money}"),
                        'turnover'  => DB::raw("turnover + {$money}"),
                        'sales_volume'  => DB::raw("sales_volume + 1")
                    ))
                ){
                    DB::rollBack();
                    \Log::debug('添加商户余额失败');
                    return false;
                }

                //添加商户营业额
                $storeTurnover = DB::table($storeTurnoverModel->getTable())
                    ->where('stores_id', $orderInfo->stores_id)
                    ->where('date', date('Y-m-d', strtotime($orderInfo->pay_time)))
                    ->first();
                if ($storeTurnover){
                    if(!DB::table($storeTurnoverModel->getTable())
                        ->where('id', $storeTurnover->id)
                        ->increment('turnover', $money)
                    ){
                        DB::rollBack();
                        \Log::debug('添加商户每日营业额失败');
                        return false;
                    }
                }else{
                    if(!DB::table($storeTurnoverModel->getTable())
                        ->insert([
                            'stores_id'     => $orderInfo->stores_id,
                            'date'          => date('Y-m-d', strtotime($orderInfo->pay_time)),
                            'turnover'      => $money
                        ])
                    ){
                        DB::rollBack();
                        \Log::debug('添加商户每日营业额失败');
                        return false;
                    }
                }

                //添加商户余额日志
                if(!DB::table($storeIncomeModel->getTable())->insert([
                    'stores_id'     => $orderInfo->stores_id,
                    'orders_id'     => $orderId,
                    'money'         => $money,
                    'create_time'   => date('Y-m-d H:i:s', time()),
                    'type'          => 1
                ])){
                    DB::rollBack();
                    \Log::debug('添加商户余额日志失败');
                    return false;
                }

                //增加商品销量
                $orderGoods = DB::table($orderGoodsModel->getTable())->select('goods_id', 'quantity')
                    ->where('orders_id', $orderId)
                    ->get();

                $quantity = 0;
                foreach ($orderGoods as $value) {
                    $quantity += $value->quantity;
                    if(!DB::table($goodsModel->getTable())
                        ->where('id', $value->goods_id)
                        ->update([
                            'sales_volume'          => DB::raw("sales_volume + {$value->quantity}"),
                            'mouth_sales_volume'    => DB::raw("mouth_sales_volume + {$value->quantity}"),
                            'people_volume'         => DB::raw("people_volume + 1")
                        ]))
                    {
                        DB::rollBack();
                        \Log::debug('添加商品销量失败');
                        return false;
                    }
                }

                //添加平台营业额
                $turnover = DB::table($turnoverModel->getTable())
                    ->where('date', date('Y-m-d', strtotime($orderInfo->pay_time)))
                    ->first();
                if ($turnover){
                    if(!DB::table($turnoverModel->getTable())
                        ->where('date', date('Y-m-d', strtotime($orderInfo->pay_time)))
                        ->increment('turnover', $money)
                    ){
                        DB::rollBack();
                        \Log::debug('添加平台营销额失败');
                        return false;
                    }
                }else{
                    if(!DB::table($turnoverModel->getTable())
                        ->insert([
                            'turnover'  => $money,
                            'date'      => date('Y-m-d', strtotime($orderInfo->pay_time))
                        ])
                    ){
                        DB::rollBack();
                        \Log::debug('修改平台营销额失败');
                        return false;
                    }
                }

                //添加商户月销量
                $goodsVolume = DB::table($storeVolumeModel->getTable())
                    ->where('stores_id', $orderInfo->stores_id)
                    ->where('year', date('Y', time()))
                    ->where('month', date('m', time()))
                    ->first();
                if ($goodsVolume){
                    if(!DB::table($storeVolumeModel->getTable())
                        ->where('stores_id', $orderInfo->stores_id)
                        ->where('year', date('Y', time()))
                        ->where('month', date('m', time()))
                        ->increment('volume', $quantity))
                    {
                        DB::rollBack();
                        \Log::debug('添加商户月销量日志失败');
                        return false;
                    }
                }else{
                    if(!DB::table($storeVolumeModel->getTable())
                        ->insert([
                            'stores_id' => $orderInfo->stores_id,
                            'year'      => date('Y', time()),
                            'month'     => date('m', time()),
                            'volume'    => $quantity
                        ]))
                    {
                        DB::rollBack();
                        \Log::debug('添加商户月销量日志失败');
                        return false;
                    }
                }

                //添加用户消费积分
//                $userInfo = DB::table($userModel->getTable())
//                    ->select('integral', 'is_shop', 'level', 'parent_id')
//                    ->where('id',$orderInfo->users_id)
//                    ->first();
//                $userData = [];
//                $userIntegralData = [];
//                $integral = floor(priceIntToFloat($orderInfo->pay_amount))
//                            -
//                            floor(priceIntToFloat($orderInfo->delivery_fee));
//
//                $userIntegralData[0] = [
//                    'users_id'      => $orderInfo->users_id,
//                    'vary'          => 1,
//                    'integral'      => $integral,
//                    'source'        => '商城消费',
//                    'create_time'   => date('Y-m-d', time())
//                ];
//
//                //未消费
//                if ($userInfo->is_shop == 0 && $userInfo->parent_id > 0){
//                    $parentIntegral      = floor($integral * 0.8);
//                    if(!DB::table($userModel->getTable())
//                        ->where('id', $userInfo->parent_id)
//                        ->update([
//                            'integral'   => $parentIntegral
//                        ])
//                    ){
//                        \Log::debug('消费上级增加余额失败');
//                        DB::rollBack();
//                        return false;
//                    }
//
//                    $userIntegralData[1] = [
//                        'users_id'      => $orderInfo->parent_id,
//                        'vary'          => 1,
//                        'integral'      => $parentIntegral,
//                        'source'        => '下级用户第一次消费赠送',
//                        'create_time'   => date('Y-m-d', time())
//                    ];
//                }
//                $userData['is_shop']    = 1;
//                $userData['integral']   = DB::raw('integral + '.$integral);
//                if( DB::table($userModel->getTable())
//                        ->where('id', $orderInfo->users_id)
//                        ->update($userData) === false
//                ){
//                    \Log::debug('orderBlock修改用户信息失败');
//                    DB::rollBack();
//                    return false;
//                }
//
//
//                /**
//                 * 添加用户积分明细日志
//                 */
//                if(!DB::table($userIntegralModel->getTable())
//                    ->insert($userIntegralData)
//                ){
//                    \Log::debug('添加用户积分明细日志失败');
//                    DB::rollBack();
//                    return false;
//                }



            }

            if(DB::table($orderModel->getTable())
                    ->where('id', $orderId)
                    ->update(['status' => $status]) === false
            ){
                DB::rollBack();
                \Log::debug('修改订单状态失败');
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }
    }


    public function orderUseRedEnvelope($userId, $orderId, $redEnvelopeId){
        $userRedEnvelopeModel       = new UserRedEnvelope();
        $orderRedEnvelopeModel      = new OrderRedEnvelope();
        $redEnvelopeModel           = new RedEnvelope();
        $orderModel                 = new Orders();

        $orderRedEnvelope = DB::table($orderRedEnvelopeModel->getTable())
            ->where('orders_id', $orderId)
            ->first();

        $orderInfo = DB::table($orderModel->getTable())
            ->where('id', $orderId)
            ->first();


        try{
            DB::beginTransaction();


            if (!empty($redEnvelopeId)){
                $userRedEnvelope = DB::table($userRedEnvelopeModel->getTable().' as ure')
                    ->select(
                        'ure.id as user_red_id',
                        'ure.start_time',
                        'ure.end_time',
                        're.*'
                    )
                    ->leftJoin($redEnvelopeModel->getTable().' as re', 're.id','=','ure.red_envelope_id')
                    ->where('ure.red_envelope_id', $redEnvelopeId)
                    ->where('ure.users_id', $userId)
                    ->whereNull('re.delete_time')
                    ->first();

                if (!$userRedEnvelope){
                    return -1;
                }

                if (
                    $userRedEnvelope->end_time < date('Y-m-d H:i:s', time())
                    ||
                    $userRedEnvelope->start_time > date('Y-m-d H:i:s', time())
                ){
                    return -2;
                }

                if (($orderInfo->pay_amount + $orderInfo->discount_amount) < $userRedEnvelope->full_amount){
                    return -3;
                }

                $payAmount = ($orderInfo->pay_amount+$orderInfo->red_discount_amount)-$userRedEnvelope->discount_amount;
                if(DB::table($orderModel->getTable())
                    ->where('id', $orderId)
                    ->update([
                        'pay_amount'            => $payAmount,
                        'red_discount_amount'   => $userRedEnvelope->discount_amount
                    ]) === false
                ){
                    \Log::debug('修改订单支付金额失败');
                    DB::rollBack();
                    return false;
                }


                if ($orderRedEnvelope){
                    if(DB::table($orderRedEnvelopeModel->getTable())
                        ->where('orders_id', $orderId)
                        ->update([
                            'red_envelope'          => $redEnvelopeId,
                            'user_red_envelope_id'  => $userRedEnvelope->id,
                            'money'                 => $userRedEnvelope->discount_amount
                        ]) === false
                    ){
                        \Log::debug('修改订单红包失败');
                        DB::rollBack();
                        return false;
                    }

                }else{
                    if(!DB::table($orderRedEnvelopeModel->getTable())
                        ->insert([
                            'orders_id'             => $orderId,
                            'red_envelope'          => $redEnvelopeId,
                            'user_red_envelope_id'  => $userRedEnvelope->id,
                            'money'                 => $userRedEnvelope->discount_amount
                        ])
                    ){
                        \Log::debug('添加订单红包失败');
                        DB::rollBack();
                        return false;
                    }
                }

            }else{
                if ($orderRedEnvelope){
                    if(
                    !DB::table($orderRedEnvelopeModel->getTable())
                        ->where('orders_id', $orderId)
                        ->delete()
                    ){
                        \Log::debug('删除订单红包失败');
                        DB::rollBack();
                        return false;
                    }
                }

            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }

    }

    public function applyOrderRefund($data){
        try{
            $orderModel             = new Orders();
            $orderGoodsModel        = new OrderGoods();
            $orderRefundModel       = new OrderRefund();

            if(DB::table($orderRefundModel->getTable())
                ->where('orders_id', $data['orders_id'])
                ->where('order_goods_id', $data['order_goods_id'])
                ->first()
            ){
                return -1;
            }

            $orderInfo = DB::table($orderModel->getTable().' as o')
                ->select(
                    'o.status as order_status',
                    'og.selling_price as goods_amount',
                    'og.sku',
                    'og.quantity',
                    'o.pay_amount',
                    'o.stores_id',
                    'o.pay_type'
                )
                ->leftJoin($orderGoodsModel->getTable().' as og', 'o.id', '=', 'og.orders_id')
                ->where('og.orders_id', $data['orders_id'])
                ->where('og.id', $data['order_goods_id'])
                ->first();

            if (!in_array($orderInfo->order_status, [1,2])){
                return -2;
            }

            $orderGoodsTotal = DB::table($orderGoodsModel->getTable())
                ->where('orders_id', $data['orders_id'])
                ->count();

            $data['order_status']               = $orderInfo->order_status;
            $data['refund_transaction_id']      = md5($data['orders_id'].$data['order_goods_id']);
            $data['stores_id']                  = $orderInfo->stores_id;
            $data['goods_amount']               = $orderInfo->goods_amount;
            $data['goods_sku']                  = $orderInfo->sku;
            $data['goods_quantity']             = $orderInfo->quantity;
            $data['pay_amount']                 = $orderInfo->pay_amount;
            $data['message']                    = '申请中';
            $data['type']                       = $orderInfo->pay_type;
            $data['order_goods_num']            = $orderGoodsTotal;

            DB::beginTransaction();
            if(!DB::table($orderRefundModel->getTable())
                ->insert($data)
            ){
                \Log::debug('添加订单退款记录失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($orderGoodsModel->getTable())
                ->where('orders_id', $data['orders_id'])
                ->where('id', $data['order_goods_id'])
                ->update([
                    'is_refund'         => 1
                ])
            ){
                \Log::debug('修改订单商品退款状态失败');
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }


    }

    /**
     * 获取退款列表
     * @param $page
     * @param $pageSize
     * @param $search
     * @return array
     */
    public function getOrderRefundList($page, $pageSize, $search){
        $orderRefundModel           = new OrderRefund();
        $orderGoodsModel            = new OrderGoods();
        $orderModel                 = new Orders();

        $sql = DB::table($orderRefundModel->getTable().' as or')
            ->select(
                'og.name as goods_name',
                'o.batch',
                DB::raw("convert(jl_or.pay_amount/100, decimal(15,2)) as pay_amount"),
                DB::raw("convert(jl_or.goods_amount/100, decimal(15,2)) as goods_amount"),
                'or.goods_quantity',
                'or.create_time',
                'or.order_goods_num',
                'or.status',
                'o.status as order_status',
                'or.cause',
                'or.id',
                'or.express_delivery',
                'or.express_delivery_image'
            )
            ->leftJoin($orderGoodsModel->getTable().' as og','og.id','=','or.order_goods_id')
            ->leftJoin($orderModel->getTable().' as o', 'o.id', '=', 'or.orders_id');

        if (isset($search['stores_id']) && !empty($search['stores_id'])){
            $sql->where('or.stores_id', $search['stores_id']);
        }

        if (isset($search['users_id']) && !empty($search['users_id'])){
            $sql->where('or.users_id', $search['users_id']);
        }

        $total          = $sql->count();
        $list           = $sql->skip(($page - 1) * $pageSize)
                            ->take($pageSize)
                            ->orderBy('or.create_time', 'desc')
                            ->get();

        return [
            'list'          => $list,
            'pagination'    => [
                'current'   => $page,
                'pageSize'  => $pageSize,
                'total'     => $total
            ]
        ];
    }


    public function checkOrderRefund($refundId, $status, $orderStatus, $money, $message){
        $orderRefundModel       = new OrderRefund();
        $userModel              = new Users();
        $userWalletModel        = new UserWalletLog();
        $weChatModel            = new WechatPay();
        $orderGoodsModel        = new OrderGoods();
        $storeIncomeModel       = new StoreIncomeLog();
        $storeModel             = new Stores();
        $orderModel             = new Orders();

        try{
            DB::beginTransaction();

            $saveData = [];
            $saveData['status'] = $status;
            switch ($status){
                case 1:
                    if ($orderStatus == 1){
                        $saveData['message'] = '成功';
                    }else{
                        $saveData['message'] = '申请成功，请填写快递信息';
                    }
                    $saveData['refund_amount'] = (int)($money*100);
                    break;
                case 2:
                    $saveData['message'] = '已填写信息';
                    break;
                case 3:
                    $saveData['message'] = '已退款';
                    break;
                case 4:
                    $saveData['message'] = $message;
            }

            if(!DB::table($orderRefundModel->getTable())
                ->where('id', $refundId)
                ->update($saveData)
            ){
                \Log::debug('修改退款订单失败'.json_encode($saveData));
                DB::rollBack();
                return false;
            }
            //退款订单信息
            $refundInfo = DB::table($orderRefundModel->getTable().' as or')
                ->select(
                    'or.users_id',
                    'or.type',
                    'or.refund_amount',
                    'or.pay_amount',
                    'or.refund_transaction_id',
                    'w.transaction_id',
                    'or.stores_id',
                    'or.orders_id',
                    'or.order_goods_id',
                    'o.batch'
                )
                ->leftJoin($weChatModel->getTable().' as w', 'or.orders_id', '=', 'w.orders_id')
                ->leftJoin($orderModel->getTable().' as o', 'o.id', '=', 'or.orders_id')
                ->where('or.id', $refundId)
                ->first();
            //status 0：审核中 1：申请成功 2：填写信息 3：已退款 4：驳回
            if ($status == 1){
                if(!DB::table($orderGoodsModel->getTable())
                    ->where('id', $refundInfo->order_goods_id)
                    ->update([
                        'refund_amount'     => $money*100
                    ])
                ){
                    \Log::debug('修改商品退款金额失败');
                    DB::rollBack();
                    return false;
                }
            }
            //同意退款
            if ($status == 3 || $status == 3 && $orderStatus == 1){
                //订单商品
                $orderGoods = DB::table($orderGoodsModel->getTable())
                    ->select('selling_price', 'quantity', 'refund_amount')
                    ->where('orders_id', $refundInfo->orders_id)
                    ->get();

                $totalGoodsAmount = 0;     //订单总支付
                $totalRefundAmount = 0;    //已退款金额
                foreach ($orderGoods as $value) {
                    $totalGoodsAmount += $value->selling_price * $value->quantity;
                    $totalRefundAmount += $value->refund_amount;
                }

                //可退款金额必须大于等于退款金额，否则只能退款可退款金额
                if ($totalGoodsAmount - $totalRefundAmount < $refundInfo->refund_amount){
                    $refundAmount = $totalGoodsAmount - $totalRefundAmount;
                    if(!DB::table($orderRefundModel->getTable())
                        ->where('id', $refundId)
                        ->update([
                            'refund_amount'     => $refundAmount
                        ])
                    ){
                        \Log::debug('修改退款订单退款金额失败');
                        DB::rollBack();
                        return false;
                    }

                }else{
                    $refundAmount = $refundInfo->refund_amount;
                }
                // 1微信支付 2支付宝支付 3余额
                if ($refundInfo->type == 3){
                    if(!DB::table($userModel->getTable())
                        ->where('id', $refundInfo->users_id)
                        ->increment('balance', $refundAmount)
                    ){
                        \Log::debug('退回用户余额失败');
                        DB::rollBack();
                        return false;
                    }

                    $balance = DB::table($userModel->getTable())
                        ->where('id', $refundInfo->users_id)
                        ->value('balance');
                    if(!DB::table($userWalletModel->getTable())
                        ->insert([
                            'users_id'      => $refundInfo->users_id,
                            'money'         => $refundAmount,
                            'balance'       => $balance,
                            'wallet_type'   => 3,
                            'stores_id'     => $refundInfo->stores_id,
                            'remake'        => '商品退款'
                        ])
                    ){
                        \Log::debug('添加钱包明细失败');
                        DB::rollBack();
                        return false;
                    }

                }else if ($refundInfo->type == 2){
                    $app = Factory::payment(\Config::get('wechat.payment.default'));
                    $result = $app->refund->byTransactionId(
                        $refundInfo->transaction_id,                //微信订单号
                        $refundInfo->refund_transaction_id,         //退款订单号
                        $refundInfo->pay_amount,                    //支付金额
                        $refundAmount,                 //退款金额
                        [
                            'refund_desc' => '商品退款',
                        ]
                    );
                    \Log::debug('微信退款信息'.json_encode($result));
                    if ($result['return_code'] != 'SUCCESS'){
                        DB::rollBack();
                        return false;
                    }
                }else if ($refundInfo->type == 3){
                    $aliPayModel        = new Alipay();
                    $outTradeNo         = DB::table($aliPayModel->getTable())
                        ->where('orders_id', $refundInfo->orders_id)
                        ->value('out_trade_no');
                    $result             = Pay::alipay(config('ali-pay.config'))->refund([
                        'out_trade_no'          => $outTradeNo,
                        'refund_amount'         => priceIntToFloat($refundAmount),
                        'refund_reason'         => '商家未接单，取消订单，金额退回'
                    ]);
                    \Log::debug('支付宝退款信息'.json_encode($result));
                    if ($result['msg'] != 'Success' && $result['code'] != 10000){
                        DB::rollBack();
                        return false;
                    }
                }

            }

            if ($status == 4){
                if(!DB::table($storeModel->getTable())
                    ->where('id', $refundInfo->stores_id)
                    ->increment('s_amount', $refundInfo->refund_amount)
                ){
                    \Log::debug('提现驳回添加商户余额失败');
                    DB::rollBack();
                    return false;
                }

                if(!DB::table($storeIncomeModel->getTable())
                    ->insert([
                        'stores_id'         => $refundInfo->stores_id,
                        'orders_id'         => $refundInfo->orders_id,
                        'money'             => $refundInfo->refund_amount,
                        'type'              => 1,
                        'remarks'           => '驳回订单：'.$refundInfo->batch.'，收入：'.priceIntToFloat($refundInfo->refund_amount)
                    ])
                ){
                    \Log::debug('提现驳回添加商户余额日志失败');
                    DB::rollBack();
                    return false;
                }

            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }
    }

    /**
     * 确认接单
     * @param $id
     * @return bool|int
     */
    public function confirmOrders($id){
        $orderModel     = new Orders();

        $path = public_path().'/qrcode/junanzenorder/';
        if (!is_dir($path)){
            @mkdir($path, 0755);
        }
        $imageName = $id.time().'.png';
        $code       = getSalt();
        $codeResult = QrCode::format('png')
            ->size(400)
            ->encoding('UTF-8')
            ->generate(
                $code,
                "{$path}/$imageName"
            );

        if ($codeResult){
            return DB::table($orderModel->getTable())
                ->where('id', $id)
                ->update([
                    'status'    => 2,
                    'qrcode'    => env('APP_URL').'/qrcode/junanzenorder/'.$imageName,
                    'code'      => $code
                ]);
        }else{
            return false;
        }
    }

    /**
     * 获取我的拼团订单
     * @param $page
     * @param $pageSize
     * @param $userId
     * @return array
     */
    public function getMyBargainOrder($page, $pageSize, $userId){
        $orderBargainModel      = new OrderBargain();
        $goodsModel             = new Goods();
        $storeModel             = new Stores();

        $prefix = env('DB_PREFIX');
        $sql = DB::table($orderBargainModel->getTable().' as ob')
            ->select(
                'ob.id',
                'ob.goods_id',
                'g.name as goods_name',
                'g.thumbnail',
                's.id as stores_id',
                's.name as store_name',
                DB::raw("convert({$prefix}ob.original_price/100, decimal(15,2)) as original_price"),
                DB::raw("convert({$prefix}ob.reserve_price/100, decimal(15,2)) as reserve_price"),
                'ob.status'
            )
            ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'ob.goods_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'g.stores_id')
            ->where('ob.users_id', $userId)
            ->orderBy('ob.create_time', 'desc');

        $total = $sql->count();
        $list  = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

        return [
            'list'          => $list,
            'pagination'    => [
                'current'   => $page,
                'pageSize'  => $pageSize,
                'total'     => $total
            ]
        ];

    }

    /**
     * 获取砍价订单详情
     * @param $bargainId
     * @param $usersId
     * @return array|bool
     */
    public function getMyBargainOrderInfo($bargainId, $usersId){
        $goodsModel             = new Goods();
        $userModel              = new Users();
        $orderBargainModel      = new OrderBargain();
        $orderBargainLogModel   = new OrderBargainLog();
        $storeInfoModel         = new StoreInfo();

        $prefix = env('DB_PREFIX');
        $bargainInfo = DB::table($orderBargainModel->getTable().' as ob')
            ->select(
                'g.name as goods_name',
                'g.thumbnail',
                'ob.original_price',
                'ob.current_price',
                DB::raw("convert({$prefix}ob.reserve_price/100, decimal(15,2)) as reserve_price"),
                'ob.time_limit',
                'ob.status',
                'ob.create_time',
                'ob.goods_id',
                'si.address'
            )
            ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'ob.goods_id')
            ->leftJoin($storeInfoModel->getTable().' as si', 'si.store_id', '=', 'g.stores_id')
            ->where('ob.id', $bargainId)
            ->where('ob.users_id', $usersId)
            ->first();

        if (!$bargainInfo){
            return false;
        }
        //已砍价格 = 原价 - 当前价格
        $bargainInfo->already_price = priceIntToFloat($bargainInfo->original_price - $bargainInfo->current_price);
        $bargainInfo->original_price = priceIntToFloat($bargainInfo->original_price);
        $bargainInfo->current_price = priceIntToFloat($bargainInfo->current_price);
        if ($bargainInfo->status == 1 || $bargainInfo->status == 2){
            $bargainInfo->remaining_time = 0;
        }else{
            $bargainInfo->remaining_time = strtotime($bargainInfo->time_limit) - time() < 0
                ?
                0
                :
                strtotime($bargainInfo->time_limit) - time();
        }


        $bargainUseLog = DB::table($orderBargainLogModel->getTable().' as ogl')
            ->select(
                'u.avatar',
                'u.name as user_name',
                'ogl.create_time',
                DB::raw("convert({$prefix}ogl.money/100, decimal(15,2)) as money")
            )
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'ogl.users_id')
            ->where('ogl.orders_id', $bargainId)
            ->get();

        return [
            'bargainInfo'       => $bargainInfo,
            'bargainUseLog'     => $bargainUseLog
        ];
    }

    /**
     * 创建砍价订单
     * @param $goodsId
     * @param $userId
     * @return bool|int
     */
    public function createBargainOrder($goodsId, $userId){
        $goodsModel             = new Goods();
        $goodsBargainModel      = new GoodsBargain();
        $orderBargainModel      = new OrderBargain();
        $orderBargainLogModel   = new OrderBargainLog();

        try{
            $goodsBargain = DB::table($goodsBargainModel->getTable().' as bg')
                ->select('bg.*', 'g.selling_price')
                ->leftJoin($goodsModel->getTable().' as g','g.id', '=','bg.goods_id')
                ->where('bg.goods_id', $goodsId)
                ->first();

            if (!$goodsBargain){
                return -1;
            }

            if ($goodsBargain->total_sum <= 0){
                return -2;
            }

            $num   = $goodsBargain->total_num - 1 == 0 ? 1 :  $goodsBargain->total_num - 1;  //剩余可砍价次数
            $money = $goodsBargain->selling_price - $goodsBargain->reserve_price; //剩余可砍价金额
            $min = 1;                                           //每个人最少能砍一分钱

            $max = ($money  - ($num-1) * $min)/$num;        //档次砍价金额上限
            if ($num == 1){
                $money = $max;
            }else{
                $money = mt_rand($min, $max);                       //产生随机的金额
            }

            DB::beginTransaction();
            $orderBargainId = DB::table($orderBargainModel->getTable())
                ->insertGetId([
                    'orders_id'         => 0,
                    'goods_id'          => $goodsId,
                    'users_id'          => $userId,
                    'original_price'    => $goodsBargain->selling_price,
                    'reserve_price'     => $goodsBargain->reserve_price,
                    'current_price'     => $goodsBargain->selling_price - $money,
                    'time_limit'        => date('Y-m-d H:i:s', time() + $goodsBargain->time_limit*60*60),
                    'total_num'         => $goodsBargain->total_num - 1,
                    'status'            => $num == 1 ? 1 : 0
                ]);
            if(!$orderBargainId){
                \Log::debug('添加砍价订单失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($orderBargainLogModel->getTable())
                ->insert([
                    'orders_id'         => $orderBargainId,
                    'users_id'          => $userId,
                    'money'             => $money
                ])
            ){
                \Log::debug('添加砍价记录失败');
                DB::rollBack();
                return false;
            }

            $totalSumResult = DB::table($goodsBargainModel->getTable())
                ->where('goods_id', $goodsId)
                ->update([
                    'total_sum'     => DB::raw('total_sum - 1')
                ]);
            if (!$totalSumResult){
                \Log::debug($totalSumResult.'修改砍价商品库存失败');
                DB::rollBack();
                return false;
            }
//            if(!DB::table($goodsBargainModel->getTable())
//                ->where('goods_id', $goodsId)
//                ->update([
//                    'total_sum'     => DB::raw('total_sum - 1')
//                ])
//            ){
//                \Log::debug('修改砍价商品库存失败');
//                DB::rollBack();
//                return false;
//            }

            Redis::setex(
                'bargain-order-cancel:'.$orderBargainId,
                30,
                'bargain-order-cancel'
            );

            DB::commit();
            return $orderBargainId;
        }catch(\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }

    /**
     * 加入砍价
     * @param $userId
     * @param $bargainId
     * @return bool|int
     */
    public function joinBargainOrder($userId, $bargainId){
        $orderBargainModel          = new OrderBargain();
        $orderBargainLogModel       = new OrderBargainLog();

        $bargain = DB::table($orderBargainModel->getTable())
            ->where('id', $bargainId)
            ->first();

        //超过订单时间限制
        if ($bargain->time_limit <= date('Y-m-d H:i:s', time())){
            return -1;
        }
        //已砍到底
        if ($bargain->current_price == $bargain->reserve_price){
            return -2;
        }
        //剩余可砍次数为0
        if ($bargain->total_num == 0){
            return -3;
        }
        //订单状态不为砍价中 0：砍价中 1：砍价成功 2：砍价失败 3：已下单
        if ($bargain->status != 0){
            return -4;
        }

        $bargainLog = DB::table($orderBargainLogModel->getTable())
            ->where('orders_id', $bargainId)
            ->where('users_id', $userId)
            ->first();
        if ($bargainLog){
            return -5;
        }

        $money = $bargain->current_price - $bargain->reserve_price;
        $num   = $bargain->total_num;  //剩余可砍价次数
        if ($num > 1){
            $min = 1;                                           //每个人最少能砍一分钱
            $max = ($money  - ($num-1) * $min)/($num-1);        //档次砍价金额上限
            $money = mt_rand($min, $max);
        }

        try{
            DB::beginTransaction();

            $bargainSaveData = [];
            $bargainSaveData['current_price']   = DB::raw('current_price - '.$money);
            $bargainSaveData['total_num']       = DB::raw('total_num - 1');
            if ($bargain->total_num == 1 || $bargain->current_price - $money == $bargain->reserve_price){
                $bargainSaveData['status']      = 1;
            }

            if(!DB::table($orderBargainModel->getTable())
                ->where('id', $bargainId)
                ->update($bargainSaveData)
            ){
                \Log::debug('修改砍价订单失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($orderBargainLogModel->getTable())
                ->insert([
                    'orders_id'     => $bargainId,
                    'users_id'      => $userId,
                    'money'         => $money
                ])
            ){
                \Log::debug('添加砍价订单日志失败');
                DB::rollBack();
                return false;
            }

            DB::commit();
            return $money;
        }catch(\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }

    }


    public function getBargainByOrder($bargainId,$userId){
        $orderBargainModel      = new OrderBargain();
        $goodsModel             = new Goods();
        $orderModel             = new Orders();
        $orderGoodsModel        = new OrderGoods();

        $bargain = DB::table($orderBargainModel->getTable().' as ob')
            ->select('ob.*', 'g.stores_id', 'g.name as goods_name', 'g.thumbnail')
            ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'ob.goods_id')
            ->where('ob.id', $bargainId)
            ->where('ob.users_id', $userId)
            ->first();

        //无记录
        if (!$bargain){
            return -1;
        }
        //未砍到底价
        if ($bargain->current_price != $bargain->reserve_price){
            return -2;
        }
        //订单超时
        if ($bargain->time_limit < date('Y-m-d H:i:s', time())){
            return -3;
        }

        try{
            DB::beginTransaction();
            $batch = $userId.(microtime(true) * 10000).mt_rand(1000, 9999);
            $orderSn = $bargain->stores_id . (microtime(true) * 10000) . $userId;

            //创建订单
            $orderId = DB::table($orderModel->getTable())
                ->insertGetId([
                    'batch'             => $batch,
                    'company_id'        => 1,
                    'users_id'          => $userId,
                    'stores_id'         => $bargain->stores_id,
                    'order_sn'          => $orderSn,
                    'goods_price'       => $bargain->original_price,
                    'pay_amount'        => $bargain->current_price,
                    'discount_amount'   => $bargain->original_price - $bargain->current_price,
                    'type'              => 1,
                    'pay_mode'          => 1,
                    'activity_type'     => 1
                ]);
            if(!$orderId){
                \Log::debug('创建订单失败');
                DB::rollBack();
                return false;
            }

            //添加订单商品
            if(!DB::table($orderGoodsModel->getTable())
                ->insert([
                    'orders_id'         => $orderId,
                    'goods_id'          => $bargain->goods_id,
                    'name'              => $bargain->goods_name,
                    'thumbnail'         => $bargain->thumbnail,
                    'selling_price'     => $bargain->current_price,
                    'quantity'          => 1
                ])
            ){
                \Log::debug('添加订单商品失败');
                DB::rollBack();
                return false;
            }

            //修改砍价订单
            if(!DB::table($orderBargainModel->getTable())
                ->where('id', $bargainId)
                ->update([
                    'orders_id' => $orderId,
                    'status'    => 3
                ])
            ){
                \Log::debug('修改砍价订单失败');
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        }catch(\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }
    }


    public function startGroupOrder($goodsId, $userId, $remake){
        $goodsModel                 = new Goods();
        $storeModel                 = new Stores();
        $storeInfoModel             = new StoreInfo();
        $goodsGroupBuyModel         = new GoodsGroupBuy();
        $orderGroupBuyModel         = new OrderGroupBuy();
        $orderGroupBuyLogModel      = new OrderGroupBuyLog();

        $groupBuy = DB::table($goodsGroupBuyModel->getTable().' as ggb')
            ->select(
                'ggb.*',
                'g.selling_price as goods_selling_price',
                'g.stores_id',
                's.name as stores_name',
                'g.name as goods_name',
                'si.address'
            )
            ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'ggb.goods_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'g.stores_id')
            ->leftJoin($storeInfoModel->getTable().' as si', 'si.store_id', '=', 'g.stores_id')
            ->where('goods_id', $goodsId)
            ->first();

        if (!$groupBuy){
            return -1;
        }

        if ($groupBuy->total_sum < 1){
            return -2;
        }

        try{

            DB::beginTransaction();
            $orderGroupBuyId = DB::table($orderGroupBuyModel->getTable())
                ->insertGetId([
                    'goods_id'          => $groupBuy->goods_id,
                    'users_id'          => $userId,
                    'stores_id'         => $groupBuy->stores_id,
                    'original_price'    => $groupBuy->goods_selling_price,
                    'selling_price'     => $groupBuy->selling_price,
                    'time_limit'        => $groupBuy->time_limit,
                    'end_time'          => date('Y-m-d H:i:s', time() + $groupBuy->time_limit * 60 * 60),
                    'total_num'         => $groupBuy->total_num
                ]);

            if (!$orderGroupBuyId){
                \Log::debug('添加拼团主订单失败');
                DB::rollBack();
                return false;
            }

            $outTradeNo = $groupBuy->stores_id . (microtime(true) * 10000) . $userId;
            $logOrderId = DB::table($orderGroupBuyLogModel->getTable())
                ->insertGetId([
                    'orders_id'         => $orderGroupBuyId,
                    'goods_id'          => $groupBuy->goods_id,
                    'users_id'          => $userId,
                    'is_self'           => 1,
                    'order_no'          => $outTradeNo,
                    'is_pay'            => 0,
                    'remake'            => $remake
                ]);
            if(!$logOrderId){
                \Log::debug('添加拼团订单日志失败');
                DB::rollBack();
                return false;
            }


            DB::commit();
            return $logOrderId;
        }catch(\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }


    /**
     * 获取当前商品正在拼团订单
     * @param $page
     * @param $pageSize
     * @param $goodsId
     * @return array
     */
    public function getGroupBuyOrder($page, $pageSize, $goodsId){
        $orderGroupModel        = new OrderGroupBuy();
        $userModel              = new Users();

        $data = DB::table($orderGroupModel->getTable().' as og')
            ->select(
                'og.id',
                'u.name',
                'og.total_num',
                'u.avatar'
            )
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'og.users_id')
            ->where('og.goods_id', $goodsId)
            ->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

        $total = DB::table($orderGroupModel->getTable())
            ->where('is_open', 1)
            ->where('total_num', '>', 0)
            ->where('goods_id', $goodsId)
            ->where('end_time', '>', date('Y-m-d H:i:s', time()))
            ->count();

        return [
            'data'      => $data,
            'total'     => $total
        ];
    }

    public function joinGroupOrder($groupBuyId, $userId, $remake){
        $orderGroupBuyModel         = new OrderGroupBuy();
        $orderGroupBuyLogModel      = new OrderGroupBuyLog();

        $group = DB::table($orderGroupBuyModel->getTable())
            ->where('id', $groupBuyId)
            ->first();

        if ($group->total_num == 0){
            return [
                'code'      => 'field',
                'message'   => '拼团人数已满'
            ];
        }

        if ($group->is_open == 0){
            return [
                'code'      => 'field',
                'message'   => '团长尚未支付开团金额'
            ];
        }

        if ($group->end_time < date('Y-m-d H:i:s', time())){
            return [
                'code'      => 'field',
                'message'   => '超过活动时间，请重新开团'
            ];
        }

        $orderGroupLogId = DB::table($orderGroupBuyLogModel->getTable())
            ->insertGetId([
                'orders_id'     => $groupBuyId,
                'goods_id'      => $group->goods_id,
                'users_id'      => $userId,
                'is_self'       => 0,
                'order_no'      => $group->stores_id . (microtime(true) * 10000) . $userId,
                'is_pay'        => 0,
                'remake'        => $remake
            ]);

        if ($orderGroupLogId){
            return [
                'code'      => 'success',
                'message'   => $orderGroupLogId
            ];
        }

        return [
            'code'      => 'faield',
            'message'   => '加入拼团失败'
        ];
    }

    /**
     * 获取我的拼团订单
     * @param $userId
     * @param $page
     * @param $pageSize
     * @return \Illuminate\Support\Collection
     */
    public function myOrderGroupBuy($userId, $page, $pageSize){
        $orderGroupBuyModel         = new OrderGroupBuy();
        $goodsModel                 = new Goods();
        $storeModel                 = new Stores();

        return DB::table($orderGroupBuyModel->getTable().' as og')
            ->select(
                'og.id',
                'og.goods_id',
                DB::raw("convert(jl_og.original_price/100, decimal(15,2)) as original_price"),
                DB::raw("convert(jl_og.selling_price/100, decimal(15,2)) as selling_price"),
                'og.is_open',
                'g.name as goods_name',
                's.name as store_name',
                'g.slogan',
                's.logo',
                'g.thumbnail'
            )
            ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'og.goods_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'og.stores_id')
            ->where('og.users_id', $userId)
            ->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();
    }

    /**
     * 后台订单详情
     * @param $id
     * @return array
     */
    public function getOrderInfo($id){
        $storeModel             = new Stores();
        $orderModel             = new Orders();
        $orderGoodsModel        = new OrderGoods();
        $orderAddressModel      = new OrderAddress();
        $districtModel          = new District();

        $orderInfo = DB::table($orderModel->getTable().' as o')
            ->select(
                'o.order_sn',
                'o.create_time',
                'o.pay_type',
                'o.status',
                DB::raw("convert(pay_amount/100, decimal(15,2)) as pay_amount"),
                DB::raw("convert(jl_o.delivery_fee/100, decimal(15,2)) as delivery_fee"),
                DB::raw("convert(discount_amount/100, decimal(15,2)) as discount_amount"),
                'o.code',
                'o.qrcode',
                's.name as store_name',
                'o.express',
                'o.expresssn',
                'oa.consignee',
                'oa.contact',
                'oa.address',
                'oa.province',
                'oa.city',
                'oa.district',
                'o.pay_time',
                's.store_type_id'
            )
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'o.stores_id')
            ->leftJoin($orderAddressModel->getTable().' as oa', 'oa.batch', '=', 'o.batch')
            ->where('o.id', $id)
            ->first();

        $remainingPaymentTime = 0;
        if ($orderInfo->status == 0){
            $remainingPaymentTime = naturalNumber(strtotime($orderInfo->create_time) + 900 - time());
        }else if($orderInfo->status == 1 && $orderInfo->store_type_id == 4){
            $remainingPaymentTime = naturalNumber(strtotime($orderInfo->create_time) + 300 - time());
        }
        $orderInfo->remaining_payment_time = $remainingPaymentTime;


        $address = '';
        $addressData = DB::table($districtModel->getTable())
            ->orWhere('id', $orderInfo->province)
            ->orWhere('id', $orderInfo->city)
            ->orWhere('id', $orderInfo->district)
            ->get();
        foreach ($addressData as $value) {
            $address .= $value->name;
        }
        $address .= $orderInfo->address;
        $orderInfo->address = $address;

        $goods = DB::table($orderGoodsModel->getTable())
            ->select(
                'name',
                DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                'quantity',
                'thumbnail'
            )
            ->where('orders_id', $id)
            ->get();

        return [
            'orderInfo'     => $orderInfo,
            'goods'         => $goods
        ];
    }

    public function recoverOrderAmount($orderSn){
        $orderModel             = new Orders();
        $userModel              = new Users();
        $userWalletModel        = new UserWalletLog();

        try{
            DB::beginTransaction();
            $order = DB::table($orderModel->getTable())
                ->where('order_sn', $orderSn)
                ->first();

            if ($order->status != 5){
                return [
                    'code'          => 'failed',
                    'message'       => '该订单不自持退款'
                ];
            }

            if(!DB::table($orderModel->getTable())
                ->where('id', $order->id)
                ->update(['status' => 2])
            ){
                \Log::debug('修改订单状态失败');
                DB::rollBack();
                return [
                    'code'          => 'failed',
                    'message'       => '修改订单状态失败'
                ];
            }

            if(!DB::table($userModel->getTable())
                ->where('id', $order->users_id)
                ->decrement('balance', $order->pay_amount))
            {
                \Log::debug('扣除用户余额失败');
                DB::rollBack();
                return [
                    'code'          => 'failed',
                    'message'       => '扣除用户余额失败'
                ];
            }


            if(!DB::table($userWalletModel->getTable())
                ->insert([
                    'users_id'      => $order->users_id,
                    'money'         => $order->pay_amount,
                    'balance'       => DB::table($userModel->getTable())->where('id', $order->users_id)->value('balance'),
                    'wallet_type'   => 1,
                    'stores_id'     => $order->stores_id,
                    'type'          => 1,
                    'remake'        => '订单被退回重新发起付款'
                ])
            ){
                \Log::debug('添加用户支付日志失败');
                DB::rollBack();
                return [
                    'code'          => 'failed',
                    'message'       => '订单被退回重新发起付款'
                ];
            }

            DB::commit();
            return [
                'code'          => 'success',
                'message'       => '操作成功'
            ];
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return [
                'code'          => 'failed',
                'message'       => '未知错误'
            ];
        }


    }

    public function saveStoreTurnover($time){
        $orderModel         = new Orders();
        $storeModel         = new Stores();
        $storeTurnoverModel = new StoreTurnover();

        $data = DB::table($orderModel->getTable().' as o')
            ->select('s.id', DB::raw("sum(jl_o.pay_amount) as money"))
            ->leftJoin($storeModel->getTable().' as s' ,'s.id', '=', 'o.stores_id')
            ->whereIn('o.status', [3,6])
            ->where('o.pay_time', 'like', $time.'%')
            ->groupBy('s.id')
            ->get();

        $saveData = [];
        foreach ($data as $key => $datum) {
            $saveData[$key]['stores_id']      = $datum->id;
            $saveData[$key]['date']           = $time;
            $saveData[$key]['turnover']       = $datum->money;
        }

        $result = DB::table($storeTurnoverModel->getTable())
            ->insert($saveData);
        dd($result);
    }



}
