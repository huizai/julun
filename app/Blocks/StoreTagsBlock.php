<?php

namespace App\Blocks;


use App\Models\StoreDevices;
use App\Models\StoreTags;
use DB;


class StoreTagsBlock
{
    private function tagSql($search)
    {
        $storeTagsModel = new StoreTags();
        $sql = DB::table($storeTagsModel->getTable());
        if (isset($search['name'])) {
            $sql->where('tag_name', 'like', "%" . $search['name'] . "%");
        }
        if (isset($search['id'])) {
            $sql->where('id', $search['id']);
        }
        if (isset($search['store_id'])) {
            $sql->where('stores_id', $search['store_id']);
        }

        return $sql;
    }

    /**
     * 获取商户服务列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeTagsList($search, $page, $pageSize, $asc = [], $desc = [])
    {
        $sql = $this->tagSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeTags = $sql->orderBy('id', 'desc')->get();

        return $storeTags;
    }

    /**
     * 获取商户服务列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeTagsListPagination(array $search, int $page = 1, int $pageSize = 20)
    {
        $sql = $this->tagSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户服务详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeTagsInfo($search)
    {
        $sql = $this->tagSql($search);
        $storeTags = $sql->first();
        return $storeTags;
    }

    public function saveStoreTag($tag, $store_id)
    {
        $storeTagModel = new StoreTags();
        $storeTag = DB::table($storeTagModel->getTable())->where('stores_id', $store_id)->get();

        $saveData = [];
        foreach ($tag as $key => $item) {
            $saveData[$key]['tag_name'] = $item;
            $saveData[$key]['stores_id'] = $store_id;
        }

        try {

            DB::beginTransaction();

            if (!$storeTag->isEmpty()) {


                if (!DB::table($storeTagModel->getTable())->where('stores_id', $store_id)->delete()) {
                    DB::rollBack();
                    return false;
                }

            }

            if (!DB::table($storeTagModel->getTable())->insert($saveData)) {
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        } catch (\Exception $exception) {
            \Log::error($exception);
            return false;
        }


    }

}
