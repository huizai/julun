<?php
namespace App\Blocks;


use App\Models\StoreDevices;
use App\Models\StoreSeat;
use App\Models\StoreSeatPosition;
use App\Models\StoreTags;
use DB;


class StoreSeatBlock{
    private function seatSql($search){
        $storeSeatModel = new StoreSeat();
        $storeSeatPositionModel = new StoreSeatPosition();
        $sql = DB::table($storeSeatModel->getTable() . ' as ss')
            ->select(
            'ss.*',
            'ssp.name as position_name'
        )
            ->leftJoin($storeSeatPositionModel->getTable() . ' as ssp', 'ssp.id', '=', 'ss.position')
            ->whereNull('ss.delete_time');
        if(isset($search['id'])){
            $sql->where('ss.id', $search['id']);
        }
        if(isset($search['store_id'])){
            $sql->where('ss.stores_id', $search['store_id']);
        }
        if(isset($search['position'])){
            $sql->where('ss.position', $search['position']);
        }

        return $sql;
    }
    /**
     * 获取商户服务列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeSeatList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->seatSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeSeat = $sql->orderBy('ss.id', 'desc')->get();

        return $storeSeat;
    }

    /**
     * 获取商户服务列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeSeatListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->seatSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户服务详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeSeatInfo($search){
        $sql = $this->seatSql($search);
        $storeSeat = $sql->first();
        return $storeSeat;
    }

}