<?php
namespace App\Blocks;

use App\Models\AgentApply;
use App\Models\AgentTurnover;
use App\Models\AgentTurnoverGoods;
use App\Models\CollectGoods;
use App\Models\CollectMillGoods;
use App\Models\Goods;
use App\Models\GoodsBargain;
use App\Models\GoodsClassifies;
use App\Models\GoodsGroupBuy;
use App\Models\GoodsImages;
use App\Models\GoodsSku;
use App\Models\GoodsSkuName;
use App\Models\GoodsSkuValue;
use App\Models\GoodsTagsMiddle;
use App\Models\OrderGoods;
use App\Models\Orders;
use App\Models\StoreActivity;
use App\Models\StoreCoupons;
use App\Models\StoreInfo;
use App\Models\StoreNavs;
use App\Models\GroupBuy;
use App\Models\StoreYunxin;
use App\Models\UserCoupons;
use App\Models\UserIntegralLog;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use App\Models\Spike;
use App\Models\Stores;

class GoodsBlock{
    private function __goodsSql($search){
        $goodsModel             = new Goods();
        $goodsClassifyModel     = new GoodsClassifies();
        $storeModel             = new Stores();
        $storeNavModel          = new StoreNavs();
        $collectGoodsModel      = new CollectGoods();
        $collectMillGoodsModel  = new CollectMillGoods();
        $storeInfoModel         = new StoreInfo();
//        $agentTurnoverGoodsModel    = new AgentTurnoverGoods();
//
//        $turnoverGoods = <<<SQL
//      SELECT COUNT(id) as order_num ,goods_id FROM agent_turnover_goods GROUP by goods_id;
//SQL;


        $prefix = env('DB_PREFIX');
        $sql = DB::table($goodsModel->getTable() . ' as g')
            ->select(
                'g.id',
                'g.stores_id',
                'g.mill_id',
                'g.classify_id',
                'g.name',
                'g.slogan',
                'g.enable_sku',
                'g.service',
                'g.params',
                'g.price_interval',
                'g.status',
                'g.thumbnail',
                DB::raw("convert({$prefix}g.selling_price/100, decimal(15,2)) as selling_price"),
                DB::raw("convert({$prefix}g.purchase_price/100, decimal(15,2)) as purchase_price"),
                DB::raw("convert({$prefix}g.original_price/100, decimal(15,2)) as original_price"),
//                'tg.order_num',
                'g.sales_volume',
                'g.mouth_sales_volume',
                'g.stock',
                'g.user_volume',
                'g.store_nav_id',
                'g.cuisine_name',
                'g.create_time',
                'gc.name as class_name',
                'gc.icon as class_icon',
                's.name as stores_name',
                's.store_type_id',
                's.rating',
                's.agency',
                's.synopsis',
                's.background',
                's.logo',
                'sn.name as nav_name',
                'si.address'
            )
            ->leftJoin($goodsClassifyModel->getTable() . ' as gc', 'g.classify_id', '=', 'gc.id')
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'g.stores_id')
            ->leftJoin($storeNavModel->getTable(). ' as sn', 'sn.id', '=', 'g.store_nav_id')
            ->leftJoin($storeInfoModel->getTable().' as si', 'si.store_id', '=', 's.id')
            ->whereNull('g.delete_time');

        if(isset($search['collect']) && $search['collect'] === 1){
            $sql->leftJoin($collectGoodsModel->getTable() . ' as cg', 'cg.goods_id', '=', 'g.id');
            if(isset($search['user_id'])) {
                $sql->where('cg.users_id', $search['user_id']);
            }
            if(isset($search['stype'])){
                $sql->where('s.store_type_id', $search['stype']);
            }
        }

        if(isset($search['app_collect']) && $search['app_collect'] === 1){
            $sql->where('cmg.store_id', $search['agent_id'])
                ->leftJoin($collectMillGoodsModel->getTable() . ' as cmg', 'cmg.goods_id', '=', 'g.id');
        }


        if (isset($search['operate'])){
            $sql->where('s.operate', $search['operate']);
        }

        if(isset($search['name'])){
            $sql->where('g.name', 'like', "%".$search['name']."%");
        }

        if(isset($search['cid'])){
            $sql->where('g.classify_id', $search['cid']);
        }

        if(isset($search['id'])){
            $sql->where('g.id', $search['id']);
            $sql->addSelect('g.detail');
        }

        if(isset($search['company'])){
            $sql->where('g.company_id', $search['company']);
        }
        if(isset($search['store_id']) && !empty($search['store_id'])){
            if(is_array($search['store_id'])){
                $sql->whereIn('g.stores_id', $search['store_id']);
            }else {
                $sql->where('g.stores_id', $search['store_id']);
            }
        }

        if(isset($search['p_id'])){
            $sql->where('g.stores_id', $search['p_id']);
        }
        if(isset($search['mill_id'])){
            $sql->where('g.mill_id', $search['mill_id']);
        }
        if(!isset($search['recovery'])){
            $sql->whereNull('g.delete_time');
        }else{
            $sql->where('g.delete_time','<>',null);
        }
        if(isset($search['status'])){
            $sql->where('g.status',$search['status']);
        }
        if(isset($search['store_type'])){
            $sql->where('s.store_type_id',$search['store_type']);
        }
        if(isset($search['nav_id'])){
            $sql->where('g.store_nav_id',$search['nav_id']);
        }

        if(isset($search['goodids'])){
            $sql->whereIn('g.id',$search['goodids']);
        }

        if(isset($search['shop_type'])){
            $sql->where('s.agent_type', 0)
                ->where('s.agency', 0);
        }

        if (isset($search['cuisine'])){
            $sql->where('g.cuisine_name',$search['cuisine']);
        }

        if (isset($search['price'])){
            $sql->whereBetween('selling_price', [$search['price'][0]*100, $search['price'][1]*100]);
        }

        if (isset($search['selling_price'])){
            $sql->where('selling_price', '>', $search['selling_price']);
        }

        if (isset($search['bargain']) && $search['bargain'] == 1){
            $goodsBargainModel  = new GoodsBargain();
            $sql->leftJoin($goodsBargainModel->getTable().' as gb', 'gb.goods_id', '=', 'g.id')
                ->addSelect(
                    DB::raw("convert({$prefix}gb.reserve_price/100, decimal(15,2)) as reserve_price"),
                    'gb.total_sum'
                );
        }

        if (isset($search['group_buy']) && $search['group_buy'] == 1){
            $goodsGroupBuyModel  = new GoodsGroupBuy();
            $sql->leftJoin($goodsGroupBuyModel->getTable().' as ggb', 'ggb.goods_id', '=', 'g.id')
                ->addSelect(
                    DB::raw("convert({$prefix}ggb.selling_price/100, decimal(15,2)) as group_buy_selling_price")
                );
        }



        if (isset($search['long']) && isset($search['lat'])){
            $sql->addSelect(
                DB::raw("SQRT(
                    POW(111.2 * (jl_si.latitude - {$search['lat']}), 2) 
                    +
                    POW(111.2 * ({$search['long']} - jl_si.longitude) * COS(jl_si.latitude / 57.3), 2))
                    
                    as distance
                ")
            );
        }

        if (isset($search['volume'])){
            $sql->orderBy('g.sales_volume', 'desc');
        }

        return $sql->orderBy('g.sort', 'asc');
    }

    /**
     * 获取商品列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function goodsList($search, $page = 1, $pageSize = 20, $asc = [], $desc = []){
        $sql = $this->__goodsSql($search);
        if (isset($search['long']) && isset($search['lat'])){
            $sql->having('distance', '<', '10')->orderBy('distance', 'desc');
        }

        if (isset($search['field']) && isset($search['sequence'])){
            $sql->orderBy('g.'.$search['field'], $search['sequence']);
        }

        if($page > 0){
            $sql->skip(($page - 1) * $pageSize)->take($pageSize);
        }

        if(empty($asc) && empty($desc)) {
            $sql->orderBy('g.sort', 'asc')
                ->orderBy('g.create_time', 'desc');
        }
        if(!empty($asc)){
            foreach ($asc as $a){
                if($a) {
                    $sql->orderBy('g.' . $a, 'asc');
                }
            }
        }
        if(!empty($desc)){
            foreach ($desc as $d){
                if($d) {
                    $sql->orderBy('g.' . $d, 'desc');
                }
            }
        }

        $goods = $sql->groupBy('g.id')
            ->get();

        $ids = [];
        foreach ($goods as $k=>$v){
            $ids[] = $v->id;
            $v->order_num = 0;
        }
        $agentTurnoverGoodsModel    = new AgentTurnoverGoods();
        $agentTurnoverModel         = new AgentTurnover();
        $TurnoverGoods = DB::table($agentTurnoverGoodsModel->getTable() . ' as atg')
            ->select(
                'atg.id',
                'atg.goods_id'
            )
            ->leftJoin($agentTurnoverModel->getTable().' as at', 'atg.turnover_id', '=', 'at.id')
            ->where('at.status', '>=', 3)
            ->whereIn('atg.goods_id', $ids)
//            ->groupBy('atg.goods_id')
            ->get();
        foreach ($goods as $k=>$v){
            foreach ($TurnoverGoods as $item){
                if ($item->goods_id == $v->id){
                    $v->order_num ++;
                }
            }

        }
//        dd($TurnoverGoods);
        return $goods;
    }

    public function goodsListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__goodsSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    /**
     * 获取商品分类列表
     * @param $pid
     * @param $companyId
     */
    public function goodsClassify($pid=null,$companyId){
        if (!$pid){
            $pid = 0;
        }
        $classify = GoodsClassifies::where('parent_id', $pid)
            ->where('company_id', $companyId)
            ->whereNull('delete_time')
            ->get();
        return $classify;
    }

    /**
     * 获取商品详情
     * @param $id
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function goodsInfo($id, $userId){

        $sql = $this->__goodsSql(['id' => $id]);
        $storeYunXinModel   = new StoreYunxin();
        $sql->leftJoin($storeYunXinModel->getTable().' as syx', 'syx.stores_id', '=', 's.id')
            ->addSelect('syx.accid as store_accid');

        $goods = $sql->first();


        if($goods){
            $goods->rating = sprintf("%.1f", $goods->rating);
            $goods->detail = htmlspecialchars_decode($goods->detail);
            //获取商品图片
            $goods->images = [];
            $goodsImage = GoodsImages::where('goods_id', $id)->get();
            foreach ($goodsImage as $image){
                $goods->images[] = $image;
            }

            $goodsSkuNameModel  = new GoodsSkuName();
            $goodsSkuValueModel = new GoodsSkuValue();
            $godosSkuModel      = new GoodsSku();
            $spikeModel         = new Spike();

            //获取商品规格
            $skuName = DB::table($goodsSkuNameModel->getTable())
                ->select(
                    'id',
                    'name',
                    'goods_id',
                    'has_thumbnail'
                )->where('goods_id', $id)->get()->toArray();

            $skuValue = DB::table($goodsSkuValueModel->getTable())->where('goods_id', $id)->get()->toArray();

            $goods->sku     = [];
            $skuStringData  = '';
            foreach ($skuName as $key => $name){
                $name->skuValue = [];
                foreach ($skuValue as $value){
                    if($value->name_id == $name->id) {
                        $value->selected = 0;
                        $name->skuValue[] = $value;
                        $skuStringData= $skuStringData.' '.$value->value;
                    }

                }
                $goods->sku[] = $name;


            }
            $goods->sku_string = $skuStringData;


            $goodsSkuData = DB::table($godosSkuModel->getTable())
                ->select(
                    'id',
                    'value_path',
                    'sales_volume',
                    'stock',
                    'thumbnail',
                    DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                    DB::raw("convert(purchase_price/100, decimal(15,2)) as purchase_price"),
                    DB::raw("convert(original_price/100, decimal(15,2)) as original_price")
                )
                ->where('goods_id', $id)->get();



            $goods->skuData = [];


            foreach ($goodsSkuData as $gSku){
                $gSku->sku_string = null;
                $skuIdData = explode(',', $gSku->value_path);
                foreach ($skuValue as $item) {
                    if (in_array($item->id,$skuIdData)){
                        $gSku->sku_string .= $item->value.' ';
                    }
                }
                $gSku->sku_string = rtrim($gSku->sku_string, ' ');
                $gSku->value_path = rtrim($gSku->value_path, ',');
                $goods->skuData[$gSku->value_path] = $gSku;
            }


            $service    = json_decode( $goods->service, true);
            if ($service){
                $goods->service = $service;
            }else{
                $goods->service = [];
            }


            $goods->tags = [];
            $goodsTags = GoodsTagsMiddle::where('goods_id', $id)->get();
            foreach ($goodsTags as $tag){
                $goods->tags[] = $tag;
            }

            if ($userId){
                $collectGoodsModel = new CollectGoods();
                $isCollect = DB::table($collectGoodsModel->getTable())
                    ->where('goods_id',$id)
                    ->where('users_id',$userId)->first();

                if (isset($isCollect->id)){
                    $goods->is_collect=1;
                }else{
                    $goods->is_collect=0;
                }
            }else{
                $goods->is_collect=0;
            }
            $goods->get_type = ['自提','快递'];
            //获取店铺优惠券
            $storeCoupon = new StoreCoupons();
            $userCoupon  = new UserCoupons();
            $coupons = DB::table($storeCoupon->getTable())
                ->where('stores_id', $goods->stores_id)
                ->whereNull('scope')
                ->get();
            foreach ($coupons as $k=>$v){
                if ($userId){
                    $isGet = DB::table($userCoupon->getTable())
                        ->where('users_id', $userId)
                        ->where('coupon_id', $v->id)
                        ->where('is_use', 0)
                        ->whereNull('delete_time')
                        ->first();
                    if (isset($isGet->id)){
                        $v->is_get=1;
                    }else{
                        $v->is_get=0;
                    }
                }else{
                    $v->is_get=0;
                }

            }
            $goods->store_coupon = $coupons;
            //获取店铺活动
            $storeActivityModel = new StoreActivity();
            $storeActivity = DB::table($storeActivityModel->getTable())->where('stores_id', $goods->stores_id)->get();
            foreach ($storeActivity as $k=>$v){
                $v->activity = [];
                $data = json_decode($v->data, true);
                $data['full_amount']        = priceIntToFloat($data['full_amount']);
                $data['discount_amount']    = priceIntToFloat($data['discount_amount']);
                $v->activity[] = $data;
            }
            $goods->store_activity = $storeActivity;
            if ($goods->params){
                $goods->good_params = json_decode($goods->params, true);
            }else{
                $goods->good_params[] = [];
            }

            $goods->count_down = 0;
            $goods->is_spike = 0;
            $time   = intval(date("H", time()));
            $time   = intval(floor($time/2));
            $time   *=2;
            if ($time<10){
                $time .='0';
            }

            //是否秒杀
            $spikeList = DB::table($spikeModel->getTable())
                ->where('goods_id', $id)
                ->where('day', date("Y-m-d", time()))
                ->where('hour', $time.':00')
                ->whereNull('delete_time')
                ->first();

            if ($spikeList){
                $nowTime  = date("Y-m-d", time())." ".$time.':00';
                $goods->selling_price = priceIntToFloat($spikeList->selling_price);
                $goods->spick_price = priceIntToFloat($spikeList->selling_price);
                $goods->is_spike = 1;
                $goods->count_down = strtotime($nowTime) + 7200 - time();
            }

        }

        return $goods;
    }

    /**
     * 获取商品详情(houtai)
     * @param $id
     * @param $store_id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function goodsInfoAdmin($id, $store_id){
        $sql = $this->__goodsSql(['id' => $id]);
        $goods = $sql->first();

        if($goods){
            $goods->detail = htmlspecialchars_decode($goods->detail);
            //获取商品图片
            $goods->images = [];
            $goodsImage = GoodsImages::where('goods_id', $id)->get();
            foreach ($goodsImage as $image){
                $goods->images[] = $image;
            }

            $goodsSkuNameModel      = new GoodsSkuName();
            $goodsSkuValueModel     = new GoodsSkuValue();
            $godosSkuModel          = new GoodsSku();
            $groupModel             = new GoodsGroupBuy();
            $spikeModel             = new Spike();
            $bargainModel           = new GoodsBargain();

            //获取商品规格
            $skuName = DB::table($goodsSkuNameModel->getTable())
                ->select(
                    'id',
                    'name',
                    'goods_id',
                    'has_thumbnail'
                )->where('goods_id', $id)->get()->toArray();
            $skuValue = DB::table($goodsSkuValueModel->getTable())->where('goods_id', $id)->get()->toArray();

            $goods->sku = [];
            foreach ($skuName as $name){
                $name->skuValue = [];
                foreach ($skuValue as $value){
                    if($value->name_id == $name->id) {
                        $value->selected = 0;
                        $name->skuValue[] = $value;
                    }
                }
                $goods->sku[] = $name;
            }

            $goodsSkuData = DB::table($godosSkuModel->getTable())
                ->select(
                    'id',
                    'value_path',
                    'sales_volume',
                    'stock',
                    'thumbnail',
                    DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                    DB::raw("convert(purchase_price/100, decimal(15,2)) as purchase_price"),
                    DB::raw("convert(original_price/100, decimal(15,2)) as original_price")
                )
                ->where('goods_id', $id)->get();

            $goods->skuData = [];
            foreach ($goodsSkuData as $gSku){
                $gSku->value_path = rtrim($gSku->value_path, ',');
                $goods->skuData[] = $gSku;
            }

            //拼团
            $group = DB::table($groupModel->getTable())
                ->where('goods_id',$id)
                ->first();
            if ($group){
                $goods->is_group = true;
                $goods->group_selling_price = priceIntToFloat($group->selling_price);
                $goods->total_num = $group->total_num;
                $goods->total_sum = $group->total_sum;
                $goods->time_limit = $group->time_limit;
            }

            //秒杀
            $spike = DB::table($spikeModel->getTable())->where('goods_id',$id)
                ->whereNull('delete_time')->get();

            if (!$spike->isEmpty()){
                $goodsSpike = [];
                foreach ($spike as $item) {
                    $goodsSpike['spike_selling_price'] = priceIntToFloat($item->selling_price);
                    $goodsSpike['day'][] = $item->day;
                    $goodsSpike['hour'][] = $item->hour;
                    $goodsSpike['stock'][] = $item->stock;
                }

                $goods->is_spike = true;
                $goods->spike_selling_price = $goodsSpike['spike_selling_price'];
                $goods->day                 = array_values(array_unique($goodsSpike['day']));
                $goods->day                 = [$goods->day[0],end($goods->day)];
                $goods->hour                = array_values(array_unique($goodsSpike['hour']));
                $goods->spike_stock         = array_unique($goodsSpike['stock']);
            }

            //砍价
            $bargain = DB::table($bargainModel->getTable())
                ->where('goods_id', $id)
                ->first();
            if ($bargain){
                $goods->is_bargain      = true;
                $goods->reserve_price   = priceIntToFloat($bargain->reserve_price);
                $goods->time_limit      = $bargain->time_limit;
                $goods->total_num       = $bargain->total_num;
                $goods->total_sum       = $bargain->total_sum;
            }

            //服务
            $goods->service = json_decode($goods->service, true);

            //标签
            $goods->tags = [];
            $goodsTags = GoodsTagsMiddle::where('goods_id', $id)->get();
            foreach ($goodsTags as $tag){
                $goods->tags[] = $tag;
            }

            if ($store_id){
//                是否收藏
                $collectMillGoodsModel = new CollectMillGoods();
                $isCollect = DB::table($collectMillGoodsModel->getTable())
                    ->where('goods_id', $id)
                    ->where('store_id', $store_id)->first();

                if (isset($isCollect->id)){
                    $goods->is_collect = 1;
                }else{
                    $goods->is_collect = 0;
                }
            }else{
                $goods->is_collect = 0;
            }
            if ($goods->price_interval){
                $goods->price_interval = json_decode($goods->price_interval, true);
            }else{
                $goods->price_interval = [];
            }

            if ($goods->params){
                $goods->good_params = json_decode($goods->params, true);
            }else{
                $goods->good_params = [];
            }

        }

        return $goods;
    }


    /**
     * 获取商品详情(蓝丝羽App)
     * @param $id
     * @param $store_id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function goodsInfoApp($id, $store_id){
        $sql = $this->__goodsSql(['id' => $id]);
        $goods = $sql->first();

        if($goods){
            //获取商品图片
            $goods->images = [];
            $goodsImage = GoodsImages::where('goods_id', $id)->get();
            foreach ($goodsImage as $image){
                $goods->images[] = $image;
            }

            $goodsSkuNameModel = new GoodsSkuName();
            $goodsSkuValueModel = new GoodsSkuValue();
            $godosSkuModel = new GoodsSku();
            $groupModel = new GroupBuy();
            $spikeModel = new Spike();

            //获取商品规格
            $skuName = DB::table($goodsSkuNameModel->getTable())
                ->select(
                    'id',
                    'name',
                    'goods_id',
                    'has_thumbnail'
                )->where('goods_id', $id)->get()->toArray();
            $skuValue = DB::table($goodsSkuValueModel->getTable())->where('goods_id', $id)->get()->toArray();

            $goods->sku = [];
            foreach ($skuName as $name){
                $name->skuValue = [];
                foreach ($skuValue as $value){
                    if($value->name_id == $name->id) {
                        $value->selected = 0;
                        $name->skuValue[] = $value;
                    }
                }
                $goods->sku[] = $name;
            }

            $goodsSkuData = DB::table($godosSkuModel->getTable())
                ->select(
                    'id',
                    'value_path',
                    'sales_volume',
                    'stock',
                    'thumbnail',
                    DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                    DB::raw("convert(purchase_price/100, decimal(15,2)) as purchase_price"),
                    DB::raw("convert(original_price/100, decimal(15,2)) as original_price")
                )
                ->where('goods_id', $id)->get();

            $goods->skuData = [];
            foreach ($goodsSkuData as $gSku){
                $gSku->value_path = rtrim($gSku->value_path, ',');
                $goods->skuData[] = $gSku;
            }

            $group = DB::table($groupModel->getTable())
                ->select('people', 'selling_price')
                ->where('goods_id',$id)->whereNull('delete_time')
                ->first();
            if ($group){
                $goods->is_group = true;
                $goods->group_selling_price = priceIntToFloat($group->selling_price);
                $goods->people = $group->people;
            }

            $spike = DB::table($spikeModel->getTable())->where('goods_id',$id)
                ->whereNull('delete_time')->get();

            if (!$spike->isEmpty()){
                $goodsSpike = [];
                foreach ($spike as $item) {
                    $goodsSpike['spike_selling_price'] = priceIntToFloat($item->selling_price);
                    $goodsSpike['day'][] = $item->day;
                    $goodsSpike['hour'][] = $item->hour;
                    $goodsSpike['stock'][] = $item->stock;
                }

                $goods->is_spike = true;
                $goods->spike_selling_price = $goodsSpike['spike_selling_price'];
                $goods->day = array_values(array_unique($goodsSpike['day']));
                $goods->day = [$goods->day[0],end($goods->day)];
                $goods->hour = array_values(array_unique($goodsSpike['hour']));
                $goods->spike_stock = array_unique($goodsSpike['stock']);
            }

            $goods->service = json_decode($goods->service, true);

            $goods->tags = [];
            $goodsTags = GoodsTagsMiddle::where('goods_id', $id)->get();
            foreach ($goodsTags as $tag){
                $goods->tags[] = $tag;
            }

            if ($store_id){
//                是否收藏
                $collectMillGoodsModel = new CollectMillGoods();
                $isCollect = DB::table($collectMillGoodsModel->getTable())
                    ->where('goods_id', $id)
                    ->where('store_id', $store_id)->first();

                if (isset($isCollect->id)){
                    $goods->is_collect = 1;
                }else{
                    $goods->is_collect = 0;
                }
//                是否申请了代理
                $agentApplyModel = new AgentApply();
                $isAgent = DB::table($agentApplyModel->getTable())
                    ->where('mill_id', $goods->mill_id)
                    ->where('store_id', $store_id)
                    ->first();
                if (isset($isAgent->id) && $goods->agency == 1){
                    if ($isAgent->status == 0){
                        $goods->p_id = null;
                    }else{
                        $goods->p_id = $isAgent->p_id;
                    }
                }else{
                    $goods->p_id = $goods->mill_id;
                }
            }else{
                $goods->is_collect = 0;
                $goods->p_id = null;
            }


            if ($goods->params){
                $goods->good_params = json_decode($goods->params, true);
            }else{
                $goods->good_params = [];
            }

            if ($goods->price_interval){
                $goods->price_interval = json_decode($goods->price_interval, true);
            }else{
                $goods->price_interval = [];
            }

            if ($goods->detail){
                $goods->detail = json_decode($goods->detail, true);
            }else{
                $goods->detail = [];
            }


        }

        return $goods;
    }


    public function getGroupBuyGoodsInfo($goodsId){
        $goodsModel             = new Goods();
        $goodsGroupBuyModel     = new GoodsGroupBuy();

        DB::table($goodsGroupBuyModel->getTable().' as ggb')
            ->select(
                'g.thumbnail',
                'g.name as goods_name',
                'g.selling_price as original_price',
                'ggb.selling_price as group_buy_selling_price'
            )
            ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'ggb.goods_id')
            ->where('goods_id', $goodsId)
            ->first();
    }

    /**
     * 添加商品
     */
    public function addGoods(
        array $data,
        array $sku=[],
        array $skuData=[],
        array $images=[],
        $group,
        $spike,
        $tags,
        $bargain)
    {
        $goodsModel             = new Goods();
        $goodsSkuModel          = new GoodsSku();
        $goodsSkuValueModel     = new GoodsSkuValue();
        $goodsSkuNameModel      = new GoodsSkuName();
        $goodsImagesModel       = new GoodsImages();
        $spikeModel             = new Spike();
        $goodsTagsMiddleModel   = new GoodsTagsMiddle();

        try{
            DB::beginTransaction();
            //添加商品的基本数据
            $goodsId = DB::table($goodsModel->getTable())->insertGetId($data);
            if(!$goodsId){
                \Log::error('添加商品基本数据失败');
                return false;
            }

            //添加商品的图片数据
            if(!empty($images)) {
                $goodsImagesData = [];
                foreach ($images as $img) {
                    if ($img != ""){
                        $goodsImagesData[] = [
                            'goods_id' => $goodsId,
                            'image' => $img,
                            'thumbnail' => $img,
                        ];
                    }
                }
                if (
                !DB::table($goodsImagesModel->getTable())->insert($goodsImagesData)
                ) {
                    DB::rollBack();
                    \Log::error('添加商品图片失败');
                    return false;
                }
            }

            if(!empty($sku) && !empty($skuData)) {
                //添加规格
                $combinationSku = $this->combinationSku($goodsId, $sku, $skuData);
                if (
                    !DB::table($goodsSkuNameModel->getTable())->insert($combinationSku['name'])
                    ||
                    !DB::table($goodsSkuValueModel->getTable())->insert($combinationSku['value'])
                    ||
                    !DB::table($goodsSkuModel->getTable())->insert($combinationSku['sku'])
                ) {
                    DB::rollBack();
                    \Log::error('添加商品规格失败');
                    return false;
                }
            }

            if (isset($group['is_group']) && $group['is_group'] === true){
                $goodsGroupModel        = new GoodsGroupBuy();
                if(!DB::table($goodsGroupModel->getTable())
                    ->insert([
                        'goods_id'          => $goodsId,
                        'original_price'    => $data['selling_price'],
                        'selling_price'     => $group['group_selling_price'] * 100,
                        'total_num'         => $group['total_num'],
                        'total_sum'         => $group['total_sum'],
                        'time_limit'        => $group['time_limit']
                    ])
                ){
                    DB::rollBack();
                    return false;
                }
            }

            if (isset($group['is_spike']) && $group['is_spike']){
                foreach ($spike['spikeData'] as &$value) {
                    $value['goods_id'] = $goodsId;
                    $value['create_time'] = date('Y-m-d H:i:s',time());
                    $value['selling_price'] = $value['selling_price'] * 100;
                }

                if(
                    !DB::table($spikeModel->getTable())->insert($spike['spikeData'])
                ){
                    DB::rollBack();
                    return false;
                }
            }

            //添加商品的标签数据
            $goodsTagsData = [];
            foreach ($tags as $v){
                $goodsTagsData[] = [
                    'goods_id'      => $goodsId,
                    'tag_id'        => $v,
                ];
            }
            if(
            !DB::table($goodsTagsMiddleModel->getTable())->insert($goodsTagsData)
            ){
                DB::rollBack();
                \Log::error('添加商品标签失败');
                return false;
            }

            /**
             * 砍价
             */
            if (isset($group['is_group']) && $bargain['is_bargain']){
                $bargain['goods_id']    = $goodsId;
                $goodsBargainModel      = new GoodsBargain();
                if(!DB::table($goodsBargainModel->getTable())
                    ->insert([
                        'goods_id'      => $goodsId,
                        'reserve_price' => $bargain['reserve_price'] * 100,
                        'time_limit'    => $bargain['time_limit'],
                        'total_num'     => $bargain['total_num'],
                        'total_sum'     => $bargain['total_sum']
                    ])
                ){
                    \Log::error('砍价商品添加失败');
                    DB::rollBack();
                    return false;
                }
            }

            DB::commit();
            return $goodsId;

        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }


    /**
     * 修改商品(废弃)
     */
    public function updateGoods(array $data, array $sku=[], array $skuData=[], array $images=[], array $tags=[], $spike=[], $group=[]){
        $goodsModel             = new Goods();
        $goodsSkuModel          = new GoodsSku();
        $goodsSkuValueModel     = new GoodsSkuValue();
        $goodsSkuNameModel      = new GoodsSkuName();
        $goodsImagesModel       = new GoodsImages();
        $goodsTagsMiddleModel   = new GoodsTagsMiddle();
        $spikeModel             = new Spike();
        $groupBuyModel          = new GroupBuy();

        DB::beginTransaction();
        try{
            //修改商品的基本数据
            $res = DB::table($goodsModel->getTable())
                ->where('id',$data['id'])
                ->update($data);
            if(!$res){
                \Log::error('修改商品基本数据失败');
                return false;
            }

            //修改商品的图片数据
            //删除旧的图片
            $delImages= $goodsImagesModel->del($data['id']);

            if ($delImages===false){
                DB::rollBack();
                \Log::error('删除旧商品图片失败');
                return false;
            }
            //添加新的图片
            $goodsImagesData = [];
            foreach ($images as $img){
                $goodsImagesData[] = [
                    'goods_id'      => $data['id'],
                    'image'         => $img,
                    'thumbnail'     => $img,
                ];
            }
            if(
            !DB::table($goodsImagesModel->getTable())->insert($goodsImagesData)
            ){
                DB::rollBack();
                \Log::error('添加商品图片失败');
                return false;
            }

            //修改规格
            //删除旧的规格
            $delSku         = GoodsSku::where('goods_id', $data['id'])->delete();
            $delSkuName     = GoodsSkuName::where('goods_id', $data['id'])->delete();
            $delSkuValue    = GoodsSkuValue::where('goods_id', $data['id'])->delete();
            if(
                $delSkuValue ===false
                ||
                $delSkuName===false
                ||
                $delSku===false
            ){
                DB::rollBack();
                \Log::error('添加商品规格失败');
                return false;
            }
            //添加新规格
            $combinationSku = $this->combinationSku($data['id'], $sku, $skuData);
            if(
                !DB::table($goodsSkuNameModel->getTable())->insert($combinationSku['name'])
                ||
                !DB::table($goodsSkuValueModel->getTable())->insert($combinationSku['value'])
                ||
                !DB::table($goodsSkuModel->getTable())->insert($combinationSku['sku'])
            ){
                DB::rollBack();
                \Log::error('添加商品规格失败');
                return false;
            }


            //修改商品的标签数据
            //删除旧的标签
            $delTags = DB::table($goodsTagsMiddleModel->getTable())->where('goods_id', $data['id'])->delete();

            if ($delTags===false){
                DB::rollBack();
                \Log::error('删除商品旧标签失败');
                return false;
            }
            //添加新的标签
            $goodsTagsData = [];
            foreach ($tags as $v){
                $goodsTagsData[] = [
                    'goods_id'      => $data['id'],
                    'tag_id'        => $v,
                ];
            }
            if(
            !DB::table($goodsTagsMiddleModel->getTable())->insert($goodsTagsData)
            ){
                DB::rollBack();
                \Log::error('添加商品标签失败');
                return false;
            }

            //秒杀
            $isSpike = DB::table($spikeModel->getTable())->where('goods_id',$data['id'])->get();
            if (!$isSpike->isEmpty()){
                if (
                !DB::table($spikeModel->getTable())->where('goods_id', $data['id'])->update([
                    'delete_time' => date('Y-m-d H:i:s', time())
                ])
                ) {
                    DB::rollBack();
                    \Log::debug(1);
                    return false;
                }
            }



            if (isset($spike['is_spike']) && $spike['is_spike'] === true) {


                foreach ($spike['spikeData'] as &$value) {
                    $value['create_time'] = date('Y-m-d H:i:s', time());
                    $value['selling_price'] = $value['selling_price'] * 100;
                }

                if (
                !DB::table($spikeModel->getTable())->insert($spike['spikeData'])
                ) {
                    DB::rollBack();
                    \Log::debug(2);
                    return false;
                }

            }

            $isGroup = DB::table($groupBuyModel->getTable())->where('goods_id',$data['id'])
                ->whereNull('delete_time')
                ->get();
            //团购
            if (isset($spike['is_group']) && $group['is_group'] === true){

                if (!$isGroup->isEmpty()){
                    \Log::debug($group);
                    if (
                    DB::table($groupBuyModel->getTable())->where('goods_id',$data['id'])->update([
                        'people'            => $group['people'],
                        'selling_price'     => $group['group_selling_price']*100
                    ]) === false
                    ){
                        DB::rollBack();
                        \Log::debug(3);
                        return false;
                    }
                }else{
                    if (!DB::table($groupBuyModel->getTable())->insert([
                        'goods_id'  => $data['id'],
                        'people'    => $group['people']
                    ])){
                        DB::rollBack();
                        \Log::debug(5);
                        return false;
                    }

                }


            }else{

                if (!$isGroup->isEmpty()){
                    if (
                    !DB::table($groupBuyModel->getTable())->where('goods_id',$data['id'])->update([
                        'delete_time' => date('Y-m-d H:i:s',time())
                    ])
                    ){
                        DB::rollBack();
                        \Log::debug(4);
                        return false;
                    }
                }


            }


            DB::commit();
            return $data['id'];
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }

    /**
     * 新华联修改商品
     * @param $data
     * @param $sku
     * @param $skuData
     * @param $images
     * @param $tags
     * @param $spike
     * @param $group
     * @return bool
     */
    public function goodsSave($data, $sku, $skuData, $images, $tags, $spike, $group, $bargain){
        $goodsModel             = new Goods();
        $goodsImagesModel       = new GoodsImages();
        $goodsSkuModel          = new GoodsSku();
        $goodsSkuValueModel     = new GoodsSkuValue();
        $goodsSkuNameModel      = new GoodsSkuName();
        $goodsTagsMiddleModel   = new GoodsTagsMiddle();
        $spikeModel             = new Spike();

        try{
            DB::beginTransaction();

            /**
             * 修改商品基本嫩信息
             */
            if(DB::table($goodsModel->getTable())->where('id', $data['id'])->update($data) === false){
                \Log::debug('商品基本信息修改失败');
                DB::rollBack();
                return false;
            }

            /**
             * 修改商品图片
             */
            $delImages= $goodsImagesModel->del($data['id']);
            if ($delImages===false){
                DB::rollBack();
                \Log::error('删除旧商品图片失败');
                return false;
            }

            $goodsImagesData = [];
            foreach ($images as $img){
                $goodsImagesData[] = [
                    'goods_id'      => $data['id'],
                    'image'         => $img,
                    'thumbnail'     => $img,
                ];
            }
            if(
            !DB::table($goodsImagesModel->getTable())->insert($goodsImagesData)
            ){
                DB::rollBack();
                \Log::error('添加商品图片失败');
                return false;
            }

            /**
             * 修改商品规格
             */
            $delSku         = GoodsSku::where('goods_id', $data['id'])->delete();
            $delSkuName     = GoodsSkuName::where('goods_id', $data['id'])->delete();
            $delSkuValue    = GoodsSkuValue::where('goods_id', $data['id'])->delete();
            if(
                $delSkuValue ===false
                ||
                $delSkuName===false
                ||
                $delSku===false
            ){
                DB::rollBack();
                \Log::error('添加商品规格失败');
                return false;
            }

            if (isset($data['enable_sku']) && !empty($data['enable_sku'])){
                //添加新规格
                $combinationSku = $this->combinationSku($data['id'], $sku, $skuData);
                if(
                    !DB::table($goodsSkuNameModel->getTable())->insert($combinationSku['name'])
                    ||
                    !DB::table($goodsSkuValueModel->getTable())->insert($combinationSku['value'])
                    ||
                    !DB::table($goodsSkuModel->getTable())->insert($combinationSku['sku'])
                ){
                    DB::rollBack();
                    \Log::error('添加商品规格失败');
                    return false;
                }
            }


            /**
             * 商品标签
             */
            $delTags = DB::table($goodsTagsMiddleModel->getTable())->where('goods_id', $data['id'])->delete();

            if ($delTags===false){
                DB::rollBack();
                \Log::error('删除商品旧标签失败');
                return false;
            }
            //添加新的标签
            if (count($tags) > 0){
                $goodsTagsData = [];
                foreach ($tags as $v){
                    $goodsTagsData[] = [
                        'goods_id'      => $data['id'],
                        'tag_id'        => $v,
                    ];
                }
                if(
                !DB::table($goodsTagsMiddleModel->getTable())->insert($goodsTagsData)
                ){
                    DB::rollBack();
                    \Log::error('添加商品标签失败');
                    return false;
                }
            }
            /**
             * 秒杀
             */
            if (
                DB::table($spikeModel->getTable())->where('goods_id', $data['id'])->delete() === false
            ) {
                DB::rollBack();
                \Log::debug('删除秒杀商品失败');
                return false;
            }

            if (isset($spike['is_spike']) && $spike['is_spike'] === true) {

                foreach ($spike['spikeData'] as &$value) {
                    $value['create_time'] = date('Y-m-d H:i:s', time());
                    $value['selling_price'] = $value['selling_price'] * 100;
                }

                if (
                !DB::table($spikeModel->getTable())->insert($spike['spikeData'])
                ) {
                    DB::rollBack();
                    \Log::debug('添加秒杀商品失败');
                    return false;
                }

            }

            /**
             * 拼团
             */

            $goodsGroupModel        = new GoodsGroupBuy();
            if (isset($group['is_group']) && $group['is_group'] === true){
                if (!DB::table($goodsGroupModel->getTable())->insert([
                    'goods_id'          => $data['id'],
                    'original_price'    => $data['selling_price'],
                    'selling_price'     => $group['group_selling_price'] * 100,
                    'total_num'         => $group['total_num'],
                    'total_sum'         => $group['total_sum'],
                    'time_limit'        => $group['time_limit']
                ])){
                    DB::rollBack();
                    \Log::debug('修改商品拼团失败');
                    return false;
                }
            }else{
                if (
                    DB::table($goodsGroupModel->getTable())->where('goods_id',$data['id'])->delete() === false
                ){
                    DB::rollBack();
                    \Log::debug('删除拼团数据失败');
                    return false;
                }
            }

            //砍价
            $goodsBargainModel      = new GoodsBargain();
            if (isset($bargain['is_bargain']) && $bargain['is_bargain']){
                $bargainData = DB::table($goodsBargainModel->getTable())
                    ->where('goods_id', $data['id'])
                    ->first();

                if ($bargainData){
                    $bargainSaveData = [
                        'reserve_price'     => $bargain['reserve_price']*100,
                        'time_limit'        => $bargain['time_limit'],
                        'total_num'         => $bargain['total_num'],
                        'total_sum'         => $bargain['total_sum']
                    ];

                    if(DB::table($goodsBargainModel->getTable())
                            ->where('goods_id', $data['id'])
                            ->update($bargainSaveData) === false
                    ){
                        DB::rollBack();
                        \Log::debug('修改商品砍价失败');
                        return false;
                    }
                }else{

                    $bargainSaveData = [
                        'reserve_price'     => $bargain['reserve_price']*100,
                        'time_limit'        => $bargain['time_limit'],
                        'total_num'         => $bargain['total_num'],
                        'total_sum'         => $bargain['total_sum'],
                        'goods_id'          => $data['id']
                    ];

                    if(DB::table($goodsBargainModel->getTable())
                            ->insert($bargainSaveData) === false
                    ){
                        DB::rollBack();
                        \Log::debug('添加商品砍价失败');
                        return false;
                    }
                }

            }else{
                if(DB::table($goodsBargainModel->getTable())
                        ->where('goods_id', $data['id'])
                        ->delete() === false
                ){
                    DB::rollBack();
                    \Log::debug('修改商品砍价失败');
                    return false;
                }
            }



            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error('修改商品失败原因：'.$exception);
            DB:: rollBack();
            return false;
        }

    }

    /**
     * 组合sku的name 和 value
     * @param $goodsId
     * @param array $sku
     * @param array $skuData
     */
    public function combinationSku($goodsId, array $sku, array $skuData){
        $skuNames   = [];
        $skuValues  = [];
        $skus       = [];
        for ($i = 0; $i < count($skuData); $i++){
            $nameId = $goodsId.'-'.($i+1);
            $skuNames[] = [
                'goods_id'      => $goodsId,
                'name'          => $skuData[$i]['sku'],
                'id'            => $nameId,
                'has_thumbnail' => $this->JudgeHasThumbnail($skuData[$i]['thumbnail'])
            ];

            for ($j = 0; $j < count($skuData[$i]['name']); $j++){
                $valueId = $nameId.'-'.($j+1);
                $skuValues[] = [
                    'id'        => $valueId,
                    'goods_id'  => $goodsId,
                    'name_id'   => $nameId,
                    'value'     => $skuData[$i]['name'][$j],
                    'thumbnail' => $skuData[$i]['thumbnail'][$j] ? $skuData[$i]['thumbnail'][$j] : null
                ];
            }
        }


        //组合sku
        $skuItemNum = 0;
        foreach ($sku as $skuItem) {
            $skus[$skuItemNum] = [
                'purchase_price'    => $skuItem['purchase_price']*100,
                'original_price'    => $skuItem['original_price']*100,
                'selling_price'     => $skuItem['selling_price']*100,
                'stock'         => $skuItem['stock'],
                'value_path'    => '',
                'goods_id'      => $goodsId
            ];

            foreach ($skuItem['value'] as $skuItemValue) {
                foreach ($skuValues as $skuValue) {
                    if ($skuItemValue === $skuValue['value']) {
                        $skus[$skuItemNum]['value_path'] .= $skuValue['id'].',';

                    }
                }
            }

            $skus[$skuItemNum]['value_path'] = substr($skus[$skuItemNum]['value_path'],0,strlen($skus[$skuItemNum]['value_path'])-1);

            $skuItemNum += 1;
        }

        return [
            'name'      => $skuNames,
            'value'     => $skuValues,
            'sku'       => $skus
        ];
    }

    private function JudgeHasThumbnail($data){
        foreach ($data as $value) {
            if ($value != null && $value != ''){
                return 1;
            }else{
                return 0;
            }
        }
    }

    /**
     * 后台获取商品分类
     * @param $classify
     * @return array
     */
    public function classifyList($classify){
        $pid = 0;
        $tree = $this->permissionsTree($classify, $pid);
        return $tree;
    }
    public function permissionsTree($data, $pid){

        $tree = [];
        foreach($data as $key => $value){
            if($value->parent_id == $pid){
                $value->children = $this->permissionsTree($data, $value->id);
                $tree[] = $value;

                unset($data[$key]);
            }
        }
        return $tree;
    }

    /**
     * 一键复制商品
     * @param $type
     * @param $id
     * @param $goodsId
     * @return bool
     */
    public function copyGoods($type, $id, $goodsId){
        $goodsModel             = new Goods();
        $goodsSkuModel          = new GoodsSku();
        $goodsSkuValueModel     = new GoodsSkuValue();
        $goodsSkuNameModel      = new GoodsSkuName();
        $goodsImagesModel       = new GoodsImages();
        //商品基本信息
        $goods = DB::table($goodsModel->getTable())->where('id', $goodsId)->first();
        //商品规格信息
        $goodsSku = DB::table($goodsSkuModel->getTable())->where('goods_id', $goodsId)->get()->toArray();
        $goodsSkuName = DB::table($goodsSkuNameModel->getTable())->where('goods_id', $goodsId)->get()->toArray();
        $goodsSkuValueName = DB::table($goodsSkuValueModel->getTable())->where('goods_id', $goodsId)->get()->toArray();
        $skuData = [];

        foreach ($goodsSkuName as $k=>$v){
            $skuData[$k]['sku'] = $v->name;
            $skuData[$k]['name'] = [];
            $skuData[$k]['thumbnail'] = [];
            foreach ($goodsSkuValueName as $key=>$val){
                if ($val->name_id == $v->id){
                    $skuData[$k]['name'][] = $val->value;
                    $skuData[$k]['thumbnail'][] = $val->thumbnail;
                }

            }
        }
        $skuValueData = [];
        foreach ($goodsSku as $k=>$v){
            $skuValueData[$k]['value'] = [];
            $skuValueData[$k]['original_price'] = $v->original_price;
            $skuValueData[$k]['selling_price'] = $v->selling_price;
            $skuValueData[$k]['purchase_price'] = $v->purchase_price;
            $skuValueData[$k]['stock'] = $v->stock;
            foreach ($goodsSkuValueName as $key=>$val){
                if (strpos($v->value_path, $val->id) !== false){
                    $skuValueData[$k]['value'][] = $val->value;

                }

            }
        }

        //商品图片信息
        $goodsImage = DB::table($goodsImagesModel->getTable())->where('goods_id', $goodsId)->get()->toArray();
        $images = [];
        foreach ($goodsImage as $key=>$val){
            $images[] = $val->image;
        }
        $data = [
            'name' => $goods->name,
            'slogan' => $goods->slogan,
            'thumbnail' => $goods->thumbnail,
            'selling_price' => $goods->selling_price,
            'purchase_price' => $goods->purchase_price,
            'original_price' => $goods->original_price,
            'stock' => $goods->stock,
            'detail' => $goods->detail,
            'sort' => $goods->sort,
            'classify_id' => $goods->classify_id,
            'status' => 2,
            'enable_sku' => $goods->enable_sku,
            'store_nav_id' => 0,
            'company_id' => $goods->company_id,
            'create_time' => date('Y-m-d H:i:s', time()),
            'params' => $goods->params,
            'service' => $goods->service
        ];
        //查看是什么身份的人复制商品
        if ($type == 'agent'){
            $data['agent_id'] = 0;
            $data['p_id'] = $goods->stores_id;
            $data['mill_id'] = $goods->mill_id;
            $data['stores_id'] = $id;

        }elseif ($type == 'store'){
            $data['agent_id'] = 0;
            $data['p_id'] = 0;
            $data['mill_id'] = 0;
            $data['stores_id'] = $id;
        }
        $goodsId    = $this->addGoods($data, $skuValueData, $skuData, $images, false, false, []);
        return $goodsId;

    }

    /**
     * 获取分类商品列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function classifyGoods($search, $page = 1, $pageSize = 3, $classify){
        foreach ($classify as $k=>$v){
            $search['cid'] = $v->id;
            $sql = $this->__goodsSql($search);
            $goods = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
                ->orderBy('g.id')
                ->get();
            $v['goods'] = $goods;
        }
        return $classify;
    }


    /**
     * 收藏商品(蓝丝羽app端)
     * @param $data
     * @param $type
     * @return bool
     */
    public function collectGood($data, $type){
        $collectMillGoodModel = new CollectMillGoods();
        if ($type==0){
            $res = DB::table($collectMillGoodModel->getTable())
                ->where('goods_id', $data['goods_id'])
                ->where('store_id', $data['store_id'])
                ->delete();

        }else{
            $res = DB::table($collectMillGoodModel->getTable())->insert($data);
        }
        return $res;
    }

    public function exchangeGoods($id, $userId){
        $userModel                      = new Users();
        $storeCouponModel               = new StoreCoupons();
        $userCouponModel                = new UserCoupons();
        $userIntegralModel              = new UserIntegralLog();

        /**
         * 积分商品信息
         */
        $goodsInfo      = DB::table($storeCouponModel->getTable())
            ->where('id', $id)
            ->first();
        /**
         * 用户积分
         */
        $userInfo   = DB::table($userModel->getTable())
            ->select('level', 'integral')
            ->where('id', $userId)
            ->first();

        if (!$goodsInfo){
            return -1;
        }

        if ($userInfo->integral < $goodsInfo->selling_price){
            return -2;
        }

        if ($goodsInfo->quantity < 1){
            return -3;
        }

        try{
            DB::beginTransaction();
            //减去用户积分
            if (
                DB::table($userModel->getTable())
                    ->where('id', $userId)
                    ->decrement('integral', $goodsInfo->selling_price) === false
            ){
                DB::rollBack();
                \Log::debug('兑换积分商品减去用户积分失败');
                return false;
            }
            //添加兑换记录
            if(
                !DB::table($userCouponModel->getTable())
                ->insert([
                    'users_id'          => $userId,
                    'coupon_id'         => $id,
                    'start_time'        => date('Y-m-d H:i:s', time()),
                    'end_time'          => date('Y-m-d H:i:s', time()+$goodsInfo->term_of_validity*60*60)
                ])
            ){
                DB::rollBack();
                \Log::debug('添加用户优惠券失败');
                return false;
            }
            //添加用户积分日志
            if(
                !DB::table($userIntegralModel->getTable())
                ->insert([
                    'users_id'      => $userId,
                    'vary'          => 2,
                    'integral'      => $goodsInfo->selling_price,
                    'source'        => "兑换积分商品{$goodsInfo->name}",
                    'create_time'   => date('Y-m-d H:i:s', time())
                ])
            ){
                DB::rollBack();
                \Log::debug('兑换积分商品添加用户积分日志失败');
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error('兑换积分商品失败：'.$exception);
            return false;
        }
    }

    public function getSpike($day, $hour){
        $goodsModel     = new Goods();
        $spikeModel     = new Spike();
        return DB::table($spikeModel->getTable().' as s')
            ->select(
                'g.id',
                DB::raw("convert(jl_s.selling_price/100, decimal(15,2)) as selling_price"),
                DB::raw("convert(jl_g.original_price/100, decimal(15,2)) as original_price"),
                'g.name',
                'g.thumbnail'
            )
            ->leftJoin($goodsModel->getTable().' as g', 's.goods_id', '=', 'g.id')
            ->where('day', $day)
            ->where('hour', $hour)
            ->get();


    }

    /**
     * 推荐商品
     * @param $userId
     * @param $storeTypeId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function recommendGoods($userId, $storeTypeId, $page, $pageSize){
        $orderModel             = new Orders();
        $orderGoodsModel        = new OrderGoods();
        $goodsModel             = new Goods();
        $storeModel             = new Stores();


        $goodsClass = DB::table($orderGoodsModel->getTable().' as og')
            ->select('g.classify_id')
            ->leftJoin($orderModel->getTable().' as o', 'o.id', '=', 'og.orders_id')
            ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'og.goods_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'o.stores_id')
            ->where('s.store_type_id', $storeTypeId)
            ->where('o.users_id', $userId)
            ->groupBy('g.classify_id')
            ->groupBy('o.stores_id')
            ->get();

        $classId = [];
        foreach ($goodsClass as $value) {
            $classId[] = $value->classify_id;
        }

        if (empty($classId)){
            $search['store_type'] = [$storeTypeId];
            $list           = $this->goodsList($search, $page, $pageSize);
            $pagination     = $this->goodsListPagination($search, $page, $pageSize);
            return [
                'list'          => $list,
                'pagination'    => $pagination
            ];

        }else{
            $sql = DB::table($goodsModel->getTable())
                ->select(
                    'id',
                    'name',
                    'thumbnail',
                    DB::raw("convert(selling_price/100, decimal(15,2)) as selling_price"),
                    'people_volume'
                )
                ->whereIn('classify_id', $classId);

            $total = $sql->count();
            return [
                'list'          => $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get(),
                'pagination'    => [
                    'current'   => $page,
                    'pageSize'  => $pageSize,
                    'total'     => $total
                ]
            ];
        }


    }

}
