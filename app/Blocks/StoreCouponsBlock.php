<?php
namespace App\Blocks;


use App\Models\StoreCoupons;
use App\Models\StoreOfflineCoupons;
use App\Models\Stores;
use Illuminate\Support\Facades\DB;


class StoreCouponsBlock{
    private function couponsSql($search){
        $storeCouponsModel = new StoreCoupons();
        $sql = DB::table($storeCouponsModel->getTable())
            ->whereNull('delete_time');
        if(isset($search['name'])){
            $sql->where('name', 'like', "%".$search['name']."%");
        }
        if(isset($search['id'])){
            $sql->where('id', $search['id']);
        }
        if(isset($search['store_id'])){
            if ($search['store_id'] < 1){
                $sql->where('belong', 1);
            }else{
                $sql->where('stores_id', $search['store_id'])
                    ->where('belong', 2);
            }

        }

        if(isset($search['scope']) && !empty($search['scope']) && $search['scope'] == '余额'){
            $sql->where('scope', $search['scope']);
        }else{
            $sql->where('scope', '!=', '余额');
        }

        if(isset($search['company'])){
            $sql->where('company_id', $search['company']);
        }

        if(isset($search['admins_id'])){
            $sql->where('admins_id', $search['admins_id']);
        }

        return $sql;
    }
    /**
     * 获取商户优惠券列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeCouponsList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->couponsSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeTags = $sql->orderBy('id', 'desc')->get();

        return $storeTags;
    }

    /**
     * 获取商户优惠券列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeCouponsListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->couponsSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户优惠券详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeCouponsInfo($search){
        $sql = $this->couponsSql($search);
        $storeTags = $sql->first();
        return $storeTags;
    }

    /**
     * 获取注册礼包优惠券列表
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function offlineCouponsList($page, $pageSize){
        $offlineCouponModel         = new StoreOfflineCoupons();
        $storeModel                 = new Stores();

        $sql = DB::table($offlineCouponModel->getTable().' as so')
            ->select(
                's.name as stores_name',
                'so.id',
                'so.name',
                DB::raw("convert(discount_amount/100, decimal(15,2)) as discount_amount"),
                'so.rule',
                'so.effective_time',
                'so.create_time',
                'so.update_time'
            )
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'so.stores_id')
            ->whereNull('so.delete_time');

        $total = $sql->count();
        $list  = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

        return [
            'list'              => $list,
            'pagination'        => [
                'total'         => $total,
                'current'       => $page,
                'pageSize'      => $pageSize
            ]
        ];

    }

    /**
     * 修改或者添加线下优惠券（注册礼包）
     * @param $id
     * @param $data
     * @return bool|int
     */
    public function saveOfflineCoupons($id, $data){
        $offlineCouponModel         = new StoreOfflineCoupons();
        if (!empty($id)){
            return DB::table($offlineCouponModel->getTable())
                ->where('id', $id)
                ->update($data);
        }else{
            $data['create_time']    = date('Y-m-d H:i:s', time());
            return DB::table($offlineCouponModel->getTable())
                ->insert($data);
        }

    }

    /**
     * 删除线下优惠券（注册礼包）
     * @param $id
     * @return int
     */
    public function removeOfflineCoupon($id){
        $offlineCouponModel         = new StoreOfflineCoupons();
        return DB::table($offlineCouponModel->getTable())
            ->where('id', $id)
            ->update([
                'delete_time'       => date('Y-m-d H:i:s', time())
            ]);
    }

}
