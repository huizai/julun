<?php
namespace App\Blocks;


use App\Libs\DianWoDa;
use App\Models\AgentTurnover;
use App\Models\BalancePay;
use App\Models\Goods;
use App\Models\OrderCoupons;
use App\Models\OrderGoods;
use App\Models\OrderGroupBuy;
use App\Models\OrderGroupBuyLog;
use App\Models\OrderRefund;
use App\Models\Orders;
use App\Models\ParkingOrder;
use App\Models\StoreCoupons;
use App\Models\StoreIncomeLog;
use App\Models\Stores;
use App\Models\Turnover;
use App\Models\UserCoupons;
use App\Models\UserIntegralLog;
use App\Models\UserRechargeLog;
use App\Models\Users;
use App\Models\UserWalletLog;
use App\Models\WechatPay;
use App\Models\WechatPayRefund;
use App\Models\WechatUsers;
use Illuminate\Support\Facades\DB;
use App\Libs\ZhongxinPay\ClientResponseHandler;
use App\Libs\ZhongxinPay\PayHttpClient;
use App\Libs\ZhongxinPay\RequestHandler;
use App\Libs\ZhongxinPay\Utils;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use function EasyWeChat\Kernel\Support\get_server_ip;
use EasyWeChat\Factory;
use Yansongda\Pay\Pay;
use App\Models\Alipay;
use App\Libs\Ketuo;
use App\Blocks\UserBlock;

class PayBlock{
    /**
     * 获取小程序中信微信支付的对象
     * @return \stdClass
     */
    private function __getZhongXinWechatPayObj(){
        $payObject = new \stdClass();

        $payObject->resHandler = new ClientResponseHandler();
        $payObject->reqHandler = new RequestHandler();
        $payObject->pay = new PayHttpClient();

        $payObject->reqHandler->setGateUrl(\Config::get('zhongxin-pay.mini.url'));

        $sign_type = \Config::get('zhongxin-pay.mini.sign_type');

        if ($sign_type == 'MD5') {
            $payObject->reqHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
            $payObject->resHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
            $payObject->reqHandler->setSignType($sign_type);
        } else if ($sign_type == 'RSA_1_1' || $sign_type == 'RSA_1_256') {
            $payObject->reqHandler->setRSAKey(file_get_contents(\Config::get('zhongxin-pay.mini.private_rsa_key')));
            $payObject->resHandler->setRSAKey(file_get_contents(\Config::get('zhongxin-pay.mini.public_rsa_key')));
            $payObject->reqHandler->setSignType($sign_type);
        }

        return $payObject;
    }

    /**
     * 订单App微信支付
     * @param $userId
     * @param $batch
     * @param $type
     * @param $orderId
     * @return bool|int|mixed
     */
    public function orderWeChatAppPay($userId, $batch, $orderId, $type){
        $wechatPayModel = new WechatPay();
        $orderBlock     = new OrderBlock();
        $orderModel     = new Orders();
        $search = [
            'users_id'  => $userId
        ];
        $outTradeNo = md5($batch . time());
        $time       = date('Y-m-d H:i:s', time());
        $weChatPayData = [];
        $payAmount  = 0;
        $openid     = '';
        if (!empty($batch)){
            $search['batch']        = $batch;
            $order = $orderBlock->orderWechatPayInfo($search);
            foreach ($order as $key => $item) {
                if($item->status !== 0){
                    return -2;
                }

                if ($item->is_use === 1){
                    return -4;
                }

                $weChatPayData[$key]['batch']           = $item->batch;
                $weChatPayData[$key]['out_trade_no']    = $outTradeNo;
                $weChatPayData[$key]['create_time']     = $time;
                $weChatPayData[$key]['orders_id']       = $item->id;
                $weChatPayData[$key]['type']            = 1;
                $payAmount += $item->pay_amount;
                $openid = $item->openid;
            }
        }

        if (!empty($orderId)){
            $search['orders_id']        = $orderId;
            $order      = $orderBlock->orderWechatPayInfo($search);
            $openid     = '';
            if($order->status !== 0){
                return -2;
            }

            if ($order->is_use === 1){
                return -4;
            }
            $payAmount = $order->pay_amount;
            $openid    = $order->openid;
        }

        if(!$order){
            return -1;
        }

        $unify = [
            'body' => '新华联梦想城福街消费',
            'out_trade_no' => $outTradeNo,
            'total_fee' => $payAmount,
            'spbill_create_ip' => get_server_ip(),
            'notify_url' => env('APP_URL').'/pluto/order/pay/wechat/app/notify'
        ];


        if ($type == 'app'){
            $unify['trade_type'] = 'APP';
            $app = Factory::payment(\Config::get('zhongxin-pay.app'));
        }else if ($type == 'pub'){
            $unify['trade_type'] = 'JSAPI';
            $unify['openid']     = $openid;
            $app = Factory::payment(\Config::get('zhongxin-pay.pub'));
        }

        $result = $app->order->unify($unify);

        try{
            if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS'){
                DB::beginTransaction();
                //添加微信支付記錄，修改訂單支付模式
                if(isset($search['batch'])){
                    if(
                        DB::table($wechatPayModel->getTable())
                            ->insert($weChatPayData) === false
                    ){
                        DB::rollBack();
                        \Log::error('支付-生成微信支付配置数据-错误:保存out_trade_no失败');
                        return -3;
                    }

                    if (DB::table($orderModel->getTable())
                            ->where('batch', $search['batch'])
                            ->update([
                                'pay_mode' => 1
                            ]) === false
                    ){
                        DB::rollBack();
                        \Log::error('支付-生成微信支付配置数据-错误:修改pay_mode失败');
                        return -3;
                    }
                }

                //修改微信支付記錄訂單號，修改訂單支付模式
                if(isset($search['orders_id'])){
                    if (DB::table($orderModel->getTable())
                            ->where('id', $search['orders_id'])
                            ->update([
                                'pay_mode' => 2
                            ]) === false
                    ){
                        DB::rollBack();
                        \Log::error('支付-生成微信支付配置数据-错误:修改pay_mode失败');
                        return -3;
                    }
                    \Log::debug(json_encode($search));
                    if(DB::table($wechatPayModel->getTable())
                        ->where('orders_id', $search['orders_id'])
                        ->first())
                    {
                        if (!DB::table($wechatPayModel->getTable())
                            ->where('orders_id', $search['orders_id'])
                            ->update([
                                'out_trade_no' => $outTradeNo
                            ])
                        ){
                            DB::rollBack();
                            \Log::error('支付-生成微信支付配置数据-错误:修改out_trade_no失败');
                            return -3;
                        }
                    }else{
                        if(!DB::table($wechatPayModel->getTable())->insert([
                            'orders_id' => $search['orders_id'],
                            'out_trade_no' => $outTradeNo,
                            'batch' => DB::table($orderModel->getTable())->where('id', $search['orders_id'])->value('batch')
                        ])){
                            DB::rollBack();
                            \Log::error('支付-生成微信支付配置数据-错误:修改out_trade_no失败');
                            return -3;
                        }
                    }

                }
                DB::commit();
                if ($type == 'pub'){
                    return $app->jssdk->sdkConfig($result['prepay_id']);
                }
                return $app->jssdk->appConfig($result['prepay_id']);
            }else{
                return false;
            }
        }catch (\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return false;
        }


    }

    /**
     * 订单APP微信支付回调
     * @param $app
     * @return mixed
     */
    public function orderWeChatAppPayNotify($app){
        $response = $app->handlePaidNotify(function ($message, $fail) {
            $orderModel         = new Orders();
            $wechatPayModel     = new WechatPay();
            $orderBlock         = new OrderBlock();
            $orderCouponModel   = new OrderCoupons();
            $userCouponModel    = new UserCoupons();
            $goodsModel         = new Goods();
            $orderGoodsModel    = new OrderGoods();
            $dianWoDa           = new DianWoDa();

            $order = $orderBlock->orderWechatPayInfo([
                'out_trade_no'  => $message['out_trade_no'],
            ]);

            if(!$order){
                \Log::error("订单支付会调:订单不存在,out_trade_no:{$message['out_trade_no']}");
                return -1;
            }

            if($order->status !== 0){
                \Log::error("订单支付会调:已支付,out_trade_no:{$message['out_trade_no']}");
                return -2;
            }


            if ($message['return_code'] === 'SUCCESS') {
                if (array_get($message, 'result_code') === 'SUCCESS') {
                    try{

                        DB::beginTransaction();

                        $sql = DB::table($orderCouponModel->getTable().' as oc')
                            ->select('oc.user_coupon')
                            ->leftJoin($orderModel->getTable().' as o', 'o.id', '=', 'oc.orders_id');

                        if ($order->pay_mode == 1){
                            $batchOrder  = $orderBlock->orderWechatPayInfo(['batch' => $order->batch]);
                            $orderId = [];
                            $storeType = 0;
                            foreach ($batchOrder as $item) {
                                $orderId[]   = $item->id;
                                $storeType = $item->store_type_id;
                                if ($item->store_type_id == 1){
                                    $orderGoods = DB::table($orderGoodsModel->getTable())
                                        ->select('name as goods_name', 'selling_price', 'quantity')
                                        ->where('orders_id', $item->id)
                                        ->get();
                                    $createResult = $dianWoDa->orderCreate(
                                        [
                                            'batch'             => $order->batch,
                                            'order_remark'      => $order->remark,
                                            'pay_amount'        => $order->pay_amount,
                                            'stores_id'         => $order->stores_id,
                                            'store_name'        => $order->store_name,
                                            'mobile'            => $order->store_mobile,
                                            'address'           => $order->store_address,
                                            'lat'               => $order->store_latitude,
                                            'lng'               => $order->store_longitude
                                        ],
                                        [
                                            'name'              => $order->consignee,
                                            'mobile'            => $order->contact,
                                            'address'           => $order->user_address,
                                            'lat'               => $order->user_latitude,
                                            'lng'               => $order->user_longitude
                                        ],
                                        json_decode(json_encode($orderGoods), true)
                                    );
                                    \Log::debug('点我达订单推送结果'.json_encode($createResult));
                                }
                            }


                            $updateData = [
                                'status'    => 1,
                                'pay_type'  => 1,
                                'pay_time'  => date('Y-m-d H:i:s', time())
                            ];

                            if ($order->type == 1 && $order->store_type_id != 4){
                                $updateData['code'] = substr($order->batch, -6);
                            }
                            if ($storeType == 4){
                                $updateData['status']   = 2;
                                $updateData['code']     = getSalt();
                                $path = public_path().'/qrcode/junanzenorder/';
                                if (!is_dir($path)){
                                    @mkdir($path, 0755);
                                }
                                $imageName = $order->id.time().'.png';
                                $code       = getSalt();
                                $codeResult = QrCode::format('png')
                                    ->size(400)
                                    ->encoding('UTF-8')
                                    ->generate(
                                        $code,
                                        "{$path}/$imageName"
                                    );
                                if ($codeResult){
                                    $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                                }
                            }
                            $orderResult = DB::table($orderModel->getTable())
                                ->where('batch', $order->batch)
                                ->where('status', 0)
                                ->whereNull('delete_time')
                                ->update($updateData);
                            if($orderResult === false){
                                DB::rollBack();
                                \Log::error("订单支付会调:更新订单状态失败,out_trade_no:{$message['out_trade_no']}");
                                return false;
                            }

                            /**
                             * 修改商品销量库存
                             */
                            $goods = DB::table($orderGoodsModel->getTable())
                                ->select('goods_id', 'quantity')
                                ->whereIn('orders_id', $orderId)
                                ->get();

                            foreach ($goods as $good) {
                                if(!DB::table($goodsModel->getTable())
                                    ->where('id', $good->goods_id)
                                    ->update([
                                        'sales_volume'          => DB::raw("sales_volume + {$good->quantity}"),
                                        'mouth_sales_volume'    => DB::raw("mouth_sales_volume + {$good->quantity}"),
                                        'stock'                 => DB::raw("stock - {$good->quantity}")
                                    ])
                                ){
                                    DB::rollBack();
                                    \Log::error("修改商品销量库存失败");
                                    return false;
                                }
                            }

                            /**
                             * 修改优惠券状态
                             */
                            $coupon = $sql->where('orders_id', $orderId)->get();
                            if (!$coupon->isEmpty()){
                                $userCoupon = [];
                                foreach ($coupon as $item) {
                                    if (!empty($item->user_coupon)){
                                        $userCoupon[] = $item->user_coupon;
                                    }
                                }
                                if(!DB::table($userCouponModel->getTable())
                                    ->where('id', $userCoupon)
                                    ->update(['is_use' => 1])
                                ){
                                    DB::rollBack();
                                    \Log::error("修改优惠券状态失败");
                                    return false;
                                }
                            }


                        }else if($order->pay_mode == 2){
                            if ($order->store_type_id == 1){
                                $orderGoods = DB::table($orderGoodsModel->getTable())
                                    ->select('name as goods_name', 'selling_price', 'quantity')
                                    ->where('orders_id', $order->id)
                                    ->get();
                                $createResult = $dianWoDa->orderCreate(
                                    [
                                        'batch'             => $order->batch,
                                        'order_remark'      => $order->remark,
                                        'pay_amount'        => $order->pay_amount,
                                        'stores_id'         => $order->stores_id,
                                        'store_name'        => $order->store_name,
                                        'mobile'            => $order->store_mobile,
                                        'address'           => $order->store_address,
                                        'lat'               => $order->store_latitude,
                                        'lng'               => $order->store_longitude
                                    ],
                                    [
                                        'name'              => $order->consignee,
                                        'mobile'            => $order->contact,
                                        'address'           => $order->user_address,
                                        'lat'               => $order->user_latitude,
                                        'lng'               => $order->user_longitude
                                    ],
                                    json_decode(json_encode($orderGoods), true)
                                );
                                \Log::debug('点我达订单推送结果'.json_encode($createResult));
                            }


                            $updateData = [
                                'status'    => 1,
                                'pay_type'  => 1,
                                'pay_time'  => date('Y-m-d H:i:s', time())
                            ];
                            
                            if ($order->type == 1 && $order->store_type_id != 4){
                                $updateData['code'] = substr($order->batch, -6);
                            }

                            if ($order->type == 4){
                                $updateData['status']   = 2;
                                $updateData['code']     = getSalt();
                                $path = public_path().'/qrcode/junanzenorder/';
                                if (!is_dir($path)){
                                    @mkdir($path, 0755);
                                }
                                $imageName = $order->id.time().'.png';
                                $code       = getSalt();
                                $codeResult = QrCode::format('png')
                                    ->size(400)
                                    ->encoding('UTF-8')
                                    ->generate(
                                        $code,
                                        "{$path}/$imageName"
                                    );
                                if ($codeResult){
                                    $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                                }
                            }
                            $orderResult = DB::table($orderModel->getTable())
                                ->where('id', $order->orders_id)
                                ->where('status', 0)
                                ->whereNull('delete_time')
                                ->update($updateData);
                            if($orderResult === false){
                                DB::rollBack();
                                \Log::error("订单支付会调:更新订单状态失败,out_trade_no:{$message['out_trade_no']}");
                                return false;
                            }

                            /**
                             * 修改商品销量库存
                             */
                            $goods = DB::table($orderGoodsModel->getTable())
                                ->select('goods_id', 'quantity')
                                ->where('orders_id', $order->id)
                                ->get();
                            foreach ($goods as $good) {
                                if(!DB::table($goodsModel->getTable())
                                    ->where('id', $good->goods_id)
                                    ->update([
                                        'sales_volume'          => DB::raw("sales_volume + {$good->quantity}"),
                                        'mouth_sales_volume'    => DB::raw("mouth_sales_volume + {$good->quantity}"),
                                        'stock'                 => DB::raw("stock - {$good->quantity}")
                                    ])
                                ){
                                    DB::rollBack();
                                    \Log::error("修改商品销量库存失败");
                                    return false;
                                }
                            }

                            /**
                             * 修改优惠券状态
                             */
                            $coupon = $sql->where('orders_id', $order->id)->value('user_coupon');
                            \Log::debug($coupon);
                            if ($coupon){
                                if(!DB::table($userCouponModel->getTable())
                                    ->where('id', $coupon)
                                    ->update(['is_use' => 1])
                                ){
                                    DB::rollBack();
                                    \Log::error("修改优惠券状态失败");
                                    return false;
                                }
                            }
                        }
                        /**
                         * 修改微信支付日志信息
                         */
                        \Log::debug(json_encode($message));
                        if(
                            DB::table($wechatPayModel->getTable())
                                ->where('out_trade_no', $message['out_trade_no'])
                                ->whereNull('delete_time')
                                ->update([
                                    'transaction_id' => $message['transaction_id']
                                ]) === false
                        ){
                            DB::rollBack();
                            \Log::error("订单支付会调:更新wechat pay中的transaction_id失败,out_trade_no:{$message['out_trade_no']}");
                            return false;
                        }

//                        if ($order->store_type_id == 4){
//                            Redis::setex('orderTypeFour-cancel:'.json_encode($orderId), 300,'orderTypeFour-cancel');
//                        }

                        DB::commit();
                        return true;
                    }catch (\Exception $exception){
                        DB::rollBack();
                        \Log::error($exception);
                        return false;
                    }
                }elseif (array_get($message, 'result_code') === 'FAIL') {
                    return $fail('支付失败，请稍后再通知我');
                }

            }else {
                return $fail('通信失败，请稍后再通知我');
            }
        });

        return $response;
    }

    public function getBalanceScanCodePay($batch, $orderId){
        $orderModel         = new Orders();
        $balancePayModel    = new BalancePay();

        $outTradeNo = md5($batch . time());
        $orderSql   = DB::table($orderModel->getTable());

        if (!empty($batch)) {
            $orderSql->where('batch', $batch);
        }
        if (!empty($orderId)) {
            $orderSql->where('id', $orderId);
        }
        $orderInfo  = $orderSql->first();

        if (!$orderInfo){
            return -1;
        }
        if ($orderInfo->status != 0){
            return -2;
        }


        $pay = $this->__getZhongXinWechatPayObj();
        $pay->reqHandler->setParameter('out_trade_no', $outTradeNo);
        $pay->reqHandler->setParameter('body', '新华联梦想城福街消费');
        $pay->reqHandler->setParameter('total_fee', 1);
        $pay->reqHandler->setParameter('mch_create_ip', get_server_ip());
        $pay->reqHandler->setParameter('service','unified.trade.native');
        $pay->reqHandler->setParameter('mch_id',\Config::get('zhongxin-pay.mini.mchId'));
        $pay->reqHandler->setParameter('version',\Config::get('zhongxin-pay.mini.version'));
        $pay->reqHandler->setParameter('sign_type',\Config::get('zhongxin-pay.mini.sign_type'));
        $pay->reqHandler->setParameter('notify_url',\Config::get('app.url').'/pluto/order/balance/callback');
        $pay->reqHandler->setParameter('is_raw','1');
        $pay->reqHandler->setParameter('nonce_str',mt_rand());
        $pay->reqHandler->createSign();
        $data = Utils::toXml($pay->reqHandler->getAllParameters());
        $pay->pay->setReqContent($pay->reqHandler->getGateURL(),$data);

        if($pay->pay->call()) {
            $pay->resHandler->setContent($pay->pay->getResContent());
            $pay->resHandler->setKey($pay->reqHandler->getKey());
            $res = $pay->resHandler->getAllParameters();
            \Log::info(date("Y-m-d H:i:s",time()).'支付返回XML'.json_encode($res));

            if($pay->resHandler->isTenpaySign()){
                if(
                    $pay->resHandler->getParameter('status') == 0
                    &&
                    $pay->resHandler->getParameter('result_code') == 0
                ){
                    try{
                        DB::beginTransaction();
                        //添加微信支付記錄，修改訂單支付模式
                        if(!empty($batch)){
                            if(
                                DB::table($balancePayModel->getTable())
                                    ->insert([
                                        'orders_id'     => $orderInfo->id,
                                        'batch'         => $orderInfo->batch,
                                        'out_trade_no'  => $outTradeNo
                                    ]) === false
                            ){
                                DB::rollBack();
                                \Log::error('支付-生成余额支付配置数据-错误:保存out_trade_no失败');
                                return -3;
                            }

                            if (DB::table($orderModel->getTable())
                                    ->where('batch', $batch)
                                    ->update([
                                        'pay_mode' => 1
                                    ]) === false
                            ){
                                DB::rollBack();
                                \Log::error('支付-生成余额支付配置数据-错误:修改pay_mode失败');
                                return -3;
                            }
                        }

                        //修改微信支付記錄訂單號，修改訂單支付模式
                        if(!empty($orderId)){
                            if (DB::table($orderModel->getTable())
                                    ->where('id', $orderId)
                                    ->update([
                                        'pay_mode' => 2
                                    ]) === false
                            ){
                                DB::rollBack();
                                \Log::error('支付-生成余额支付配置数据-错误:修改pay_mode失败');
                                return -3;
                            }

                            if(DB::table($balancePayModel->getTable())
                                ->where('orders_id', $orderId)
                                ->first())
                            {
                                if (!DB::table($balancePayModel->getTable())
                                    ->where('orders_id', $orderId)
                                    ->update([
                                        'out_trade_no' => $outTradeNo
                                    ])
                                ){
                                    DB::rollBack();
                                    \Log::error('支付-生成余额支付配置数据-错误:修改out_trade_no失败');
                                    return -3;
                                }
                            }else{
                                if(!DB::table($balancePayModel->getTable())->insert([
                                    'orders_id' => $orderId,
                                    'out_trade_no' => $outTradeNo,
                                    'batch' => DB::table($orderModel->getTable())->where('id', $orderId)
                                                ->value('batch')
                                ])){
                                    DB::rollBack();
                                    \Log::error('支付-生成余额支付配置数据-错误:修改out_trade_no失败');
                                    return -3;
                                }
                            }

                        }
                        DB::commit();
                        return $res['code_img_url'];
                    }catch (\Exception $exception){
                        \Log::error($exception);
                        return false;
                    }

                }else{
                    \Log::error('1支付-生成余额支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
                    return false;
                }
            }else{
                \Log::error('2支付-生成余额支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
                return false;
            }
        }else{
            \Log::error('3支付-生成余额支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
            return false;
        }

    }

    public function balanceCallBack($outTradeno, $transactionId, $totalFee){
        $orderModel         = new Orders();
        $orderBlock         = new OrderBlock();
        $orderCouponModel   = new OrderCoupons();
        $userCouponModel    = new UserCoupons();
        $goodsModel         = new Goods();
        $orderGoodsModel    = new OrderGoods();
        $userModel          = new Users();
        $userIntegralModel  = new UserIntegralLog();
        $balancePayModel    = new BalancePay();

        $order      = DB::table($orderModel->getTable().' as o')
            ->select('o.*')
            ->leftJoin($balancePayModel->getTable().' as bp', 'bp.orders_id', '=', 'o.id')
            ->where('bp.out_trade_no', $outTradeno)
            ->first();

        //已经支付
        if($order->status !== 0){
            \Log::error("订单支付会调:已支付,out_trade_no:{$outTradeno}");
            return -2;
        }

        if(!$order){
            \Log::error("订单支付会调:订单不存在,out_trade_no:{$outTradeno}");
            return -1;
        }

        try{

            DB::beginTransaction();

            $sql = DB::table($orderCouponModel->getTable().' as oc')
                ->select('oc.user_coupon')
                ->leftJoin($orderModel->getTable().' as o', 'o.id', '=', 'oc.orders_id');

            $payAmount = 0;         //支付总费用
            if ($order->pay_mode == 1){
                $updateData = [
                    'status'    => 1,
                    'pay_type'  => 1,
                    'pay_time'  => date('Y-m-d H:i:s', time())
                ];
                if ($order->type == 1){
                    $updateData['code'] = substr($order->batch, -6);
                }
                $orderResult = DB::table($orderModel->getTable())
                    ->where('batch', $order->batch)
                    ->where('status', 0)
                    ->whereNull('delete_time')
                    ->update($updateData);
                if($orderResult === false){
                    DB::rollBack();
                    \Log::error("订单支付会调:更新订单状态失败,out_trade_no:{$outTradeno}");
                    return false;
                }

                $batchOrder  = $orderBlock->orderWechatPayInfo(['batch' => $order->batch]);
                $orderId = [];
                foreach ($batchOrder as $item) {
                    $orderId[] = $item->id;
                    $payAmount += $item->pay_amount;
                }

                $goods = DB::table($orderGoodsModel->getTable())
                    ->select('goods_id', 'quantity')
                    ->whereIn('orders_id', $orderId)
                    ->get();

                foreach ($goods as $good) {
                    if(!DB::table($goodsModel->getTable())
                        ->where('id', $good->goods_id)
                        ->update([
                            'sales_volume'          => DB::raw("sales_volume + {$good->quantity}"),
                            'mouth_sales_volume'    => DB::raw("mouth_sales_volume + {$good->quantity}"),
                            'stock'                 => DB::raw("stock - {$good->quantity}")
                        ])
                    ){
                        DB::rollBack();
                        \Log::error("修改销量库存失败");
                        return false;
                    }
                }
                /**
                 * 修改优惠券状态
                 */
                $coupon = $sql->where('orders_id', $orderId)->get();
                if (!$coupon->isEmpty()){
                    $userCoupon = [];
                    foreach ($coupon as $item) {
                        if (!empty($item->user_coupon)){
                            $userCoupon[] = $item->user_coupon;
                        }
                    }
                    if(!DB::table($userCouponModel->getTable())
                        ->where('id', $userCoupon)
                        ->update(['is_use' => 1])
                    ){
                        DB::rollBack();
                        \Log::error("修改优惠券状态失败");
                        return false;
                    }
                }


            }else if($order->pay_mode == 2){
                $payAmount = $order->pay_amount;
                $updateData = [
                    'status'    => 1,
                    'pay_type'  => 1,
                    'pay_time'  => date('Y-m-d H:i:s', time())
                ];
                if ($order->type == 1){
                    $updateData['code'] = substr($order->batch, -6);
                }
                $orderResult = DB::table($orderModel->getTable())
                    ->where('id', $order->orders_id)
                    ->where('status', 0)
                    ->whereNull('delete_time')
                    ->update($updateData);
                if($orderResult === false){
                    DB::rollBack();
                    \Log::error("订单支付会调:更新订单状态失败,out_trade_no:{$outTradeno}");
                    return false;
                }

                /**
                 * 修改销量库存
                 */
                $goods = DB::table($orderGoodsModel->getTable())
                    ->select('goods_id', 'quantity')
                    ->where('orders_id', $order->id)
                    ->get();

                foreach ($goods as $good) {
                    if(!DB::table($goodsModel->getTable())
                        ->where('id', $good->goods_id)
                        ->update([
                            'sales_volume'          => DB::raw("sales_volume + {$good->quantity}"),
                            'mouth_sales_volume'    => DB::raw("mouth_sales_volume + {$good->quantity}"),
                            'stock'                 => DB::raw("stock - {$good->quantity}")
                        ])
                    ){
                        DB::rollBack();
                        \Log::error("修改销量库存失败");
                        return false;
                    }
                }

                /**
                 * 修改优惠券状态
                 */
                $coupon = $sql->where('orders_id', $order->id)->value('user_coupon');
                if ($coupon){
                    if(!DB::table($userCouponModel->getTable())
                        ->where('id', $coupon)
                        ->update(['is_use' => 1])
                    ){
                        DB::rollBack();
                        \Log::error("修改优惠券状态失败");
                        return false;
                    }
                }
            }


            if(
                DB::table($balancePayModel->getTable())
                    ->where('out_trade_no', $outTradeno)
                    ->update([
                        'transaction_id' => $transactionId
                    ]) === false
            ){
                DB::rollBack();
                \Log::error("订单支付会调:更新wechat pay中的transaction_id失败,out_trade_no:{$outTradeno}");
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }

    /**
     * 订单微信支付（中信）
     * @param $userId
     * @param $batch
     * @return bool
     */
    public function wechatPayByZhongxin($userId, $batch, $type, $orderId){
        $wechatPayModel = new WechatPay();
        $orderBlock     = new OrderBlock();
        $orderModel     = new Orders();
        $search = [
            'users_id'  => $userId
        ];
        $outTradeNo = md5($batch . time());
        $time       = date('Y-m-d H:i:s', time());
        $weChatPayData = [];
        $payAmount = 0;
        if (!empty($batch)){
            $search['batch']        = $batch;
            $order = $orderBlock->orderWechatPayInfo($search);
            foreach ($order as $key => $item) {
                if($item->status !== 0){
                    return -2;
                }

                if ($item->is_use === 1){
                    return -4;
                }
                $openid                                 = $item->openid;
                $weChatPayData[$key]['batch']           = $item->batch;
                $weChatPayData[$key]['out_trade_no']    = $outTradeNo;
                $weChatPayData[$key]['create_time']     = $time;
                $weChatPayData[$key]['orders_id']       = $item->id;
                $weChatPayData[$key]['type']            = 1;
                $payAmount += $item->pay_amount;
            }
        }

        if (!empty($orderId)){
            $search['orders_id']        = $orderId;
            $order      = $orderBlock->orderWechatPayInfo($search);
            if($order->status !== 0){
                return -2;
            }

            if ($order->is_use === 1){
                return -4;
            }
            $openid     = $order->openid;
            $payAmount  = $order->pay_amount;
        }

        if(!$order){
            return -1;
        }

        $pay = $this->__getZhongXinWechatPayObj();
        $pay->reqHandler->setParameter('out_trade_no', $outTradeNo);
        $pay->reqHandler->setParameter('body', '新华联梦想城福街消费');
        $pay->reqHandler->setParameter('total_fee', $payAmount);
        $pay->reqHandler->setParameter('mch_create_ip', get_server_ip());

        $pay->reqHandler->setParameter('mch_id',\Config::get('zhongxin-pay.mini.mchId'));
        $pay->reqHandler->setParameter('version',\Config::get('zhongxin-pay.mini.version'));
        $pay->reqHandler->setParameter('sign_type',\Config::get('zhongxin-pay.mini.sign_type'));
        $pay->reqHandler->setParameter('notify_url',\Config::get('app.url').'/pluto/order/pay/zhongxin/callback?company=1');
        $pay->reqHandler->setParameter('sub_openid', $openid);
        $pay->reqHandler->setParameter('is_raw','1');
        if ($type == 'h5'){
            $pay->reqHandler->setParameter('service','pay.weixin.wappay');
        }else{
            $pay->reqHandler->setParameter('service','pay.weixin.jspay');
        }

        if($type == 'pub'){
            $pay->reqHandler->setParameter('sub_appid','wxda61a3b58bb9983c');
        }else if($type == 'mini'){
            $pay->reqHandler->setParameter('sub_appid','wx10cb0441bd039cfb');
        }

        $pay->reqHandler->setParameter('nonce_str',mt_rand());
        $pay->reqHandler->createSign();
        $data = Utils::toXml($pay->reqHandler->getAllParameters());
        $pay->pay->setReqContent($pay->reqHandler->getGateURL(),$data);

        if($pay->pay->call()){
            $pay->resHandler->setContent($pay->pay->getResContent());
            $pay->resHandler->setKey($pay->reqHandler->getKey());
            $res = $pay->resHandler->getAllParameters();

            \Log::info(date("Y-m-d H:i:s",time()).'支付返回XML'.json_encode($res));
            if($pay->resHandler->isTenpaySign()){
                if(
                    $pay->resHandler->getParameter('status') == 0
                    &&
                    $pay->resHandler->getParameter('result_code') == 0
                ){
                    //更新订单支付信息

                    try{
                        DB::beginTransaction();
                        //添加微信支付記錄，修改訂單支付模式
                        if(isset($search['batch'])){
                            if(
                                DB::table($wechatPayModel->getTable())
                                    ->insert($weChatPayData) === false
                            ){
                                DB::rollBack();
                                \Log::error('支付-生成微信支付配置数据-错误:保存out_trade_no失败');
                                return -3;
                            }

                            if (DB::table($orderModel->getTable())
                                    ->where('batch', $search['batch'])
                                    ->update([
                                        'pay_mode' => 1
                                    ]) === false
                            ){
                                DB::rollBack();
                                \Log::error('支付-生成微信支付配置数据-错误:修改pay_mode失败');
                                return -3;
                            }
                        }

                        //修改微信支付記錄訂單號，修改訂單支付模式
                        if(isset($search['orders_id'])){
                            if (DB::table($orderModel->getTable())
                                    ->where('id', $search['orders_id'])
                                    ->update([
                                        'pay_mode' => 2
                                    ]) === false
                            ){
                                DB::rollBack();
                                \Log::error('支付-生成微信支付配置数据-错误:修改pay_mode失败');
                                return -3;
                            }
                            \Log::debug(json_encode($search));
                            if(DB::table($wechatPayModel->getTable())
                                ->where('orders_id', $search['orders_id'])
                                ->first())
                            {
                                if (!DB::table($wechatPayModel->getTable())
                                    ->where('orders_id', $search['orders_id'])
                                    ->update([
                                        'out_trade_no' => $outTradeNo
                                    ])
                                ){
                                    DB::rollBack();
                                    \Log::error('支付-生成微信支付配置数据-错误:修改out_trade_no失败');
                                    return -3;
                                }
                            }else{
                                if(!DB::table($wechatPayModel->getTable())->insert([
                                    'orders_id' => $search['orders_id'],
                                    'out_trade_no' => $outTradeNo,
                                    'batch' => DB::table($orderModel->getTable())->where('id', $search['orders_id'])->value('batch')
                                ])){
                                    DB::rollBack();
                                    \Log::error('支付-生成微信支付配置数据-错误:修改out_trade_no失败');
                                    return -3;
                                }
                            }

                        }
                        DB::commit();
                    }catch (\Exception $exception){
                        \Log::error($exception);
                        return false;
                    }

                    $payInfo = $pay->resHandler->getParameter('pay_info');
                    return json_decode($payInfo);
                }else{
                    \Log::error('1支付-生成微信支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
                    return false;
                }
            }else{
                \Log::error('2支付-生成微信支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
                return false;
            }
        }else{
            \Log::error('3支付-生成微信支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
            return false;
        }
    }

    /**
     * 订单微信支付回调（中信）
     * @param $outTradeno
     * @param $transactionId
     * @param $totalFee
     * @return bool|int
     */
    public function wechatPayByZhongxinCallback($outTradeno, $transactionId, $totalFee){
        $orderModel         = new Orders();
        $weChatPayModel     = new WechatPay();
        $orderBlock         = new OrderBlock();
        $orderCouponModel   = new OrderCoupons();
        $userCouponModel    = new UserCoupons();
        $goodsModel         = new Goods();
        $orderGoodsModel    = new OrderGoods();
        $userModel          = new Users();
        $userIntegralModel  = new UserIntegralLog();

        $order = $orderBlock->orderWechatPayInfo([
            'out_trade_no'  => $outTradeno,
//            'pay_amount'    => $totalFee
        ]);
        \Log::debug(json_encode($order));

        //已经支付
        if($order->status !== 0){
            \Log::error("订单支付会调:已支付,out_trade_no:{$outTradeno}");
            return -2;
        }

        if(!$order){
            \Log::error("订单支付会调:订单不存在,out_trade_no:{$outTradeno}");
            return -1;
        }




        try{

            DB::beginTransaction();

            $sql = DB::table($orderCouponModel->getTable().' as oc')
                ->select('oc.user_coupon')
                ->leftJoin($orderModel->getTable().' as o', 'o.id', '=', 'oc.orders_id');

            $payAmount = 0;         //支付总费用
            $deliveryAmount = 0;    //运费
            \Log::debug(json_encode($order));
            if ($order->pay_mode == 1){
                $updateData = [
                    'status'    => 1,
                    'pay_type'  => 4,
                    'pay_time'  => date('Y-m-d H:i:s', time())
                ];
                if ($order->type == 1){
                    $updateData['code'] = substr($order->batch, -6);
                }
                if ($order->store_type_id == 4){
                    $updateData['status']   = 2;
                    $updateData['code']     = getSalt();
                    $path = public_path().'/qrcode/junanzenorder/';
                    if (!is_dir(public_path().'/qrcode')){
                        @mkdir($path, 0755);
                    }
                    if (!is_dir($path)){
                        @mkdir($path, 0755);
                    }
                    $imageName = $order->id.time().'.png';
                    $code       = getSalt();
                    $codeResult = QrCode::format('png')
                        ->size(400)
                        ->encoding('UTF-8')
                        ->generate(
                            $code,
                            "{$path}/$imageName"
                        );
                    if ($codeResult){
                        $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                    }
                }
                $orderResult = DB::table($orderModel->getTable())
                    ->where('batch', $order->batch)
                    ->where('status', 0)
                    ->whereNull('delete_time')
                    ->update($updateData);
                if($orderResult === false){
                    DB::rollBack();
                    \Log::error("订单支付会调:更新订单状态失败,out_trade_no:{$outTradeno}");
                    return false;
                }

                $batchOrder  = $orderBlock->orderWechatPayInfo(['batch' => $order->batch]);
                $orderId = [];
                foreach ($batchOrder as $item) {
                    $orderId[] = $item->id;
                    $payAmount += $item->pay_amount;
                    $deliveryAmount += $item->delivery_fee;
                }

                $goods = DB::table($orderGoodsModel->getTable())
                    ->select('goods_id', 'quantity')
                    ->whereIn('orders_id', $orderId)
                    ->get();

                foreach ($goods as $good) {
                    if(!DB::table($goodsModel->getTable())
                        ->where('id', $good->goods_id)
                        ->update([
                            'sales_volume'          => DB::raw("sales_volume + {$good->quantity}"),
                            'mouth_sales_volume'    => DB::raw("mouth_sales_volume + {$good->quantity}"),
                            'stock'                 => DB::raw("stock - {$good->quantity}")
                        ])
                    ){
                        DB::rollBack();
                        \Log::error("修改销量库存失败");
                        return false;
                    }
                }
                /**
                 * 修改优惠券状态
                 */
                $coupon = $sql->where('orders_id', $orderId)->get();
                if (!$coupon->isEmpty()){
                    $userCoupon = [];
                    foreach ($coupon as $item) {
                        if (!empty($item->user_coupon)){
                            $userCoupon[] = $item->user_coupon;
                        }
                    }
                    if(!DB::table($userCouponModel->getTable())
                        ->where('id', $userCoupon)
                        ->update(['is_use' => 1])
                    ){
                        DB::rollBack();
                        \Log::error("修改优惠券状态失败");
                        return false;
                    }
                }


            }else if($order->pay_mode == 2){
                $payAmount = $order->pay_amount;
                $deliveryAmount = $order->delivery_fee;
                $updateData = [
                    'status'    => 1,
                    'pay_type'  => 4,
                    'pay_time'  => date('Y-m-d H:i:s', time())
                ];
                if ($order->type == 1){
                    $updateData['code'] = substr($order->batch, -6);
                }
                if ($order->store_type_id == 4){
                    $updateData['status']   = 2;
                    $updateData['code']     = getSalt();
                    $path = public_path().'/qrcode/junanzenorder/';
                    if (!is_dir($path)){
                        @mkdir($path, 0755);
                    }
                    $imageName = $order->id.time().'.png';
                    $code       = getSalt();
                    $codeResult = QrCode::format('png')
                        ->size(400)
                        ->encoding('UTF-8')
                        ->generate(
                            $code,
                            "{$path}/$imageName"
                        );
                    if ($codeResult){
                        $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                    }
                }
                $orderResult = DB::table($orderModel->getTable())
                    ->where('id', $order->orders_id)
                    ->where('status', 0)
                    ->whereNull('delete_time')
                    ->update($updateData);
                if($orderResult === false){
                    DB::rollBack();
                    \Log::error("订单支付会调:更新订单状态失败,out_trade_no:{$outTradeno}");
                    return false;
                }

                /**
                 * 修改销量库存
                 */
                $goods = DB::table($orderGoodsModel->getTable())
                    ->select('goods_id', 'quantity')
                    ->where('orders_id', $order->id)
                    ->get();

                foreach ($goods as $good) {
                    if(!DB::table($goodsModel->getTable())
                        ->where('id', $good->goods_id)
                        ->update([
                            'sales_volume'          => DB::raw("sales_volume + {$good->quantity}"),
                            'mouth_sales_volume'    => DB::raw("mouth_sales_volume + {$good->quantity}"),
                            'stock'                 => DB::raw("stock - {$good->quantity}")
                        ])
                    ){
                        DB::rollBack();
                        \Log::error("修改销量库存失败");
                        return false;
                    }
                }

                /**
                 * 修改优惠券状态
                 */
                $coupon = $sql->where('orders_id', $order->id)->value('user_coupon');
                if ($coupon){
                    if(!DB::table($userCouponModel->getTable())
                        ->where('id', $coupon)
                        ->update(['is_use' => 1])
                    ){
                        DB::rollBack();
                        \Log::error("修改优惠券状态失败");
                        return false;
                    }
                }
            }


            if(
                DB::table($weChatPayModel->getTable())
                    ->where('out_trade_no', $outTradeno)
                    ->whereNull('delete_time')
                    ->update([
                        'transaction_id' => $transactionId
                    ]) === false
            ){
                DB::rollBack();
                \Log::error("订单支付会调:更新wechat pay中的transaction_id失败,out_trade_no:{$outTradeno}");
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }


    /**
     * 余额充值微信支付（中信）
     * @param $userId
     * @param $batch
     * @param $type
     * @return bool|int|mixed
     */
    public function rechargeWechatPayByZhongxin($userId, $batch, $type){
        $wechatPayModel         = new WechatPay();
        $rechargeModel          = new UserRechargeLog();
        $wechatUserModel        = new WechatUsers();
        $userModel              = new Users();
        $user = $userModel->where('id', $userId)->first();
        if (!$user){
            return false;
        }

        $wechatUser = $wechatUserModel->where(['id' => $user->wechat_user_id])->first();

        $recharge = $rechargeModel->where(['out_trade_no' => $batch, 'users_id' => $userId])->first();

        if(!$recharge){
            return -1;
        }

        if($recharge->status !== 0){
            return -2;
        }

        $outTradeNo = md5($batch . time());

        $pay = $this->__getZhongXinWechatPayObj();
        $pay->reqHandler->setParameter('out_trade_no', $outTradeNo);
        $pay->reqHandler->setParameter('body', '新华联梦想城充值');
        $pay->reqHandler->setParameter('total_fee', $recharge->total_fee);
        $pay->reqHandler->setParameter('mch_create_ip', get_server_ip());
        $pay->reqHandler->setParameter('service','pay.weixin.jspay');
        $pay->reqHandler->setParameter('mch_id',\Config::get('zhongxin-pay.mini.mchId'));
        $pay->reqHandler->setParameter('version',\Config::get('zhongxin-pay.mini.version'));
        $pay->reqHandler->setParameter('sign_type',\Config::get('zhongxin-pay.mini.sign_type'));
        $pay->reqHandler->setParameter('notify_url',\Config::get('app.url').'/pluto/user/pay/zhongxin/back?company=1');
        $pay->reqHandler->setParameter('sub_openid', $wechatUser->openid);
        $pay->reqHandler->setParameter('is_raw','1');

        if($type == 'pub'){
            $pay->reqHandler->setParameter('sub_appid','wxda61a3b58bb9983c');
        }else if($type == 'mini'){
            $pay->reqHandler->setParameter('sub_appid','wx10cb0441bd039cfb');
//            $pay->resHandler->setParameter('is_minipg', '1');
        }

        $pay->reqHandler->setParameter('nonce_str',mt_rand());
        $pay->reqHandler->createSign();
        $data = Utils::toXml($pay->reqHandler->getAllParameters());
        $pay->pay->setReqContent($pay->reqHandler->getGateURL(),$data);

        if($pay->pay->call()){
            $pay->resHandler->setContent($pay->pay->getResContent());
            $pay->resHandler->setKey($pay->reqHandler->getKey());
            $res = $pay->resHandler->getAllParameters();

            \Log::info(date("Y-m-d H:i:s",time()).'支付返回XML'.json_encode($res));
            if($pay->resHandler->isTenpaySign()){
                if(
                    $pay->resHandler->getParameter('status') == 0
                    &&
                    $pay->resHandler->getParameter('result_code') == 0
                ){
                    //更新订单支付信息
                    if(
                        DB::table($wechatPayModel->getTable())
                            ->insert([
                                'batch'         => $batch,
                                'out_trade_no'  => $outTradeNo,
                                'type'          => 2,
                                'create_time'   => date('Y-m-d H:i:s', time())
                            ]) === false
                    ){
                        \Log::error('支付-生成微信支付配置数据-错误:保存out_trade_no失败');
                        return -3;
                    }

                    $payInfo = $pay->resHandler->getParameter('pay_info');
                    return json_decode($payInfo);
                }else{
                    \Log::error('支付-生成微信支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
                    return false;
                }
            }else{
                \Log::error('支付-生成微信支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
                return false;
            }
        }else{
            \Log::error('支付-生成微信支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
            return false;
        }
    }




    /**
     * 余额充值微信回调（中信）
     * @param $outTradeno
     * @param $transactionId
     * @param $totalFee
     * @return bool|int
     * @throws \Exception
     */
    public function rechargeWechatPayByZhongxinCallback($outTradeno, $transactionId, $totalFee){
        $userRechargeLogModel           = new UserRechargeLog();
        $wechatPayModel                 = new WechatPay();
        $usersModel                     = new Users();
        $walletModel                    = new UserWalletLog();
        $storeCouponModel               = new StoreCoupons();
        $userCouponModel                = new UserCoupons();
        $userBlock                      = new UserBlock();

        $userRecharge = DB::table($userRechargeLogModel->getTable() . ' as o')
            ->select(
                'o.*'
            )
            ->leftJoin($wechatPayModel->getTable() . ' as wp', 'wp.batch', '=', 'o.out_trade_no')
            ->where('wp.out_trade_no', $outTradeno)
            ->first();

        if(!$userRecharge){
            \Log::error("订单支付会调:订单不存在,out_trade_no:{$outTradeno}");
            return -1;
        }

        //已经支付
        if($userRecharge->status !== 0){
            \Log::error("订单支付会调:已支付,out_trade_no:{$outTradeno}");
            return -2;
        }

        try{
            $sendMoney = $userBlock->getRechargeActivity($userRecharge->total_fee);
            $money = 0;

            $activityId = 0;
            if ($sendMoney){
                $activityLog    = DB::table($userRechargeLogModel->getTable())
                    ->where('activity_id', $sendMoney->id)
                    ->where('users_id', $userRecharge->users_id)
                    ->first();


                if (!$activityLog){
                    $activityId = $sendMoney->id;
                    $userRecharge->total_fee += $sendMoney->send;
                    $money = $sendMoney->send;
                }
            }

            if(
                DB::table($userRechargeLogModel->getTable())
                    ->where('id', $userRecharge->id)
                    ->where('status', 0)
                    ->update([
                        'status'        => 1,
                        'pay_type'      => 1,
                        'pay_time'      => date('Y-m-d H:i:s', time()),
                        'total_fee'     => $userRecharge->total_fee,
                        'remake'        => '充值活动赠送：'.priceIntToFloat($money).'元',
                        'activity_id'   => $activityId
                    ]) === false
            ){
                \Log::error("订单支付会调:更新订单状态失败,out_trade_no:{$outTradeno}");
                return false;
            }

            if(
                DB::table($usersModel->getTable())
                    ->where('id', $userRecharge->users_id)
                    ->increment('balance',$userRecharge->total_fee) === false
            ){
                \Log::error("订单支付会调:更新会员余额失败,out_trade_no:{$outTradeno}");
                return false;
            }
            \Log::info("小程序余额充值成功==>>>>>>>>用户id====>>>>{$userRecharge->users_id}=>>>>>>>>>>充值金额=>>>>>>>>>>>>>{$userRecharge->total_fee}");
            $balance = DB::table($usersModel->getTable())
                ->where('id', $userRecharge->users_id)
                ->value('balance');
            if(
                DB::table($walletModel->getTable())
                    ->insertGetId(
                        [
                            'users_id'      => $userRecharge->users_id,
                            'money'         => $userRecharge->total_fee,
                            'wallet_type'   => 1,
                            'balance'       => $balance,
                            'type'          => 2,
                            'remake'        => '充值活动赠送：'.priceIntToFloat($money).'元'
                        ]
                    ) === false
            ){
                \Log::error("订单支付会调:更新会员余额失败,out_trade_no:{$outTradeno}");
                return false;
            }

            if(
                DB::table($wechatPayModel->getTable())
                    ->where('out_trade_no', $outTradeno)
                    ->whereNull('delete_time')
                    ->update([
                        'transaction_id' => $transactionId
                    ]) === false
            ){
                DB::rollBack();
                \Log::error("订单支付会调:更新wechat pay中的transaction_id失败,out_trade_no:{$outTradeno}");
                return false;
            }


            $coupon = DB::table($storeCouponModel->getTable())
                ->where('scope', '充值')
                ->where('recharge_amount', '<=', $userRecharge->total_fee)
                ->where('quantity', '>', 0)
                ->whereNull('delete_time')
                ->get()->toArray();
            \Log::debug('充值优惠券信息'.json_encode($coupon));

            if (!empty($coupon)){
                $saveUserCoupon = [];
                foreach ($coupon as $key => $value) {
                    $couponId = [];
                    $couponId[] = $value->id;
                    $saveUserCoupon[$key]['users_id']         = $userRecharge->users_id;
                    $saveUserCoupon[$key]['coupon_id']        = $value->id;
                    $saveUserCoupon[$key]['start_time']       = date('Y-m-d H:i:s', time());
                    $saveUserCoupon[$key]['end_time']         = date('Y-m-d H:i:s', time() + $value->term_of_validity * 60 *60);
                    $saveUserCoupon[$key]['code']             = null;
                    $saveUserCoupon[$key]['qr_code']          = null;
                    if ($value->virtual == 2){
                        $path = public_path().'/qrcode/coupon/user';
                        if (!is_dir(public_path().'/qrcode/coupon')){
                            @mkdir(public_path().'/qrcode/coupon', 0755);
                        }

                        if (!is_dir($path)){
                            @mkdir($path, 0755);
                        }

                        $code = getSalt(12);
                        $imageName = $userRecharge->users_id.time().$value->id.".png";
                        QrCode::format('png')
                            ->size(500)
                            ->encoding('UTF-8')
                            ->generate(
                                $code,
                                $path."/".$imageName
                            );

                        $saveUserCoupon[$key]['code']           = $code;
                        $saveUserCoupon[$key]['qr_code']        = env('APP_URL').'/qrcode/coupon/user/'.$imageName;
                    }
                }

                if(!DB::table($userCouponModel->getTable())
                    ->insert($saveUserCoupon)
                ){
                    \Log::debug('添加充值优惠券失败');
                }

                if(!DB::table($storeCouponModel->getTable())
                    ->whereIn('id', $couponId)
                    ->decrement('quantity')
                ){
                    \Log::debug('减少优惠券数量失败');
                }
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }


    /**
     * 退款
     * @param $orderId
     * @return bool|int|mixed
     */
    public function orderZhongxinRefund($orderId){
        $orderModel         = new Orders();
        $weChatPayModel     = new WechatPay();
        $weChatRefundModel  = new WechatPayRefund();

        $order      = DB::table($orderModel->getTable())->where('id', $orderId)->first();
        $orderInfo  = DB::table($weChatPayModel->getTable())->where('orders_id', $orderId)->first();

        $refund     = [];
        $refund['orders_id']        = $orderId;
        $refund['out_refund_no']    = $orderId . (microtime(true) * 10000) . $order->users_id;
        $refund['total_fee']        = $order->pay_amount;
        $refund['refund_fee']       = $order->pay_amount;

        $refundId = DB::table($weChatRefundModel->getTable())->insertGetId($refund);
        if( $refundId === false ){
            DB::rollBack();
            \log::error('新增退款记录失败');
            return false;
        }

        $pay = $this->__getZhongXinWechatPayObj();
        $pay->reqHandler->setParameter('service', 'unified.trade.refund');
        $pay->reqHandler->setParameter('version',\Config::get('zhongxin-pay.mini.version'));
        $pay->reqHandler->setParameter('sign_type',\Config::get('zhongxin-pay.mini.sign_type'));
        $pay->reqHandler->setParameter('mch_id',\Config::get('zhongxin-pay.mini.mchId'));
        $pay->reqHandler->setParameter('out_trade_no', $orderInfo->out_trade_no);
        $pay->reqHandler->setParameter('out_refund_no', $refund['out_refund_no']);
        $pay->reqHandler->setParameter('total_fee', 1); //$order->pay_amount
        $pay->reqHandler->setParameter('refund_fee', 1); //退款金额
        $pay->reqHandler->setParameter('op_user_id', \Config::get('zhongxin-pay.mini.mchId'));
        $pay->reqHandler->setParameter('nonce_str', mt_rand());
        $pay->reqHandler->createSign();

        $data = Utils::toXml($pay->reqHandler->getAllParameters());
        $pay->pay->setReqContent($pay->reqHandler->getGateURL(),$data);

        if($pay->pay->call()) {
            $pay->resHandler->setContent($pay->pay->getResContent());
            $pay->resHandler->setKey($pay->reqHandler->getKey());
            $res = $pay->resHandler->getAllParameters();

            \Log::info(date("Y-m-d H:i:s", time()) . '退款XML' . json_encode($res));

            if($pay->resHandler->isTenpaySign()){
                if($pay->resHandler->getParameter('result_code') == 0){
                    //更新订单退款信息
                    if(
                        DB::table($weChatRefundModel->getTable())
                            ->where('id', $refundId)
                            ->update([
                                'status'         => 1
                            ]) === false
                    ){
                        \Log::error('退款，更新退款状态失败');
                        return -1;
                    }
                }else{
                    if(
                        DB::table($weChatRefundModel->getTable())
                            ->where('id', $refundId)
                            ->update([
                                'status'         => 2,
                                'reason'         => json_decode(json_encode($res),true)['err_msg']
                            ]) === false
                    ){
                        \Log::error('退款，更新退款状态失败');
                        return -1;
                    }
                }
                $payInfo = $pay->resHandler->getParameter('pay_info');
                return json_decode($payInfo);
            }else{
                \Log::error('支付-生成微信支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
                return false;
            }
        }else{
            \Log::error('支付-生成微信支付配置数据-错误:'.$pay->resHandler->getParameter('message'));
            return false;
        }

    }

    /**
     * 订单余额支付
     * @param $userId
     * @param $batch
     * @param $orderId
     * @param $money
     * @param $userInfo
     * @return bool
     */
    public function balancePay($userId, $batch, $orderId, $userInfo, $orderInfo, $money){

        try{
            DB::beginTransaction();

            $orderModel         = new Orders();
            $walletModel        = new UserWalletLog();
            $userModel          = new Users();
            $userCouponModel    = new UserCoupons();
            $orderCouponModel   = new OrderCoupons();
            $goodsModel         = new Goods();
            $orderGoodsModel    = new OrderGoods();

            $payAmount = 0;         //支付总费用
            $deliveryAmount = 0;    //运费

            /**
             * 添加用户余额支付日志
             */
            $storeType = 0;
            $orderType = 1;
            foreach ($orderInfo as $item) {
                $storeType = $item->store_type_id;
                $payAmount += $item->pay_amount;
                $deliveryAmount += $item->delivery_fee;

                $orderType = $item->type;
                $orderBatch = $item->batch;

                $walletData = [
                    'users_id'      => $userId,
                    'money'         => $item->pay_amount,
                    'balance'       => $userInfo->balance,
                    'wallet_type'   => 1,
                    'stores_id'     => $item->stores_id,
                    'type'          => 1,
                    'create_time'   => date('Y-m-d H:i:s', time())
                ];
                if(!DB::table($walletModel->getTable())->insert($walletData)){
                    DB::rollBack();
                    \Log::debug('添加余额支付日志失败,添加数据：'.json_encode($walletData));
                    return false;
                }
            }

            /**
             * 修改用户余额
             */
            if(DB::table($userModel->getTable())->where('id', $userId)->decrement('balance', $money) === false){
                DB::rollBack();
                \Log::debug('修改用户余额失败');
                return false;
            }


            /**
             * 修改订单位置
             */
            $sql = DB::table($orderModel->getTable().' as o')
                ->select('oc.user_coupon', 'o.id')
                ->leftJoin($orderCouponModel->getTable().' as oc', 'o.id', '=', 'oc.orders_id');



            if (!empty($batch)){
                $coupon = $sql->where('batch', $batch)
                    ->get()->toArray();

                $userCouponId = [];
                $ordersId = [];
                foreach ($coupon as $item) {
                    $ordersId[] = $item->id;
                    if (!empty($item->user_coupon)){
                        $userCouponId[] = $item->user_coupon;
                    }
                }

                if(!empty($userCouponId)){
                    if(
                    !DB::table($userCouponModel->getTable())
                        ->whereIn('id', $userCouponId)
                        ->update(['is_use' => 1])
                    ){
                        \Log::debug('优惠券信息'.json_encode($coupon));
                        DB::rollBack();
                        return false;
                    }
                }

                /**
                 * 修改商品销量
                 */
                $goods = DB::table($orderGoodsModel->getTable())
                    ->select('goods_id', 'quantity')
                    ->whereIn('orders_id', $ordersId)
                    ->get();
                foreach ($goods as $value) {
                    if(!DB::table($goodsModel->getTable())
                        ->where('id', $value->goods_id)
                        ->update(array(
                            'sales_volume'          => DB::raw("sales_volume + ".$value->quantity),
                            'mouth_sales_volume'    => DB::raw("mouth_sales_volume + ".$value->quantity),
                            'stock'                 => DB::raw("stock - ".$value->quantity),
                        ))
                    ){
                        \Log::debug('修改商品销量失败');
                        DB::rollBack();
                        return false;
                    }
                }

                $updateData = [
                    'pay_mode'  => 1,
                    'status'    => 1,
                    'pay_time'  => date('Y-m-d H:i:s', time()),
                    'pay_type'  => 3
                ];
                if ($orderType == 1 && $storeType != 4){
                    $updateData['code'] = substr($orderBatch, -6);
                }
                if ($storeType == 4){
                    $updateData['status']   = 2;
                    $updateData['code']     = getSalt();
                    $path = public_path().'/qrcode/junanzenorder/';
                    if (!is_dir($path)){
                        @mkdir($path, 0755);
                    }
                    $imageName = $ordersId[0].time().'.png';
                    $code       = getSalt();
                    $codeResult = QrCode::format('png')
                        ->size(400)
                        ->encoding('UTF-8')
                        ->generate(
                            $code,
                            "{$path}/$imageName"
                        );
                    if ($codeResult){
                        $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                    }
                }
                $mode = DB::table($orderModel->getTable())->where('batch', $batch)->update($updateData);


            }else{
                $userCouponId = $sql->where('oc.id', $orderId)
                    ->value('user_coupon');
                if ($userCouponId){
                    if(
                    !DB::table($userCouponModel->getTable())
                        ->where('id', $userCouponId)
                        ->update(['is_use' => 1])
                    ){
                        \Log::debug('修改优惠券信息失败');
                        DB::rollBack();
                        return false;
                    }
                }

                /**
                 * 修改商品销量
                 */
                $goods = DB::table($orderGoodsModel->getTable())
                    ->select('goods_id', 'quantity')
                    ->where('orders_id', $orderId)
                    ->get();

                foreach ($goods as $value) {
                    if(!DB::table($goodsModel->getTable())
                        ->where('id', $value->goods_id)
                        ->update(array(
                            'sales_volume'          => DB::raw("sales_volume + {$value->quantity}"),
                            'mouth_sales_volume'    => DB::raw("mouth_sales_volume + {$value->quantity}"),
                            'stock'                 => DB::raw("stock - {$value->quantity}"),
                        ))
                    ){
                        \Log::debug('修改商品销量失败');
                        DB::rollBack();
                        return false;
                    }
                }

                $updateData = [
                    'pay_mode'  => 0,
                    'status'    => 1,
                    'pay_time'  => date('Y-m-d H:i:s', time()),
                    'pay_type'  => 3
                ];
                if ($storeType == 4){
                    $updateData['status']   = 2;
                    $updateData['code']     = getSalt();
                    $path = public_path().'/qrcode/junanzenorder/';
                    if (!is_dir($path)){
                        @mkdir($path, 0755);
                    }
                    $imageName = $orderId.time().'.png';
                    $code       = getSalt();
                    $codeResult = QrCode::format('png')
                        ->size(400)
                        ->encoding('UTF-8')
                        ->generate(
                            $code,
                            "{$path}/$imageName"
                        );
                    if ($codeResult){
                        $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                    }
                }

                if ($orderType == 1){
                    $updateData['code'] = substr($orderBatch, -6);
                }

                $mode = DB::table($orderModel->getTable())->where('id', $orderId)->update($updateData);


            }

            if (!$mode){
                DB::rollBack();
                \Log::debug('修改订单pay_mode、状态失败');
                return false;
            }

//            if ($storeType == 4){
//                Redis::setex('orderTypeFour-cancel:'.json_encode($ordersId), 300,'test');
//            }
            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error('余额支付失败，原因：'.$exception);
            \Log::error('订单数据'.json_encode($orderInfo));
            return false;
        }


    }


    /**
     * 订单支付宝支付
     * @param $userId
     * @param $batch
     * @param $orderId
     * @return array|bool|int|\Symfony\Component\HttpFoundation\Response
     */
    public function aliPay($userId, $batch, $orderId, $type){
        $aliPayModel    = new Alipay();
        $orderBlock     = new OrderBlock();
        $orderModel     = new Orders();
        $search         = [
            'users_id'  => $userId
        ];
        $outTradeNo     = md5($batch . time());
        $time           = date('Y-m-d H:i:s', time());
        $aliPayData     = [];

        /**
         * batch
         *
         *
         */
        $payAmount = 0;
        if (!empty($batch)){
            $search['batch']        = $batch;
            $order = $orderBlock->orderAliPayInfo($search);
            foreach ($order as $key => $item) {
                if ($item->is_use === 1){
                    return -4;
                }
                if($item->status !== 0){
                    return -2;
                }
                $aliPayData[$key]['batch']           = $item->batch;
                $aliPayData[$key]['out_trade_no']    = $outTradeNo;
                $aliPayData[$key]['create_time']     = $time;
                $aliPayData[$key]['orders_id']       = $item->id;
                $payAmount += $item->pay_amount;
            }
        }

        /**
         * orders_id
         */
        if (!empty($orderId)){
            $search['orders_id']        = $orderId;
            $order      = $orderBlock->orderAliPayInfo($search);
            if ($order->is_use === 1){
                return -4;
            }

            if($order->status !== 0){
                return -2;
            }
            $payAmount = $order->pay_amount;
        }


        if (!$order){
            return -1;
        }
        \Log::debug($outTradeNo);
        $data = [
            'subject'      => '新华联梦想城福街消费',
            'out_trade_no' => $outTradeNo,
            'total_amount' => priceIntToFloat($payAmount),
        ];

        try{
            DB::beginTransaction();
            //添加支付宝支付記錄，修改訂單支付模式
            if(isset($search['batch']) && !empty($search['batch'])){
                if(
                    DB::table($aliPayModel->getTable())
                        ->insert($aliPayData) === false
                ){
                    DB::rollBack();
                    \Log::error('支付-生成支付宝配置数据-错误:保存out_trade_no失败');
                    return -3;
                }

                if (DB::table($orderModel->getTable())
                        ->where('batch', $search['batch'])
                        ->update([
                            'pay_mode' => 1
                        ]) === false
                ){
                    DB::rollBack();
                    \Log::error('支付-生成支付宝配置数据-错误:修改pay_mode失败');
                    return -3;
                }
            }

            //修改支付宝支付記錄訂單號，修改訂單支付模式
            if(isset($search['orders_id']) && !empty($search['orders_id'])){
                if (DB::table($orderModel->getTable())
                        ->where('batch', $order->batch)
                        ->update([
                            'pay_mode' => 2
                        ]) === false
                ){
                    DB::rollBack();
                    \Log::error('支付-生成支付宝配置数据-错误:修改pay_mode失败');
                    return -3;
                }
                $aliPayInfo = DB::table($aliPayModel->getTable())
                    ->where('orders_id', $search['orders_id'])
                    ->first();
                if ($aliPayInfo){
                    if (DB::table($aliPayModel->getTable())
                            ->where('orders_id', $search['orders_id'])
                            ->update([
                                'out_trade_no' => $outTradeNo
                            ]) === false
                    ){
                        DB::rollBack();
                        \Log::error('支付-生成支付宝配置数据-错误:修改out_trade_no失败');
                        return -3;
                    }
                }else{
                    if(!DB::table($aliPayModel->getTable())->insert([
                        'out_trade_no'    => $outTradeNo,
                        'create_time'     => $time,
                        'orders_id'       => $order->id,
                        'batch'           => $order->batch
                    ])){
                        DB::rollBack();
                        \Log::error('支付-生成支付宝配置数据-错误:添加out_trade_no失败');
                        return -3;
                    }
                }

            }

            DB::commit();

            if ($type == 'app'){
                $data = Pay::alipay(config('ali-pay.config'))->app($data);
            }else{
                $data = Pay::alipay(config('ali-pay.config'))->wap($data);
            }

            return $data;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }


    }

    /**
     * 订单支付宝支付回调
     * @param $notifyData
     * @return bool
     */
    public function aliPayNotify($notifyData){
        $orderModel         = new Orders();
        $aliPayModel        = new Alipay();
        $orderBlock         = new OrderBlock();
        $orderCouponModel   = new OrderCoupons();
        $userCouponModel    = new UserCoupons();
        $goodsModel         = new Goods();
        $orderGoodsModel    = new OrderGoods();
        $dianWoDa           = new DianWoDa();

        \Log::debug('支付宝回调信息：'.json_encode($notifyData));

        /**
         * 支付成功
         */
        if ($notifyData->trade_status == 'TRADE_SUCCESS' || $notifyData->trade_status == 'TRADE_FINISHED '){

            $order      = $orderBlock->orderAliPayInfo([
                'out_trade_no'  => $notifyData->out_trade_no,
            ]);
            try{

                DB::beginTransaction();
                $sql = DB::table($orderCouponModel->getTable().' as oc')
                    ->select('oc.user_coupon')
                    ->leftJoin($orderModel->getTable().' as o', 'oc.orders_id', '=', 'o.id');

                $payAmount = 0;         //支付总费用
                $deliveryAmount = 0;    //运费

                if ($order->pay_mode == 1){

                    $batchOrder  = $orderBlock->orderAliPayInfo(['batch' => $order->batch]);
                    $orderId = [];
                    foreach ($batchOrder as $item) {
                        $orderId[] = $item->id;
                        $payAmount += $item->pay_amount;
                        $deliveryAmount += $item->delivery_fee;
                        $storeTypeId = $item->store_type_id;

                        if ($item->store_type_id == 1){
                            $orderGoods = DB::table($orderGoodsModel->getTable())
                                ->select('name as goods_name', 'selling_price', 'quantity')
                                ->where('orders_id', $item->id)
                                ->get();
                            $createResult = $dianWoDa->orderCreate(
                                [
                                    'batch'             => $order->batch,
                                    'order_remark'      => $order->remark,
                                    'pay_amount'        => $order->pay_amount,
                                    'stores_id'         => $order->stores_id,
                                    'store_name'        => $order->store_name,
                                    'mobile'            => $order->store_mobile,
                                    'address'           => $order->store_address,
                                    'lat'               => $order->store_latitude,
                                    'lng'               => $order->store_longitude
                                ],
                                [
                                    'name'              => $order->consignee,
                                    'mobile'            => $order->contact,
                                    'address'           => $order->user_address,
                                    'lat'               => $order->user_latitude,
                                    'lng'               => $order->user_longitude
                                ],
                                json_decode(json_encode($orderGoods), true)
                            );
                            \Log::debug('点我达订单推送结果'.json_encode($createResult));
                        }
                    }


                    $updateData = [
                        'status'    => 1,
                        'pay_type'  => 2,
                        'pay_time'  => date('Y-m-d H:i:s', time())
                    ];
                    if ($order->type == 1 && $order->store_type_id != 4){
                        $updateData['code'] = substr($order->batch, -6);
                    }
                    if ($storeTypeId == 4){
                        $updateData['status']   = 2;
                        $updateData['code']     = getSalt();
                        $path = public_path().'/qrcode/junanzenorder/';
                        if (!is_dir($path)){
                            @mkdir($path, 0755);
                        }
                        $imageName = $order->id.time().'.png';
                        $code       = getSalt();
                        $codeResult = QrCode::format('png')
                            ->size(400)
                            ->encoding('UTF-8')
                            ->generate(
                                $code,
                                "{$path}/$imageName"
                            );
                        if ($codeResult){
                            $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                        }
                    }
                    $orderResult = DB::table($orderModel->getTable())
                        ->where('batch', $order->batch)
                        ->where('status', 0)
                        ->whereNull('delete_time')
                        ->update($updateData);
                    if($orderResult === false){
                        DB::rollBack();
                        \Log::error("订单支付会调:更新订单状态失败,out_trade_no:{$notifyData->out_trade_no}");
                        return false;
                    }

                    /**
                     * 修改商品销量库存
                     */
                    $goods = DB::table($orderGoodsModel->getTable())
                        ->select('goods_id', 'quantity')
                        ->whereIn('orders_id',$orderId)
                        ->get();

                    foreach ($goods as $good) {
                        if(!DB::table($goodsModel->getTable())
                            ->where('id', $good->goods_id)
                            ->update([
                                'sales_volume' => DB::raw("sales_volume + {$good->quantity}"),
                                'mouth_sales_volume'  => DB::raw("mouth_sales_volume + {$good->quantity}"),
                                'stock'  => DB::raw("stock - {$good->quantity}"),
                            ])
                        ){
                            DB::rollBack();
                            \Log::error("修改商品销量库存失败");
                            return false;
                        }
                    }

                    /**
                     * 修改优惠券状态
                     */
                    $coupon = $sql->whereIn('orders_id', $orderId)
                        ->get();
                    if (!$coupon->isEmpty()){
                        $userCouponId = [];
                        foreach ($coupon as $item) {
                            if (!empty($item->user_coupon)){
                                $userCouponId[] = $item->user_coupon;
                            }

                        }

                        if(!DB::table($userCouponModel->getTable())
                            ->whereIn('id', $userCouponId)
                            ->update(['is_use' => 1])
                        ){
                            DB::rollBack();
                            \Log::error("修改优惠券状态失败");
                            return false;
                        }
                    }


                }else if($order->pay_mode == 2){
                    if ($order->store_type_id == 1){
                        $orderGoods = DB::table($orderGoodsModel->getTable())
                            ->select('name as goods_name', 'selling_price', 'quantity')
                            ->where('orders_id', $order->id)
                            ->get();
                        $createResult = $dianWoDa->orderCreate(
                            [
                                'batch'             => $order->batch,
                                'order_remark'      => $order->remark,
                                'pay_amount'        => $order->pay_amount,
                                'stores_id'         => $order->stores_id,
                                'store_name'        => $order->store_name,
                                'mobile'            => $order->store_mobile,
                                'address'           => $order->store_address,
                                'lat'               => $order->store_latitude,
                                'lng'               => $order->store_longitude
                            ],
                            [
                                'name'              => $order->consignee,
                                'mobile'            => $order->contact,
                                'address'           => $order->user_address,
                                'lat'               => $order->user_latitude,
                                'lng'               => $order->user_longitude
                            ],
                            json_decode(json_encode($orderGoods), true)
                        );
                        \Log::debug('点我达订单推送结果'.json_encode($createResult));
                    }


                    $updateData = [
                        'status'    => 1,
                        'pay_type'  => 1,
                        'pay_time'  => date('Y-m-d H:i:s', time())
                    ];
                    if ($order->type == 1 && $order->store_type_id != 4){
                        $updateData['code'] = substr($order->batch, -6);
                    }
                    if ($order->store_type_id == 4){
                        $updateData['status']   = 2;
                        $updateData['code']     = getSalt();
                        $path = public_path().'/qrcode/junanzenorder/';
                        if (!is_dir($path)){
                            @mkdir($path, 0755);
                        }
                        $imageName = $order->id.time().'.png';
                        $code       = getSalt();
                        $codeResult = QrCode::format('png')
                            ->size(400)
                            ->encoding('UTF-8')
                            ->generate(
                                $code,
                                "{$path}/$imageName"
                            );
                        if ($codeResult){
                            $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                        }
                    }
                    $orderResult = DB::table($orderModel->getTable())
                        ->where('id', $order->orders_id)
                        ->where('status', 0)
                        ->whereNull('delete_time')
                        ->update($updateData);
                    if($orderResult === false){
                        DB::rollBack();
                        \Log::error("订单支付会调:更新订单状态失败,out_trade_no:{$notifyData->out_trade_no}");
                        return false;
                    }

                    /**
                     * 修改商品销量库存
                     */
                    $goods = DB::table($orderGoodsModel->getTable())
                        ->select('goods_id', 'quantity')
                        ->where('orders_id', $order->id)
                        ->get();

                    foreach ($goods as $good) {
                        if(!DB::table($goodsModel->getTable())
                            ->where('id', $good->goods_id)
                            ->update([
                                'sales_volume' => DB::raw("sales_volume + {$good->quantity}"),
                                'mouth_sales_volume'  => DB::raw("mouth_sales_volume + {$good->quantity}"),
                                'stock'  => DB::raw("stock - {$good->quantity}"),
                            ])
                        ){
                            DB::rollBack();
                            \Log::error("修改商品销量库存失败");
                            return false;
                        }
                    }

                    $coupon = $sql->where('orders_id', $order->stores_id)
                        ->value('user_coupon');
                    if ($coupon){
                        if(!DB::table($userCouponModel->getTable())
                            ->where('id', $coupon)
                            ->update(['is_use' =>1])
                        ){
                            DB::rollBack();
                            \Log::error("修改优惠券状态失败");
                            return false;
                        }
                    }
                }


                if(
                    DB::table($aliPayModel->getTable())
                        ->where('out_trade_no', $notifyData->out_trade_no)
                        ->update([
                            'trade_no'          => $notifyData->trade_no,
                            'total_amount'      => $notifyData->total_amount * 100,
                            'seller_id'         => $notifyData->seller_id,
                            'merchant_order_no' => $notifyData->merchant_order_no,
                            'type'              => 1
                        ]) === false
                ){
                    DB::rollBack();
                    \Log::error("更新支付宝支付失败，out_trade_no:{$notifyData->out_trade_no}");
                    return false;
                }

//                if (isset($storeTypeId) && $storeTypeId == 4){
//                    Redis::setex('orderTypeFour-cancel:'.json_encode($orderId), 300,'orderTypeFour-cancel');
//                }

                DB::commit();
                return true;
            }catch (\Exception $exception){
                DB::rollBack();
                \Log::error($exception);
                return false;
            }


        }



    }

    /**
     * 支付宝用户充值
     * @param $batch
     * @return bool|\Symfony\Component\HttpFoundation\Response
     */
    public function aliPayRecharge($batch, $type){
        $aliPayModel        = new Alipay();
        $outTradeNo         = md5($batch . time());


        $data = [
            'subject'      => '新华联梦想城充值',
            'out_trade_no' => $outTradeNo,
            'total_amount' => priceIntToFloat(UserRechargeLog::where('out_trade_no', $batch)->value('total_fee')),
        ];

        if ($type == 'app'){
            $aliPayData         = Pay::alipay(config('ali-pay.recharge'))->app($data);
        }else if ($type == 'h5'){
            $aliPayData         = Pay::alipay(config('ali-pay.recharge'))->wap($data);
        }
        if (!$aliPayData){
            return false;
        }

        /**
         * type 1：订单  2：充值 3：预约 4：停车场
         */
        if (!DB::table($aliPayModel->getTable())->insert([
            'batch'         => $batch,
            'out_trade_no'  => $outTradeNo,
            'type'          => 2
        ])){
            return false;
        }

        return $aliPayData;
    }

    /**
     * 支付宝用户充值回调
     * @param $notifyData
     * @return bool
     */
    public function aliPayRechargeNotify($notifyData){
        if ($notifyData->trade_status == 'TRADE_SUCCESS' || $notifyData->trade_status == 'TRADE_FINISHED '){
            $userRechargeModel    = new UserRechargeLog();
            $userModel            = new Users();
            $aliPayModel          = new Alipay();
            $walletModel          = new UserWalletLog();
            $userBlock            = new UserBlock();

            $userRechargeInfo     = DB::table($userRechargeModel->getTable().' as o')
                ->select('o.*', 'u.balance')
                ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'o.users_id')
                ->leftJoin($aliPayModel->getTable().' as a', 'a.batch', '=', 'o.out_trade_no')
                ->where('a.out_trade_no', $notifyData->out_trade_no)
                ->first();
            try{
                $sendMoney  = $userBlock->getRechargeActivity($userRechargeInfo->total_fee);
                $money = 0;
                $activityId = 0;
                if ($sendMoney){
                    $activityLog = DB::table($userRechargeModel->getTable())
                        ->where('users_id', $userRechargeInfo->users_id)
                        ->where('activity_id', $sendMoney->id)
                        ->first();

                    if (!$activityLog){
                        $activityId = $sendMoney->id;
                        $money = $sendMoney->send;
                        $userRechargeInfo->total_fee += $sendMoney->send;
                    }
                }

                /**
                 * 修改用户余额
                 */
                if(!DB::table($userModel->getTable())
                    ->where('id', $userRechargeInfo->users_id)
                    ->increment('balance', $userRechargeInfo->total_fee)
                ){
                    DB::rollBack();
                    \Log::debug('用户充值修改余额失败');
                    return false;
                }

                /**
                 *  修改充值日志状态
                 */
                if(!DB::table($userRechargeModel->getTable())
                    ->where('out_trade_no', $userRechargeInfo->out_trade_no)
                    ->update([
                        'status'        => 1,
                        'total_fee'     => $userRechargeInfo->total_fee,
                        'remake'        => '充值活动赠送：'.priceIntToFloat($money).'元',
                        'acitivty_id'   => $activityId
                    ])
                ){
                    DB::rollBack();
                    \Log::debug('用户充值修改支付宝支付状态失败');
                    return false;
                }

                /**
                 * 添加钱包明细日志
                 */
                if(!DB::table($walletModel->getTable())
                    ->insert([
                        'users_id'      => $userRechargeInfo->users_id,
                        'money'         => $userRechargeInfo->total_fee,
                        'balance'       => $userRechargeInfo->balance,
                        'wallet_type'   => 0,
                        'stores_id'     => 0,
                        'type'          => 2,
                        'remake'        => '充值活动赠送：'.priceIntToFloat($money).'元'
                    ])
                ){
                    \Log::debug('添加钱包明细失败');
                    DB::rollBack();
                    return false;
                }

                /**
                 * 修改支付宝支付信息
                 */
                if(!DB::table($aliPayModel->getTable())->where('out_trade_no', $notifyData->out_trade_no)
                    ->update([
                        'trade_no'          => $notifyData->trade_no,
                        'total_amount'      => $notifyData->total_amount,
                        'seller_id'         => $notifyData->seller_id,
                        'merchant_order_no' => $notifyData->merchant_order_no
                    ])){
                    DB::rollBack();
                    \Log::debug('修改支付宝支付信息失败');
                    return false;
                }



                \Log::info("APP支付宝余额充值成功==>>>>>>>>用户id====>>>>{$userRechargeInfo->users_id}=>>>>>>>>>>充值金额=>>>>>>>>>>>>>{$userRechargeInfo->total_fee}");
                DB::commit();
                return true;
            }catch (\Exception $exception){
                DB::rollBack();
                \Log::error('APP支付宝余额充值失败===>>>>>>>>>'.$exception);
                return false;
            }
        }
    }


    /**
     * 余额充值APP微信支付
     * @param $batch
     * @return array|bool
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function weChatPayRecharge($batch){
        $weChatPay          = new WechatPay();
        $outTradeNo         = md5($batch . time());

        $app = Factory::payment(\Config::get('zhongxin-pay.app'));
        $result = $app->order->unify([
            'body' => '新华联梦想城充值',
            'out_trade_no' => $outTradeNo,
            'total_fee' => UserRechargeLog::where('out_trade_no', $batch)->value('total_fee'),
            'spbill_create_ip' => get_server_ip(),
            'notify_url' => env('APP_URL').'/pluto/user/pay/wechat/recharge/notify',
            'trade_type' => 'APP'
        ]);

        \Log::debug('result:'.json_encode($result));
        if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS'){
            /**
             * type 1：订单 2：充值 3：预约 4：停车场
             */
            if (!DB::table($weChatPay->getTable())->insert([
                'batch'         => $batch,
                'out_trade_no'  => $outTradeNo,
                'type'          => 2,
                'orders_id'     => 0
            ])){
                return false;
            }
            $jsSdk = $app->jssdk->appConfig($result['prepay_id']);
            \Log::debug($jsSdk);
            return $jsSdk;
        }else{
            return false;
        }
    }

    /**
     * app微信支付余额充值回调
     * @param $app
     */
    public function weChatPayRechargeNotify($app){
        $response = $app->handlePaidNotify(function ($message, $fail) {
            if ($message['return_code'] === 'SUCCESS') {
                if (array_get($message, 'result_code') === 'SUCCESS') {

                    $userRechargeModel      = new UserRechargeLog();
                    $userModel              = new Users();
                    $weChatModel            = new WechatPay();
                    $walletModel            = new UserWalletLog();
                    $userBlock            = new UserBlock();


                    $userRechargeInfo = DB::table($userRechargeModel->getTable().' as o')
                        ->select('o.*', 'u.balance')
                        ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'o.users_id')
                        ->leftJoin($weChatModel->getTable().' as w', 'w.batch', '=', 'o.out_trade_no')
                        ->where('w.out_trade_no', $message['out_trade_no'])
                        ->first();

                    try{
                        DB::beginTransaction();
                        /**
                         * 修改用户余额
                         */
                        $sendMoney  = $userBlock->getRechargeActivity($userRechargeInfo->total_fee);

                        $money = 0;
                        $activityId = 0;
                        if ($sendMoney){
                            $activityLog = DB::table($userRechargeModel->getTable())
                                ->where('users_id', $userRechargeInfo->users_id)
                                ->where('activity_id', $sendMoney->id)
                                ->first();

                            if (!$activityLog){
                                $activityId = $sendMoney->id;
                                $money = $sendMoney->send;
                                $userRechargeInfo->total_fee += $sendMoney->send;
                            }
                        }

                        if(!DB::table($userModel->getTable())
                            ->where('id', $userRechargeInfo->users_id)
                            ->increment('balance', $userRechargeInfo->total_fee)){

                            DB::rollBack();
                            \Log::debug('用户充值修改余额失败');
                            return false;
                        }

                        /**
                         * 修改订单状态
                         */
                        if(!DB::table($userRechargeModel->getTable())
                            ->where('out_trade_no', $userRechargeInfo->out_trade_no)
                            ->update([
                                'status'        => 1,
                                'total_fee'     => $userRechargeInfo->total_fee,
                                'remake'        => '充值活动赠送：'.priceIntToFloat($money).'元',
                                'activity_id'   => $activityId
                            ]))
                        {
                            DB::rollBack();
                            \Log::debug('用户充值修改微信支付状态失败');
                            return false;
                        }

                        /**
                         * 修改微信支付信息
                         */
                        if(!DB::table($weChatModel->getTable())->where('out_trade_no', $message['out_trade_no'])
                            ->update([
                                'transaction_id'        => $message['transaction_id'],
                                'out_trade_no'             => $message['out_trade_no'],
                            ])
                        ){
                            DB::rollBack();
                            \Log::debug('修改支付宝支付信息失败');
                            return false;
                        }

                        if(!DB::table($walletModel->getTable())
                            ->insert([
                                'users_id'      => $userRechargeInfo->users_id,
                                'money'         => $userRechargeInfo->total_fee,
                                'wallet_type'   => 0,
                                'stores_id'     => 0,
                                'balance'       => $userRechargeInfo->balance,
                                'type'          => 2,
                                'remake'        => '充值活动赠送：'.priceIntToFloat($money).'元'
                            ])
                        ){
                            DB::rollBack();
                            return false;
                        }


                        \Log::info("APP微信支付余额充值成功==>>>>>>>>用户id====>>>>{$userRechargeInfo->users_id}=>>>>>>>>>>充值金额=>>>>>>>>>>>>>{$userRechargeInfo->total_fee}");
                        DB::commit();
                        return true;
                    }catch (\Exception $exception){
                        DB::rollBack();
                        \Log::error('APP微信支付余额充值失败==>>>>>>>>>'.$exception);
                        return false;
                    }

                }elseif (array_get($message, 'result_code') === 'FAIL') {
                    return $fail('支付失败，请稍后再通知我');
                }

            }else {
                return $fail('通信失败，请稍后再通知我');
            }

        });

        return $response;
    }

    //更新订单信息
    public function turnoverUpdate($type, $turnover){
        $agentTurnoverModel = new AgentTurnover();
        $storesModel = new Stores();
//        查询上级信息
        try{
//          修改订单状态
            $res = DB::table($agentTurnoverModel->getTable())
                ->where('id', $turnover->id)
                ->update([
                    'pay_type'  => $type,
                    'pay_time'  => date('Y-m-d H:i:s', time()),
                    'status'    => 1,
                ]);
            if(!$res){
                DB::rollBack();
                \Log::error('修改订单状态失败');
                return false;
            }
//            上级代理/厂家加余额

            if(DB::table($storesModel->getTable())
                ->where('id', $turnover->p_id)
                ->increment('balance', $turnover->total_price) === false ){
                DB::rollBack();
                \Log::error('上级代理/厂家加余额');
                return false;
            }
            DB::commit();
            return $res;
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }


    }

    /**
     * 停车场支付宝支付
     * @param $usersId
     * @param $number
     * @param $payAmount
     * @return int|\Symfony\Component\HttpFoundation\Response
     */
    public function getParkingPayment($usersId, $number, $payAmount){
        $outTradeNo = md5($usersId.$number.time());
        $parkingOrder = new ParkingOrder();

        if (!DB::table($parkingOrder->getTable())->insert([
            'users_id'      => $usersId,
            'number'        => $number,
            'out_trade_no'  => $outTradeNo,
            'pay_type'      => 2,
            'pay_amount'    => $payAmount
        ])){
            return -1;
        }

        $data = [
            'subject'      => '新华联梦想城福街消费',
            'out_trade_no' => $outTradeNo,
            'total_amount' => $payAmount,
        ];

        $aliPayData         = Pay::alipay(config('ali-pay.parking'))->app($data);

        return $aliPayData;
    }

    public function parkingAppWeChatPayNotify($app){
        $response = $app->handlePaidNotify(function ($message, $fail) {

            if ($message['return_code'] === 'SUCCESS') {
                if (array_get($message, 'result_code') === 'SUCCESS') {
                    try{
                        DB::beginTransaction();

                        $parkingOrderModel  = new ParkingOrder();
                        $parkingBlock       = new ParkingBlock();
                        $turnoverModel      = new Turnover();
                        $weChatModel        = new WechatPay();
                        $userCouponModel    = new UserCoupons();
                        $parkingInfo        = $parkingBlock->getParkingUserPayAmount($message['out_trade_no']);

                        $orderDetail = DB::table($parkingOrderModel->getTable())
                            ->where('orderNo', $message['out_trade_no'])
                            ->first();
                        if (!$orderDetail){
                            return false;
                        }

                        if (!empty($orderDetail->coupons_id) && $orderDetail->discount_amount > 0){
                            if(DB::table($userCouponModel->getTable())
                                    ->where('id', $orderDetail->coupons_id)
                                    ->update(['is_use' => 1]) === false
                            ){
                                DB::rollBack();
                                \Log::debug('修改用户优惠券信息失败');
                                return false;
                            }
                        }

                        if(!DB::table($weChatModel->getTable())
                            ->insert([
                                'out_trade_no'      => $message['out_trade_no'],
                                'transaction_id'    => $message['transaction_id'],
                                'type'              => 4
                            ])
                        ){
                            \Log::debug('添加微信支付日志失败');
                            DB::rollBack();
                            return false;
                        }

                        if(!DB::table($parkingOrderModel->getTable())
                            ->where('orderNo', $message['out_trade_no'])
                            ->update([
                                'payTime'      => date('Y-m-d H:i:s', time()),
                                'pay_status'    => 2,
                                'pay_type'      => 1
                            ])
                        ){
                            DB::rollBack();
                            \Log::debug('修改订单失败');
                            return false;
                        }

                        $turnover = DB::table($turnoverModel->getTable())
                            ->where('date', date('Y-m-d', time()))
                            ->first();
                        if ($turnover){
                            if(!DB::table($turnoverModel->getTable())
                                ->where('date', date('Y-m-d', time()))
                                ->increment('turnover', $parkingInfo->pay_amount)
                            ){
                                DB::rollBack();
                                \Log::debug('修改平台营业额日志失败');
                                return false;
                            }
                        }else{
                            if(!DB::table($turnoverModel->getTable())
                                ->insert([
                                    'date'      => date('Y-m-d', time()),
                                    'turnover'  => $parkingInfo->pay_amount
                                ])
                            ){
                                DB::rollBack();
                                \Log::debug('添加平台营业额日志失败');
                                return false;
                            }
                        }

                        $keTuo      = new Ketuo();
                        $feeResult  = $keTuo->payParkingFee2(
                            $message['out_trade_no'],
                            $parkingInfo->pay_amount,
                            $parkingInfo->discount_amount,
                            5,
                            $parkingInfo->users_id,
                            $parkingInfo->discount_time
                        );
                        \Log::debug(json_encode($feeResult));


                        DB::commit();
                        return true;
                    }catch (\Exception $exception){
                        DB::rollBack();
                        \Log::error($exception);
                        return false;
                    }
                }elseif (array_get($message, 'result_code') === 'FAIL') {
                    return $fail('支付失败，请稍后再通知我');
                }

            }else {
                return $fail('通信失败，请稍后再通知我');
            }
        });

        return $response;
    }

    /**
     * 停车场支付宝支付回调
     * @param $notifyData
     * @return mixed
     */
    public function parkingAliPay($notifyData){

        if ($notifyData->trade_status == 'TRADE_SUCCESS' || $notifyData->trade_status == 'TRADE_FINISHED '){

            $parkingOrderModel  = new ParkingOrder();
            $parkingBlock       = new ParkingBlock();
            $turnoverModel      = new Turnover();
            $userCouponModel    = new UserCoupons();
            $parkingInfo        = $parkingBlock->getParkingUserPayAmount($notifyData->out_trade_no);

            try{
                $orderDetail = DB::table($parkingOrderModel->getTable())
                    ->where('orderNo', $notifyData->out_trade_no)
                    ->first();
                if (!$orderDetail){
                    \Log::error('未找到停车订单');
                    return false;
                }

                if (!empty($orderDetail->coupons_id) && $orderDetail->discount_amount > 0){
                    if(DB::table($userCouponModel->getTable())
                        ->where('id', $orderDetail->coupons_id)
                        ->update(['is_use' => 1]) === false
                    ){
                        DB::rollBack();
                        \Log::error('修改用户优惠券信息失败');
                        return false;
                    }
                }


                if(!DB::table($parkingOrderModel->getTable())
                    ->where('orderNo', $notifyData->out_trade_no)
                    ->update([
                        'payTime'      => date('Y-m-d H:i:s', time()),
                        'pay_status'    => 2,
                        'pay_type'      => 2
                    ])
                ){
                    DB::rollBack();
                    \Log::error('修改用户余额失败');
                    return false;
                }

                $turnover = DB::table($turnoverModel->getTable())
                    ->where('date', date('Y-m-d', time()))
                    ->first();
                if ($turnover){
                    if(!DB::table($turnoverModel->getTable())
                        ->where('date', date('Y-m-d', time()))
                        ->increment('turnover', $parkingInfo->pay_amount)
                    ){
                        DB::rollBack();
                        \Log::error('修改平台营业额日志失败');
                        return false;
                    }
                }else{
                    if(!DB::table($turnoverModel->getTable())
                        ->insert([
                            'date'      => date('Y-m-d', time()),
                            'turnover'  => $parkingInfo->pay_amount
                        ])
                    ){
                        DB::rollBack();
                        \Log::error('添加平台营业额日志失败');
                        return false;
                    }
                }

                $keTuo      = new Ketuo();
                $feeResult  = $keTuo->payParkingFee2(
                    $notifyData->out_trade_no,
                    $parkingInfo->pay_amount,
                    $parkingInfo->discount_amount,
                    5,
                    $parkingInfo->users_id,
                    $parkingInfo->discount_time
                );
                \Log::debug(json_encode($feeResult));

                DB::commit();
                return true;
            }catch (\Exception $exception){
                \Log::error($exception);
                DB::rollBack();
                return false;
            }

        }
    }

    public function manualOrderRefundPay($orderId){
        $orderModel             = new Orders();
        $orderRefundModel       = new OrderRefund();
        $orderGoodsModel        = new OrderGoods();
        $userModel              = new Users();
        $storeModel             = new Stores();
        $userWalletModel        = new UserWalletLog();
        $storeIncomeModel       = new StoreIncomeLog();

        try{
            $order = DB::table($orderModel->getTable().' as o')
                ->select('o.*', 's.name as store_name')
                ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'o.stores_id')
                ->where('o.id', $orderId)
                ->first();

            DB::beginTransaction();
            if ($order->pay_type != 3 || $order->status == 0 || $order->status == 5){
                return -1;
            }

            if ($order->status == 3){
                if(!DB::table($storeModel->getTable())
                    ->where('id', $order->stores_id)
                    ->decrement($order->pay_amount)
                ){
                    \Log::debug('减少商家余额失败');
                    DB::rollBack();
                    return false;
                }

                //添加商户余额日志
                if(!DB::table($storeIncomeModel->getTable())->insert([
                    'stores_id'     => $order->stores_id,
                    'orders_id'     => $orderId,
                    'money'         => $order->pay_amount,
                    'create_time'   => date('Y-m-d H:i:s', time()),
                    'type'          => 3
                ])){
                    DB::rollBack();
                    \Log::debug('添加商户余额日志失败');
                    return false;
                }
            }

            $orderGoodsTotal = DB::table($orderGoodsModel->getTable())
                ->where('orders_id', $orderId)
                ->get()->toArray();

            $data = [];
            foreach ($orderGoodsTotal as $item) {
                $data['users_id']               = $order->users_id;
                $data['stores_id']              = $order->stores_id;
                $data['orders_id']              = $order->id;
                $data['refund_transaction_id']  = md5($orderId.$item->goods_id);
                $data['order_goods_num']        = count($orderGoodsTotal);
                $data['order_goods_id']         = $item->id;
                $data['goods_amount']           = $item->selling_price;
                $data['goods_sku']              = $item->sku;
                $data['goods_quantity']         = $item->quantity;
                $data['pay_amount']             = $order->pay_amount;
                $data['refund_amount']          = $order->pay_amount;
                $data['message']                = '退款成功';
                $data['cause']                  = '手动退款';
                $data['type']                   = $order->pay_type;

                if(!DB::table($orderGoodsModel->getTable())
                    ->where('id', $item->id)
                    ->update([
                        'refund_amount'     => $item->selling_price,
                        'is_refund'         => 1
                    ])
                ){
                    \Log::debug('修改订单商品失败');
                    DB::rollBack();
                    return  false;
                }
            }

            if(!DB::table($orderRefundModel->getTable())
                ->insert($data)){
                \Log::debug('手动退款,添加退款订单失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($userModel->getTable())
                ->where('id', $order->users_id)
                ->increment('balance', $order->pay_amount)
            ){
                \Log::debug('手动退款，添加用户余额失败');
                DB::rollBack();
                return false;
            }

            if(!DB::table($userWalletModel->getTable())
                ->insert([
                    'users_id'          => $order->users_id,
                    'money'             => $order->pay_amount,
                    'balance'           => DB::table($userModel->getTable())->where('id', $order->users_id)->value('balance'),
                    'stores_id'         => 2,
                    'type'              => 1,
                    'remake'            => '手动退款：'.$order->store_name.'商家退款，订单号：'.$order->order_sn
                ])
            ){
                \Log::debug('手动退款：添加用户余额日志失败');
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        }catch(\Exception $exception){
            \Log::error('手动退款失败：'.$exception);
           DB::rollBack();
           return false;
        }


    }

    /**
     * 拼团微信支付
     * @param $groupLogId
     * @param $type
     * @return array
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function groupBuyWeChatPay($groupLogId, $type){
        $orderGroupLogModel     = new OrderGroupBuyLog();
        $orderGroupModel        = new OrderGroupBuy();

        $orderLog   = DB::table($orderGroupLogModel->getTable().' as ogl')
            ->select('ogl.*', 'og.selling_price')
            ->leftJoin($orderGroupModel->getTable().' as og', 'og.id', '=', 'ogl.orders_id')
            ->where('ogl.id', $groupLogId)
            ->first();

        $app = Factory::payment(\Config::get('zhongxin-pay.app'));
        $unifyData = [
            'body' => '新华联梦想城福街消费',
            'out_trade_no' => $orderLog->order_no,
            'total_fee' => 1,//$orderLog->selling_price,
            'spbill_create_ip' => get_server_ip(),
            'notify_url' => env('APP_URL').'/pluto/order/group/app/wechat/pay/notify',
            'trade_type' => 'APP'
        ];
        if ($type == 'app'){
            $unifyData['trade_type'] = 'APP';
        }elseif($type == 'h5'){
            $unifyData['trade_type'] = 'MWEB';
        }
        $result = $app->order->unify($unifyData);

        if ($type == 'app'){
            return $app->jssdk->appConfig($result['prepay_id']);
        }elseif ($type == 'h5') {
            return $app->jssdk->sdkConfig($result['prepay_id'], false);
        }

    }

    public function groupBuyWeChatPayNotify($app){
        $response = $app->handlePaidNotify(function ($message, $fail) {
            $orderGroupModel            = new OrderGroupBuy();
            $orderGroupLogModel         = new OrderGroupBuyLog();
            $weChatPayModel             = new WechatPay();
            $orderModel                 = new Orders();
            $orderGoodsModel            = new OrderGoods();
            $goodsModel                 = new Goods();

            $groupBuy = DB::table($orderGroupLogModel->getTable().' as ogl')
                ->select('ogl.*', 'og.selling_price', 'og.total_num', 'g.name as goods_name', 'g.thumbnail')
                ->leftJoin($orderGroupModel->getTable().' as og', 'og.id', '=', 'ogl.orders_id')
                ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'og.goods_id')
                ->where('order_no', $message['out_trade_no'])
                ->first();

            if (!$groupBuy){
                \Log::error("订单支付会调:拼团订单不存在,out_trade_no:{$message['out_trade_no']}");
                return false;
            }

            if ($groupBuy->is_pay == 1){
                \Log::error("订单支付会调:拼团订单已支付,out_trade_no:{$message['out_trade_no']}");
                return false;
            }


            if ($message['return_code'] === 'SUCCESS') {
                if (array_get($message, 'result_code') === 'SUCCESS') {
                    \Log::debug('拼团微信支付回调数据：'.json_encode($message));
                    try{

                        DB::beginTransaction();
                        if(!DB::table($orderGroupLogModel->getTable())
                            ->where('order_no', $message['out_trade_no'])
                            ->update([
                                'is_pay'            => 1,
                                'pay_type'          => 2,
                                'transaction_id'    => $message['transaction_id']
                            ])
                        ){
                            \Log::debug('修改订单日志失败');
                            DB::rollBack();
                            return false;
                        }

                        $groupBuySaveData = [
                            'total_num'       => DB::raw("total_num - 1")
                        ];
                        if ($groupBuy->is_self == 1){
                            $groupBuySaveData['is_open']    = 1;
                        }

                        if(!DB::table($orderGroupModel->getTable())
                            ->where('id', $groupBuy->orders_id)
                            ->update($groupBuySaveData)
                        ){
                            \Log::debug('修改主订单失败');
                            DB::rollBack();
                            return false;
                        }

                        if(!DB::table($weChatPayModel->getTable())
                            ->insert([
                                'orders_id'         => $groupBuy->id,
                                'out_trade_no'      => $message['out_trade_no'],
                                'transaction_id'    => $message['transaction_id'],
                                'type'              => 5
                            ])
                        ){
                            \Log::debug('添加支付宝支付记录失败');
                            DB::rollBack();
                            return false;
                        }

                        //拼团人数满足 && 团长已开团
                        if ($groupBuy->total_num - 1 == 0 && $groupBuy->is_open == 1){
                            $groupBuyLog = DB::table($orderGroupLogModel->getTable())
                                ->where('orders_id', $groupBuy->id)
                                ->get();

                            $orderGoodsData = [];
                            foreach ($groupBuyLog as $key => $item) {
                                $orderData                = [];
                                $orderData['batch']       = $item->users_id.(microtime(true) * 10000).mt_rand(1000, 9999);
                                $orderData['company_id']  = 1;
                                $orderData['users_id']    = $item->users_id;
                                $orderData['stores_id']   = $groupBuy->stores_id;
                                $orderData['order_sn']    = $item->order_no;
                                $orderData['goods_price'] = $groupBuy->original_price;
                                $orderData['pay_time']    = $groupBuy->selling_price;
                                $orderData['pay_type']    = $item->pay_type;
                                $orderData['status']      = 2;
                                $orderData['remark']      = $item->remake;
                                $orderData['type']        = 1;
                                $orderData['pay_mode']    = 1;
                                $orderData['activity_type'] = 2;

                                $orderId = DB::table($orderModel->getTable())
                                    ->insertGetId($orderData);
                                if (!$orderId){
                                    \Log::debug('添加订单记录失败');
                                    DB::rollBack();
                                    return false;
                                }

                                $updateData             = [];
                                $updateData['code']     = getSalt();
                                $path = public_path().'/qrcode/junanzenorder/';
                                if (!is_dir(public_path().'/qrcode')){
                                    @mkdir(public_path().'/qrcode', 0755);
                                }
                                if (!is_dir($path)){
                                    @mkdir($path, 0755);
                                }
                                $imageName = $orderId.time().'.png';
                                $code       = getSalt();
                                $codeResult = QrCode::format('png')
                                    ->size(400)
                                    ->encoding('UTF-8')
                                    ->generate(
                                        $code,
                                        "{$path}/$imageName"
                                    );
                                if ($codeResult){
                                    $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                                }

                                if(!DB::table($orderModel->getTable())
                                    ->where('id', $orderId)
                                    ->update($updateData)
                                ){
                                    \Log::debug('修改订单记录失败');
                                    DB::rollBack();
                                    return false;
                                }

                                $orderGoodsData[$key]['orders_id'] = $orderId;
                                $orderGoodsData[$key]['goods_id'] = $groupBuy->goods_id;
                                $orderGoodsData[$key]['name'] = $groupBuy->goods_name;
                                $orderGoodsData[$key]['thumbnail'] = $groupBuy->thumbnail;
                                $orderGoodsData[$key]['selling_price'] = $groupBuy->selling_price;
                                $orderGoodsData[$key]['quantity'] = 1;
                            }

                            if(!DB::table($orderGoodsModel->getTable())
                                ->insert($orderGoodsData)
                            ){
                                \Log::debug('添加订单商品失败');
                                DB::rollBack();
                                return false;
                            }

                        }

                        DB::commit();
                        return true;
                    }catch (\Exception $exception){
                        DB::rollBack();
                        \Log::error($exception);
                        return false;
                    }
                }elseif (array_get($message, 'result_code') === 'FAIL') {
                    return $fail('支付失败，请稍后再通知我');
                }

            }else {
                return $fail('通信失败，请稍后再通知我');
            }
        });

        return $response;
    }

    /**
     * 支付宝支付
     * @param $groupLogId
     * @return false|string
     */
    public function groupBuyAliPay($groupLogId){
        $orderGroupLogModel     = new OrderGroupBuyLog();
        $orderGroupModel        = new OrderGroupBuy();

        $orderLog   = DB::table($orderGroupLogModel->getTable().' as ogl')
            ->select('ogl.*', 'og.selling_price')
            ->leftJoin($orderGroupModel->getTable().' as og', 'og.id', '=', 'ogl.orders_id')
            ->where('ogl.id', $groupLogId)
            ->first();

        $data = [
            'subject'      => '新华联梦想城福街拼团消费',
            'out_trade_no' => $orderLog->order_no,
            'total_amount' => 0.01//priceIntToFloat($orderLog->selling_price),
        ];

        return Pay::alipay(config('ali-pay.group'))->app($data)->getContent();
    }

    /**
     * 拼团支付宝支付回调
     * @param $notifyData
     * @return array|bool
     */
    public function groupBuyAliPayNotify($notifyData){
        \Log::debug('支付宝回调信息：'.json_encode($notifyData));

        /**
         * 支付成功
         */
        if ($notifyData->trade_status == 'TRADE_SUCCESS' || $notifyData->trade_status == 'TRADE_FINISHED '){

            $orderGroupModel            = new OrderGroupBuy();
            $orderGroupLogModel         = new OrderGroupBuyLog();
            $aliPayModel                = new Alipay();
            $orderModel                 = new Orders();
            $orderGoodsModel            = new OrderGoods();
            $goodsModel                 = new Goods();

            $groupBuy = DB::table($orderGroupLogModel->getTable().' as ogl')
                ->select('ogl.*', 'og.selling_price', 'og.total_num', 'g.name as goods_name', 'g.thumbnail')
                ->leftJoin($orderGroupModel->getTable().' as og', 'og.id', '=', 'ogl.orders_id')
                ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'og.goods_id')
                ->where('order_no', $notifyData->out_trade_no)
                ->first();
            if (!$groupBuy){
                return [
                    'code'          => 'failed',
                    'message'       => '订单不存在'
                ];
            }

            if ($groupBuy->is_pay == 1){
                return [
                    'code'          => 'failed',
                    'message'       => '订单已支付'
                ];
            }

            try{
                DB::beginTransaction();

                if(!DB::table($orderGroupLogModel->getTable())
                    ->where('order_no', $notifyData->out_trade_no)
                    ->update([
                        'is_pay'            => 1,
                        'pay_type'          => 2,
                        'transaction_id'    => $notifyData->transaction_id
                    ])
                ){
                    \Log::debug('修改订单日志失败');
                    DB::rollBack();
                    return false;
                }

                $groupBuySaveData = [
                    'total_num'       => DB::raw("total_num - 1")
                ];
                if ($groupBuy->is_self == 1){
                    $groupBuySaveData['is_open']    = 1;
                }

                if(!DB::table($orderGroupModel->getTable())
                    ->where('id', $groupBuy->orders_id)
                    ->update($groupBuySaveData)
                ){
                    \Log::debug('修改主订单失败');
                    DB::rollBack();
                    return false;
                }

                if(!DB::table($aliPayModel->getTable())
                    ->insert([
                        'orders_id'         => $groupBuy->id,
                        'out_trade_no'      => $notifyData->out_trade_no,
                        'trade_no'          => $notifyData->trade_no,
                        'total_amount'      => $groupBuy->selling_price,
                        'seller_id'         => $notifyData->seller_id,
                        'type'              => 5
                    ])
                ){
                    \Log::debug('添加支付宝支付记录失败');
                    DB::rollBack();
                    return false;
                }

                if ($groupBuy->total_num - 1 == 0 && $groupBuy->is_open == 1){
                    $groupBuyLog = DB::table($orderGroupLogModel->getTable())
                        ->where('orders_id', $groupBuy->id)
                        ->get();

                    $orderGoodsData = [];
                    foreach ($groupBuyLog as $key => $item) {
                        $orderData                = [];
                        $orderData['batch']       = $item->users_id.(microtime(true) * 10000).mt_rand(1000, 9999);
                        $orderData['company_id']  = 1;
                        $orderData['users_id']    = $item->users_id;
                        $orderData['stores_id']   = $groupBuy->stores_id;
                        $orderData['order_sn']    = $item->order_no;
                        $orderData['goods_price'] = $groupBuy->original_price;
                        $orderData['pay_time']    = $groupBuy->selling_price;
                        $orderData['pay_type']    = $item->pay_type;
                        $orderData['status']      = 2;
                        $orderData['remark']      = $item->remake;
                        $orderData['type']        = 1;
                        $orderData['pay_mode']    = 1;
                        $orderData['activity_type'] = 2;

                        $orderId = DB::table($orderModel->getTable())
                            ->insertGetId($orderData);
                        if (!$orderId){
                            \Log::debug('添加订单记录失败');
                            DB::rollBack();
                            return false;
                        }

                        $updateData             = [];
                        $updateData['code']     = getSalt();
                        $path = public_path().'/qrcode/junanzenorder/';
                        if (!is_dir(public_path().'/qrcode')){
                            @mkdir(public_path().'/qrcode', 0755);
                        }
                        if (!is_dir($path)){
                            @mkdir($path, 0755);
                        }
                        $imageName = $orderId.time().'.png';
                        $code       = getSalt();
                        $codeResult = QrCode::format('png')
                            ->size(400)
                            ->encoding('UTF-8')
                            ->generate(
                                $code,
                                "{$path}/$imageName"
                            );
                        if ($codeResult){
                            $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                        }

                        if(!DB::table($orderModel->getTable())
                            ->where('id', $orderId)
                            ->update($updateData)
                        ){
                            \Log::debug('修改订单记录失败');
                            DB::rollBack();
                            return false;
                        }

                        $orderGoodsData[$key]['orders_id'] = $orderId;
                        $orderGoodsData[$key]['goods_id'] = $groupBuy->goods_id;
                        $orderGoodsData[$key]['name'] = $groupBuy->goods_name;
                        $orderGoodsData[$key]['thumbnail'] = $groupBuy->thumbnail;
                        $orderGoodsData[$key]['selling_price'] = $groupBuy->selling_price;
                        $orderGoodsData[$key]['quantity'] = 1;
                    }

                    if(!DB::table($orderGoodsModel->getTable())
                        ->insert($orderGoodsData)
                    ){
                        \Log::debug('添加订单商品失败');
                        DB::rollBack();
                        return false;
                    }

                }

                DB::commit();
                return true;
            }catch (\Exception $exception){
                DB::rollBack();
                \Log::error($exception);
                return false;
            }

        }
    }

    public function groupBuyBalancePay($groupLogId, $userId){
        $orderGroupModel            = new OrderGroupBuy();
        $orderGroupLogModel         = new OrderGroupBuyLog();
        $orderModel                 = new Orders();
        $orderGoodsModel            = new OrderGoods();
        $goodsModel                 = new Goods();
        $userModel                  = new Users();

        $groupBuy = DB::table($orderGroupLogModel->getTable().' as ogl')
            ->select(
                'ogl.*',
                'og.selling_price',
                'og.total_num',
                'g.name as goods_name',
                'g.thumbnail',
                'og.is_open'
            )
            ->leftJoin($orderGroupModel->getTable().' as og', 'og.id', '=', 'ogl.orders_id')
            ->leftJoin($goodsModel->getTable().' as g', 'g.id', '=', 'og.goods_id')
            ->where('ogl.id', $groupLogId)
            ->first();


        if (!$groupBuy){
            return [
                'code'      => 'failed',
                'message'   => '订单记录不存在'
            ];
        }

        $userInfo  = DB::table($userModel->getTable())->where('id', $userId)->first();
        if ($userInfo->balance < $groupBuy->selling_price){
            return [
                'code'      => 'failed',
                'message'   => '余额不足，请充值'
            ];
        }

        try{
            DB::beginTransaction();

            if(!DB::table($orderGroupLogModel->getTable())
                ->where('id', $groupLogId)
                ->update([
                    'is_pay'            => 1,
                    'pay_type'          => 3
                ])
            ){
                \Log::debug('修改订单日志失败');
                DB::rollBack();
                return [
                    'code'      => 'failed',
                    'message'   => '未知错误'
                ];
            }

            $groupBuySaveData = [
                'total_num'       => DB::raw("total_num - 1")
            ];
            if ($groupBuy->is_self == 1){
                $groupBuySaveData['is_open']    = 1;
            }

            if(!DB::table($orderGroupModel->getTable())
                ->where('id', $groupBuy->orders_id)
                ->update($groupBuySaveData)
            ){
                \Log::debug('修改主订单失败');
                DB::rollBack();
                return [
                    'code'      => 'failed',
                    'message'   => '未知错误'
                ];
            }

            if ($groupBuy->total_num - 1 == 0 && $groupBuy->is_open == 1){
                \Log::debug(json_encode($groupBuy));
                $groupBuyLog = DB::table($orderGroupLogModel->getTable())
                    ->where('orders_id', $groupBuy->id)
                    ->get();

                $orderGoodsData = [];
                foreach ($groupBuyLog as $key => $item) {
                    $orderData                = [];
                    $orderData['batch']       = $item->users_id.(microtime(true) * 10000).mt_rand(1000, 9999);
                    $orderData['company_id']  = 1;
                    $orderData['users_id']    = $item->users_id;
                    $orderData['stores_id']   = $groupBuy->stores_id;
                    $orderData['order_sn']    = $item->order_no;
                    $orderData['goods_price'] = $groupBuy->original_price;
                    $orderData['pay_time']    = $groupBuy->selling_price;
                    $orderData['pay_type']    = $item->pay_type;
                    $orderData['status']      = 2;
                    $orderData['remark']      = $item->remake;
                    $orderData['type']        = 1;
                    $orderData['pay_mode']    = 1;
                    $orderData['activity_type'] = 2;

                    $orderId = DB::table($orderModel->getTable())
                        ->insertGetId($orderData);
                    if (!$orderId){
                        \Log::debug('添加订单记录失败');
                        DB::rollBack();
                        return [
                            'code'      => 'failed',
                            'message'   => '未知错误'
                        ];
                    }

                    $updateData             = [];
                    $updateData['code']     = getSalt();
                    $path = public_path().'/qrcode/junanzenorder/';
                    if (!is_dir(public_path().'/qrcode')){
                        @mkdir(public_path().'/qrcode', 0755);
                    }
                    if (!is_dir($path)){
                        @mkdir($path, 0755);
                    }
                    $imageName = $orderId.time().'.png';
                    $code       = getSalt();
                    $codeResult = QrCode::format('png')
                        ->size(400)
                        ->encoding('UTF-8')
                        ->generate(
                            $code,
                            "{$path}/$imageName"
                        );
                    if ($codeResult){
                        $updateData['qrcode'] = env('APP_URL').'/qrcode/junanzenorder/'.$imageName;
                    }

                    if(!DB::table($orderModel->getTable())
                        ->where('id', $orderId)
                        ->update($updateData)
                    ){
                        \Log::debug('修改订单记录失败');
                        DB::rollBack();
                        return [
                            'code'      => 'failed',
                            'message'   => '未知错误'
                        ];
                    }

                    $orderGoodsData[$key]['orders_id'] = $orderId;
                    $orderGoodsData[$key]['goods_id'] = $groupBuy->goods_id;
                    $orderGoodsData[$key]['name'] = $groupBuy->goods_name;
                    $orderGoodsData[$key]['thumbnail'] = $groupBuy->thumbnail;
                    $orderGoodsData[$key]['selling_price'] = $groupBuy->selling_price;
                    $orderGoodsData[$key]['quantity'] = 1;
                }

                if(!DB::table($orderGoodsModel->getTable())
                    ->insert($orderGoodsData)
                ){
                    \Log::debug('添加订单商品失败');
                    DB::rollBack();
                    return [
                        'code'      => 'failed',
                        'message'   => '未知错误'
                    ];
                }

            }

            DB::commit();
            return ['code'      => 'success'];
        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return [
                'code'      => 'failed',
                'message'   => '未知错误'
            ];
        }

    }


}
