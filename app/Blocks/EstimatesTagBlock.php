<?php
namespace App\Blocks;




use App\Models\GoodsEstimatesTag;
use DB;


class EstimatesTagBlock{
    private function estimatesTagSql($search){
        $goodsEstimatesModel = new GoodsEstimatesTag();
        $sql = DB::table($goodsEstimatesModel->getTable())->whereNull('delete_time');
        if(isset($search['title'])){
            $sql->where('title', 'like', "%".$search['title']."%");
        }
        if(isset($search['id'])){
            $sql->where('id', $search['id']);
        }

        return $sql;
    }
    /**
     * 获取商户服务列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function estimatesList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->estimatesTagSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $goodsEstimates = $sql->orderBy('id', 'desc')->get();

        return $goodsEstimates;
    }

    /**
     * 获取商户服务列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function estimatesListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->estimatesTagSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户服务详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function tagsInfo($search){
        $sql = $this->estimatesTagSql($search);
        $goodsEstimates = $sql->first();
        return $goodsEstimates;
    }

}
