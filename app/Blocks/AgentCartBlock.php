<?php
namespace App\Blocks;


use App\Models\AgentCarts;

use App\Models\AgentTurnover;
use App\Models\AgentTurnoverAddress;
use App\Models\AgentTurnoverGoods;
use App\Models\AgentUser;
use App\Models\Goods;
use App\Models\GoodsSku;
use App\Models\GoodsSkuName;
use App\Models\GoodsSkuValue;
use App\Models\StoreAddress;
use App\Models\StoreInfo;
use App\Models\Stores;
use DB;


class AgentCartBlock{
    /**
     * 添加或者更新购物车
     * @param $data
     * @param $agentId
     * @param $pId
     * @param $millId
     * @param $goodsId
     * @param $companyId
     */
    public function addCart($data, $storeId, $pId, $millId, $goodsId, $companyId){
        if (empty($data)){
            \Log::error('参数错误');
            return false;
        }

        try{
            DB::beginTransaction();
            $agentCartModel = new AgentCarts();

            foreach ($data as $k=>&$v){
                $cartSql = DB::table($agentCartModel->getTable())
                    ->where('mill_id', $millId)
                    ->where('p_id', $pId)
                    ->where('store_id', $storeId)
                    ->where('company_id', $companyId)
                    ->where('goods_id', $goodsId);

                $cartInfo = $cartSql->where('sku_path', $v['sku_path'])->first();
                if (!$cartInfo){
                    $cartData[] =
                        [
                            'mill_id' => $millId,
                            'p_id' => $pId,
                            'goods_id' => $goodsId,
                            'store_id' => $storeId,
                            'company_id' => $companyId,
                            'quantity' => $v['quantity'],
                            'sku_path' => $v['sku_path'],
                            'sku_value' => $v['sku_value']
                        ];

                }else{
                    $res = $cartSql->increment('quantity', $v['quantity']);
                    if (!$res){
                        DB::rollBack();
                        \Log::error('更新购物车失败');
                        return false;
                    }
                }
            }
            if (isset($cartData)){
                $res = DB::table('agent_carts')->insert($cartData);
            }

            if (!$res){
                DB::rollBack();
                \Log::error('添加购物车失败');
                return false;
            }
            DB::commit();
            return $res;

        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }

    /**
     * 商品规格列表
     * @param $goodsId
     */
    public function skuList($goodsId){
        $skuModel       = new GoodsSku();
        $goodsModel     = new Goods();
        $skuList = DB::table($skuModel->getTable(). ' as sku')
            ->select(
                'sku.*',
                'g.name as goods_name'

            )
            ->leftJoin($goodsModel->getTable() . ' as g', 'sku.goods_id', '=', 'g.id')
            ->where('goods_id', $goodsId)->get();
        //获取价格区间的规格所在位置
        $skunameModel       = new GoodsSkuName();
        $skuName = DB::table($skunameModel->getTable(). ' as sku_name')->where('goods_id', $goodsId)->get();
        $skuNamePosition = null;

        foreach ($skuName as $k=>$v){
            if ($v->name == "价格区间"){
                $skuNamePosition = $k;
            }

        }
        if (!$skuList->isEmpty()){

            $sukList = json_decode(json_encode($skuList), true);
            foreach ($sukList as $k=>$v){
                $skuValue[$k]['value_path'] = $v['value_path'];
                $skuValue[$k]['price']      = $v['selling_price'];
            }
            $skuValueModel = new GoodsSkuValue();
//dd($skuValue);
            foreach ($skuValue as $k=>$v){
                $v['value_path'] = explode(',', $v['value_path']);
                $skuName = $skuValueModel::whereIn('id', $v['value_path'])->get()->toArray();

                $string = '';
                foreach ($skuName as $key=>$value){
                    $string .= $value['value'].' ';


                }

                if ($skuNamePosition){
                    $numInterval = explode("~", $skuName[$skuNamePosition]['value']);
                }else{
                    $numInterval = [1, 'max'];
                }
                $arr[$k]['sku_value']       = $string;
                $arr[$k]['num_interval']    = [
                    'min'=>(int)$numInterval[0],
                    'max'=>$numInterval[1] == 'max'? 'max'  : (int)$numInterval[1]
                ];
                $arr[$k]['price']           = $v['price'];
            }

            foreach ($arr as $k=>$v){
                $newArr[] = [
                    'goods_id'      => $sukList[$k]['goods_id'],
                    'sku_path'      => $sukList[$k]['value_path'],
                    'sku_value'     => $v['sku_value'],
                    'sku_price'     => $v['price']/100,
                    'num_interval'  => $v['num_interval'],
                    'goods_name'    => $sukList[$k]['goods_name']
                ];
            }
        }else{

            $good = DB::table($goodsModel->getTable())->where('id', $goodsId)->first();
            $newArr[] = [
                'goods_id'      => $good->id,
                'sku_path'      => '',
                'sku_value'     => '',
                'sku_price'     => $good->selling_price/100,
                'num_interval'  => ['min'=>1, 'max'=>'max'],
                'goods_name'    => $good->name
            ];
        }

        return ($newArr);

    }


    private function __cartsSql($search){
        $goodsModel    = new Goods();
        $agentCart     = new AgentCarts();
        $storeModel    = new Stores();
        $sukModel      = new GoodsSku();
        $sql = DB::table($agentCart->getTable() . ' as ac')
            ->select(
                'ac.*',
                's.name',
                'g.name as goods_name'
            )
            ->leftJoin($goodsModel->getTable() . ' as g', 'g.id', '=', 'ac.goods_id')
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'ac.mill_id');
        if (isset($search['store_id'])){
            $sql->where('ac.store_id', $search['store_id']);
        }

        if (isset($search['mill_id'])){
            $sql->where('ac.mill_id', $search['mill_id']);
        }

        return $sql;
    }

    /**
     * 获取购物车列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function cartsList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->__cartsSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);
        $carts = $sql->orderby('ac.id', 'desc')->get();

        return $carts;
    }

    public function cartsListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__cartsSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    /**
     * 创建入库单
     * @param $data
     */
    public function turnoverAdd($data){
        $cartModel              = new AgentCarts();
        $goodModel              = new Goods();
        $sukModel               = new GoodsSku();
        $agentTurnoverModel     = new AgentTurnover();
        $agentTurnoverGoodModel = new AgentTurnoverGoods();
//        $data = [
//            ['id'=>26,'p_id'=>2,'mill_id'=>2,'company_id'=>1,'agent_id'=>12,'quantity'=>5,'goods_id'=>19,'sku_path'=>'','sku_value'=>''],
//            ['id'=>25,'p_id'=>2,'mill_id'=>2,'company_id'=>1,'agent_id'=>12,'quantity'=>10,'goods_id'=>14,'sku_path'=>'13','sku_value'=>'传统辣味']
//        ];
//        echo json_encode($data);die;
        try{
            DB::beginTransaction();
            //添加入库单数据

            $quantity   = 0;
            $totalPrice = 0;
            foreach ($data as $k => &$v) {
                $quantity += $v['quantity'];
                $storeId   = $v['store_id'];
                $mill_id   = $v['mill_id'];
                $pId       = $v['p_id'];
                //计算总价格
                if ($v['sku_path']){
                    $price = $sukModel::where('value_path', $v['sku_path'])->first();
                    if (empty($price)){
                        DB::rollBack();
                        \Log::error('价格错误');
                        return false;
                    }
                    $totalPrice +=$price->selling_price*$v['quantity'];
                }else{
                    $price = $goodModel::where('id', $v['goods_id'])->first();
                    if (empty($price)){
                        DB::rollBack();
                        \Log::error('价格错误');
                        return false;
                    }
                    $totalPrice +=$price->selling_price*$v['quantity'];
                }
                $v['price'] = $price->selling_price;
                $v['total_price'] = $price->selling_price*$v['quantity'];

            }
            $turnoverData = [
                'store_id'      => $storeId,
                'mill_id'       => $mill_id,
                'p_id'          => $pId,
                'number'        => $quantity,
                'turnover_no'   => "CR".time().mt_rand(10,99),
                'total_price'   => $totalPrice,
            ];
            $res = DB::table($agentTurnoverModel->getTable())->insertGetId($turnoverData);
            if (!$res){
                DB::rollBack();
                \Log::error('创建入库单失败');
                return false;
            }
            //添加入库单商品数据
            foreach ($data as $k => &$v){
                $turnoverGoodData[] = [
                    'goods_id'      => $v['goods_id'],
                    'turnover_id'   => $res,
                    'sku_path'      => $v['sku_path'],
                    'sku_value' => $v['sku_value'],
                    'price' => $v['price'],
                    'total_price' => $v['total_price'],
                    'number' => $v['quantity'],
                ];
                $cartIds[] = $v['id'];
            }
            $turnoverGoodDataRes = DB::table($agentTurnoverGoodModel->getTable())->insert($turnoverGoodData);
            if (!$turnoverGoodDataRes){
                DB::rollBack();
                \Log::error('创建入库单失败');
                return false;
            }
            $cartDel = DB::table($cartModel->getTable())->whereIn('id', $cartIds)->delete();
            if (!$cartDel){
                DB::rollBack();
                \Log::error('删除购物车失败');
                return false;
            }

            DB::commit();
            return $res;

        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }

    /**
     * 创建入库单(立即购买)
     * @param $data
     */
    public function turnoverAddNow($data){
        $cartModel                  = new AgentCarts();
        $goodModel                  = new Goods();
        $sukModel                   = new GoodsSku();
        $agentTurnoverModel         = new AgentTurnover();
        $agentTurnoverGoodModel     = new AgentTurnoverGoods();
        $storeAddressModel          = new StoreAddress();
        $agentTurnoverAddressModel  = new AgentTurnoverAddress();
        try{
            DB::beginTransaction();
            //添加入库单数据

            $quantity   = 0;
            $totalPrice = 0;
            foreach ($data['sku'] as $k => &$v) {
                $quantity += $v['quantity'];
                //计算总价格
                if ($v['sku_path']){
                    $price = $sukModel::where('value_path', $v['sku_path'])->first();
                    if (empty($price)){
                        DB::rollBack();
                        \Log::error('价格错误');
                        return false;
                    }
                    $totalPrice +=$price->selling_price*$v['quantity'];
                }else{
                    $price = $goodModel::where('id', $v['goods_id'])->first();
                    if (empty($price)){
                        DB::rollBack();
                        \Log::error('价格错误');
                        return false;
                    }
                    $totalPrice +=$price->selling_price*$v['quantity'];
                }
                $v['price'] = $price->selling_price;
                $v['total_price'] = $price->selling_price*$v['quantity'];

            }
            $turnoverData = [
                'store_id'      => $data['store_id'],
                'mill_id'       => $data['mill_id'],
                'p_id'          => $data['p_id'],
                'number'        => $quantity,
                'turnover_no'   => "CR".time().mt_rand(10,99),
                'total_price'   => $totalPrice,
                'remake'   => $totalPrice,
            ];
            $res = DB::table($agentTurnoverModel->getTable())->insertGetId($turnoverData);
            if (!$res){
                DB::rollBack();
                \Log::error('创建入库单失败');
                return false;
            }
            //获得选择的地址数据
            $address = DB::table($storeAddressModel->getTable())
                ->where('id', $data['address_id'])
                ->first();

            if(!isset($address->id)){
                DB::rollBack();
                \Log::error('地址数据有误');
                return $address;
            }

            //添加订单地址数据
            if(
            !DB::table($agentTurnoverAddressModel->getTable())
                ->insert([
                    'turnover_id'   => $res,
                    'consignee'     => $address->consignee,
                    'contact'       => $address->contact,
                    'province'      => $address->province,
                    'city'          => $address->city,
                    'district'      => $address->district,
                    'address'       => $address->address
                ])
            ){
                DB::rollBack();
                \Log::error('添加订单地址数据失败');
                return false;
            }
            //添加入库单商品数据
            foreach ($data['sku'] as $k => &$v){
                $turnoverGoodData[] = [
                    'goods_id'      => $v['goods_id'],
                    'turnover_id'   => $res,
                    'sku_path'      => $v['sku_path'],
                    'sku_value' => $v['sku_value'],
                    'price' => $v['price'],
                    'total_price' => $v['total_price'],
                    'number' => $v['quantity'],
                ];
//                $cartIds[] = $v['id'];
            }
            $turnoverGoodDataRes = DB::table($agentTurnoverGoodModel->getTable())->insert($turnoverGoodData);
            if (!$turnoverGoodDataRes){
                DB::rollBack();
                \Log::error('创建入库单失败');
                return false;
            }

            $messageBlock = new MessageBlock();
            $content = "您有新的供货订单，请注意查收。";
            $messageId = $messageBlock->messageAdd($data['p_id'], $content);

            if (!$messageId){
                DB::rollBack();
                \Log::error('消息添加失败');
                return false;
            }

            if(isset($data['cart'])){
                $cartDel = DB::table($cartModel->getTable())->whereIn('id', $data['cart'])->delete();
                if (!$cartDel){
                    DB::rollBack();
                    \Log::error('删除购物车失败');
                    return false;
                }
            }


            DB::commit();
            return $res;

        }catch (\Exception $exception){
            DB::rollBack();
            \Log::error($exception);
            return false;
        }

    }



    private function __cartSql($search){
        $goodsModel         = new Goods();
        $agentCartModel     = new AgentCarts();
        $storeModel         = new Stores();
        $sukModel           = new GoodsSku();
        $sql = DB::table($agentCartModel->getTable() . ' as ac')
            ->select(
                'ac.*',
                's.name',
//                's.id as mill_id',
                's.logo',
                'g.name as goods_name'
            )
            ->leftJoin($goodsModel->getTable() . ' as g', 'g.id', '=', 'ac.goods_id')
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'ac.mill_id');
        if (isset($search['store_id'])){
            $sql->where('ac.store_id', $search['store_id']);
        }


        return $sql;
    }

    /**
     * 获取购物车列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function cartList($search, $page, $pageSize, $asc = [], $desc = []){
        $sql = $this->__cartSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);
        $cartStore = $sql->groupBy('mill_id')->get();

        $storeIds = [];
        foreach ($cartStore as $cs){
            $storeIds[] = $cs->mill_id;
        }

        $list = [];
        foreach ($cartStore as $k=>$v){
            $list[$k]['mill_id']    = $v->mill_id;
            $list[$k]['p_id']       = $v->p_id;
            $list[$k]['store_name'] = $v->name;
            $list[$k]['logo']       = $v->logo;
            $goods                  = $this->cart(['store_id' => $v->store_id, 'mill_id' => $v->mill_id])->get();
            foreach ($goods as $g){
                if ($g->sku_selling_price){
                    $g->selling_price   = priceIntToFloat($g->sku_selling_price);
                    $g->purchase_price  = priceIntToFloat($g->sku_purchase_price);
                    $g->original_price  = priceIntToFloat($g->sku_original_price);
                }else{
                    $g->selling_price   = priceIntToFloat($g->g_selling_price);
                    $g->purchase_price  = priceIntToFloat($g->g_purchase_price);
                    $g->original_price  = priceIntToFloat($g->g_original_price);
                }
                unset($g->g_selling_price);
                unset($g->g_purchase_price);
                unset($g->g_original_price);
                unset($g->sku_selling_price);
                unset($g->sku_purchase_price);
                unset($g->sku_original_price);
            }
            $list[$k]['goods']      = $goods;
        }

        return $list;
    }

    public function cartListPagination(array $search, int $page = 1, int $pageSize = 20){
        $sql = $this->__cartSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }

    public function cart(array $search){
        $agentCartModel = new AgentCarts();
        $goodsModel = new Goods();
        $goodsSkuModel = new GoodsSku();
        $storeModel = new Stores();

        $sql = DB::table($agentCartModel->getTable() . ' as ac')
            ->select(
                'ac.*',
                'g.name',
                'g.thumbnail',
                'g.stock as stock',
                'g.selling_price as g_selling_price',
                'g.purchase_price as g_purchase_price',
                'g.original_price as g_original_price',
                'g.enable_sku',
                'gsku.selling_price as sku_selling_price',
                'gsku.purchase_price as sku_purchase_price',
                'gsku.original_price as sku_original_price',
                'gsku.stock as sku_stock',
                's.logo',
                's.name as store_name'
            )
            ->leftJoin($goodsModel->getTable() . ' as g', 'ac.goods_id', '=', 'g.id')
            ->leftJoin($goodsSkuModel->getTable() . ' as gsku', 'ac.sku_path', '=', 'gsku.value_path')
            ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'ac.p_id')
            ->whereNull('g.delete_time');
        if (isset($search['store_id'])){
            $sql->where('ac.store_id', $search['store_id']);
        }
        if (isset($search['mill_id'])){
            $sql->where('ac.mill_id', $search['mill_id']);
        }

        if(isset($search['store_ids'])){
            $sql->whereIn('ac.store_id', $search['store_ids']);
        }


        return $sql;
    }


    /**
     * 确认订单数据
     * @param $data
     */
    public function confirmTurnover($data){
//        上级丶厂家丶自己的信息
        $storeModel     = new Stores();
        $storeInfoModel = new StoreInfo();
        $store = DB::table($storeModel->getTable() . ' as s')
            ->select(
                's.id',
                's.name',
                's.real_name',
                's.logo'
            )
            ->leftJoin($storeInfoModel->getTable() . ' as si', 's.id', '=', 'si.store_id')
            ->where('s.id', $data['store_id'])
            ->first();

        $pStore = DB::table($storeModel->getTable() . ' as p')
            ->select(
                'p.id',
                'p.name',
                'p.real_name',
                'p.freight_type',
                'p.logo'
            )
            ->leftJoin($storeInfoModel->getTable() . ' as pi', 'p.id', '=', 'pi.store_id')
            ->where('p.id', $data['p_id'])
            ->first();

        $mStore = DB::table($storeModel->getTable() . ' as m')
            ->select(
                'm.id',
                'm.name',
                'm.real_name',
                'm.logo'
            )
            ->leftJoin($storeInfoModel->getTable() . ' as mi', 'm.id', '=', 'mi.store_id')
            ->where('m.id', $data['mill_id'])
            ->first();
//        运费
        $freight = $pStore->freight_type;
//        商品数据
        $goodsBlock = new GoodsBlock();
        $sukModel   = new GoodsSku();
        $number = 0;
        $species = 0;

        foreach ($data['sku'] as $k=>$v){
            if (!isset($v['goods_id'])){
//                商品id有误
                return false;
            }else{
                $search['goodids'][] = $v['goods_id'];
            }
            $number += (int)$v['quantity'];
            $species++;
        }

        $goods = $goodsBlock->goodsList($search, 1, count($search['goodids']));

        $goods = json_decode(json_encode($goods), true);
//        组合数据
        $totalPrice = 0;
        foreach ($goods as $k=>$v){
            $good[$k]['id']            = $v['id'];
            $good[$k]['name']          = $v['name'];
            $good[$k]['slogan']        = $v['slogan'];
            $good[$k]['thumbnail']     = $v['thumbnail'];
            $good[$k]['quantity']      = $data['sku'][$k]['quantity'];
            $good[$k]['sku_path']      = $data['sku'][$k]['sku_path'];
            $good[$k]['sku_value']     = $data['sku'][$k]['sku_value'];
            if (empty($data['sku'][$k]['sku_path']) && empty($data['sku'][$k]['sku_value'])){
//              没有规格取商品价格

                $total = $v['selling_price'] * $data['sku'][$k]['quantity'] ;
                $price = $v['selling_price'];
            }else{
//              有规格时按规格价格
                $price = $sukModel::where('value_path', $data['sku'][$k]['sku_path'])->first();
                $total = priceIntToFloat($price->selling_price*$data['sku'][$k]['quantity']);
                $price = priceIntToFloat($price->selling_price);
            }

            $good[$k]['price']         = $price;
            $good[$k]['total_price']   = priceIntToFloat($total*100);
            $totalPrice += (int)$total;
        }
        $res = [
            's'             => $store,
            'p'             => $pStore,
            'm'             => $mStore,
            'goods'         => $good,
            'cart'          => $data['cart'],
            'freight'       => $freight,
            'number'        => $number,
            'total_price'   => priceIntToFloat($totalPrice*100),
            'goods_species' => $species,
        ];
        return $res;

    }

}