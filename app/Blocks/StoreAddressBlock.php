<?php

namespace App\Blocks;


use App\Models\StoreAddress;
use App\Models\District;
use DB;


class StoreAddressBlock
{
    private function addressSql($search)
    {
        $storeAddressModel = new StoreAddress();
        $districtModel      = new District();
        $sql = DB::table($storeAddressModel->getTable() . ' as sa')
            ->select([
                'sa.*',
                'pd.name as province_name',
                'cd.name as city_name',
                'dd.name as district_name',
            ])
            ->leftJoin($districtModel->getTable() . ' as pd', 'pd.id', '=', 'sa.province')
            ->leftJoin($districtModel->getTable() . ' as cd', 'cd.id', '=', 'sa.city')
            ->leftJoin($districtModel->getTable() . ' as dd', 'dd.id', '=', 'sa.district')
            ->whereNull('delete_time');


        if (isset($search['id'])) {
            $sql->where('sa.id', $search['id']);
        }
        if (isset($search['store_id'])) {
            $sql->where('sa.store_id', $search['store_id']);
        }

        return $sql;
    }

    /**
     * 获取商户地址列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeAddressList($search, $page, $pageSize, $asc = [], $desc = [])
    {
        $sql = $this->addressSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $storeTags = $sql->orderBy('is_default', 'desc')->get();

        return $storeTags;
    }

    /**
     * 获取商户地址列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function storeAddressListPagination(array $search, int $page = 1, int $pageSize = 20)
    {
        $sql = $this->addressSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }


    /**
     * 获取商户地址详情
     * @param $search
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function storeAddressInfo($search)
    {
        $sql = $this->addressSql($search);
        $storeTags = $sql->first();

        return $storeTags;
    }


//保存商户地址
    public function saveStoreAddress($data)
    {
        $storeAddressModel = new StoreAddress();
//        判断是否有地址
        $storeAddress = DB::table($storeAddressModel->getTable())
            ->where('store_id', $data['store_id'])
            ->whereNull('delete_time')
            ->get();
//        判断是否有默认地址
        $storeAddressDefault = DB::table($storeAddressModel->getTable())
            ->where('store_id', $data['store_id'])
            ->where('is_default', 1)
            ->whereNull('delete_time')
            ->first();

        try {

            DB::beginTransaction();

            if (!$storeAddress->isEmpty()){
                if ($storeAddressDefault->id && $data['is_default'] == 1){
//                    去掉之前的默认地址
                    $defaultUpdate = DB::table($storeAddressModel->getTable())
                        ->where('id', $storeAddressDefault->id)
                        ->update([
                            'is_default' => 0
                        ]);
                    if ($defaultUpdate === false) {
                        \Log::error('修改之前的默认地址失败');
                        DB::rollBack();
                        return false;
                    }
                    $res = DB::table($storeAddressModel->getTable())->insertGetId($data);
                    if ($res === false) {
                        \Log::error('添加默认商户地址失败');
                        DB::rollBack();
                        return false;
                    }
                }else{
                    $res = DB::table($storeAddressModel->getTable())->insertGetId($data);
                    if ($res === false) {
                        \Log::error('添加商户地址失败');
                        DB::rollBack();
                        return false;
                    }
                }
            }else{
                $data['is_default'] = 1;
                $res = DB::table($storeAddressModel->getTable())->insertGetId($data);
                if ($res === false) {
                    \Log::error('添加商户地址失败');
                    DB::rollBack();
                    return false;
                }

            }

            DB::commit();
            return true;
        } catch (\Exception $exception) {
            \Log::error($exception);
            return false;
        }


    }

//修改商户地址
    public function usaveStoreAddress($data)
    {
        $storeAddressModel = new StoreAddress();
        $res = DB::table($storeAddressModel->getTable())->where('id', $data['id'])
            ->update($data);
        return $res;


    }

}
