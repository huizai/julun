<?php

namespace App\Blocks;


use App\Models\PageNav;
use App\Models\StoreDevices;
use App\Models\StoreNavs;
use App\Models\StoreTags;
use DB;


class NavsBlock
{
    private function navSql($search)
    {
        $pageNavModel = new PageNav();
        $sql = DB::table($pageNavModel->getTable())->whereNull('delete_time');
        if (isset($search['name'])) {
            $sql->where('name', 'like', "%" . $search['name'] . "%");
        }
        if (isset($search['id'])) {
            $sql->where('id', $search['id']);
        }
        if (isset($search['store_id'])) {
            $sql->where('stores_id', $search['store_id']);
        }

        return $sql;
    }

    /**
     * 页面配置信息列表
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function NavsList($search, $page, $pageSize, $asc = [], $desc = [])
    {
        $sql = $this->navSql($search);
        $sql->skip(($page - 1) * $pageSize)->take($pageSize);

        $navs = $sql->orderBy('id', 'desc')->get();


        return $navs;
    }

    /**
     * 获取页面配置信息列表分页信息
     * @param $search
     * @param $page
     * @param $pageSize
     */
    public function NavsListPagination(array $search, int $page = 1, int $pageSize = 20)
    {
        $sql = $this->navSql($search);
        return [
            'total' => $sql->count(),
            'pageSize' => $pageSize,
            'current' => $page,
        ];
    }



}
