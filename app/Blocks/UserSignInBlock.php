<?php
namespace  App\Blocks;

use App\Models\UserIntegralLog;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use App\Models\UserSign;
use App\Models\UserSignLog;

class UserSignInBlock{

    /**
     * 签到
     * @param $userId
     * @return array|bool|int
     */
    public function userSignIn($userId){
        $signInModel        = new UserSign();
        $signInLogModel     = new UserSignLog();
        $userIntegralModel  = new UserIntegralLog();
        $userModel          = new Users();

        /**
         * 查看今天是否签到
         */
        if(DB::table($signInLogModel->getTable())
            ->where('users_id', $userId)
            ->where('time', date('Y-m-d', time()))
            ->first()
        ){
            return -1;
        }

        $signIn = DB::table($signInModel->getTable())
            ->where('users_id', $userId)
            ->where('time', date('Y-m', time()))
            ->first();

        try{
            DB::beginTransaction();

            /**
             * 添加签到次数
             */
            $integral = 1;
            $data = [];
            $data['integral']       = $integral;
            $data['grand_total']    = 0;
            if ($signIn){
                $signInTime = date('Y-m-d', strtotime($signIn->update_time));
                $today      = date('Y-m-d', strtotime("-1 day"));
                $data['continuous_total'] = $signInTime != $today ? 1 : $signIn->continuous_total + 1;

                if(!DB::table($signInModel->getTable())
                    ->where('id', $signIn->id)
                    ->update([
                        'total'             => DB::raw('total + 1'),
                        'continuous_total'  => $signInTime != $today ? 1 : DB::raw('continuous_total + 1')
                    ])
                ){
                    \Log::debug('添加签到次数失败');
                    DB::rollBack();
                    return false;
                }
                /**
                 * 计算签到积分
                 */
                switch ($data['continuous_total']){
                    case 7:
                        $data['grand_total'] = 5;
                        $integral += 5;
                        break;
                    case 14:
                        $data['grand_total'] = 10;
                        $integral += 10;
                        break;
                    case date('t'):
                        $data['grand_total'] = 15;
                        $integral += 15;
                        break;
                }

            }else{
                if(!DB::table($signInModel->getTable())
                    ->insert([
                        'users_id'          => $userId,
                        'time'              => date('Y-m', time()),
                        'total'             => 1,
                        'continuous_total'  => 1
                    ])
                ){
                    \Log::debug('添加签到次数失败');
                    DB::rollBack();
                    return false;
                }
            }

            /**
             * 添加签到日志
             */
            if(!DB::table($signInLogModel->getTable())
                ->insert([
                    'users_id'  => $userId,
                    'time'      => date('Y-m-d', time())
                ])
            ){
                \Log::Debug('添加签到日志失败');
                DB::rollBack();
                return false;
            }

            /**
             * 添加积分明细日志
             */
            if(!DB::table($userIntegralModel->getTable())
                ->insert([
                    'users_id'      => $userId,
                    'vary'          => 1,
                    'integral'      => $integral,
                    'source'        => '签到'
                ])
            ){
                \Log::debug('添加积分明细日志失败');
                DB::rollBack();
                return false;
            }
            /**
             * 添加用户积分
             */
            if(!DB::table($userModel->getTable())
                ->where('id', $userId)
                ->increment('integral', $integral)
            ){
                \Log::debug('添加用户积分失败');
                DB::rollBack();
                return false;
            }

            /**
             * 判断用户当前积分是否能升级
             */
            if(!$userModel->userUpgrade($userId)){
                \Log::debug('用户升级失败');
                DB::rollBack();
                return false;
            }

            DB::commit();
            return $data;
        }catch (\Exception $exception){
            \Log::error('签到失败'.$exception);
            DB::rollBack();
            return false;
        }


    }

    /**
     * 获取我的签到时间
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function getMySignTime($userId){
        $userSignLogModel       = new UserSignLog();
        return DB::table($userSignLogModel->getTable())
            ->where('users_id', $userId)
            ->where('time', 'like', date('Y-m', time()).'%')
            ->get();
    }

    public function getMonthSign($userId, $time){
        $userSignModel          = new UserSign();
        return DB::table($userSignModel->getTable())
            ->where('users_id', $userId)
            ->where('time', $time)
            ->first();
    }
}