<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-26
 * Time: 下午4:35
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgentRegRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'company_id' => 'required',
//            'username' => 'required',
//            'password' => 'required',
            'real_name' => 'required',
            'mobile' => 'required',
            'code' => 'required',
            'agent_type' => 'required'
//            'company' => 'required',
//            'type' => 'required'
        ];
    }
}