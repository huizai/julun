<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-26
 * Time: 下午4:35
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgentMsgRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd(11);
        return [
            'username' => 'bail|required',
            'id' => 'bail|required',
//            'password' => 'bail|required',
            'real_name' => 'bail|required',
            'mobile' => 'bail|required',
            'company' => 'bail|required',
            'main_type' => 'bail|required'

        ];
    }
}