<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact' => [
                'required',
                'regex:/^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,3,6,7,8]{1}\d{8}$|^18[\d]{9}$/'
            ],
            'consignee' => 'required',
            'province'  => 'required',
            'city'      => 'required',
            'district'  => 'required',
            'address'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'contact.required'          => '请输入手机号码',
            'contact.regex'             => '请输入正确的手机号码',
            'consignee.required'        => '请输入联系人',
            'province.required'         => '请选择省份',
            'city.required'             => '请选择市',
            'district.required'         => '请选择区',
            'address.required'          => '请输入详细地址'
        ];
    }
}
