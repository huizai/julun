<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-26
 * Time: 下午4:35
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopSetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tel' => 'bail|required',
            'logo' => 'bail|required',
            'title' => 'bail|required',
        ];
    }
}