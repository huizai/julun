<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminStoreInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required',
            'mobile'                => 'required',
            'logo'                  => 'required',
            'address'               => 'required',
            'long'                  => 'required',
            'lat'                   => 'required',
            'business_license'      => 'required',
            'am'                    => 'required',
            'pm'                    => 'required',
            'contact'               => 'required',
            'id'                    => 'required',
        ];
    }
}
