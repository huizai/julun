<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-26
 * Time: 下午4:35
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'        => 'bail|required|numeric',
            'mill_id'   => 'bail|required|numeric',
            'p_id'      => 'bail|required|numeric',
            'store_id'  => 'bail|required|numeric',
            'cart_data' => 'required'

        ];
    }
}