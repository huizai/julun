<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'store_name'                => 'required',      //店铺名称
            'principal'                 => 'required',      //负责人
            'mobile'                    => 'required',      //商家联系电话
            'position'                  => 'required',      //商家店铺位置
            'area'                      => 'required',      //商家店铺面积
            'rent'                      => 'required',      //商家租金
            'payment_time'              => 'required',      //商家缴费日期
            'lease_start_time'          => 'required',      //租赁开始时间
            'lease_end_time'            => 'required',      //租赁结束时间
            'type'                      => 'required',      //合作方式
            'status'                    => 'required',      //状态
        ];
    }


    public function messages()
    {
        return [
            'principal.required'            => '请输入负责人姓名',
            'mobile.required'               => '请输入商家联系电话',
            'position.required'             => '请输入商家店铺位置',
            'area.required'                 => '请输入商家店铺面积',
            'rent.required'                 => '请输入商家租金',
            'payment_time.required'         => '请输入商家缴费日期',
            'lease_start_time.required'     => '请输入租赁开始时间',
            'lease_end_time.required'       => '请输入租赁结束时间',
            'type.required'                 => '请选择合作方式',
            'status.required'               => '请选择缴费状态',
        ];
    }
}
