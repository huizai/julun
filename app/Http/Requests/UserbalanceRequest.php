<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-26
 * Time: 下午5:45
 */

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UserbalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'user_id' => 'bail|required|numeric',
            'number' => 'bail|required|numeric|min:0',
            'type' => 'bail|required|numeric|min:0|max:1',
            'money_type' => 'bail|required|numeric|min:0|max:1'

        ];
    }
}