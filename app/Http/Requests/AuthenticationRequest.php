<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-26
 * Time: 下午4:35
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthenticationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'agent_id'              => 'required',
                'organ_type'            => 'required',
                'license_num'           => 'required',
                'province'              => 'required',
                'city'                  => 'required',
                'district'              => 'required',
                'street'                => 'required',
                'address'               => 'required',
                'scope'                 => 'required',
                'license_img'           => 'required',
//                'intelligence_img'      => 'required',
//                'trademark_img'         => 'required',
                'bank_num'              => 'required',
                'bank_username'         => 'required',
                'bank_name'             => 'required',
                'person_name'           => 'required',
                'person_number'         => 'required',
                'person_id_card_z'      => 'required',
                'person_id_card_f'      => 'required'
            ];
    }
}