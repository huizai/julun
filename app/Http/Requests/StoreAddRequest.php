<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'classifity_id'         => 'required',
            'name'                  => 'required',
            'mobile'                => 'required',
            'password'              => 'required',
            'logo'                  => 'required',
            'address'               => 'required',
            'long'                  => 'required',
            'lat'                   => 'required',
            'business_license'      => 'required',
            'am'                    => 'required',
            'pm'                    => 'required',
            'contact'               => 'required',
            'store_type_id'         => 'required',

        ];
    }
}
