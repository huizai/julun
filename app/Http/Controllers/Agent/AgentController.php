<?php

namespace App\Http\Controllers\Agent;

use App\Blocks\StoreBlock;
use App\Http\Controllers\Controller;
use App\Http\Requests\AgentMsgRequest;
use App\Libs\ResponseMessage;
use App\Models\AgentUser;
use App\Models\AgentUserInfo;
use App\Models\Banks;
use App\Models\Stores;
use Illuminate\Http\Request;
use App\Http\Requests\AgentRegRequest;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\AuthenticationRequest;
use App\Blocks\AgentBlock;
use Illuminate\Support\Facades\Validator;


class AgentController extends Controller{
    /**
     * 代理登陆
     * @param AdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(AdminRequest $request){
        $data = $request->only(['username', 'password']);
        $data['password'] = md5($data['password']);
        $agentUserModel = new AgentUser();
        $agent = $agentUserModel::where('mobile','=',$data['username'])->where('password','=',$data['password'])->first();
        if (!$agent){
            return response()->json(ResponseMessage::getInstance()->failed("NO_USER")->response());
        }

        $session['mobile']  = session('mobile',$agent['mobile']);
        $session['company_id']  = session('company_id',$agent['company_id']);
        $session['company']  = session('company',$agent['company']);
        $session['agent_id']  = session('agent_id',$agent['id']);
        $session['type']  = session('type',$agent['type']);
        $session['username']  = session('username',$agent['username']);
        $session['loginTime'] = session('loginTime',time());
        $token = md5($session['mobile'].$session['loginTime']);
        \Cache::put($token, $session, 24*60*30);
        $res = [
            'mobile'=>$agent->mobile,
//            'real_name'=>$agent->real_name,
            'username'=>$agent->username,
            'id'=>$agent->id,
            'token'=>$token,
            'company'=>$agent->company,
            'avatar'=>$agent->avatar,
            'type'=>$agent->type,

        ];

        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }
    /**
     * 代理注册
     * @param AgentRegRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentReg(AgentRegRequest $request){
        $data = $request->only([
            'real_name',
            'code',
            'mobile',
            'p_mobile',
            'company_id',
            'agent_type'
        ]);
        $data['password'] = $request->get('password', '123456');
        $data['re_password'] = $request->get('re_password', '123456');
        if ($data['password'] != $data['re_password'] ){
            return response()->json(ResponseMessage::getInstance()
                    ->setFailedCode("REG_EXIST",'两次密码不一致')->failed()->response());
        }
        $data['password'] = md5($data['password']);

//        dd($data['password']);
        $data['username'] = "lansiu-".uniqid();

        $data['avatar']   = "https://img.alicdn.com/imgextra/i1/2880673396/O1CN01GmOzdU1axPXV2BSOr_!!2880673396.jpg_310x310.jpg";

        $agentUserModel = new AgentUser();
        if (\Cache::get($data['mobile'])!==$data['code']){
                return response()->json(ResponseMessage::getInstance()
                    ->setFailedCode("REG_EXIST",'验证码错误')->failed()->response());
        }
//        验存
        $agent = $agentUserModel::where('mobile', $data['mobile'])->first();
        if (isset($agent->id)){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("REG_EXIST",'此手机号已注册代理')->failed()->response());
        }
        $res = $agentUserModel::create($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("REG_EXIST",'注册失败')->failed()->response());
        }

    }


    /**
     * 代理主营类型  待定
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentType(){
        $data = [
            ['id'=>1,'name'=>"家具"],
            ['id'=>2,'name'=>"电器"],
            ['id'=>3,'name'=>"生物"],
            ['id'=>4,'name'=>"食品"],
            ['id'=>5,'name'=>"衣物"],
            ['id'=>6,'name'=>"家具"],
            ['id'=>7,'name'=>"家具"],
            ['id'=>8,'name'=>"家具"],
            ['id'=>9,'name'=>"家具"],
        ];
        $AgentModel = new AgentUser();

    }

    /**
     * 账号认证
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setAuthentication(Request $request){
        $data = $request->only([
            'organ_type',
            'longitude',
            'latitude',
            'license_num',
            'province',
            'city',
            'district',
            'street',
            'address',
            'scope',
            'business_license',
            'intelligence_img',
            'trademark_img',
            'bank_num',
            'bank_username',
            'bank_name',
            'person_name',
            'person_number',
            'person_id_card_z',
            'person_id_card_f',
            'store_id',
        ]);
        $data['remake']     = $request->get('remake',null);
//        $data['status']     = $request->get('status',0);
        $validator = Validator::make($data, [
            'store_id'              => 'required',
            'organ_type'            => 'required',
            'license_num'           => 'required',
            'longitude'             => 'required',
            'latitude'              => 'required',
            'province'              => 'required',
            'city'                  => 'required',
            'district'              => 'required',
            'street'                => 'required',
            'address'               => 'required',
            'scope'                 => 'required',
            'business_license'      => 'required',
            'bank_num'              => 'required',
            'bank_username'         => 'required',
            'bank_name'             => 'required',
            'person_name'           => 'required',
            'person_number'         => 'required',
            'person_id_card_z'      => 'required',
            'person_id_card_f'      => 'required'
        ], [
            'store_id.required'         => '商户id不能为空！！',
            'organ_type.required'       => '机构类型不能为空！！',
            'license_num.required'      => '营业执照注册号不能为空！',
            'longitude.required'        => '经纬度不能为空！！',
            'latitude.required'         => '经纬度不能为空！！',
            'province.required'         => '省不能为空！',
            'city.required'             => '市不能为空！',
            'district.required'         => '区/县不能为空！',
            'street.required'           => '街道/镇不能为空',
            'address.required'          => '地址不能为空',
            'scope.required'            => '营业范围不能为空',
            'business_license.required' => '营销执照扫描件不能为空',
            'bank_num.required'         => '银行对公账号不能为空',
            'bank_username.required'    => '对公账号名不能为空',
            'bank_name.required'        => '开户行不能为空',
            'person_name.required'      => '法人姓名不能为空',
            'person_number.required'    => '法人身份证号不能为空',
            'person_id_card_z.required' => '法人身份证号正面不能为空',
            'person_id_card_f.required' => '法人身份证号反面不能为空'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }

        $agentBlock = new AgentBlock();
        $res = $agentBlock->setAgentInfo($data,$data['store_id']);

        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->setFailedCode('AGENT_EXIST', $res)->failed()->response());
        }
    }

    /**
     * 银行列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function bank(){
        $bank = new Banks();
        $list = $bank->get();
        return response()->json(ResponseMessage::getInstance()->success($list)->response());

    }


    /**
     * 账号详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentInfo(Request $request){
        $id         = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $agentBlock = new AgentBlock();
        $res = $agentBlock->agentInfo($id);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }


    /**
     * 基本信息填写
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentMsgUpdate(Request $request){
        $data = $request->only(['mobile', 'account', 'password', 'classifity_id', 'logo', 'main_type', 'real_name', 'name', 'id']);
        if (!empty($data['password'])){
            $data['password'] = md5($data['password']);
        }else{
            unset($data['password']);
        }
        $validator = Validator::make($data, [
            'mobile'             => 'required',
            'account'            => 'required',
            'classifity_id'      => 'required',
            'logo'               => 'required',
            'main_type'          => 'required',
            'real_name'          => 'required',
            'name'               => 'required',
            'id'                 => 'required'
        ], [
            'id.required'               => '商户id不能为空！！',
            'mobile.required'           => '手机号码不能为空！！',
            'classifity_id.required'    => '分类id不能为空！',
            'logo.required'             => '商户logo不能为空！',
            'main_type.required'        => '营业范围不能为空',
            'real_name.required'        => '真实姓名不能为空',
            'name.required'             => '店铺名称不能为空'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }

        $data['operate'] = intval($request->get('operate', false));

        $data['agency'] = intval($request->get('agency', false));

        $agentBlock = new AgentBlock();
        $res = $agentBlock->agentMsg($data,$data['id']);
        if ($res==true){
            $storeModel = new Stores();
            $res = $storeModel->find($data['id']);
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("AGENT_FAILED",$res)->failed()->response());
        }

    }

    /**
     * 基本信息详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentMsg(Request $request){
        $id         = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $agentBlock = new AgentBlock();
        $res = $agentBlock->agentMsgInfo($id);


        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

    /**
     * 首页信息展示
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function main(Request $request){
        $agentId = $request->get('agent_id');

        if (!isset($agentId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $agentBlock = new AgentBlock();
        $res = $agentBlock->main($agentId);

        return response()->json(ResponseMessage::getInstance()->success($res)->response());

    }



    /**
     * 厂家开启代理模式
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setAgency(Request $request){
        if(!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id     = $request->get('id');
        $status = $request->get('status', 0);
//        判断是否为厂家
        $store = Stores::where('id', $id)->value('agent_type');

        if ($store != 0){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("AGENT_FAILED",'厂家才能开启或关闭代理模式！')->failed()->response());
        }
        $res = Stores::where('id', $id)->update(['agency' => $status]);
        if ($res!==false){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }



    public function download(){
        $downloadUrl = env('DOWNLOAD_URL');

        return view('download', ['downloadUrl' => $downloadUrl]);
    }
    public function registerView(Request $request){
        $shareKey = $request->get('key');

        $user = Users::where('share_key', $shareKey)->first();

        $mobile = '';
        if($user){
            $mobile = $user->mobile;
        }

        $downloadUrl = env('DOWNLOAD_URL');

        return view('register', ['recommend_mobile' => $mobile, 'downloadUrl' => $downloadUrl]);
    }
    /**
     * 用户通过手机号密码注册
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function register(Request $request){
        $mobile             = $request->get('mobile');
        $recommendMobile    = $request->get('recommend_mobile');
        $validateCode       = $request->get('validate_code');
        $password           = $request->get('password');
        $repassword         = $request->get('repassword');

        $cacheCode = \Cache::get($mobile);

        if($validateCode != $cacheCode){
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        if($password !== $repassword){
            return response()->json(ResponseMessage::getInstance()->failed('RE_PASSWORD_ERROR')->response());
        }

        if(Users::where('mobile', $mobile)->first()){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('ERROR',  '手机号已注册')->failed()->response());
        }

        $userBlock = new UserBlock();
        $user = $userBlock->registerByMobile($mobile, $password, $recommendMobile);

        if($user){
            unset($user->password);
            return response()->json(ResponseMessage::getInstance()->success(['user' => $user, 'token' => $this->loginConfig($user)])->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }



}