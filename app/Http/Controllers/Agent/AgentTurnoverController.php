<?php

namespace App\Http\Controllers\Agent;

use App\Blocks\AgentBlock;
use App\Blocks\AgentGoodsBlock;
use App\Blocks\MessageBlock;
use App\Blocks\PayBlock;
use App\Http\Controllers\Controller;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Validator;
use App\Libs\ResponseMessage;

use App\Models\AgentTurnover;
use Illuminate\Http\Request;
use Yansongda\Pay\Pay;


class AgentTurnoverController extends Controller{
    /**
     * 出入库订单列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 20);
        $search                     = [];
        $searchField                = ['turnover_type', 'store_id', 'status', 'turnover_no'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
//        if (!isset($search['status'])){
//            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
//        }
        $agentBlock = new AgentBlock();
        $turnover = $agentBlock->turnoverList($search, $page, $pageSize);
        $pagination = $agentBlock->turnoverListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $turnover, 'pagination' => $pagination])->response());
    }

    /**
     * 出入库订单详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function turnoverInfo(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search = ['id' => $id];
        $agentBlock = new AgentBlock();
        $turnover = $agentBlock->turnoverInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($turnover)->response());

    }

    /**
     * 出入库订单审核发货
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusUpdate(Request $request){
        $id     = $request->get('id');
        $status = $request->get('status');
        if (!isset($id)&&!isset($status)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $agentTurnoverModel = new AgentTurnover();
        $res = $agentTurnoverModel->where('id', $id)->update([
            'status' => $status,
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 出入库订单发货
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendTurnover(Request $request){
        $data = $request->only([
            'turnover_id',
            'send_no',
            'send_company',
            'send_member',
            'send_mobile',
            'send_image'
        ]);
        $validator = Validator::make($data, [
            'turnover_id'                  => 'required',
            'send_no'                      => 'required',
            'send_company'                 => 'required',
            'send_member'                  => 'required',
            'send_mobile'                  => 'required',
            'send_image'                   => 'required',
        ], [
            'turnover_id.required'          => '订单id不能为空！！',
            'send_no.required'              => '发货单号不能为空！！',
            'send_company.required'         => '货运公司不能为空！',
            'send_member.required'          => '货运联系人不能为空！',
            'send_mobile.required'          => '货运联系方式不能为空！',
            'send_image.required'           => '发货凭证不能为空！',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $data['fahuo_time'] = date('Y-m-d H:i:s', time());
        $turnoverId = $data['turnover_id'];
        unset($data['turnover_id']);
        $data['status'] = 2;
        $agentTurnoverModel = new AgentTurnover();
        $res = $agentTurnoverModel->where('id', $turnoverId)->update($data);
        //        TODO::没有用事务
        $agentBlock = new AgentBlock();
        $turnover = $agentBlock->turnoverInfo([$data['turnover_id']]);

        $messageBlock = new MessageBlock();
        $content = "您在【".$turnover->m_name."】的订单已发货";
        $messageId = $messageBlock->messageAdd($data['store_id'], $content);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 出入库确认收货与取消订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelConfirm(Request $request){
        $id     = $request->get('id');
        $type   = $request->get('type');
        if (!isset($id)&&!isset($type)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $data = [
            'cancel'    => 5,
            'confirm'   => 3,
        ];
        $status = $data[$type];
        if (!isset($status)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $agentTurnoverModel = new AgentTurnover();
        $res = $agentTurnoverModel->where('id', $id)->update([
            'status' => $status,
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 订单支付凭证与地址选择
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function turnoverPay(Request $request){
        global $companyId;
        $data = $request->only([
           'turnover_id',
           'pay_type',
           'pay_image'
        ]);
        $data['pay_time'] = date('Y-m-d H:i:s', time());
        $validator = Validator::make($data, [
            'turnover_id'                   => 'required',
            'pay_type'                      => 'required',
            'pay_image'                     => 'required',
        ], [
            'turnover_id.required'                  => '订单id不能为空！！',
            'pay_type.required'                     => '支付方式不能为空！！',
            'pay_image.required'                    => '支付凭证不能为空！',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $agentTurnoverBlock = new AgentGoodsBlock();
        $res = $agentTurnoverBlock->turnoverPay($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    public function payType(){
        return response()->json(ResponseMessage::getInstance()->success([
            ['pay_type' => 1, 'pay_name' => "银行转账", 'image' => "https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/app/156404877922.png"],
            ['pay_type' => 2, 'pay_name' => "支付宝", 'image' => "https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/app/156404876392.png"],
            ['pay_type' => 3, 'pay_name' => "担保交易", 'image' => "https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/app/156404872938.png"]
        ])->response());
    }

    /**
     * 线上支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function onlinePay(Request $request){
//        AliPay
//        wxPay
        $id = $request->get('turnover_id');
        $storeId = $request->get('store_id');
        $type = $request->get('type', 'wxPay');
        $payBlock = new PayBlock();
        $data = $payBlock->onlinePay($type, $storeId, $id);
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 微信支付回调
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Yansongda\Pay\Exceptions\InvalidConfigException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function wxPayNotify(){
        $app = Factory::payment(config('wechat.lansiu'));
        \Log::info(date("Y-m-d H:i:s",time()).'订单支付回调：'.json_encode("回调成功"));
        $response = $app->handlePaidNotify(function($message, $fail){
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $agentTurnoverModel = new AgentTurnover();
            $turnover = DB::table($agentTurnoverModel->getTable())
                ->where('turnover_no', substr($message['out_trade_no'],0,strrpos($message['out_trade_no'],"_")))
                ->first();
            if (!$turnover) { // 如果订单不存在 或者 订单已经支付过了
                \Log::info(date("Y-m-d H:i:s",time()).'订单支付回调：'.json_encode("订单不存在"));
                return true; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
            }
            ///////////// <- 建议在这里调用微信的【订单查询】接口查一下该笔订单的情况，确认是已经支付 /////////////
            if ($message['return_code'] === 'SUCCESS') { // return_code 表示通信状态，不代表支付状态
                // 用户是否支付成功
                if (array_get($message, 'result_code') === 'SUCCESS') {
                    // 更新支付时间 支付状态

                    $payBlock = new PayBlock();
                    return $payBlock->turnoverUpdate(4, $turnover);

//                    try{
//                        if(
//                            DB::table($userRechargeLogModel->getTable())
//                                ->where('id', $recharge->id)
//                                ->where('status', 0)
//                                ->update([
//                                    'status'    => 1,
//                                    'pay_type'  => 1,
//                                    'pay_time'  => date('Y-m-d H:i:s', time())
//                                ]) === false
//                        ){
//                            \Log::error("订单支付回调:更新订单状态失败,out_trade_no:{$recharge->out_trade_no}");
//                            return false;
//                        }
//                        if(
//                            DB::table($usersModel->getTable())
//                                ->where('id', $recharge->users_id)
//                                ->increment('balance', $recharge->total_fee) === false
//                        ){
//                            \Log::error("订单支付回调:更新会员余额失败,out_trade_no:{$recharge->out_trade_no}");
//                            return false;
//                        }
//                        $balance = DB::table($usersModel->getTable())
//                            ->where('id', $recharge->users_id)
//                            ->pluck('balance');
//                        if(
//                            DB::table($walletModel->getTable())
//                                ->insertGetId(
//                                    [
//                                        'users_id'    => $recharge->users_id,
//                                        'money'  => $recharge->total_fee,
//                                        'wallet_type'  => 0,
//                                        'balance' => $balance
//                                    ]
//                                ) === false
//                        ){
//                            \Log::error("订单支付回调:更新会员余额失败,out_trade_no:{$recharge->out_trade_no}");
//                            return false;
//                        }
//
//                        if(
//                            DB::table($wechatPayModel->getTable())
//                                ->where('out_trade_no', $message['out_trade_no'])
//                                ->whereNull('delete_time')
//                                ->update([
//                                    'transaction_id' => $message['transaction_id']
//                                ]) === false
//                        ){
//                            DB::rollBack();
//                            \Log::error("订单支付回调:更新wechat pay中的transaction_id失败,out_trade_no:{$recharge->out_trade_no}");
//                            return false;
//                        }
//
//                        DB::commit();
//                        return true;
//                    }catch (\Exception $exception){
//                        DB::rollBack();
//                        \Log::error($exception);
//                        return false;
//                    }

                    // 用户支付失败
                } elseif (array_get($message, 'result_code') === 'FAIL') {
                    return $fail('支付失败，请稍后再通知我');
                }
            } else {
                return $fail('通信失败，请稍后再通知我');
            }
            return true; // 返回处理完成
        });
        return $response; // Laravel 里请使用：return $response 不是则 $response->send();;

    }

    /**
     * 支付宝支付回调
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Yansongda\Pay\Exceptions\InvalidConfigException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function aliPayNotify(){
        $aliPay         = Pay::alipay(config('ali-pay.lansiu'));
        $notifyData     = $aliPay->verify();
        /**
         * 支付成功
         */
        if ($notifyData->trade_status == 'TRADE_SUCCESS' || $notifyData->trade_status == 'TRADE_FINISHED'){
            $agentTurnoverModel = new AgentTurnover();
            $turnover = $agentTurnoverModel::where('turnover_no', substr($notifyData->out_trade_no,0,strrpos($notifyData->out_trade_no,"_")))
                ->first();
            // 更新支付时间 支付状态

            \Log::info(date("Y-m-d H:i:s",time()).'订单号：'. (substr($notifyData->out_trade_no,0,strrpos($notifyData->out_trade_no,"_"))));

            $payBlock = new PayBlock();
            $res =  $payBlock->turnoverUpdate(2, $turnover);
        }else{
            $res = false;
        }
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

}