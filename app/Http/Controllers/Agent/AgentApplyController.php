<?php

namespace App\Http\Controllers\Agent;

use App\Blocks\MessageBlock;
use App\Http\Controllers\Controller;
use App\Http\Requests\AgentMsgRequest;
use App\Libs\ResponseMessage;
use App\Models\AgentApply;
use App\Models\Stores;
use Illuminate\Http\Request;
use App\Blocks\AgentBlock;
use Illuminate\Support\Facades\Validator;


class AgentApplyController extends Controller{
    /**
     * 厂家列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function millList(Request $request){
        $id = $request->get('store_id');

        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search         = [];
        $searchField    = ['store_id', 'name', 'status'];

        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $agentBlock = new AgentBlock();
        $list = $agentBlock->millList($search, $page, $pageSize, $id);
        $pagination = $agentBlock->millListPagination($search, $page, $pageSize, $id);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $list, 'pagination' => $pagination])->response());
    }
    /**
     * 申请代理
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyAdd(Request $request){
        global $companyId;
        $data = $request->only(['mill_id', 'level', 'store_id']);
        $validator = Validator::make($data, [
            'mill_id'          => 'required',
            'level'            => 'required',
            'store_id'         => 'required'
        ], [
            'mill_id.required'         => '厂家id不能为空！！',
            'level.required'           => '代理等级不能为空！！',
            'store_id.required'        => '商户id不能为空！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $data['company_id'] = $companyId;
        $agentBlock = new AgentBlock();
//        代理信息
        $agent =  $agentBlock->agentInfo($data['store_id']);
        if (empty($agent->store_id) || $agent->status!=2){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("AGENT_FAILED",'账号还未认证，请认证后再申请代理')->failed()->response());
        }
        if ($agent->store_id==$data['mill_id']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("AGENT_FAILED",'自己不能申请自己')->failed()->response());
        }
        if ($agent->agent_type == 0){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("AGENT_FAILED",'厂家不能申请代理')->failed()->response());
        }

        $res = $agentBlock->agentApply($data,$agent);
        if (is_int($res)){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("AGENT_FAILED",$res)->failed()->response());
        }
    }

    /**
     * 申请信息详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyInfo(Request $request){
        global $companyId;
        $storeId = $request->get('store_id');
        $millId  = $request->get('mill_id');
        if (!isset($storeId)||!isset($millId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $agentBlock = new AgentBlock();
        $res = $agentBlock->applyInfo($storeId, $millId);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }
    /**
     * 下级代理列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyList(Request $request){
        global $companyId;
        $storeId = $request->get('id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search         = ['p_id' => $storeId];
        $searchField    = ['name', 'mill_id', 'status'];

        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $agentBlock = new AgentBlock();
        $list = $agentBlock->agentExamList($search, $page, $pageSize);
        $pagination = $agentBlock->agentExamListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $list, 'pagination' => $pagination])->response());
    }
    /**
     * 下级代理申请审核
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyExam(Request $request){
        global $companyId;
        $data = $request->only(['id', 'status', 'remake']);
        $validator = Validator::make($data, [
            'id'             => 'required',
            'status'         => 'required',
            'remake'         => 'required',
        ], [
            'id.required'            => '申请id不能为空！！',
            'status.required'        => '状态值不能为空！！',
            'remake.required'        => '审核说明不能为空！',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }

        $agentApplyModel = new AgentApply();
        $storeModel = new Stores();
        $res = $agentApplyModel::where('id', $data['id'])->update($data);

        //        TODO::没有用事务
        $apply = $agentApplyModel::where('id', $data['id'])->first();
        $mill  = $storeModel::where('id', $apply->mill_id)->first();
        $messageBlock = new MessageBlock();
        if ($data['status']==1){
            $content = "您已成为【".$mill->name."】厂家的代理";
        }else{
            $content = "您的【".$mill->name."】代理申请信息已被拒绝，请重新申请，拒绝原因：".$data['remake'];
        }
        $messageId = $messageBlock->messageAdd($apply->store_id, $content);
//        if (!$messageId){
//            DB::rollBack();
//            \Log::error('消息添加失败');
//            return false;
//        }
        if ($res===false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }

    }

    /**
     * 已代理厂家列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyMillList(Request $request){
        global $companyId;
        $storeId = $request->get('id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 100);
        $search         = ['store_id' => $storeId];
        $searchField    = [];

        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $agentBlock = new AgentBlock();
        $list = $agentBlock->applyMillList($search, $page, $pageSize);
        $pagination = $agentBlock->applyMillListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $list, 'pagination' => $pagination])->response());
    }

    /**
     * 下级代理收录列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function employAgentList(Request $request){
        global $companyId;
        $storeId = $request->get('id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search         = ['id' => $storeId];
        $searchField    = ['name', 'mill_id'];

        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $agentBlock = new AgentBlock();
        $list = $agentBlock->employAgentList($search, $page, $pageSize);
        $pagination = $agentBlock->employAgentListPagination($search, $page, $pageSize);
        if ($list){
            return response()->json(ResponseMessage::getInstance()->success(['list' => $list, 'pagination' => $pagination])->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

    }

    /**
     * 收录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function employAgentGet(Request $request){
        global $companyId;
        $data = $request->only(['id', 'store_id']);
        $validator = Validator::make($data, [
            'id'                => 'required',
            'store_id'          => 'required',
        ], [
            'id.required'               => '申请id不能为空！！',
            'store_id.required'         => '店铺id不能为空！！',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $agentApplyModel = new AgentApply();
        $res = $agentApplyModel::where('id', $data['id'])->update(['p_id' => $data['store_id']]);
        if ($res !== false){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

    }

}