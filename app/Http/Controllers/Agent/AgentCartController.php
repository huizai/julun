<?php

namespace App\Http\Controllers\Agent;

use App\Blocks\AgentCartBlock;
use App\Http\Controllers\Controller;
use App\Http\Requests\CartAddRequest;
use Illuminate\Http\Request;
use App\Models\GoodsSkuValue;
use App\Libs\ResponseMessage;
use App\Models\AgentCarts;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AgentCartController extends Controller{
    /**
     * 获取代理的购物车列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 20);
        $storeId                    = $request->get('id');
        $millId                     = $request->get('mill_id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search = ['store_id' => $storeId, 'mill_id' => $millId];
        $agentCartBlock = new AgentCartBlock();
        $carts = $agentCartBlock->cartsList($search, $page, $pageSize);
        $pagination = $agentCartBlock->cartsListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $carts, 'pagination' => $pagination])->response());
    }

    /**
     * 获取代理的购物车列表（多商户）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cartList(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 20);
        $storeId                    = $request->get('id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search = ['store_id' => $storeId];
        $agentCartBlock = new AgentCartBlock();
        $carts = $agentCartBlock->cartList($search, $page, $pageSize);
        $pagination = $agentCartBlock->cartListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $carts, 'pagination' => $pagination])->response());
    }

    /**
     * 添加或者更新购物车
     * @param CartAddRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cartAdd(CartAddRequest $request){
        global $companyId;

        $goodsId    = $request->get('id');
        $millId     = $request->get('mill_id');
        $pId        = $request->get('p_id');
        $storeId    = $request->get('store_id');
        $data       = json_decode($request->get('cart_data', json_encode([])), true);
        $agentCartBlock = new AgentCartBlock();

        $res = $agentCartBlock->addCart($data, $storeId, $pId, $millId, $goodsId, $companyId);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
    /**
     * 获取商品规格列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sukList(Request $request){
        global $companyId;

        $goodsId    = $request->get('id');
        if (!isset($goodsId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $agentCartBlock = new AgentCartBlock();

        $res = $agentCartBlock->skuList($goodsId);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 购物车删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cartDel(Request $request){
        global $companyId;
        $cartId    = $request->get('id');
        if (!isset($cartId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $cartModel = new AgentCarts();
        $res = $cartModel->whereIn('id', $cartId)
            ->delete();
        if($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 购物车修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cartUpdate(Request $request){
        global $companyId;
        $cartId    = $request->get('id');
        $quantity  = $request->get('quantity', 1);
        if (!isset($cartId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $cartModel = new AgentCarts();
        $res = $cartModel->where('id', $cartId)->update([
            'quantity' => $quantity,
        ]);
        if($res!==false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 创建入库单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function turnoverAdd(Request $request){
        global $companyId;
        $data = json_decode($request->get('cart_data', json_encode([])), true);
        if (!isset($data)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $agentCartBlock = new AgentCartBlock();
        $res = $agentCartBlock->turnoverAdd($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 创建入库单(快速购买)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function turnoverAddNow(Request $request){
        global $companyId;
        $data = $request->only([
            'mill_id',
            'p_id',
            'sku',
            'store_id',
            'cart',
            'address_id',
        ]);
        $validator = Validator::make($data, [
            'mill_id'           => 'required',
            'store_id'          => 'required',
            'sku'               => 'required',
            'address_id'        => 'required',
            'p_id'              => 'required',
        ], [
            'goods_id.required'       => '商品id不能为空！',
            'mill_id.required'        => '厂家id不能为空！',
            'sku.required'            => '商品数据不能为空！',
            'p_id.required'           => '商户id不能为空！',
            'address_id.required'     => '地址不能为空！',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $data["sku"] = json_decode($data["sku"],true);

        $data["remake"] = $request->get('remake', null);

        $agentCartBlock = new AgentCartBlock();
        $res = $agentCartBlock->turnoverAddNow($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 确认订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmTurnover(Request $request){
        global $companyId;
//        echo json_encode(
//            [
//                ['goods_id'=>'348', 'quantity'=>'1', 'sku_path'=>'', 'sku_value'=>''],
//                ['goods_id'=>'189', 'quantity'=>'2', 'sku_path'=>'189-1-1,189-2-1', 'sku_value'=>'低配 红色']
//            ]
//        );die;
        $data = $request->only([
            'mill_id',
            'p_id',
            'sku',
            'cart',
            'store_id'
        ]);
        $validator = Validator::make($data, [
            'mill_id'           => 'required',
            'store_id'          => 'required',
            'sku'               => 'required',
            'p_id'              => 'required',
        ], [
            'goods_id.required'       => '商品id不能为空！',
            'mill_id.required'        => '厂家id不能为空！',
            'sku.required'            => '商品数据不能为空！',
            'p_id.required'           => '商户id不能为空！',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $data["sku"] = json_decode($data["sku"],true);
        if (!isset($data['cart'])){
            $data['cart'] = [];
        }
        $agentCartBlock = new AgentCartBlock();
        $res = $agentCartBlock->confirmTurnover($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}