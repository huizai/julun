<?php

namespace App\Http\Controllers\Agent;

use App\Blocks\AgentGoodsBlock;
use App\Blocks\MarketBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;



class MarketController extends Controller{


    /**
     * 交易数据统计
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function turnoverMarket(Request $request){
        global $companyId;
        $storeId = $request->get('store_id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search = ['year' => $request->get('year', date('Y',time()))];
        $searchField    = [
            'month'             //月
        ];
        foreach ($searchField as $field){

            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $marketBlock = new MarketBlock();
        $list = $marketBlock->turnoverMarket($search, $storeId);
        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }

    /**
     * 商品交易数据统计
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function goodsMarket(Request $request){
        global $companyId;
        $storeId = $request->get('store_id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search = [];
        $searchField    = [
            'cid',
            'name',
            'start_time',
            'end_time',
        ];
        foreach ($searchField as $field){

            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $marketBlock = new MarketBlock();
        $list = $marketBlock->goodsMarket($search, $page, $pageSize, $storeId);
        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }
    /**
     * 商户交易数据统计
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeMarket(Request $request){
        global $companyId;
        $storeId = $request->get('store_id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search = [];
        $searchField    = [
            'name'
        ];
        foreach ($searchField as $field){

            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $marketBlock = new MarketBlock();
        $list = $marketBlock->storeMarket($search, $page, $pageSize, $storeId);
        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }
}
