<?php

namespace App\Http\Controllers\Agent;

use App\Blocks\AgentGoodsBlock;

use App\Blocks\EstimatesBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;

use App\Blocks\GoodsEstimatesBlock;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;


class EstimatesController extends Controller{


    /**
     * 获取评论列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function estimatesList(Request $request){
        global $companyId;

        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search         = [];
        $searchField    = [
            'goods_id',         //按名称搜索商品
        ];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $EstimatesBlock = new EstimatesBlock();
        $estimates      = $EstimatesBlock->estimatesList($search, $page, $pageSize);
        $pagination     = $EstimatesBlock->estimatesListPagination($search, $page, $pageSize);
        $score          = $EstimatesBlock->estimatesScore($search);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $estimates, 'pagination' => $pagination, 'store' => $score])->response());
    }

    /**
     * 获取评论详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function estimatesInfo(Request $request){
        $id     = $request->get('id');
        $token  = $request->get('token');
        if($token == 'test123'){
            $userId = 9;
        }else {
            $userInfo = \Cache::get($token);
            if(isset($userInfo->id)){
                $userId = $userInfo->id;
            }else{
                $userId = null;
            }


        }


        $goodsBlock = new AgentGoodsBlock();
        $goods = $goodsBlock->goodsInfo($id, $userId);
        return response()->json(ResponseMessage::getInstance()->success($goods)->response());
    }

    /**
     * 添加商品评论
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addEstimatesGoods(Request $request){
        $data = $request->only([
            'store_id',
            'turnover_id',
            'comment',
        ]);
        $validator = Validator::make($data, [
            'store_id'              => 'required',
            'turnover_id'           => 'required',
            'comment'               => 'required'
        ], [
            'store_id.required'             => '商户id不能为空！！',
            'turnover_id.required'          => '订单id不能为空！！',
            'comment.required'              => '评论内容不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        $comment = json_decode($data['comment'], true);
        $commentData = [];
        $imageData = [];
//        dd($comment);
        foreach ($comment as $key => $item) {

            if ($item['star'] < 1){
                return response()->json(ResponseMessage::getInstance()->failed()
                    ->setFailedCode('PARAM_ERROR','评分不能小于1分')
                    ->response());
            }

            $commentData[$key]['store_id']              = $data['store_id'];
            $commentData[$key]['goods_id']              = $item['goods_id'];
            $commentData[$key]['sku_value']             = $item['sku_value'];
            $commentData[$key]['number']                = $item['number'];
            $commentData[$key]['content']               = $item['content'];
            $commentData[$key]['star']                  = $item['star'];

            if (isset($item['image'])){
                $imageData[$key]['image']                   = $item['image'];
                $imageData[$key]['goods_id']                = $item['goods_id'];
            }

        }

        $EstimatesBlock = new EstimatesBlock();
        $result = $EstimatesBlock->addEstimatesGoods($commentData, $imageData, $data['turnover_id']);
        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }



    }
}
