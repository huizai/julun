<?php

namespace App\Http\Controllers\Agent;

use App\Blocks\AgentGoodsBlock;
use App\Blocks\GoodsBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\Goods;
use App\Models\GoodsClassifies;
use App\Models\GroupBuy;
use App\Models\Spike;
use App\Models\StoreNavs;
use App\Blocks\GoodsEstimatesBlock;
use App\Models\Stores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\StoreActivity;


class GoodsController extends Controller{


    /**
     * 获取商品列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function goodsList(Request $request){
        global $companyId;

        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search         = ['company' => $companyId, 'status' => 2];
        $searchField    = [
            'name',         //按名称搜索商品
            'cid',          //按分类搜索商品
            'agent_id',     //按代理搜索商品
        ];
        $fieldData = [
            'id'     => 'id',
            '销量'    => 'sales_volume',
            '价格'    => 'selling_price'
        ];
//        $search['operate']      = 1;//是否需要申请代理才能购买
        $search['field']        = $fieldData[$request->get('field', 'id')];
        $search['sequence']     = $request->get('sequence', 'desc');
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $goodsBlock = new AgentGoodsBlock();
        $goods = $goodsBlock->goodsList($search, $page, $pageSize);
        $pagination = $goodsBlock->goodsListPagination($search, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success(['list' => $goods, 'pagination' => $pagination])->response());
    }

    /**
     * 获取商品详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function goodsInfo(Request $request){
        $id     = $request->get('id');
        $token  = $request->get('token');
        if($token == 'test123'){
            $userId = 9;
        }else {
            $userInfo = \Cache::get($token);
            if(isset($userInfo->id)){
                $userId = $userInfo->id;
            }else{
                $userId = null;
            }


        }


        $goodsBlock = new AgentGoodsBlock();
        $goods = $goodsBlock->goodsInfo($id, $userId);
        return response()->json(ResponseMessage::getInstance()->success($goods)->response());
    }

    /**
     * 添加商品评论
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userEstimatesGoods(Request $request){
        if (!$request->filled(['comment'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $comment        = $request->post('comment');

        $commentData  = [];
        $imageData    = [];
        foreach ($comment as $key => $item) {

            if ($item['score'] < 1){
                return response()->json(ResponseMessage::getInstance()->failed()
                    ->setFailedCode('PARAM_ERROR','评分不能小于1分')
                    ->response());
            }

            $commentData[$key]['order_goods_id']    = $item['order_goods_id'];
            $commentData[$key]['goods_id']          = $item['goods_id'];
            $commentData[$key]['estimate']          = $item['estimate'];
            $commentData[$key]['score']             = $item['score'];
            $commentData[$key]['users_id']          = $g_uid;

            if (isset($item['image'])){
                $imageData[$key]['order_goods_id']      = $item['order_goods_id'];
                $imageData[$key]['image']               = $item['image'];
            }


        }


        $goodsEstimatesBlock = new GoodsEstimatesBlock();
        $result = $goodsEstimatesBlock->userEstimatesGoods($commentData, $imageData);


        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }



    }
}
