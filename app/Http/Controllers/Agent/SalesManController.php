<?php

namespace App\Http\Controllers\Agent;

use App\Blocks\AgentGoodsBlock;
use App\Blocks\MarketBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;



class SalesManController extends Controller{


    /**
     * 业务员注册
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function salesManReg(Request $request){
        $data = $request->only([
            'code',
            'mobile',

        ]);

        $data['password'] = $request->get('password', '123456');
        $data['re_password'] = $request->get('re_password', '123456');
        if ($data['password'] != $data['re_password'] ){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("REG_EXIST",'两次密码不一致')->failed()->response());
        }
        unset($data['re_password']);
        $data['password']   = md5($data['password']);
        $data['name']       = "lansiu-".uniqid();
        $data['account']    = date('Ymd').mt_rand(999, 9999);
        $data['logo']       = "https://img.alicdn.com/imgextra/i1/2880673396/O1CN01GmOzdU1axPXV2BSOr_!!2880673396.jpg_310x310.jpg";
        $data['company_id'] = $request->get('company', 1);
        $data['roles_id']   = 4;//基础角色的角色id如果删除需要变更这里的角色分配
        $data['agent_type'] = $request->get('agent_type', 1);;
        $storeModel = new Stores();

        if (\Cache::get($data['mobile'])!==$data['code']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("REG_EXIST",'验证码错误')->failed()->response());
        }
//        验存
        $store = $storeModel::where('mobile', $data['mobile'])->first();
        if (isset($store->id)){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("REG_EXIST",'此手机号已被注册')->failed()->response());
        }
        unset($data['code']);
        $res = $storeModel->saveStoreUser($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("REG_EXIST",'注册失败')->failed()->response());
        }

    }
}
