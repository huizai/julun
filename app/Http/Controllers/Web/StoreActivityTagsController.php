<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Models\StoreActivityTags;
use Validator;

class StoreActivityTagsController extends Controller{

    //减
    private $lessImage    = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153708411.png';
    //买
    private $buyImage     = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153713144.png';
    //特
    private $bargainImage = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153716328.png';
    //新
    private $newImage     = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/store_activity_tag/156153722068.png';


    /**
     * 商户活动标签列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreActivityTagsList(Request $request){
        if (!$request->get('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $stores_id = $request->get('stores_id');
        $activityTagsModel = new StoreActivityTags();
        $data = $activityTagsModel->getStoreActivityTagsList($stores_id);

        foreach ($data as $value) {
            switch ($value->type)
            {
                case 0:
                    $value->icon = $this->lessImage;
                    break;
                case 1:
                    $value->icon = $this->buyImage;
                    break;
                case 2:
                    $value->icon = $this->bargainImage;
                    break;
                case 3:
                    $value->icon = $this->newImage;
                    break;
            }
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


    /**
     * 添加商户活动标签
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addStoreActivityTags(Request $request){
        $validator = Validator::make($request->all(), [
            'stores_id'     => 'required',
            'title'         => 'required',
            'type'          => 'required'
        ],[
            'stores_id.required'    => 'STORE PARAM_ERROR',
            'title.required'        => '请输入标题！',
            'type.required'         => '请选择类型'
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $data = $request->only(['stores_id', 'title', 'type']);
        $storeActivityTagsModel = new StoreActivityTags();

        $tagsCount = $storeActivityTagsModel->where('stores_id', $data['stores_id'])->count();
        if ($tagsCount >= 4){
            return response()->json(
                ResponseMessage::getInstance()->failed()
                    ->setFailedCode('TAG_COUNT','最多只能添加4个标签！')
                    ->response()
            );
        }

        $data['create_time'] = date('Y-m-d H:i:s', time());
        $result = $storeActivityTagsModel->addStoresActivityTags($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success('添加成功')->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('ADD_ERROR')->response());
    }

    /**
     * 修改商户活动标签
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStoreActivityTags(Request $request){
        $validator = Validator::make($request->all(), [
            'title'         => 'required',
            'type'          => 'required',
            'id'            => 'required'
        ],[
            'title.required'        => '请输入标题！',
            'type.required'         => '请选择类型！',
            'id.required'           => '请选择一个标签进行修改！'
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $data = $request->only(['stores_id', 'title', 'type', 'id']);
        $storeActivityTagsModel = new StoreActivityTags();
        $result = $storeActivityTagsModel->updateStoreActivityTags($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success('修改成功')->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('UPDATE_ERROR')->response());
    }

    /**
     * 商户活动标签删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteStoreActivityTags(Request $request){
        $validator = Validator::make($request->all(), [
            'id'            => 'required'
        ],[
            'id.required'           => '请选择一个标签进行删除！'
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $id = $request->post('id');
        $storeActivityTagsModel = new StoreActivityTags();
        $result = $storeActivityTagsModel->deleteStoreActivityTags($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success('删除成功')->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('DELETE_ERROR')->response());
    }
}
