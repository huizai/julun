<?php
namespace App\Http\Controllers\Web;

use App\Blocks\StatisticsBlock;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrdersRequest;
use App\Models\OrderGoods;
use App\Models\Orders;
use App\Models\Users;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;


class StatisticsController extends Controller{


    public function index(){  //平台
        global $companyId;
        $user = new Users();
        $orders = new Orders();
        $users=$user->usesTotal($companyId);//当天注册人数&总人数

        $orders=$orders->ordersTotal($companyId);//当天订单数&总订单数

        return response()->json(ResponseMessage::getInstance()->success(['users' => $users, 'orders' => $orders])->response());
    }

    public function StatisticsTotal(OrdersRequest $request){ //1/今日 2/昨日 3/最近七日 4/本月

        global $companyId;
        $status=$request->only('status');

        if(!is_numeric($status['status'])){
            return response()->json(ResponseMessage::getInstance()->failed("PARAM_ERROR")->response());
        }

        $orders = new Orders();
        $total  = $orders->ordersTotal($companyId,null,['status']);

        if($total){
            return response()->json(ResponseMessage::getInstance()->success([ 'orders' => $total])->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("PARAM_ERROR")->response());
        }
    }

    /**
     * 获取商品销量排行
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGoodsSalesRanking(Request $request){
        $orderBy    = $request->get('orderBy', 'desc');

        $statisticsBlock        = new StatisticsBlock();
        $data = $statisticsBlock->getGoodsSalesRanking($orderBy);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取用户
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserData(){
        $statisticsBlock        = new StatisticsBlock();
        $data = $statisticsBlock->getUserData();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 用户注册统计
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserRegister(Request $request){
        $time       = $request->get('time', [
            date('Y-m-d', strtotime("-7 day")),
            date('Y-m-d', time())
        ]);

        $statisticsBlock        = new StatisticsBlock();
        $data   = $statisticsBlock->getUserRegister($time);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 优惠券发行统计
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCouponIssue(Request $request){
        $time = $request->get('time', [
            date('Y-m-d', strtotime("-7 day")),
            date('Y-m-d', time())
        ]);

        $statisticsBlock        = new StatisticsBlock();
        $data   = $statisticsBlock->getCouponIssue($time);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 优惠券每天使用量统计
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCouponUse(Request $request){
        $time = $request->get('time', [
            date('Y-m-d', strtotime("-7 day")),
            date('Y-m-d', time())
        ]);

        $statisticsBlock        = new StatisticsBlock();
        $data   = $statisticsBlock->getCouponUse($time);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


    /**停车场统计
     * @return \Illuminate\Http\JsonResponse
     */
    public function getParkingTotal(){
        $statisticsBlock        = new StatisticsBlock();
        $data   = $statisticsBlock->getParkingTotal();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取商户营业额
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getManageTurnover(){

        $statisticsBlock        = new StatisticsBlock();
        $data   = $statisticsBlock->getManageTurnover();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取平台营业额
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function platformTurnover(Request $request){
        $time = $request->get('time');

        if (empty($time)){
            $time =  [
                date('Y-m-d', strtotime("-7 day")),
                date('Y-m-d', time())
            ];
        }

        $statisticsBlock        = new StatisticsBlock();
        $data   = $statisticsBlock->platformTurnover($time);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取商家营业额
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMerchantStatistics(Request $request){
        $storeId            = $request->get('stores_id', 0);
        $time               = $request->get('time');
        if (empty($time)){
            $time           = [
                date('Y-m-d', strtotime("-29 day")),
                date('Y-m-d', time())
            ];
        }

        $type               = $request->get('type');
        $year               = $request->get('year', date('Y', time()));
        $month              = $request->get('month', date('Y-m', time()));
        $day                = $request->get('day', date('Y-m-d', time()));
        $statisticsBlock    = new StatisticsBlock();
        $data = $statisticsBlock->getMerchantStatistics($storeId, $time, $type, $year, $month, $day);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取商家某天已入账订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTimeOrder(Request $request){
        if(!$request->filled(['stores_id', 'date'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $storeId        = $request->get('stores_id');
        $date           = $request->get('date');
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 10);

        $statisticsBlock    = new StatisticsBlock();
        $data = $statisticsBlock->getTimeOrder($storeId, $date, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取订单商品
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderGoods(Request $request){
        if (!$request->filled('orders_id')){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        $orderId            = $request->get('orders_id');
        $orderGoodsModel    = new OrderGoods();
        $data   = $orderGoodsModel->getOrderGoods($orderId);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 商品统计
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGoodsStatistics(Request $request){
        $year       = $request->get('year',  '');
        $month      = $request->get('month', '');
        $day        = $request->get('day',   '');
        $name       = $request->get('name', '');
        $storeId    = $request->get('stores_id', '');

        $statisticsBlock    = new StatisticsBlock();
        $data = $statisticsBlock->getGoodsStatistics($year, $month, $day, $name, $storeId);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取商家排行
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMerchantRating(Request $request){
        $year       = $request->get('year',  '');
        $month      = $request->get('month', '');
        $day        = $request->get('day',   '');
        $name       = $request->get('name', '');

        $statisticsBlock    = new StatisticsBlock();
        $data = $statisticsBlock->getMerchantRating($year, $month, $day, $name);
        foreach ($data as $key => &$datum) {
            $datum->rating = $key + 1;
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取用户消费统计
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserConsumption(Request $request){
        $year       = $request->get('year',  date('Y'));
        $month      = $request->get('month', date('Y-m'));
        $day        = $request->get('day',   date('Y-m-d'));

        if (empty($year)){
            $year = date('Y');
        }
        if (empty($month)){
            $month = date('Y-m');
        }if (empty($day)){
            $day = date('Y-m-d');
        }

        $statisticsBlock    = new StatisticsBlock();
        $data = $statisticsBlock->getUserConsumption($year, $month, $day);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取商家某一时间段每天营业额导出数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreExportTurnover(Request $request){
        $time           = $request->get('time');
        $storeName      = $request->get('store_name', '');

        if (empty($time)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请选择时间')->response());
        }

        $statisticsBlock    = new StatisticsBlock();
        $data = $statisticsBlock->getStoreExportTurnover($time, $storeName);

        if ($data['code'] == 200){
            return response()->json(ResponseMessage::getInstance()->success($data['data'])->response());
        }
        return response()->json(ResponseMessage::getInstance()->failed()
            ->setFailedCode('ERROR', $data['message'])->response());

    }
    /**
     * 获取商家某一时间段每天营业额导出数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreExportTurnoverNew(Request $request){
        $search     = ['name', 'mobile','cid', 'status', 'recommend', 'store_type', 'create_time', 'orderColumn'];//店铺名称，电话号码,店铺分类, 状态
        $searchData = [];
        foreach ($search as $item) {
            if ($request->has($item)) {
                $searchData[$item] = $request->get($item);
            }
        }
        $storeBlock  = new StoreBlock();
        $res=$storeBlock->getStore($searchData);
        if(!empty($res)){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '')->response());

        }
    }

}
