<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blocks\GoodsIntegralBlock;
use App\Libs\ResponseMessage;

class GoodsIntegralController extends Controller
{

    /**
     * 积分商品列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function goodsList(Request $request){
        $page               = $request->get('page', 1);
        $pageSize           = $request->get('pageSize', 1);
        $name               = $request->get('name', '');
        $goodsBlock         = new GoodsIntegralBlock();
        $data               = $goodsBlock->goodsList($page, $pageSize, $name);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 添加修改积分商品
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveGoods(Request $request){
        $isGet      = ['name', 'selling_price', 'image', 'status'];
        if (!$request->filled($isGet)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请填写完整信息')->response());
        }

        $id                 = $request->get('id', '');
        $data               = $request->only($isGet);
        $goodsBlock         = new GoodsIntegralBlock();
        $result             = $goodsBlock->saveGoods($data, $id);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除积分商品
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeGoods(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        $id         = $request->get('id');
        $goodsBlock = new GoodsIntegralBlock();
        $result     = $goodsBlock->removeGoods($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
