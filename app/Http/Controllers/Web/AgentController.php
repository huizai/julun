<?php
/**
 * Created by PhpStorm.
 * User: m1507
 * Date: 2019/4/9
 * Time: 18:02
 */

namespace App\Http\Controllers\Web;

use App\Blocks\AgentBlock;
use App\Http\Controllers\Controller;
use App\Http\Requests\AgentExamRequest;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;

class AgentController extends Controller
{
    /**
     * 代理审核列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        $search     = ['mobile', 'main_type', 'name', 'status'];//电话号码, 经营类型,机构名称, 状态
        $searchData = [];
        foreach ($search as $item) {
            if ($request->has($item)) {
                $searchData[$item] = $request->get($item);
            }
        }

        $agentBlock = new AgentBlock();
        $res = $agentBlock->agentAdminList($searchData,$page,$pageSize);
        $pagination = $agentBlock->agentAdminListPagination($searchData, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $res, 'pagination' => $pagination])->response());

    }
//    代理认证详情
    public function agentInfo(Request $request){
        global $companyId;
        $id = $request->only(['id']);
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $agentBlock = new AgentBlock();
        $res = $agentBlock->agentInfo($id);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }
//    认证审核
    public function examAgent(AgentExamRequest $request){
        global $companyId;
        $data = $request->only(['id', 'status']);
        $data['remake'] = $request->get('remake', "");
        $agentBlock = new AgentBlock();
        $res = $agentBlock->examAgent($data);
        if ($res !== false){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }
//    代理审核-同意  （废弃：合并为一个接口）
    public function consentAgent(Request $request){
        global $companyId;
        $id = $request->only(['id']);
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $agentBlock = new AgentBlock();
        $res = $agentBlock->consentAgent($id);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }
}