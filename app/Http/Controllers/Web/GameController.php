<?php

namespace App\Http\Controllers\web;

use App\Libs\ResponseMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameDraw;
use App\Models\GameDrawSolve;

class GameController extends Controller
{

    /**
     * 获取签列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGameDrawList(){
        $gameDrawModel      = new GameDraw();
        $data = $gameDrawModel->getGameDraw();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 添加或者修改签
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveGameDraw(Request $request){
        if (!$request->filled('content')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '请输入内容')->response());
        }
        $id                 = $request->get('id', '');
        $content            = $request->get('content');
        if (mb_strlen($content) > 4){
            return response()->json(ResponseMessage::getInstance()->failed()
                    ->setFailedCode('ERROR', '不能超过4个字')->response());
        }
        $gameDrawModel      = new GameDraw();
        $result     = $gameDrawModel->saveGameDraw($id, $content);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除签
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeGameDraw(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '请选择签')->response());
        }

        $id                 = $request->get('id');
        $gameDrawModel      = new GameDraw();
        $result     = $gameDrawModel->removeGameDraw($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取解签列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGameDrawSolve(Request $request){
        $id                 = $request->get('id');
        $gameSolveModel     = new GameDrawSolve();
        $data       = $gameSolveModel->getGameDrawSolve($id);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 修改或者添加解签
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveGameDrawSolve(Request $request){
        if (!$request->filled('content')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '请输入内容')->response());
        }

        $id                 = $request->get('id');
        $gameId             = $request->get('game_id');
        $content            = $request->get('content');
        $gameSolveModel     = new GameDrawSolve();
        $result     = $gameSolveModel->saveGameDrawSolve($id, $gameId, $content);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除解签
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeGameDrawSolve(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '请选择签')->response());
        }

        $id                 = $request->get('id');
        $gameSolveModel     = new GameDrawSolve();
        $result     = $gameSolveModel->removeGameDrawSolve($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
