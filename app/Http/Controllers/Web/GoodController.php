<?php

namespace App\Http\Controllers\Web;

use App\Blocks\GoodsBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\CollectMillGoods;
use App\Models\Goods;
use App\Models\GoodsClassifies;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\GoodsRequest;

class GoodController extends Controller{
    /**
     * 获取商品列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 20);
        $shopType                   = $request->get('store_type', '商城');
        $search                     = ['company' => $companyId];
        $searchField                = ['name', 'cid', 'store_id', 'p_id', 'mill_id', 'recovery', 'shop_type', 'status'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        if($shopType == '批发商城'){
            $search['shop_type'] = 1;
        }
        $fieldData = [
            'id' => 'id',
            '销量' => 'sales_volume',
            '价格' => 'selling_price'
        ];
        $search['field']        = $fieldData[$request->get('field', 'id')];
        $search['sequence']     = $request->get('sequence', 'desc');
        if (!isset($search['field'])){
            $search['field'] = 'id';
            $search['sequence'] = 'desc';
        }
        $goodsBlock = new GoodsBlock();
        $goods = $goodsBlock->goodsList($search, $page, $pageSize);
        $pagination = $goodsBlock->goodsListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $goods, 'pagination' => $pagination])->response());
    }
    /**
     * 商品添加
     * @param GoodsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(GoodsRequest $request){
        global $companyId;
        $data = $request->only([
            'name',
            'slogan',
            'thumbnail',
            'selling_price',
            'purchase_price',
            'original_price',
            'stock',
            'detail',
            'sort',
            'status',
            'enable_sku',
            'p_id',
            'stores_id',
            'agent_id',
            'mill_id',
            'store_nav_id',
            'cuisine_name'
        ]);
        $data['classify_id']        = $request->get('classify_id', 0);
        $data['company_id']         = $companyId;
        $data['create_time']        = date('Y-m-d H:i:s', time());
        $data['selling_price']      = $data['selling_price']*100;
        $data['purchase_price']     = $data['purchase_price']*100;
        $data['original_price']     = $data['original_price']*100;
        $data['params']             = json_encode($request->get('params', []));
        $service                    = $request->get('service', '');
        $data['service']            = json_encode(explode(',', str_replace("，", ",", $service)));
        $data['detail']             = htmlspecialchars($data['detail']);
        $sku        = json_decode($request->get('sku', json_encode([])),true);
        $images     = $request->get('images', []);
        $skuData    = json_decode($request->get('sku_data', json_encode([])), true);
        $group      = $request->only(['is_group','total_num', 'total_sum', 'time_limit', 'group_selling_price']);
        $spike      = $request->only(['is_spike','spikeData']);
        $tags       = $request->get('tags', []);
        $bargain    = $request->only(['is_bargain', 'reserve_price', 'time_limit', 'total_num', 'total_sum']);

        $goodsBlock = new GoodsBlock();
        $goodsId    = $goodsBlock->addGoods($data, $sku, $skuData, $images, $group, $spike, $tags, $bargain);
        if($goodsId !== false){
            return response()->json(ResponseMessage::getInstance()->success($goodsId)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }
    /**
     * 商品修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
//    public function update(Request $request){
//        global $companyId;
//        $data = $request->only([
//            'name',
//            'slogan',
//            'thumbnail',
//            'selling_price',
//            'purchase_price',
//            'original_price',
//            'sales_volume',
//            'stock',
//            'detail',
////            'service',
//            'sort',
//            'enable_sku',
//            'classify_id',
//            'id',
//            'status',
////            'params',
//            'store_nav_id',
//            'cuisine_name'
//        ]);
//        $validator = Validator::make($data, [
//            'id'                => 'required',
//            'name'              => 'required',
//            'thumbnail'         => 'required',
//            'selling_price'     => 'required',
//            'enable_sku'        => 'required',
//            'stock'             => 'required',
//            'classify_id'       => 'required',
//        ], [
//            'id.required'               => '商品id不能为空！！',
//            'name.required'             => '商品名不能为空！！',
//            'thumbnail.required'        => '商品图片不能为空！',
//            'selling_price.required'    => '出售价格不能为空！',
//            'enable_sku.required'       => '是否启用规格不能为空！',
//            'stock.required'            => '库存不能为空！',
//            'classify_id'               => '分类不能为空',
//        ]);
//        if ($validator->fails()) {
//            $res = [
//                'type' => $validator->errors()->keys()[0],
//                'message' => $validator->errors()->first()
//            ];
//            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
//        }
//        $data['selling_price']      = $data['selling_price']*100;
//        $data['purchase_price']     = $data['purchase_price']*100;
//        $data['original_price']     = $data['original_price']*100;
////        $data['params']             = json_encode( $request->get('params', []));
////        $data['price_interval']     = $request->get('price_interval', json_encode([]));
//        if (!$request->filled('service')){
//            $data['service'] = null;
//        }else{
//            $data['service'] = json_encode(explode(',', str_replace("，", ",", $request->get('service', ''))));
//        }
//
//        $goodsModel = new Goods();
//        //验存
//        $good = $goodsModel::find($data['id']);
//        if (empty($good['id'])){
//            return response()->json(ResponseMessage::getInstance()->failed('DATA_EXIST')->response());
//        }
//
//        $sku        = json_decode($request->get('sku', json_encode([])),true);
//        $images     = $request->get('images', []);
//        $skuData    = json_decode($request->get('sku_data', json_encode([])), true);
//        $tags       = $request->get('tags', []);
//        $group      = $request->only(['is_group','people', 'group_selling_price']);
//        $spike      = $request->only(['is_spike','spikeData']);
//        $goodsBlock = new GoodsBlock();
//        foreach ($data as &$datum) {
//            if (empty($datum)){
//                $datum = null;
//            }
//        }
//        $goodsId    = $goodsBlock->updateGoods($data, $sku, $skuData, $images, $tags, $spike, $group);
//        if($goodsId !== false){
//            return response()->json(ResponseMessage::getInstance()->success($goodsId)->response());
//        }else{
//            return response()->json(ResponseMessage::getInstance()->failed("UPDATE_ERROR")->response());
//        }
//    }

    /**
     * 修改商品
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function goodsSave(Request $request){
        /**
         * 商品基本信息
         */
        $data = $request->only([
            'name',             //商品名称
            'slogan',           //商品介绍
            'thumbnail',        //缩略图
            'selling_price',    //出售价格
            'purchase_price',   //进价
            'original_price',   //原价
            'stock',            //库存
            'detail',           //详情
            'enable_sku',       //是否启用规格
            'classify_id',      //分类ID
            'id',               //商品ID
            'status',           //商品状态，上下架
            'store_nav_id',     //商品（菜品）导航
            'cuisine_name'      //菜系
        ]);

        $validator = Validator::make($data, [
            'id'                => 'required',
            'name'              => 'required',
            'thumbnail'         => 'required',
            'selling_price'     => 'required',
            'stock'             => 'required',
            'classify_id'       => 'required',
        ], [
            'id.required'               => '商品id不能为空！！',
            'name.required'             => '商品名不能为空！！',
            'thumbnail.required'        => '商品图片不能为空！',
            'selling_price.required'    => '出售价格不能为空！',
            'purchase_price.required'   => '出售价格不能为空！',
            'original_price.required'   => '出售价格不能为空！',
            'stock.required'            => '库存不能为空！',
            'classify_id'               => '分类不能为空',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }

        $data['selling_price']      = $data['selling_price'] * 100;
        $data['purchase_price']     = $data['purchase_price'] * 100;
        $data['original_price']     = $data['original_price'] * 100;
        $data['detail']             = htmlspecialchars($data['detail']);
        $data['params']             = json_encode( $request->get('params', []));
        /**
         * 服务
         */
        if (!$request->filled('service')){
            $data['service'] = null;
        }else{
            $data['service'] = json_encode(explode(',', str_replace("，", ",", $request->get('service', ''))));
        }

        /**
         *  商品图片
         */
        $images     = $request->get('images', []);
        /**
         * 商品规格
         */
        $sku        = json_decode($request->get('sku', json_encode([])),true);
        $skuData    = json_decode($request->get('sku_data', json_encode([])), true);
        if (!isset($data['enable_sku']) || empty($data['enable_sku'])){
            $data['enable_sku'] = 0;
        }
        /**
         * 标签
         */
        $tags       = $request->get('tags', []);
        /**
         * 拼团
         */
        $group      = $request->only(['is_group','total_num', 'total_sum', 'time_limit', 'group_selling_price']);
        if ($group['is_group'] === 'true'){
            $group['is_group'] = true;
        }else if ($group['is_group'] === 'false'){
            $group['is_group'] = false;
        }
        /**
         * 秒杀
         */
        $spike      = $request->only(['is_spike','spikeData']);
        if ($spike['is_spike'] === 'true'){
            $spike['is_spike'] = true;
        }else if ($spike['is_spike'] === 'false'){
            $spike['is_spike'] = false;
        }
        //砍价
        $bargain    = $request->only(['is_bargain', 'reserve_price', 'time_limit', 'total_num', 'total_sum']);

        $goodsBlock = new GoodsBlock();
        $result     = $goodsBlock->goodsSave($data, $sku, $skuData, $images, $tags, $spike, $group, $bargain);

        if($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商品删除（软删除）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $id = $request->get('id');
        $res = Goods::where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time()),
        ]);
        if(!$res){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }
    /**
     * 商品回收
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recovery(Request $request){
        $ids = $request->get('id', []);

        if (!isset($ids)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $goodsModel = new Goods();
        $res = $goodsModel->whereIn('id', $ids)->update([
            'delete_time' => null,
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
    /**
     * 商品删除（真删除）
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        $goodsModel = new Goods();
        //        验存
        $good = $goodsModel::find($id);
        if (empty($good['id'])){
            return response()->json(ResponseMessage::getInstance()->failed('DATA_EXIST')->response());
        }
        $res = $goodsModel->destroy($id);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }
    }


    /**
     * 获取商品分类
     * @return \Illuminate\Http\JsonResponse
     */
    public function classify(){
        global $companyId;

        $classify = GoodsClassifies::where('company_id', $companyId)
            ->whereNull('delete_time')
            ->get();
        $goodsBlock = new GoodsBlock();
        $classifyList = $goodsBlock->classifyList($classify);
        return response()->json(ResponseMessage::getInstance()->success($classifyList)->response());
    }

    /**
     * 商品状态修改（上下架审核操作商品时）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function goodStatus(Request $request){
        $ids        = $request->get('id', []);
        $status     = $request->get('status', 0);
        if (!isset($ids)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $goodsModel = new Goods();
        $res = $goodsModel->whereIn('id', $ids)->update([
            'status' => $status
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商品详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        $id         = $request->get('id');
        $store_id   = $request->get('store_id', null);
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $goodsBlock = new GoodsBlock();
        $goods = $goodsBlock->goodsInfoAdmin($id, $store_id);
        return response()->json(ResponseMessage::getInstance()->success($goods)->response());
    }


    /**
     * 商品详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function readApp(Request $request){
        $id         = $request->get('id');
        $store_id   = $request->get('store_id', null);
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $goodsBlock = new GoodsBlock();
        $goods = $goodsBlock->goodsInfoApp($id, $store_id);
        return response()->json(ResponseMessage::getInstance()->success($goods)->response());
    }
    /**
     * 一键复制
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function copy(Request $request){
        $type       = $request->get('type', 'agent');//商户还是代理
        $id         = $request->get('id');//商户/代理id
        $goodsId    = $request->get('goods_id');//商品id
        if (!isset($ids) && !isset($goodsId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $goodsBlock = new GoodsBlock();
        $res = $goodsBlock->copyGoods($type, $id, $goodsId);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 收藏商品（蓝丝羽app）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectGood(Request $request){
        $storeId = $request->get('id');
        $goodsId  = $request->get('goods_id');
//        dd(123);
        $type    = $request->get('type', 0);//0：取消收藏 1：收藏
        if (!isset($storeId) || !isset($goodsId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $data = [
            'goods_id'   => $goodsId,
            'store_id'  => $storeId
        ];
        $goodsBlock = new GoodsBlock();
        $collect = $goodsBlock->collectGood($data, $type);
        if($collect === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 商品收藏列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectList(Request $request){
        global $companyId;
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $storeId = $request->get('store_id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search     = ['name'];//商品名称
        $searchData = ['agent_id' => $storeId];
        foreach ($search as $item) {
            if ($request->has($item)) {
                $searchData[$item] = $request->get($item);
            }
        }

        $searchData['app_collect'] = 1;
        $goodsBlock = new GoodsBlock();
        $goods = $goodsBlock->goodsList($searchData, $page, $pageSize);
        $pagination = $goodsBlock->goodsListPagination($searchData, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $goods, 'pagination' => $pagination])->response());
    }

    /**
     * 商品批量取消收藏
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectCancel(Request $request){
        global $companyId;
        $storeId = $request->get('store_id');
        $goodsId = $request->get('goods_id', []);
        $collectMillGoodModel = new CollectMillGoods();
        $res = $collectMillGoodModel
            ->whereIn('goods_id', $goodsId)
            ->where('store_id', $storeId)
            ->delete();
        if ($res!==false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

}
