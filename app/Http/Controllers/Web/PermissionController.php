<?php

namespace App\Http\Controllers\Web;

use App\Blocks\PermissionBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\AdminRoles;
use App\Models\AdminUrls;
use Illuminate\Http\Request;
use App\Models\AdminUser;
use App\Models\AdminUserSidebar;
use Illuminate\Session\Store;
use Validator;
use App\Models\AdminSidebar;
use App\Models\Stores;

class PermissionController extends Controller{

    /**
     * 后台-管理员列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminUser(Request $request){
        $page = $request->get('page',1);
        $pageSize = $request->get('pageSize',10);
        $nickname = trim($request->get('nickname'));

        $adminUserModel = new AdminUser();
        $data = $adminUserModel->adminUser($page, $pageSize, $nickname);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());

    }

    /**
     * 后台-管理员详细信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminUserInfo(Request $request){

        $validator = Validator::make($request->all(), [
            'id'    => 'required',
            'type'  => 'required',
        ],[
            'id.required'   => 'id不能为空！',
            'type.required' => '类型不能为空！',
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $id = (int)$request->get('id');
        $type = (int)$request->get('type');
        //账号信息
        $adminUserModel = new AdminUser();
        $adminUserSidebarModel = new AdminUserSidebar();

        //菜单信息
        $sidebar_id = $adminUserSidebarModel->getUserSidebar($id, $type);
        $userInfo = $adminUserModel->adminUserInfo($id);

        $sidebar = [];
        foreach ($sidebar_id as $item) {
            $sidebar[] = $item->sidebar_id;
        }
        $userInfo->sidebar_id = $sidebar;
        $userInfo->roles = ['admin'];
        return response()->json(ResponseMessage::getInstance()->success($userInfo)->response());

    }

    /**
     * 后台-添加管理员账号
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAdminUser(Request $request){

        $validator = Validator::make($request->all(), [
            'username'      => 'required',
            'nickname'      => 'required',
            'password'      => 'required',
            'avatar'        => 'required',
            'status'        => 'required',
            'sidebar_id'    => 'required',
            'type'          => 'required',
        ],[
            'username.required'         => '用户名不能为空！',
            'nickname.required'         => '昵称不能为空！',
            'password.required'         => '密码不能为空！',
            'avatar.required'           => '头像不能为空！',
            'status.required'           => '状态不能为空！',
            'sidebar_id.required'       => '菜单不能为空！',
            'type.required'             => '类型不能为空！',
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $data = $request->only(['username', 'nickname', 'password', 'avatar', 'status']);
        $data['create_time'] = date('Y-m-d H:i:s',time());
        $data['password'] = md5($data['password']);
        $sidebar_id = $request->post('sidebar_id');
        $type = $request->post('type');


        $adminUserModel = new AdminUser();
        $result = $adminUserModel->addAdminUser($data, $sidebar_id, $type);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success(['id'=>$result])->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('ADD_ERROR')->response());
    }

    /**
     * 后台-修改管理员信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAdminUser(Request $request){
        $validator = Validator::make($request->all(), [
            'id'            => 'required',
            'sidebar_id'    => 'array',
            'avatar'        => 'required',
            'status'        => 'required',
            'type'          => 'required',
        ],[
            'id.required'           => 'id不能为空！',
            'sidebar_id.array'      => '菜单格式错误',
            'avatar.required'       => '请上传头像！',
            'status.required'       => '请选择状态！',
            'type.required'         => '请选择类型',
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $id = (int)$request->post('id');
        $data = $request->only(['password','nickname','avatar','status']);
        $sidebar_id = $request->post('sidebar_id');
        $type = $request->post('type');

        if (empty($data)){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


        if (isset($data['password'])){
            $data['password'] = md5($data['password']);
        }
        $data['update_time'] = date('Y-m-d H:i:s',time());

        $adminUserModel = new AdminUser();
        $result = $adminUserModel->updateAdminUser($id, $data, $sidebar_id, $type);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('UPDATE_ERROR')->response());

    }

    /**
     * 后台-删除管理员
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAdminUser(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ],[
            'id.required' => 'id不能为空！',
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $id = (int)$request->post('id');

        $adminUserModel = new AdminUser();
        $result = $adminUserModel->deleteAdminUser($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('DELETE_ERROR')->response());
    }


    /**
     * 后台-节点列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminUrlList(Request $request){
        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 10);
        $search = $request->only(['url', 'method']);

        $urlModel = new AdminUrls();
        $data = $urlModel->adminUrlList($page, $pageSize, $search);
        $pagination = $urlModel->adminUrlPagination($page, $pageSize, $search);

        return response()->json(ResponseMessage::getInstance()->success([
            'list' => $data,
            'pagination' => $pagination
        ])->response());
    }

    /**
     * 后台-节点信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminUrlInfo(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ],[
            'id.required' => 'id不能为空！',
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $id = $request->get('id');

        $urlModel = new AdminUrls();
        $data = $urlModel->adminUrlInfo($id);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 后台-添加节点
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addUrl(Request $request){
        $validator = Validator::make($request->all(), [
            'url' => 'required',
            'method' => 'required'
        ],[
            'url.required' => 'url不能为空！',
            'method.required' => 'method不能为空！'
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $data = $request->only(['url','method']);

        $urlModel = new AdminUrls();
        $result = $urlModel->addUrl($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('ADD_ERROR')->response());
    }

    /**
     * 后台-修改节点
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUrl(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ],[
            'id.required' => 'id不能为空！'
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $id = (int)$request->post('id');
        $data = $request->only(['url','method']);

        if (empty($data)){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        $urlModel = new AdminUrls();
        $data['updated_at'] = date('Y-m-d H:i:s',time());
        $result = $urlModel->updateUrl($id,$data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('UPDATE_ERROR')->response());
    }

    /**
     * 后台-删除节点
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUrl(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ],[
            'id.required' => 'id不能为空！'
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $id = (int)$request->post('id');

        $urlModel = new AdminUrls();
        $result = $urlModel->deleteUrl($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('DELETE_ERROR')->response());
    }





    /**
     * 后台菜单树路由
     * @param $data
     * @param $pid
     * @return array
     */
    public function getSidebarTree($data, $pid){

        $tree = [];

        foreach($data as $key => $value){
            if($value['parent_id'] == $pid){
                $value['children'] = $this->getSidebarTree($data, $value['id']);
                $tree[] = $value;

                unset($data[$key]);
            }
        }
        return $tree;
    }


    /**
     * 后台菜单树路由
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSidebar(Request $request){
        if (!$request->has('type')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->post('id');
        $type = $request->post('type');

        $adminSidebarModel = new AdminSidebar();
        $adminUserSidebarModel = new AdminUserSidebar();
        $sidebarData = $adminUserSidebarModel->getUserSidebar($id, $type);

        $sidebarId = [];
        foreach ($sidebarData as $value) {
            $sidebarId[] = $value->sidebar_id;
        }





        $sidebarList = $adminSidebarModel->getSidebarList(1, 999, ['sidebar_id' => $sidebarId]);
        $sidebarList = json_decode(json_encode($sidebarList),true);
        foreach ($sidebarList as &$value) {
            if ($value['hidden'] == 1){
                $value['hidden'] = true;
            }else{
                $value['hidden'] = false;
            }

            if (isset($value['title'])){
                $value['meta']['title'] = $value['title'];
            }

            if (isset($value['icon'])){
                $value['meta']['icon'] = $value['icon'];
            }

            foreach ($value as $key => $item) {

                if (!isset($item)){
                    unset($value[$key]);
                }
            }

        }

        $data = $this->getSidebarTree($sidebarList,0);
        return response()->json(ResponseMessage::getInstance()->success($data)->response());

    }

    /**
     * 后台页面路由列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function sidebarList(Request $request){
        $page = $request->get('page',1);
        $pageSize = $request->get('pageSize', 10);
        $search = $request->only(['title','tree']);
        $adminSidebarModel = new AdminSidebar();

        $sidebarList = $adminSidebarModel->getSidebarList($page, $pageSize, $search);
        $pagination = $adminSidebarModel->getSidebarPagination($page, $pageSize, $search);

        foreach ($sidebarList as &$item) {
            if ($item->hidden == 1){
                $item->hidden = true;
            }else{
                $item->hidden = false;
            }
        }

        if (isset($search['tree'])){
            $sidebarList = json_decode(json_encode($sidebarList),true);
            $sidebarList = $this->getSidebarTree($sidebarList,0);
        }

        return response()->json(ResponseMessage::getInstance()->success([
            'list' => $sidebarList,
            'pagination' => $pagination
        ])->response());
    }

    /**
     * 获取用户菜单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserSidebar(Request $request){
        if (!$request->has('type')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->get('id');
        $type = $request->get('type');

        $adminUserSidebarModel = new AdminUserSidebar();
        $data = $adminUserSidebarModel->getUserSidebar($id, $type);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 修改用户菜单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserSidebar(Request $request){
        if (!$request->has(['type', 'sidebar_id'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $sidebar_id = $request->get('sidebar_id');
        $type = $request->get('type');

        $sidebarData = [];
        foreach ($sidebar_id as $key => $item) {
            $sidebarData[$key]['sidebar_id'] = $item;
            $sidebarData[$key]['type'] = $type;
        }

        $adminUserSidebarModel = new AdminUserSidebar();
        $result = $adminUserSidebarModel->updateUserSidebar($sidebarData, $type);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());
    }


    /**
     * 修改菜单基本信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sidebarUpdate(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $postData = ['id', 'parent_id', 'redirect', 'title', 'path', 'name', 'component', 'hidden', 'icon'];
        $data = [];
        foreach ($postData as $value) {
            $data[$value] = $request->post($value);
        }

        $adminSidebarModel = new AdminSidebar();
        $result = $adminSidebarModel->sidebarUpdate($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());

    }

    /**
     * 删除菜单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sidebarDelete(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->post('id');

        $adminSidebarModel = new AdminSidebar();
        $result = $adminSidebarModel->sidebarDelete($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());
    }


    /**
     * 添加菜单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sidebarAdd(Request $request){

        $data = $request->only([
            'parent_id',
            'path',
            'redirect',
            'name',
            'component',
            'hidden',
            'icon',
            'title',
            'breadcrumb',
            'url'
        ]);
        $data['create_time'] = date('Y-m-d H:i:s',time());
        $adminSidebarModel = new AdminSidebar();
        $result = $adminSidebarModel->sidebarAdd($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());
    }

    /**
     * 平台添加商户账号
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveStoreUser(Request $request){
        if (!$request->has(['mobile','name','store_type_id','classifity_id'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $companyId;
        $data = $request->only(['mobile','name','store_type_id','classifity_id']);
        $data['company_id'] = $companyId;
        $data['password'] = md5(123456);
        $data['account'] = date('Ymd').mt_rand(999, 9999);

        $storeModel = new Stores();
        $result = $storeModel->saveStoreUser($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());

    }

    /**
     * 管理员信息
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdminUserInfo(){
        global $adminId;
        $adminUserModel     = new AdminUser();
        $info               = $adminUserModel->getAdminUserInfo($adminId);
        $info->roles        = ['admin'];
        return response()->json(ResponseMessage::getInstance()->success($info)->response());
    }


    /**
     * 后台-获得管理员信息与菜单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRole(Request $request){

        $validator = Validator::make($request->all(), [
            'id'    => 'required',
        ],[
            'id.required'   => 'id不能为空！',
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $id = (int)$request->get('id');
        $type = $request->get('type');
        $adminUserSidebarModel = new AdminUserSidebar();
        if ($type == ''){
            //账号信息
            $type = 1;
            $adminUserModel = new AdminUser();
            $userInfo = $adminUserModel->adminUserInfo($id);

        }else{
            if ($type == "store"){
                $type = 2;
            }elseif ($type == "mill"){
                $type = 3;
            }

            $storeModel = new Stores();
            $userInfo = $storeModel->adminUserInfo($id);
        }

        //获取用户组
        $sidebarId = $adminUserSidebarModel->getUserSidebar($id, $type);
        $sidebar = [];
        foreach ($sidebarId as $item) {
            $sidebar[] = $item->sidebar_id;
        }
        $userInfo->sidebar_id = $sidebar;
        //获取菜单
        $permissionBlock = new PermissionBlock();
        $role = $permissionBlock->getRole($sidebar);

        $userInfo->role = $role;
        return response()->json(ResponseMessage::getInstance()->success($userInfo)->response());

    }

}
