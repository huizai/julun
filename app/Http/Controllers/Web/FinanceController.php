<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-28
 * Time: 下午3:46
 */
namespace App\Http\Controllers\Web;
use App\Blocks\UserBlock;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
class FinanceController extends Controller{


    public function index(Request $request){ //充值积分金额列表
        global $companyId;

        $page = $request->get('currentPage', 1);
        $pageSize = $request->get('pageSize', 20);
        $search = ['name', 'username'];//名字，电话号码，积分，余额
        $searchData = [];
        foreach ($search as $item) {
            if ($request->has($item)) {
                $searchData[$item] = $request->get($item);
            }
        }
        $searchData['company_id']=$companyId;

        $users = new UserBlock();
        $res = $users->userBalance_log($searchData,$page,$pageSize);

        $pagination = $users->userBalance_logPagination($searchData, $page, $pageSize);
//                print_r($res);die;
        return response()->json(ResponseMessage::getInstance()->success(['list' => $res, 'pagination' => $pagination])->response());
    }


}