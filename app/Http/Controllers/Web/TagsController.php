<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Web;
use App\Blocks\StoreTagsBlock;
use App\Blocks\TagsBlock;
use App\Models\Tags;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;

class TagsController extends Controller{
    /**
     * 商户标签列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['name'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $tagsBlock  = new TagsBlock();
        $tags       = $tagsBlock->tagsList($search, $page, $pageSize);
        $pagination = $tagsBlock->tagsListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $tags, 'pagination' => $pagination])->response());
    }

    /**
     * 商户标签详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search     = ['id' => $id];
        $tagsBlock  = new TagsBlock();
        $tags  = $tagsBlock->tagsInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($tags)->response());
    }


    /**
     * 商户标签修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'tag_name',
            'id',
        ]);
        $validator = Validator::make($data, [
            'id'                => 'required',
            'tag_name'          => 'required'
        ], [
            'id.required'               => '标签id不能为空！！',
            'tag_name.required'         => '标签名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        //验存  防止标签名重复
        $tagsModel  = new Tags();
        $tags       = $tagsModel->where('tag_name', $data['tag_name'])->first();
        if ($tags && $tags->id!=$data['id']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "标签名称不能重复")->failed()->response());
        }

        $res = $tagsModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商户标签添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'tag_name'
        ]);
        $validator = Validator::make($data, [
            'tag_name'         => 'required'
        ], [
            'tag_name.required'        => '标签名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        //验存  防止标签名重复
        $tagsModel  = new Tags();
        $tags       = $tagsModel->where('tag_name', $data['tag_name'])->first();
        if ($tags){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "标签名称不能重复")->failed()->response());
        }

        $res = $tagsModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商户标签删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $tagsModel  = new Tags();
        $res = $tagsModel->destroy($id);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 修改商品图片为https
     * @return int
     */
    public function updateImage(){
        set_time_limit (0);
        $table = 'goods_images';
        $filed = 'image';
        $data = DB::table($table)->select('id',$filed)->get();

        $updateData = [];
        foreach ($data as $key => $datum) {
            if (!strstr($datum->$filed, 'https')){
                $updateData[$key]['id'] = $datum->id;
                $updateData[$key][$filed] = str_replace("http","https",$datum->$filed);
            }

        }

//        return $updateData;
        try{
            DB::beginTransaction();
            foreach ($updateData as $value) {
                DB::table($table)->where('id', $value['id'])->update([
                    $filed => $value[$filed]
                ]);
            }

            DB::commit();
            return 1;
        }catch (\Exception $exception){
            \Log::error($exception);
            return 2;
        }


    }






}
