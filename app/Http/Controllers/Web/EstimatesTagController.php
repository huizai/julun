<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Web;
use App\Blocks\EstimatesTagBlock;
use App\Blocks\GoodsEstimatesBlock;
use App\Blocks\StoreTagsBlock;
use App\Blocks\TagsBlock;
use App\Models\GoodsEstimatesTag;
use App\Models\Tags;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EstimatesTagController extends Controller{
    /**
     * 标签关键词列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['title'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $goodsEstimatesBlock    = new EstimatesTagBlock();
        $goodsEstimates         = $goodsEstimatesBlock->estimatesList($search, $page, $pageSize);
        $pagination             = $goodsEstimatesBlock->estimatesListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $goodsEstimates, 'pagination' => $pagination])->response());
    }



    /**
     * 关键词修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'title',
            'store_type_id',
            'id',
        ]);
        $validator = Validator::make($data, [
            'id'                => 'required',
            'store_type_id'     => 'required',
            'title'             => 'required'
        ], [
            'id.required'               => '关键词id不能为空！！',
            'store_type_id.required'    => '标签类型不能为空！！',
            'tag_name.required'         => '关键词不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        //验存  防止关键词重复

        $goodsEstimatesTagModel     = new GoodsEstimatesTag();
        $goodsEstimatesTags         = $goodsEstimatesTagModel->where('title', $data['title'])
            ->where('store_type_id', $data['store_type_id'])
            ->first();
        if ($goodsEstimatesTags && $goodsEstimatesTags->id!=$data['id']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "关键词不能重复")->failed()->response());
        }

        $res = $goodsEstimatesTagModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 关键词添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'title',
            'store_type_id',
        ]);
        $validator = Validator::make($data, [
            'store_type_id'     => 'required',
            'title'             => 'required'
        ], [
            'store_type_id.required'    => '标签类型不能为空！！',
            'tag_name.required'         => '关键词不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        //验存  防止标签名重复
        $goodsEstimatesTagModel     = new GoodsEstimatesTag();
        $goodsEstimatesTags         = $goodsEstimatesTagModel->where('title', $data['title'])
            ->where('store_type_id', $data['store_type_id'])
            ->first();
        if ($goodsEstimatesTags){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "关键词不能重复")->failed()->response());
        }

        $res = $goodsEstimatesTagModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 关键词删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $goodsEstimatesTagModel = new GoodsEstimatesTag();
        $res = $goodsEstimatesTagModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }






}
