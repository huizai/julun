<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\Banners;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Blocks\BannerBlock;
use App\Http\Requests\BannerRequest;
use Illuminate\Support\Facades\Cache;

class BannerController extends Controller{
    /**
     * 获取轮播图列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;

        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search         = ['company' => $companyId];
        $searchField    = ['position'];

        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $goodsBlock = new BannerBlock();

        $goods = $goodsBlock->bannersList($search, $page, $pageSize);
        $pagination = $goodsBlock->bannersListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $goods, 'pagination' => $pagination])->response());

    }
    /**
     * 轮播图添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(BannerRequest $request){
        global $companyId;
        $data = $request->only(['images','url','sort','position','uptime','downtime']);
        $data['company_id']     = $companyId;
        if (empty($data['sort'])){
            $data['sort']=0;
        }
        $Banners = new Banners();
        $res = $Banners::create($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("CREATE_EXIST")->response());
        }

    }
    /**
     * 轮播图修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BannerRequest $request){
        global $companyId;
        $data = $request->only(['images','url','sort','position','uptime','downtime']);
        $id = $request->get('id');
        if (empty($data['sort'])){
            $data['sort']=0;
        }
        if (empty($id)){
            return response()->json(ResponseMessage::getInstance()->failed("PARAM_ERROR")->response());
        }
        $data['company_id']     = $companyId;
        $Banners = new Banners();
        $res = $Banners::where('id', $id)->update($data);
        if ($res !== false){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("UPDATE_EXIST")->response());
        }

    }
    /**
     * 轮播图删除
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (empty($id)){
            return response()->json(ResponseMessage::getInstance()->failed("PARAM_ERROR")->response());
        }
        $Banners = new Banners();
//        验存
        $banner = $Banners::find($id);
        if (empty($banner['id'])){
            return response()->json(ResponseMessage::getInstance()->failed('DATA_EXIST')->response());
        }
        $res = $Banners->destroy($id);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }
    }
}