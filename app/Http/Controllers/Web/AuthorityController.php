<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\Admin;
use App\Models\AdminUser;
use App\Models\Stores;
use Illuminate\Http\Request;
use App\Blocks\AdminAuthorityBlock;

class AuthorityController extends Controller{

    /**
     * 管理员列表
     * @param Request $request
     * @return mixed
     */
    public function adminUserList(Request $request){
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $username       = $request->get('username', '');
        $authorityBlock = new AdminAuthorityBlock();
        $data           = $authorityBlock->adminUserList($page, $pageSize, $username);

        foreach ($data['list'] as $datum) {
            $userMenu = $this->__getAdminUserRoute($datum->id, 'company');

            foreach ($userMenu as $value) {
                $datum->user_menu[] = $value['id'];
            }
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 添加管理员账号
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAdminUser(Request $request){
        $userInfo = ['username', 'nickname', 'password', 'roles_id'];
        if (!$request->filled($userInfo)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请填写完整信息')
                ->response());
        }
        $userInfoData               = $request->only($userInfo);
        $userInfoData['roles_id']   = implode(',', $userInfoData['roles_id']);
        $userInfoData['avatar']     = $request->get('avatar', env('APP_URL').'/images/default-user-logo/header.png');
        $userInfoData['password']   = md5($userInfoData['password']);
        $adminModel                 = new AdminUser();
        $admin                      = $adminModel->getUserInfo($userInfoData['username']);
        if ($admin){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('ACCOUNT_ERROR','该账号已被注册')
                ->failed()->response());
        }
        $result                     = $adminModel->registerAdminUser($userInfoData);
        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
        return response()->json(ResponseMessage::getInstance()->failed()->response());

    }

    /**
     * 删除管理员
     * @param Request $request
     * @return mixed
     */
    public function deleteAdminUser(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id             = $request->get('id');
        $authorityBlock = new AdminAuthorityBlock();
        $result         = $authorityBlock->deleteAdminUser($id);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 修改管理员信息
     * @param Request $request
     * @return mixed
     */
    public function updateAdminUser(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id         = $request->get('id');
        $data       = ['username', 'password', 'avatar', 'nickname', 'status', 'roles_id'];
        $updateData = [];
        foreach ($data as $datum) {
            if ($request->filled($datum)){
                $updateData[$datum] = $request->get($datum);
            }
        }

        if (isset($updateData['roles_id'])){
            $updateData['roles_id'] = implode(',', $updateData['roles_id']);
        }

        if (count($updateData) > 0){
            if (isset($updateData['password']) && !empty($updateData['password'])){
                $updateData['password'] = md5($updateData['password']);
            }

            $authorityBlock = new AdminAuthorityBlock();
            $result = $authorityBlock->updateAdminUser($id, $updateData);

            if ($result !== false){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }

    }

    public function __getAdminUserRoute($admin_id, $type){
        $authorityModel     = new AdminAuthorityBlock();
        $data               = $authorityModel->getUserRoute($admin_id, $type);
        $data               = json_decode(json_encode($data), true);
        $roteId             = [];

        foreach ($data as $key => &$value) {
            if ($value['pid'] == 0){
                $roteId[] = $value['id'];
            }

            $data[$key] = array_filter($value, function ($item){
                if($item === '' || $item === null){
                    return false;
                }
                return true;
            });

            if (isset($value['meta'])){
                $value['meta'] = json_decode($value['meta'], true);
            }


            if (isset($value['alwaysShow']) && $value['alwaysShow'] == 1){
                $value['alwaysShow'] = true;
            }else if (isset($value['alwaysShow']) && $value['alwaysShow'] == 0){
                $value['alwaysShow'] = false;
            }

            if (isset($value['hidden']) && $value['hidden'] == 1){
                $value['hidden'] = true;
            }else if (isset($value['hidden']) && $value['hidden'] == 0){
                $value['hidden'] = false;
            }
        }
        return $data;
    }

    /**
     * 获取管理员路由菜单
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdminUserRoute(Request $request){
        if (!$request->filled(['id', 'type'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $adminId    = $request->get('id');
        $type       = $request->get('type');
        $data = $this->__getAdminUserRoute($adminId, $type);

        $data = $this->getSidebarTree($data, 0);

//        $data = [
//            [
//                'alwaysShow'        => true,
//                'path'              => "/authority",
//                'meta'              => [
//                    'icon'          => "authority",
//                    'title'         => "权限管理"
//                ],
//                'children'          => [
//                    [
//                        'name'          => "Authority-User-Manage",
//                        'path'          => "authority-user-manage",
//                        'meta'          => ['title' => "管理员管理"]
//                    ],
//                ]
//            ],
//            [
//                'hidden'            => true,
//                'path'              => "*",
//                'redirect'          => "/404"
//            ]
//        ];


        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    public function getSidebarTree($data, $pid){

        $tree = [];

        foreach($data as $key => $value){
            if($value['pid'] == $pid){
                $value['children'] = $this->getSidebarTree($data, $value['id']);
                $tree[] = $value;

                unset($data[$key]);
            }
        }
        return $tree;
    }


    /**
     * 菜单列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function menuList(){

        $authorityModel     = new AdminAuthorityBlock();
        $data               = $authorityModel->menuList();
        foreach ($data as $value) {
            $value->meta = json_decode($value->meta, true);
            $value->title = $value->meta['title'];
            $value->icon = isset($value->meta['icon']) ? $value->meta['icon'] : '';
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 添加菜单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function menuAdd(Request $request){
        $data = $request->only(['pid', 'alwaysShow', 'path', 'name', 'hidden', 'redirect', 'icon', 'title']);

        if (isset($data['title'])){
            $data['meta'] = ['title' => $data['title']];
            unset($data['title']);
        }

        if(isset($data['icon'])){
            $data['meta']['icon'] = $data['icon'];
        }
        unset($data['icon']);
        if (isset($data['meta'])){
            $data['meta'] = json_encode($data['meta']);
        }


        if (!isset($data['pid'])){
            $data['pid'] = 0;
        }

        $authorityModel     = new AdminAuthorityBlock();
        $result             = $authorityModel->menuAdd($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**修改菜单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function menuUpdate(Request $request){
        $id   = $request->get('id');
        $data = $request->only(['pid', 'alwaysShow', 'path', 'name', 'hidden', 'redirect', 'icon', 'title']);

        if (isset($data['title'])){
            $data['meta'] = ['title' => $data['title']];
            unset($data['title']);
        }

        if(isset($data['icon'])){
            $data['meta']['icon'] = $data['icon'];
        }
        unset($data['icon']);
        $data['meta'] = json_encode($data['meta']);

        $authorityModel     = new AdminAuthorityBlock();
        $result             = $authorityModel->menuUpdate($id, $data);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除菜单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function menuDelete(Request $request){
        $id = $request->get('id');
        $authorityModel     = new AdminAuthorityBlock();
        $result             = $authorityModel->menuDelete($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 角色列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function rolesList(){
        $authorityModel     = new AdminAuthorityBlock();
        $data               = $authorityModel->rolesList();

        foreach ($data as $datum) {
            $datum->menu_id = explode(',', $datum->menu_id);

            foreach ($datum->menu_id as &$item) {
                $item = (int)$item;
            }
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 添加角色
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rolesAdd(Request $request){
        $name       = $request->get('name');
        $menuId     = $request->get('menu_id');
        if (count($menuId) <= 0 ){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('MENU_ERROR', '请至少选择一个菜单')
                ->response());
        }

        $menuId     = implode(',', $menuId);
        $authorityModel     = new AdminAuthorityBlock();
        $result             = $authorityModel->rolesAdd($name, $menuId);
        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 修改角色
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rolesUpdate(Request $request){
        $id         = $request->get('id');
        $name       = $request->get('name');
        $menuId     = $request->get('menu_id');

        if (count($menuId) <= 0 ){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('MENU_ERROR', '请至少选择一个菜单')
                ->response());
        }

        $menuId     = implode(',', $menuId);
        $authorityModel     = new AdminAuthorityBlock();
        $result             = $authorityModel->rolesUpdate($id, $name, $menuId);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除角色
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rolesDelete(Request $request){
        $id                 = $request->get('id');
        $authorityModel     = new AdminAuthorityBlock();
        $result             = $authorityModel->rolesDelete($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 树状路由
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTreeRoute(){
        $authorityModel     = new AdminAuthorityBlock();
        $data               = $authorityModel->menuList();
        $data               = json_decode(json_encode($data), true);

        foreach ($data as &$datum) {
            $datum['meta'] = json_decode($datum['meta'], true);
            $datum['title'] = $datum['meta']['title'];
        }
        $data               = $this->getSidebarTree($data, 0);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 修改或添加管理员所在角色之外的菜单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserRoute(Request $request){

        $id     = $request->get('id');
        $menuId = $request->get('menu_id');
        if (count($menuId) <= 0){
            $menuId = null;
        }else{
            $menuId = implode(',', $menuId);
        }

        $authorityModel     = new AdminAuthorityBlock();
        $result             = $authorityModel->updateUserRoute($id, $menuId);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取管理员信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserInfo(Request $request){
        $id     = $request->get('id');
        $type   = $request->get('type', 'company');
        $data = [];
        if ($type == 'company'){
            $admin      = AdminUser::where('id', $id)->first();
            $data['nickname']   = $admin->nickname;
            $data['username']   = $admin->username;
            $data['avatar']     = $admin->avatar;
        }else{
            $admin      = Stores::where('id', $id)->first();
            $data['nickname']   = $admin->name;
            $data['username']   = $admin->mobile;
            $data['avatar']     = $admin->logo;
        }
        $data['roles']          = ['admin'];
        $data['status']         = $admin->status;
        $data['delete_time']    = $admin->delete_time;
//        $data       = [
//            'avatar'        => $admin->avatar,
//            'roles'         => ['admin'],
//            'username'      => $admin->username,
//            'nickname'      => $admin->nickname,
//            'status'        => $admin->status,
//            'delete_time'   => $admin->delete_time
//        ];

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 退出登录
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request){
        $token = $request->get('token');

        $result = \Cache::forget($token);
        if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

}
