<?php

namespace App\Http\Controllers\Web;

use App\Blocks\StoreBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\StoreLease;
use Illuminate\Http\Request;
use App\Http\Requests\StoreLessRequest;
use PhpOffice\PhpSpreadsheet\IOFactory;

class StoreLeaseController extends Controller{

    /**
     * 获取店铺租赁金列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreLeaseList(Request $request){
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 10);
        $searchArray    = ['status', 'name'];
        $search         = [];
        foreach ($searchArray as $item) {
            if ($request->filled($item)){
                $search[$item]  = $request->get($item);
            }
        }

        $storeBlock     = new StoreBlock();
        $data           = $storeBlock->getStoreLeaseList($page, $pageSize, $search);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());

    }

    /**
     * 添加或者修改店铺租赁信息
     * @param StoreLessRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveStoreLease(StoreLessRequest $request){
        $data           = $request->validated();
        $id             = $request->get('id', '');
        $data['remark'] = $request->get('remark', '');

        $storeBlock     = new StoreBlock();
        $result         = $storeBlock->saveStoreLease($id, $data);
        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该商家已添加')->response());
        }elseif ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商家缴费
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storePayment(Request $request){
        $requestData = ['store_name', 'type', 'pay_amount', 'payment_time', 'pay_type'];
        if (!$request->filled($requestData)){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        $data                   = $request->only($requestData);
        $data['pay_amount']     = $data['pay_amount'] * 100;
        $data['remark']         = $request->get('remark', '');
        $id                     = $request->get('id');
        $storeBlock     = new StoreBlock();
        $result         = $storeBlock->storePayment($data, $id);
        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 导入商家租赁信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function saveStoreLeaseBysExcel(Request $request){
        $file           = $request->file('file');
        if ($file->getClientOriginalExtension() != 'xlsx'){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('FILE_ERROR', '请上传xlsx格式的Excel表格文件')->response());
        }

        $inputFileType  = IOFactory::identify($file);
        $objReader      = IOFactory::createReader($inputFileType);
        $objPHPExcel    = $objReader->load($file);
        $data = $objPHPExcel->getSheet(0)->toArray();
        unset($data[0]);


        $saveData = [];
        foreach ($data as $keys => $value) {
            $saveData[$keys] = [];
            foreach ($value as $key => $item) {
                switch ($key){
                    case 0:
                        $storeId = StoreLease::where('store_name', $item)
                            ->where('store_name', $item)
                            ->first();

                        if($storeId){
                            return response()->json(ResponseMessage::getInstance()->failed()
                                ->setFailedCode('ERROR', "商家：{$item}已添加")->response());
                        }

                        $saveData[$keys]['store_name'] = $item;
                        break;
                    case 1:
                        $saveData[$keys]['principal'] = $item;
                        break;
                    case 2:
                        $saveData[$keys]['mobile'] = $item;
                        break;
                    case 3:
                        $saveData[$keys]['position'] = $item;
                        break;
                    case 4:
                        $saveData[$keys]['area'] = $item;
                        break;
                    case 5:
                        $saveData[$keys]['rent'] = $item;
                        break;
                    case 6:
                        $saveData[$keys]['payment_time'] = date('Y-m-d', strtotime($item));
                        break;
                    case 7:
                        $saveData[$keys]['lease_start_time'] = date('Y-m-d', strtotime($item));
                        break;
                    case 8:
                        $saveData[$keys]['lease_end_time'] = date('Y-m-d', strtotime($item));
                        break;
                    case 9:
                        $saveData[$keys]['type'] = $item == '面积合租' ? 1 : $item == '联营扣点' ? 2 : 3;
                        break;
                    case 10:
                        $saveData[$keys]['remark'] = $item;
                        break;
                    case 11:
                        $saveData[$keys]['status'] = $item == '正常' ? 1 : 2;
                        break;
                }
            }
        }
        $storeLeaseModel        = new StoreLease();
        try{
            $result = $storeLeaseModel->saveStoreLease($saveData);
            if ($result){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }catch (\Exception $exception){
            \Log::error($exception);
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }

    /**
     * 获取商家租赁缴费记录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paymentRecording(Request $request){
        $name = $request->get('store_name');
        $page = $request->get('page',1);
        $pageSize = $request->get('pageSize', 10);

        $storeBlock = new StoreBlock();
        $data = $storeBlock->paymentRecording($page, $pageSize, $name);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

}