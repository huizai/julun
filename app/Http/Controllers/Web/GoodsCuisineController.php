<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;
use App\Models\GoodsCuisine;

class GoodsCuisineController extends Controller{

    /**
     * 菜单列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request){
        $page               = $request->get('page', 1);
        $pageSize           = $request->get('pageSize', 10);
        $search             = [];
        $search['name']     = $request->get('name');

        $cuisineModel       = new GoodsCuisine();
        $data = $cuisineModel->list($page, $pageSize, $search);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());

    }

    /**
     * 修改商品菜系
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        if (!$request->filled(['id', 'name'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $id                 = $request->get('id');
        $name               = $request->get('name');

        $cuisineModel       = new GoodsCuisine();
        $result = $cuisineModel->cuisineUpdate($id, $name);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除商品菜系
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id                 = $request->get('id');
        $cuisineModel       = new GoodsCuisine();
        $result = $cuisineModel->cuisineDelete($id);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    public function add(Request $request){
        if (!$request->filled('name')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $name               = $request->get('name');
        $cuisineModel       = new GoodsCuisine();
        $result             = $cuisineModel->cuisineAdd($name);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
