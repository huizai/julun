<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Web;
use App\Blocks\UserBlock;
use App\Http\Controllers\Controller;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller{
    /**
     * 用户列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){  //列表
        global $companyId;
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        $search     = ['name', 'mobile'];//名字，电话号码
        $searchData = [];
        foreach ($search as $item) {
            if ($request->has($item)) {
                $searchData[$item] = $request->get($item);
            }
        }
        $searchData['company_id']=$companyId;
        $usersBlock     = new UserBlock();
        $users          = $usersBlock->userList($searchData,$page,$pageSize);
        $pagination     = $usersBlock->userListPagination($searchData, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $users, 'pagination' => $pagination])->response());
    }


    /**
     * 用户详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search     = ['id' => $id];
        $usersBlock     = new UserBlock();
        $users          = $usersBlock->userInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($users)->response());
    }

    /**
     * 用户删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $userModel = new Users();
        $res = $userModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }

    /**
     * 后台修改用户余额积分
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recharge(Request $request){
        global $companyId;
        $data= $request->only(
               'user_id',
                    'number',
                    'type',
                    'money_type',
                    'operator',
                    'remake'
        );

        $validator = Validator::make($data, [
            'user_id'               => 'required',
            'number'                => 'required',
            'type'                  => 'required',
            'money_type'            => 'required',
            'operator'              => 'required',
            'remake'                => 'required'
        ], [
            'user_id.required'       => '用户id不能为空！！',
            'number.required'        => '金额不能为空！！',
            'type.required'          => '充值方式不能为空！！',
            'money_type.required'    => '金钱类型不能为空！！',
            'operator.required'      => '管理员id不能为空！！',
            'remake.required'        => '备注不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        $users = new UserBlock();
        $data['company_id']=$companyId;
        $data['create_time']=date('Y-m-d H:i:s', time());
        $data['credit_sn']="RC".date("YmdHis").mt_rand(1000,9999);
//        $data['operator']=$adminId;
        $res=$users->updateBalanceIntegral($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("ERROR")->response());
        }
    }

    /**
     * 设置用户为VIP
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function settingUserVip(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PRAM_ERROR')->response());
        }

        $userId     = $request->get('id');
        $vip        = $request->get('vip');
        $userModel  = new Users();
        $result     = $userModel->settingUserVip($userId, $vip);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取余额10元以上的用户数量
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserBalanceIsTen(){
        $userBlock      = new UserBlock();
        $count          = $userBlock->getUserBalanceIsTen();

        return response()->json(ResponseMessage::getInstance()->success($count)->response());
    }
}