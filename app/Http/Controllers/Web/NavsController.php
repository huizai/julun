<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Web;
use App\Blocks\NavsBlock;
use App\Models\PageNav;
use App\Models\StoreNavs;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class NavsController extends Controller{
    /**
     * 页面配置信息列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['name'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $navBlock       = new NavsBlock();
        $navs           = $navBlock->NavsList($search, $page, $pageSize);
        $pagination     = $navBlock->NavsListPagination($search, $page, $pageSize);
        $navs = json_decode(json_encode($navs), true);


        foreach ($navs as $k=>&$v){
            if ($v['url']){
                $url = json_decode($v['url'], true);
                $v['url'] = $url['url'];
                if (isset($url['params'])){
                    $v['cid'] = $url['params']['cid'];
                    $v['title'] = $url['params']['title'];
                }
            }else{
                $v['url'] = '';
                $v['cid'] = '';
                $v['title'] = '';
            }

        }
        return response()->json(ResponseMessage::getInstance()->success(['list' => $navs, 'pagination' => $pagination])->response());
    }


    /**
     * 页面配置信息修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'id',
            'cid',
            'name',
            'url',
            'icon',
            'position',
            'title'
        ]);
        $validator = Validator::make($data, [
            'id'           => 'required',
            'name'         => 'required',
            'url'          => 'required',
            'icon'         => 'required',
            'position'     => 'required'
        ], [
            'id.required'          => '导航id不能为空！！',
            'name.required'        => '导航名称不能为空！！',
            'url.required'         => '导航地址不能为空！！',
            'icon.required'        => '导航图标不能为空！！',
            'position.required'    => '导航位置不能为空！！',

        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }


        $url                        = [];
        $url['url']                 = $data['url'];
        if (isset($data['cid'])){
            $url['params']['cid']     = $data['cid'];
            unset($data['cid']);
        }elseif (empty($data['cid'])){
            unset($data['cid']);
        }

        if (isset($data['title'])){
            $url['params']['title']       = $data['title'];
            unset($data['title']);
        }elseif (empty($data['title'])){
            unset($data['title']);
        }

        $data['url']                = json_encode($url);

        $NavsModel = new PageNav();
        
        $res = $NavsModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 页面配置信息添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'cid',
            'name',
            'url',
            'icon',
            'position',
            'title'
        ]);
        $validator = Validator::make($data, [
            'name'         => 'required',
            'url'          => 'required',
            'icon'         => 'required',
            'position'     => 'required'
        ], [
            'name.required'        => '导航名称不能为空！！',
            'url.required'         => '导航地址不能为空！！',
            'icon.required'        => '导航图标不能为空！！',
            'position.required'    => '导航位置不能为空！！',

        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        $url                        = [];
        $url['url']                 = $data['url'];
        if (isset($data['cid'])){
            $url['params']['cid']     = $data['cid'];
            unset($data['cid']);
        }elseif (empty($data['cid'])){
            unset($data['cid']);
        }

        if (isset($data['title'])){
            $url['params']['title']       = $data['title'];
            unset($data['title']);
        }elseif (empty($data['title'])){
            unset($data['title']);
        }

        $data['url']                = json_encode($url);

        $NavsModel = new PageNav();
        $res = $NavsModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 页面配置信息删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $NavsModel = new PageNav();
        $res = $NavsModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }





}
