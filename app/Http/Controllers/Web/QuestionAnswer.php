<?php

namespace App\Http\Controllers\web;

use App\Libs\ResponseMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\QuestionAnswer as QA;

class QuestionAnswer extends Controller
{
    /**
     * 获取QA问答列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQuestionList(){
        $QAModel        = new QA();
        $data           = $QAModel->getQuestionList();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 修改或者添加QA问答
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function questionAnswerSave(Request $request){
        $getData = ['problem', 'answer'];
        if (!$request->filled($getData)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请填写完整信息')->response());
        }

        $data       = $request->only($getData);
        $id         = $request->get('id', '');
        $QAModel    = new QA();
        $result     = $QAModel->questionAnswerSave($id, $data);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除QA问答
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function questionAnswerRemove(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请选择一条QA问答')->response());
        }

        $id         = $request->get('id');
        $QAModel    = new QA();
        $result     = $QAModel->questionAnswerRemove($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }
}
