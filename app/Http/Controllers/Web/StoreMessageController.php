<?php

namespace App\Http\Controllers\Web;

use App\Libs\ResponseMessage;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreMessageController extends Controller{

    /**
     * 消息列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function messageList(Request $request){

        if (!$request->filled('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $storeId        = $request->get('stores_id');
        $messageModel   = new Message();

        $list           = $messageModel->messageList($storeId);

        $readId         = [];
        foreach ($list as $item) {
            $readId[]   = $item->id;
        }

//        $messageModel->messageRead($readId);
        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }
}
