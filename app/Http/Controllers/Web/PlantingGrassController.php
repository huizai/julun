<?php
namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\PlantingGrass;
use App\Models\PlantingGrassClassify;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Blocks\PlantingGrassBlock;
use App\Http\Controllers\Tool\JiGuangPushController as Push;

class PlantingGrassController extends Controller{

    /**
     * 种草社区列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassList(Request $request){
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 10);
        $search     = ['title', 'status'];

        $searchData = [];
        foreach ($search as $value) {
            if ($request->filled($value)){
                $searchData[$value] = $request->get($value);
            }
        }

        $grassBlock = new PlantingGrassBlock();
        $list       = $grassBlock->grassList($page, $pageSize, $searchData);
        $pagination = $grassBlock->grassPagination($page, $pageSize, $searchData);

        foreach ($list as $item) {
            $item->content = julun_cut_str($item->content, 12);
        }

        return response()->json(ResponseMessage::getInstance()->success([
            'list'          => $list,
            'pagination'    => $pagination
        ])->response());
    }

    /**
     * 种草社区详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassInfo(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id             = $request->get('id');
        $usersId        = Null;
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 10);

        $grassBlock     = new PlantingGrassBlock();
        $data = $grassBlock->grassInfo($id, $usersId, $page, $pageSize);

        foreach ($data->goods as $value) {
            $value->name = julun_cut_str($value->name, 8);
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 种草社区审核或者修改种草社区贴子、评论状态
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassReview(Request $request){
        if (!$request->filled(['status', 'id', 'type'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $status         = $request->get('status');
        $id             = $request->get('id');
        $type           = $request->get('type');


        $grassBlock     = new PlantingGrassBlock();
        $result         = $grassBlock->grassHidden($id, $type, $status);

        if ($result){
            if ($type == 'grass'){
                $grassModel         = new PlantingGrass();
                $userInfo           = $grassModel->getUserInfo($id);
                if(!empty($userInfo->reg_id)){
                    $push               = new Push();
                    $push->grass($userInfo->reg_id)->push();
                }

            }
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除种草社区贴子或者评论
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassOrCommentDelete(Request $request){
        if (!$request->filled(['id', 'type'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id             = $request->get('id');
        $type           = $request->get('type');

        $grassBlock     = new PlantingGrassBlock();
        $result         = $grassBlock->grassDelete($id, $type);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取种草社区话题分类
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlantingClassify(){
        $plantingClassifyModel      = new PlantingGrassClassify();
        $data                       = $plantingClassifyModel->getClassify();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 修改或者添加种草社区话题分类
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function plantingClassifySave(Request $request){
        if (!$request->filled('name')){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('PARAM_ERROR', '请输入分类名称')->response());
        }
        $id                         = $request->get('id', '');
        $name                       = $request->get('name');
        $plantingClassifyModel      = new PlantingGrassClassify();
        $result                     = $plantingClassifyModel->plantingClassifySave($id, $name);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除种草社区话题分类
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function plantingClassifyRemove(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('PARAM_ERROR', '请选择一个话题分类')->response());
        }

        $id                         = $request->get('id');
        $plantingClassifyModel      = new PlantingGrassClassify();
        $result                     = $plantingClassifyModel->plantingClassifyRemove($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
