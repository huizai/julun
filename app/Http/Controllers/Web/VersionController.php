<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Version;
use App\Libs\ResponseMessage;

class VersionController extends Controller
{

    /**
     * 获取版本信息
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVersionInfo(){
        $versionModel       = new Version();
        $data               = $versionModel->getVersionInfo();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 修改版本信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveVersion(Request $request){
        $postData = ['version', 'is_force', 'url'];
        if (!$request->filled($postData)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $id                     = $request->get('id', '');
        $data                   = $request->only($postData);
        $data['description']    = $request->get('description', '');
        $versionModel           = new Version();
        $result                 = $versionModel->saveVersion($data, $id);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
