<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Web;
use App\Blocks\GoodClassifiesBlock;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\GoodsClassifies;

class GoodsClassifiesController extends Controller{
    /**
     * 商品分类列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = ['company' => $companyId];
        $searchField                = ['name', 'type'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $goodClassifiesBlock = new GoodClassifiesBlock();
        $goodClass = $goodClassifiesBlock->goodClassList($search, $page, $pageSize);
        $pagination = $goodClassifiesBlock->goodClassListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $goodClass, 'pagination' => $pagination])->response());
    }

    /**
     * 商品分类详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search                     = ['company' => $companyId, 'id' => $id];
        $goodClassifiesBlock = new GoodClassifiesBlock();
        $goodClass = $goodClassifiesBlock->goodClassInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($goodClass)->response());
    }


    /**
     * 商品分类修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'icon',
            'parent_id',
            'id',
            'sort'
        ]);
        $validator = Validator::make($data, [
            'id'           => 'required',
            'name'         => 'required',
            'icon'         => 'required',
            'parent_id'    => 'required'
        ], [
            'id.required'          => '分类id不能为空！！',
            'name.required'        => '分类名称不能为空！！',
            'icon.required'        => '分类图标不能为空！',
            'parent_id'            => '上级分类不能为空',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        if ($data['id']==$data['parent_id']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CLASSEXIT', "上级id不能是自己")->failed()->response());
        }
        $goodClassModel = new GoodsClassifies();
        $res = $goodClassModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商品分类添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'icon',
            'parent_id',
            'sort'
        ]);
        $validator = Validator::make($data, [
            'name'         => 'required',
            'parent_id'    => 'required'
        ], [
            'name.required'        => '分类名称不能为空！！',
            'parent_id'            => '上级分类不能为空',
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        $data['company_id']     = $companyId;
        $goodClassModel = new GoodsClassifies();
        //验存  防止分类名重复
        $goodClass = $goodClassModel->where('name', $data['name'])->first();
        if ($goodClass){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "分类名称不能重复")->failed()->response());
        }
        $res = $goodClassModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商品分类删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $goodsModel = new GoodsClassifies();
        $res = $goodsModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }




}
