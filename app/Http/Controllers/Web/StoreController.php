<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Web;
use App\Blocks\StoreBlock;
use App\Http\Controllers\Controller;
use App\Models\CollectMill;
use App\Models\Integral;
use App\Models\Orders;
use App\Models\StoreType;
use App\Models\UserIntegralLog;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Models\Stores;
use App\Http\Requests\StoreAddRequest;
use App\Models\StoreRecommendLeisure;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller{
    public function index(Request $request){  //列表
        global $companyId;
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        $search     = ['name','cid', 'status', 'store_type', 'create_time', 'orderColumn'];//店铺名称，电话号码,店铺分类, 状态
        $searchData = [];
        foreach ($search as $item) {
            if ($request->has($item)) {
                $searchData[$item] = $request->get($item);
            }
        }
        $storeBlock  = new StoreBlock();
        $res=$storeBlock->getStore($searchData,$page,$pageSize);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }

    //用户积分详细
    public function Integral(Request $request){
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        $search     = ['userid','create_time'];//积分列表搜索：用户手机号码、时间段筛选
        $searchData = [];
        foreach ($search as $item) {
            if ($request->has($item)) {
                $searchData[$item] = $request->get($item);
            }
        }
        if(!isset($searchData['userid'])||empty($searchData['userid'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $model=new UserIntegralLog();
        $res=$model->index($searchData,$page,$pageSize);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());

    }


    //用户积分列表
    public function IntegralLists(Request $request){
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        $search     = [ 'mobile','create_time','name'];//积分列表搜索：用户手机号码、时间段筛选
        $searchData = [];
        foreach ($search as $item) {
            if ($request->has($item)) {
                $searchData[$item] = $request->get($item);
            }
        }

        $model=new UserIntegralLog();
        $res=$model->lists($searchData,$page,$pageSize);
        return response()->json(ResponseMessage::getInstance()->success( $res)->response());

    }

//    修改店铺信息
    public function setStoreMsg(Request $request){
        global $companyId;
        $data = $request->only([
            'mobile',               //商户手机号
            'logo',                 //商户logo
            'store_type_id',        //类型id
            'is_lineup',            //是否需要排队
            'is_book',              //是否支持预订
            'delivery_fee',         //配送费
            'address',              //详细地址
            'init_delivery_price',  //起送价格
            'long',                 //经纬度
            'lat',                  //经纬度
            'business_license',     //营业许可证
            'food_license',         //食品许可证
            'am',                   //早上营业时间
            'pm',                   //下午营业时间
            'introduce',            //商家介绍
            'per_capita',           //人均消费
            'contact',              //联系电话
            'take_out',             //是否支持外卖
            'tag',                  //商户标签
            'id',                   //商户id
            'status',               //状态
            'name',                 //商户名称
            'classifity_id',        //商户分类
            'store_type_id',        //商户类型
        ]);


        $data['init_delivery_price'] = $data['init_delivery_price'] * 100;
        $data['per_capita'] = $data['per_capita'] * 100;
        $data['delivery_fee'] = $data['delivery_fee'] * 100;

        if ($request->has('password')){
            $data['password'] = md5($request->post('password'));
        }
        $storeBlock = new StoreBlock();
        $tag = $request->get('tag');
        $res = $storeBlock->adminStoreInfoMsg($data, $tag);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('UPDATE_EXIST')->response());
        }
    }

    /**
     * 修改商户基本信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateBasicInfo(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $companyId;
        $id = $request->post('id');
        //store数据
        $updateData = [
            'name',                         //商户名称
            'account',                      //账号
            'delivery_fee',                 //配送费
            'init_delivery_price',          //配送起送价格
            'is_book',                      //是否开启预约
            'is_lineup',                    //是否开启排队
            'logo',                         //商户logo
            'mobile',                       //商户手机
            'classifity_id',                //商户分类ID
            'store_type_id',                //商户类型ID
            'status',                       //商户审核状态
            'operate',                      //是否营业
            'cuisine_name',                 //商户所属菜系
            'platform_rating',              //平台对商户评分
            'sort'                          //商户排序
        ];
        $data = [];
        foreach ($updateData as  $value) {
            if ($request->has($value)){
                $data[$value] = $request->get($value);
            }
        }
        $data['id'] = $id;

        if ($request->filled('password') && $request->post('password')){
            $data['password'] = md5($request->post('password'));
        }
        //配送费
        if (isset($data['delivery_fee'])){
            $data['delivery_fee'] = $data['delivery_fee'] *100;
        }
        //起送价格
        if (isset($data['init_delivery_price'])){
            $data['init_delivery_price'] = $data['init_delivery_price'] *100;
        }


        //storeInfo数据
        $updateInfoData = [
            'am',                               //早上营业时间
            'pm',                               //下午营业时间
            'longitude',                        //经度
            'latitude',                         //纬度
            'contact',                          //联系电话
            'introduce',                        //商家介绍
            'per_capita',                       //人均消费
            'take_out',                         //是否支持外卖
            'ali_pay',                          //支付宝账号
            'wechat_pay',                       //微信账号
            'bank_card',                        //银行卡号
            'address',                          //商户地址
            'business_license',                 //营业许可证
            'food_license',                     //食品许可证
        ];
        $infoData = [];
        foreach ($updateInfoData as $key =>$value) {
            if ($request->has($value)){
                $infoData[$value] = $request->get($value);
            }
        }
        $infoData['store_id'] = $id;
        //人均消费
        if (isset($infoData['per_capita'])){
            $infoData['per_capita'] = $infoData['per_capita'] * 100;
        }

        if (isset($infoData['long'])){
            $infoData['longitude']  = $infoData['long'];
            unset($infoData['long']);
        }

        if (isset($infoData['lat'])){
            $infoData['latitude']  = $infoData['lat'];
            unset($infoData['lat']);
        }

        $devices = $request->post('devices', []);
        $tag = $request->post('tag', []);
        $storeImage = $request->post('image', []);

        $storeBlock = new StoreBlock();
        $result = $storeBlock->updateBasicInfo($data, $infoData, $devices, $companyId, $storeImage, $tag);

        if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        return response()->json(ResponseMessage::getInstance()->success()->response());
    }

    /**
     * 获取店铺详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeInfo(Request $request){
        $storeId = $request->get('store_id');
        $id = $request->get('id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $storeBlock = new StoreBlock();
        $store = $storeBlock->storeAdminInfo($storeId, $id);
        $store->image = $storeBlock->getStoreImage($storeId);
        return response()->json(ResponseMessage::getInstance()->success($store)->response());
    }

    /**
     * 收藏店铺
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectStore(Request $request){
        $storeId = $request->get('id');
        $millId  = $request->get('store_id');
        $type    = $request->get('type', 0);//0：取消收藏 1：收藏
        if (!isset($storeId) || !isset($millId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $data = [
            'mill_id'   => $millId,
            'store_id'  => $storeId
        ];
        $storeBlock = new StoreBlock();
        $collect = $storeBlock->collectStore($data, $type);
        if($collect === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 店铺收藏列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectList(Request $request){
        global $companyId;
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $storeId = $request->get('store_id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search     = ['name'];//店铺名称
        $searchData = ['store_id' => $storeId];
        foreach ($search as $item) {
            if ($request->has($item)) {
                $searchData[$item] = $request->get($item);
            }
        }
        $searchData['collect'] = 1;
        $storeBlock = new StoreBlock();
        $res    = $storeBlock->storeAdminList($searchData, $page, $pageSize);
        $pagination = $storeBlock->storeAdminListPagination($searchData, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $res, 'pagination' => $pagination])->response());
    }

    /**
     * 店铺批量取消收藏
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectCancel(Request $request){
        global $companyId;
        $storeId = $request->get('store_id');
        $millId = $request->get('mill_id', []);
        $collectMillModel = new CollectMill();
        $res = $collectMillModel
            ->whereIn('mill_id', $millId)
            ->where('store_id', $storeId)
            ->delete();
        if ($res!==false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }


    /**
     * 获取店铺分类列表
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function classifyList(Request $request){
        global $companyId;
        $typeId       = $request->get('type_id',0);
        $storeBlock   = new StoreBlock();
        $classifyList = $storeBlock->classifyList($companyId,  $typeId);
        return response()->json(ResponseMessage::getInstance()->success($classifyList)->response());
    }

    /**
     * 获取店铺类型列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function typeList(){
        global $companyId;
        $storeTypeModel = new StoreType();
        $typeList = $storeTypeModel->get();
        return response()->json(ResponseMessage::getInstance()->success($typeList)->response());
    }

    /**
     * 添加店铺
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeAdd(StoreAddRequest $request){
        global $companyId;
        $data = $request->only([
            'classifity_id',        //分类id
            'name',                 //商户名称
            'mobile',               //商户手机号
            'password',             //商户密码
            'logo',                 //商户logo
            'store_type_id',        //类型id
            'is_lineup',            //是否需要排队
            'is_book',              //是否支持预订
            'delivery_fee',         //配送费
            'init_delivery_price',  //起送价格
            'address',              //详细地址
            'long',                 //经纬度
            'lat',                  //经纬度
            'business_license',     //营业许可证
            'food_license',         //食品许可证
            'am',                   //早上营业时间
            'pm',                   //下午营业时间
            'introduce',            //商家介绍
            'per_capita',           //人均消费
            'contact',              //联系电话
            'take_out',             //是否支持外卖
            'tag',                  //商户标签
        ]);
        $data['company_id']             = $companyId;
        $data['password']               = md5($data['password']);
        $data['create_time']            = date('Y-m-d H:i:s', time());
        $tag                            = $request->get('tag');

        if (isset($data['init_delivery_price'])){
            $data['init_delivery_price']    = $data['init_delivery_price'] * 100;
        }

        if (isset($data['delivery_fee'])){
            $data['delivery_fee']    = $data['delivery_fee'] * 100;
        }

        if (isset($data['delivery_fee'])){
            $data['delivery_fee']    = $data['delivery_fee'] * 100;
        }

        $storeBlock = new StoreBlock();
        $res = $storeBlock->addStore($data, $tag);
        if (is_int($res)){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->setFailedCode('123', $res)->failed()->response());
        }
    }

    /**
     * 添加商户
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addStoreBasic(Request $request){
        $getData = ['classifity_id', 'mobile', 'name', 'store_type_id'];
        if (!$request->filled($getData)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请填写完整信息')->response());
        }

        $data = $request->only(['classifity_id', 'mobile', 'name', 'store_type_id']);
        $data['password']   = md5(123456);
        $data['account']    = date('Ymd').mt_rand(999, 9999);
        $data['logo']       = env('APP_URL').'/images/logo.png';

        switch ($data['store_type_id'])
        {
            case 1:
                $data['roles_id'] = 2;
                break;
            case 2:
                $data['roles_id'] = 3;
                break;
            case 3:
                $data['roles_id'] = 4;
                break;
            case 4:
                $data['roles_id'] = 6;
                break;
        }

        $storeBlock = new StoreBlock();
        $result = $storeBlock->addStoreBasic($data);
        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('NAME_ERROR', '当前商户已存在，请修改商户名称')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('NAME_ERROR', '该手机号已被注册，请更换手机号')->response());
        }else if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 店铺删除
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeDel(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $Stores = new Stores();
        $res = $Stores->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 修改商户状态
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStoreStatus(Request $request){
        if (!$request->has(['id','status'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->post('id');
        $status = $request->post('status');

        $storeBlock = new StoreBlock();
        $result = $storeBlock->updateStoreStatus($id, $status);

        if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        return response()->json(ResponseMessage::getInstance()->success()->response());
    }


    /**
     * 删除精选好店
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteChosenStore(Request $request){
        if (!$request->has('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->post('stores_id');

        $storeBlock = new StoreBlock();
        $result = $storeBlock->deleteChosenStore($id);

        if($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 修改精选好店信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateChosenStore(Request $request){
        if (!$request->has('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id         = $request->post('stores_id');
        $sort       = $request->post('sort',0);
        $image      = $request->post('image', '');

        $storeBlock = new StoreBlock();
        $result     = $storeBlock->updateChosenStore($id, $image, $sort);

        if($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 添加精选好店信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addChosenStore(Request $request){
        if (!$request->has('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $store_id   = $request->post('stores_id');
        $sort       = $request->post('sort',0);
        $image      = $request->post('image', '');

        $storeBlock = new StoreBlock();
        $result     = $storeBlock->addChosenStore($store_id, $image, $sort);

        if($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取推荐休闲娱乐商家
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRecommendLeisureStore(Request $request){
        $recommendStore = new StoreRecommendLeisure();
        $storeBlock     = new StoreBlock();
        $store = $recommendStore->getStoreId();
        $storeId = [];
        foreach ($store as $item) {
            $storeId[] = $item->store_id;
        }

        $data = $storeBlock->storeList(['id' => $storeId]);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 添加休闲娱乐推荐店铺
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recommendLeisureStoreSave(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed()
            ->setFailedCode('ERROR', '请选择商户')->response());
        }

        $id = $request->get('id');
        $recommendStore = new StoreRecommendLeisure();
        $result = $recommendStore->saveStore($id);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('SAVE_ERROR', '推荐店铺最多存在4家')->response());
        }else if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

    }

    /**
     * 删除休闲娱乐推荐商家
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recommendLeisureStoreRemove(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '请选择商户')->response());
        }

        $id = $request->get('id');
        $recommendStore = new StoreRecommendLeisure();
        $result = $recommendStore->deleteStore($id);

        if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 获取商户合同
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreContract(Request $request){
        $storeId = $request->get('stores_id');
        $storeBlock = new StoreBlock();
        $data = $storeBlock->getStoreContract($storeId);
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 上传商户合同信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadStoreContract(Request $request){
        $getData = ['start_time', 'end_time', 'image', 'document', 'mobile', 'stores_id'];
        if (!$request->filled($getData)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请填写完整信息')->response());
        }
        $id = $request->get('id', '');

        $data = $request->only($getData);
        $data['remind_time'] =  date( 'Y-m-d', strtotime("{$data['end_time']} -1 month") );
        $storeBlock = new StoreBlock();
        $result = $storeBlock->uploadStoreContract($data, $id);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取品牌商家
     * @return \Illuminate\Support\Collection
     */
    public function getStoreBrand(){
        $storeBlock     = new StoreBlock();
        $data = $storeBlock->getStoreBrand();
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 添加品牌商家
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveStoreBrand(Request $request){
        if (!$request->filled('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $id             = $request->get('stores_id');
        $storeBlock     = new StoreBlock();
        $brand          = $storeBlock->getStoreBrandById($id);
        if ($brand){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该商户已为品牌商户，请勿重复添加')->response());
        }

        $result = $storeBlock->saveStoreBrand($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除品牌商家
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeStoreBrand(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $id             = $request->get('id');
        $storeBlock     = new StoreBlock();
        $result = $storeBlock->removeStoreBrand($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    public function getStoreType(){
        $storeTypeModel         = new StoreType();
        $data = $storeTypeModel->getStoreType();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    public function getStoreOrderExportData(Request $request){
        $orderModel     = new Orders();
        $userModel      = new Users();
        $storeId        = $request->get('stores_id', '');

        $prefix = env('DB_PREFIX');
        $sql = DB::table($orderModel->getTable().' as o')
            ->select(
                'o.order_sn',
                'u.name',
                DB::raw("convert({$prefix}o.pay_amount/100, decimal(15,2)) as pay_amount"),
                'o.create_time',
                DB::raw("(CASE WHEN {$prefix}o.pay_type = 1 THEN '微信支付' WHEN {$prefix}o.pay_type = 2 THEN '支付宝支付' ELSE '余额支付' END) AS pay_type"),
                DB::raw("if({$prefix}o.status= 2, '待收货', '已完成') as status")
            )
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'o.users_id')
            ->whereIn('o.status', [2,3]);


        if (!empty($storeId)){
            $sql->where('o.stores_id', $storeId);
        }
            $data = $sql->orderByDesc('o.create_time')
            ->get();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

}
