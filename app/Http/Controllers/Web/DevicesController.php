<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Web;
use App\Blocks\DevicesBlock;
use App\Blocks\StoreDevicesBlock;
use App\Models\Devices;
use App\Models\StoreDevices;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DevicesController extends Controller{
    /**
     * 商户服务列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = ['company' => $companyId];
        $searchField                = ['name'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $devicesBlock   = new DevicesBlock();
        $devices        = $devicesBlock->devicesList($search, $page, $pageSize);
        $pagination     = $devicesBlock->devicesListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $devices, 'pagination' => $pagination])->response());
    }

    /**
     * 商户服务详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search                     = ['company' => $companyId, 'id' => $id];
        $devicesBlock = new DevicesBlock();
        $devices = $devicesBlock->devicesInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($devices)->response());
    }


    /**
     * 商户服务修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'id',
            'sort'
        ]);
        $validator = Validator::make($data, [
            'id'           => 'required',
            'name'         => 'required'
        ], [
            'id.required'          => '标签id不能为空！！',
            'name.required'        => '标签名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        //验存  防止标签名重复
        $devicesModel   = new Devices();
        $devices        = $devicesModel->where('name', $data['name'])->first();
        if ($devices && $devices->id!=$data['id']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "标签名称不能重复")->failed()->response());
        }

        $res = $devicesModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商户服务添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'sort'
        ]);
        $validator = Validator::make($data, [
            'name'         => 'required'
        ], [
            'name.required'        => '标签名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        $data['company_id']     = $companyId;
        //验存  防止标签名重复
        $devicesModel   = new Devices();
        $devices        = $devicesModel->where('name', $data['name'])->first();
        if ($devices){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "标签名称不能重复")->failed()->response());
        }

        $res = $devicesModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商户服务删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $devicesModel = new Devices();
        $res = $devicesModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }




}