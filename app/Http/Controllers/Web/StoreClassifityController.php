<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Web;
use App\Blocks\StoreClassifityBlock;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\StoreClassifity;

class StoreClassifityController extends Controller{
    /**
     * 商户分类列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = ['company' => $companyId];
        $searchField                = ['name'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $storeClassifityBlock = new StoreClassifityBlock();
        $storeClass = $storeClassifityBlock->storeClassList($search, $page, $pageSize);
        $pagination = $storeClassifityBlock->storeClassListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $storeClass, 'pagination' => $pagination])->response());
    }

    /**
     * 商户分类详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search                     = ['company' => $companyId, 'id' => $id];
        $storeClassifityBlock = new StoreClassifityBlock();
        $storeClass = $storeClassifityBlock->storeClassInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($storeClass)->response());
    }


    /**
     * 商户分类修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'icon',
            'parent_id',
            'type_id',
            'id',
            'sort'
        ]);
        $validator = Validator::make($data, [
            'id'           => 'required',
            'name'         => 'required',
            'type_id'      => 'required',
            'icon'         => 'required',
            'parent_id'    => 'required'
        ], [
            'id.required'          => '分类id不能为空！！',
            'type_id.required'     => '类型id不能为空！！',
            'name.required'        => '分类名称不能为空！！',
            'icon.required'        => '分类图标不能为空！',
            'parent_id'            => '上级分类不能为空',
        ]);

        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        if ($data['id'] == $data['parent_id']) {
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CLASSEXIT', "上级分类不能是自己")->failed()->response());
        }

        $storeClassModel = new StoreClassifity();
        $storeClass = $storeClassModel->where('name', $data['name'])->first();
        if ($storeClass && $storeClass->id != $data['id']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "分类名称不能重复")->failed()->response());
        }

        $goodClassModel = new StoreClassifity();
        $res = $goodClassModel->where('id', $data['id'])->update($data);
        if ($res !== false) {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商户分类添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'type_id',
            'parent_id',
            'sort',
            'icon'
        ]);
        $validator = Validator::make($data, [
            'name'         => 'required',
            'type_id'      => 'required',
            'parent_id'    => 'required',
            'icon'         => 'required'
        ], [
            'name.required'                 => '分类名称不能为空！',
            'type_id.required'              => '类型id不能为空！',
            'icon.required'                 => '分类图标不能为空！',
            'parent_id.required'            => '上级分类不能为空！',
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        $data['company_id']     = $companyId;
        $storeClassModel = new StoreClassifity();
        //验存  防止分类名重复
        $storeClass = $storeClassModel->where('name', $data['name'])->first();
        if ($storeClass){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "分类名称不能重复")->failed()->response());
        }
        $res = $storeClassModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商户分类删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $storeClassModel = new StoreClassifity();
        $res = $storeClassModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    public function getStoreClass(){
        $storeClassBlock = new StoreClassifityBlock();
        $data = $storeClassBlock->getStoreClass();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }




}
