<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\About;
use App\Models\ShopSet;
use App\Http\Requests\ShopSetRequest;
use Illuminate\Http\Request;

class ShopController extends Controller{
    /**
     * 商城设置
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function set(ShopSetRequest $request){
        global $companyId;
        $data = $request->only(['tel', 'logo', 'title', 'description']);

        $Shop = new ShopSet();
        $shop = $Shop::where('company_id', $companyId)->orderBy('id','desc')->first();

        if (!isset($shop->id)){
            $data['company_id']     = $companyId;
            $data['create_time']    = date('Y-m-d H:i:s', time());
            $res = $Shop::create($data);
        }else{
            $res = $Shop::where('id', $shop['id'])->where('company_id',$companyId)->update($data);
        }
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('CREATE_EXIST')->response());
        }

    }

    /**
     * 关于我们
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function about(Request $request){
        global $companyId;
        $data = $request->only(['text']);
        $aboutModel = new About();
        $about = $aboutModel::where('company_id', $companyId)->orderBy('id','desc')->first();

        if (!isset($about->id)){
            $data['company_id']     = $companyId;
            $res = $aboutModel::create($data);
        }else{
            $res = $aboutModel::where('id', $about->id)->where('company_id',$companyId)->update($data);
        }
        if ($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('CREATE_EXIST')->response());
        }

    }
    /**
     * 关于我们详情
     * @return \Illuminate\Http\JsonResponse
     */
    public function aboutInfo(){
        global $companyId;
        $aboutModel = new About();
        $about = $aboutModel::where('company_id', $companyId)->first();
        return response()->json(ResponseMessage::getInstance()->success($about)->response());

    }
}