<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-30
 * Time: 下午2:13
 */
namespace  App\Http\Controllers\Web;



use App\Http\Controllers\Controller;
use App\Models\WechatBaseConfig;
use App\Models\WechatPayConfig;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;

class ConfigureController extends Controller {



    public function index(){ //配置信息
        global $companyId;

        $wechat_pay_config = new WechatPayConfig();
        $info = $wechat_pay_config->weChat_confInfo($companyId);//支付配置

        $wechat_base_config = new WechatBaseConfig();
        $base=$wechat_base_config->weChat_confBase($companyId);//基本配置
        return response()->json(ResponseMessage::getInstance()->success(['info' => $info,'base'=>$base])->response());
    }

/*    public function confUpdate(Request $request){
        global $companyId;
        $status=$request->get('status');//1为基本配置 2为支付配置
      if($status=='1'){
          $id=$request->get('id');
          $search = ['secret', 'type', 'aes_key', 'appid','token'];
          $searchData = [];
          foreach ($search as $item) {
              if ($request->has($item)) {
                  $searchData[$item] = $request->get($item);
              }
          }
          $wechat_base_config = new WechatBaseConfig();
          $searchData['company_id']=$companyId;
          $res=$wechat_base_config->weChat_confUpdate($id,$searchData);
          print_r($res);

      }elseif ($status=='2'){
          $search = ['username', 'mobile', 'time', 'movie_title'];
          $searchData = [];
          foreach ($search as $item) {
              if ($request->has($item)) {
                  $searchData[$item] = $request->get($item);
              }
          }
      }
    }*/

    public function setAlertOver(Request $request){
//        可以不仅仅是这一个发送接收源在env增加就行了
        $data = $request->only(['SOURCE','TIXIAN_RECEIVER']);
        $res = modifyEnv($data);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }
}