<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\AdminUser;
use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use Illuminate\Support\Facades\Redis;

class LoginController extends Controller
{
    /**
     * 后台登陆
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(AdminRequest $request)
    {
        $data = $request->only(['username', 'password']);
//        $validator = Validator::make($data, [
//            'username' => 'required',
//            'password' => 'required',
//        ], [
//            'username.required' => '用户名不能为空！！',
//            'password.required' => '密码不能为空！',
//        ]);
//        if ($validator->fails()) {
//            $res = [
//                'type' => $validator->errors()->keys()[0],
//                'message' => $validator->errors()->first()
//            ];
//            return response()->json(ResponseMessage::getInstance()->failed($res)->response());
//        }
        $data['password'] = md5($data['password']);
        $User = new AdminUser();
        $user = $User::where('username', '=', $data['username'])->first();

        if(!$user){
            return response()->json(ResponseMessage::getInstance()->failed("LOGIN_FAILED")->response());
        }

        if ($user->status == 1) {
            return response()->json(ResponseMessage::getInstance()->failed("PROHIBIT")->response());
        }

        if (!$user || $user->delete_time) {
            return response()->json(ResponseMessage::getInstance()->failed("NO_USER")->response());
        }

        if ($data['password'] != $user->password){
            return response()->json(ResponseMessage::getInstance()->failed("PASSWORD_ERROR")->response());
        }

        $session['username'] = session('username', $data['username']);
        $session['loginTime'] = session('loginTime', time());
        $session['company_id'] = session('company_id', $user['company_id']);
        $session['admin_id'] = session('admin_id', $user['id']);
        $token = md5($session['username'] . $session['loginTime']);
        \Cache::put($token, $session, 24 * 60 * 30);

        $res = [
            'username' => $user->username,
            'company_id' => $user->company_id,
            'id' => $user->id,
            'avatar' => $user->avatar,
            'token' => $token,
            'rules' => [$user->rules]
        ];

        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }


    public function testRedis(){
        Redis::set('name', 'guwenjie');
        $values = Redis::get('name');
        dd($values);
    }
}
