<?php

namespace App\Http\Controllers\Web;

use App\Blocks\StoreCouponsBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OfflineCouponsController extends Controller{

    /**
     * 线下优惠券列表（注册礼包）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function offlineCouponsList(Request $request){
        $page                   = $request->get('page', 1);
        $pageSize               = $request->get('pageSize', 10);
        $storeCouponBlock       = new StoreCouponsBlock();
        $data                   = $storeCouponBlock->offlineCouponsList($page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 添加或者修改线下优惠券（注册礼包）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveOfflineCoupons(Request $request){
        $data       = $request->only(['name', 'stores_id', 'discount_amount', 'rule', 'effective_time', 'address']);
        $validator  = Validator::make($data,[
            'name'                  => 'required',
            'stores_id'             => 'required',
            'discount_amount'       => 'required',
            'rule'                  => 'required',
            'effective_time'        => 'required',
            'address'               => 'required'
        ],[
            'stores_id.required'        => '请选择商户',
            'name.required'             => '请输入优惠券名称',
            'discount_amount.required'  => '请输入优惠券金额',
            'rule.required'             => '请输入优惠券使用规则',
            'effective_time.required'   => '请输入优惠券有效时间',
            'address.required'          => '请输入使用地址'
        ]);

        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $data['discount_amount']    = $data['discount_amount']*100;
        $id                         = $request->get('id', '');
        $storeCouponBlock           = new StoreCouponsBlock();
        $result                     = $storeCouponBlock->saveOfflineCoupons($id, $data);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除线下优惠券（注册礼包）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeOfflineCoupon(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请选择一张优惠券')->response());
        }

        $id                 = $request->get('id');
        $storeCouponModel   = new StoreCouponsBlock();
        $result             = $storeCouponModel->removeOfflineCoupon($id);
        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}