<?php
namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\Integral;
use Illuminate\Http\Request;

class IntegralController extends Controller{

    /**
     * 积分列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $integralModel  = new Integral();
        $data           = $integralModel->index();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


    /**
     * 修改积分
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateIntegral(Request $request){
        if (!$request->has('id', 'integral')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id             = $request->get('id');
        $integral       = $request->get('integral');
        $integralModel  = new Integral();
        $result         = $integralModel->updateIntegral($id, $integral);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

    /**
     * 删除该操作积分
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteIntegral(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id             = $request->get('id');
        $integralModel  = new Integral();
        $result         = $integralModel->deleteIntegral($id);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }





}
