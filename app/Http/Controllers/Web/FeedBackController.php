<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedBackController extends Controller{

    /**
     * 获取用户反馈意见
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserFeedBack(Request $request){
        $page               = $request->get('page', 1);
        $pageSize           = $request->get('pageSize', 10);
        $feedBackModel      = new Feedback();
        $data  = $feedBackModel->getUserFeedBack($page, $pageSize);
        foreach ($data as $value) {
            if (!empty($value->images)){
                $value->images = explode(',', $value->images);
            }
        }


        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }
}