<?php
namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;
use App\Models\UserRechargeLog;

class UserRechargeController extends Controller{

    /**
     * 获取用户充值记录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request){
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        $search     = $request->only(['pay_type', 'time', 'mobile']);
        $userRechargeModel      = new UserRechargeLog();
        $data                   = $userRechargeModel->list($page, $pageSize, $search);
        $time = [];
        if (isset($search['time'])){
            $time = $search['time'];
        }
        $payType = [];
        if (isset($search['pay_type'])){
            $payType = $search['pay_type'];
        }
        $data['count']          = $userRechargeModel->getUserRechargeCountMoney($time, $payType);
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取某一时间段需要导出的数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function exportExcelData(Request $request){
//        $time = $request->get('time');
//        if (empty($time)){
//            return response()->json(ResponseMessage::getInstance()->failed()->setFailedCode('PARAM_ERROR', '请选择时间段')->response());
//        }
//        $userRechargeModel      = new UserRechargeLog();
//        $data = $userRechargeModel->exportExcelData($time);
        $search     = $request->only(['pay_type', 'time', 'mobile']);
        $searchData = [];
//        foreach ($search as $item) {
//            if ($request->has($item)) {
//                print_r($request->input("$item"));
//                $searchData[$item] = $request->input($item);
//            }
//        }
//        print_r($search);print_r($searchData);die;
        $storeBlock  = new UserRechargeLog();
        $res=$storeBlock->exportExcelData($search);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }
}