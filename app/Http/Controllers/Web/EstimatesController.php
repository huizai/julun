<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Web;

use App\Blocks\GoodsEstimatesBlock;
use App\Models\GoodsEstimates;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EstimatesController extends Controller{
    /**
     * 商品评论列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['name', 'store_id'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $goodsEstimatesBlock    = new GoodsEstimatesBlock();
        $goodsEstimates         = $goodsEstimatesBlock->estimatesList($search, $page, $pageSize);
        $pagination             = $goodsEstimatesBlock->estimatesListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $goodsEstimates, 'pagination' => $pagination])->response());
    }

    /**
     * 商品评论详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search     = ['id' => $id];
        $goodsEstimatesBlock    = new GoodsEstimatesBlock();
        $goodsEstimates         = $goodsEstimatesBlock->estimatesInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($goodsEstimates)->response());
    }

    /**
     * 商品评论删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $goodsEstimatesModel  = new GoodsEstimates();
        $res = $goodsEstimatesModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商品评论回复
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reply(Request $request){
        global $companyId;
        $data = $request->only([
            'estimate',
            'p_id',
            'users_id',
        ]);
        $validator = Validator::make($data, [
            'estimate'          => 'required',
            'p_id'              => 'required',
            'users_id'           => 'required',
        ], [
            'estimate.required'         => '回复内容不能为空！！',
            'p_id.required'             => '参数错误！！',
            'users_id.required'          => '参数错误！！',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $data['goods_id'] = 0;
        $data['score'] = 0;

        $goodsEstimatesBlock    = new GoodsEstimatesBlock();
        $res = $goodsEstimatesBlock->estimatesReply($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("CREATE_ERROR")->response());
        }
    }

    /**
     * 同意商品评论
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkGoodsEstimate(Request $request){
        if(!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->get('id');
        $goodsEstimatesBlock    = new GoodsEstimatesBlock();
        $result = $goodsEstimatesBlock->checkGoodsEstimate($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }



}
