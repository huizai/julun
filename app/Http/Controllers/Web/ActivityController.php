<?php
namespace App\Http\Controllers\Web;

use App\Libs\ResponseMessage;
use App\Models\Activity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityController extends Controller{

    /**
     * 公益活动列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activityList(Request $request){
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $title          = $request->get('title', '');

        $activityModel  = new Activity();
        $data           = $activityModel->activityList($page, $pageSize, $title);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 添加公益活动
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addActivity(Request $request){
        $isPost = [
            'title',
            'thumbnail',
            'content',
            'start_time',
            'hour_time',
            'address',
            'people',
            'is_free'
        ];
        foreach ($isPost as $item) {
            if (!$request->filled($item)){
                return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
            }
        }

        $data                   = $request->only($isPost);
        $data['sort']           = $request->get('sort', 0);
        $data['company_id']     = 1;

        $activityModel  = new Activity();
        $result         = $activityModel->addActivity($data);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 修改公益活动
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateActivity(Request $request){
        $isPost = [
            'id',
            'title',
            'thumbnail',
            'content',
            'start_time',
            'hour_time',
            'address',
            'people',
            'is_free'
        ];
        foreach ($isPost as $item) {
            if (!$request->filled($item)){
                return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
            }
        }

        $data                   = $request->only($isPost);
        $data['sort']           = $request->get('sort', 0);
        $data['company_id']     = 1;

        $activityModel  = new Activity();
        $result         = $activityModel->updateActivity($data);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }


    /**
     * 删除公益活动
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteActivity(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id             = $request->get('id');
        $activityModel  = new Activity();
        $result         = $activityModel->deleteActivity($id);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
