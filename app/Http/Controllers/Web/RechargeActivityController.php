<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\RechargeActivity;
use Illuminate\Http\Request;

class RechargeActivityController extends Controller{

    /**获取充值活动列表
     * @return array
     */
    public function list(){
        $rechargeActivityModel  = new RechargeActivity();
        $data = $rechargeActivityModel->list();

        return ResponseMessage::getInstance()->success($data)->response();
    }

    /**
     * 修改充值活动
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function modification(Request $request){
        if (!$request->filled(['id', 'start_time', 'end_time', 'condition', 'send'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $data               = $request->only(['id', 'start_time', 'end_time', 'condition', 'send']);
        $data['condition']  = $data['condition'] * 100;
        $data['send']       = $data['send'] * 100;

        $rechargeActivityModel  = new RechargeActivity();
        $result = $rechargeActivityModel->modification($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());
    }

    /**
     * 添加充值活动
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addition(Request $request){
        if (!$request->filled(['start_time', 'end_time', 'condition', 'send'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $data               = $request->only(['start_time', 'end_time', 'condition', 'send']);
        $data['condition']  = $data['condition'] * 100;
        $data['send']       = $data['send'] * 100;

        $rechargeActivityModel  = new RechargeActivity();
        $result = $rechargeActivityModel->addition($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());
    }

    /**
     * 删除充值活动
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->get('id');
        $rechargeActivityModel  = new RechargeActivity();
        $result = $rechargeActivityModel->remove($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());
    }
}