<?php

namespace App\Http\Controllers\web;

use App\Libs\ResponseMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RedEnvelope;

class RedEnvelopeController extends Controller
{

    /**
     * 获取红包列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function redEnvelopeList(){
        $redEnvelopeModel       = new RedEnvelope();
        $data                   = $redEnvelopeModel->getRedEnvelopeList();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 添加或者修稿红包
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function redEnvelopeSave(Request $request){
        $getData = ['title', 'full_amount', 'discount_amount', 'type', 'effective_time'];
        if (!$request->filled($getData)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请填写完整信息')->response());
        }

        $data                    = $request->only($getData);
        $id                      = $request->get('id', '');
        $redEnvelopeModel        = new RedEnvelope();
        $data['discount_amount'] = $data['discount_amount'] *100;
        $data['full_amount']     = $data['full_amount'] *100;
        $result     = $redEnvelopeModel->redEnvelopeSave($id, $data);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('SAVE_ERROR', '只能添加一个新用户首单红包')->response());
        }else if($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 删除红包
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function redEnvelopeRemove(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请选择一个红包')->response());
        }

        $id                 = $request->get('id');
        $redEnvelopeModel   = new RedEnvelope();
        $result     = $redEnvelopeModel->redEnvelopeRemove($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

}
