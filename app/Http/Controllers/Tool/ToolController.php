<?php

namespace App\Http\Controllers\Tool;
use App\Http\Controllers\Controller;
use App\Http\Requests\SmsRequest;
use App\Libs\Qiruiyun;
use App\Libs\ResponseMessage;
use App\Models\District;
use Illuminate\Http\Request;
use zgldh\QiniuStorage\QiniuStorage;
use QL\QueryList;

class ToolController extends Controller {
    /**
     * 发送短信验证码
     * @param SmsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginAuthCode(SmsRequest $request){
        $mobile = trim($request->get('mobile'));
        $code = getSalt('6' , 1);

        $time = 1;

        //测试手机号
        $testMobile = [
            '15074766120',
            '13112341234',
            '18874130825',
            '15673396137'
        ];

        if(in_array($mobile, $testMobile)){
            $code = '123456';
            \Cache::put($mobile , $code , $time);
            $res = true;

        }else{
            \Cache::put($mobile , $code , $time);

            //启瑞云平台发送短信
            //---start---
            $smsLib = new Qiruiyun();
            $res = $smsLib->send($mobile, "【长沙财鼎商业管理有限公司】验证码{$code}。请勿将验证码泄露于他人。");
            //---end-----
        }

        if($res === true) {
            \Log::debug("手机号码{$mobile},验证码{$code}");
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    //图片验证码
    public function captcha(){
        //需要gd库并且需要支持freetype
        $captcha['url'] = captcha_src();
        return response()->json(ResponseMessage::getInstance()->success($captcha)->response());
    }

    public function captchaValidate(Request $request)
    {
        $rules = ['captcha' => 'required|captcha'];
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 上传图片
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImg(Request $request) {
        $param = 'img';
        if($request->hasFile($param)){
            $file = $request->file($param);

            //上传到本地
//            $destinationPath	= public_path()."/upload";
//            $filename			= uniqid();
//
//            $exten = substr($file->getClientOriginalName()  , strpos($file->getClientOriginalName() , '.' ) + 1 );
//
//            if( $request->file($param)->isValid() ){
//                if( $file->move($destinationPath , $filename . '.' . $exten )  ){
//                    $filePath = env('APP_URL')."/upload/".$filename.'.'.$exten;
//                    return response()->json(ResponseMessage::getInstance()->success(['img_path' => $filePath])->response());
//                }
//            }

            //上传到七牛
            // 初始化
            $disk = QiniuStorage::disk('qiniu');
            // 重命名文件
            $fileName = md5($file->getClientOriginalName().time().rand()).'.'.$file->getClientOriginalExtension();

            // 上传到七牛
            $bool = $disk->put('image_'.$fileName,file_get_contents($file->getRealPath()));
            // 判断是否上传成功
            if ($bool) {
                $path = $disk->downloadUrl('image_'.$fileName);
                return response()->json(ResponseMessage::getInstance()->success(['img_path' => $path])->response());
            }
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());
    }

    /**
     * 获取地址
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function areaLinkAge(Request $request){
        $pid = $request->get('pid', 1);
        $area = District::where('parent_id', $pid)->orderBy('adcode')->get();
        return response()->json(ResponseMessage::getInstance()->success($area)->response());
    }


    /**
     * 获取验证码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateCodeFromSms(Request $request)
    {
        $mobile = $request->get('mobile');
        if (!is_numeric($mobile) && strlen($mobile) != 11){
            return response()->json(ResponseMessage::getInstance()->failed('MOBILE_FORMAT_ERROR')->response());
        }
//        if(!isMobileNum($mobile)){
//            return response()->json(ResponseMessage::getInstance()->failed('MOBILE_FORMAT_ERROR')->response());
//        }
        $code = getSalt(6, 1);

        $testMobile = explode(',', env('TEST_MOBILE'));
//        $testMobile = ['18874130125'];
        if (in_array($mobile, $testMobile)) {
            $code = '123456';
            \Cache::put($mobile, $code, 1);
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        \Cache::put($mobile, $code, 1);

        $response = \App\Libs\Aliyun\Sms::VerCode($mobile, $code);
        \Log::debug(json_encode($response));
        if ($response->Code === 'OK') {
            \Log::debug($code);
            \Cache::put($mobile, $code, 1);
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else if ($response->Code === 'isv.BUSINESS_LIMIT_CONTROL') {
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('SMS_ERROR', '验证码发送次数过多，请稍后再试')->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


//    获取京东地区数据
    public function getArea(){
        $ql = QueryList::get('https://trade.jd.com/shopping/order/getOrderInfo.action?rid=1561532612326');

        $rules = [
//            'area_id'   => $ql->find('.ui-area-content .ui-switchable-panel .ui-area-content-list>li>a')->attrs('data-id'),
            'area'      => $ql->find('#js_N_navHighlight .ntes-nav-login-title')->attrs('href')

        ];

        $rules = json_decode(json_encode($rules),true);
dd($rules);
        $maoyanIds = [];
        foreach ($rules['maoyan_id'] as $key => $rule) {

            $deUrl = urldecode($rule);

            preg_match('/movie\/(\d+)/', $deUrl, $result);

            if(isset($result[1])){
                $maoyanIds[] = $result[1];
            }

        }

        $rules['maoyan_id'] = $maoyanIds;
        return $rules;
    }
}
