<?php

namespace App\Http\Controllers\Tool;
use App\Http\Controllers\Controller;
use App\Libs\OSS;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;

class AliyunOssController extends Controller {

    /**
     * 上传图片（阿里云）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImage(Request $request){
        $param = 'image';
        $action = $request->get('action');
        if($request->hasFile($param)) {
            $image = $request->file($param);
            $content_type = mime_content_type($image->getRealPath());
            $extension = empty($image->getClientOriginalExtension()) ? $image->extension() : $image->getClientOriginalExtension();
            $file_name = $action . '/' . time() . rand(10, 99) . '.' . $extension;
            if(!$image->isValid()){
                return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
            }

            $content = file_get_contents($image);
            $bucket_name = \Config::get('alioss.BucketName');
            OSS::publicUploadContent($bucket_name, $file_name, $content, ['ContentType' => $content_type]);

            $url = OSS::getPublicObjectURL($bucket_name, $file_name).'?x-oss-process=image/quality,q_50';
            $url = str_replace("http","https",$url);
            return response()->json(ResponseMessage::getInstance()->success($url)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }
}
