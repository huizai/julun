<?php

namespace App\Http\Controllers\Tool;

use JPush\Client AS JPush;

class JiGuangPushController
{


    public $data = [];
    public $regId = '';

    public function push()
    {

        $client = new JPush(config('push.app_key'), config('push.secret'), null);

        try{
            $result = $client->push()
                ->setPlatform(['ios','android'])
                ->addRegistrationId($this->regId)
                ->iosNotification($this->data['alert'], array(
                    'category' => $this->data['title'],
                    'mutable-content' => true,
                    'content-available' => true,
                    'extras' => $this->data['extras'],
                    'badge' => '0'
                ))
                ->androidNotification($this->data['alert'], array(
                    'style' => 3,
                    'title' => $this->data['title'],
                    'extras' => $this->data['extras'],
                ))
                ->message($this->data['alert'], array(
                    'title' => $this->data['alert'],
                ))
                ->options([
                    'apns_production'   => true
                ])
                ->send();

            return $result;

        }catch(\JPush\Exceptions\APIConnectionException $e){
            \Log::error(json_encode($e));
        }catch (\JPush\Exceptions\APIRequestException $e) {
            \Log::error(json_encode($e));
        }

    }

    //排队
    public function lineup($regId, $name, $bookId)
    {
        $this->regId = $regId;
        $params = [
            'back'  => 1,
            'id'    => $bookId,
            'name'  => $name
        ];
        $this->data = [
            'title' => '排队',
            'alert' => "您在{$name}排队成功",
            'extras' => [
                'page' => 'ShopReserveDetail',
                'type' => 'push',
                'params' => \GuzzleHttp\json_encode($params)
            ]
        ];

        return $this;
    }

    //唤醒
    public function wake($regId)
    {
        $this->regId = $regId;
        $params = [];
        $this->data = [
            'title' => '优惠券',
            'alert' => "您收到新的优惠券",
            'extras' => [
                'page' => 'Hb',
                'type' => 'push',
                'params' => \GuzzleHttp\json_encode($params)
            ]
        ];
        return $this;
    }

    //种草审核
    public function grass($regId){
        $this->regId = $regId;
        $params = [];
        $this->data = [
            'title' => '种草社区',
            'alert' => "您在种草社区发布的贴子已通过审核",
            'extras' => [
                'page' => 'MCommunity',
                'type' => 'push',
                'params' => \GuzzleHttp\json_encode($params)
            ]
        ];
        return $this;
    }

    //发货
    public function Ship($regId, $orderId)
    {
        $this->regId = $regId;
        $params = [
            'id'    => $orderId
        ];
        $this->data = [
            'title' => '订单',
            'alert' => "您的订单已发货，请注意查看",
            'message' => 'ship',
            'extras' => [
                'page' => 'OrderDetail',
                'type' => 'push',
                'params' => \GuzzleHttp\json_encode($params)
            ]
        ];
        return $this;
    }


}
