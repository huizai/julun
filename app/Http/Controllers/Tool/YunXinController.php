<?php

namespace App\Http\Controllers\Tool;

use App\Libs\ResponseMessage;
use App\Libs\YunXin\ServerApi;
use App\Http\Controllers\Controller;
use App\Models\StoreYunxin;
use App\Models\UserYunxin;
use Illuminate\Http\Request;

class YunXinController extends Controller
{


    public function __getYunXinObject(){
        $yunXin = new ServerApi();
        return $yunXin;
    }

    /**
     * 刷新云信token
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshUserYunXinToken(){
        global $g_uid;
        $userYunXinModel        = new UserYunxin();
        $userInfo               = $userYunXinModel->getUserYunXinInfo($g_uid);
        $yunXin                 = $this->__getYunXinObject();
        $yunXinInfo             = $yunXin->updateUserToken($userInfo->accid);
        if ($yunXinInfo['code'] == 200){
            $result = $userYunXinModel->updateUserYunXinInfo($g_uid, $userInfo->accid, $yunXinInfo['info']['token']);
            if ($result !== false){
                return response()->json(ResponseMessage::getInstance()->success([
                    'accid'     => $userInfo->accid,
                    'token'     => $yunXinInfo['info']['token']
                ])->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取accid、通信token
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserYunXinInfo(){
        global $g_uid;
        $userYunXinModel    = new UserYunxin();
        $userInfo           = $userYunXinModel->getUserYunXinInfo($g_uid);
        return response()->json(ResponseMessage::getInstance()->success([
            'accid'     => $userInfo->accid,
            'token'     => $userInfo->token
        ])->response());
    }

    public function refreshStoreYunXinToken(Request $request){
        $storeId                = $request->get('stores_id');
        $storeYunXinModel       = new StoreYunxin();
        $storeInfo              = $storeYunXinModel->getStoreYunXinInfo($storeId);
        $yunXin                 = $this->__getYunXinObject();
        $yunXinInfo             = $yunXin->updateUserToken($storeInfo->accid);

        if ($yunXinInfo['code'] == 200){
            $result = $storeYunXinModel->updateStoreYunXinInfo($storeId, $storeInfo->accid, $yunXinInfo['info']['token']);
            if ($result !== false){
                return response()->json(ResponseMessage::getInstance()->success([
                    'accid'     => $storeInfo->accid,
                    'token'     => $yunXinInfo['info']['token']
                ])->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
