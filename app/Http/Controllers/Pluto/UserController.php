<?php

namespace App\Http\Controllers\Pluto;
use App\Blocks\GoodsBlock;
use App\Blocks\OrderBlock;
use App\Blocks\PayBlock;
use App\Blocks\StoreBlock;
use App\Blocks\UserBlock;
use App\Blocks\UserSignInBlock;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UserAddressRequest;
use App\Libs\ResponseMessage;
use App\Libs\YunXin\ServerApi;
use App\Libs\ZhongxinPay\ClientResponseHandler;
use App\Libs\ZhongxinPay\PayHttpClient;
use App\Libs\ZhongxinPay\RequestHandler;
use App\Models\CollectGoods;
use App\Models\CollectStores;
use App\Models\District;
use App\Models\Service;
use App\Models\UserAddress;
use App\Models\UserCoupons;
use App\Models\UserKeyword;
use App\Models\UserMessage;
use App\Models\UserRedEnvelope;
use App\Models\Users;
use App\Models\UserViewRecording;
use App\Models\UserYunxin;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use App\Libs\Ketuo;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Validator;
use Yansongda\Pay\Pay;

class UserController extends Controller {

    public function __getYunXinObject(){
        $yunXin = new ServerApi();
        return $yunXin;
    }
    /**
     * 手机号密码登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginByPassword(Request $request){
        $mobile     = $request->get('mobile');
        $password   = $request->get('password');
        $regId      = $request->get('reg_id', '');

        $userBlock = new UserBlock();
        $user = $userBlock->getUserInfoByMobile($mobile);

        //验证账号是否存在
        if(!$user){
            return response()->json(ResponseMessage::getInstance()->failed('NO_MOBILE')->response());
        }

        if (empty($user->password)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '未设置密码，请使用验证码登录')->response());
        }

        //验证密码是否正确
        if(!\Hash::check($password, $user->password)){
            return response()->json(ResponseMessage::getInstance()->failed('PASSWORD_ERROR')->response());
        }

        $userBlock->saveRegId($user->id, $regId, 'app');

        unset($user->password);
        $token = md5($user->id.date('YmdHis', time()));
        \Cache::put($token, $user, 30);

        $user->token = $token;

        $yunXinModel    = new UserYunxin();
        $userInfo       = $yunXinModel->getUserYunXinInfo($user->id);
        $yunXin         = $this->__getYunXinObject();
        $yunXinInfo     = $yunXin->updateUserToken($userInfo->accid);
        if ($yunXinInfo['code'] == 200){
            if($yunXinModel->updateUserYunXinInfo($user->id, $userInfo->accid, $yunXinInfo['info']['token']) !== false){
                $user->yunxin_token = $yunXinInfo['info']['token'];
                $user->accid = $userInfo->accid;
            }
        }
        return response()->json(ResponseMessage::getInstance()->success($user)->response());
    }

    /**
     * 退出登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginOut(Request $request){
        global $g_uid;
        $token = $request->get('token');
        $user = Users::where('id', $g_uid)->whereNull('delete_time')->first();
        \Cache::forget($token);

        return response()->json(ResponseMessage::getInstance()->success($user)->response());
    }

    /**
     * 手机号验证码登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function loginByVc(Request $request){
        $mobile = $request->get('mobile');
        $regId  = $request->get('reg_id', '');

        $validateCode = $request->get('validate_code');

        $cacheCode = \Cache::get($mobile);

        if($validateCode != $cacheCode){
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        $userBlock = new UserBlock();
        $user = $userBlock->getUserInfoByMobile($mobile);
        if(!$user){
            //用户不存在，则添加用户
            $avatar     = env('APP_URL').'/images/head/default/'.mt_rand(1,10).'.jpg';
            $inviteCode = $request->get('invite_code');
            $name       = $mobile;
            $password   = null;

            $userId = $userBlock->registerUser($mobile, $name, $password, $avatar, $inviteCode);

            $user = $userBlock->getUserInfoById($userId);
            if(!$user){
                //添加不成功
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }

        $userBlock->saveRegId($user->id, $regId, 'app');

        unset($user->password);
        $token = md5($user->id.date('YmdHis', time()));
        \Cache::put($token, $user, 30);

        $user->token = $token;

        $yunXinModel    = new UserYunxin();
        $userInfo       = $yunXinModel->getUserYunXinInfo($user->id);
        $yunXin         = $this->__getYunXinObject();
        $yunXinInfo     = $yunXin->updateUserToken($userInfo->accid);

        if ($yunXinInfo['code'] == 200){
            if($yunXinModel->updateUserYunXinInfo($user->id, $userInfo->accid, $yunXinInfo['info']['token']) !== false){
                $user->yunxin_token = $yunXinInfo['info']['token'];
            }
        }
        return response()->json(ResponseMessage::getInstance()->success($user)->response());
    }


    /**
     * 手机号注册
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerForMobile(RegisterRequest $request){
        $mobile         = $request->get('mobile');
        $name           = '新用户'.chr(rand(97, 122)).substr($mobile, -5);
        $password       = $request->get('password');
        $validateCode   = $request->get('validate_code');
        $regId          = $request->get('reg_id', '');

        $avatar     = env('APP_URL').'/images/head/default/'.mt_rand(1,10).'.jpg';
        $inviteCode = $request->get('invite_code');

        //短信验证码
        $cacheCode = \Cache::get($mobile);
        \Log::debug("储存验证码{$cacheCode},接收验证码{$validateCode}");
        if(!$cacheCode || $validateCode != $cacheCode){
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        $userBlock = new UserBlock();
        $userId = $userBlock->registerUser($mobile, $name, $password, $avatar, $inviteCode, 0, $regId);

        if($userId === -1){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('MOBILE_ERROR', '该手机已被注册')
                ->response());
        }else if($userId){
            return response()->json(ResponseMessage::getInstance()->success(['user_id' => $userId])->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 当前登录用户的信息
     * @return \Illuminate\Http\JsonResponse
     */
    public function currentLoginUserInfo(){
        global $g_uid;
        $user = Users::where('id', $g_uid)->whereNull('delete_time')->first();
        if (!$user){
            return response()->json(ResponseMessage::getInstance()->failed('NO_USER')->response());
        }
        $user->balance = priceIntToFloat($user->balance);
        return response()->json(ResponseMessage::getInstance()->success($user)->response());
    }

    /**
     * 获取用户地址
     * @param UserAddressRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userAddress(){
        global $g_uid;

        $userBlock = new UserBlock();
        $address = $userBlock->getUserAddressList($g_uid, []);
        return response()->json(ResponseMessage::getInstance()->success($address)->response());
    }

    /**
     * 获取地址详情
     */
    public function userAddressInfo($id){
        global $g_uid;
        $userBlock = new UserBlock();
        $address = $userBlock->getUserAddressInfo($g_uid, ['id' => $id]);
        return response()->json(ResponseMessage::getInstance()->success($address)->response());
    }

    /**
     * 获取默认地址地址详情
     */
    public function userAddressDefault(){
        global $g_uid;
        $userBlock = new UserBlock();
        $address = $userBlock->getUserAddressInfo($g_uid, ['default' => 1]);
        return response()->json(ResponseMessage::getInstance()->success($address)->response());
    }


    /**
     * 删除地址
     */
    public function deleteUserAddress($id){
        global $g_uid;
        $status = UserAddress::where('id', $id)->where('users_id', $g_uid)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);

        if($status){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 更新收货地址
     * @param UserAddressRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserAddress(UserAddressRequest $request){
        global $g_uid;

        if(!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->get('id');

        $updateField = [
            'consignee',
            'contact',
            'province',
            'city',
            'district',
            'address',
            'longitude',
            'latitude'
        ];

        $updateData = [];
        foreach($updateField as $field){
            if($request->has($field)){
                $updateData[$field] = $request->get($field);
            }
        }

        $districtModel          = new District();
        $address                = $districtModel->getAddress($updateData['province'], $updateData['city'], $updateData['district']);
        $requestAddress         = '';
        foreach ($address as $item) {
            $requestAddress .= $item->name;
        }
        $requestAddress .= $updateData['address'];
        \Log::debug($requestAddress);
        $url        = 'https://restapi.amap.com/v3/geocode/geo?key=5f07eca1c8780ce236d99d07735607aa&address='.$requestAddress;
        $getAddress = json_decode(curlGet($url), true);
        if ($getAddress['infocode'] == 10000){
            $lagData = explode(',', $getAddress['geocodes'][0]['location']);
            $updateData['longitude'] = $lagData[0];
            $updateData['latitude']  = $lagData[1];
        }

        $status = UserAddress::where('id', $id)->where('users_id', $g_uid)->update($updateData);

        if($status !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 添加收货地址
     * @param UserAddressRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addUserAddress(UserAddressRequest $request){
        global $g_uid;
        global $companyId;

        $data = [];
        $data['company_id']     = $companyId;
        $data['users_id']       = $g_uid;
        $data['consignee']      = $request->get('consignee');
        $data['contact']        = $request->get('contact');
        $data['province']       = $request->get('province');
        $data['city']           = $request->get('city');
        $data['district']       = $request->get('district');
        $data['address']        = $request->get('address');
        $data['longitude']      = $request->get('longitude');
        $data['latitude']       = $request->get('latitude');
        $data['create_time']    = date('Y-m-d H:i:s', time());

        $districtModel          = new District();
        $address                = $districtModel->getAddress($data['province'], $data['city'], $data['district']);
        $requestAddress         = '';
        foreach ($address as $item) {
            $requestAddress .= $item->name;
        }
        $requestAddress .= $data['address'];
        $url        = 'https://restapi.amap.com/v3/geocode/geo?key=5f07eca1c8780ce236d99d07735607aa&address='.$requestAddress;
        $getAddress = json_decode(curlGet($url), true);
        if ($getAddress['infocode'] == 10000){
            $lagData = explode(',', $getAddress['geocodes'][0]['location']);
            $data['longitude'] = $lagData[0];
            $data['latitude']  = $lagData[1];
        }

        $address = UserAddress::create($data);
        if($address){
            return response()->json(ResponseMessage::getInstance()->success(['address_id' => $address->id])->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 微信小程序绑定手机号
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function wxMiniBindMobile(Request $request){
        global $g_uid;

        $mobile         = $request->get('mobile');
        $validateCode   = $request->get('validate_code');
        $invitationCode = $request->get('invitation_code', '');
        //短信验证码
        $cacheCode = \Cache::get($mobile);

        if(!$cacheCode || $validateCode != $cacheCode){
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        $userBlock = new UserBlock();

        $status = $userBlock->wxMiniBindMobile($g_uid, $mobile, $invitationCode);

        if($status === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else if($status === -1){
            return response()->json(ResponseMessage::getInstance()->failed('MOBILE_BIND')->response());
        }else{
            $user = $userBlock->getUserInfoByMobile($mobile);
            if($user) {
                $oldToken = $request->get('token');
                \Cache::forget($oldToken);

                unset($user->password);
                $token = md5($user->id . date('YmdHis', time()));
                \Cache::put($token, $user, 30);

                $user->token = $token;
                return response()->json(ResponseMessage::getInstance()->success($user)->response());
            }else{
                \Log::error("需要重新登录; 手机号:{$mobile},userId:{$g_uid}");
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }
    }

    /**
     * 领取优惠券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function receiveCoupon(Request $request){
        global $g_uid;

        $couponId = $request->get('coupon_id');

        $userBlock = new UserBlock();
        $status = $userBlock->receiveCoupon($g_uid, $couponId);

        if($status === -1){
            return response()->json(
                ResponseMessage::getInstance()
                    ->setFailedCode('DONE', '优惠券已领完')
                    ->failed()
                    ->response());
        }else if($status === -2){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('DONE', '已领取该优惠券，请使用后再次领取')
                ->response());
        }else if($status === -3){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('DONE', '积分不足')
                ->response());
        }else if($status === -4){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('DONE', '优惠券已领完')
                ->response());
        }else if($status === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 获取我的优惠券列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function myCoupon(Request $request){
        global $g_uid;
        $scope      = $request->get('scope', '');
        $virtual    = $request->get('virtual', 1);
        $userBlock  = new UserBlock();


        $coupon     = $userBlock->myCoupon($g_uid, $scope, $virtual);
        return response()->json(ResponseMessage::getInstance()->success($coupon)->response());
    }

    /**
     * 收藏商品
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectGoods(Request $request){
        global $g_uid;

        $userId    = $g_uid;
        $goodsId   = $request->get('goods_id');

        $userBlock = new UserBlock();
        $collect   = $userBlock->collectGoods($userId, $goodsId);

        if($collect === -1){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('COLLECT_ERROR','该商品已收藏请不要重复收藏')
                ->response()
            );
        }else if ($collect === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 取消商品收藏
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelCollectGoods(Request $request){
        global $g_uid;
        $goodsId = $request->get('goods_id');

        if (is_array($goodsId)){
            $result = CollectGoods::whereIn('users_id', $g_uid)
                ->where('goods_id', $goodsId)->delete();
        }else{
            $result = CollectGoods::where('users_id', $g_uid)
                ->where('goods_id', $goodsId)->delete();
        }
        if($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 取消店铺收藏
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelCollectStore(Request $request){
        global $g_uid;
        $storeId = $request->get('store_id');

        if(
            CollectStores::where('users_id', $g_uid)
                ->where('stores_id', $storeId)->delete() === false
        ){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 收藏店铺
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectStore(Request $request){
        global $g_uid;
        $storeId = $request->get('store_id');

        $data = [];
        $data['users_id']   = $g_uid;
        $data['stores_id']  = $storeId;


        $userBlock = new UserBlock();
        $collect = $userBlock->collectStore($data);

        if($collect === -1){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('COLLECT_ERROR','该店铺已收藏请不要重复收藏')
                ->response()
            );
        }else if ($collect === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 用户收藏
     */
    public function myCollect(Request $request){
        global $g_uid;
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);

        $search = [
            'collect' => 1,
            'user_id' => $g_uid,

        ];
        $type = $request->get('type', '商品');
        $stype = $request->get('stype', null);

        if($stype){
            $search['stype'] = $stype;
        }

        if($type === '商品'){
            $goodsBlock = new GoodsBlock();
            $goods = $goodsBlock->goodsList($search, $page, $pageSize);
            $pagination = $goodsBlock->goodsListPagination($search, $page, $pageSize);

            return response()->json(ResponseMessage::getInstance()->success(['list' => $goods, 'pagination' => $pagination])->response());
        }else{

            $storeBlock = new StoreBlock();
            $store = $storeBlock->storeList($search, $page, $pageSize);

            $storeIds = [];
            foreach ($store as $s){
                $storeIds[] = $s->id;
            }

            $goodsBlock = new GoodsBlock();
            $goods = $goodsBlock->goodsList(['store_id' => $storeIds], 1, 20000);

            foreach ($store as $s){
                $s->recommend_goods = [];
                foreach ($goods as $g){
                    if($g->stores_id === $s->id) {
                        $s->recommend_goods[] = $g;
                    }
                }
            }
            $pagination = $storeBlock->storeListPagination($search, $page, $pageSize);

            return response()->json(ResponseMessage::getInstance()->success(['list' => $store, 'pagination' => $pagination])->response());
        }
    }

    public function ketuotest(){
        $ketuo = new Ketuo;
        $res = $ketuo->getParkingPaymentInfo();

        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }
    /**
     * 会员权益
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function equity(Request $request){
        global $g_uid;
        $user = Users::where('id', $g_uid)->whereNull('delete_time')->first();


        $equity = [
            'user' => [
                'name'              => empty($user) ? '' : $user->name,
                'avatar'            => empty($user) ? '' : $user->avatar,
                'level'             => empty($user) ? '' : $user->level,
                'max_experience'    => 5000,
                'experience'        => empty($user) ? '' : $user->integral,
            ],
            'equity' => [
                '免费停车',
                '生日特惠',
                '免费抽奖',
                '积分兑换',
            ],
            'explain' => '1：会员的有效期是自升级后一年，级别过期后，我们会根据您过去一年的成长值，重新调整您的会员级别。2：会员级别的有效期限能延长吗？当前会员级别就可以顺延一年。3：我是钻石会员，会员级别到期后没达到延长升级条件，会如何降级？会员级别到期后，',
            'CounterService'        => [
                '免费眼镜清洗（宝岛眼镜）',
                '免费皮具护理',
                '免费化妆/修眉',
                '免费皮鞋护理',
                '免费熨烫衣服'
            ],
            'ConvenienceService'    => [
                '手机加油站',
                'VIP休息室',
                '免费拨打电话',
                '免费饮水',
                '贴心寄存',
                '免费干衣服务',
                '免费提供母婴服务',
                '免费提供雨鞋套',
                '免费提供女性用品',
                '免费提供外用药品',
                '免费提供清洁用品',
                '免费提供针线包',
                '免费打乒乓球'
            ]
        ];
        return response()->json(ResponseMessage::getInstance()->success($equity)->response());
    }

    /**
     * 邀请好友
     * @return \Illuminate\Http\JsonResponse
     */
    public function invitation(Request $request){
        global $g_uid;

        $pages = $request->get('pages');
        $userBlock = new UserBlock();
        $user = $userBlock->getUserInfoById($g_uid);

        $savePath = 'qrcode/invitation';
        $codeImg = $userBlock->getQrcode($g_uid,$user->invite_code,$pages,$savePath);

        return response()->json(ResponseMessage::getInstance()->success(['qrcode' => $codeImg])->response());
    }

    /**
     * 修改登录密码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request){
        global $g_uid;

        $data = $request->only([
            'code',
            'mobile',
            'password',
            'confirm_password'
        ]);

        if (\Cache::get($data['mobile'])!==$data['code']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("ERROR",'验证码错误')->failed()->response());
        }

        if ($data['password']!==$data['confirm_password']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("ERROR",'两次密码不一致')->failed()->response());
        }

        $data['password'] = bcrypt($data['password']);

        $res = Users::where('id', $g_uid)->update(['password' => $data['password']]);

        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }

    /**
     * 忘记密码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function modifyPassword(Request $request){
        if (!$request->filled('validate_code')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')
                ->setFailedCode('CODE_ERROR', '请输入验证码')->response());
        }
        if (!$request->filled('mobile')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')
                ->setFailedCode('MOBILE_ERROR', '请输入手机号码')->response());
        }
        if (!$request->filled('password')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')
                ->setFailedCode('PASSWORD_ERROR', '请输入密码')->response());
        }
        $validateCode   = $request->get('validate_code');
        $mobile         = $request->get('mobile');
        $password       = bcrypt($request->get('password'));

        $cacheCode = \Cache::get($mobile);
        if($validateCode != $cacheCode){
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }


        $res = Users::where('mobile', $mobile)->update(['password' => $password]);

        if ($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }



    }

    /**
     * 修改用户信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserInfo(Request $request){
        global $g_uid;

        $data = $request->only([
            'name',
            'avatar'
        ]);
        $validator = Validator::make($data, [
            'name'      => 'required',
            'avatar'    => 'required',

        ], [
            'name.required'      => '关键词id不能为空！！',
            'avatar.required'    => '标签类型不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        $res = Users::where('id', $g_uid)->update($data);

        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }

    /**
     * 修改绑定手机号
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserMobile(Request $request){
        global $g_uid;
        $data = $request->only([
            'new_mobile',
            'code'
        ]);
        if (\Cache::get($data['new_mobile'])!==$data['code']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("ERROR",'验证码错误')->failed()->response());
        }
        $res = Users::where('id', $g_uid)->update(['mobile' => $data['new_mobile']]);

        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }

    /**
     * 用户钱包明细
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userStream(Request $request){
        global $g_uid;

        $page               = $request->get('page', 1);
        $pageSize           = $request->get('pageSize', 10);
        $search['user_id']  = $g_uid;

        $userBlock = new UserBlock();
        $list = $userBlock->userStream($page, $pageSize, $search);
        $pagination = $userBlock->userStreamPagination($page, $pageSize, $search);

        foreach ($list as $value){
            if ($value->type === 1){
                $value->pay_amount = '-'.$value->pay_amount;
                $title = '支付'." ({$value->store_name})";
            }else if ($value->type === 2){
                $value->pay_amount = '+'.$value->pay_amount;
                $title = '充值';
            }else if ($value->type === 3){
                $value->pay_amount = '-'.$value->pay_amount;
                $title = '预约付款'." ({$value->store_name})";
            }else if ($value->type === 4){
                $value->pay_amount = '-'.$value->pay_amount;
                $title = '停车场付款';
            }else if ($value->type === 5){
                $value->pay_amount = '+'.$value->pay_amount;
                $title = '退款';
            }else {
                $value->pay_amount = '+'.$value->pay_amount;
                $title = $value->remake;
            }
            $value->titile      = $title;
            $value->pay_time = date('Y-m-d', strtotime($value->pay_time));
        }

        return response()->json(ResponseMessage::getInstance()->success([
            'list' => $list,
            'pagination' => $pagination
        ])->response());
    }
    /**
     * 用户付款码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userPaycode(Request $request){
        try{
            global $g_uid;
            $path = public_path().'/qrcode/pay/';
            if (!is_dir($path)){
                @mkdir($path, 0755);
            }

            $time = time();
            $data = [
                'redirect_type'     => 'native',
                'path'              => 'Pay',
                'params'            => [
                    'code'          => $time.$g_uid.strlen($g_uid)
                ]
            ];
            QrCode::format('png')
                ->size(500)
                ->encoding('UTF-8')
                ->generate(
                    json_encode($data),
                    $path."{$g_uid}.png"
                );
            Redis::setex('payQrCode:'.$g_uid, 60, $time);

            $url = env('APP_URL')."/qrcode/pay/{$g_uid}.png?t=".mt_rand(1000, 9999);
            return response()->json(ResponseMessage::getInstance()->success(['qrcode' => $url])->response());
        }catch(\Exception $exception){
            \Log::error($exception);
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 客服中心列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userService(){
        $data = Service::orderBy('sort', 'DESC')->get();
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 客服中心详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userServiceInfo($id){
        $data = Service::where('id',$id)->first();
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 意见反馈
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userFeedback(Request $request){
        global $g_uid;

        if (!$request->filled('content')){
            return response()->json(
                ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('CONTENT_FAILED','请输入反馈内容！')
                ->response()
            );
        }

        if ($request->has('images')){

            $images = $request->get('images');
            if (!is_array($images)){
                return response()->json(
                    ResponseMessage::getInstance()
                        ->failed()
                        ->setFailedCode('IMAGE_FORMAT','图片格式错误')
                        ->response()
                );
            }
            if (count($images) > 3){
                return response()->json(
                    ResponseMessage::getInstance()
                        ->failed()
                        ->setFailedCode('IMAGE_COUNT','图片不能超过3张')
                        ->response()
                );
            }

            $data['images'] = implode(',', $images);
        }

        $requestData = ['contact', 'contact_method'];
        foreach ($requestData as $value) {
            if ($request->filled($value)){
                $data[$value] = $request->get($value);
            }
        }

        $data['users_id'] = $g_uid;
        $data['content'] = $request->get('content');
        $userBlock = new UserBlock();
        $res = $userBlock->feedbackAdd($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }


    /**
     * 公益活动报名
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userActivityApply(Request $request){
        global $g_uid;
        $data['users_id'] = $g_uid;
        $data['act_id'] = $request->get('act_id');
        $userBlock = new UserBlock();

        $res = $userBlock->activityApply($data);
        if($res === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ACTIVITY_ERROR', '请不要重复报名')->response());
        }else if ($res === false || $res === 0){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($data)->response());
        }


    }
    /**
     * 报名的公益活动
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userActivityMyApply(Request $request){
        global $g_uid;
        $type = $request->get('type');
        $userBlock = new UserBlock();
        $data = $userBlock->activityMyApply($g_uid,$type);
        if($data){
            return response()->json(ResponseMessage::getInstance()->success($data)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }


    /**
     * 充值订单详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rechargeLog(Request $request){
        global $g_uid;
        $batch   = $request->get('batch');//充值订单号
        $userBlock = new UserBlock();

        $batch = $userBlock->recharge_log($g_uid, $batch);

        if($batch) {
            return response()->json(ResponseMessage::getInstance()->success($batch)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }



    /**
     * 初始化充值订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function initRecharge(Request $request){
        if (!$request->filled(['total_fee', 'type'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $companyId;
        global $g_uid;
        $totalFee   = $request->get('total_fee');//充值金额
        $type       = $request->get('type');
        $remake     = $request->get('remake', null);//充值备注

        $userBlock = new UserBlock();

        $batch = $userBlock->initRecharge($g_uid, $companyId, $totalFee, $remake, $type);

        if($batch == -1){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('ERROR', '请先绑定手机号码')->failed()->response());
        }else if($batch) {
            return response()->json(ResponseMessage::getInstance()->success($batch)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 小程序余额充值微信支付（中信）
     */
    public function rechargePay(Request $request){
        global $g_uid;
        $batch      = $request->get('batch');
        $type       = $request->get('type', 'pub');

        $payBlock = new PayBlock();
        $data = $payBlock->rechargeWechatPayByZhongxin($g_uid, $batch, $type);

        if($data === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }elseif ($data === -1){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('ORDER_NOT_EXSIT', '订单不存在')->response());
        }elseif($data === -2){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('ORDER_PAID', '订单已支付')->response());
        }elseif($data === -3){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($data)->response());
        }
    }

    /**
     * 支付会调（中信）
     * 文档地址: https://open.swiftpass.cn/openapi/wiki?index=67&chapter=7
     * @param Request $request
     */
    public function rechargePayCallback(){
        $pay = $this->__getPayObject();
        $xml = file_get_contents('php://input');
        $pay->resHandler->setContent($xml);
        $pay->resHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
        if($pay->resHandler->isTenpaySign()){
            if(
                $pay->resHandler->getParameter('status') == 0
                &&
                $pay->resHandler->getParameter('result_code') == 0
            ){
                $tradeno = $pay->resHandler->getParameter('out_trade_no');
                $transactionId = $pay->resHandler->getParameter('out_transaction_id');
                $totalFee = $pay->resHandler->getParameter('total_fee');


                $payBlock = new PayBlock();
                $status = $payBlock->rechargeWechatPayByZhongxinCallback($tradeno, $transactionId, $totalFee);

                if($status === true) {
                    ob_clean();
                    echo 'success';
                    exit();
                }else{
                    echo 'failure1';
                    exit();
                }

            }else{
                echo 'failure1';
                exit();
            }
        }else{
            echo 'failure2';
        }
    }

    /**
     * 支付（中信）
     */
    private function __getPayObject(){
        $payObject = new \stdClass();

        $payObject->resHandler = new ClientResponseHandler();
        $payObject->reqHandler = new RequestHandler();
        $payObject->pay = new PayHttpClient();

        $payObject->reqHandler->setGateUrl(\Config::get('zhongxin-pay.mini.url'));

        $sign_type = \Config::get('zhongxin-pay.mini.sign_type');

        if ($sign_type == 'MD5') {
            $payObject->reqHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
            $payObject->resHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
            $payObject->reqHandler->setSignType($sign_type);
        } else if ($sign_type == 'RSA_1_1' || $sign_type == 'RSA_1_256') {
            $payObject->reqHandler->setRSAKey(file_get_contents(\Config::get('zhongxin-pay.mini.private_rsa_key')));
            $payObject->resHandler->setRSAKey(file_get_contents(\Config::get('zhongxin-pay.mini.public_rsa_key')));
            $payObject->reqHandler->setSignType($sign_type);
        }

        return $payObject;
    }


    /**
     * 添加用户浏览记录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewRecordingAdd(Request $request){
        global $g_uid;
        if (!$request->filled(['type_id', 'type'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $type     = $request->get('type');
        $typeId   = $request->get('type_id');

        $result = UserViewRecording::insert([
            'users_id'  => $g_uid,
            'type_id'   => $typeId,
            'type'      => $type
        ]);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 用户浏览记录列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewRecordingList(Request $request){

        if (!$request->filled('type')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $tpe        = $request->get('type');
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 10);

        $userBlock      = new UserBlock();
        $data           = $userBlock->viewRecordingList($page, $pageSize, $tpe, $g_uid);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


    /**
     * 历史购买记录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function historyBuy(Request $request){
        global $g_uid;
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 1);
        $search     = [
            'users_id'  =>$g_uid
        ];

        $orderBlock     = new OrderBlock();
        $list           = $orderBlock->orderList($search, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($list)->response());


    }

    /**
     * 关键词搜索
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function keywordSearch(Request $request){
        if (!$request->has(['keyword', 'position'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $token = $request->get('token', '');
        if (!empty($token)){
            if($token == 'test123'){
                $g_uid = 9;
            }else{
                $userInfo = \Cache::get($token);
                $g_uid = $userInfo->id;
            }
        }

        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        $type       = $request->get('type', '');
        $keyword    = $request->get('keyword');
        $position   = $request->get('position');
        $minPrice   = $request->get('minPrice');
        $maxPrice   = $request->get('maxPrice');
        $storeId    = $request->get('stores_id');




        $fieldData = [
            'id' => 'id',
            '销量' => 'sales_volume',
            '价格' => 'selling_price'
        ];
        $search     = [];
        $search['field']        = $fieldData[$request->get('field', 'id')];
        $search['sequence']     = $request->get('sequence', 'desc');


        if (empty($minPrice)){
            $minPrice = 0;
            $search['price'][0] = 0;
        }else{
            $search['price'][0] = $minPrice;
        }

        if (empty($maxPrice)){
            $search['selling_price'] = $minPrice;
            unset($search['price']);
        }else{
            $search['price'][1] = $maxPrice;
        }



        $goodsBlock     = new GoodsBlock();
        $storeBlock     = new StoreBlock();
        $keywordModel   = new UserKeyword();

        $list       = '';
        $pagination = '';

        if ($position == '商城首页'){

            if ($type == 'goods'){

                $search['name']             = $keyword;
                $search['store_type']       = 2;
                $search['store_id']         = $storeId;

                $list           = $goodsBlock->goodsList($search,$page, $pageSize);
                $pagination     = $goodsBlock->goodsListPagination($search, $page, $pageSize);
            }else if ($type == 'store'){
                $search['name']             = $keyword;
                $search['store_type']       = 2;
                $search['operate']          = 1;

                $list           = $storeBlock->storeList($search, $page, $pageSize);
                $pagination     = $storeBlock->storeListPagination($search, $page, $pageSize);
            }

        }else if ($position == '首页'){

            if ($type == 'goods'){
                $search['name']             = $keyword;
                $list           = $goodsBlock->goodsList($search,$page, $pageSize);
                $pagination     = $goodsBlock->goodsListPagination($search, $page, $pageSize);
            }else if ($type == 'store'){
                $search['name']             = $keyword;
                $search['operate']          = 1;
                $search['store_type']       = 4;
                $list           = $storeBlock->storeList($search, $page, $pageSize);
                $pagination     = $storeBlock->storeListPagination($search, $page, $pageSize);
            }

        }else if ($position == '外卖首页'){
            $search['name']             = $keyword;
            $search['store_type']       = 1;
            $search['operate']          = 1;
//            $search['goods']            = $keyword;

//            dd($search);
            $type           = 'store';
            $list           = $storeBlock->storeList($search, $page, $pageSize);
            $pagination     = $storeBlock->storeListPagination($search, $page, $pageSize);

        }


        if (!empty($g_uid) &&  !empty($keyword)){
            $keywordModel->saveKeyword($g_uid, $keyword, $type);
        }


        return response()->json(ResponseMessage::getInstance()->success([
            'list'          => $list,
            'pagination'    => $pagination
        ])->response());


    }

    /**
     * 清除历史搜索记录
     * @return \Illuminate\Http\JsonResponse
     */
    public function emptyKeyword(){
        global $g_uid;
        $result = UserKeyword::where('users_id', $g_uid)->delete();

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 历史搜索关键字、热搜关键字
     * @return \Illuminate\Http\JsonResponse
     */
    public function keywordList(Request $request){
        $token = $request->get('token', '');
        $userId = '';
        if (!empty($token)){
            if($token == 'test123'){
                $userId = 9;
            }else{
                $userInfo = \Cache::get($token);
                $userId = $userInfo->id;
            }
        }

        $type           = $request->get('type', '');
        $keywordModel   = new UserKeyword();
        $hot            = $keywordModel->keywordHot($type);
        $historyData    = [];
        if (!empty($userId)){
            $history    = $keywordModel->keywordList($userId, $type);
            foreach ($history as $item) {
                $historyData[]  = $item->keyword;
            }
        }

        $hotData        = [];
        foreach ($hot as $item) {
            $hotData[]  = $item->keyword;
        }

        return response()->json(ResponseMessage::getInstance()->success([
            'hot'       => $hotData,
            'history'   => $historyData
        ])->response());
    }


    /**
     * 设置支付密码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function settingPayPassword(Request $request){
        if (!$request->filled(['code', 'password', 'mobile'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $code           = $request->get('code');
        $password       = (int)$request->get('password');
        $mobile         = $request->get('mobile');

        if (strlen($password) != 6){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('PASSWORD_LENGTH_ERROR', '密码长度为6位数字')
                ->failed()->response());
        }


        if ($mobile == '18684916858'){
            $cacheCode      = '123456';
        }else{
            $cacheCode      = \Cache::get($mobile);
        }
        if($code != $cacheCode){
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        $password       = md5($password);
        $userBlock      = new UserBlock();
        $result         = $userBlock->settingPayPassword($password, $g_uid);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 检测是否设置支付密码
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkPayPassword(){
        global $g_uid;
        $userBlock      = new UserBlock();
        $password       = $userBlock->checkPayPassword($g_uid);

        if (empty($password)){
            return response()->json(ResponseMessage::getInstance()->success(0)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success(1)->response());
        }

    }

    /**
     * APP支付宝用户充值
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function aliPayRecharge(Request $request){
        if (!$request->filled('batch')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $payBlock   = new PayBlock();
        $batch      = $request->get('batch');
        $type       = $request->get('type', 'app');

        $result     = $payBlock->aliPayRecharge($batch, $type);

        if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($result->getContent())->response());
        }
    }

    /**
     * 支付宝用户充值回调
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Yansongda\Pay\Exceptions\InvalidConfigException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function aliPayRechargeNotify(){

        $aliPay         = Pay::alipay(config('ali-pay.recharge'));
        $notifyData     = $aliPay->verify();
        $payBlock       = new PayBlock();

        $result         = $payBlock->aliPayRechargeNotify($notifyData);
        if ($result !== false){
            return $aliPay->success();
        }

    }

    /**
     * 余额充值APP微信支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function weChatPayRecharge(Request $request){
        if (!$request->filled('batch')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }


        $batch      = $request->get('batch');
        $payBlock   = new PayBlock();
        $result     = $payBlock->weChatPayRecharge($batch);
        if ($result){
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * app微信支付余额充值回调
     * @return mixed
     */
    public function weChatPayRechargeNotify(){
        $app        = Factory::payment(\Config::get('zhongxin-pay.app'));
        $payBlock   = new PayBlock();
        $result = $payBlock->weChatPayRechargeNotify($app);
        \Log::debug($result);
        return $result;
    }

    /**
     * 兑换积分商品
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function exchangeGoods(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请选择需要兑换的商品')->response());
        }
        global $g_uid;
        $id             = $request->get('id');
        $goodsBlock     = new GoodsBlock();
        $result         = $goodsBlock->exchangeGoods($id, $g_uid);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('GOODS_ERROR', '商品已下架')->response());
        }else if($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('INTEGRAL_ERROR', '积分不足')->response());
        }else if($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('GOODS_ERROR', '该商品暂无库存')->response());
        }else if($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

    }

    /**
     * 获取用户红包
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserRedEnvelope(Request $request){
        global $g_uid;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 20);
        $userRedEnvelopeModel       = new UserRedEnvelope();
        $data   = $userRedEnvelopeModel->getUserRedEnvelope($page, $pageSize, $g_uid);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());

    }

    /**
     * 获取用户推荐二维码
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserRecommendQrCode(){
        global $g_uid;

        $path = public_path().'/qrcode/user/';
        if (!is_dir($path)){
            @mkdir($path, 0755);
        }

        $backGroundPath = public_path().'/qrcode/background/';
        if (!is_dir($backGroundPath)){
            @mkdir($backGroundPath, 0755);
        }

        $imageName      = $g_uid.'.png';

        if (file_exists($backGroundPath.$imageName) && file_exists($path.$imageName)){
            $returnBackGround   = env('APP_URL').'/qrcode/background/'.$imageName;
            $returnUser         = env('APP_URL').'/qrcode/user/'.$imageName;
            return response()->json(ResponseMessage::getInstance()->success([
                'background'        => $returnBackGround,
                'user'              => $returnUser
            ])->response());
        }

        if (file_exists($path.$imageName)){
            $userCode = $path.$imageName;
        }else{
            $userCodeResult = QrCode::format('png')
                ->size(400)
                ->encoding('UTF-8')
                ->generate(
                    env('APP_URL')."/pluto/user?id={$g_uid}",
                    "{$path}/{$imageName}"
                );

            if ($userCodeResult){
                $userCode = $path.$imageName;
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }


        $userCode = imagecreatefrompng($userCode);

        $backGround = public_path().'/images/background/user.png';
        $backGround = imagecreatefrompng($backGround);

        $image = imageCreatetruecolor(imagesx($backGround),imagesy($backGround));
        $color = imagecolorallocate($image, 255,255,254);
        imagefill($image, 0, 0, $color);
        imageColorTransparent($image, $color);
        imagecopyresampled($image,$backGround,0,0,0,0,imagesx($backGround),imagesy($backGround),imagesx($backGround),imagesy($backGround));
        imagecopymerge($image,$userCode, 180,540,0,0,imagesx($userCode),imagesy($userCode), 100);
        $result = imagepng($image, $backGroundPath.$imageName);

        if ($result){
            $returnUser         = env('APP_URL').'/qrcode/user/'.$imageName;
            $returnBackGround   = env('APP_URL').'/qrcode/background/'.$imageName;
            return response()->json(ResponseMessage::getInstance()->success([
                'background'        => $returnBackGround,
                'user'              => $returnUser
            ])->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }

    /**
     * 获取用户消息
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserMessage(){
        global $g_uid;
        $userMessageModel       = new UserMessage();
        $data = $userMessageModel->getUserMessage($g_uid);
        foreach ($data as $item) {
            $item->avatar = env('APP_URL').'/images/kefu.png';
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 发送邮件
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEmail(Request $request){
        $validator = Validator::make($request->all(), [
            'email'     => 'required|email',
        ],[
            'email.required'    => '请输入邮箱地址',
            'email.email'       => '请输入正确的邮箱地址！'
        ]);


        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        global $g_uid;
        $cacheNumber = \Cache::get('email'.$g_uid)['number'];

        $email  = $validator->validate()['email'];
        $number = $cacheNumber ? $cacheNumber : getSalt(6, 1);
        Mail::send('emails', ['number' => $number], function ($message) use ($email){
            $message->to($email)->subject('邮箱验证');
        });

        $result = Mail::failures();

        if ($result == []){
            if (!$cacheNumber){
                $data = [
                    'email'     => $email,
                    'userId'    => $g_uid,
                    'number'    => $number
                ];
                \Cache::put('email'.$g_uid, $data, 300);
            }

            return response()->json(ResponseMessage::getInstance()->success()->setFailedCode('', '发送成功')->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

    /**
     * 绑定邮件
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bindEmail(Request $request){

        $validator = Validator::make($request->all(), [
            'number'     => 'required|numeric',
        ],[
            'number.required'     => '请输入验证码',
            'number.numeric'      => '请输入数字'
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('PARAM_ERROR',$validator->errors()->first())
                ->failed()->response());
        }

        $number  = $validator->validate()['number'];
        if (mb_strlen($number) != 6){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('PARAM_ERROR', '请输入6位数字')->failed()->response());
        }

        global $g_uid;
        $data = \Cache::get('email'.$g_uid);
        if (empty($data) || $g_uid != $data['userId'] || $number != $data['number']){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '验证码错误或已过期')->response());
        }

        $userBlock      = new UserBlock();
        if($userBlock->getUserInfoByEmail($data['email'])){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该邮箱已被绑定')->response());
        }

        $result         = $userBlock->bindingEmail($data);
        if ($result){
            \Cache::forget('email'.$g_uid);
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }


    /**
     * 签到
     * @return \Illuminate\Http\JsonResponse
     */
    public function userSignIn(){
        global $g_uid;
        $userSignInBlock        = new UserSignInBlock();
        $result = $userSignInBlock->userSignIn($g_uid);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '您今天已签到，请明天再来')->response());
        }else if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }
    }

    /**
     * 获取签到时间
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMySignTime(){
        global $g_uid;
        $userSignInBlock        = new UserSignInBlock();
        $signLogTime            = $userSignInBlock->getMySignTime($g_uid);
        $SignMonthTime          = $userSignInBlock->getMonthSign($g_uid, date('Y-m'));
//        dd($SignMonthTime);
        $timeData = [];
        foreach ($signLogTime as $item) {
            $timeData[] = date('d', strtotime($item->time));
        }
        $SevenDays              = $SignMonthTime ? 7 - $SignMonthTime->continuous_total : 7;
        $FifteenDays            = $SignMonthTime ? 15 - $SignMonthTime->continuous_total: 15;
        return response()->json(ResponseMessage::getInstance()->success([
            'time'              => $timeData,
            'total'             => $SignMonthTime ? $SignMonthTime->total : 0,
            'continuous_total'  => $SignMonthTime ? $SignMonthTime->continuous_total : 0,
            'continuous'        => [
                date('d', strtotime($SevenDays." day")),
                date('d', strtotime($FifteenDays." day")),
                date('t', time()),
            ],
            'rule'              => '连续7天签到的会员，赠送10个积分。连续15天签到的会员，赠送20个积分。连续一个月签到的会员，赠送30个积分'
        ])->response());
    }

    /**
     * 用户填写邀请码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function inviteUser(Request $request){
        if (!$request->filled('invite_code')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请输入邀请码')->response());
        }
        global $g_uid;
        $inviteCode     = $request->get('invite_code');
        $userBlock      = new UserBlock();
        $result         = $userBlock->inviteUser($g_uid, $inviteCode);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '邀请码无效')->response());
        }else if ($result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 用户升级
     * @return \Illuminate\Http\JsonResponse
     */
    public function userUpgrade(){
        global $g_uid;
        $userBlock      = new UserBlock();
        $result         = $userBlock->userUpgrade($g_uid);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取我的线下优惠券列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function myOfflineCoupon(Request $request){
        global $g_uid;
        $page           = $request->get('page',1);
        $pageSize       = $request->get('pageSize', 10);
        $userBlock      = new UserBlock();
        $data           = $userBlock->myOfflineCoupon($page, $pageSize, $g_uid);

        return response()->jsoN(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取用户余额付款码
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserPayQrCode(){
        try{
            global $g_uid;
            $path = public_path().'/qrcode/pay/';
            if (!is_dir($path)){
                @mkdir($path, 0755);
            }

            $time = time();
            $data = [
                'redirect_type'     => 'native',
                'path'              => 'Pay',
                'params'            => [
                    'code'          => $time.$g_uid.strlen($g_uid)
                ]
            ];
            QrCode::format('png')
                ->size(500)
                ->encoding('UTF-8')
                ->generate(
                    json_encode($data),
                    $path."{$g_uid}.png"
                );
            Redis::setex('payQrCode:'.$g_uid, 60, $time);

            $url = env('APP_URL')."/qrcode/pay/{$g_uid}.png";
            return response()->json(ResponseMessage::getInstance()->success($url)->response());
        }catch(\Exception $exception){
            \Log::error($exception);
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

    /**
     * 获取用户注册礼包
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOfflineCoupon(){
        global $g_uid;
        $userBlock      = new UserBlock();
        $data           = $userBlock->getOfflineCoupon($g_uid);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 抽奖
     * @return \Illuminate\Http\JsonResponse
     */
    public function userLottery(){
        global $g_uid;
        $userBlock      = new UserBlock();
        $result         = $userBlock->userLottery($g_uid);
        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '抽奖次数已用完')->response());
        }else if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }
    }

    /**
     * 用户余额兑换
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function exchangeBalanceCoupon(Request $request){
        if (!$request->filled('coupon_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $couponId       = $request->get('coupon_id');
        $userBlock      = new UserBlock();
        $result         = $userBlock->exchangeBalanceCoupon($g_uid, $couponId);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '优惠券已下架')->response());
        }else if ($result === -2) {
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '库存不足')->response());
        }else if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 扫码领券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function receiveScanCodeCoupon(Request $request){
        if (!$request->filled('code')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }


        global $g_uid;
        $code       = $request->get('code');
        $userBlock  = new UserBlock();
        $result     = $userBlock->receiveScanCodeCoupon($code, $g_uid);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '优惠券已下架')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '库存不足')->response());
        }else if ($result === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '已领取过该优惠券，不能重复领取')->response());
        }else if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 分享增加抽奖次数
     * @return \Illuminate\Http\JsonResponse
     */
    public function shareLottery(){
        global $g_uid;
        $userBlock      = new UserBlock();
        $result         = $userBlock->shareLottery($g_uid);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '分享增加抽奖次数已上限')->response());
        }elseif($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取优惠券想起详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function couponInfo(Request $request){
        if(!$request->filled('coupon_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $couponId           = $request->get('coupon_id');
        $userCouponModel    = new UserCoupons();
        $data               = $userCouponModel->getUserCouponInfo($couponId, $g_uid);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    public function getUserInfoById(Request $request){
        if(!$request->filled('users_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $g_uid;
        $userId     = $request->get('users_id');
        $userBlock  = new UserBlock();
        $data       = $userBlock->getUserInfoByIdAttention($g_uid, $userId);


        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }
}
