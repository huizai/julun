<?php

namespace App\Http\Controllers\Pluto;

use App\Blocks\GoodsBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\Goods;
use App\Models\GoodsBargain;
use App\Models\GoodsClassifies;
use App\Models\GoodsGroupBuy;
use App\Models\GroupBuy;
use App\Models\Spike;
use App\Blocks\GoodsEstimatesBlock;
use App\Models\Stores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\StoreActivity;
use App\Models\UserViewRecording;
use App\Blocks\GoodsIntegralBlock;

class GoodsController extends Controller{

    /**
     * 获取商品分类
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function goodsClassify(Request $request){
        global $companyId;
        $pid = $request->get('pid', 0);
        $type = $request->get('type');

        $classify = GoodsClassifies::where('parent_id', $pid)
            ->where('company_id', $companyId)
            ->where('type', $type)
            ->whereNull('delete_time')
            ->orderBy('sort', 'desc')
            ->get();
        return response()->json(ResponseMessage::getInstance()->success($classify)->response());
    }


    /**
     * 获取商品分类商品
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function classifyGoods(Request $request){
        global $companyId;
        $pid = $request->get('pid', 0);
        $type = $request->get('type');

        $classify = GoodsClassifies::where('parent_id', $pid)
            ->where('company_id', $companyId)
            ->where('type', $type)
            ->whereNull('delete_time')
            ->get();
        $goodsBlock = new GoodsBlock();
        $search = [];
        $list = $goodsBlock->classifyGoods($search, $page = 1, $pageSize = 3, $classify);
        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }

    /**
     * 获取商品列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function goodsList(Request $request){
        global $companyId;
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search         = ['company' => $companyId, 'status' => 2];
        $searchField    = [
            'name',         //按名称搜索商品
            'cid',          //按分类搜索商品
            'store_id',     //按店铺搜索商品
            'nav_id',       //按导航搜索
            'splike',
            'splike_day',
            'splike_hour',
            'group_buy',
            'guess_like',
            'agent_id',     //按代理搜索商品
            'type',
            'cuisine',
            'long',
            'lat'
        ];
        $fieldData = [
            'id' => 'id',
            '销量' => 'sales_volume',
            '价格' => 'selling_price'
        ];
        $search['operate']      = 1;//是否营业
        $search['field']        = $fieldData[$request->get('field', 'id')];
        $search['sequence']     = $request->get('sequence', 'desc');
        $search['bargain']      = $request->get('bargain', 0);
        $search['group_buy']      = $request->get('group_buy', 0);
        $storeType           = $request->get('store_type', '商城');
        if($storeType == '餐饮外卖'){
            $search['store_type'] = [1];
        }elseif($storeType === '商城'){
            $search['store_type'] = [2];
        }elseif($storeType === '聚南珍'){
            $search['store_type'] = [4];
        }



        $desc  = explode(',', $request->get('sort_desc'));
        $asc   = explode(',', $request->get('sort_asc'));

        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $icon = '';
        //秒杀
        if(isset($search['splike']) && $search['splike'] == 1) {
            if (isset($search['splike_day']) && isset($search['splike_hour'])) {
                $search['goodids'] = [];
                $spikeList = Spike::where('day', $search['splike_day'])
                    ->where('hour', $search['splike_hour'])
                    ->whereNull('delete_time')
                    ->get();
                foreach ($spikeList as $k => $v) {
                    $search['goodids'][] = $v->goods_id;
                }
            }
            $icon = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/goodsIcon/156082897873.png';
        }

        //砍价
        if ($search['bargain'] == 1){
            $search['goodids'] = [];
            $goodsBargainModel   = new GoodsBargain();
            $bargainList         = $goodsBargainModel->getBargainList($page, $pageSize);
            foreach ($bargainList as $key => $value) {
                $search['goodids'][] = $value->goods_id;
            }
        }

        //拼团
        if ($search['group_buy'] == 1){
            $search['goodids'] = [];
            $goodsGroupBuyModel  = new GoodsGroupBuy();
            $groupBuyList         = $goodsGroupBuyModel->getGroupBuyList($page, $pageSize);
            foreach ($groupBuyList as $k => $v) {
                $search['goodids'][] = $v->goods_id;
            }
        }

        $goodsModel             = new Goods();
        $storeModel             = new Stores();
        if(isset($search['guess_like'])){
            $search['goodids'] = [];
            $goodList = DB::table($goodsModel->getTable() . ' as g')
                ->select(
                    'g.id',
                    's.store_type_id'
                )
                ->leftJoin($storeModel->getTable() . ' as s', 's.id', '=', 'g.stores_id')
                ->whereNull('g.delete_time')
                ->where('s.store_type_id', $search['store_type'])
                ->where('g.status', 2)
                ->where('g.stores_id', '!=', 0)
                ->where('g.company_id', $companyId)
                ->get();

            $goodList = json_decode(json_encode($goodList), true);
            $goodids = array_rand($goodList,10);
            $search['goodids'] = [$goodList[$goodids[0]]['id'], $goodList[$goodids[1]]['id'], $goodList[$goodids[2]]['id']];
            unset($search['store_type']);
        }

        //优惠、满减
        if (isset($search['type'])){
            $activity = StoreActivity::where('type', $search['type'])->whereNull('delete_time')->get();
            $search['store_id'] = [];
            foreach ($activity as $k => $v) {
                $search['store_id'][] = $v->stores_id;
            }

            $search['store_id'] = array_unique($search['store_id']);
            $icon = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/icon/156688998425.png';
        }

        $goodsBlock = new GoodsBlock();


        $goods = $goodsBlock->goodsList($search, $page, $pageSize, $asc, $desc);
        $pagination = $goodsBlock->goodsListPagination($search, $page, $pageSize);

        foreach ($goods as $value) {
            if (!empty($icon)){
                $value->icon = $icon;
            }

            if(isset($search['splike']) && $search['splike'] == 1 && !empty($search['goodids'])) {
                foreach ($spikeList as $item) {
                    if ($value->id == $item->goods_id){
                        $value->selling_price = priceIntToFloat($item->selling_price);
                    }

                }
            }

        }

        return response()->json(ResponseMessage::getInstance()->success(['list' => $goods, 'pagination' => $pagination])->response());
    }

    /**
     * 获取商品详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function goodsInfo(Request $request){
        $id     = $request->get('id');
        $token = $request->get('token');
        if($token == 'test123'){
            $userId = 9;
        }else {
            $userInfo = \Cache::get($token);
            if(isset($userInfo->id)){
                $userId = $userInfo->id;
            }else{
                $userId = null;
            }


        }

        if (isset($userId)){
            UserViewRecording::insert([
                'users_id'  => $userId,
                'type_id'   => $id,
                'type'      => 1
            ]);
        }


        $goodsBlock = new GoodsBlock();
        $goods = $goodsBlock->goodsInfo($id, $userId);
        return response()->json(ResponseMessage::getInstance()->success($goods)->response());
    }

    public function getGroupBuyGoodsInfo(Request $request){
        if (!$request->filled('goods_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $goodsId    = $request->get('goods_id');
        $goodsBlock = new GoodsBlock();

    }

    /**
     * 秒杀时间
     */
    public function splikeConfig(){
        $day = date('Y-m-d', time());
        $hour = date('H', time());
        $time = time();
        $splike = [];

        if($hour % 2 != 0){
            $hour--;
        }

        while($hour<24){
            if($hour % 2 == 0){
                if($hour < 10){
                    $hour = '0'. $hour;
                }
                $timeStamp  = strtotime($day.$hour.':00');
                $splikeTime = 7200;
                if (($timeStamp+$splikeTime) < $time){//已结束
                    $status     = "已结束";
                    $countDown  = 0;
                }elseif (($timeStamp+$splikeTime) > $time && $timeStamp < $time){//抢购中
                    $status     = "抢购中";
                    $countDown  = ($timeStamp + $splikeTime) - $time;
                }elseif ($timeStamp > $time){//即将开始
                    $status     = "即将开始";
                    $countDown  = $timeStamp - $time;
                }
                $splike[] = [
                    'day'           => $day,
                    'hour'          => $hour.':00',
                    'count_down'    => $countDown,
                    'status'        => $status

                ];

            }
            $hour ++;
            if(count($splike) == 5){
                break;
            }
        }

        if(count($splike) < 5){
            $day = date('Y-m-d', strtotime('+1 day'));
            $hour = 0;

            while($hour<24){
                if($hour % 2 == 0){
                    if($hour < 10){
                        $hour = '0'. $hour;
                    }
                    $timeStamp  = strtotime($day.$hour.':00');
                    $splikeTime = 7200;
                    if (($timeStamp+$splikeTime) < $time){//已结束
                        $status     = "已结束";
                        $countDown  = 0;
                    }elseif (($timeStamp+$splikeTime) > $time && $timeStamp < $time){//抢购中
                        $status     = "抢购中";
                        $countDown  = ($timeStamp + $splikeTime) - $time;
                    }elseif ($timeStamp > $time){//即将开始
                        $status     = "即将开始";
                        $countDown  = $timeStamp - $time;
                    }
                    $splike[] = [
                        'day'           => $day,
                        'hour'          => $hour.':00',
                        'count_down'    => $countDown,
                        'status'        => $status

                    ];
                }
                $hour ++;
                if(count($splike) == 5){
                    break;
                }
            }
        }

        return response()->json(ResponseMessage::getInstance()->success($splike)->response());
    }

    /**
     * 添加商品评论
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userEstimatesGoods(Request $request){
        if (!$request->filled(['comment'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $comment        = $request->post('comment');
        if (isset($comment['image'])){
            if (count($comment['image']) > 8){
                return response()->json(ResponseMessage::getInstance()->failed()
                    ->setFailedCode('IMAGE_ERROR', '图片不能超过8张')
                    ->response());
            }
        }


        $commentData  = [];
        $imageData    = [];
        $orderGoodsId = [];
        if (!is_array($comment)){
            $comment = json_decode($comment, true);
        }

        foreach ($comment as $key => $item) {

            if ($item['score'] < 1){
                return response()->json(ResponseMessage::getInstance()->failed()
                    ->setFailedCode('PARAM_ERROR','评分不能小于1分')
                    ->response());
            }

            $orderGoodsId[]                         = $item['order_goods_id'];
            $commentData[$key]['order_goods_id']    = $item['order_goods_id'];
            $commentData[$key]['goods_id']          = $item['goods_id'];
            $commentData[$key]['estimate']          = $item['estimate'];
            $commentData[$key]['score']             = $item['score'];
            $commentData[$key]['stores_id']         = isset($item['stores_id']) ? $item['stores_id'] : null;
            $commentData[$key]['users_id']          = $g_uid;
            $storeId                                = isset($item['stores_id']) ? $item['stores_id'] : null;

            if (isset($item['image'])){
                $imageData[$key]['order_goods_id']      = $item['order_goods_id'];
                $imageData[$key]['image']               = $item['image'];
            }


        }


        $goodsEstimatesBlock = new GoodsEstimatesBlock();
        $result = $goodsEstimatesBlock->userEstimatesGoods($commentData, $imageData, $orderGoodsId, $storeId);


        if ($result === true){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else if($result == -1) {
            return response()->json(
                ResponseMessage::getInstance()
                    ->failed()
                    ->setFailedCode('ORDER_COMMENT', '请确认收货')
                    ->response()
            );
        }else if ($result == -2){
            return response()->json(
                ResponseMessage::getInstance()
                    ->failed()
                    ->setFailedCode('ORDER_COMMENT', '请不要重复评价')
                    ->response()
            );
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

    /**
     * 推荐商品
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recommendGoods(Request $request){
        global $g_uid;
        $position       = $request->get('position', '商城');
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $storeTypeId    = $position == '商城' ? 2 : 1;

        $goodsBlock     = new GoodsBlock();
        $data           = $goodsBlock->recommendGoods($g_uid, $storeTypeId, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }
}
