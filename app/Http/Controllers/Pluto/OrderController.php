<?php

namespace App\Http\Controllers\Pluto;

use App\Blocks\OrderBlock;
use App\Blocks\PayBlock;
use App\Blocks\UserBlock;
use App\Http\Controllers\Controller;
use App\Http\Requests\CartRequest;
use App\Libs\ResponseMessage;
use App\Libs\ZhongxinPay\ClientResponseHandler;
use App\Libs\ZhongxinPay\PayHttpClient;
use App\Libs\ZhongxinPay\RequestHandler;
use App\Models\Carts;
use App\Models\Orders;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Redis;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yansongda\Pay\Pay;
use App\Libs\ExpressDelivery as ED;

class OrderController extends Controller{

    private $resHandler = null;
    private $reqHandler = null;
    private $pay = null;



    /**
     * 立即购买提交
     * @param CartRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function buyNow(Request $request){
        global $g_uid;
        global $companyId;
        $requestData = ['goods_id', 'quantity'];
        foreach ($requestData as $value) {
            if (!$request->filled($value)){
                return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
            }
        }

        $goodsId    = $request->get('goods_id');
        $quantity   = $request->get('quantity');
        $skuIds     = explode(',',$request->get('sku'));
        $couponId   = $request->get('coupon_id', '');

        $orderBlock = new OrderBlock();
        $payGoodsAmount = $orderBlock->countGoodgs($goodsId, $quantity, $skuIds, $g_uid, $couponId);

        if($payGoodsAmount === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else if($payGoodsAmount === -1){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else if($payGoodsAmount === -2){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('GOODS_DELETED', '商品不存在')
                ->response());
        }else if($payGoodsAmount === -3){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('COUPON_ERROR', '请领取优惠券')
                ->response());
        }else if($payGoodsAmount === -4){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('COUPON_ERROR', '优惠券已过期')
                ->response());
        }else if($payGoodsAmount === -5){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('COUPON_ERROR', '不满足优惠条件')
                ->response());
        }else if($payGoodsAmount === -6){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('COUPON_ERROR', '该优惠券只能再首单使用')
                ->response());
        }else{

            foreach ($payGoodsAmount->goods as $value) {
                $value->selling_price = priceIntToFloat($value->selling_price);
                $value->purchase_price = priceIntToFloat($value->purchase_price);
                $value->original_price = priceIntToFloat($value->original_price);
            }
            $payGoodsAmount->full_amount = priceIntToFloat($payGoodsAmount->full_amount);
            $payGoodsAmount->pay_amount = priceIntToFloat($payGoodsAmount->pay_amount);
            $payGoodsAmount->useFRDiscountAmount = priceIntToFloat($payGoodsAmount->useFRDiscountAmount);
            $payGoodsAmount->discount_amount = priceIntToFloat($payGoodsAmount->discount_amount);
            if ($payGoodsAmount->activity){
                $payGoodsAmount->activity->data->full_amount = priceIntToFloat($payGoodsAmount->activity->data->full_amount);
                $payGoodsAmount->activity->data->discount_amount = priceIntToFloat($payGoodsAmount->activity->data->discount_amount);
            }

            return response()->json(ResponseMessage::getInstance()->success($payGoodsAmount)->response());
        }
    }

    /**
     * 立即购买初始化订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function initOrderBuyNow(Request $request){
        global $companyId;
        global $g_uid;
        $cart = $request->get('cart');

        $orderType              = $request->get('order_type', 0);
        $remark                 = $request->get('remark','');
        $address_id             = $request->get('address_id',0);
        $foodDeliveryTime       = $request->get('food_delivery_time');
        $deliveryTime           = $request->get('delivery_time');

        $orderBlock = new OrderBlock();
        $cart = json_decode($cart,true);
        $batch = $orderBlock->initOrderBuyNow($g_uid,$companyId,$cart,$orderType,$remark,$address_id,$foodDeliveryTime,$deliveryTime);

        if ($batch === -1){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('SKU_ERROR', '商品规格不存在')
                ->response());
        }else if($batch == -2){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('SKU_ERROR', '请选择商品规格')
                ->response());
        }else if($batch) {
            return response()->json(ResponseMessage::getInstance()->success($batch)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 添加或者更新购物车
     * @param CartRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addOrUpdateCart(CartRequest $request){
        global $g_uid;
        $goodsId    = $request->get('goods_id');
        $seatId     = $request->get('seat_id');
        $quantity   = $request->get('quantity');
        $skuIds     = explode(',',$request->get('sku'));
        $type       = $request->get('type', 1);

        $orderBlock = new OrderBlock();
        $data       = $orderBlock->cart($g_uid, $goodsId, $quantity, $skuIds, $seatId, $type);

        if($data === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else if($data === -1){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('SKU_ERROR', '请选择正确的规格')->response());
        }else if($data === -2){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('GOODS_DELETED', '商品不存在')->response());
        }else if($data === -3){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('SKU_ERROR', '请选择规格')->response());
        }else{
            $data->full_amount      = priceIntToFloat($data->full_amount);
            $data->pay_amount       = priceIntToFloat($data->pay_amount);
            $data->discount_amount  = priceIntToFloat($data->discount_amount);
            return response()->json(ResponseMessage::getInstance()->success($data)->response());
        }
    }

    /**
     * 删除购物车
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCart(Request $request){
        global $g_uid;
        $cartId = explode(',', $request->get('cart_id'));

        if(
            Carts::where('users_id', $g_uid)
            ->whereIn('id', $cartId)
            ->delete() === false
        ){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 购物车计算价格
     */
    public function choiceCart(Request $request){
        global $companyId;
        global $g_uid;

        $cartIds = explode(',', $request->get('cart_ids'));

        $search = [
            'cart_id'   => $cartIds,
            'users_id' => $g_uid,
            'company_id' => $companyId
        ];

        $orderBlock = new OrderBlock();
        $cart = $orderBlock->cartList($search, 1, count($cartIds));
        return response()->json(ResponseMessage::getInstance()->success([
            'list' => $cart->cart,
            'total_discount_amount' => $cart->total_discount_amount,
            'total_pay_amount' => $cart->total_pay_amount,
            'goods_sum' => $cart->goods_sum,
            'goods_number' => $cart->goods_number
        ])->response());
    }

    /**
     * 购物车列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cartList(Request $request){
        global $companyId;
        global $g_uid;
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        $couponId   = $request->get('coupon_id', '');
        $search = [
            'users_id' => $g_uid,
            'company_id' => $companyId
        ];
        if($request->has('store_id')){
            $search['store_id'] = $request->get('store_id');
        }

        $storeType           = $request->get('store_type', null);
        if($storeType == '餐饮外卖'){
            $search['store_type'] = [1];
        }elseif($storeType === '商城'){
            $search['store_type'] = [2];
        }
        $search['type'] = 1;


        $orderBlock = new OrderBlock();
        $cart = $orderBlock->cartList($search, $page, $pageSize, $couponId);
        $pagination = $orderBlock->cartListPagination($search, $page, $pageSize);

        if ($cart === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('COUPON_ERROR', '请领取优惠券')->response());
        }else if ($cart === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('COUPON_ERROR', '优惠券不存在')->response());
        }else if ($cart === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('COUPON_ERROR', '优惠券已过期')->response());
        }else if ($cart === -4){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('COUPON_ERROR', '当前消费不满足优惠券条件')->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success([
                'list' => $cart->cart,
                'total_discount_amount' => $cart->total_discount_amount,        //优惠金额
                'total_pay_amount'      => $cart->total_pay_amount ,            //支付金额
                'total_full_amount'     => $cart->total_full_amount ,           //原价
                'coupon'                => $cart->coupon,                       //优惠券信息
                'goods_sum'             => $cart->goods_sum,                    //商品数量
                'goods_number'          => $cart->goods_number,                 //商品个数
                'pagination'            => $pagination                          //分页
            ])->response());
        }

    }

    /**
     * 获取送餐时间
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTakeawayTime(Request $request){
        global $g_uid;
        if (!$request->has('address_id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ADDRESS_ERROR', '请选择地址')->response());
        }
        $addressId = $request->get('address_id');
        $time = date('H:i', time() + 1800);

        return response()->json(ResponseMessage::getInstance()->success($time)->response());
    }

    /**
     * 扫码点餐的购物车
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cartListBySeat(Request $request){
        global $companyId;
        $storeId    = (int)$request->get('store_id');
        $seatId     = (int)$request->get('seat_id');

        $search = [
            'company_id'    => $companyId,
            'store_type'    => [1],
            'store_id'      => $storeId,
            'seat_id'       => $seatId,
            'type'          => 2
        ];
        $orderBlock = new OrderBlock();

        $cart = $orderBlock->cartListByOneStore($search);

        $activityObject             = new \StdClass;
        $activityObject->stores_id  = $storeId;
        $activityObject->goods      = $cart;

        $activity = $orderBlock->activityDiscount([$activityObject]);
        if (!empty($activity[0]->activity)){
            $activity[0]->activity->data->full_amount = priceIntToFloat($activity[0]->activity->data->full_amount);
            $activity[0]->activity->data->discount_amount = priceIntToFloat($activity[0]->activity->data->discount_amount);
        }



        $userCart = [];
        $a = 0;
        foreach ($cart as $c){
            $a += $c->selling_price*$c->quantity;
            $c->selling_price = priceIntToFloat($c->selling_price);
            $c->purchase_price = priceIntToFloat($c->purchase_price);
            $c->original_price = priceIntToFloat($c->original_price);
            if(isset($userCart[$c->users_id])){
                $userCart[$c->users_id]['cart'][] = $c;
            }else{
                $userCart[$c->users_id] = [
                    'user_name'     => $c->user_name,
                    'user_id'       => $c->users_id,
                    'user_avatar'   => $c->avatar,
                    'cart'          => []
                ];
                $userCart[$c->users_id]['cart'][] = $c;
            }
        }


        $userCart = array_values($userCart);
        $dataObject = new \StdClass;
        $dataObject->data = $userCart;
        $dataObject->pay_amount         = priceIntToFloat($activity[0]->pay_amount);
        $dataObject->full_amount        = priceIntToFloat($activity[0]->full_amount);
        $dataObject->discount_amount    = priceIntToFloat($activity[0]->discount_amount);
        $dataObject->activity           = $activity[0]->activity;

        return response()->json(ResponseMessage::getInstance()->success($dataObject)->response());
    }

    /**
     * 外卖的购物车
     */
    public function cartListByStore(Request $request){
        global $companyId;
        global $g_uid;

        $storeId    = $request->get('store_id');

        $search = [
            'users_id'      => $g_uid,
            'company_id'    => $companyId,
            'store_type'    => [1],
            'store_id'      => $storeId,
        ];
        $orderBlock = new OrderBlock();

        $cart = $orderBlock->cartListByOneStore($search);

        return response()->json(ResponseMessage::getInstance()->success($cart)->response());
    }

    /**
     * 初始化订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function initOrder(Request $request){
        global $companyId;
        global $g_uid;
        $cart = $request->get('cart');

        $online     = $request->get('online', 0);
        $orderType  = $request->get('order_type', 0);
        $remark     = $request->get('remark','');
        $address_id = $request->get('address_id',0);

        $orderBlock = new OrderBlock();

        $batch = $orderBlock->initOrder($g_uid, $companyId, json_decode($cart), $orderType,$remark,$address_id,$online);

        if ($batch === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '不支持城市')->response());
        }else if ($batch === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '门店不存在')->response());
        }else if ($batch === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '门店未完成入驻')->response());
        }else if ($batch === -4){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '门店已被禁用')->response());
        }else if ($batch === -5){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '订单超距')->response());
        }else if ($batch === -6){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '账户余额不足')->response());
        }else if ($batch === -7){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '重复下单')->response());
        }else if ($batch === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($batch)->response());
        }
    }

    /**
     * 重新下单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request){
        if (!$request->filled('orders_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        global $companyId;
        $orderId    = $request->get('orders_id');
        $online     = $request->get('online', 0);

        $orderBlock = new OrderBlock();
        $batch = $orderBlock->reorder($orderId, $g_uid, $companyId, $online);

        if($batch) {
            return response()->json(ResponseMessage::getInstance()->success($batch)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     *
     */
    private function __getPayObject(){
        $payObject = new \stdClass();

        $payObject->resHandler = new ClientResponseHandler();
        $payObject->reqHandler = new RequestHandler();
        $payObject->pay = new PayHttpClient();

        $payObject->reqHandler->setGateUrl(\Config::get('zhongxin-pay.mini.url'));

        $sign_type = \Config::get('zhongxin-pay.mini.sign_type');

        if ($sign_type == 'MD5') {
            $payObject->reqHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
            $payObject->resHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
            $payObject->reqHandler->setSignType($sign_type);
        } else if ($sign_type == 'RSA_1_1' || $sign_type == 'RSA_1_256') {
            $payObject->reqHandler->setRSAKey(file_get_contents(\Config::get('zhongxin-pay.mini.private_rsa_key')));
            $payObject->resHandler->setRSAKey(file_get_contents(\Config::get('zhongxin-pay.mini.public_rsa_key')));
            $payObject->reqHandler->setSignType($sign_type);
        }

        return $payObject;
    }

    /**
     * 修改订单地址
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateOrderAddress(Request $request){
        global $g_uid;
        $batch      = $request->get('batch');
        $addressId  = $request->get('address_id');

        $orderBlock = new OrderBlock();

        $status = $orderBlock->updateOrderAddress($batch, $g_uid, $addressId);
        if($status === true){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 更新订单优惠券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateOrderCoupon(Request $request){
        global $g_uid;
        $orderId        = $request->get('order_id');
        $couponId       = $request->get('coupon_id');
        $batch          = $request->get('batch', '');

        $orderBlock = new OrderBlock();

        $payAmount = $orderBlock->updateOrderCoupon($orderId, $g_uid, $couponId, $batch);
        if($payAmount === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '未领取该优惠券')->response());
        }elseif($payAmount === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该优惠券已过期')->response());
        }elseif ($payAmount === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '不满足优惠券使用条件')->response());
        }elseif ($payAmount === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * APP微信支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderAppPay(Request $request){
        global $g_uid;
        $batch      = $request->get('batch', '');
        $orderId    = $request->get('orders_id', '');
        $type       = $request->get('type', 'app');
        $orderModel = new Orders();


        if (empty($batch) && empty($orderId) || !empty($batch) && !empty($orderId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $payTime    = $orderModel->orderPatTime($batch, $orderId);
        if ($payTime === 0){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('ORDER_ERROR','订单已失效')
                ->response());
        }

        $payBlock = new PayBlock();
        $data = $payBlock->orderWeChatAppPay($g_uid, $batch, $orderId, $type);

        if($data === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }elseif ($data === -1){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('ORDER_NOT_EXSIT', '订单不存在')->response());
        }elseif($data === -2){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('ORDER_PAID', '订单已支付')->response());
        }elseif($data === -3){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($data)->response());
        }
    }

    /**
     * APP订单微信支付回调
     * @return mixed
     */
    public function orderAppWeChatPayCallback(){
        $app = Factory::payment(\Config::get('zhongxin-pay.app'));
        $payBlock         = new PayBlock();
        $response = $payBlock->orderWeChatAppPayNotify($app);
        if ($response !== false){
            \Log::debug($response);
            return $response;
        }

    }

    /**
     * 小程序微信支付（中信）
     * 文档地址: https://open.swiftpass.cn/openapi/wiki?index=67&chapter=7
     */
    public function orderPay(Request $request){
        global $g_uid;
        $batch      = $request->get('batch', '');
        $type       = $request->get('type', 'pub');
        $orderId    = $request->get('orders_id', '');
        $orderModel = new Orders();


        if (empty($batch) && empty($orderId) || !empty($batch) && !empty($orderId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $payTime    = $orderModel->orderPatTime($batch, $orderId);
        if ($payTime === 0){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('ORDER_ERROR','订单已失效')
                ->response());
        }

        $payBlock = new PayBlock();
        $data = $payBlock->wechatPayByZhongxin($g_uid, $batch, $type, $orderId);

        if($data === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }elseif ($data === -1){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('ORDER_NOT_EXSIT', '订单不存在')->response());
        }elseif($data === -2){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('ORDER_PAID', '订单已支付')->response());
        }elseif($data === -3){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }elseif($data === -4){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('ORDER_PAID', '请更换未使用的优惠券')->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($data)->response());
        }
    }

    /**
     * 支付会调
     * 文档地址: https://open.swiftpass.cn/openapi/wiki?index=67&chapter=7
     * @param Request $request
     */
    public function orderPayCallback(){
        $pay = $this->__getPayObject();
        $xml = file_get_contents('php://input');
        $pay->resHandler->setContent($xml);
        $pay->resHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
        if($pay->resHandler->isTenpaySign()){
            if(
                $pay->resHandler->getParameter('status') == 0
                &&
                $pay->resHandler->getParameter('result_code') == 0
            ){
                $tradeno = $pay->resHandler->getParameter('out_trade_no');
                $transactionId = $pay->resHandler->getParameter('out_transaction_id');
                $totalFee = $pay->resHandler->getParameter('total_fee');


                $payBlock = new PayBlock();
                $status = $payBlock->wechatPayByZhongxinCallback($tradeno, $transactionId, $totalFee);

                if($status === true) {
                    ob_clean();
                    echo 'success';
                    exit();
                }else{
                    echo 'failure1';
                    exit();
                }

            }else{
                echo 'failure1';
                exit();
            }
        }else{
            echo 'failure2';
        }
    }

    /**
     * 获取余额支付二维码 （中信）
     * @param Request $requeste
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBalanceScanCodePay(Request $request){
        $batch          = $request->get('batch', '');
        $orderId        = $request->get('orders_id', '');
        $orderModel     = new Orders();
        $payTime        = $orderModel->orderPatTime($batch, $orderId);

        if (empty($batch) && empty($orderId) || !empty($batch) && !empty($orderId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        if ($payTime === 0){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('ORDER_ERROR','订单已失效')
                ->response());
        }

        $payBlock = new PayBlock();
        $data = $payBlock->getBalanceScanCodePay($batch, $orderId);

        if($data === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }elseif ($data === -1){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('ORDER_NOT_EXSIT', '订单不存在')->response());
        }elseif($data === -2){
            return response()->json(ResponseMessage::getInstance()->setFailedCode('ORDER_PAID', '订单已支付')->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($data)->response());
        }
    }

    public function balanceCallBack(){
        $pay = $this->__getPayObject();
        $xml = file_get_contents('php://input');
        $pay->resHandler->setContent($xml);
        $pay->resHandler->setKey(\Config::get('zhongxin-pay.mini.key'));
        if($pay->resHandler->isTenpaySign()){
            if(
                $pay->resHandler->getParameter('status') == 0
                &&
                $pay->resHandler->getParameter('result_code') == 0
            ){
                $tradeno = $pay->resHandler->getParameter('out_trade_no');
                $transactionId = $pay->resHandler->getParameter('out_transaction_id');
                $totalFee = $pay->resHandler->getParameter('total_fee');

                \Log::debug($tradeno);
                \Log::debug($transactionId);
                \Log::debug($totalFee);
                $payBlock = new PayBlock();
                $status = $payBlock->balanceCallBack($tradeno, $transactionId, $totalFee);

                if($status === true) {
                    ob_clean();
                    echo 'success';
                    exit();
                }else{
                    echo 'failure1';
                    exit();
                }

            }else{
                echo 'failure1';
                exit();
            }
        }else{
            echo 'failure2';
        }
    }

    /**
     * 退款
     * @param Request $request
     */
    public function orderZhongxinRefund(Request $request){
        if (!$request->filled('orders_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $orderId        = $request->get('orders_id');
        $payBlock       = new PayBlock();
        $result         = $payBlock->orderZhongxinRefund($orderId);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()
            ->setFailedCode('ORDER_UPDATE_ERROR', '更新退款状态失败')
            ->failed()->response());
        }else if($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 根据批次号获取订单信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderByBatch(Request $request){
        if (!$request->filled('batch')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $batch = $request->get('batch');
        $orderBlock = new OrderBlock();
        \Log::debug('orderByBatch：'.$batch);
        $order = $orderBlock->orderByBatch($batch);

        if ($order === false){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '未找到该订单')->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($order)->response());
        }

    }

    /**
     * 获取订单列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderList(Request $request){
        global $g_uid;
        $storeType = $request->get('store_type');
        $search         = [
            'users_id'      => $g_uid,
            'store_type'    => $storeType
        ];

        if($storeType == '餐饮外卖'){
            $search['store_type'] = [1];
        }elseif($storeType === '商城'){
            $search['store_type'] = [2];
        }elseif($storeType === '聚南珍'){
            $search['store_type'] = [4];
        }

        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);

        $searchField = ['status', 'batch'];

        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $search['online']   = $request->get('online', '');


        $orderBlock = new OrderBlock();
        $order = $orderBlock->orderList($search, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($order)->response());
    }

    /**
     * 订单详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderInfo(Request $request){
        global $g_uid;
        $orderId = $request->get('order_id');

        $orderBlock = new OrderBlock();
        $order = $orderBlock->orderList(['order_id' => $orderId, 'users_id' => $g_uid], 1, 1);
        if(isset($order[0])){
            $order = $order[0];
            switch ($order->pay_type)
            {
                case 1:
                    $order->pay_type = '微信支付';
                    break;
                case 2:
                    $order->pay_type = '支付宝支付';
                    break;
                case 3:
                    $order->pay_type = '余额支付';
                    break;
                default:
                    $order->pay_type = '暂无支付方式';
            }

            $order->address = $orderBlock->getOrderAddress($order->batch);
            if ($order->store_type_id == 4 && $order->status == 1){
                $order->remaining_payment_time  = naturalNumber(strtotime($order->pay_time) + 300 - time());
            }else{
                $order->remaining_payment_time  = naturalNumber(strtotime($order->create_time) + 900 - time());
            }


            return response()->json(ResponseMessage::getInstance()->success($order)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 修改订单状态
     * @param Request $request
     * @return mixed
     */
    public function updateOrderStatus(Request $request){
        $data = $request->only([
            'order_id',
            'status',
        ]);
        $validator = Validator::make($data, [
            'order_id'        => 'required',
            'status'          => 'required'
        ], [
            'order_id.required'       => '订单id不能为空！！',
            'status.required'         => '订单id状态不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        $orderBlock     = new OrderBlock();
        $result         = $orderBlock->updateOrderStatus($data['order_id'], $data['status']);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()
                ->failed('DATA_EXIST')
                ->response());
        }else if($result === -2){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('FAILURE','已支付，不能进行取消操作')
                ->response());
        }else if($result === -3){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('FAILURE','已支付，订单无法取消')
                ->response());
        }else if($result === -4){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('FAILURE','无法修改状态')
                ->response());
        }else if($result === -5){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('FAILURE','无法取消订单')
                ->response());
        }else if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }

    /**
     * 自提订单核销码
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderCode($id){
//        global $g_uid;
//        $user = Users::where('id', $g_uid)->whereNull('delete_time')->first();
//        $order = Orders::where('id', $id)->where('type', 1)->whereNull('delete_time')->first();
//        if (!$order){
//            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
//        }
        $qrcodeRedirectUrl = 'https://www.baidu.com';
        if(!is_dir(public_path("qrcode/ordercode"))){
            mkdir(public_path("qrcode/ordercode"));
        }
        QrCode::format('png')->size(200)->margin(1)->generate($qrcodeRedirectUrl, public_path("qrcode/ordercode/{$id}.png"));
        $codeImg = \Config::get('app.url')."/qrcode/ordercode/{$id}.png";

        return response()->json(ResponseMessage::getInstance()->success(['qrcode' => $codeImg])->response());
    }

    /**
     * 确认订单
     * @param Request $request
     */
    public function orderEnter(Request $request){

        global $g_uid;
        if (!$request->filled(['store_id', 'long', 'lat'])){
           return  response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $long           = $request->get('long');
        $lat            = $request->get('lat');
        $store_id       = $request->get('store_id');
        $coupon_id      = $request->get('coupon_id', '');

        $orderBlock = new OrderBlock();
        $data = $orderBlock->orderEnter($g_uid, $store_id, $coupon_id, $long, $lat);

        if ($data === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('COUPON_ERROR', '请领取优惠券')->response());
        }else if ($data === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('COUPON_ERROR', '优惠券不存在')->response());
        }else if ($data === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('COUPON_ERROR', '优惠券已过期')->response());
        }else if ($data === -4){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ENTER_ERROR', '超过配送距离')->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($data)->response());
        }
    }


    /**
     * 购物车使用优惠券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderUseCoupons(Request $request){
        if (!$request->filled('stores_id')){
            return  response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global  $g_uid;
        $storeId        = $request->get('stores_id');
        $couponsId      = $request->get('coupons_id', '');

        $orderBlock = new OrderBlock();
        $data = $orderBlock->orderUseCoupons($storeId, $g_uid, $couponsId);

        if($data === -3){
            return response()->json(
                ResponseMessage::getInstance()->failed()
                    ->setFailedCode('COUPONS_ERROR', '请先领取优惠券')
                    ->response()
            );
        }

        if($data === -1){
            return response()->json(
                ResponseMessage::getInstance()->failed()
                    ->setFailedCode('COUPONS_ERROR', '优惠券失效')
                    ->response()
            );
        }

        if($data === -2){
            return response()->json(
                ResponseMessage::getInstance()->failed()
                    ->setFailedCode('COUPONS_ERROR', '未满足优惠条件')
                    ->response()
            );
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


    /**
     *通过订单批次号修改订单配送方式
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderDeliveryUpdate(Request $request){
        if (!$request->has([ 'type', 'remark', 'batch'])){
            return  response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $ordersId   = $request->get('orders_id');
        $batch      = $request->get('batch');
        $type       = $request->get('type');
        $remark     = $request->get('remark');

        if (!isset($ordersId) && !isset($batch)){
            return  response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $orderBlock = new OrderBlock();
        $result = $orderBlock->orderDeliveryUpdate($ordersId, $batch, $type, $remark);


        if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 余额支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function balancePay(Request $request){
        if(!$request->filled('pay_password')){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('PASSWORD_EMPTY', '请输入密码')
                ->failed()->response());
        }
        $payBlock       = new PayBlock();
        $orderBlock     = new OrderBlock();
        $userBlock      = new UserBlock();

        global $g_uid;
        $orderId        = $request->get('orders_id', '');
        $batch          = $request->get('batch', '');
        $password       = $request->get('pay_password');

        $userInfo       = $userBlock->getUserInfo($g_uid);

        if (empty($userInfo->pay_password)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PAY_PASSWORD_ERROR', '请设置支付密码')
                ->response());
        }

        if (md5($password) != $userInfo->pay_password){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('PASSWORD_ERROR', '密码错误')
                ->response());
        }

        if (!empty($orderId) && !empty($batch) || empty($orderId) && empty($batch)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }


        $orderInfo      = $orderBlock->orderInfo($batch, $orderId);
        if (empty($orderInfo)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '未找到该订单')->response());
        }

        $money          = 0;

        foreach ($orderInfo as $item) {
            if ($item->is_use === 1){
                return response()->json(ResponseMessage::getInstance()->failed()
                    ->setFailedCode('ERROR', '请更换未使用的优惠券')->response());
            }

            if ($item->status != 0){
                return response()->json(ResponseMessage::getInstance()
                    ->failed()
                    ->setFailedCode('ORDER_ERROR','已购买，请勿重新购买')
                    ->response());
            }
            $money += $item->pay_amount;
        }


        if ($userInfo->balance < $money){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('BALANCE_ERROR', '余额不足，请充值')
                ->failed()->response());
        }

        $result         = $payBlock->balancePay($g_uid, $batch, $orderId, $userInfo, $orderInfo, $money);


        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 支付宝支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function aliPay(Request $request){
        global $g_uid;
        $batch      = $request->get('batch', '');
        $orderId    = $request->get('orders_id', '');
        $type       = $request->get('type', 'app');
        $orderModel = new Orders();

        if (empty($batch) && empty($orderId) || !empty($batch) && !empty($orderId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $payTime    = $orderModel->orderPatTime($batch, $orderId);

        if ($payTime === -1){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('ORDER_NOT_EXSIT', '订单不存在')->response());
        }
        if ($payTime === 0){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('ORDER_ERROR','订单已失效')
                ->response());
        }

        $payBlock   = new PayBlock();

        $result     = $payBlock->aliPay($g_uid, $batch, $orderId, $type);
        if($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }elseif ($result === -1){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('ORDER_NOT_EXSIT', '订单不存在')->response());
        }elseif($result === -2){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('ORDER_PAID', '订单已支付')->response());
        }elseif($result === -3){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }elseif($result === -4){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('ERROR', '请更换未使用的优惠券')->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($result->getContent())->response());
        }
    }

    /**
     * 支付宝回调
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Yansongda\Pay\Exceptions\InvalidConfigException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function aliPayNotify(){
        $aliPay         = Pay::alipay(config('ali-pay.config'));
        $notifyData     = $aliPay->verify();
        $payBlock       = new PayBlock();

        $result         = $payBlock->aliPayNotify($notifyData);
        if ($result !== false){
            \Log::debug($aliPay->success());
            return $aliPay->success();
        }
    }

    /**
     * 查询快递
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderExpressDelivery(Request $request){
        if (!$request->filled('order_id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请选择一个订单')->response());
        }

        $orderId    = $request->get('order_id');
        $ed         = new ED();
        $orderModel = new Orders();
        $orderInfo  = $orderModel->orderInfo($orderId);
        \Log::debug($orderInfo->abbreviation);
        \Log::debug($orderInfo->expresssn);
        $result     = $ed->getOrderTracesByJson($orderInfo->abbreviation, $orderInfo->expresssn);
        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '暂无物流信息')->response());
        }


    }

    /**
     * 订单使用红包
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderUseRedEnvelope(Request $request){
        if(!$request->filled('orders_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $g_uid;
        $orderId            = $request->get('orders_id');
        $redRedEnvelopeId   = $request->get('red_envelope_id', '');
        $orderBlock         = new OrderBlock();
        $result             = $orderBlock->orderUseRedEnvelope($g_uid, $orderId, $redRedEnvelopeId);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '未领取或已失效')->response());
        }elseif ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '未在使用期限内')->response());
        }elseif ($result === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '不满足红包优惠条件')->response());
        }elseif ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 申请退款
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyOrderRefund(Request $request){
        $validator = Validator::make($request->all(), [
            'orders_id'                 => 'required',
            'order_goods_id'            => 'required',
            'cause'                     => 'required',
            'refund_certificate'        => 'required'
        ], [
            'orders_id.required'                => '请选择订单',
            'order_goods_id.required'           => '请选择商品',
            'cause.required'                    => '请输入退款原因',
            'refund_certificate.required'       => '请上传图片'
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('PARAM_ERROR',$validator->errors()->first())
                ->failed()->response());
        }

        global $g_uid;
        $data               = $validator->validate();
        $data['users_id']   = $g_uid;
        $orderBlock         = new OrderBlock();
        $result             = $orderBlock->applyOrderRefund($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取我的砍价订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyBargainOrder(Request $request){
        global $g_uid;
        $page               = $request->get('page', 1);
        $pageSize           = $request->get('pageSize', 10);

        $orderBlock         = new OrderBlock();
        $data = $orderBlock->getMyBargainOrder($page, $pageSize, $g_uid);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 砍价订单详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyBargainOrderInfo(Request $request){
        if (!$request->filled('bargain_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $bargainId          = $request->get('bargain_id');
        $orderBlock         = new OrderBlock();
        $data               = $orderBlock->getMyBargainOrderInfo($bargainId, $g_uid);

        if ($data === false){
            return response()->json(ResponseMessage::getInstance()->failed('DATA_EXIST')->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($data)->response());
        }
    }

    /**
     * 创建砍价订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBargainOrder(Request $request){
        if (!$request->filled('goods_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }


        global $g_uid;
        $goodsId            = $request->get('goods_id');
        $orderBlock         = new OrderBlock();
        $result = $orderBlock->createBargainOrder($goodsId, $g_uid);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该商品已下架')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该商品暂无库存')->response());
        }else if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }
    }

    /**
     * 加入砍价
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function joinBargainOrder(Request $request){
        if (!$request->filled('bargain_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $bargainId          = $request->get('bargain_id');
        $orderBlock         = new OrderBlock();
        $result             = $orderBlock->joinBargainOrder($g_uid, $bargainId);
        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '已超过砍价时间')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '价格已到底价')->response());
        }else if ($result === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '剩余砍价次数不足')->response());
        }else if ($result === -4){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '订单已失效')->response());
        }else if ($result === -5){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '已为该订单砍过价')->response());
        }else if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }

    }

    /**
     * 砍价订单砍价成功创建普通订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBargainByOrder(Request $request){
        if (!$request->filled('bargain_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $bargainId      = $request->get('bargain_id');
        $orderBlock     = new OrderBlock();
        $result = $orderBlock->getBargainByOrder($bargainId, $g_uid);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '无订单记录')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该订单未砍到底价')->response());
        }else if ($result === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '已超过砍价时间')->response());
        }else if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 获取当前商品正在拼团订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGoodsGroupBuyOrder(Request $request){
        if (!$request->filled('goods_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 10);
        $goodsId    = $request->get('goods_id');

        $orderBlock = new OrderBlock();
        $data       = $orderBlock->getGroupBuyOrder($page, $pageSize, $goodsId);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 开启拼团
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function startGroupOrder(Request $request){
        if(!$request->filled(['goods_id'])){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        global $g_uid;
        $goodsId        = $request->get('goods_id');
        $remake         = $request->get('remake', '');
        $orderBlock     = new OrderBlock();
        $result         = $orderBlock->startGroupOrder($goodsId, $g_uid, $remake);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '商品已下架')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '暂无库存')->response());
        }else if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }
    }

    /**
     * 加入拼团
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function joinGroupOrder(Request $request){
        if (!$request->filled('group_buy_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $groupBuyId     = $request->get('group_buy_id');
        $remake         = $request->get('remake');
        $orderBlock     = new OrderBlock();
        $result = $orderBlock->joinGroupOrder($groupBuyId, $g_uid, $remake);

        if ($result['code'] == 'success'){
            return response()->json(ResponseMessage::getInstance()->success($result['message'])->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', $result['message'])->response());
        }
    }

    /**
     * 获取我的拼团订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function myOrderGroupBuy(Request $request){
        global $g_uid;
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 10);

        $orderBlock     = new OrderBlock();
        $data           = $orderBlock->myOrderGroupBuy($g_uid, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 拼团支付宝支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function groupBuyAliPay(Request $request){
        if (!$request->filled('group_log_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $groupLogId     = $request->get('group_log_id');
        $orderBlock     = new PayBlock();
        $data           = $orderBlock->groupBuyAliPay($groupLogId);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 拼团支付宝支付回调
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Yansongda\Pay\Exceptions\InvalidConfigException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function groupBuyAliPayNotify(){
        $aliPay         = Pay::alipay(config('ali-pay.group'));
        $notifyData     = $aliPay->verify();
        $payBlock       = new PayBlock();

        if($payBlock->groupBuyAliPayNotify($notifyData)['code'] == 'success'){
            return $aliPay->success();
        }
    }

    /**
     * 拼团微信支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function groupBuyWeChatPay(Request $request){
        if (!$request->filled('group_log_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $groupLogId     = $request->get('group_log_id');
        $type           = $request->get('type', 'app');
        $orderBlock     = new PayBlock();
        $data           = $orderBlock->groupBuyWeChatPay($groupLogId, $type);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 拼团微信支付回调
     * @return mixed
     */
    public function groupBuyWeChatPayNotify(){
        $app = Factory::payment(\Config::get('zhongxin-pay.app'));
        $payBlock         = new PayBlock();
        $response = $payBlock->groupBuyWeChatPayNotify($app);
        if ($response !== false){
            return $response;
        }
    }


    /**
     * 拼团余额支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function groupBuyBalancePay(Request $request){
        if (!$request->filled('group_log_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $groupLogId     = $request->get('group_log_id');
        $orderBlock     = new PayBlock();
        $result = $orderBlock->groupBuyBalancePay($groupLogId, $g_uid);

        if ($result['code'] == 'success'){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', $result['message'])->response());
        }
    }




}
