<?php

namespace App\Http\Controllers\Pluto;

use App\Blocks\GoodsBlock;
use App\Blocks\OrderBlock;
use App\Blocks\PayBlock;
use App\Blocks\StoreBlock;
use App\Http\Controllers\Controller;
use App\Libs\DianWoDa;
use App\Libs\Ketuo;
use App\Libs\ResponseMessage;
use App\Libs\YunXin\ServerApi;
use App\Models\About;
use App\Models\Activity;
use App\Models\ActivityApply;
use App\Models\Banners;
use App\Models\GameDraw;
use App\Models\GameDrawSolve;
use App\Models\Orders;
use App\Models\OrderTakeawayMessage;
use App\Models\PageNav;
use App\Models\StoreCoupons;
use App\Models\StoreInfo;
use App\Models\Stores;
use App\Models\StoreSeatLineup;
use App\Models\StoreYunxin;
use App\Models\UserYunxin;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Version;
use App\Models\QuestionAnswer as QA;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use QrCode;
use App\Http\Controllers\Tool\StoreJiGuangPushController as StorePush;
use App\Http\Controllers\Tool\JiGuangPushController as Push;
use App\Blocks\UserBlock;

class IndexController extends Controller{

    /**
     * 获取轮播图
     * @return \Illuminate\Http\JsonResponse
     */
    public function bannerList(Request $request){
        global $companyId;
        $position = $request->get('position', 'canyin');

        $date = date('Y-m-d H:i:s', time());

        $banner = Banners::where('company_id', $companyId)
            ->where('uptime', '<=', $date)
            ->where('downtime', '>=', $date)
            ->where('position', $position)
            ->orderBy('sort', 'asc')
            ->get();

        return response()->json(ResponseMessage::getInstance()->success($banner)->response());
    }

    /**
     * 页面配置
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pageConfig(Request $request){
        global $companyId;
        $position   = $request->get('position', '商城首页');
        $source     = $request->get('source', 'h5');
        $token      = $request->get('token', '');

        $userId     = '';
        if (!empty($token)){
            if($token == 'test123'){
                $userId = 87;
            }else {
                $userInfo = \Cache::get($token);
                if (!empty($userInfo) && isset($userInfo->id)){
                    $userId = $userInfo->id;
                }

            }
        }


        $date = date('Y-m-d H:i:s', time());

        $banner = Banners::where('company_id', $companyId)
            ->where('uptime', '<=', $date)
            ->where('downtime', '>=', $date)
            ->where('position', $position)
            ->orderBy('sort', 'asc')
            ->get();

        $nav = PageNav::where('position', $position)
            ->whereNull('delete_time')
            ->orderBy('sort', 'asc')
            ->get();
        $nav = json_decode(json_encode($nav), true);
        foreach ($nav as $k=>&$v){
            if (!empty($v['url'])){
                $v['url'] = json_decode($v['url'], true);
            }

        }

        $seckill = [];
        $pageNavModel = new PageNav();
        if ($position == '首页'){

            $recommend =$pageNavModel->getPageConfig('精选推荐');

        }elseif ($position == '商城首页'){

//            $recommend =$pageNavModel->getPageConfig('活动专区');
            $recommend = [];
            $goodsModel = new GoodsBlock();
            $time   = intval(date("H", time()));
            $time   = intval(floor($time/2));
            $time   *=2;
            if ($time<10){
                $time .='0';
            }
            $nowTime  = date("Y-m-d", time())." ".$time.':00';
            $seckill['time'] = strtotime($nowTime) + 7200 - time();
            $seckill['data'] = $goodsModel->getSpike(date("Y-m-d", time()), $time);



        }else{

            $recommend =$pageNavModel->getPageConfig('休闲娱乐');


        }

        if (!empty($recommend)){
            foreach ($recommend as &$value){
                if (!empty($value->url)){
                    $value->url = json_decode($value->url, true);
                }

            }
        }


        if (!empty($userId)){
            $lineup = StoreSeatLineup::where('users_id', $userId)
                ->where('stores_id', 1)
                ->where('is_use', 0)
                ->whereNull('delete_time')
                ->first();
        }else{
            $lineup = '';
        }


        $hot = [
            'image'         => 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/banner/remenmeishi%402x.png',
            'title'         => '大斌家串串火锅',
            'is_lineup'     => $lineup ? 1: 0,
            'people'        => $lineup ? $lineup->people : 0,
            'extra'         => [
                'lineup' => 183,
                'store_id' => 1
            ]
        ];

        return response()->json(ResponseMessage::getInstance()->success([
            'banner'        => $banner,
            'nav'           => $nav,
            'recommend'     => $recommend,
            'hot'           => $hot,
            'seckill'       => $seckill,
            'food'          => [
                'image'     => 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/foot/157473820071.png?x-oss-process=image/quality,q_50',
                'url'       => 'TakeOutCopy',
                'store_type'=> 4
            ]
        ])->response());
    }

    /**
     * 关于我们
     * @return \Illuminate\Http\JsonResponse
     */
    public function about(){
        global $companyId;
        $aboutModel = new About();
        $about = $aboutModel::where('company_id', $companyId)->first();
        return response()->json(ResponseMessage::getInstance()->success($about)->response());
    }

    /**
     * 公益活动列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function activity(Request $request){
        global $companyId;
        $token          = $request->get('token', '');
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        if (!empty($token)){
            if($token == 'test123'){
                $userId = 87;
            }else {
                $userInfo = \Cache::get($token);
                if (!empty($userInfo)){
                    $userId = $userInfo->id;
                }

            }
        }

        $activityModel          = new Activity();
        $activityApplyModel     = new ActivityApply();

        $sql = $activityModel::where('company_id',$companyId)
            ->where('start_time','>=',date('Y-m-d',time()))
            ->whereNull('delete_time')
            ->orderBy('sort','desc')
            ->orderBy('id','desc');

        $total = $sql->count();
        $activitys      = $sql->skip(($page - 1) * $pageSize)->take($pageSize)->get();
        $pagination     =  [
            'total'     => $total,
            'pageSize'  => $pageSize,
            'current'   => $page,
        ];

        $userActivityId = [];
        if (isset($userId) && !empty($userId)){
            $userActivity = $activityApplyModel::where('users_id', $userId)->get();
            foreach ($userActivity as $item) {
                $userActivityId[] = $item->act_id;
            }

        }

        foreach ($activitys as $value) {
            if (in_array($value->id, $userActivityId)){
                $value->is_join = 1;
            }else{
                $value->is_join = 0;
            }
        }


        return response()->json(ResponseMessage::getInstance()->success(['list' => $activitys, 'pagination' => $pagination])->response());
    }

    /**
     * 公益活动详情
     * @return \Illuminate\Http\JsonResponse
     */
    public function activityInfo(Request $request){
        $token  = $request->get('token', '');
        $id     = $request->get('id');

        if (!empty($token)){
            if($token == 'test123'){
                $userId = 87;
            }else {
                $userInfo = \Cache::get($token);
                $userId = $userInfo->id;
            }
        }

        $activity = Activity::where('id',$id)
            ->whereNull('delete_time')->first();
        if (isset($userId) && !empty($userId)){
            $userActivity = ActivityApply::where('users_id', $userId)->where('act_id', $id)->first();
            if ($userActivity){
                $activity->is_join = 1;
            }else{
                $activity->is_join = 0;
            }
        }else{
            $activity->is_join = 0;
        }


        if($activity){
            return response()->json(ResponseMessage::getInstance()->success($activity)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("ACTIVITY_EXIST")->response());
        }

    }



    /**
     * 小程序是否上线0：未上线 1：已上线
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(){


        return response()->json(ResponseMessage::getInstance()->success([
            'check' => 0
        ])->response());
    }

    public function getPaymentMethod(Request $request){
        $type = $request->get('type', 'app');
        $position = $request->get('position', 'order');
        $data = [
            [
                'icon' => 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/pay_icon/156242058031.png',
                'type' => 'ye',
                'name' => '余额支付'
            ],
            [
                'icon'  => 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/pay_icon/156242066383.png',
                'type'  => 'wx',
                'name'  => '微信支付'
            ],
            [
                'icon'  => 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/pay_icon/156242068363.png',
                'type'  => 'zfb',
                'name'  => '支付宝支付'
            ]

        ];
        if ($type == 'app' && $position == 'order'){//APP订单支付
            $payType[0] = $data[0];
            $payType[1] = $data[1];
            $payType[2] = $data[2];
        }else if ($type == 'app' && $position == 'recharge'){//APP充值
            $payType = [];
            $payType[0] = $data[1];
            $payType[1] = $data[2];
        }else if ($type == 'app' && $position == 'snack'){//APP聚南珍
            $payType[0] = $data[0];
            $payType[1] = $data[1];
            $payType[2] = $data[2];
        }else if ($type == 'mini' && $position == 'order'){//小程序订单
            $payType[0] = $data[1];
        }else if ($type == 'mini' && $position == 'recharge'){//小程序充值
            $payType[0] = $data[1];
        }else if ($type == 'mini' && $position == 'snack'){//小程序聚南珍
            $payType[0] = $data[0];
            $payType[0] = $data[1];
        }


        return response()->json(ResponseMessage::getInstance()->success($payType)->response());
    }


    /**
     * 检测手机验证码是否正确
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkMobile(Request $request){
        if (!$request->filled('mobile')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $mobile = $request->get('mobile');

        $user_mobile = Users::where('mobile', $mobile)->first();

        if ($user_mobile){
            return response()->json(ResponseMessage::getInstance()->success(false)->response());
        }

        return response()->json(ResponseMessage::getInstance()->success(true)->response());
    }

    /**
     * 验证手机验证码是否正确
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkMobileCode(Request $request){
        if (!$request->filled(['validate_code', 'mobile'])){
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        $testMobile = [
            '15074766120',
            '13112341234'
        ];

        $mobile         = $request->get('mobile');
        $validateCode   = $request->get('validate_code');

        if(in_array($mobile, $testMobile) && $validateCode == '123456'){
            return response()->json(ResponseMessage::getInstance()->success(true)->response());
        }

        $cacheCode      = \Cache::get($mobile);
        if(!$cacheCode || $validateCode != $cacheCode){
            return response()->json(ResponseMessage::getInstance()->success(false)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success(true)->response());
        }
    }

    public function xhlmxc(){

        return view('xhlmxc');
    }

    public function storeSeatQrCode(){

        $content = '请使用微信扫码或者下载“梦想商区”app扫码！';
        return response($content);
    }

    /**
     * 检查更新app版本
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVersion(Request $request){
        dd();
        if (!$request->filled(['version', 'type'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $version            = $request->getContent('version');
        $versionModel       = new Version();
        $data               = $versionModel->getVersionInfo();
        $data->type         = $data->type === 1 ? 'Android' : 'IOS';
        if ($data->version > $version){
            $data->is_newest = 1;
        }else{
            $data->is_newest = 0;
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


    /**
     * 获取QA问答问题列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQuestion(){
        $QAModel        = new QA();
        $data           = $QAModel->getQuestionList();

        $result = [];
        foreach ($data as $key => $value) {
            $result[$key]['id']         = $value->id;
            $result[$key]['problem']    = $value->problem;
        }

        return response()->json(ResponseMessage::getInstance()->success($result)->response());
    }

    /**
     * 获取QA问答答案
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAnswer(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARAM_ERROR', '请选择一个问题')->response());
        }

        $id             = $request->get('id');
        $QAModel        = new QA();
        $data           = $QAModel->getAnswer($id);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


    /**
     * 获取签列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGameDraw(){
        $gameDrawModel          = new GameDraw();
        $gameDrawSolveModel     = new GameDrawSolve();
        $gameDrawData           = $gameDrawModel->getRandomDraw();
        $gameDrawData = json_decode(json_encode($gameDrawData), true);
        if (!empty($gameDrawData)){
            $gameDrawData[0]['solve']    = $gameDrawSolveModel->getRandomDrawSolve($gameDrawData[0]['id']);
            return response()->json(ResponseMessage::getInstance()->success($gameDrawData[0])->response());
        }else{

            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

    }


    /**
     * 用户批量注册云信
     */
    public function registerUserYunXin(Request $request){
        $userModel          = new Users();
        $yunXin             = new ServerApi();
        $userYunXinModel    = new UserYunxin();
        $page = $request->get('page');

        ini_set('max_execution_time','0');

        $user = DB::table($userModel->getTable())->whereNull('delete_time')
            ->skip($page)->take(10)
            ->get();

        $userInfo = [];
        foreach ($user as $key => $item) {
            $userInfo[$key]['users_id']  = $item->id;
            $userInfo[$key]['accid']     = md5($item->id.$item->name);
            $yunXinInfo = $yunXin->updateUserToken(md5($item->id.$item->name));
            if ($yunXinInfo['code'] === 200){
                $userInfo[$key]['token']     = $yunXinInfo['info']['token'];
            }else{
                $result = $yunXin->createUserId(md5($item->id.$item->name));
                if ($result['code'] === 200){
                    $userInfo[$key]['token']     = $result['info']['token'];
                }
            }
        }

        DB::table($userYunXinModel->getTable())->insert($userInfo);
    }

    /**
     * 商户批量注册云信
     * @param Request $request
     */
    public function registerStoreYunXin(Request $request){
        $StoresModel        = new Stores();
        $yunXin             = new ServerApi();
        $storeYunXinModel   = new StoreYunxin();
        $page = $request->get('page');

        ini_set('max_execution_time','0');

        $store = DB::table($StoresModel->getTable())->whereNull('delete_time')
            ->skip($page)->take(10)
            ->get();

        $storeInfo = [];
        foreach ($store as $key => $item) {
            $storeInfo[$key]['stores_id']  = $item->id;
            $storeInfo[$key]['accid']     = md5($item->id.$item->name);
            $yunXinInfo = $yunXin->updateUserToken(md5($item->id.$item->name));
            if ($yunXinInfo['code'] === 200){
                $storeInfo[$key]['token']     = $yunXinInfo['info']['token'];
            }else{
                $result = $yunXin->createUserId(md5($item->id.$item->name));
                if ($result['code'] === 200){
                    $storeInfo[$key]['token']     = $result['info']['token'];
                }
            }
        }

        DB::table($storeYunXinModel->getTable())->insert($storeInfo);
    }


    public function createUserYunXin(){
        $yunXin         = new ServerApi();
        $name           = 'hjh';
        $avatar         = 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/avatar/156982544437.jpeg';

        $result = $yunXin->updateUinfo(md5('hjh'), $name, $avatar);
        dd($result);
    }

    /**
     * 获取抽奖数据
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLotteryCoupon(){
        $storeCouponModel       = new StoreCoupons();
        $data   = $storeCouponModel->getScopeCoupon('抽奖');
        $dataTotal = count($data);
        for ($i = $dataTotal; $i <= 8; $i++){
            if ($i < 8){
                $data[$i] = ['id' => 0, 'name' => '未中奖'];
            }
        }
        return response()->json(ResponseMessage::getInstance()->success([
            'data'      => $data,
            'rule'      => '抽奖规则：每人/天免费一次，分享可增加一次，最多分享五次，每人/天最多6次'
        ])->response());
    }

    public function getBargainMoney(){
        for ($i = 0; $i < 100; $i++){
            echo $this->getLottery();
            echo "<br/>";
        }
//        $app = Factory::payment(\Config::get('zhongxin-pay.app'));
//        $result = $app->order->queryByOutTradeNumber('15f503a4827f9b4abeeae89099de260d');
//        dd($result);
    }

    public function getLottery(){
        $data = [
            ['id' => 1, 'name' => '奖品一', 'odds' => 1],
            ['id' => 2, 'name' => '奖品二', 'odds' => 2],
            ['id' => 3, 'name' => '奖品三', 'odds' => 3],
            ['id' => 4, 'name' => '奖品四', 'odds' => 4],
            ['id' => 5, 'name' => '奖品五', 'odds' => 5],
            ['id' => 6, 'name' => '奖品六', 'odds' => 6],
        ];

        $proSum = 0;
        foreach ($data as $value) {
            $proSum += $value['odds'];
        }
        $result = '';
        foreach ($data as $key => $proCur) {
            $randNum = mt_rand(1, 100);
            if ($randNum <= $proCur['odds']) {
                $result = $proCur['name'];
                break;
            } else {
                $proSum -= $proCur['odds'];
            }
        }
//        unset ($proArr);

        return empty($result) ? '未中奖' : $result;
    }

    /**
     * 取消订单
     * @return mixed
     */
    public function orderCancel(){
        $dwd        = new DianWoDa();
        $params     = [
            'order_original_id'     => env('DIANWODA_APPKEY').'161157198755912324515',
            'cancel_type'           => 3,
            'cancel_reason'         => '订单超时'
        ];

        return $dwd->request('dianwoda.order.cancel', $params);
    }

    /**
     * 商户确认出餐
     * @return mixed
     */
    public function orderMealDone(){
        $dwd        = new DianWoDa();
        $params     = [
            'order_original_id'     => env('DIANWODA_APPKEY').'161157198755912324515',
            'time_meal_ready'       => getMillisecond()
        ];

        return $dwd->request('dianwoda.order.mealdone', $params);
    }

    public function orderRemarkUpdate(){
        $dwd        = new DianWoDa();
        $params     = [
            'order_original_id'     => env('DIANWODA_APPKEY').'324157676279291743351',
            'order_info_content'    => '修改订单备注：下次一定'
        ];

        return $dwd->request('dianwoda.order.remark.update', $params);
    }

    //生成签名
    private function sign( $timestamp, $nonce, $api, $biz_params ) {
        $unsignRequestParams = array(
            'timestamp' => $timestamp,
            'nonce' => $nonce,
            'type' => $api
        );
        ksort($unsignRequestParams);
        $unsignStr = '';
        foreach ($unsignRequestParams as $key => $value){
            $unsignStr = $unsignStr.$key.'='.$value.'&';
        }
        $unsignStr = $unsignStr.'body='.$biz_params;
        $unsignStr = $unsignStr.'&secret='.env('DIANWODA_SECRET');
        return sha1($unsignStr);
    }

    /**
     * 点我达回调信息
     * @return false|string
     */
    public function dianWoDaRollBack(){
        $query = $_SERVER["QUERY_STRING"];
        parse_str($query,$params);
        $timestamp = $params['timestamp'];
        $nonce = $params['nonce'];
        $type = $params['type'];
        $sign = $params['sign'];
        $body = @file_get_contents('php://input');

        if( abs(getMillisecond() - $timestamp) > 5 * 60 * 1000 ){
            $returnData = [
                'code'      => 'fail',
                'message'   => '过期请求'
            ];
            return json_encode($returnData);
        }

        $local_sign = $this->sign($timestamp,$nonce,$type,$body);
        if($local_sign != $sign){
            $returnData = [
                'code'      => 'fail',
                'message'   => '错误签名'
            ];
            return json_encode($returnData);
        }

        $redis_key = $timestamp.'-'.$nonce;

        $repeat_req = Redis::get($redis_key);
        if($repeat_req != NULL){
            $returnData = [
                'code'      => 'fail',
                'message'   => '重复请求'
            ];
            return json_encode($returnData);
        }

        Redis::setex($redis_key, 10 ,'rollBack');

        if($type == 'dianwoda.order.status-update'){
            $body           = json_decode($body, true);
            $orderModel     = new Orders();
            $orderBlock     = new OrderBlock();
            \Log::debug($body);
            $orderInfo      = $orderModel->orderInfoByBatch($body['content']['order_original_id']);
            if ($body['content']['action_code'] == 'customer_sign'){
                //点我达骑手已确认送达
                $orderBlock->updateOrderStatus($orderInfo->id, 3);
            }else if ($body['content']['action_code'] == 'rider_obtain'){
                //点我达骑手已离店
                $orderBlock->updateOrderStatus($orderInfo->id, 2);
            }else if (
                $body['content']['action_code'] == 'customer_sign_abnormal'
                ||
                $body['content']['action_code'] == 'rider_cancel'
                ||
                $body['content']['action_code'] == 'customerservice_cancel'
                ||
                $body['content']['action_code'] == 'platform_cancel'
            ){
                //订单异常，订单已取消或用户签收异常
                $takeawayMessageModel       = new OrderTakeawayMessage();
                $takeawayMessageModel->saveMessage([
                    'batch'                 => $body['content']['order_original_id'],
                    'code'                  => $body['content']['action_sub_code'],
                    'message'               => $body['content']['action_detail'],
                ]);
            }

            return json_encode(['code' => 'success']);

        }

    }

    /**
     * 获取订单信息
     * @return mixed
     */
    public function orderQuery(){
        $dwd        = new DianWoDa();
        $params     = [
            'order_original_id'     => env('DIANWODA_APPKEY').'_'.'324157681411951776592'
        ];

        return $dwd->request('dianwoda.order.query', $params);
    }

    public function registerStoreTakeaway(){
        $storeModel         = new Stores();
        $storeInfoModel     = new StoreInfo();

        $dwd                = new DianWoDa();
        $store = DB::table($storeModel->getTable().' as s')
            ->select(
                's.id as stores_id',
                's.name as store_name',
                'si.address',
                's.mobile',
                'si.longitude as lng',
                'si.latitude as lat'
            )
            ->leftJoin($storeInfoModel->getTable().' as si', 'si.store_id', '=', 's.id')
            ->where('s.store_type_id', 1)
            ->whereNull('s.delete_time')
            ->whereNotNull('s.mobile')
            ->whereNotNull('si.longitude')
            ->whereNotNull('si.latitude')
            ->get();
        $store = json_decode(json_encode($store), true);

        try{
            DB::beginTransaction();

            foreach ($store as $item) {
                $result = $dwd->sellerTransportationConfirm($item);
                \Log::debug($item['stores_id'].$result);
            }

            DB::commit();
            return 1;
        }catch(\Exception $exception){
            \Log::error($exception);
            DB::rollBack();
            return 2;
        }

    }

    public function getDownLoad(){
        return view('download', [
            'android'       => 'https://api.hnxhlmxc.com/xhlmxc.1.2.apk',
            'ios'           => 'https://apps.apple.com/cn/app/id1475218384',
            'config'        => [
                'name'      => '新华联梦想城',
                'ios'       => [
                    'last_version' => ''
                ]
            ],
            'name'          => '新华联梦想城'
        ]);
    }

    public function test(){
        try{
            $storeModel     = new Stores();
            $store = DB::table($storeModel->getTable())
                ->select('name', 'id')
                ->where('id',220 )
                ->get();
            if (!is_dir(public_path().'/images')){
                @mkdir(public_path().'/images', 0755);
            }

            if (!is_dir(public_path().'/images/store')){
                @mkdir(public_path().'/images/store', 0755);
            }

            $path = public_path().'/images/store';
            $data = [
                'redirect_type'     => 'native',
                'path'              => 'ShopFoodOrderDetail',
                'params'            => [
                    'id'            => 213,
                    'type'          => 4
                ]
            ];
            $imageName = '依芬醉 米酒'.".png";
            \SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')
                ->size(500)
                ->encoding('UTF-8')
                ->merge('/public/images/logo.png',.15)
                ->generate(
                    json_encode($data),
                    $path.'/'.$imageName
                );
        }catch(\Exception $exception){
            \Log::error($exception);
        }


    }

    /**
     * 批量关闭商户
     */
    public function downStore(){
        $storeBlock  = new StoreBlock();
        $result = $storeBlock->downStore();
        return $result;
    }

    /**
     * 批量获取上级二维码
     */
    public function generateQrCode(){
        $storeBlock     = new StoreBlock();
        $result   = $storeBlock->generateQrCode();
        return $result;
    }

    public function download(Request $request){
        $apkConfig = \Config::get("apk");

        if(empty($apkConfig)){
            return false;
        }
        $host       = env("APP_URL");
        $appName    = 'mengxiangshangqu';

        $plistContent = <<<PLIST
<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>items</key>
        <array>
            <dict>
                <key>assets</key>
                <array>
                    <dict>
                        <key>kind</key>
                        <string>software-package</string>
                        <key>url</key>
                        <string>{$host}/apk/{$appName}/ios-v{$apkConfig['ios']['last_version']}.ipa</string>
                    </dict>
                    <dict>
                        <key>kind</key>
                        <string>display-image</string>
                        <key>url</key>
                        <string>{$host}/apk/{$appName}/57.png</string>
                    </dict>
                    <dict>
                        <key>kind</key>
                        <string>full-size-image</string>
                        <key>url</key>
                        <string>{$host}/apk/{$appName}/512.png</string>
                    </dict>
                </array>
                <key>metadata</key>
                <dict>
                    <key>bundle-identifier</key>
                     <string>{$apkConfig['ios']['bundle']}</string>
                     <key>bundle-version</key>
                    <string>{$apkConfig['ios']['last_version']}</string>
                    <key>kind</key>
                    <string>software</string>
                    <key>title</key>
                    <string>{$apkConfig['name']}</string>
                </dict>
            </dict>
        </array>
    </dict>
</plist>
PLIST;

        if(!file_exists(public_path()."/apk")){
            @mkdir(public_path()."/apk");
        }

        if(!file_exists(public_path()."/apk/{$appName}")){
            @mkdir(public_path()."/apk/{$appName}");
        }
        file_put_contents(public_path()."/apk/{$appName}/ios.plist", $plistContent);

        $downloadUrl = env('DOWNLOAD_URL');

        $android = env("APP_URL")."/xhlmxc.1.5.apk?v=".mt_rand(1000, 9999);
//        $ios = 'itms-services://?action=download-manifest&url='.env("APP_URL")."/apk/{$appName}/ios.plist";

        $ios = 'https://apps.apple.com/cn/app/id1475218384';
        return view('download-fir', ['downloadUrl' => $downloadUrl, 'android' => $android, 'ios' => $ios, 'config' => $apkConfig, 'name' => $appName]);
    }

    /**
     * 余额支付手动退款
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function manualOrderRefundPay(Request $request){
        if (!$request->filled('orders_id')){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        $orderId            = $request->get('orders_id');
        $payBlock           = new PayBlock();
        $result = $payBlock->manualOrderRefundPay($orderId);
        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该订单不支持手动退款')->response());
        }if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 批量添加自定义价格商品
     */
    public function addStoreGoods(){
        $storeBlock     = new StoreBlock();
        $result = $storeBlock->addStoreGoods();
        var_dump($result);
    }

    public function recoverOrderAmount(){
        $orderBlock     = new OrderBlock();
        $result = $orderBlock->recoverOrderAmount('19215773301374930537');
        return $result;
    }

    public function saveStoreTurnover(Request $request){
        $time = $request->get('time');
        $orderBlock     = new OrderBlock();
        $orderBlock->saveStoreTurnover($time);
    }

    public function testPush(){
        $push   = new StorePush();
        $result = $push->Ship('1517bfd3f751be562a5',1)->push();
        dd($result);
    }

    public function storeServiceAgreement(){
        return view('/agreement/storeService');
    }

    public function userServiceAgreement(){
        return view('agreement/userService');
    }

    public function foodAgreement(){
        return view('agreement/foodService');
    }

    public function privacyAgreement(){
        return view('agreement/privacyService');
    }


}