<?php

namespace App\Http\Controllers\Pluto;

use App\Blocks\GoodsBlock;
use App\Blocks\GoodsEstimatesBlock;
use App\Blocks\UserBlock;
use App\Blocks\StoreBlock;
use App\Blocks\StoreBooksBlock;
use App\Blocks\StoreImageBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\GoodsCuisine;
use App\Models\StoreActivity;
use App\Models\StoreBooks;
use App\Models\StoreClassifity;
use App\Models\StoreCoupons;
use App\Models\StoreInfo;
use App\Models\StoreNavs;
use App\Models\Stores;
use App\Models\StoreSeat;
use App\Models\StoreSeatLineup;
use App\Models\UserCoupons;
use App\Models\UserMessage;
use App\Models\Users;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Models\StoreRecommendLeisure;
use DB;
use Yansongda\Pay\Pay;
use App\Http\Controllers\Tool\JiGuangPushController as Push;

class StoreController extends Controller
{

    /**
     * 获取商户分类
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeClassify(Request $request)
    {
        global $companyId;
        $pid = $request->get('pid', 0);
        $classify = StoreClassifity::where('parent_id', $pid)
            ->where('company_id', $companyId)
            ->whereNull('delete_time')
            ->orderBy('sort', 'asc')
            ->get();
        return response()->json(ResponseMessage::getInstance()->success($classify)->response());
    }

    /**
     * 获取品牌商家
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreBrand(){
        $storeBlock         = new StoreBlock();
        $data               = $storeBlock->getStoreBrand();
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取店铺列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeList(Request $request)
    {
        global $companyId;
        \Log::debug(json_encode($request->all()));

        if ($request->filled('token')){
            $token = $request->get('token');
            if($token == 'test123'){
                $g_uid = 9;
            }else {
                $userInfo = \Cache::get($token);
                if (gettype($userInfo) == 'object'){
                    if (!empty($userInfo) && isset($userInfo->id)){
                        $g_uid = $userInfo->id;
                    }
                }
            }
        }


        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 20);
        $search = ['company' => $companyId, 'operate' => 1];
        $type = $request->get('type', null);

        if ($type == '餐饮外卖') {
            $search['store_type'] = [1];
        } elseif ($type === '商城') {
            $search['store_type'] = [2];
        } elseif ($type === '休闲娱乐'){
            $search['store_type'] = [3];
        }elseif ($type === '聚南珍'){
            $search['store_type'] = [4];
        }

        $searchField = [
            'name',
            'cid',
            'is_book',
            'choice_store',
            'recommend',
            'is_lineup',
            'lat',
            'long',
            'init_delivery_price',
            'delivery_fee',
            'activity',
            'cuisine',
            'stores_id'
        ];

        foreach ($searchField as $field) {
            if ($request->filled($field)) {
                $search[$field] = $request->get($field);
            }
        }

        $fieldData = [
            'id'          => 'id',
            '销量'        => 'sales_volume',
            '人气'        => 'user_volume',
            '评分'        => 'rating'
        ];

        if ($request->filled('field')){
            $search['field'] = $fieldData[$request->get('field', 'id')];
        }else{
            $search['field'] = $fieldData['id'];
        }
        $search['status'] = 2;
        $search['sequence'] = $request->get('sequence', 'desc');
        $storeBlock = new StoreBlock();

        $store = $storeBlock->storeList($search, $page, $pageSize);

        //是否推荐商品
        if ($request->has('goods') && $request->get('goods') == 1) {
            $storeIds = [];
            foreach ($store as $s) {
                $storeIds[] = $s->id;
            }

            $goodsBlock = new GoodsBlock();
            $goods = $goodsBlock->goodsList(['store_id' => $storeIds], 1, 20000);

            foreach ($store as $s) {
                $s->recommend_goods = [];
                foreach ($goods as $g) {
                    if ($g->stores_id === $s->id) {
                        $s->recommend_goods[] = $g;
                    }
                }
            }
        }

        if (isset($search['is_lineup']) && $search['is_lineup'] == 1 && isset($g_uid) ){
            $storeLineupModel   = new StoreSeatLineup();
            $userStoreLineup    = $storeLineupModel->userStoreLineup($g_uid);

            foreach ($store as $value) {
                foreach ($userStoreLineup as $item) {
                    if ($value->id === $item->stores_id) {
                        $value->user_lineup = 1;
                    }else{
                        $value->user_lineup = 0;
                    }
                }
            }

        }

        $pagination = $storeBlock->storeListPagination($search, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success(['list' => $store, 'pagination' => $pagination])->response());
    }

    /**
     * 获取店铺详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeInfo(Request $request)
    {
        $id = $request->get('id');
        $token = $request->get('token');
        if ($token == 'test123') {
            $userId = 9;
        } else {
            $userInfo = \Cache::get($token);
            if (isset($userInfo->id)) {
                $userId = $userInfo->id;
            } else {
                $userId = null;
            }


        }

        $storeBlock = new StoreBlock();
        $store = $storeBlock->storeInfo($id, $userId);
        return response()->json(ResponseMessage::getInstance()->success($store)->response());
    }

    /**
     * 店铺左侧导航店铺左侧导航
     * @param $storeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeNav($storeId)
    {
        $navs = StoreNavs::where('stores_id', $storeId)
            ->select('id', 'name')
            ->whereNull('delete_time')
            ->orderBy('sort', 'asc')
            ->get();
        return response()->json(ResponseMessage::getInstance()->success($navs)->response());
    }

    /**
     * 返回餐饮菜单特殊格式
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeMenu(Request $request)
    {
        global $companyId;

        $storeId = $request->get('store_id');
        $search = [
            'company' => $companyId,
            'status' => 2,
            'store_id' => $storeId
        ];
        $searchField = [
            'name',         //按名称搜索商品
            'cid',          //按分类搜索商品
            'nav_id',       //按导航搜索
            'long',
            'lat'
        ];

//        $search['store_type'] = [1, 4];

        $desc = explode(',', $request->get('sort_desc'));
        $asc = explode(',', $request->get('sort_asc'));

        foreach ($searchField as $field) {
            if ($request->has($field)) {
                $search[$field] = $request->get($field);
            }
        }

        $goodsBlock = new GoodsBlock();
        $goods = $goodsBlock->goodsList($search, 0, 0, $asc, $desc);

        $navs = StoreNavs::where('stores_id', $storeId)
            ->select('id', 'name')
            ->whereNull('delete_time')
            ->orderBy('sort', 'asc')
            ->get()->toJson();

        $navs = json_decode($navs);

        $sortGoods = [];

        $distance = isset($goods[0]->distance) ? $goods[0]->distance : '';
        if ($navs){
            foreach ($navs as $n) {
                $n->goods = [];
                foreach ($goods as $g) {
                    if ($n->id == $g->store_nav_id) {
                        $n->goods[] = $g;
                        $sortGoods[] = $g;
                    }
                }
            }
        }else{
            $sortGoods = $goods;
        }
        

        return response()->json(ResponseMessage::getInstance()->success([
                'goods'     => $sortGoods,
                'nav'       => $navs,
                'distance'  => $distance
            ]
        )->response());
    }

    /**
     * 店铺排队数据统计
     * @param $storeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeSeatInfo($storeId)
    {
        $total = 0;
        $data = [
            //大桌
            '大桌' => [
                'number' => 0,
                'time' => 0,
            ],
            //中桌
            '中桌' => [
                'number' => 0,
                'time' => 0,
            ],
            //小桌
            '小桌' => [
                'number' => 0,
                'time' => 0,
            ]
        ];

        $storeSeatLiveUp = StoreSeatLineup::where('stores_id', $storeId)
            ->whereNull('delete_time')
            ->where('is_use', 0)
            ->get();

        /**
         * 小桌 4人以下
         * 中卓 10人以下
         * 大桌 10人以上
         */
        foreach ($storeSeatLiveUp as $s) {
            $total++;
            if ($s->people <= 4) {
                $data['小桌']['number']++;
                $data['小桌']['time'] += 5;
            } else if ($s->people <= 10) {
                $data['中桌']['number']++;
                $data['中桌']['time'] += 5;
            } else {
                $data['大桌']['number']++;
                $data['大桌']['time'] += 5;
            }
        }

        return response()->json(ResponseMessage::getInstance()->success(['data' => $data, 'total' => $total])->response());
    }

    /**
     * 排队
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeSeatLineUp(Request $request)
    {
        if (!$request->filled(['store_id', 'people'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $g_uid;
        $storeId = $request->get('store_id');
        $people = $request->get('people');

        /**
         * 一个人一段时间只能排队一次
         */
        $storeSeatLineupModel       = new StoreSeatLineup();
        $userModel                  = new Users();
        $messageModel               = new UserMessage();
        $userLineUp                 = $storeSeatLineupModel->userLineup($g_uid, $storeId);
        $storeModel                 = new Stores();
        $storeInfo                  = $storeModel->adminUserInfo($storeId);

        if ($userLineUp) {
            return response()
                ->json(ResponseMessage::getInstance()
                    ->setFailedCode('USERLINEUP', "该用户已经排过队了")
                    ->failed()
                    ->response());
        }

        /**
         * 小桌 4人以下
         * 中卓 10人以下
         * 大桌 10人以上
         */
        if ($people <= 4) {
            $count = StoreSeatLineup::where('stores_id', $storeId)
                ->where('people', '<=', 4)
                ->whereNull('delete_time')
                ->where('is_use', 0)
                ->count();
            $lineupNumber = 'A' . sprintf("%02d", $count + 1);
        } else if ($people <= 10) {
            $count = StoreSeatLineup::where('stores_id', $storeId)
                ->where('people', '<=', 10)
                ->whereNull('delete_time')
                ->where('is_use', 0)
                ->count();
            $lineupNumber = 'B' . sprintf("%02d", $count + 1);
        } else {
            $count = StoreSeatLineup::where('stores_id', $storeId)
                ->where('people', '>', 10)
                ->whereNull('delete_time')
                ->where('is_use', 0)
                ->count();
            $lineupNumber = 'C' . sprintf("%02d", $count + 1);
        }
        $lineupId = $storeSeatLineupModel->createLineup($g_uid, $storeId, $people, $lineupNumber);

        if ($lineupId) {
            $lineup = new \StdClass;
            $lineup->users_id       = $g_uid;
            $lineup->stores_id      = $storeId;
            $lineup->people         = $people;
            $lineup->lineup_number  = $lineupNumber;
            $lineup->id             = $lineupId;
            $lineup->number         = $count;
            $lineup->time           = $count * 5;

            $messageData = [
                'users_id'      => $g_uid,
                'content'       => "您在{$storeInfo->name}排队成功",
            ];
            $messageModel->saveUserMessage($messageData);

            $userInfo   = $userModel->getUserInfo($g_uid);
            if(!empty($userInfo->reg_id)){
                $push       = new Push();
                $push->lineup($userInfo->reg_id, $storeInfo->name, $lineupId)->push();
                return response()->json(ResponseMessage::getInstance()->success($lineup)->response());
            }

        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取排队信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeSeatLineUpInfo(Request $request)
    {
        global $g_uid;
        $lineupModel    = new StoreSeatLineup();
        $storeId = $request->get('store_id');
        $type = $request->get('type', 'mini');

        $storeSeatLiveUp = $lineupModel->userLineup($g_uid, $storeId);
        /**
         * 小桌 4人以下
         * 中卓 10人以下
         * 大桌 10人以上
         */

        if ($storeSeatLiveUp->people <= 4) {
            $storeSeatLiveUp->seat = "小桌";
        } else if ($storeSeatLiveUp->people <= 10) {
            $storeSeatLiveUp->seat = "中桌";
        } else {
            $storeSeatLiveUp->seat = "大桌";
        }

        /*
         * 预计排队时间计算
         * */
        $count = $lineupModel->getLineupTime($storeId, $storeSeatLiveUp->people);

        $storeSeatLiveUp->number = $count; //前方排队人数
        $storeSeatLiveUp->time = $count * 5;

        if ($storeSeatLiveUp) {
            $qrcodeRedirectUrl = '';
            if ($type === 'mini') {
                $qrcodeRedirectUrl = 'https://www.baidu.com';
            } else if ($type === 'app') {
                $qrcodeRedirectUrl = 'https://www.taobao.com';
            }

            if (!is_dir(public_path() . '/qrcode/lineup')) {
                mkdir(public_path() . '/qrcode/lineup', 0775, true);
            }
            QrCode::format('png')->size(200)->margin(1)->generate($qrcodeRedirectUrl, public_path("qrcode/lineup/{$storeSeatLiveUp->id}.png"));
            $storeSeatLiveUp->qrcode = \Config::get('app.url') . "/qrcode/lineup/{$storeSeatLiveUp->id}.png";
        }
        return response()->json(ResponseMessage::getInstance()->success($storeSeatLiveUp)->response());
    }

    /**
     * 取消排号
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteStoreSeatLineUp(Request $request)
    {
        global $g_uid;
        $id = $request->get('id');

        if (StoreSeatLineup::where('id', $id)
                ->where('users_id', $g_uid)
                ->update(['delete_time' => date('Y-m-d H:i:s', time())]) === false) {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 我的排号信息
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeSeatLineUpList()
    {
        global $g_uid;

        $storeBlock = new StoreBlock();

        $lineup = $storeBlock->storeSeatLineUpList($g_uid);
        return response()->json(ResponseMessage::getInstance()->success($lineup)->response());
    }

    /**
     * 确认预定包厢
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function enterBoxBook(Request $request){
        global $g_uid;
        $seatId     = $request->get('seat_id');
        $couponId   = $request->get('coupon_id', '');
        $storeBlock = new StoreBlock();
        $data = $storeBlock->enterBoxBook($seatId, $couponId, $g_uid);

        if ($data === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('COUPON_ERROR', '未领取该优惠券')->response());
        }else if ($data === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('COUPON_ERROR', '优惠券已过期')->response());
        }else if ($data === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('COUPON_ERROR', '当前消费金额条件不满足优惠条件')->response());
        }else if (!$data){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


    /**
     * 预订
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeBook(Request $request)
    {
        global $g_uid;
        $data = $request->only([
            'stores_id',
            'position',
            'people',
            'book_time',
            'mobile',
            'name',
            'sex',
            'remark',
            'coupon_id',
            'seat_id',
            'activity_id'
        ]);
        $data['users_id'] = $g_uid;

        $storeBlock = new StoreBlock();
        $storeType  = $storeBlock->getStoreType($data['stores_id']);
        $book       = $storeBlock->storeBook($data);


        $formId = $request->get('formId', '');

        if ($book !== false) {
            if (!empty($formId) && $storeType != 3) {
                $userBlock = new UserBlock();
                $store = $storeBlock->storeInfo($data['stores_id'],  $g_uid);
                $positions = $storeBlock->storeSeatPosition($data['stores_id']);
                $position = '';
                foreach ($positions as $po) {
                    if ($data['position'] == $po->id) {
                        $position = $po->name;
                    }
                }


                $kws = ['0' => $data['name'], '1' => $store->name, '2' => $data['book_time'], '3' => $position, '4' => $data['people']];
                $data = $userBlock->smallWxMessage($g_uid, $formId, '7oVxXtyVTxw387MxEAO7eVc9fVkIX7xlqngwDgzt1Ho', $kws);
            }
            return response()->json(ResponseMessage::getInstance()->success($book)->response());

        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取预订详细信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeBookInfo(Request $request)
    {
        if (!$request->filled('book_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $bookId = $request->get('book_id');
        $storeBlock = new StoreBlock();
        $book = $storeBlock->storeBookInfo($bookId);

        if ($book) {
            /**
             * 增加中文映射
             */
            $date = date('m月d日', strtotime($book->book_time));
            $isToday = date('Y-m-d', strtotime($book->book_time)) === date('Y-m-d', time()) ? "(今日)" : '';
            $time = date('H:i', strtotime($book->book_time));
            $data = [
                '人数' => $book->people,
                '日期' => $date . $isToday,
                '时间' => $time,
                '位置' => $book->position_name,
                '预约人' => $book->name,
                '联系方式' => $book->mobile,
                '备注' => $book->remark
            ];
            $book->map = $data;

            return response()->json(ResponseMessage::getInstance()->success($book)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }

    /**
     * 取消预约
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelStoreBook(Request $request)
    {
        global $g_uid;
        $bookId = $request->get('book_id');

        if (
            StoreBooks::where('users_id', $g_uid)
                ->where('id', $bookId)
                ->update([
                    'status' => 2
                ]) === false
        ) {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 我的预订
     * @return \Illuminate\Http\JsonResponse
     */
    public function myStoreBook(Request $request)
    {
        $status     = $request->get('status', '');
        $type       = $request->get('type', '');
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        global $g_uid;

        $storeBlock = new StoreBlock();
        $book       = $storeBlock->storeBookList($g_uid, $type, $page, $pageSize, $status);

        return response()->json(ResponseMessage::getInstance()->success($book)->response());
    }

    /**
     * 预约配置信息
     * @param $storeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeBookConfig($storeId)
    {
        $storeBlock = new StoreBlock();
        $storeBookBlock = new StoreBooksBlock();
        $position = $storeBlock->storeSeatPosition($storeId);

        $token = request('token');
        if ($token == 'test123') {
            $userId = 9;
        } else {
            $userInfo = \Cache::get($token);
            if (isset($userInfo->id)) {
                $userId = $userInfo->id;
            } else {
                $userId = null;
            }
        }


        if ($userId) {
            $userBook = $storeBookBlock->getUserBook($userId, $storeId);
            if ($userBook) {
                $isBook = 1;
            } else {
                $isBook = 0;
            }
        } else {
            $isBook = 0;
        }


        $weekArray = ["日", "一", "二", "三", "四", "五", "六"];

        $bookTime = [[
            'date' => date('Y-m-d', time()),
            'week' => $weekArray[date('w')]
        ]];
        for ($i = 1; $i <= 6; $i++) {
            $date = date('Y-m-d', strtotime("+{$i} day"));
            $bookTime[] = [
                'date' => $date,
                'week' => $weekArray[date('w', strtotime($date))]
            ];
        }

        return response()->json(ResponseMessage::getInstance()->success([
            'position' => $position,
            'book_time' => $bookTime,
            'business_hours' => [
                '上午' => ['00:00', '12:00'],
                '下午' => ['12:00', '24:00']
            ],
            'is_book' => $isBook
        ])->response());
    }

    /**
     * 获取优惠券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeCoupon(Request $request)
    {
        $token = $request->get('token');

        if($token == 'test123'){
            $user_id = 9;
        }else {
            $userInfo = \Cache::get($token);
            if(!isset($userInfo->id)){
                return response()->json(ResponseMessage::getInstance()->failed('NO_LOGIN')->response());
            }
            $user_id = $userInfo->id;
        }

        $storeId = $request->get('store_id');


        $storeCoupon = StoreCoupons::where('stores_id', $storeId)
            ->where('virtual', 1)
            ->where('belong' ,2)
            ->whereNull('delete_time')
            ->get();

        $userCoupon = UserCoupons::where('users_id', $user_id)
            ->where('is_use', 0)
            ->where('end_time', '>', date('Y-m-d H:i:s', time()))
            ->get();
        $userCouponId = [];
        foreach ($userCoupon as $value) {
            $userCouponId[] = $value->coupon_id;
        }

        foreach ($storeCoupon as $item) {
            $item->maturity_time = $item->term_of_validity.'小时';
            $item->full_amount = priceIntToFloat($item->full_amount);
            $item->discount_amount = priceIntToFloat($item->discount_amount);
            if (in_array($item->id, $userCouponId)){
                $item->is_get = 1;
            }else{
                $item->is_get = 0;
            }
        }

        return response()->json(ResponseMessage::getInstance()->success($storeCoupon)->response());
    }

    public function storeIndex(Request $request)
    {
        $storeId = $request->get('store_id');


    }


    /**
     * 商户标签商品
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeTagGoods(Request $request)
    {
//        $storeId        = $request->get('store_id');

        $search = [];
        $searchField = [
            'store_id',
            'tag_id'
        ];

        foreach ($searchField as $field) {
            if ($request->has($field)) {
                $search[$field] = $request->get($field);
            }
        }
        $storeBlock = new StoreBlock();
        $storeGoodsTags = $storeBlock->storeGoodsTags($search);
        return response()->json(ResponseMessage::getInstance()->success($storeGoodsTags)->response());
    }


    /**
     * 获取店铺/商品评论列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function estimatesList(Request $request)
    {
        global $companyId;
        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 10);
        $search = [];
        $searchField = ['store_id', 'goods_id', 'title'];
        foreach ($searchField as $field) {
            if ($request->has($field)) {
                $search[$field] = $request->get($field);
            }
        }

        $storeType = $request->get('store_type', '商城');
        if ($storeType == '餐饮外卖') {
            $search['store_type_id'] = [1];
        } elseif ($storeType === '商城') {
            $search['store_type_id'] = [2];
        }

        $goodsEstimatesBlock = new GoodsEstimatesBlock();
        $goodsEstimates = $goodsEstimatesBlock->estimatesList($search, $page, $pageSize);
        $pagination = $goodsEstimatesBlock->estimatesListPagination($search, $page, $pageSize);
        $score = $goodsEstimatesBlock->estimatesScore($search, $page, $pageSize);
//        $tags                   = $goodsEstimatesBlock->estimatesTags($search);
        return response()->json(ResponseMessage::getInstance()
            ->success([
                'list' => $goodsEstimates,
                'pagination' => $pagination,
                'store_score' => $score
            ])
            ->response());
    }


    /**
     * 获取店铺/商品评论标签
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function estimatesTags(Request $request)
    {
        global $companyId;
        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 10);
        $search = [];
        $searchField = ['store_id', 'goods_id'];
        foreach ($searchField as $field) {
            if ($request->has($field)) {
                $search[$field] = $request->get($field);
            }
        }

        $storeType = $request->get('store_type', '商城');
        if ($storeType == '餐饮外卖') {
            $search['store_type_id'] = [1];
        } elseif ($storeType === '商城') {
            $search['store_type_id'] = [2];
        }

        $goodsEstimatesBlock = new GoodsEstimatesBlock();
        $tags = $goodsEstimatesBlock->estimatesTags($search);
        return response()->json(ResponseMessage::getInstance()->success($tags)->response());
    }


    /**
     * 店铺评论详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function estimatesInfo(Request $request)
    {
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)) {
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search = ['id' => $id];
        $goodsEstimatesBlock = new GoodsEstimatesBlock();
        $goodsEstimates = $goodsEstimatesBlock->estimatesInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($goodsEstimates)->response());
    }


    /**
     * 获取店铺相册列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function imageList(Request $request)
    {
        global $companyId;
        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 10);
        $search = [];
        $searchField = ['store_id'];
        foreach ($searchField as $field) {
            if ($request->has($field)) {
                $search[$field] = $request->get($field);
            }
        }


        $storeImageBlock = new StoreImageBlock();
        $storeImage = $storeImageBlock->imageList($search, $page, $pageSize);
        $pagination = $storeImageBlock->imageListPagination($search, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()
            ->success([
                'list' => $storeImage,
                'pagination' => $pagination
            ])
            ->response());
    }

    /**
     * 菜系列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function cuisineList(){
        $data   = GoodsCuisine::whereNull('delete_time')->get();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


    /**
     * 领券中心，所有商户未过期的优惠券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function couponsCenter(Request $request){
        $token                  = $request->get('token', '');
        if($token == 'test123'){
            $userId = 87;
        }else if (empty($token)){
            $userId = null;
        }else{
            $userInfo = \Cache::get($token);
            $userId = $userInfo->id;
        }
        $page                   = $request->get('page', 1);
        $pageSize               = $request->get('pageSize', 10);
        $searchField            = ['code', 'scope', 'belong'];
        $search                 = [];
        foreach ($searchField as $value) {
            if ($request->filled($value)) {
                $search[$value] = $request->get($value);
            }
        }
        $couponsModel           = new StoreCoupons();
        $data                   = $couponsModel->storeCouponsCenter($page, $pageSize, $userId, $search);

        if (empty($data['list']) && isset($search['code'])){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '未找到该优惠券')->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($data)->response());
        }



    }


    /**
     * 商户座位列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function seatList(Request $request){
        if (!$request->has('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $storeId                = $request->get('stores_id');
        $page                   = $request->get('page', 1);
        $pageSize               = $request->get('pageSize', 10);

        $seatModel              = new StoreSeat();
        $data                   = $seatModel->seatList($page, $pageSize, $storeId);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 休闲娱乐推荐商户
     * @return \Illuminate\Http\JsonResponse
     */
    public function recommendLeisureStore(){
        $storeBlock             = new StoreBlock();
        $recommendStoreModel    = new StoreRecommendLeisure();
        $store      = $recommendStoreModel->getStoreId();
        $storeId    = [];
        foreach ($store as $item) {
            $storeId[] = $item->store_id;
        }

        $data = $storeBlock->storeList(['id' => $storeId]);
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 预定余额支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeBookBalancePay(Request $request){
        global $g_uid;
        if (!$request->filled('book_id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '请选择订单支付')->response());
        }

        if (!$request->filled('pay_password')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '请输入支付密码')->response());
        }

        $bookId         = $request->get('book_id');
        $password       = $request->get('pay_password');
        $userBlock      = new UserBlock();
        $bookBlock      = new StoreBooksBlock();

        $userInfo       = $userBlock->getUserInfo($g_uid);
        if(empty($userInfo->pay_password)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '请设置支付密码')->response());
        }

        if (md5($password) != $userInfo->pay_password){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '密码错误')->response());
        }

        $result = $bookBlock->storeBookBalancePay($bookId, $userInfo->balance);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该订单已支付')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '余额不足')->response());
        }else if ($result === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该订单不需要支付')->response());
        }else if(!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 预定支付宝支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeBookAliPay(Request $request){
        global $g_uid;
        if (!$request->filled('book_id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '请选择订单支付')->response());
        }

        $bookId         = $request->get('book_id');
        $bookBlock      = new StoreBooksBlock();

        $result = $bookBlock->storeBookAliPay($bookId);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该订单已支付')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该订单不需要支付')->response());
        }else if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }
    }

    /**
     * 预订支付宝回调
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Yansongda\Pay\Exceptions\InvalidConfigException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function storeBookAliPayNotify(){
        $aliPay         = Pay::alipay(config('ali-pay.book'));
        $notifyData     = $aliPay->verify();
        $bookBlock      = new StoreBooksBlock();

        $result         = $bookBlock->storeBookAliPayNotify($notifyData);

        if ($result !== false){
            return $aliPay->success();
        }
    }

    /**
     * APP预订微信支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeBookWeChatPay(Request $request){
        if (!$request->filled('book_id')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '请选择订单支付')->response());
        }

        $bookId         = $request->get('book_id');
        $bookBlock      = new StoreBooksBlock();
        $result = $bookBlock->storeBookWeChatPay($bookId);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该订单已支付')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该订单不需要支付')->response());
        }else if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }
    }

    /**
     * APP预订微信支付回调
     * @return mixed
     */
    public function storeBookWeChatPayNotify(){
        $app            = Factory::payment(\Config::get('zhongxin-pay.app'));
        $bookBlock      = new StoreBooksBlock();
        return $bookBlock->storeBookWeChatPayNotify($app);
    }

    /**
     * 预订评论
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bookComment(Request $request){
        if (!$request->filled(['content', 'book_id'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $g_uid;
        $content        = $request->get('content');
        $image          = $request->get('image');
        $bookId         = $request->get('book_id');
        $incognito   = $request->get('is_incognito');

        $storeBlock     = new StoreBlock();
        $result         = $storeBlock->bookComment($g_uid, $bookId, $content, $image, $incognito);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 预订退款
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bookRefund(Request $request){
        if (!$request->filled('book_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        global $g_uid;
        $bookId         = $request->get('book_id');
        $cause          = $request->get('cause');
        $image          = $request->get('image', []);
        $storeBlock     = new StoreBlock();
        $result         = $storeBlock->bookRefund($g_uid, $bookId, $cause, $image);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该订单不存在')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该订单不能退款')->response());
        }else if ($result === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '正在退款中，请耐心等待')->response());
        }else if(!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 获取休闲娱乐商家评论
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreComment(Request $request){
        if (!$request->filled('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $storeId        = $request->get('stores_id');
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 10);
        $storeBlock     = new StoreBlock();
        $data = $storeBlock->getStoreComment($storeId, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取商户活动
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreActivity(Request $request){
        if (!$request->filled('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $storeId        = $request->get('stores_id');
        $activityModel  = new StoreActivity();
        $activity = $activityModel->getStoreActivity($storeId);

        $data = [
            'full_reduction' => [
                'title'     => '',
                'icon'      => 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/icon/157145743050.png'
            ],
            'full_discount' => [
                'title'     => '',
                'icon'      => 'https://ncf-app-images.oss-cn-shenzhen.aliyuncs.com/icon/157145745178.png'
            ]
        ];
        foreach ($activity as $item) {
            $item->data = json_decode($item->data);
            if ($item->type == 'full_reduction'){
                $data['full_reduction']['title'] .= $item->data->title.',';
            }else if ($item->type == 'full_discount'){
                $data['full_discount']['title'] .= $item->data->title.',';
            }
        }


        $data['full_reduction']['title'] = rtrim($data['full_reduction']['title'], ',');
        $data['full_discount']['title'] = rtrim($data['full_discount']['title'], ',');

        if (empty($data['full_reduction']['title'])){
            unset($data['full_reduction']);
        }

        if (empty($data['full_discount']['title'])){
            unset($data['full_discount']);
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取商户详细信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreInfo(Request $request){
        if(!$request->filled('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $storeId        = $request->get('stores_id');
        $storeInfoModel = new StoreInfo();
        $field          = ['address'];
        $data           = $storeInfoModel->getStoreInfo($storeId, $field);
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


}
