<?php

namespace App\Http\Controllers\Pluto;
use App\Blocks\UserBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\WechatSubIds;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use App\Models\WechatOpenplatformToken;
use EasyWeChat\OpenPlatform\Server\Guard;
use function GuzzleHttp\Psr7\build_query;

class WechatController extends Controller {

    /**
     * 小程序登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function loginForMini(Request $request){
        global $wechatId;
        $code = $request->get('code');
        $inviteCode = $request->get('inviteCode',null);

        $app = Factory::miniProgram(\Config::get('wechat.db'));
        $session = $app->auth->session($code);

        \Log::debug($session);
        if(isset($session['errcode'])){
            return response()->json(ResponseMessage::getInstance()->failed()->setFailedCode($session['errcode'], $session['errmsg'])->response());
        }

        if(!$session){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
        $openid = $session['openid'];

        $userBlock = new UserBlock();
        $user = $userBlock->getUserInfoByOpenid($openid);
        if(!$user) {
            $wechatUser = [
                'wechat_base_config_id' => $wechatId,
                'openid' => $openid,
                'origin' => '小程序'
            ];

            $userId = $userBlock->wechatMiniLogin($wechatUser, $inviteCode);
            if(!$userId){
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            $userId = $user->id;
        }

        $session['user_id'] = $userId;
        $userBlock->saveRegId($userId, '', 'mini');

        $token = md5($openid.time());
        \Cache::put($token, $session, 24*60*30);
        return response()->json(ResponseMessage::getInstance()->success(['token' => $token])->response());
    }

    /**
     * 小程序授权更新用户信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \EasyWeChat\Kernel\Exceptions\DecryptException
     */
    public function authorizeForMini(Request $request){
        $token = $request->get('token');
        $encryptedData = $request->get('encryptedData');
        $iv = $request->get('iv');

        $session = (array)\Cache::get($token);

        \Log::debug($session);

        if(!$session){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        $app = Factory::miniProgram(\Config::get('wechat.db'));

        $decryptedData = $app->encryptor->decryptData($session['session_key'], $iv, $encryptedData);

        $userData = [
            'nickname'  => isset($decryptedData['nickName']) ? $decryptedData['nickName'] : null,
            'province'  => isset($decryptedData['province']) ? $decryptedData['province'] : null,
            'city'      => isset($decryptedData['city']) ? $decryptedData['city'] : null,
            'sex'       => isset($decryptedData['gender']) ? $decryptedData['gender'] : null,
            'avatar'    => isset($decryptedData['avatarUrl']) ? $decryptedData['avatarUrl'] : null,
            'mobile'    => isset($decryptedData['mobile']) ? $decryptedData['mobile'] : null
        ];

        $openid = $decryptedData['openId'];
        $userId = $session['user_id'];

        $userBlock = new UserBlock();
        $res = $userBlock->wechatMiniAuthorize($userId, $openid, $userData);

        if($res){
            $userInfo = $userBlock->getUserInfoByOpenid($openid);
            $userInfo->token = $token;
            \Cache::put($token, $userInfo, 24*60*30);
            return response()->json(ResponseMessage::getInstance()->success($userInfo)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    //发起授权
    public function openPlatformPreAuth(){
        $config = config('wechat.open_platform.default');
        $openPlatform = Factory::openPlatform($config);

        $pre = $openPlatform->getPreAuthorizationUrl(env('APP_URL').'/pluto/wechat/open-platform/callback');

        return view('preauth',['url' => $pre]);
    }

    public function openPlatform(){
        $config = \Config::get('wechat.open_platform.default');

        \Log::debug($config);
        $openPlatform = Factory::openPlatform($config);

        $server = $openPlatform->server;

        // 处理授权成功事件
        $server->push(function ($message) use ($openPlatform){
            \Log::debug(2222);
            \Log::debug($message);
            $token = $openPlatform->handleAuthorize($message['AuthorizationCode']);
            \Log::debug($token);
        }, Guard::EVENT_AUTHORIZED);

        // 处理授权更新事件
        $server->push(function ($message) use ($openPlatform){
            \Log::debug(33333);
            \Log::debug($message);
            $token = $openPlatform->handleAuthorize($message['AuthorizationCode']);

            \Log::debug($token);
        }, Guard::EVENT_UPDATE_AUTHORIZED);

        // 处理授权取消事件
        $server->push(function ($message) {
            \Log::debug($message);
            \Log::debug(4444);
        }, Guard::EVENT_UNAUTHORIZED);

        return $server->serve();
    }

    //授权回调
    public function openPlatformPreAuthCallback(Request $request){
        $config = \Config::get('wechat.open_platform.default');
        $openPlatform = Factory::openPlatform($config);
        $res = $openPlatform->handleAuthorize();
        $appid = $res['authorization_info']['authorizer_appid'];
        $refresh_token = $res['authorization_info']['authorizer_refresh_token'];

        if(WechatOpenplatformToken::updateOrCreate([
                'appid' => $appid
            ],
                [
                    'appid' => $appid,
                    'refresh_token' => $refresh_token
                ]) === false){
            return response('授权失败');
        }else{
            return response('授权成功');
        }
    }

    //事件监听
    public function openPlatformCallback($appid,Request $request){
        $wechat = WechatOpenplatformToken::where('appid', $appid)->first();
        if(!$wechat){
            return response('不存在改微信公众号的信息');
        }
        $config = \Config::get('wechat.open_platform.default');
        $openPlatform = Factory::openPlatform($config);
        $wechatApp = $openPlatform->officialAccount($wechat->appid, $wechat->refresh_token);

        $wechatApp->server->push(function ($message) {
            switch ($message['MsgType']) {
                case 'event':
                    return '收到事件消息';
                    break;
                case 'text':
                    return '收到文字消息';
                    break;
                case 'image':
                    return '收到图片消息';
                    break;
                case 'voice':
                    return '收到语音消息';
                    break;
                case 'video':
                    return '收到视频消息';
                    break;
                case 'location':
                    return '收到坐标消息';
                    break;
                case 'link':
                    return '收到链接消息';
                    break;
                case 'file':
                    return '收到文件消息';
                // ... 其它消息
                default:
                    return '收到其它消息';
                    break;
            }

        });

        return $wechatApp->server->serve();
    }

    //登录
    public function openPlatformLogin($appid, Request $request){
        $wechat = WechatOpenplatformToken::where('appid', $appid)->first();
        if(!$wechat){
            return response('该公众号未在微信第三方平台『创研科技事业部』授权');
        }
        $config = \Config::get('wechat.open_platform.default');
        $openPlatform = Factory::openPlatform($config);
        $wechatApp = $openPlatform->officialAccount($wechat->appid, $wechat->refresh_token);

        return $wechatApp->oauth->redirect(env('APP_URL').'/pluto/wechat/open-platform/login/callback/'.$wechat->appid.'?'.build_query($request->all()));
    }

    //登录回调
    public function openPlatformLoginCallback($appid, Request $request){
        $wechat = WechatOpenplatformToken::where('appid', $appid)->first();
        if(!$wechat){
            return response('登录失败');
        }
        $config = \Config::get('wechat.open_platform.default');
        $openPlatform = Factory::openPlatform($config);
        $wechatApp = $openPlatform->officialAccount($wechat->appid, $wechat->refresh_token);

        $userService = $wechatApp->oauth->user();
        $weixinUserInfo = $userService->getOriginal();


        if(!isset($weixinUserInfo['openid'])){
            \Log::debug('不存在openid');
            \Log::debug($weixinUserInfo);
            return response()->json(['code' => 0 , 'msg' => '登录失败']);
        }

        $userService = $wechatApp->user;
        $user = $userService->get($weixinUserInfo['openid']);

        \Log::debug($user);
        $user = (object)$user;

//        if(!$user->subscribe){
//            return redirect("/follow?wid={$appid}");
//        }

        $userData = [];
        $userData['nickname']                   = isset($user->nickname) ? $user->nickname : '';
        $userData['avatar']                     = isset($user->headimgurl) ? $user->headimgurl : '';
        $userData['openid']                     = $user->openid;
        $userData['subscribe_time']             = isset($user->subscribe_time) ? date('Y-m-d H:i:s',$user->subscribe_time) : null;
        $userData['subscribe']                  = isset($user->subscribe) ? $user->subscribe : null;
        $userData['sex']                        = isset($user->sex)? $user->sex : 0 ;
        $userData['city']                       = isset($user->city)? $user->city : 0 ;
        $userData['province']                   = isset($user->province)? $user->province : 0 ;
        $userData['district']                   = isset($user->country)? $user->country : 0 ;
        $userData['wechat_base_config_id']      = $wechat->id;
        $userData['create_time']                = date('Y-m-d H:i:s', time());
        $userData['origin']                     = '公众号';
        $userData['invite_code']                = $request->get('invite_code', null);


        $userBlock = new UserBlock();
        if($userBlock->saveWechatUser($userData)){
            $userInfo = $userBlock->getUserInfoByOpenid($user->openid);
            $token = md5($user->openid .time());
            $userInfo->token = $token;
            \Cache::put($token, $userInfo, 24*60*30);

            if($request->has('redirect')){
                $redirect = $request->get('redirect');
                if($redirect === 'index'){
                    return redirect(env('APP_WEB_URL') . "/?token={$token}&wid={$appid}");
                }elseif($redirect === 'coupon'){
                    return redirect(env('APP_WEB_URL') . "/?token={$token}&wid={$appid}#/pages/my/cardTicket/cardTicket");
                }elseif($redirect === 'plantingGrass'){
                    return redirect(env('APP_WEB_URL') . "/?token={$token}&wid={$appid}#/pages/community/communityIndex/index");
                }elseif($redirect === 'parking'){
                    return redirect(env('APP_WEB_URL') . "/?token={$token}&wid={$appid}#/pages/park_car/index/index");
                }elseif($redirect === 'myCenter'){
                    return redirect(env('APP_WEB_URL') . "/?token={$token}&wid={$appid}#/pages/my/index");
                }
            }

            return response()->json(ResponseMessage::getInstance()->success($userInfo)->response());
        }
    }


    public function openPlatformJsSdk($appid, Request $request){
        $apis = $request->input('apis');
        if($apis){
            $apis_arr = explode(',',$apis);
        }else{
            $apis_arr = ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareWeibo'];
        }

        $wechat = WechatOpenplatformToken::where('appid', $appid)->first();
        if(!$wechat){
            return response('登录失败');
        }
        $config = \Config::get('wechat.open_platform.default');
        $openPlatform = Factory::openPlatform($config);
        $wechatApp = $openPlatform->officialAccount($wechat->appid, $wechat->refresh_token);

        $js = $wechatApp->jssdk;

        $debug = true;
        if($request->has('debug')){
            $debug = $request->input('debug');
        }

        if($request->has('url')) {
            $js->setUrl($request->get('url'));
        }

        $config = $js->buildConfig($apis_arr, $debug, $beta = true, $json = true);
        if(!empty($config)){
            return response()->json(ResponseMessage::getInstance()->success($config)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    public function checkSignature(Request $request)
    {
        $signature = $request->get('signature');
        $timestamp = $request->get('timestamp');
        $nonce = $request->get('nonce');

        $token = 'sendtemplatemsg';
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );

        if($tmpStr == $signature){
            return $request->get('echostr');

        }else{
                return $request->get('echostr');
        }
    }

    /**
        收集小程序提交过来的FormId和PayId
     */
    public function saveFormidOrPayid(Request $request){
        $save_id = $request->get('save_id');
        $type = $request->get('type',0);

        if(WechatSubIds::updateOrCreate(['save_id'=>$save_id,'type'=>$type]) === false){
            return response('保存失败');
        }else{
            return response('保存成功');
        }

    }
}
