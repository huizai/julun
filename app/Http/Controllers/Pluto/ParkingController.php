<?php

namespace App\Http\Controllers\Pluto;

use App\Blocks\PayBlock;
use App\Blocks\UserBlock;
use App\Http\Controllers\Controller;
use App\Libs\Ketuo;
use App\Libs\ResponseMessage;
use App\Models\StoreCoupons;
use App\Models\UserCoupons;
use Illuminate\Http\Request;
use App\Blocks\ParkingBlock;
use Yansongda\Pay\Pay;
use EasyWeChat\Factory;

class ParkingController extends Controller{

    /**
     * 绑定用户车牌号码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function tieNumber(Request $request){
        global $g_uid;
        if (!$request->filled('number')){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('CARD_ERROR','请输入车牌号')
                ->response());
        }
        $number         = $request->get('number');
        $parkingBlock   = new ParkingBlock();
        $result         = $parkingBlock->bindingNumberPlate($g_uid, $number);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('NUMBER_ERROR', '最多只能绑定3个车牌号')
                ->response());
        }else if ($result == false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * 删除车牌号
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteNumber(Request $request){
        $numberId       = $request->get('number_id');
        $parkingBlock   = new ParkingBlock();
        $result         = $parkingBlock->deleteNumber($numberId);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 停车场规则说明
     * @return \Illuminate\Http\JsonResponse
     */
    public function getParkingRule(){
        $parkingBlock   = new ParkingBlock();
        $data           = $parkingBlock->getParkingRule();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取停车场信息和车牌号
     * @return \Illuminate\Http\JsonResponse
     */
    public function getParkingInfo(){
        global $g_uid;
        $parkingLib     = new Ketuo();
        $parkInfo       = $parkingLib->getParkingLotInfo();
        $parkingBlock   = new ParkingBlock();
        $userNumber     = $parkingBlock->getUserNumberPlate($g_uid);
        $data           = [
            'park'      => $parkInfo,
            'number'    => $userNumber,
            'car_total' => count($userNumber)
        ];


        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 缴费记录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function parkingOrderList(Request $request){
        global $g_uid;
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $parkingBlock   = new ParkingBlock();
        $data           = $parkingBlock->parkingOrderList($g_uid, $page, $pageSize);



        return response()->json(ResponseMessage::getInstance()->success($data)->response());

    }

    /**
     * 获取停车费用
     * @param Request $request
     */
    public function getParkingPayment(Request $request){
        global $g_uid;

        $number         = $request->get('number');
        $couponId       = $request->get('coupon_id', '');
        $parkingLib     = new Ketuo();

        $data = $parkingLib->getParkingPaymentInfo($number);
        if (empty($data)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PARKING_ERROR', '暂无停车缴费记录')->response());
        }

        $discountAmount         = 0;            //优惠金额
        $couponInfo             = [];           //优惠券信息
        if (!empty($couponId)){
            $couponModel            = new StoreCoupons();
            $userCouponModel        = new UserCoupons();

            $userCoupon             = $userCouponModel->getUserParkingCoupon($g_uid, 0, $couponId);
            if (!$userCoupon){
                return response()->json(ResponseMessage::getInstance()->failed()
                    ->setFailedCode('COUPON_ERROR', '请领取优惠券')->response());
            }

            $couponInfo             = $couponModel->getCouponInfo($couponId);
            if ($userCoupon->end_time < date('Y-m-d H:i:s', time())){
                return response()->json(ResponseMessage::getInstance()->failed()
                    ->setFailedCode('COUPON_ERROR', '优惠券已过期')->response());
            }
            if ($couponInfo->stores_id !== 0){
                return response()->json(ResponseMessage::getInstance()->failed()
                    ->setFailedCode('COUPON_ERROR', '请使用平台优惠券')->response());
            }

            if ($couponInfo->scope != '停车场'){
                return response()->json(ResponseMessage::getInstance()->failed()
                    ->setFailedCode('COUPON_ERROR', '请使用停车场优惠券')->response());
            }

            $discountAmount = $couponInfo->discount_amount;
            $couponInfo->full_amount        = priceIntToFloat($couponInfo->full_amount);
            $couponInfo->discount_amount    = priceIntToFloat($couponInfo->discount_amount);
        }else{
            $userCouponModel        = new UserCoupons();
            $couponInfo = $userCouponModel->getUserParkingCoupon($g_uid, $data['payable']);
            if ($couponInfo){
                $discountAmount = $couponInfo->discount_amount;
                $couponInfo->full_amount        = priceIntToFloat($couponInfo->full_amount);
                $couponInfo->discount_amount    = priceIntToFloat($couponInfo->discount_amount);
            }else{
                $couponInfo = [];
            }
        }




        $parkingBlock               = new ParkingBlock();


        $data['number']             = $number;
        $data['users_id']           = $g_uid;
        $data['elapsedTime']        = floor($data['elapsedTime']/60).'小时'.($data['elapsedTime']%60).'分钟';
        $data['discount_amount']    = $discountAmount;
        $data['discount_time']      = 0;
        if (!empty($couponInfo) && $data['payable'] > 0){
            $data['coupons_id']     = $couponInfo->id;
            $data['pay_amount']     = $parkingBlock->getPaymentRecharge($data['orderNo'], $discountAmount,0);
        }else{
            $data['coupons_id']     = null;
            $data['pay_amount']     = $data['payable'];
        }

        $parkingBlock->createParkingOrder($data);

        $response = [
            'number'                => $number,
            'users_id'              => $g_uid,
            'elapsedTime'           => $data['elapsedTime'],
            'discount_amount'       => priceIntToFloat($discountAmount),
            'totalAmount'           => priceIntToFloat($data['payable']-$discountAmount) <= priceIntToFloat(0) ? 0 : priceIntToFloat($data['payable']-$discountAmount),
            'payable'               => priceIntToFloat($data['pay_amount']),
            'coupon'                => $couponInfo,
            'orderNo'               => $data['orderNo'],
            'rules'                 => '会员免费3小时'
        ];

        return response()->json(ResponseMessage::getInstance()->success($response)->response());
    }

    /**
     * 停车场支付宝支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function parkingAliPay(Request $request){
        if (!$request->filled('orderNo')){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
        global $g_uid;
        $orderNo        = $request->get('orderNo');
        $parkingBlock   = new ParkingBlock();
        $payAmount      = $parkingBlock->getParkingPayAmount($orderNo);

        $data = [
            'subject'      => '新华联梦想城福街消费',
            'out_trade_no' => $orderNo,
            'total_amount' => priceIntToFloat($payAmount),
        ];
        $aliPayData         = Pay::alipay(config('ali-pay.parking'))->app($data);
        return response()->json(ResponseMessage::getInstance()->success($aliPayData->getContent())->response());
    }

    /**
     * 支付宝回调地址
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Yansongda\Pay\Exceptions\InvalidConfigException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function parkingAliPayNotify(){
        $aliPay         = Pay::alipay(config('ali-pay.parking'));
        $notifyData     = $aliPay->verify();
        $payBlock       = new PayBlock();
        $result         = $payBlock->parkingAliPay($notifyData);
        if ($result){
            return $aliPay->success();
        }

    }

    /**
     * 停车场余额支付
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function parkingRechargePay(Request $request){
        if(!$request->filled('pay_password')){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('PASSWORD_EMPTY', '请输入密码')
                ->failed()->response());
        }

        global $g_uid;
        $orderNo        = $request->get('orderNo');
        $payPassword    = $request->get('pay_password');

        $userBlock      = new UserBlock();
        $parkingBlock   = new ParkingBlock();
        $userInfo       = $userBlock->getUserInfo($g_uid);

        if (empty($userInfo->pay_password)){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PAY_PASSWORD_ERROR', '请设置支付密码')
                ->response());
        }

        if (md5($payPassword) != $userInfo->pay_password){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('PASSWORD_ERROR', '密码错误')
                ->response());
        }

        $parkingInfo = $parkingBlock->getParkingUserPayAmount($orderNo);

        if ($userInfo->balance < $parkingInfo->payable){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('BALANCE_ERROR', '余额不足，请充值')
                ->failed()->response());
        }

        \Log::debug($orderNo.'+'.$parkingInfo->payable.'+'.$g_uid);
        $parkingBlock   = new ParkingBlock();
        $result         = $parkingBlock->parkingRechargePay(
            $orderNo,
            $parkingInfo->payable,
            $g_uid,
            $parkingInfo->discount_money,
            $parkingInfo->discount_time
        );

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }

    /**
     * APP停车场微信支付
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function parkingAppWeChatPay(Request $request){
        if (!$request->filled('orderNo')){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        $orderNo        = $request->get('orderNo');
        $parkingBlock   = new ParkingBlock();
        $payAmount      = $parkingBlock->getParkingPayAmount($orderNo);

        $app = Factory::payment(\Config::get('zhongxin-pay.app'));
        $result = $app->order->unify([
            'body' => '新华联梦想城福街消费',
            'out_trade_no' => $orderNo,
            'total_fee' => 1,
            'notify_url' => env('APP_URL').'/pluto/parking/pay/wechat/notify',
            'trade_type' => 'APP',
        ]);
        \Log::debug($result);
        if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS'){
            $sdk = $app->jssdk->appConfig($result['prepay_id']);
            return response()->json(ResponseMessage::getInstance()->success($sdk)->response());

        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());
    }

    /**
     * APP停车场微信支付回调
     * @return mixed
     */
    public function parkingAppWeChatPayNotify(){
        $app            = Factory::payment(\Config::get('zhongxin-pay.app'));
        $payBlock       = new PayBlock();
        return $payBlock->parkingAppWeChatPayNotify($app);
    }

}
