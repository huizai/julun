<?php
namespace  App\Http\Controllers\Pluto;

use App\Models\PlantingGrassClassify;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Blocks\PlantingGrassBlock;

class PlantingGrassController extends Controller{

    /**
     * 发布种草社区
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addGrass(Request $request){
        global $g_uid;
        $requestData = ['title', 'content', 'goods_id', 'classify'];
        if (!$request->filled($requestData)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $content    = $request->get('content');
        $goodsId    = $request->get('goods_id');
        $title      = $request->get('title');
        $images     = $request->get('images', []);
        $classify   = $request->get('classify');
        if (!is_array($goodsId) || !is_array($images)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        if (count($images) > 5){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $thumbnail = $images[0];

        $grassBlock = new PlantingGrassBlock();
        $result = $grassBlock->addGrass($g_uid, $title, $content, $thumbnail, $goodsId, $images, $classify);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }

    /**
     * 种草社区列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassList(Request $request){
        $g_uid = null;
        if ($request->filled('token')){
            $token = $request->get('token');
            if($token == 'test123'){
                $g_uid = 87;
            }else {
                $userInfo = \Cache::get($token);
                if(!isset($userInfo->id)){
                    return response()->json(ResponseMessage::getInstance()->failed('NO_LOGIN')->response());
                }
                \Cache::put($token, $userInfo, 24*60*365);
                $g_uid = $userInfo->id;
            }
        }
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        $search     = ['title', 'type', 'id', 'user_id', 'classify'];

        $searchData = [];
        $searchData['users_id'] = $g_uid;
        foreach ($search as $value) {
            if ($request->filled($value)){
                $searchData[$value] = $request->get($value);
            }
        }
        $searchData['status'] = $request->get('status', 1);

        $grassBlock = new PlantingGrassBlock();
        $list       = $grassBlock->grassList($page, $pageSize, $searchData);
        $pagination = $grassBlock->grassPagination($page, $pageSize, $searchData);


        return response()->json(ResponseMessage::getInstance()->success(
            [
                'list'          => $list,
                'pagination'    => $pagination
            ]
        )->response());

    }

    /**
     * 种草详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassInfo(Request $request){
        $g_uid = null;
        if ($request->filled('token')){
            $token = $request->get('token');
            if($token == 'test123'){
                $g_uid = 87;
            }else {
                $userInfo = \Cache::get($token);
                if(!isset($userInfo->id)){
                    return response()->json(ResponseMessage::getInstance()->failed('NO_LOGIN')->response());
                }
                \Cache::put($token, $userInfo, 24*60*365);
                $g_uid = $userInfo->id;
            }
        }

        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id         = $request->get('id');
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 20);
        $grassBlock = new PlantingGrassBlock();
        $data = $grassBlock->grassInfo($id, $g_uid, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 发布种草社区评论
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassCommentAdd(Request $request){
        global $g_uid;
        $requestData = ['content', 'id'];
        if (!$request->filled($requestData)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $commentData['planting_grass_id']   = $request->get('id');
        $commentData['content']             = $request->get('content');
        $commentData['users_id']            = $g_uid;
        $grassBlock = new PlantingGrassBlock();
        $result = $grassBlock->grassCommentAdd($commentData);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success('评论成功')->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('COMMENT_ERROR')->response());
        }
    }


    /**
     * 种草社区贴子点赞（取消赞）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassAwesomeSave(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $g_uid;

        $data['planting_grass_id']         = (int)$request->get('id');
        $data['users_id']                   = $g_uid;
        $grassBlock     = new PlantingGrassBlock();
        $result         = $grassBlock->grassAwesomeSave($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 隐藏社区贴子（评论）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassHidden(Request $request){
        if (!$request->filled(['id','type', 'status'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id         = $request->get('id');
        $type       = $request->get('type');
        $status     = $request->get('status');
        $grassBlock = new PlantingGrassBlock();
        $result = $grassBlock->grassHidden($id, $type, $status);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除种草社区贴子（评论）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassDelete(Request $request){
        global $g_uid;
        if (!$request->filled(['id', 'type'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id         = $request->get('id');
        $type       = $request->get('type');
        $grassBlock = new PlantingGrassBlock();
        $result = $grassBlock->grassDelete($id, $type);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 种草社区贴子评论点赞
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassCommentAwesome(Request $request){
        global $g_uid;
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $data['users_id']       = $g_uid;
        $data['comment_id']     = $request->get('id');

        $grassBlock = new PlantingGrassBlock();
        $result     = $grassBlock->grassCommentAwesome($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 种草社区添加收藏
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassCollectionSave(Request $request){
        global $g_uid;
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $data['users_id']               = $g_uid;
        $data['planting_grass_id']      = $request->get('id');

        $grassBlock = new PlantingGrassBlock();
        $result     = $grassBlock->grassCollectionSave($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 删除收藏种草社区贴子
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function grassCollectionDelete(Request $request){
        global $g_uid;
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $data['users_id']         = $g_uid;
        $data['id']               = $request->get('id');

        $grassBlock = new PlantingGrassBlock();
        $result     = $grassBlock->grassCollectionDelete($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 种草关注（取消关注）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userAttention(Request $request){
        if (!$request->has('users_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $g_uid;
        $userId  = $request->get('users_id');
        if ($userId == $g_uid){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '不能关注自己')->response());
        }
        $grassBlock = new PlantingGrassBlock();
        $result = $grassBlock->userAttention($g_uid, $userId);

        if ($result !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 获取种草社区话题分类
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlantingClassify(Request $request){
        $plantingClassifyModel      = new PlantingGrassClassify();
        $data                       = $plantingClassifyModel->getClassify();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

}
