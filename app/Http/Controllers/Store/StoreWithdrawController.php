<?php
namespace App\Http\Controllers\Store;

use App\Libs\ResponseMessage;
use App\Blocks\StoreBlock;
use App\Models\StoreWithdraw;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blocks\StoreWithDrawBlock;
use App\Models\Stores;

class StoreWithdrawController extends Controller{

    /**
     * 提现列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function withdrawList(Request $request){
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 10);
        $search = ['store_id', 'status'];
        $searchData = [];
        foreach ($search as $value) {
            if ($request->has($value)){
                $searchData[$value] = $request->get($value);
            }
        }

        $withdrawModel = new StoreWithDrawBlock();
        $list = $withdrawModel->withdrawList($page, $pageSize, $searchData);
        $pagination = $withdrawModel->withdrawPagination($page, $pageSize, $searchData);

        return response()->json(ResponseMessage::getInstance()->success([
            'list' => $list,
            'pagination' => $pagination
        ])->response());
    }


    /**
     * 修改提现状态
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateWithdrawStatus(Request $request){
        if ($request->post('status') == 1){
            $isPost = ['id','status'];
        }

        if ($request->post('status') == 2){
            $isPost = ['id','status','remarks'];
        }

        if (!$request->filled($isPost)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $data = $request->only($isPost);

        $storeModel     = new Stores();
        $withdrawModel  = new StoreWithdraw();


        if ($data['status'] == 0 ){
            $storeInfo      = $storeModel->adminUserInfo($data['stores_id']);
            if ($storeInfo->balance  < $data['money']){
                return response()->json(ResponseMessage::getInstance()
                    ->failed()
                    ->setFailedCode('INSUFFICIENT_BALANCE','余额不足')->response());
            }


            $withdraw       = $withdrawModel->getStoreWithdraw($data['stores_id']);
            if ($withdraw){
                return response()->json(ResponseMessage::getInstance()
                    ->failed()
                    ->setFailedCode('WITHDRAW_ERROR', '请等待之前的申请审核')->response());
            }

        }

        $withdrawBlock  = new StoreWithDrawBlock();
        $result         = $withdrawBlock->updateWithdrawStatus($data);


        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());
    }

    /**
     * 获取商家每天营业额
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreTurnover(Request $request){
        if (!$request->filled('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $storeId        = $request->get('stores_id');
        $time           = $request->get('time',
            [
                date('Y-m-d', strtotime("-7 day")),
                date('Y-m-d', time())
            ]
        );

        $withdrawBlock  = new StoreWithDrawBlock();
        $data = $withdrawBlock->getStoreTurnover($storeId, $time);
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 申请提现
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applicationWithdraw(Request $request){
        if (!$request->filled(['data', 'stores_id'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $storeId        = $request->get('stores_id');
        $data           = $request->get('data');
        $withdrawBlock  = new StoreWithDrawBlock();
        $result         = $withdrawBlock->applicationWithdraw($storeId, $data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
