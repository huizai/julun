<?php
namespace App\Http\Controllers\Store;

use App\Blocks\MessageBlock;
use App\Libs\ResponseMessage;
use App\Blocks\StoreBlock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blocks\StoreWithDrawBlock;

class StoreMessageController extends Controller{

    /**
     * 消息列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function messageList(Request $request){
        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 10);
        $search = ['store_id'];
        $searchData = [];
        foreach ($search as $value) {
            if ($request->has($value)){
                $searchData[$value] = $request->get($value);
            }
        }

        $messageBlock = new MessageBlock();
        $list = $messageBlock->messageList($searchData, $page, $pageSize);
        $pagination = $messageBlock->messageListPagination($searchData, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success([
            'list' => $list,
            'pagination' => $pagination
        ])->response());
    }

    /**
     * 消息已读
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function messageRead(Request $request){
        $storeId = $request->get('store_id');
        if (!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed("PARAM_ERROR")->response());
        }

        $messageBlock = new MessageBlock();
        $res = $messageBlock->messageRead($storeId);

        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
