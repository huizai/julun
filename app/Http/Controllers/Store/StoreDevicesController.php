<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Store;
use App\Blocks\StoreDevicesBlock;
use App\Models\StoreDevices;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StoreDevicesController extends Controller{
    /**
     * 商户服务列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = ['company' => $companyId];
        $searchField                = ['name', 'store_id'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $storeDevicesBlock = new StoreDevicesBlock();
        $storeDevices   = $storeDevicesBlock->storeDevicesList($search, $page, $pageSize);
        $pagination     = $storeDevicesBlock->storeDevicesListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $storeDevices, 'pagination' => $pagination])->response());
    }

    /**
     * 商户服务详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search                     = ['company' => $companyId, 'id' => $id];
        $storeDevicesBlock = new StoreDevicesBlock();
        $storeDevices = $storeDevicesBlock->storeDevicesInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($storeDevices)->response());
    }


    /**
     * 商户服务修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'id',
            'sort'
        ]);
        $validator = Validator::make($data, [
            'id'           => 'required',
            'name'         => 'required'
        ], [
            'id.required'          => '标签id不能为空！！',
            'name.required'        => '标签名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        //验存  防止标签名重复
        $storeDevicesModel = new StoreDevices();
        $storeDevices = $storeDevicesModel->where('name', $data['name'])->first();
        if ($storeDevices){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "标签名称不能重复")->failed()->response());
        }

        $res = $storeDevicesModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商户服务添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'stores_id',
            'sort'
        ]);
        $validator = Validator::make($data, [
            'stores_id'           => 'required',
            'name'         => 'required'
        ], [
            'stores_id.required'   => '商户id不能为空！！',
            'name.required'        => '标签名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        $data['company_id']     = $companyId;
        //验存  防止标签名重复
        $storeDevicesModel = new StoreDevices();
        $storeDevices = $storeDevicesModel->where('name', $data['name'])->first();
        if ($storeDevices){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "标签名称不能重复")->failed()->response());
        }

        $res = $storeDevicesModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商户服务删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $storeDevicesModel = new StoreDevices();
        $res = $storeDevicesModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }




}
