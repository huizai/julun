<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Store;
use App\Blocks\StoreDevicesBlock;
use App\Blocks\StoreTagsBlock;
use App\Models\StoreDevices;
use App\Models\StoreTags;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StoreTagsController extends Controller{
    /**
     * 商户标签列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['name', 'store_id'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $storeTagsBlock = new StoreTagsBlock();
        $storeTags      = $storeTagsBlock->storeTagsList($search, $page, $pageSize);
        $pagination     = $storeTagsBlock->storeTagsListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $storeTags, 'pagination' => $pagination])->response());
    }

    /**
     * 商户标签详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search                     = ['id' => $id];
        $storeTagsBlock = new StoreTagsBlock();
        $storeTags = $storeTagsBlock->storeTagsInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($storeTags)->response());
    }


    /**
     * 商户标签修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'tag_name',
            'id',
        ]);
        $validator = Validator::make($data, [
            'id'           => 'required',
            'tag_name'         => 'required'
        ], [
            'id.required'          => '标签id不能为空！！',
            'tag_name.required'        => '标签名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        //验存  防止标签名重复
        $storeTagsModel = new StoreTags();
        $storeTags = $storeTagsModel->where('tag_name', $data['tag_name'])->first();
        if ($storeTags){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "标签名称不能重复")->failed()->response());
        }

        $res = $storeTagsModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商户标签添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'tag_name',
            'stores_id',
        ]);
        $validator = Validator::make($data, [
            'stores_id'           => 'required',
            'tag_name'         => 'required'
        ], [
            'stores_id.required'   => '商户id不能为空！！',
            'tag_name.required'        => '标签名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        //验存  防止标签名重复
        $storeTagsModel = new StoreTags();
        $storeTags = $storeTagsModel->where('tag_name', $data['tag_name'])->first();
        if ($storeTags){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "标签名称不能重复")->failed()->response());
        }

        $res = $storeTagsModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商户标签删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $storeTagsModel = new StoreTags();
        $res = $storeTagsModel->destroy($id);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 设置商户标签
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request){
        if (!$request->has(['tag','store_id'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $tag = $request->post('tag');
        $store_id = $request->post('store_id');

        $storeTagBlock = new StoreTagsBlock();
        $result = $storeTagBlock->saveStoreTag($tag, $store_id);

        if($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());

    }




}
