<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Store;
use App\Blocks\StoreDevicesBlock;
use App\Blocks\StoreFansBlock;
use App\Blocks\StoreTagsBlock;
use App\Models\StoreDevices;
use App\Models\StoreTags;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StoreFansController extends Controller{
    /**
     * 商户粉丝列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['name', 'store_id'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $storeFansBlock = new StoreFansBlock();
        $storeFans      = $storeFansBlock->storeFansList($search, $page, $pageSize);
        $pagination     = $storeFansBlock->storeFansListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $storeFans, 'pagination' => $pagination])->response());
    }

    /**
     * 商户粉丝详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search                     = ['id' => $id];
        $storeFansBlock = new StoreFansBlock();
        $storeFans      = $storeFansBlock->storeFansInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($storeFans)->response());
    }






}
