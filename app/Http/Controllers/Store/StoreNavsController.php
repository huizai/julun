<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Store;
use App\Blocks\StoreDevicesBlock;
use App\Blocks\StoreNavsBlock;
use App\Blocks\StoreTagsBlock;
use App\Models\StoreDevices;
use App\Models\StoreNavs;
use App\Models\StoreTags;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StoreNavsController extends Controller{
    /**
     * 商户导航列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['name', 'store_id'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $storeNavBlock  = new StoreNavsBlock();
        $storeNavs      = $storeNavBlock->storeNavsList($search, $page, $pageSize);
        $pagination     = $storeNavBlock->storeNavsListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $storeNavs, 'pagination' => $pagination])->response());
    }


    /**
     * 商户导航修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'sort',
            'id',
        ]);
        $validator = Validator::make($data, [
            'id'           => 'required',
            'name'         => 'required'
        ], [
            'id.required'          => '导航id不能为空！！',
            'name.required'        => '导航名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        //验存  防止导航名重复
        $storeNavsModel = new StoreNavs();

        $res = $storeNavsModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商户导航添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'sort',
            'stores_id',
        ]);
        $validator = Validator::make($data, [
            'stores_id'           => 'required',
            'name'                => 'required'
        ], [
            'stores_id.required'   => '商户id不能为空！！',
            'name.required'        => '导航名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        $storeNavsModel = new StoreNavs();
        $res = $storeNavsModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商户导航删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $storeNavsModel = new StoreNavs();
        $res = $storeNavsModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }





}
