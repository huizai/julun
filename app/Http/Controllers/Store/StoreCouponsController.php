<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Store;
use App\Blocks\StoreCouponsBlock;
use App\Models\StoreCoupons;
use App\Models\UserCoupons;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use QrCode;

class StoreCouponsController extends Controller{
    /**
     * 优惠券列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = ['company'=>$companyId];
        $searchField                = ['name', 'store_id', 'scope', 'admins_id'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $storeCouponsBlock  = new StoreCouponsBlock();
        $storeCoupons       = $storeCouponsBlock->storeCouponsList($search, $page, $pageSize);
        foreach ($storeCoupons as $value) {
            $value->full_amount = priceIntToFloat($value->full_amount);
            $value->discount_amount = priceIntToFloat($value->discount_amount);
            $value->selling_price = json_decode($value->selling_price, true);
            $value->recharge_amount = priceIntToFloat($value->recharge_amount);
            if ($value->scope == '抽奖'){
                $value->odds = priceIntToFloat($value->odds);
            }
        }
        $pagination         = $storeCouponsBlock->storeCouponsListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $storeCoupons, 'pagination' => $pagination])->response());
    }

    /**
     * 优惠券详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search             = ['id' => $id];
        $storeCouponsBlock  = new StoreCouponsBlock();
        $storeCoupons       = $storeCouponsBlock->StoreCouponsInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($storeCoupons)->response());
    }


    /**
     * 优惠券修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'term_of_validity',
            'quantity',
            'id'
        ]);
        $validator = Validator::make($data, [
            'term_of_validity'      => 'required',
            'quantity'              => 'required',
            'name'                  => 'required',
            'id'                    => 'required'
        ], [
            'term_of_validity.required'     => '请输入有效期限',
            'quantity.required'             => '请输入优惠券数量',
            'name.required'                 => '请输入优惠券名称',
            'id.required'                   => '请选择优惠券'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        $data['company_id']             = $companyId;
        $data['full_amount']            = $request->get('full_amount', 0) * 100;
        $data['discount_amount']        = $request->get('discount_amount', 0) * 100;
        $data['create_time']            = date('Y-m-d H:i:s', time());
        $data['scope']                  = $request->get('scope', '');
        $data['virtual']                = $request->get('virtual', 1);
        $data['type']                   = $request->get('type', '');
        $data['stores_id']              = $request->get('stores_id', 0);
        $data['belong']                 = $request->get('belong', 1);
        $data['address']                = $request->get('address', '');
        $data['selling_price']          = $request->get('selling_price', 0);
        $data['odds']                   = $request->get('odds', null);
        $data['recharge_amount']        = $request->get('recharge_amount', 0);
        $data['rule']                   = $request->get('rule', '');

        $storeCouponsModel              = new StoreCoupons();
        if (!empty($data['scope'])){
            if ($data['scope'] == '新人首单红包'){
                $coupon = $storeCouponsModel->getScopeCoupon($data['scope']);
                if (!empty($coupon)){
                    return response()->json(ResponseMessage::getInstance()->failed()
                        ->setFailedCode('ERROR', '只能添加一张新人首单红包')->response());
                }
            }

            if ($data['scope'] == '抽奖'){
                if (empty($data['odds'])){
                    return response()->json(ResponseMessage::getInstance()->failed()
                        ->setFailedCode('ERROR', '请设置中奖概率')->response());
                }
                $data['odds']  = $data['odds'] * 100;

                $coupon = $storeCouponsModel->getScopeCoupon($data['scope']);
                $couponTotal = count($coupon);
                foreach ($coupon as $item) {
                    if ($item->id == $data['id']){
                        $total = 7;
                        break;
                    }else{
                        $total = 8;
                    }
                }
                if ($couponTotal >= $total){
                    return response()->json(ResponseMessage::getInstance()->failed()
                        ->setFailedCode('ERROR', '抽奖券最多只能设置8张')->response());
                }
            }

            if ($data['scope'] == '余额'){
                if (!empty($data['scope']) && $data['scope'] == '余额'){
                    if ($data['quantity']  > 1){
                        return response()->json(ResponseMessage::getInstance()->failed()
                            ->setFailedCode('ERROR', '余额兑换券数量只能设置为1')->response());
                    }
                    $data['code']       = getSalt(12);
                }
            }

            if ($data['scope'] == '充值'){
                $data['recharge_amount'] = $data['recharge_amount'] * 100;
            }

        }

        $res = $storeCouponsModel->updateCoupon($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 优惠券添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'name',
            'term_of_validity',
            'quantity'
        ]);
        $validator = Validator::make($data, [
            'term_of_validity'      => 'required',
            'quantity'              => 'required',
            'name'                  => 'required'
        ], [
            'term_of_validity.required'     => '请输入有效期限',
            'quantity.required'             => '请输入优惠券数量',
            'name.required'                 => '请输入优惠券名称'
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        $data['company_id']             = $companyId;
        $data['full_amount']            = $request->get('full_amount', 0) * 100;
        $data['discount_amount']        = $request->get('discount_amount', 0) * 100;
        $data['create_time']            = date('Y-m-d H:i:s', time());
        $data['scope']                  = $request->get('scope', '');
        $data['virtual']                = $request->get('virtual', 1);
        $data['type']                   = $request->get('type', '');
        $data['stores_id']              = $request->get('stores_id', 0);
        $data['belong']                 = $request->get('belong', 1);
        $data['address']                = $request->get('address', '');
        $data['selling_price']          = $request->get('selling_price', 0);
        $data['odds']                   = $request->get('odds', null);
        $data['recharge_amount']        = $request->get('recharge_amount', 0);
        $data['rule']                   = $request->get('rule', '');
        $data['admins_id']              = $request->get('admins_id', '');

        $storeCouponsModel  = new StoreCoupons();
        if (!empty($data['scope'])){
            if ($data['scope'] == '新人首单红包'){
                $coupon = $storeCouponsModel->getScopeCoupon($data['scope']);
                if (!empty($coupon)){
                    return response()->json(ResponseMessage::getInstance()->failed()
                        ->setFailedCode('ERROR', '只能添加一张新人首单红包')->response());
                }
            }

            if ($data['scope'] == '抽奖'){
                if (empty($data['odds'])){
                    return response()->json(ResponseMessage::getInstance()->failed()
                        ->setFailedCode('ERROR', '请设置中奖概率')->response());
                }
                $data['odds']  = $data['odds'] * 100;

                $scopeTotal = count($storeCouponsModel->getScopeCoupon($data['scope']));
                if ($scopeTotal >= 8){
                    return response()->json(ResponseMessage::getInstance()->failed()
                        ->setFailedCode('ERROR', '抽奖券最多只能设置8张')->response());
                }
            }

            if ($data['scope'] == '余额'){
                if ($data['quantity']  > 1){
                    return response()->json(ResponseMessage::getInstance()->failed()
                        ->setFailedCode('ERROR', '余额兑换券数量只能设置为1')->response());
                }
                $data['code']       = getSalt(12);
            }

            if ($data['scope'] == '充值'){
                $data['recharge_amount'] = $data['recharge_amount'] * 100;
            }
        }

        $res = $storeCouponsModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 优惠券删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $StoreCouponsModel = new StoreCoupons();
        $res = $StoreCouponsModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 点赞优惠券生成二维码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateQrCode(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id                 = $request->get('id');
        $storeCouponsModel  = new StoreCoupons();
        $couponInfo         = $storeCouponsModel->getCouponInfo($id);

        if ($couponInfo->scope != '积赞送券'){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '只有积赞送券优惠券能生成二维码')->response());
        }

        if (!is_dir(public_path().'/qrcode/coupon')){
            @mkdir(public_path().'/qrcode/coupon', 0755);
        }
        $path = public_path().'/qrcode/coupon/store';
        if (!is_dir($path)){
            @mkdir($path, 0755);
        }
        $imageName = $id.'.png';
        $code = getSalt(12);
        $data = [
            'redirect_type'         => 'native',
            'path'                  => '',
            'params'                => [
                'code'              => $code
            ]
        ];
        $codeResult = QrCode::format('png')
            ->size(400)
            ->encoding('UTF-8')
            ->generate(
                json_encode($data),
                "{$path}/$imageName"
            );


        if ($codeResult){
            $url = env('APP_URL').'/qrcode/coupon/store/'.$imageName;
            if($storeCouponsModel->updateCoupon([
                'id'        => $id,
                'code'      => $code,
                'qr_code'   => $url
            ])){
                return response()->json(ResponseMessage::getInstance()->success($url)->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }



    }

    /**
     * 获取用户已使用的平台优惠券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserUseCoupon(Request $request){
        $page               = $request->get('page', 1);
        $pageSize           = $request->get('pageSize', 10);
        $search             = [
            'stores_id'     => $request->get('stores_id', ''),
            'mobile'        => $request->get('mobile', ''),
            'is_use'        => $request->get('is_use', 1),
            'is_check'      => $request->get('is_check', 0)
        ];

        $userCouponUseModel = new UserCoupons();
        $data = $userCouponUseModel->getUserUseCoupon($search, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 核销用户已使用优惠券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkUserCoupon(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id                 = $request->get('id');
        $userCouponUseModel = new UserCoupons();
        $result             = $userCouponUseModel->checkUserCoupon($id);
        if ($result['code'] == 'success'){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', $result['message'])->response());
        }
    }

    /**
     * 获取当前店铺用户未使用的线下优惠券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserKindCoupon(Request $request){
        if (!$request->filled('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $userCouponModel        = new UserCoupons();
        $page                   = $request->get('page', 1);
        $pageSize               = $request->get('pageSize', 10);
        $search                 = [];
        $search['stores_id']    = $request->get('stores_id');
        $search['code']         = $request->get('code', '');
        $search['mobile']       = $request->get('mobile', '');
        $data   = $userCouponModel->getUserKindCoupon($search, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 兑换线下优惠券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userUserKindCoupon(Request $request){
        $code                   = $request->get('code');
        $id                     = $request->get('id');
        $userCouponModel        = new UserCoupons();
        $result = $userCouponModel->userUserKindCoupon($id, $code);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '未找到该优惠券')->response());
        }else if ($result === -2){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '优惠券已过期')->response());
        }else if ($result === -3){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('ERROR', '该优惠券已使用')->response());
        }else if ($result === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

}
