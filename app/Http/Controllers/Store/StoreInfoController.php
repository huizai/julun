<?php
/**
 * Created by PhpStorm.
 * User: m1507
 * Date: 2019/4/9
 * Time: 15:29
 */

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use App\Models\Stores;
use Illuminate\Http\Request;
use App\Http\Requests\StoreInfoRequest;
use App\Blocks\StoreBlock;
use App\Libs\ResponseMessage;
use Illuminate\Support\Facades\Validator;

class StoreInfoController extends Controller
{
    public function setStoreMsg(StoreInfoRequest $request){
//        官方图片
//        地址
//        营业时间
//        广告
//        商家介绍
//        商家标签
//        店铺图片
//        营业许可证
        global $companyId;
        global $storeId;
        $data = $request->only(['address','long','lat','business_license','food_license','am','pm','introduce','tags','image']);
        $storeBlock = new StoreBlock();
        $res = $storeBlock->storeInfoMsg($storeId,$data);
        if (is_int($res)){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('UPDATE_EXIST')->response());
        }


    }

    /**
     * 商户信息修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setStore(Request $request){
        global $companyId;
        $data = $request->only([
            'id',
            'name',
            'logo',
            'background',
//            'freight_type',
            'synopsis',
        ]);
        $validator = Validator::make($data, [
            'id'                => 'required',
            'name'              => 'required',
            'logo'              => 'required',
            'background'        => 'required',
//            'freight_type'      => 'required',
            'synopsis'          => 'required',

        ], [
            'id.required'               => '店铺id不能为空！！',
            'name.required'             => '店铺名称不能为空！！',
            'logo.required'             => '店铺logo不能为空！！',
            'background.required'       => '店铺店招不能为空！！',
//            'freight_type.required'     => '店铺运费模式不能为空',
            'synopsis.required'         => '店铺简介不能为空！！',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        $storeModel = new Stores();

        $res = $storeModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商户供货平台运费设置
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setFreight(Request $request){
        global $companyId;
        $data = $request->only([
            'id',
            'freight_type',
        ]);
        $validator = Validator::make($data, [
            'id'                => 'required',
            'freight_type'      => 'required'
        ], [
            'id.required'               => '店铺id不能为空！！',
            'freight_type.required'     => '店铺运费模式不能为空'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        $storeModel = new Stores();

        $res = $storeModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}