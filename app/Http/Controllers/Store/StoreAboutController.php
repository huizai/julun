<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Store;
use App\Blocks\StoreAboutBlock;
use App\Models\StoreAbout;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StoreAboutController extends Controller{
    /**
     * 商户介绍列表（待定）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['name', 'store_id'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $storeAboutBlock  = new StoreAboutBlock();
        $storeAbout       = $storeAboutBlock->storeAboutList($search, $page, $pageSize);
        $pagination       = $storeAboutBlock->storeAboutListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $storeAbout, 'pagination' => $pagination])->response());
    }


    /**
     * 商户介绍设置
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setAbout(Request $request){
        global $companyId;
        $data = $request->only(['title', 'store_id', 'content', 'image']);

        $validator = Validator::make($data, [
            'title'             => 'required',
            'content'           => 'required',
            'image'             => 'required',
            'store_id'          => 'required'
        ], [
            'title.required'        => '标题不能为空！！',
            'content.required'      => '内容不能为空！！',
            'image.required'        => '图片不能为空！！',
            'store_id.required'     => '店铺id不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        $storeAboutModel = new StoreAbout();
        $about = $storeAboutModel::where('store_id', $data['store_id'])->orderBy('id','desc')->first();

        if (!isset($about->id)){
            $res = $storeAboutModel::create($data);
        }else{
            $res = $storeAboutModel::where('id', $about->id)->update($data);
        }
        if ($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('CREATE_EXIST')->response());
        }
    }

    /**
     * 商户介绍详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function aboutInfo(Request $request){
        global $companyId;

        $storeId = $request->get('store_id');
        if(!isset($storeId)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search['store_id'] = $storeId;
        $storeAboutBlock  = new StoreAboutBlock();
        $storeAbout       = $storeAboutBlock->storeAboutInfo($search);
//        $res = [
//            'store_id'  => $storeAbout->store_id,
//            'title'     => $storeAbout->title,
//            'content'   => $storeAbout->content,
//            'image'     => $storeAbout->image
//        ];
        return response()->json(ResponseMessage::getInstance()->success($storeAbout)->response());

    }






}
