<?php

namespace App\Http\Controllers\Store;

use App\Blocks\StoreBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\Stores;
use App\Http\Requests\AdminRequest;
use App\Models\StoreSeatPosition;
use Illuminate\Http\Request;
use App\Libs\YunXin\ServerApi;
use App\Models\StoreYunxin;

class StoreController extends Controller{

    /**
     * 后台登陆
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(AdminRequest $request){
        $data = $request->only(['username','password', 'reg_id']);
        $data['password'] = md5($data['password']);
        $data['agent_type'] = $request->get('agent_type');
        $Store = new Stores();
        $store = $Store::where('mobile','=',$data['username'])->whereNull('delete_time')->first();
        $account = $Store::where('account', $data['username'])->whereNull('delete_time')->first();
        if (!$store){
            $store = $account;
            if (!$store){
                return response()->json(ResponseMessage::getInstance()->failed("NO_USER")->response());
            }

        }
        if ($data['password'] != $store->password){
            return response()->json(ResponseMessage::getInstance()->failed("PASSWORD_ERROR")->response());
        }
        if ($store->agent_type != $data['agent_type'] && isset($data['agent_type'])){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('RANK_ERROR', "身份信息有误")
                ->failed()
                ->response());
        }

        if (isset($data['reg_id']) && !empty($data['reg_id'])){
            $Store::where('id', $store->id)->update(['reg_id' => $data['reg_id']]);
        }
        $session['mobile']  = session('mobile',$store['mobile']);
        $session['loginTime'] = session('loginTime',time());
        $session['company_id']= session('company_id',$store['company_id']);
        $session['store_id']  = session('store_id',$store['id']);
        $token = md5($session['mobile'].$session['loginTime']);
        \Cache::put($token, $session, 24*60*30);
        $res = [
            'name'                  => $store->name,
            'company_id'            => $store->company_id,
            'classifity_id'         => $store->classifity_id,
            'id'                    => $store->id,
            'logo'                  => $store->logo,
            'agent_type'            => $store->agent_type,
            'type'                  => $store->agent_type == 0 ? 'mill':'store',
            'status'                => $store->status,
            'token'                 => $token,
            'store_type_id'         => $store->store_type_id
        ];

        $yunXin             = new ServerApi();
        $storeYunXinModel   = new StoreYunxin();
        $storeYunXinInfo    = $storeYunXinModel->getStoreYunXinInfo($store->id);
        $yunXinInfo         = $yunXin->updateUserToken($storeYunXinInfo->accid);

        if ($yunXinInfo['code'] == 200){
            if($storeYunXinModel->updateStoreYunXinInfo($store->id, $storeYunXinInfo->accid, $yunXinInfo['info']['token']) !== false){
                $res['yunxin_token'] = $yunXinInfo['info']['token'];
                $res['accid'] = $storeYunXinInfo->accid;
            }
        }

        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }


    /**
     * 修改登录密码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request){
        $data = $request->only([
            'code',
            'mobile',
            'password',
            're_password'
        ]);

        if (\Cache::get($data['mobile'])!==$data['code']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("ERROR",'验证码错误')->failed()->response());
        }

        if ($data['password']!==$data['re_password']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("ERROR",'两次密码不一致')->failed()->response());
        }

        $data['password'] = md5($data['password']);
        $Store = new Stores();
        $res = $Store::where('mobile', $data['mobile'])->update(['password' => $data['password']]);
        if ($res!==false){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }


    /**
     * 获取店铺座位位置分布
     * @return \Illuminate\Http\JsonResponse
     */
    public function seatPosition(){
        $classify = StoreSeatPosition::get();
        return response()->json(ResponseMessage::getInstance()->success($classify)->response());
    }

    /**
     * 获取商家类型
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreType(Request $request){

        if(!$request->has('store_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->get('store_id');
        if (empty($id)){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        $type = Stores::where('id',$id)->value('store_type_id');
        return response()->json(ResponseMessage::getInstance()->success($type)->response());
    }


    /**
     * 商户注册
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentReg(Request $request){
        $data = $request->only([
            'code',
            'mobile',

        ]);

        $data['password'] = $request->get('password', '123456');
        $data['re_password'] = $request->get('re_password', '123456');
        if ($data['password'] != $data['re_password'] ){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("REG_EXIST",'两次密码不一致')->failed()->response());
        }
        unset($data['re_password']);
        $data['password']   = md5($data['password']);
        $data['name']       = "lansiu-".uniqid();
        $data['account']    = date('Ymd').mt_rand(999, 9999);
        $data['logo']       = "https://img.alicdn.com/imgextra/i1/2880673396/O1CN01GmOzdU1axPXV2BSOr_!!2880673396.jpg_310x310.jpg";
        $data['company_id'] = $request->get('company', 1);
        $data['roles_id']   = 4;//基础角色的角色id如果删除需要变更这里的角色分配
        $data['agent_type'] = $request->get('agent_type', 1);;
        $storeModel = new Stores();

        if (\Cache::get($data['mobile'])!==$data['code']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("REG_EXIST",'验证码错误')->failed()->response());
        }
//        验存
        $store = $storeModel::where('mobile', $data['mobile'])->first();
        if (isset($store->id)){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("REG_EXIST",'此手机号已被注册')->failed()->response());
        }
        unset($data['code']);
        $res = $storeModel->saveStoreUser($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode("REG_EXIST",'注册失败')->failed()->response());
        }

    }

    /**
     * 商户开启营业
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setOperate(Request $request){
        if(!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id     = $request->get('id');
        $status = $request->get('status', 0);
        $res = Stores::where('id', $id)->update(['operate' => $status]);
        if ($res!==false){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 首页数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexPage(Request $request){
        $position = $request->get('position', '批发商城');
        $storeBlock = new StoreBlock();
        $res = $storeBlock->indexPage($position);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }


    /**
     * 个人中心数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function personal(Request $request){
        $storeId = $request->get('store_id');
        $storeBlock = new StoreBlock();
        $res = $storeBlock->personal($storeId);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }

    /**
     * 手机号验证码登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function loginByVc(Request $request){
        $mobile = $request->get('mobile');
        $agentType = $request->get('agent_type');
        if(!isMobileNum($mobile)){
            return response()->json(ResponseMessage::getInstance()->failed('MOBILE_FORMAT_ERROR')->response());
        }

        $validateCode = $request->get('validate_code');

        $cacheCode = \Cache::get($mobile);

        if($validateCode != $cacheCode){
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        $StoreModel = new Stores();
        $store = $StoreModel::where('mobile', $mobile)->first();
        if(!isset($store->id)){
            return response()->json(ResponseMessage::getInstance()->failed('NO_USER')->response());
        }

        if ($store->agent_type != $agentType && isset($agentType)){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('RANK_ERROR', "身份信息有误")
                ->failed()
                ->response());
        }

        $session['mobile']  = session('mobile', $store['mobile']);
        $session['loginTime'] = session('loginTime', time());
        $session['company_id']= session('company_id', $store['company_id']);
        $session['store_id']  = session('store_id', $store['id']);
//        dd($session);
        $token = md5($session['mobile'].$session['loginTime']);
        \Cache::put($token, $session, 24*60*30);
        $res = [
            'name'=>$store->name,
            'company_id'=>$store->company_id,
            'classifity_id'=>$store->classifity_id,
            'id'=>$store->id,
            'logo'=>$store->logo,
            'agent_type'=>$store->agent_type,
            'type'=>$store->agent_type==0 ? 'mill':'store',
            'status'=>$store->status,
            'token'=>$token
        ];
        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }

    /**
     * 找回密码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function passwordByVc(Request $request){
        $data = $request->only(['mobile', 'validate_code', 'password', 're_password']);
        if(!isMobileNum($data['mobile'])){
            return response()->json(ResponseMessage::getInstance()->failed('MOBILE_FORMAT_ERROR')->response());
        }

        $cacheCode = \Cache::get($data['mobile']);

        if($data['validate_code'] != $cacheCode){
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        if($data['password'] != $data['re_password']){
            return response()->json(ResponseMessage::getInstance()->failed('PASSWORD_ERROR')->response());
        }

        $StoreBlock = new StoreBlock();
        $res = $StoreBlock->passwordByVc($data);
        if ($res === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }

    }

    /**
     * 获取店铺营业额
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreTurnover(Request $request){
        if (!$request->filled('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $storeId        = $request->get('stores_id');
        $storeBlock     = new StoreBlock();
        $data           = $storeBlock->getStoreTurnover($storeId);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 生成商家二维码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateStoreQrCode(Request $request){
        if(!$request->filled('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $storeId        = $request->get('stores_id');
        $storeBlock     = new StoreBlock();
        $result         = $storeBlock->generateStoreQrCode($storeId);

        if ($result){
            return response()->jsoN(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->jsoN(ResponseMessage::getInstance()->failed()->response());
        }
    }

}
