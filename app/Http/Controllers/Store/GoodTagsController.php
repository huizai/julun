<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Store;
use App\Blocks\GoodTagsBlock;
use App\Blocks\StoreDevicesBlock;
use App\Blocks\StoreTagsBlock;
use App\Models\GoodsTags;
use App\Models\StoreDevices;
use App\Models\StoreTags;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class GoodTagsController extends Controller{
    /**
     * 商品标签列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['name', 'store_id'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $goodTagsBlock  = new GoodTagsBlock();
        $goodTags       = $goodTagsBlock->goodTagsList($search, $page, $pageSize);
        $pagination     = $goodTagsBlock->goodTagsListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $goodTags, 'pagination' => $pagination])->response());
    }

    /**
     * 商品标签详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search                     = ['id' => $id];
        $goodTagsBlock  = new GoodTagsBlock();
        $goodTags       = $goodTagsBlock->goodTagsInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($goodTags)->response());
    }


    /**
     * 商品标签修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $storeId = $request->post('store_id');
        $data = $request->only([
            'tag_name',
            'id',
            'mr_name'
        ]);
        $validator = Validator::make($data, [
            'id'                => 'required',
            'tag_name'          => 'required',
            'mr_name'           => 'required'
        ], [
            'id.required'               => '标签id不能为空！！',
            'tag_name.required'         => '标签名称不能为空！！',
            'mr_name.required'          => '标签英文名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        //验存  防止标签名重复
        $goodTagsModel = new GoodsTags();
        $goodTags = $goodTagsModel
            ->where('stores_id', $storeId)
            ->where('tag_name', $data['tag_name'])
            ->first();

        if ($goodTags && $goodTags->id != $data['id']){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "标签名称不能重复")->failed()->response());
        }

        $res = $goodTagsModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商品标签添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;

        $data = $request->only([
            'tag_name',
            'stores_id',
            'mr_name'
        ]);

        $validator = Validator::make($data, [
            'stores_id'         => 'required',
            'tag_name'          => 'required',
            'mr_name'           => 'required'
        ], [
            'stores_id.required'        => '商户不能为空！！',
            'tag_name.required'         => '标签名称不能为空！！',
            'mr_name.required'          => '标签英文名称不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        //验存  防止标签名重复
        $goodTagsModel = new GoodsTags();
        $goodTags = $goodTagsModel
            ->where('stores_id', $data['stores_id'])
            ->where('tag_name', $data['tag_name'])
            ->first();
        if ($goodTags){
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode('CREATEEXIT', "标签名称不能重复")->failed()->response());
        }

        $res = $goodTagsModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商品标签删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $goodTagsModel = new GoodsTags();
        $res = $goodTagsModel->destroy($id);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
