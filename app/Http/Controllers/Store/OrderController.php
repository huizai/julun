<?php

namespace App\Http\Controllers\Store;

use App\Blocks\OrderBlock;
use App\Blocks\UserBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\Stores;
use App\Models\UserMessage;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Models\Orders;
use App\Http\Requests\OrderSendRequest;
use App\Models\ExpressDelivery as ED;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Tool\JiGuangPushController as Push;
use Illuminate\Support\Facades\Redis;


class OrderController extends Controller{
    /**
     * 获取订单列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        if (!$request->has('store_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search         = ['company' => $companyId];
        $searchField    = ['order_sn', 'online', 'status', 'time', 'pay_type', 'search_store_id'];


        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $search['store_id'] = $request->get('store_id');


        $Order = new Orders();
        $list = $Order->orderList($search, $page, $pageSize);
//        $pagination = $Order->orderListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }

    /**
     * 后台订单详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderInfo(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id             = $request->get('id');
        $orderBlock     = new OrderBlock();
        $data           = $orderBlock->getOrderInfo($id);
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }


    /**
     * 订单发货
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(OrderSendRequest $request){
        global $companyId;

        $data = $request->only(['id','express','expresssn','store_id']);
        $data['stores_id'] = $data['store_id'];
        unset($data['store_id']);
        //验存
        $orderModel         = new Orders();
        $storeModel         = new Stores();
        $userModel          = new Users();
        $userMessageModel   = new UserMessage();


        $order = DB::table($orderModel->getTable().' as o')
            ->select('o.id', 's.name', 'o.users_id', 'u.reg_id')
            ->leftJoin($storeModel->getTable().' as s', 's.id', '=', 'o.stores_id')
            ->leftJoin($userModel->getTable().' as u', 'u.id', '=', 'o.users_id')
            ->where('o.stores_id',$data['stores_id'])
            ->where('o.status',1)
            ->where('o.id', $data['id'])
            ->first();

        if (!$order){
            return response()->json(ResponseMessage::getInstance()->failed('DATA_EXIST')->response());
        }

        $data['status']=2;
        $res = DB::table($orderModel->getTable())
            ->where('id', $data['id'])
            ->where('company_id',$companyId)
            ->where('stores_id',$data['stores_id'])
            ->where('status',1)
            ->update($data);

        if ($res){
            DB::table($userMessageModel->getTable())
                ->insert([
                    'users_id'      => $order->users_id,
                    'content'       => "您在【{$order->name}】购买的商品已发货"
                ]);

            if (!empty($order->reg_id)){
                $push = new Push();
                $pushResult = $push->Ship($order->reg_id, $data['id'])->push();
                \Log::debug(\GuzzleHttp\json_encode($pushResult));
            }
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("UPDATE_EXIST")->response());
        }
    }

    /**
     * 订单备注
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remake(Request $request){
        global $companyId;
        global $storeId;
        $data = $request->only(['id','remake']);
        if (!isset($data['id'])||!isset($data['remake'])){
            return response()->json(ResponseMessage::getInstance()->failed("PARAM_ERROR")->response());
        }
        //验存
        $Order = new Orders();
        $order = $Order::where('company_id',$companyId)->where('stores_id',$storeId)->find($data['id'])->toArray();
        if (!$order){
            return response()->json(ResponseMessage::getInstance()->failed('DATA_EXIST')->response());
        }
        $res = $Order::where('id', $data['id'])->where('company_id',$companyId)->where('stores_id',$storeId)->update($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("UPDATE_EXIST")->response());
        }
    }

    /**
     * 获取快递公司信息
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExpressDelivery(){
        $edModel   = new ED();
        $data      = $edModel->getExpressDelivery();

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 获取退款列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderRefundList(Request $request){
        if (!$request->filled('stores_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 10);
        $storeId        = $request->get('stores_id');

        $orderBlock     = new OrderBlock();
        $data           = $orderBlock->getOrderRefundList($page, $pageSize, ['stores_id' => $storeId]);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 审核退款订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkOrderRefund(Request $request){
        if (!$request->filled(['status', 'order_status', 'refund_id'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $status         = $request->get('status');
        $refundId       = $request->get('refund_id');
        $orderStatus    = $request->get('order_status');
        $money          = $request->get('money');
        $message        = $request->get('message', '');
        $orderBlock     = new OrderBlock();
        $result         = $orderBlock->checkOrderRefund($refundId, $status, $orderStatus, $money, $message);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 确认接单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmOrders(Request $request){
        if (!$request->filled('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id         = $request->get('id');
        $orderBlock = new OrderBlock();
        $result     = $orderBlock->confirmOrders($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 后台商户确认收货
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmReceipt(Request $request){
        $orderId        = $request->get('orders_id');
        $orderBlock     = new OrderBlock();
        $result = $orderBlock->updateOrderStatus($orderId, 3);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()
                ->failed('DATA_EXIST')
                ->response());
        }else if($result === -2){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('FAILURE','已支付，不能进行取消操作')
                ->response());
        }else if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 商家端取消聚南珍订单进行退款
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelOrder(Request $request){
        if (!$request->filled('orders_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $orderId        = $request->get('orders_id');
        $orderBlock     = new OrderBlock();
        $result         = $orderBlock->updateOrderStatus($orderId, 5, 'store');

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()
                ->failed('DATA_EXIST')
                ->response());
        }else if($result === -3){
            return response()->json(ResponseMessage::getInstance()
                ->failed()
                ->setFailedCode('FAILURE','该订单无法取消')
                ->response());
        }else if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 余额二维码付款
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function balanceQrCodePay(Request $request){
        if (!$request->filled(['code', 'money', 'stores_id'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $code       = $request->get('code');
        $codeLen    = strlen($code);
        $userLen    = substr($code, -1);
        $userId     = substr($code, $codeLen-$userLen-1, $userLen);
        $time       = substr($code, 0, $codeLen-$userLen-1);
        $qrCodeTime = Redis::get('payQrCode:'.$userId);

        if (time() - $time > 60 || $time != $qrCodeTime || !$qrCodeTime){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('QRCODE_ERROR', '二维码已过期')->response());
        }

        $money      = $request->get('money')*100;
        $storeId    = $request->get('stores_id');

        $userBlock  = new UserBlock();
        $result     = $userBlock->balanceQrCodePay($userId, $storeId, $money);

        if ($result === -1){
            return response()->json(ResponseMessage::getInstance()->failed()
                ->setFailedCode('PAY_ERROR', '该用户余额不足')->response());
        }else if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }
}
