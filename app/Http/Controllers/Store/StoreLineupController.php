<?php

namespace App\Http\Controllers\Store;

use App\Blocks\StoreSeatLineupBlock;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreLineupController extends Controller{

    /**
     * 商户排队列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeLineupList(Request $request){

        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 10);
        $search = ['stores_id', 'lineup_number'];

        $searchData = [];
        foreach ($search as $value) {
            if ($request->has($value)){
                $searchData[$value] = $request->get($value);;
            }
        }

        $storeSeatLineupBlock = new StoreSeatLineupBlock();
        $list = $storeSeatLineupBlock->storeLineupList($page, $pageSize, $searchData);
        $pagination = $storeSeatLineupBlock->storeLineupPagination($page, $pageSize, $searchData);

        return response()->json(ResponseMessage::getInstance()->success([
            'list' => $list,
            'pagination' => $pagination
        ])->response());
    }


    /**
     * 删除排队信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeLineupDelete(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->post('id');
        $storeSeatLineupBlock = new StoreSeatLineupBlock();
        $result = $storeSeatLineupBlock->storeLineupDelete($id);

        if ($result) {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 修改排队状态
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeLineupUpdate(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->post('id');
        $storeSeatLineupBlock = new StoreSeatLineupBlock();
        $result = $storeSeatLineupBlock->storeLineupUpdate($id);

        if ($result) {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
