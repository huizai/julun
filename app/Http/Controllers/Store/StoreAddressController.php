<?php
namespace App\Http\Controllers\Store;

use App\Blocks\StoreAddressBlock;
use App\Blocks\StoreBlock;
use App\Libs\ResponseMessage;
use App\Models\StoreAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class StoreAddressController extends Controller{

    /**
     * 商户地址列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['store_id', 'default'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $storeAddressBlock  = new StoreAddressBlock();
        $storeAddress       = $storeAddressBlock->storeAddressList($search, $page, $pageSize);
        $pagination         = $storeAddressBlock->storeAddressListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $storeAddress, 'pagination' => $pagination])->response());
    }

    /**
     * 商户地址详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search                     = ['id' => $id];
        $storeAddressBlock  = new StoreAddressBlock();
        $storeAddress       = $storeAddressBlock->storeAddressInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($storeAddress)->response());
    }



    /**
     * 商户地址修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = [];
        $data['id']             = $request->get('id', 1);
        $data['company_id']     = $companyId;
        $data['store_id']       = $request->get('store_id', 1);
        $data['consignee']      = $request->get('consignee');
        $data['contact']        = $request->get('contact');
        $data['province']       = $request->get('province');
        $data['city']           = $request->get('city');
        $data['district']       = $request->get('district');
        $data['address']        = $request->get('address');
        $data['longitude']      = $request->get('longitude');
        $data['latitude']       = $request->get('latitude');
        $data['is_default']     = $request->get('is_default', 0);
        $data['create_time']    = date('Y-m-d H:i:s', time());

        $validator = Validator::make($data, [
            'id'              => 'required',
            'store_id'        => 'required',
            'consignee'       => 'required',
            'contact'         => 'required',
            'province'        => 'required',
            'city'            => 'required',
            'district'        => 'required',
            'address'         => 'required',
        ], [
            'id.required'            => '地址id不能为空！！',
            'store_id.required'      => '商户id不能为空！！',
            'consignee.required'     => '收货人不能为空！！',
            'contact.required'       => '联系电话不能为空！',
            'province.required'      => '省不能为空！',
            'city.required'          => '市不能为空！',
            'district.required'      => '区不能为空！',
            'address'                => '详细地址不能为空',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $addressBlock = new StoreAddressBlock();
        $address = $addressBlock->usaveStoreAddress($data);

        if($address !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()
                ->json(ResponseMessage::getInstance()->setFailedCode("FAILED", "地址修改失败")
                ->failed()->response());
        }

    }

    /**
     * 商户地址添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = [];
        $data['company_id']     = $companyId;
        $data['store_id']       = $request->get('store_id', 1);
        $data['consignee']      = $request->get('consignee');
        $data['contact']        = $request->get('contact');
        $data['province']       = $request->get('province');
        $data['city']           = $request->get('city');
        $data['district']       = $request->get('district');
        $data['address']        = $request->get('address');
        $data['longitude']      = $request->get('longitude');
        $data['latitude']       = $request->get('latitude');
        $data['is_default']     = $request->get('is_default', 0);
        $data['create_time']    = date('Y-m-d H:i:s', time());

        $validator = Validator::make($data, [
            'store_id'        => 'required',
            'consignee'       => 'required',
            'contact'         => 'required',
            'province'        => 'required',
            'city'            => 'required',
            'district'        => 'required',
            'address'         => 'required',
        ], [
            'store_id.required'      => '商户id不能为空！！',
            'consignee.required'     => '收货人不能为空！！',
            'contact.required'       => '联系电话不能为空！',
            'province.required'      => '省不能为空！',
            'city.required'          => '市不能为空！',
            'district.required'      => '区不能为空！',
            'address'                => '详细地址不能为空',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $addressBlock = new StoreAddressBlock();
        $address = $addressBlock->saveStoreAddress($data);
        if($address){
            return response()->json(ResponseMessage::getInstance()->success($address)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 商户地址删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $addressModel = new StoreAddress();
        $res = $addressModel->where('id', $id)
            ->update([
                'delete_time' => date('Y-m-d H:i:s', time())
            ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}
