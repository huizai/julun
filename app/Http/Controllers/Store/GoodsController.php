<?php

namespace App\Http\Controllers\Store;

use App\Blocks\GoodsBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\Goods;
use App\Models\GoodsClassifies;
use App\Models\GoodsImages;
use App\Models\GoodsSku;
use App\Models\GoodsSkuName;
use App\Models\GoodsSkuValue;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\GoodsRequest;

class GoodsController extends Controller{
    /**
     * 获取商品列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        global $storeId;
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search         = ['company' => $companyId,'store_id'=>$storeId];
        $searchField    = ['name', 'cid'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $goodsBlock = new GoodsBlock();
        $goods = $goodsBlock->goodsList($search, $page, $pageSize);
        $pagination = $goodsBlock->goodsListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $goods, 'pagination' => $pagination])->response());
    }
    /**
     * 商品添加
     * @param GoodsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(GoodsRequest $request){
        global $companyId;
        global $storeId;
        $data = $request->only([
            'name',
            'slogan',
            'thumbnail',
            'selling_price',
            'purchase_price',
            'original_price',
            'sales_volume',
            'stock',
            'detail',
            'sort',
            'classify_id'
        ]);
        $data['company_id']     = $companyId;
        $data['stores_id']      = $storeId;
        $data['create_time']    = date('Y-m-d H:i:s', time());
        $sku        = json_decode($request->get('sku_data'),true);
        $images     = $request->get('images');
        $skuData    = json_decode($request->get('sku'), true);
        $goodsBlock = new GoodsBlock();
        $goodsId = $goodsBlock->addGoods($data, $sku, $skuData, $images);
        if($goodsId !== false){
            return response()->json(ResponseMessage::getInstance()->success($goodsId)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("CREATE_EXIST")->response());
        }

    }
    /**
     * 商品修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        global $storeId;
        $data = $request->only([
                'name',
                'slogan',
                'thumbnail',
                'selling_price',
                'purchase_price',
                'original_price',
                'sales_volume',
                'stock',
                'detail',
                'sort',
                'classify_id',
                'id'
            ]);
        $validator = Validator::make($data, [
            'id' => 'required',
            'name' => 'required',
            'thumbnail' => 'required',
            'selling_price' => 'required',
            'detail' => 'required',
            'stock' => 'required',
            'classify_id' => 'required',
        ], [
            'id.required' => '商品id不能为空！！',
            'name.required' => '商品名不能为空！！',
            'thumbnail.required' => '商品图片不能为空！',
            'selling_price.required' => '出售价格不能为空！',
            'detail.required' => '商品详情不能为空！',
            'stock.required' => '库存不能为空！',
            'classify_id' => '分类不能为空',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()->setFailedCode($res['type'],$res['message'])->failed()->response());
        }
        $data['update_time'] = date('Y-m-d H:i:s', time());
        $goodsModel = new Goods();
        //验存
        $good = $goodsModel::find($data['id']);
        if (empty($good['id'])){
            return response()->json(ResponseMessage::getInstance()->failed('DATA_EXIST')->response());
        }
        $sku        = json_decode($request->get('sku_data'),true);
        $images     = $request->get('images');
        $skuData    = json_decode($request->get('sku'), true);
        $goodsBlock = new GoodsBlock();
        $goodsId    = $goodsBlock->updateGoods($data, $sku, $skuData, $images);
        if($goodsId !== false){
            return response()->json(ResponseMessage::getInstance()->success($goodsId)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("CREATE_EXIST")->response());
        }
    }


    /**
     * 商品删除（软删除）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $goodsModel = new Goods();
        $res = $goodsModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
    /**
     * 商品回收
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recovery(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $goodsModel = new Goods();
        $res = $goodsModel->where('id', $id)->update([
            'delete_time' => null,
        ]);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
    /**
     * 商品删除（真删除）
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        $goodsModel = new Goods();
        //        验存
        $good = $goodsModel::find($id);
        if (empty($good['id'])){
            return response()->json(ResponseMessage::getInstance()->failed('DATA_EXIST')->response());
        }
        $res = $goodsModel->destroy($id);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }
    }


    /**
     * 获取商品分类
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function classify(Request $request){
        global $companyId;
        $pid = $request->get('pid', 0);
        $classify = GoodsClassifies::where('parent_id', $pid)
            ->where('company_id', $companyId)
            ->whereNull('delete_time')
            ->get();
        return response()->json(ResponseMessage::getInstance()->success($classify)->response());
    }



}