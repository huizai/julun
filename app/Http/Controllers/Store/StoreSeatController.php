<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-25
 * Time: 下午6:52
 */

namespace App\Http\Controllers\Store;
use App\Blocks\StoreSeatBlock;
use App\Models\Stores;
use App\Models\StoreSeat;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use QrCode;

class StoreSeatController extends Controller{
    /**
     * 座位信息列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['position', 'store_id'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $storeSeatBlock = new StoreSeatBlock();
        $storeSeat      = $storeSeatBlock->storeSeatList($search, $page, $pageSize);
        $pagination     = $storeSeatBlock->storeSeatListPagination($search, $page, $pageSize);

        foreach ($storeSeat as $item) {
            $item->original_price   = priceIntToFloat($item->original_price);
            $item->selling_price    = priceIntToFloat($item->selling_price);
        }
        return response()->json(ResponseMessage::getInstance()->success(['list' => $storeSeat, 'pagination' => $pagination])->response());
    }

    /**
     * 座位信息详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search                     = ['id' => $id];
        $storeSeatBlock = new StoreSeatBlock();
        $storeSeat = $storeSeatBlock->storeSeatInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($storeSeat)->response());
    }


    /**
     * 座位信息修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'position',
            'seat_number',
            'min_capacity',
            'max_capacity',
            'id',
            'original_price',
            'selling_price',
            'period',
            'start_time',
            'ent_time',
            'stores_id'
        ]);
        $validator = Validator::make($data, [
            'id'                => 'required',
            'position'          => 'required',
            'seat_number'       => 'required',
            'min_capacity'      => 'required',
            'max_capacity'      => 'required'
        ], [
            'id.required'               => '座位id不能为空！！',
            'position.required'         => '座位位置不能为空！！',
            'seat_number.required'      => '座位编号不能为空！！',
            'min_capacity.required'     => '最小容纳范围不能为空！！',
            'max_capacity.required'     => '最大容纳范围不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        $data['capacity_range'] = $data['min_capacity'].'~'.$data['max_capacity'];
        //验存  防止标签名重复
        $storeSeatModel = new StoreSeat();

        if (isset($data['original_price'])){
            $data['original_price'] = $data['original_price'] * 100;
        }
        if (isset($data['selling_price'])){
            $data['selling_price'] = $data['selling_price'] * 100;
        }
        $res = $storeSeatModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 座位信息添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request){
        global $companyId;
        $data = $request->only([
            'position',
            'seat_number',
            'min_capacity',
            'max_capacity',
            'stores_id',
            'original_price',
            'selling_price',
            'period',
            'start_time',
            'ent_time'
        ]);
        $validator = Validator::make($data, [
            'stores_id'           => 'required',
            'position'          => 'required',
            'seat_number'       => 'required',
            'min_capacity'      => 'required',
            'max_capacity'      => 'required'
        ], [
            'stores_id.required'   => '商户id不能为空！！',
            'position.required'         => '座位位置不能为空！！',
            'seat_number.required'      => '座位编号不能为空！！',
            'min_capacity.required'     => '最小容纳范围不能为空！！',
            'max_capacity.required'     => '最大容纳范围不能为空！！'
        ]);
        if ($validator->fails()) {
            $res = [
                'type'      => $validator->errors()->keys()[0],
                'message'   => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }

        $data['capacity_range'] = $data['min_capacity'].'~'.$data['max_capacity'];
        //验存  防止标签名重复
        $storeSeatModel = new StoreSeat();

        if (isset($data['original_price'])){
            $data['original_price'] = $data['original_price'] * 100;
        }
        if (isset($data['selling_price'])){
            $data['selling_price'] = $data['selling_price'] * 100;
        }
        $res = $storeSeatModel::create($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 座位信息删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $storeSeatModel = new StoreSeat();
        $res = $storeSeatModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * 生成商户座位二维码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateSeatQrCode(Request $request){
        if (!$request->has(['seat_id', 'stores_id'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $seatId         = $request->get('seat_id');
        $storeId        = $request->get('stores_id');
        $name           = Stores::where('id', $storeId)->value('name');

        $path = public_path().'/pluto';

        if (!is_dir($path)){
            @mkdir($path, 0755);
        }
        $imageName      = $storeId.time().$seatId.'.png';

        $result = QrCode::format('png')
            ->size(500)
            ->encoding('UTF-8')
            ->generate(
                env('APP_URL')."/pluto/store?company=1&id={$storeId}&seat_id={$seatId}&store_name={$name}",
                "{$path}/{$imageName}"
            );

        if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        $imagePath = env('APP_URL').'/pluto/'.$imageName;

        $seatResult = StoreSeat::where('id', $seatId)->update(['qrcode' => $imagePath]);

        if ($seatResult){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }




}
