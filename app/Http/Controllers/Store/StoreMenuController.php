<?php
/**
 * Created by PhpStorm.
 * User: m1507
 * Date: 2019/4/9
 * Time: 15:29
 */

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreMenuRequest;
use App\Blocks\StoreMenuBlock;
use App\Models\StoreMenu;
use App\Libs\ResponseMessage;

class StoreMenuController extends Controller
{
    public function index(Request $request){
        global $companyId;
        global $storeId;
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $search         = ['company' => $companyId,'store_id' => $storeId];
        $searchField    = ['title'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }
        $storeMenu = new StoreMenuBlock();
        $menus = $storeMenu->menusList($search, $page, $pageSize);
        $pagination = $storeMenu->menusListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $menus, 'pagination' => $pagination])->response());
    }
    public function addMenu(StoreMenuRequest $request){

        global $companyId;
        global $storeId;
        $data = $request->only(['title','price','image']);
        $data['company_id']     = $companyId;
        $data['store_id']      = $storeId;
        $storeMenu = new StoreMenu();
        $res = $storeMenu::create($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('CREATE_EXIST')->response());
        }
    }
    public function updateMenu(StoreMenuRequest $request){

        global $companyId;
        global $storeId;
        $data = $request->only(['title','price','image']);
        $id = $request->get('id');
        if (!$id){
            return response()->json(ResponseMessage::getInstance()->failed('UPDATE_EXIST')->response());
        }
//        验存
        $storeMenu = new StoreMenu();
        $menu = $storeMenu::where('company_id',$companyId)->where('store_id',$storeId)->orderBy('id','desc')->first()->toArray();
        if (!$menu){
            return response()->json(ResponseMessage::getInstance()->failed("DATA_EXIST")->response());
        }
        $res = $storeMenu::where('id', $id)->update($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("UPDATE_EXIST")->response());
        }
    }
    public function delMenu(Request $request){

        global $companyId;
        global $storeId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $storeMenu = new StoreMenu();
        //验存
        $menu = $storeMenu::where('company_id',$companyId)->where('store_id',$storeId)->where('delete_time',null)->find($id);
        if (empty($menu['id'])){
            return response()->json(ResponseMessage::getInstance()->failed('DATA_EXIST')->response());
        }
        $res = $storeMenu->where('id', $id)->where('store_id',$storeId)->where('company_id',$companyId)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}