<?php
/**
 * Created by PhpStorm.
 * User: hhj
 * Date: 19-3-27
 * Time: 下午4:30
 */
namespace   App\Http\Controllers\Store;


use App\Blocks\OrderBlock;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrdersRequest;
use App\Libs\ResponseMessage;
use App\Models\Orders;
use Illuminate\Http\Request;

class MerchantController extends Controller{


    public function merchant(){ //商户
        global $storeId;
        global $companyId;
        $orders = new Orders();
        $total  = $orders->ordersTotal($companyId,$storeId);//当天订单数
        $status = $orders->ordersStatus($companyId,$storeId);//店铺的售后商品，待发货商品
        $ordersBlock = new OrderBlock();
        $ranking  = $ordersBlock->ordersRanking($companyId,$storeId);//商品销量排行
        return response()->json(ResponseMessage::getInstance()->success([ 'orders' => $total,'status'=>$status,'ranking'=>$ranking])->response());
    }


    public function total(OrdersRequest $request){ //1/今日 2/昨日 3/最近七日 4/本月
        global $storeId;
        global $companyId;
        $status=$request->only('status');

        if(!is_numeric($status['status'])){
            return response()->json(ResponseMessage::getInstance()->failed("PARAM_ERROR")->response());
        }

        $orders = new Orders();
        $total  = $orders->ordersTotal($companyId,$storeId,$status['status']);

        if($total){
            return response()->json(ResponseMessage::getInstance()->success([ 'orders' => $total])->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed("PARAM_ERROR")->response());
        }
    }
}