<?php
/**
 * Created by PhpStorm.
 * User: m1507
 * Date: 2019/4/9
 * Time: 15:29
 */

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blocks\StoreBooksBlock;
use App\Models\StoreBooks;
use App\Libs\ResponseMessage;
use Illuminate\Support\Facades\Validator;

class StoreBookController extends Controller
{
    /**
     * 商户预订信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        global $companyId;
        $page                       = $request->get('page', 1);
        $pageSize                   = $request->get('pageSize', 10);
        $search                     = [];
        $searchField                = ['mobile', 'store_id'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $storeBooksBlock = new StoreBooksBlock();
        $storeBooks = $storeBooksBlock->storeBookList($search, $page, $pageSize);
        $pagination = $storeBooksBlock->storeBookListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $storeBooks, 'pagination' => $pagination])->response());
    }

    /**
     * 商户预订信息详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request){
        global $companyId;
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search = ['id' => $id];
        $storeBooksBlock = new StoreBooksBlock();
        $storeBooks = $storeBooksBlock->storeBooksInfo($search);
        return response()->json(ResponseMessage::getInstance()->success($storeBooks)->response());
    }


    /**
     * 商户预订信息修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        global $companyId;
        $data = $request->only([
            'position',
            'people',
            'book_time',
            'mobile',
            'id',
            'sex',
            'name',
            'status'
        ]);
        $validator = Validator::make($data, [
            'id'           => 'required',
            'position'     => 'required',
            'book_time'    => 'required',
            'mobile'       => 'required',
            'sex'          => 'required',
            'name'         => 'required',
            'people'       => 'required',
            'status'       => 'required',
        ], [
            'id.required'           => '预订信息id不能为空！！',
            'position.required'     => '预订位置不能为空！！',
            'book_time.required'    => '用餐时间不能为空！！',
            'mobile.required'       => '手机号码不能为空！',
            'sex.required'          => '性别不能为空',
            'name.required'         => '联系人不能为空',
            'people.required'       => '用餐人数不能为空',
            'status.required'       => '订单状态不能为空',
        ]);
        if ($validator->fails()) {
            $res = [
                'type' => $validator->errors()->keys()[0],
                'message' => $validator->errors()->first()
            ];
            return response()->json(ResponseMessage::getInstance()
                ->setFailedCode($res['type'], $res['message'])->failed()->response());
        }
        $storeBookModel = new StoreBooks();
        $res = $storeBookModel->where('id', $data['id'])->update($data);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

//    /**
//     * 预订信息添加(待定)
//     * @param Request $request
//     * @return \Illuminate\Http\JsonResponse
//     */
//    public function add(Request $request){
//        global $companyId;
//        $data = $request->only([
//            'name',
//            'type_id',
//            'icon',
//            'parent_id',
//            'sort'
//        ]);
//        $validator = Validator::make($data, [
//            'name'         => 'required',
//            'icon'         => 'required',
//            'type_id'      => 'required',
//            'parent_id'    => 'required'
//        ], [
//            'name.required'        => '分类名称不能为空！',
//            'type_id'              => '类型id不能为空！',
//            'icon.required'        => '分类图标不能为空！',
//            'parent_id'            => '上级分类不能为空！',
//        ]);
//        if ($validator->fails()) {
//            $res = [
//                'type'      => $validator->errors()->keys()[0],
//                'message'   => $validator->errors()->first()
//            ];
//            return response()->json(ResponseMessage::getInstance()
//                ->setFailedCode($res['type'], $res['message'])->failed()->response());
//        }
//        $data['company_id']     = $companyId;
//        $storeClassModel = new StoreClassifity();
//        //验存  防止分类名重复
//        $storeClass = $storeClassModel->where('name', $data['name'])->first();
//        if ($storeClass){
//            return response()->json(ResponseMessage::getInstance()
//                ->setFailedCode('CREATEEXIT', "分类名称不能重复")->failed()->response());
//        }
//        $res = $storeClassModel::create($data);
//        if($res){
//            return response()->json(ResponseMessage::getInstance()->success($res)->response());
//        }else{
//            return response()->json(ResponseMessage::getInstance()->failed()->response());
//        }
//    }


    /**
     * 预订信息删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del(Request $request){
        $id = $request->get('id');
        if (!isset($id)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $storeBookModel = new StoreBooks();
        $res = $storeBookModel->where('id', $id)->update([
            'delete_time' => date('Y-m-d H:i:s', time())
        ]);
        if($res !== false){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * 获取预订退款列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBookRefund(Request $request){
        if (!$request->filled('store_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $page               = $request->get('page', 1);
        $pageSize           = $request->get('pageSize', 10);
        $status             = $request->get('status');
        $storeId            = $request->get('store_id');
        $storeBookBlock     = new StoreBooksBlock();
        $data = $storeBookBlock->getBookRefund($storeId, $status, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    public function updateBookRefund(Request $request){
        if (!$request->filled(['id', 'status'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id                 = $request->get('id');
        $status             = $request->get('status');
        $refundAmount       = $request->get('refund_amount', 0) * 100;
        $remark             = $request->get('remark', '');
        $storeBookBlock     = new StoreBooksBlock();
        $result = $storeBookBlock->updateBookRefund($id, $status, $refundAmount, $remark);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }

}
