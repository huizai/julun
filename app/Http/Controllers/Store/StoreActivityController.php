<?php

namespace App\Http\Controllers\Store;

use App\Libs\ResponseMessage;
use Illuminate\Http\Request;
use App\Blocks\StoreActivityBlock;
use Validator;

class StoreActivityController
{


    /**
     * 活动列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activityList(Request $request)
    {
        $page       = $request->get('page', 1);
        $pageSize   = $request->get('pageSize', 10);
        $store_id   = $request->get('store_id', '');

        $activityBlock = new StoreActivityBlock();
        $result = $activityBlock->activityList($page, $pageSize, $store_id);

        return response()->json(ResponseMessage::getInstance()->success($result)->response());
    }

    /**
     * 修改活动信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activityUpdate(Request $request){
        $validator = Validator::make($request->all(), [
            'store_id'         => 'required',
            'title'             => 'required',
            'full_amount'       => 'required',
            'discount_amount'   => 'required',
            'start_time'        => 'required',
            'end_time'          => 'required',
            'type'              => 'required',
        ],[
            'store_id.required'         => '商户不能为空！',
            'title.required'            => '标题不能为空！',
            'full_amount.required'      => 'full_amount不能为空！',
            'discount_amount.required'  => 'discount_amount不能为空！',
            'start_time.required'       => '开始时间不能为空！',
            'end_time.required'         => '结束时间不能为空！',
            'type.required'             => '类型不能为空',
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $id = (int)$request->post('id');
        $type = $request->post('type');
        $data = $request->only([ 'title', 'full_amount', 'discount_amount', 'start_time','end_time']);

        $data['full_amount']        = $data['full_amount'] * 100;
        $data['discount_amount']    = $data['discount_amount'] * 100;

        $data = json_encode($data);

        $activityBlock = new StoreActivityBlock();
        $result = $activityBlock->activityUpdate($id, $data, $type);

        if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed('UPDATE_ERROR')->response());
        }

        return response()->json(ResponseMessage::getInstance()->success('修改成功')->response());
    }


    /**
     * 添加活动
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activityAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'store_id'         => 'required',
            'title'             => 'required',
            'full_amount'       => 'required',
            'discount_amount'   => 'required',
            'start_time'        => 'required',
            'end_time'          => 'required',
            'type'              => 'required',
        ],[
            'store_id.required'         => '商户不能为空！',
            'title.required'            => '标题不能为空！',
            'full_amount.required'      => 'full_amount不能为空！',
            'discount_amount.required'  => 'discount_amount不能为空！',
            'start_time.required'       => '开始时间不能为空！',
            'end_time.required'         => '结束时间不能为空！',
            'type.required'             => '类型不能为空',
        ]);

        if ($validator->fails()) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR',$validator->errors()->first())->failed()->response());
        }

        $store_id = (int)$request->post('store_id');
        $type = $request->post('type');
        $data = $request->only([ 'title', 'full_amount', 'discount_amount', 'start_time','end_time']);

        $data['full_amount']        = $data['full_amount'] * 100;
        $data['discount_amount']    = $data['discount_amount'] * 100;

        $data = json_encode($data);

        $activityBlock = new StoreActivityBlock();
        $result = $activityBlock->activityAdd($store_id, $data, $type);

        if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed('ADD_ERROR')->response());
        }

        return response()->json(ResponseMessage::getInstance()->success('添加成功')->response());
    }

    /**
     * 删除活动
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activityDelete(Request $request){
        if (!$request->post('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->post('id');

        $activityBlock = new StoreActivityBlock();
        $result = $activityBlock->activityDelete($id);

        if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed('DELETE_ERROR')->response());
        }

        return response()->json(ResponseMessage::getInstance()->success('删除成功')->response());
    }
}
