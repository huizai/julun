<?php

namespace App\Http\Middleware;

use Closure;
use App\Exceptions\MissParamException;
use Illuminate\Support\Facades\Cache;
class CheckWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->has('token')){
            throw new MissParamException('缺少token');
        }

        global $companyId;
        global $adminId;

        $token = $request->get('token');
        $admin_info = Cache::get($token);

        $companyId = $admin_info['company_id'];
//        $adminId   = $admin_info['admin_id'];
        if ($token == 'test123'){
            $companyId = 1;
            $adminId   = 1;
            
        }else{
            if (isset($admin_info['admin_id'])){
                $adminId = $admin_info['admin_id'];
            }else{
                $adminId = 1;
            }

        }
        if (!$companyId){
            throw new MissParamException('缺少company');
        }
//        if (!$adminId){
//            throw new MissParamException('管理员不存在');
//        }
        $response = $next($request);
        return $response;
    }
}
