<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/web/*',
        '/web/goods/add',
        '/web/login',
        '/web/shop/set',
        '/web/banner/add',
        '/web/banner/update',
        '/web/login',
        '/member/updateInt',
        '/member/updateBalance',
        '/statistics/total',
        '/configure/confUpdate',
        '/web/setAlertOver',
        '/tag/addTag',
        '/store/setStoreMsg',
        '/store/classify/list',
        '/store/add',
        '/agent/exam',
        '/agent/consent',
        '/agent/info'

    ];
}
