<?php

namespace App\Http\Middleware;

use Closure;
use App\Exceptions\MissParamException;
use Illuminate\Support\Facades\Cache;
class CheckAgent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->has('token')){
            throw new MissParamException('缺少token');
        }

        global $companyId;
//        global $agentId;
        $token = $request->get('token');
        $agentInfo = Cache::get($token);

        $companyId = $agentInfo['company_id'];
//        $agentId   = $agentInfo['agent_id'];
        if ($token == 'agent123'){
            $companyId = 1;
//            $agentId   = 2;
        }
        if (!$companyId){
            throw new MissParamException('缺少company');
        }
//        if (!$agentId){
//            throw new MissParamException('用户不存在');
//        }
        $response = $next($request);
        return $response;
    }
}
