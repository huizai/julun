<?php

namespace App\Http\Middleware;

use App\Libs\ResponseMessage;
use Closure;
use App\Exceptions\MissParamException;
use Illuminate\Support\Facades\Cache;

class CheckStore
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->has('token')){
            throw new MissParamException('缺少token');
        }

        global $companyId;
//        global $storeId;
        $token = $request->get('token');
        if ($token == 'store123'){
            $companyId = 1;
        }else{
            $store_info = Cache::get($token);
            if (gettype($store_info) != 'array'){
                if(!isset($userInfo->id)){
                    return response()->json(ResponseMessage::getInstance()->failed('NO_LOGIN')->response());
                }
            }
            $companyId = $store_info['company_id'];
        }

        if (!$companyId){
            \Log::debug($request->path());
            throw new MissParamException('缺少单位ID');
        }

        $response = $next($request);
        return $response;
    }
}
