<?php

namespace App\Http\Middleware;

use App\Libs\ResponseMessage;
use Closure;
use Config , Cookie;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request , Closure $next)
    {
        $token = $request->get('token');

        global $g_uid;

        if($token === 'test123'){
            $g_uid = 506;
        }else {
            $userInfo = \Cache::get($token);
            if(!isset($userInfo->id)){
                return response()->json(ResponseMessage::getInstance()->failed('NO_LOGIN')->response());
            }
            \Cache::put($token, $userInfo, 24*60*365);
            $g_uid = $userInfo->id;
        }

        $response = $next($request);
        return $response;
    }
}
