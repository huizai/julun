<?php

namespace App\Http\Middleware;

use App\Exceptions\MissParamException;
use Closure;

class GetCompanyId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->has('company')){
            throw new MissParamException('缺少company');
        }

        global $companyId;
        $companyId = $request->get('company');

        $response = $next($request);
        return $response;
    }
}
