<?php

namespace App\Http\Middleware;
use App\Exceptions\MissParamException;
use App\Models\WechatBaseConfig;
use Closure;
use Config;

class SetWechatConfig
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('appid')) {
            global $appId;
            $appId = $request->get('appid');
            $config = \Cache::get($appId);
            if(!$config) {
                $config = WechatBaseConfig::where('appid', $appId)->first();

                \Cache::put($appId, $config, 1);
            }
            if($config){
                global $wechatId;
                global $companyId;
                $wechatId = $config->id;
                $companyId = $config->company_id;
                Config::set('wechat.db.app_id', $config->appid);
                Config::set('wechat.db.secret', $config->secret);
                Config::set('wechat.db.token', $config->token);
                Config::set('wechat.db.aes_key', $config->aes_key);
//                $cache = new FilesystemCache();
//                Config::set('wechat.cache', $cache);
            }else{
                throw new MissParamException("微信appid({$appId})的数据不存在");
            }
        }else{
            throw new MissParamException("缺少appid参数");
        }
        $response = $next($request);
        return $response;
    }
}
