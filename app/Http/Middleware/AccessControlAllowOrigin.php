<?php

namespace App\Http\Middleware;

use Closure;

class AccessControlAllowOrigin
{
    /**
     *
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $requestHeader = $request->header();

        if(isset($requestHeader['origin'])) {
            $origin = $requestHeader['origin'];
            $originUrl = parse_url($origin[0]);
            $url = $originUrl['host'];

            if(isset($originUrl['port'])){
                $url .= ':' . $originUrl['port'];
            }
            header('Access-Control-Allow-Origin:http://' . $url);
            header('Access-Control-Allow-Credentials:true');
            header('Access-Control-Allow-Headers: Origin,userSign, X-Requested-With, Content-Type, Accept');
        }

        return $next($request);
    }

}