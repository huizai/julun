<?php

if (!function_exists('isMobile')) {
    function isMobile()
    {
        $useragent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $useragent_commentsblock = preg_match('/.∗?/', $useragent, $matches) > 0 ? $matches[0] : '';


        $mobile_os_list = array('Google Wireless Transcoder', 'Windows CE', 'WindowsCE', 'Symbian', 'Android', 'armv6l', 'armv5', 'Mobile', 'CentOS', 'mowser', 'AvantGo', 'Opera Mobi', 'J2ME/MIDP', 'Smartphone', 'Go.Web', 'Palm', 'iPAQ');
        $mobile_token_list = array('Profile/MIDP', 'Configuration/CLDC-', '160×160', '176×220', '240×240', '240×320', '320×240', 'UP.Browser', 'UP.Link', 'SymbianOS', 'PalmOS', 'PocketPC', 'SonyEricsson', 'Nokia', 'BlackBerry', 'Vodafone', 'BenQ', 'Novarra-Vision', 'Iris', 'NetFront', 'HTC_', 'Xda_', 'SAMSUNG-SGH', 'Wapaka', 'DoCoMo', 'iPhone', 'iPod', 'iPad');

        $found_mobile = checkSubstrs($mobile_os_list, $useragent) || checkSubstrs($mobile_token_list, $useragent_commentsblock);

        if ($found_mobile) {
            return true;
        } else {
            return false;
        }
    }
}

//将以分为单位的price格式改成以元为单位的格式
if (!function_exists('priceIntToFloat')) {
    function priceIntToFloat($price)
    {
        return sprintf("%.2f", ($price / 100));
    }
}

if (!function_exists('checkSubstrs')) {
    function checkSubstrs($substrs, $text)
    {

        foreach ($substrs as $substr) {
            if (false !== strpos($text, $substr)) {
                return true;
            }
        }
        return false;
    }
}

/**
 * 获取随机值
 * @param int $len
 * @return string
 */
if (!function_exists('getSalt')) {
    function getSalt($len = 8, $num = 0)
    {
        $salt = '';
        if ($num == 0) {
            $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        } else {
            $str = "0123456789";
        }
        $max = strlen($str) - 1;

        for ($i = 0; $i < $len; $i++) {
            $salt .= $str[rand(0, $max)];
        }

        return $salt;
    }
}

/**
 * 获取加密串
 * @param $str
 * @param $salt
 * @return string
 */
if (!function_exists('encrypt_sha_md5')) {
    function encrypt_sha_md5($str, $salt)
    {
        return sha1(md5($str . $salt));
    }
}

/**
 * @param $url
 * @return mixed
 * curl get
 */
if (!function_exists('curlGet')) {
    function curlGet($url, $cookie = '')
    {
        $ch = curl_init();
        $user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36';
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        // 2. 设置选项，包括URL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:192.168.2.11', 'CLIENT-IP:192.168.2.11'));  //构造IP
        curl_setopt($ch, CURLOPT_REFERER, "http://www.baidu.com/");   //构造来路
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        if(!empty($cookie)){
            curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        }

        // 3. 执行并获取HTML文档内容
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}

/**
 * @param $url
 * @return mixed
 * curl post
 */
if (!function_exists('curlPost')) {
    function curlPost($url, $data)
    {
        \Log::debug($url);
        $ch = curl_init();
        $user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:17.0) Gecko/20100101 Firefox/17.0 FirePHP/0.7.1';
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        // 2. 设置选项，包括URL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // 3. 执行并获取HTML文档内容
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}

//获取顶级域名
if (!function_exists('getdomain')) {
    function getdomain($url)
    {
        $host = strtolower($url);
        if (strpos($host, '/') !== false) {
            $parse = @parse_url($host);
            $host = $parse ['host'];
        }
        $topleveldomaindb = array('com', 'edu', 'gov', 'int', 'mil', 'net', 'org', 'biz', 'info', 'pro', 'name', 'museum', 'coop', 'aero', 'xxx', 'idv', 'mobi', 'cc', 'me');
        $str = '';
        foreach ($topleveldomaindb as $v) {
            $str .= ($str ? '|' : '') . $v;
        }

        $matchstr = "[^\.]+\.(?:(" . $str . ")|\w{2}|((" . $str . ")\.\w{2}))$";
        if (preg_match("/" . $matchstr . "/ies", $host, $matchs)) {
            $domain = $matchs ['0'];
        } else {
            $domain = $host;
        }
        return $domain;
    }
}
//将 xml数据转换为数组格式。
if (!function_exists('xml_to_array')) {
    function xml_to_array($xml)
    {
        $reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
        if (preg_match_all($reg, $xml, $matches)) {
            $count = count($matches[0]);
            for ($i = 0; $i < $count; $i++) {
                $subxml = $matches[2][$i];
                $key = $matches[1][$i];
                if (preg_match($reg, $subxml)) {
                    $arr[$key] = xml_to_array($subxml);
                } else {
                    $arr[$key] = $subxml;
                }
            }
        }
        return $arr;
    }
}


//获取远程图片类型
if (!function_exists('getRemoteImgType')) {
    function getRemoteImgType($url)
    {
        $type = '';
        $imgHeaderData = get_headers($url, 1);
        switch ($imgHeaderData['Content-Type']) {
            case 'image/png' :
                $type = 'png';
                break;
            case 'image/jpeg' :
                $type = 'jpg';
                break;
            case 'image/gif' :
                $type = 'gif';
                break;
            case 'image/bmp' :
                $type = 'bmp';
                break;
        }

        return $type;
    }
}

//下载文件
if (!function_exists('downloadFile')) {
    function downloadFile($filepath, $filename)
    {
        header('Content-Description: File Transfer');

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        readfile($filepath);
    }
}

/**
 * 自动去掉小数末尾的0
 * @param  float  $num [小数]
 * @return float       [返回去掉小数末尾0的小数]
 */
if (!function_exists('del0')) {
    function del0($s)
    {
        $s = trim(strval($s));
        if (preg_match('#^-?\d+?\.0+$#', $s)) {
            return preg_replace('#^(-?\d+?)\.0+$#','$1',$s);
        }
        if (preg_match('#^-?\d+?\.[0-9]+?0+$#', $s)) {
            return preg_replace('#^(-?\d+\.[0-9]+?)0+$#','$1',$s);
        }
        return $s;
    }
}

/**
 * @param $mobile
 * @return int
 * 判断是否是手机号
 */
if(!function_exists('isMobileNum')) {
    function isMobileNum($mobile)
    {
        return preg_match("/^13[\d]{9}$|^14[5,7]{1}\d{8}$|^19[\d]{9}$|^15[^4]{1}\d{8}$|^17[0,3,6,7,8]{1}\d{8}$|^18[\d]{9}$|^16[\d]{9}$/", $mobile);
    }
}
if(!function_exists('getMillisecond')) {
    function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }
}

/**
 * 判断一个值是否存在一个二维数组中返回所在位置
 * @access public
 * @param string        $value 值
 * @param array  $array 二维数组
 * @return int
 * @author 91minsir
 */
if (!function_exists('deep_in_array')) {
    function deep_in_array($value, $array)
    {
        foreach ($array as $item) {
            if ($item['name'] == $value) {
                return $item['id'];
            } else {
                continue;
            }
        }
        return false;
    }
}

/**
 * 判断一个值是否存在一个二维数组中返回bool
 * @access public
 * @param array         $array 二维数组
 * @param string        $value 值
 * @return bool
 * @author 91minsir
 */
if (!function_exists('deep_in_array_a')) {
    function deep_in_array_a($value, $array)
    {
        foreach ($array as $item) {
            if (!is_array($item)) {
                if ($item == $value) {
                    return true;
                } else {
                    continue;
                }
            }

            if (in_array($value, $item)) {
                return true;
            } else if (deep_in_array($value, $item)) {
                return true;
            }
        }
        return false;
    }
}
/**
 * 限制字符长度，用...代替
 * @param string $sourcestr
 * @param string $cutlength
 * @return string
 * @author 91minsir
 */
if (!function_exists('julun_cut_str')) {
    function julun_cut_str($sourcestr, $cutlength)
    {
        $returnstr = '';
        $i = 0;
        $n = 0;
        $str_length = strlen($sourcestr);//字符串的字节数
        while (($n < $cutlength) and ($i <= $str_length)) {
            $temp_str = substr($sourcestr, $i, 1);
            $ascnum = Ord($temp_str);//得到字符串中第$i位字符的ascii码
            if ($ascnum >= 224)    //如果ASCII位高与224，
            {
                $returnstr = $returnstr . substr($sourcestr, $i, 3); //根据UTF-8编码规范，将3个连续的字符计为单个字符
                $i = $i + 3;            //实际Byte计为3
                $n++;            //字串长度计1
            } elseif ($ascnum >= 192) //如果ASCII位高与192，
            {
                $returnstr = $returnstr . substr($sourcestr, $i, 2); //根据UTF-8编码规范，将2个连续的字符计为单个字符
                $i = $i + 2;            //实际Byte计为2
                $n++;            //字串长度计1
            } elseif ($ascnum >= 65 && $ascnum <= 90) //如果是大写字母，
            {
                $returnstr = $returnstr . substr($sourcestr, $i, 1);
                $i = $i + 1;            //实际的Byte数仍计1个
                $n++;            //但考虑整体美观，大写字母计成一个高位字符
            } else                //其他情况下，包括小写字母和半角标点符号，
            {
                $returnstr = $returnstr . substr($sourcestr, $i, 1);
                $i = $i + 1;            //实际的Byte数计1个
                $n = $n + 0.5;        //小写字母和半角标点等与半个高位字符宽...
            }
        }
        if ($str_length > $i) {
            $returnstr = $returnstr . "...";//超过长度时在尾处加上省略号
        }
        return $returnstr;
    }
}

/**
 * alertOver推送
 * @param $source string
 * @param $receiver string
 * @param $title string
 * @param $content string
 * @return array
 * @author nelson
 */
if (!function_exists('alertOver')) {
    function alertOver($source, $receiver, $title, $content, $url)
    {
//调用示例
//    $res = alertOver(env('SOURCE'),env('TIXIAN_RECEIVER'),'测试','测试内容','www.baidu.com');
//    return response()->json(ResponseMessage::getInstance()->success($res)->response());
        $data = [
            "source" => $source,//发送源
            "receiver" => $receiver,//多个接收组可以通过传值来改变推送信息的接收人如果只有一个可以写死
            'content' => $content,
            'title' => $title,
            'url' => $url
        ];
        $result = request_by_curl("https://api.alertover.com/v1/alert", $data);
        return $result;
    }
}
/**
 * Curl版本
 * 使用方法：
 * $post_string = "app=request&version=beta";
 * request_by_curl('http://blog.snsgou.com/restServer.php', $post_string);
 */
if (!function_exists('request_by_curl')) {
    function request_by_curl($remote_server, $post_string)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $remote_server);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLOPT_USERAGENT, "snsgou.com's CURL Example beta");
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}


/**
 * 修改env文件
 * @param array $data
 * @return array
 * @author 91minsir
 */
if (!function_exists('modifyEnv')) {
    function modifyEnv(array $data)
    {
        $envPath = base_path() . DIRECTORY_SEPARATOR . '.env';

        $contentArray = collect(file($envPath, FILE_IGNORE_NEW_LINES));

        $contentArray->transform(function ($item) use ($data) {
            foreach ($data as $key => $value) {
                if (str_contains($item, $key)) {
                    return $key . '=' . $value;
                }
            }

            return $item;
        });

        $content = implode($contentArray->toArray(), "\n");

        $res = \File::put($envPath, $content);
        return $res;
    }
}


/**
 * 获得一个自然数
 * @param $number
 * @return int
 * @author nelson
 */
if (!function_exists('naturalNumber')) {
    function naturalNumber($number)
    {
        if ($number < 0) {
            $number = 0;
        }
        return $number;
    }
}

/**
 * @param $num 科学计数法字符串  如 2.1E-5
 * @param int $double 小数点保留位数 默认5位
 * @return string
 * @author 91minsir
 */
if (!function_exists('jlSctonum')) {
    function jlSctonum($num, $double = 5)
    {
        if (false !== stripos($num, "e")) {
            $a = explode("e", strtolower($num));
            return bcmul($a[0], bcpow(10, $a[1], $double), $double);
        }
        return $num;
    }
}


/**
 * 获取一年有多少天
 * @param int $year 年
 * @return int
 * @author 91minsir
 */
if (!function_exists('cal_days_in_year')) {
    function cal_days_in_year($year){

        $days=0;

        for($month=1;$month<=12;$month++){

            $days = $days + cal_days_in_month(CAL_GREGORIAN,$month,$year);

        }

        return $days;

    }
}


/**
 * 获取当前毫秒数
 */
if (!function_exists('getMillisecond')) {
    function getMillisecond(){
        list($s1, $s2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
    }
}
