<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="email=no" />
    <title>长沙财鼎商业管理有限公司</title>
    <script src="{{URL::asset("/")}}js/bowser.min.js" type="text/javascript"></script>
    {{--<script src="{{URL::asset("/")}}js/redirect.js" type="text/javascript"></script>--}}
    <script>
        var isPad = bowser.tablet;
        if(isPad){
            var meta = document.createElement("meta");
            var head = document.getElementsByTagName("head")[0];
            head.insertBefore(meta,head.childNodes[0]);
            meta.setAttribute("name","viewport");
            meta.setAttribute("content","width=device-width, initial-scale=0.6, user-scalable=0, minimum-scale=0.6, maximum-scale=0.6");
        }
    </script>
    <link rel="stylesheet" type="text/css" href="{{URL::asset("/")}}css/global.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset("/")}}css/index20181111.css">
    <link rel="shortcut icon" href="https://www.hnxhlmxc.com/favicon.ico" type="image/vnd.microsoft.icon">
</head>
<body>
<div class="wrap head_wrap_index">
    <div class="head_wrap">
        <div class="head_bgcolor"></div>
        <div class="head">
            <div class="head_con">
                <h1 class="logo">长沙财鼎商业管理有限公司</h1>
            </div>
        </div>
    </div><div class="banner_wrap">
        <div class="flexslider">
            <ul class="slides">
                <li class="slide-video">
                    <img src="{{URL::asset("/")}}img/banner.png">
                    {{--<video src="https://www.tencent.com/video/bannerbg1.mp4" loop autoplay preload muted></video>--}}
                    {{--<h3 class="text text3">长沙财鼎商业管理有限公司</h3>--}}
                </li>
            </ul>
        </div>
        <div class="custom-navigation">
            <div class="custom-controls-container"></div>
        </div>
    </div>
    <div class="banner-video">
        <div class="mask"></div>
        <div class="video">
            <div id="video18"></div>
            <div class="close">×</div>
        </div>
    </div>
    <div class="fullmask"></div>
    <div class="about-wrap">
        <h3 class="title">长沙财鼎商业管理有限公司</h3>
        <p>成立于2018-07-09，注册资金2000万。</p>
        <p>经营范围包括商业管理；酒店管理；市场调研服务；商业活动组织；商业活动策划；会议、展览及相关服务；停车场运营管理；</p>
        <p>商品市场的运营与管理；会展业的运营和服务；物业管理；房屋租赁；场地租赁；市场管理服务；房地产中介服务。</p>
    </div>
    <div class="news-wrap">
        <div class="news-item news-item-1">
            <a href="#" class="news-hd">
                <div class="bg"></div>
                <!-- <h4 class="tit">关于我们</h4> -->
                <!-- <p class="des">关于我们</p> -->
            </a>
            <div class="news-bd">
                <div class="news-list first">
                    长沙财鼎商业管理有限公司
                    <div class="news-date">成立于2018-07-09 </div>
                    <div class="news-date">注册资金2000万  </div>
                    <div class="news-date">长沙市望城区金峰园小区A08栋68号 </div>
                </div>
            </div>
        </div>
        <div class="news-item news-item-2">
            <a href="#" class="news-hd">
                <div class="bg"></div>
                <!--   <h4 class="tit">联系我们</h4> -->
                <!-- <p class="des">联系我们</p> -->
            </a>
            <div class="news-bd">
                <div class="news-list first">
                    有问题请联系我们
                    <div class="news-date">手机: +86-13217414308 </div>
                    <div class="news-date">座机：0731-89606783 </div>
                    <div class="news-date">Email：254805979@qq.com </div>
                </div>
            </div>
        </div>

    </div>

    <div class="footer">
        <div class="footer_con">
            <div class="footer_con_wrap">
                <div class="footer_attention">
                    <h4 class="title">关注我们</h4>
                    <ul class="link_list">
                        <li><a href="javascript:;" class="tencent_wx" target="_blank" title=""><i class="ico_wx"></i>微信</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer_wx_layer">
                <div class="wx_account">
                    <div class="account_title">
                        微信公众帐号：新华联梦想城
                    </div>
                    <div class="account_image">
                    </div>
                    <div class="account_close">
                    </div>
                </div>
            </div>

            <div class="footer_copyright">
                Copyright &copy; 2005 - <span class="footyear"></span> changshacaiding. All Rights Reserved.<em>长沙财鼎商业管理有限公司 版权所有</em>
            </div>
        </div>
    </div>
</div>
<script src="{{URL::asset("/")}}js/jquery-1.11.3.min.js"></script>
<script src="{{URL::asset("/")}}js/jquery.dotdotdot.min.js"></script>
<script src="{{URL::asset("/")}}js/jquery.flexslider-min.js"></script>
<script src="{{URL::asset("/")}}js/txplayer.js"></script>
<script src="{{URL::asset("/")}}js/index20181111.js"></script>
<script src="{{URL::asset("/")}}js/template.js"></script>
<script src="{{URL::asset("/")}}js/script.js"></script>
<script>
    var _mtac = {};
    (function() {
        var mta = document.createElement("script");
        mta.src = "{{URL::asset("/")}}js/stats.js";
        mta.setAttribute("name", "MTAH5");
        mta.setAttribute("sid", "500665694");
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(mta, s);
    })();
</script>
<script type="text/javascript" src="https://tajs.qq.com/stats?sId=59367890" charset="UTF-8"></script>
</body>
</html>
