<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>缔盟注册</title>
    <meta name="keywords" content="信用卡智能管家 养卡 提额 代还" />
    <meta name="description" content="信用卡智能管家 养卡 提额 代还" />

    <link rel="stylesheet" type="text/css" href="{{URL::asset("/")}}vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset("/")}}fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset("/")}}fonts/iconic/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset("/")}}css/util.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset("/")}}css/main.css">
</head>

<body>

<div class="limiter">
    <div class="container-login100" style="background-image: url('images/bg-01.jpg');">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
            <div class="login100-form validate-form">
                <span class="login100-form-title p-b-49">注册缔盟</span>

                <div class="wrap-input100 validate-input m-b-23" data-validate="请输入手机号">
                    <span class="label-input100">手机号</span>
                    <input class="input100 mobile" type="text" name="mobile" placeholder="请输入手机号" autocomplete="off">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-23 ">
                    <span class="label-input100">验证码</span>
                    <div style="display: flex;align-items: center;justify-content: space-between">
                        <div data-validate="请输入验证码">
                            <input class="input100 smsCode" type="text" name="sms_code" placeholder="请输入验证码">
                            <span class="focus-input100" data-symbol="&#xf404;"></span>
                        </div>
                        <div style="font-size: 12px;" class="getSmsCode">获取验证码</div>
                    </div>
                </div>

                <div class="wrap-input100 validate-input m-b-23" data-validate="请输入确认密码">
                    <span class="label-input100">密码</span>
                    <input class="input100 password" type="password" name="pass" placeholder="请输入确认密码">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-23" data-validate="请输入确认密码">
                    <span class="label-input100">确认密码</span>
                    <input class="input100 repassword" type="password" name="repass" placeholder="请输入确认密码">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>

                <div class="wrap-input100  m-b-23" data-validate="推广人">
                    <span class="label-input100">推广人</span>
                    <input class="input100 recommend" readonly type="text" name="recommend" value="{{$recommend_mobile}}" placeholder="请输入推广人">
                    <span class="focus-input100" data-symbol="&#xf126;"></span>
                </div>

                <div class="text-right p-t-8 p-b-31" style="font-size: 12px;display: flex;align-items: center;flex-direction: row-reverse">
                    我已阅读并同意《用户注册协议》<input id="checkbox" style="margin-right: 5px" type="checkbox"/>
                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button id="submit" class="login100-form-btn">立即注册</button>
                    </div>
                </div>

                <div class="flex-col-c p-t-25"  style="font-size: 12px;">
                    <a style="font-size: 12px;" href="{{$downloadUrl}}">已有账号,点击下载APP</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset("/")}}vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="{{URL::asset("/")}}js/main.js"></script>
</body>

</html>