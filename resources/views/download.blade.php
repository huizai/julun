<!DOCTYPE html>
<!-- saved from url=(0048)https://fir.im/tfdk?utm_source=fir&utm_medium=qr -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="minimal-ui,width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">
    <meta name="format-detection" content="telephone=no">
    <meta name="robots" content="noindex,nofollow">
    <link rel="dns-prefetch" href="https://api.fir.im/">
    <link rel="dns-prefetch" href="https://download.fir.im/">
    <link rel="stylesheet" href="/fir.im/e3f4f7c3.download.css">
    <script src="/fir.im/hm.js"></script>
    <script async="" src="/fir.im/analytics.js"></script>
    <script type="text/javascript" charset="utf-8">startTime = new Date();</script>
    <title>{{$config['name']}}</title>
    <link rel="apple-touch-icon" href="/fir.im/0f76f1d787b8ea276ba9766abe5ec3896c414d2f">
    <link rel="apple-touch-icon-precomposed" href="/fir.im/0f76f1d787b8ea276ba9766abe5ec3896c414d2f">

    <style>
        .open-tips{
            position: fixed;
            background: rgba(0,0,0,0.8);
            z-index: 100;
            height: 100%;
            width: 100%;
        }
    </style>
</head>
<body class="app"><img src="/fir.im/Transparent.gif" style="display: none" alt="">
<div class="masklayer" id="MaskLayer" style="display:none"></div>
<span class="pattern left"><img src="/fir.im/download_pattern_left.png"></span> <span class="pattern right">
    <img src="/fir.im/download_pattern_right.png"></span>
<div class="open-tips" style="display: none">
    <img style="width: 80%;float: right;margin-right: 20px" src={{URL::asset("/")}}images/weixin-tips.png>
</div>
<div class="out-container">
    <div class="main">
        <header itemscope="" itemtype="http://schema.org/SoftwareApplication">
            <span style="display:none;" itemprop="description"></span>
            <span style="display:none;" itemprop="url">https://fir.im/tfdk</span>
            <span style="display:none;" itemprop="operatingSystem">android</span>
            <span style="display:none;" itemprop="name">{{$config['name']}}</span>

            <div class="table-container">
                <div class="cell-container">
                    <div class="app-brief">
                        <div class="icon-container wrapper">
                            <i class="icon-icon_path bg-path"></i>
                            <span class="icon">
                                <img src="/img/app.logo.png" itemprop="image">
                            </span>
                        </div>

                        <p class="release-type wrapper">&nbsp;</p>
                        <h1 class="name wrapper">
                            <span class="icon-warp">
                                <i class="icon-android"></i>
                                <i class="icon-ios"></i>
                                {{$config['name']}}
                            </span>
                        </h1>

                        <div class="release-info">
                            <p><span itemprop="softwareVersion">{{$config['ios']['last_version']}}</span></p>
                        </div>

                        <div id="actions" class="actions type-ios download">
                            <button class="android">下载安装</button>
                            <button class="ios">下载安装</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="new_layout">
            </div>
        </header>
    </div>
</div>

<script src="/fir.im/qrcode.js"></script>
<script src="/fir.im/markup.js"></script>


<script type="text/javascript">
    //安卓下载地址
    var androidUrl = "{{$android}}";
    //IOS下载地址
    var iosUrl = "{{$ios}}";
    var openTips = document.querySelector('.open-tips');
    var androidBtn = document.querySelector(".download .android");
    var IOSBtn = document.querySelector(".download .ios");

    checkos();
    //显示按钮
    //按钮点击事件
    androidBtn.addEventListener('touchstart',function(){
        if(isWeChat()){
            openTips.style.display = 'block';
        }else{
            location.href = androidUrl;
        }
    })
    IOSBtn.addEventListener('touchstart',function(){
        if(isWeChat()){
            openTips.style.display = 'block';
        }else{
            location.href = iosUrl;
        }
    })

    function checkos(){
        var u = navigator.userAgent;
        var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
        var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
        if(isAndroid){
            document.querySelector('.icon-ios').style.display = 'none';
            IOSBtn.style.display = "none";
            return;
        }else if(isiOS){
            document.querySelector('.icon-android').style.display = 'none';
            androidBtn.style.display = "none";
            return;
        }else{
            document.querySelector('.icon-ios').style.display = 'none';
            IOSBtn.style.display = "none";
            return;
        }
    }

    function isWeChat(){
        var ua = window.navigator.userAgent.toLowerCase();
        if(ua.match(/MicroMessenger/i) == 'micromessenger'){
            return true;
        }else{
            return false;
        }
    }
</script>
</body>
</html>
