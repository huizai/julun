<?php

Route::group(['namespace' => 'Tool', 'prefix' => 'tool', 'middleware' => ['origin']], function (){
    Route::options('/{all}', function(\Illuminate\Http\Request $request) {
        $origin = $request->header('ORIGIN', '*');
        header("Access-Control-Allow-Origin: $origin");
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, token, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie');
    })->where(['all' => '([a-zA-Z0-9-]|/)+']);
    //发送短信验证码
    Route::post('/sms','ToolController@loginAuthCode');

    //图形验证码
    Route::any('/captcha', 'ToolController@captcha');
    Route::post('/captcha/validate', 'ToolController@captchaValidate');

    //上传图片
    Route::post('/upload/img', 'ToolController@uploadImg');

    //获取区域
    Route::any('/area', 'ToolController@areaLinkAge');


    //获取京东地区数据
    Route::any('/get/area', 'ToolController@getArea');



    //阿里云
    Route::post('/ali/sms/vc', 'ToolController@validateCodeFromSms');
    Route::post('/ali/upload/image', 'AliyunOssController@uploadImage');

    Route::group(['prefix' => 'yunxin'], function (){
        Route::group(['middleware' => 'apiAuth'], function (){
            Route::post('/user/refresh/token', 'YunXinController@refreshUserYunXinToken');
            Route::post('/user/info', 'YunXinController@getUserYunXinInfo');
        });

        Route::group(['middleware' => 'CheckStore'], function (){
            Route::post('/store/refresh/token', 'YunXinController@refreshStoreYunXinToken');
        });

    });



});
