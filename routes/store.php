<?php

Route::group(['namespace'=>'Store','middleware' => ['origin']], function (){
    Route::options('/{all}', function(\Illuminate\Http\Request $request) {
        $origin = $request->header('ORIGIN', '*');
        header("Access-Control-Allow-Origin: $origin");
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, token, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie');
    })->where(['all' => '([a-zA-Z0-9-]|/)+']);

    Route::post('/login', 'StoreController@login');
    Route::post('/register', 'StoreController@agentReg');
    Route::get('/index/page', 'StoreController@indexPage');

    Route::post('/login/by/vc', 'StoreController@loginByVc');
    Route::post('/password/by/vc', 'StoreController@passwordByVc');

    Route::post('/change/password', 'StoreController@changePassword');
    Route::get('/seat/position', 'StoreController@seatPosition');
        Route::group(['middleware' => ['CheckStore']], function (){
            Route::any('/goods/list', 'GoodsController@index');
            Route::post('/goods/add', 'GoodsController@create');
            Route::any('/goods/del', 'GoodsController@del');
            Route::any('/goods/recovery', 'GoodsController@recovery');
            Route::any('/goods/classify', 'GoodsController@classify');//商品分类列表
            Route::post('/goods/update', 'GoodsController@update');//商品修改

            Route::get('/get/type', 'StoreController@getStoreType');//获取商户类型

            Route::any('/merchant', 'MerchantController@merchant');
            Route::post('/total', 'MerchantController@total');

            Route::any('/order', 'OrderController@index');
            Route::any('/order/info', 'OrderController@getOrderInfo');
            Route::post('/order/send', 'OrderController@send');
            Route::post('/order/remake', 'OrderController@remake');
            Route::any('/order/refund/list', 'OrderController@getOrderRefundList');
            Route::post('/order/refund/check', 'OrderController@checkOrderRefund');
            Route::post('/order/confirm/receipt', 'OrderController@confirmReceipt');
            Route::post('/order/cancel', 'OrderController@cancelOrder');

            Route::get('/book/refund', 'StoreBookController@getBookRefund');
            Route::post('/book/refund/update', 'StoreBookController@updateBookRefund');

            Route::any('/personal', 'StoreController@personal');

            Route::post('/set/StoreMsg', 'StoreInfoController@setStoreMsg');
            Route::post('/set/store', 'StoreInfoController@setStore');
            Route::post('/set/freight', 'StoreInfoController@setFreight');



            Route::get('/menu/index', 'StoreMenuController@index');
            Route::post('/menu/add', 'StoreMenuController@addMenu');
            Route::post('/menu/update', 'StoreMenuController@updateMenu');
            Route::get('/menu/del', 'StoreMenuController@delMenu');


            Route::get('/book/list', 'StoreBookController@index');
            Route::get('/book/read', 'StoreBookController@read');
            Route::post('/book/update', 'StoreBookController@update');
            Route::get('/book/del', 'StoreBookController@del');


            Route::get('/devices/list', 'StoreDevicesController@index');
            Route::get('/devices/read', 'StoreDevicesController@read');
            Route::post('/devices/update', 'StoreDevicesController@update');
            Route::post('/devices/add', 'StoreDevicesController@add');
            Route::get('/devices/del', 'StoreDevicesController@del');

            Route::get('/tags/list', 'StoreTagsController@index');
            Route::get('/tags/read', 'StoreTagsController@read');
            Route::post('/tags/update', 'StoreTagsController@update');
            Route::post('/tags/add', 'StoreTagsController@add');
            Route::get('/tags/del', 'StoreTagsController@del');
            Route::post('/tags/save', 'StoreTagsController@save');


            Route::get('/seat/list', 'StoreSeatController@index');
            Route::get('/seat/read', 'StoreSeatController@read');
            Route::post('/seat/update', 'StoreSeatController@update');
            Route::post('/seat/add', 'StoreSeatController@add');
            Route::get('/seat/del', 'StoreSeatController@del');
            Route::any('/seat/qrcode', 'StoreSeatController@generateSeatQrCode');



            Route::get('/coupons/list', 'StoreCouponsController@index');
            Route::get('/coupons/read', 'StoreCouponsController@read');
            Route::post('/coupons/update', 'StoreCouponsController@update');
            Route::post('/coupons/add', 'StoreCouponsController@add');
            Route::get('/coupons/del', 'StoreCouponsController@del');
            Route::post('/coupons/generate/qrcode', 'StoreCouponsController@generateQrCode');
            Route::any('/user/coupon/use', 'StoreCouponsController@getUserUseCoupon');
            Route::any('/user/coupon/kind', 'StoreCouponsController@getUserKindCoupon');
            Route::post('/user/coupon/kind/use', 'StoreCouponsController@userUserKindCoupon');
            Route::post('/check/coupon', 'StoreCouponsController@checkUserCoupon');

            Route::get('/activity/list','StoreActivityController@activityList');
            Route::post('/activity/update','StoreActivityController@activityUpdate');
            Route::post('/activity/add','StoreActivityController@activityAdd');
            Route::post('/activity/del','StoreActivityController@activityDelete');


            Route::get('/good/tags/list', 'GoodTagsController@index');
            Route::get('/good/tags/read', 'GoodTagsController@read');
            Route::post('/good/tags/update', 'GoodTagsController@update');
            Route::post('/good/tags/add', 'GoodTagsController@add');
            Route::get('/good/tags/del', 'GoodTagsController@del');


            Route::get('/fans/list','StoreFansController@index');
            Route::get('/fans/read','StoreFansController@read');

            Route::get('/withdraw/list','StoreWithdrawController@withdrawList');
            Route::post('/withdraw/status','StoreWithdrawController@updateWithdrawStatus');
            Route::get('/withdraw/turnover','StoreWithdrawController@getStoreTurnover');
            Route::post('/withdraw/application','StoreWithdrawController@applicationWithdraw');


            Route::any('/navs/list', 'StoreNavsController@index');
            Route::post('/navs/update', 'StoreNavsController@update');
            Route::post('/navs/add', 'StoreNavsController@add');
            Route::any('/navs/del', 'StoreNavsController@del');

            Route::any('/lineup/list', 'StoreLineupController@storeLineupList');
            Route::post('/lineup/update', 'StoreLineupController@storeLineupUpdate');
            Route::post('/lineup/delete', 'StoreLineupController@storeLineupDelete');


            Route::any('/set/operate', 'StoreController@setOperate');


            Route::any('/address/list', 'StoreAddressController@index');
            Route::any('/address/read', 'StoreAddressController@read');
            Route::post('/address/update', 'StoreAddressController@update');
            Route::post('/address/add', 'StoreAddressController@add');
            Route::any('/address/del', 'StoreAddressController@del');


            Route::any('/message/list', 'StoreMessageController@messageList');
            Route::any('/message/read', 'StoreMessageController@messageRead');

            Route::any('/about/list', 'StoreAboutController@index');
            Route::any('/about/info', 'StoreAboutController@aboutInfo');
            Route::post('/about/set', 'StoreAboutController@setAbout');

            Route::any('/order/express/delivery', 'OrderController@getExpressDelivery');
            Route::post('/confirm/orders', 'OrderController@confirmOrders');

            Route::any('/turnover', 'StoreController@getStoreTurnover');
            Route::any('/generate/qrcode', 'StoreController@generateStoreQrCode');
            Route::post('/balance/pay', 'OrderController@balanceQrCodePay');
        });

});
