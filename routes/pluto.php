<?php

Route::group(['namespace'=>'Pluto', 'middleware' => ['origin']], function (){

    Route::options('/{all}', function(\Illuminate\Http\Request $request) {
        $origin = $request->header('ORIGIN', '*');
        header("Access-Control-Allow-Origin: $origin");
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, token, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie');
    })->where(['all' => '([a-zA-Z0-9-]|/)+']);

    //支付宝订单回调
    Route::any('/order/pay/ali/notify', 'OrderController@aliPayNotify');
    //支付宝充值回调
    Route::any('/user/pay/ali/recharge/notify', 'UserController@aliPayRechargeNotify');
    //支付宝停车场回调
    Route::any('/parking/pay/ali/notify', 'ParkingController@parkingAliPayNotify');
    //预订支付宝回调
    Route::any('/store/book/pay/alipay/notify', 'StoreController@storeBookAliPayNotify');


    //APP微信订单回调
    Route::any('/order/pay/wechat/app/notify', 'OrderController@orderAppWeChatPayCallback');
    //APP微信充值回调
    Route::any('/user/pay/wechat/recharge/notify', 'UserController@weChatPayRechargeNotify');
    //APP微信预订回调
    Route::any('/store/book/pay/wechat/notify', 'StoreController@storeBookWeChatPayNotify');
    //APP微信停车场回调
    Route::any('/parking/pay/wechat/notify', 'ParkingController@parkingAppWeChatPayNotify');

    Route::any('/order/balance/callback', 'OrderController@balanceCallBack');

    //拼团支付宝支付回调
    Route::any('/order/group/alipay/notify', 'OrderController@groupBuyAliPay');
    //拼团微信支付回调
    Route::any('/order/group/app/wechat/pay/notify', 'OrderController@groupBuyAliPay');



    Route::any('/index', 'IndexController@xhlmxc');//官网
    Route::group(['middleware' => ['getCompanyId']], function (){
        //抽奖
        Route::any('lottery', 'IndexController@getLotteryCoupon');
        //轮播图
        Route::any('/banner', 'IndexController@bannerList');
        //首页配置
        Route::any('/page/config', 'IndexController@pageConfig');
        //关于我们
        Route::any('/about', 'IndexController@about');
        //公益活动列表
        Route::any('/activity/list', 'IndexController@activity');
        //公益活动详情
        Route::any('/activity', 'IndexController@activityInfo');
        //支付方式
        Route::any('/pay/type', 'IndexController@getPaymentMethod');

        //订单核销码
        Route::any('/order/code/{id}', 'OrderController@orderCode');

        /**
         * 用户
         */
        Route::group(['prefix' => 'user'], function (){
            //停车场科拓测试
            Route::post('/ketuo', 'UserController@ketuotest');

            //通过手机号注册账号
            Route::post('/register/mobile', 'UserController@registerForMobile');

            //通过账号密码登录
            Route::post('/login/password', 'UserController@loginByPassword');
            //通过手机号验证码登录
            Route::post('/login/vc', 'UserController@loginByVc');

            //用户退出登录
            Route::post('/logout', 'UserController@loginOut');
            //关键词搜索
            Route::any('/keyword/search', 'UserController@keywordSearch');
            //关键词列表
            Route::any('/keyword/list', 'UserController@keywordList');
            //apiAuth中间件需要登录
            Route::group(['middleware' => 'apiAuth'], function (){
                //获取用户优惠券详情
                Route::post('/coupon/info', 'UserController@couponInfo');
                //获取用户签到时间
                Route::any('/sign/time', 'UserController@getMySignTime');
                //获取用户注册礼包
                Route::any('/offline/coupon', 'UserController@getOfflineCoupon');
                //获取用户余额支付二维码
                Route::post('/pay/qrcode', 'UserController@getUserPayQrCode');
                //我的线下优惠券列表
                Route::any('/offline/coupons', 'UserController@myOfflineCoupon');
                //用户升级
                Route::post('/upgrade', 'UserController@userUpgrade');
                //填写邀请码
                Route::post('/invite', 'UserController@inviteUser');
                //发送邮箱验证码
                Route::post('/send/email', 'UserController@sendEmail');
                //绑定邮箱
                Route::post('/bind/email', 'UserController@bindEmail');
                //用户推广码
                Route::any('/qrcode', 'UserController@getUserRecommendQrCode');
                //用户消息列表
                Route::any('/message', 'UserController@getUserMessage');
                //获取用户红包
                Route::post('/red/envelope', 'UserController@getUserRedEnvelope');

                //抽奖
                Route::post('/lottery', 'UserController@userLottery');
                //兑换积分商品
                Route::any('/exchange/goods', 'UserController@exchangeGoods');

                //清除历史搜索记录
                Route::any('/keyword/empty', 'UserController@emptyKeyword');

                /**
                 * 用户收货地址CURD
                 */
                Route::post('/address/add', 'UserController@addUserAddress');
                Route::post('/address/update', 'UserController@updateUserAddress');
                Route::post('/address/delete/{id}', 'UserController@deleteUserAddress');
                Route::any('/address', 'UserController@userAddress');
                Route::any('/address/default', 'UserController@userAddressDefault');
                Route::any('/address/{id}', 'UserController@userAddressInfo');

                //领取优惠券
                Route::post('/receive/coupon', 'UserController@receiveCoupon');
                //用户优惠券列表
                Route::any('/coupon', 'UserController@myCoupon');
                //用户兑换余额优惠券
                Route::post('/coupon/exchange/balance','UserController@exchangeBalanceCoupon');
                //用户领取积赞优惠券
                Route::post('/coupon/scan/code','UserController@receiveScanCodeCoupon');
                //分享增加抽奖次数
                Route::post('/share/lottery','UserController@shareLottery');
                /**
                 * 用户信息
                 */
                //当前登录用户的信息
                Route::any('/current', 'UserController@currentLoginUserInfo');
                //根据ID获取用户信息
                Route::any('/info', 'UserController@getUserInfoById');
                //会员权益
                Route::any('/equity', 'UserController@equity');
                //邀请好友
                Route::any('/invitation', 'UserController@invitation');
                //修改登录密码
                Route::post('/change/password', 'UserController@changePassword');
                //修改用户信息
                Route::post('/update/msg', 'UserController@updateUserInfo');
                //修改绑定手机号
                Route::post('/update/mobile', 'UserController@updateUserMobile');
                //钱包明细
                Route::any('/stream', 'UserController@userStream');
                //付款码
                Route::any('/paycode', 'UserController@userPaycode');
                //用户收藏
                Route::any('/collect/list', 'UserController@myCollect');
                //客服中心列表
                Route::any('/service', 'UserController@userService');
                //客服详情
                Route::any('/service/{id}', 'UserController@userServiceInfo');
                //意见反馈
                Route::any('/feedback', 'UserController@userFeedback');
                //公益活动报名
                Route::any('/activity/apply', 'UserController@userActivityApply');
                //我参与的公益活动
                Route::any('/activity/myapply', 'UserController@userActivityMyApply');
                //添加浏览记录
                Route::any('/view/add', 'UserController@viewRecordingAdd');
                //浏览记录
                Route::any('/view', 'UserController@viewRecordingList');
                /**
                 * 小程序绑定手机号
                 */
                Route::post('/wxmini/bind/mobile', 'UserController@wxMiniBindMobile');

                /**
                 * 收藏
                 */
                Route::post('/collect/store', 'UserController@collectStore');
                Route::post('/collect/goods', 'UserController@collectGoods');
                Route::post('/collect/store/cancel', 'UserController@cancelCollectStore');
                Route::post('/collect/goods/cancel', 'UserController@cancelCollectGoods');
                Route::any('/collect/list', 'UserController@myCollect');

                /**
                 * 支付
                 */
                //充值订单详情
                Route::any('/recharge/log', 'UserController@rechargeLog');
                //初始化充值订单
                Route::any('/recharge/int', 'UserController@initRecharge');
                //余额充值微信支付（中信）
                Route::any('/recharge/pay', 'UserController@rechargePay');
                //余额充值APP微信支付
                Route::any('/recharge/wehcat/pay', 'UserController@weChatPayRecharge');

                //历史购买记录
                Route::any('/history/buy', 'UserController@historyBuy');
                //设置支付密码
                Route::post('/setting/pay/password', 'UserController@settingPayPassword');
                //检测是否设置支付密码
                Route::post('/check/pay/password', 'UserController@checkPayPassword');
                //APP支付宝用户充值
                Route::post('/pay/ali/recharge', 'UserController@aliPayRecharge');
                //用户签到
                Route::any('/sign/in', 'UserController@userSignIn');

            });
            //余额充值微信支付会调（中信）
            Route::any('/pay/zhongxin/back', 'UserController@rechargePayCallback');



        });

        /**
         * 商品
         */
        Route::group(['prefix' => 'goods'], function (){
            //获取商品分类
            Route::any('/classify', 'GoodsController@goodsClassify');
            //获取商品列表
            Route::any('/list', 'GoodsController@goodsList');
            //获取商品详情
            Route::any('/info', 'GoodsController@goodsInfo');
            //秒杀时间
            Route::any('/splike/config', 'GoodsController@splikeConfig');
            //获取商品分类商品
            Route::any('/classify/good', 'GoodsController@classifyGoods');

            Route::group(['middleware' => 'apiAuth'], function (){
                //添加商品评论
                Route::post('/user/estimates', 'GoodsController@userEstimatesGoods');
                Route::any('/recommend', 'GoodsController@recommendGoods');
            });




        });

        /**
         * 店铺
         */
        Route::group(['prefix' => 'store'], function (){
            //休闲娱乐推荐商户
            Route::any('/recommend/leisure', 'StoreController@recommendLeisureStore');
            //获取商户详细信息
            Route::any('/detailed', 'StoreController@getStoreInfo');
            //菜系列表
            Route::any('/cuisine', 'StoreController@cuisineList');
            //获取店铺分类
            Route::any('/classify', 'StoreController@storeClassify');
            //获取店铺列表
            Route::any('/list', 'StoreController@storeList');
            //返回餐饮菜单特殊格式
            Route::any('/menu', 'StoreController@storeMenu');
            //获取店铺详情
            Route::any('/info', 'StoreController@storeInfo');
            //店铺评论列表
            Route::any('/estimates/list', 'StoreController@estimatesList');
            //店铺评论详情
            Route::any('/estimates/info', 'StoreController@estimatesInfo');
            //店铺评论标签
            Route::any('/estimates/tags', 'StoreController@estimatesTags');
            //店铺相册列表
            Route::any('/image/list', 'StoreController@imageList');
            //商户标签商品
            Route::any('/tag/good', 'StoreController@storeTagGoods');
            //店铺左侧导航
            Route::any('/nav/{storeId}', 'StoreController@storeNav');
            //店铺排队数据统计
            Route::any('/seat/info/{storeId}', 'StoreController@storeSeatInfo');
            //获取优惠券
            Route::any('/coupon','StoreController@storeCoupon');
            //领券中心，所有商户未过期的优惠券
            Route::any('/coupon/center', 'StoreController@couponsCenter');
            //商户座位列表
            Route::any('/seat/list', 'StoreController@seatList');
            //获取休闲娱乐商家评论
            Route::any('/leisure/comment', 'StoreController@getStoreComment');
            //获取品牌商家
            Route::any('/brand', 'StoreController@getStoreBrand');
            //获取商户活动
            Route::any('/activity', 'StoreController@getStoreActivity');

            Route::group(['middleware' => 'apiAuth'], function (){

                /**
                 * 排号
                 */
                //排队
                Route::post('/seat/lineup', 'StoreController@storeSeatLineUp');
                //获取排队信息
                Route::any('/seat/lineup/info', 'StoreController@storeSeatLineUpInfo');
                //我的排号信息
                Route::any('/seat/lineup/list', 'StoreController@storeSeatLineUpList');
                //取消排号
                Route::post('/seat/lineup/delete', 'StoreController@deleteStoreSeatLineUp');

                /**
                 * 预订
                 */
                //确认预定包厢
                Route::post('/ent/book', 'StoreController@enterBoxBook');
                //预订
                Route::post('/book', 'StoreController@storeBook');
                //获取预订详细信息
                Route::any('/book/info', 'StoreController@storeBookInfo');
                //我的预订
                Route::any('/book/list', 'StoreController@myStoreBook');
                //取消预约
                Route::post('/book/cancel', 'StoreController@cancelStoreBook');
                //预定余额支付
                Route::post('/book/pay/balance', 'StoreController@storeBookBalancePay');
                //预定支付宝支付
                Route::post('/book/pay/alipay', 'StoreController@storeBookAliPay');
                //预订评论
                Route::post('/book/comment', 'StoreController@bookComment');
                //预订退款
                Route::post('/book/refund', 'StoreController@bookRefund');
                //APP预订微信支付
                Route::post('/book/pay/wechat', 'StoreController@storeBookWeChatPay');


            });
            //预约配置信息
            Route::any('/book/config/{storeId}', 'StoreController@storeBookConfig');

        });

        /**
         * 购物车和订单
         */
        Route::group(['prefix' => 'order'], function (){
            //订单微信支付会调（中信）
            Route::post('/pay/zhongxin/callback', 'OrderController@orderPayCallback');

            Route::group(['middleware' => 'apiAuth'], function (){
                //获取我的砍价订单
                Route::any('/bargain', 'OrderController@getMyBargainOrder');
                //砍价订单详情
                Route::any('/bargain/info', 'OrderController@getMyBargainOrderInfo');
                //创建砍价订单
                Route::post('/bargain/create', 'OrderController@createBargainOrder');
                //砍价订单创建普通订单
                Route::post('/bargain/by/order', 'OrderController@getBargainByOrder');
                //加入砍价 帮人砍价
                Route::post('/bargain/join', 'OrderController@joinBargainOrder');
                //开启拼团
                Route::post('/start/group/buy', 'OrderController@startGroupOrder');
                //加入拼团
                Route::post('/join/group/buy', 'OrderController@joinGroupOrder');
                //我的拼团订单
                Route::any('/my/group/buy', 'OrderController@myOrderGroupBuy');
                //获取当前商品正在拼团订单
                Route::any('/goods/group/buy', 'OrderController@getGoodsGroupBuyOrder');
                //拼团支付宝支付
                Route::post('/group/buy/alipay', 'OrderController@groupBuyAliPay');
                //拼团微信支付
                Route::post('/group/buy/wechat/pay', 'OrderController@groupBuyWeChatPay');
                //拼团余额支付
                Route::post('/group/buy/balance/pay', 'OrderController@groupBuyBalancePay');
                //申请退款
                Route::any('/apply/refund', 'OrderController@applyOrderRefund');
                //查询快递
                Route::any('/express/delivery', 'OrderController@getOrderExpressDelivery');
                //微信订单支付（中信）
                Route::post('/pay/zhongxin', 'OrderController@orderPay');
                //退款（中信）
                Route::post('/pay/zhongxin/refund', 'OrderController@orderZhongxinRefund');
                //订单APP微信支付
                Route::post('/pay/wechat/app', 'OrderController@orderAppPay');
                //订单支付宝支付
                Route::post('/pay/ali', 'OrderController@aliPay');
                //余额支付
                Route::post('/balance/pay', 'OrderController@balancePay');
                //添加或者更新购物车
                Route::post('/cart', 'OrderController@addOrUpdateCart');
                //删除购物车
                Route::post('/cart/delete', 'OrderController@deleteCart');
                //购物车计算价格
                Route::any('/cart/chose', 'OrderController@choiceCart');
                //购物车列表
                Route::any('/cart/list', 'OrderController@cartList');
                //购物车使用优惠券
                Route::any('/cart/coupons', 'OrderController@orderUseCoupons');
                //确认订单
                Route::any('/enter', 'OrderController@orderEnter');
                //扫码点餐的购物车
                Route::any('/seat/cart/list/', 'OrderController@cartListBySeat');
                //外卖的购物车
                Route::any('/store/cart/list/', 'OrderController@cartListByStore');
                //立即购买提交
                Route::post('/buy', 'OrderController@buyNow');
                //立即购买初始化订单
                Route::post('/initbuy', 'OrderController@initOrderBuyNow');
                //重新下单
                Route::post('/reorder', 'OrderController@reorder');
                //立即购买初始化订单
                Route::post('/init', 'OrderController@initOrder');
                //根据批次号获取订单信息
                Route::any('/', 'OrderController@orderByBatch');
                //获取订单列表
                Route::any('/list', 'OrderController@orderList');
                //订单详情
                Route::any('/info', 'OrderController@orderInfo');
                //修改订单地址
                Route::post('/update/address', 'OrderController@updateOrderAddress');
                //更新订单优惠券
                Route::post('/update/coupon', 'OrderController@updateOrderCoupon');
                //修改订单状态
                Route::any('/update/status', 'OrderController@updateOrderStatus');
                //通过订单批次号修改订单配送方式
                Route::any('/update/type','OrderController@orderDeliveryUpdate');
                //获取送餐时间
                Route::any('/cart/takeaway/time','OrderController@getTakeawayTime');
                //订单使用红包
                Route::post('/use/red/envelope', 'OrderController@orderUseRedEnvelope');

                Route::post('/balance/scan/code/pay', 'OrderController@getBalanceScanCodePay');
            });



        });
    });



    /**
     * 微信
     */
    Route::group(['prefix' => 'wechat'], function (){

        Route::any('/checksignature', 'WechatController@checkSignature');
        //收集小程序提交过来的FormId和PayId
        Route::any('/saveids', 'WechatController@saveFormidOrPayid');

        Route::group(['middleware' => 'wechatConfig'], function () {
            //小程序登录
            Route::post('/login/mini', 'WechatController@loginForMini');
            //小程序授权更新用户信息
            Route::post('/authorize/mini', 'WechatController@authorizeForMini');
        });


        Route::group(['middleware' => 'getCompanyId'], function (){
            //登录
            Route::any('/open-platform/login/{appid}', 'WechatController@openPlatformLogin');
            //登录回调
            Route::any('/open-platform/login/callback/{appid}', 'WechatController@openPlatformLoginCallback');
        });

        ////发起授权
        Route::any('/open-platform/pre-auth', 'WechatController@openPlatformPreAuth');
        ////授权回调
        Route::any('/open-platform/callback', 'WechatController@openPlatformPreAuthCallback');
        Route::any('/open-platform', 'WechatController@openPlatform');
        Route::any('/open-platform/jssdk/{appid}', 'WechatController@openPlatformJsSdk');
        ////事件监听
        Route::any('/open-platform/{appid}/callback', 'WechatController@openPlatformCallback');
    });

    /**
     * 种草
     */
    Route::group(['prefix' => 'grass'], function (){
        Route::group(['middleware' => 'apiAuth'], function (){
            //发布种草社区
            Route::post('/save', 'PlantingGrassController@addGrass');
            //发布种草社区评论
            Route::post('/comment/save', 'PlantingGrassController@grassCommentAdd');
            //种草社区贴子点赞（取消赞）
            Route::post('/awesome', 'PlantingGrassController@grassAwesomeSave');
            //隐藏社区贴子（评论）
            Route::post('/hidden', 'PlantingGrassController@grassHidden');
            //删除种草社区贴子（评论）
            Route::post('/remove', 'PlantingGrassController@grassDelete');
            //种草社区贴子评论点赞
            Route::post('/comment/awesome', 'PlantingGrassController@grassCommentAwesome');
            //种草社区添加收藏
            Route::post('/collection/save', 'PlantingGrassController@grassCollectionSave');
            //删除收藏种草社区贴子
            Route::post('/collection/remove', 'PlantingGrassController@grassCollectionDelete');
            //种草关注（取消关注）
            Route::any('/attention', 'PlantingGrassController@userAttention');

        });
        //获取种草社区话题分类
        Route::any('/classify', 'PlantingGrassController@getPlantingClassify');
        //种草社区列表
        Route::any('/list', 'PlantingGrassController@grassList');
        //种草详情
        Route::any('/info', 'PlantingGrassController@grassInfo');
    });


    //check（用于小程序发布审核）
    Route::any('/check', 'IndexController@check');


    Route::any('/pay/wx/back', 'UserController@rechargeWxPayCallback');
    //检测手机验证码是否正确
    Route::any('/check/mobile', 'IndexController@checkMobile');
    //检测手机验证码是否正确
    Route::any('/check/mobile/code', 'IndexController@checkMobileCode');
    //忘记密码
    Route::any('/modify/password', 'UserController@modifyPassword');


    Route::group(['prefix' => 'park', 'middleware' => 'apiAuth'], function (){
        //绑定用户车牌号码
        Route::post('/user/tie/number', 'ParkingController@tieNumber');
        //停车场规则说明
        Route::any('/rule', 'ParkingController@getParkingRule');
        //获取停车场信息和车牌号
        Route::any('/info', 'ParkingController@getParkingInfo');
        //缴费记录
        Route::any('/order', 'ParkingController@parkingOrderList');
        //获取停车费用
        Route::any('/get/payment', 'ParkingController@getParkingPayment');
        //停车场支付宝支付
        Route::any('/alipay', 'ParkingController@parkingAliPay');
        //APP停车场微信支付
        Route::any('/wechat', 'ParkingController@parkingAppWeChatPay');
        //删除车牌号
        Route::post('/delete/number', 'ParkingController@deleteNumber');
        //停车场余额支付
        Route::post('/recharge', 'ParkingController@parkingRechargePay');
    });


    //获取签列表
    Route::any('/game/draw', 'IndexController@getGameDraw');


    //除app、微信扫描商家座位二维码内容
    Route::any('/store', 'IndexController@storeSeatQrCode');
    //检查更新app版本
    Route::post('/version', 'IndexController@getVersion');
    //获取QA问答问题列表
    Route::any('/question', 'IndexController@getQuestion');
    //获取QA问答答案
    Route::any('/answer', 'IndexController@getAnswer');
    //用户批量注册云信
    Route::any('/yunxin/user', 'IndexController@registerUserYunXin');
    //商户批量注册云信
    Route::any('/yunxin/store', 'IndexController@registerStoreYunXin');
    Route::any('/yunxin/create', 'IndexController@createUserYunXin');
    Route::any('/registerStoreTakeaway', 'IndexController@registerStoreTakeaway');
    Route::any('/orderQuery', 'IndexController@orderQuery');
    Route::any('/dianWoDaRollBack', 'IndexController@dianWoDaRollBack');

    Route::any('/download', 'IndexController@download');

    Route::any('/downStore', 'IndexController@downStore');
    Route::any('/generateQrCode', 'IndexController@generateQrCode');
    Route::any('/manual/order/refundPay', 'IndexController@manualOrderRefundPay');
    Route::any('/addStoreGoods', 'IndexController@addStoreGoods');
    Route::any('/recoverOrderAmount', 'IndexController@recoverOrderAmount');

    Route::any('/saveStoreTurnover', 'IndexController@saveStoreTurnover');
    Route::any('/push', 'IndexController@testPush');
    Route::get('/agreement/service/store', 'IndexController@storeServiceAgreement');
    Route::get('/agreement/service/user', 'IndexController@userServiceAgreement');
    Route::get('/agreement/food', 'IndexController@foodAgreement');
    Route::get('/agreement/privacy', 'IndexController@privacyAgreement');

});



