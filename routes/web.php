<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "Web" middleware group. Now create something great!
|
*/

use function foo\func;

Route::get('/', function () {
    return view('welcome');
});


Route::any('/test', 'Pluto\\IndexController@xhlmxc');//官网


Route::group(['namespace'=>'Web', 'middleware' => ['origin']], function () {
    Route::options('/{all}', function(\Illuminate\Http\Request $request) {
        $origin = $request->header('ORIGIN', '*');
        header("Access-Control-Allow-Origin: $origin");
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: x-xsrf-token, x-token, Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, token, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie');
    })->where(['all' => '([a-zA-Z0-9-]|/)+']);

    Route::group(['prefix' => 'web'], function () {
        Route::post('/login', 'LoginController@login');
        Route::get('/test', 'LoginController@testRedis');


        Route::group(['middleware' => 'CheckWeb'], function () {

                Route::post('/shop/set', 'ShopController@set');
                Route::post('/about', 'ShopController@about');
                Route::get('/about/info', 'ShopController@aboutInfo');
                Route::get('/banner/list', 'BannerController@index');
                Route::post('/banner/add', 'BannerController@add');
                Route::post('/banner/update', 'BannerController@update');
                Route::get('/banner/del', 'BannerController@del');

                Route::post('/setAlertOver', 'ConfigureController@setAlertOver');



            Route::group(['prefix' => 'member'], function () { //用户列表
                Route::get('/list', 'MemberController@index');
                Route::get('/read', 'MemberController@read');
                Route::get('/del', 'MemberController@del');
                Route::post('/recharge', 'MemberController@recharge');
                Route::post('/setting/vip', 'MemberController@settingUserVip');
                Route::post('/balanceIs/ten', 'MemberController@getUserBalanceIsTen');
            });
            Route::group(['prefix' => 'statistics'], function () { //平台首页
                Route::any('/index', 'StatisticsController@index');
                Route::post('/total', 'StatisticsController@StatisticsTotal');

                Route::any('/goods', 'StatisticsController@getGoodsSalesRanking');
                Route::any('/user', 'StatisticsController@getUserData');
                Route::any('/user/register', 'StatisticsController@getUserRegister');
                Route::any('/coupon', 'StatisticsController@getCouponIssue');
                Route::any('/coupon/use', 'StatisticsController@getCouponUse');

                Route::any('/parking', 'StatisticsController@getParkingTotal');
                Route::any('/manage', 'StatisticsController@getManageTurnover');
                Route::any('/turnover', 'StatisticsController@platformTurnover');

                Route::any('/merchant', 'StatisticsController@getMerchantStatistics');
                Route::any('/time/order', 'StatisticsController@getTimeOrder');
                Route::any('/time/order/goods', 'StatisticsController@getOrderGoods');
                Route::any('/goods/statistics', 'StatisticsController@getGoodsStatistics');
                Route::any('/merchant/rating', 'StatisticsController@getMerchantRating');
                Route::any('/user/consumption', 'StatisticsController@getUserConsumption');
                Route::any('/store/export/turnover', 'StatisticsController@getStoreExportTurnover');
                Route::any('/store/export/turnovernew', 'StatisticsController@getStoreExportTurnoverNew');
            });
            Route::group(['prefix' => 'finance'], function () {  //充值积分金额列表
                Route::get('/index', 'FinanceController@index');

            });
            Route::group(['prefix' => 'configure'], function () {  //微信conf
                Route::get('/index', 'ConfigureController@index');
                Route::post('/confUpdate', 'ConfigureController@confUpdate');//更改配置
            });
//            Route::group(['prefix' => 'tag'], function () {  //商户标签
//                Route::get('/index', 'TagController@index');//标签列表
//                Route::post('/addTag', 'TagController@addTag');//添加商户标签
//                Route::get('/delTag/{id}', 'TagController@delTag');//删除标签
//            });
            Route::group(['prefix' => 'store'], function () { //商户列表
                Route::any('/export/order', 'StoreController@getStoreOrderExportData');
                Route::get('/get/class','StoreClassifityController@getStoreClass');
                Route::get('/index', 'StoreController@index');
                Route::post('/update', 'StoreController@setStoreMsg');
                Route::any('/info', 'StoreController@storeInfo');
                Route::post('/add', 'StoreController@storeAdd');
                Route::post('/classify/list', 'StoreController@classifyList');
                Route::get('/type/list', 'StoreController@typeList');
                Route::get('/del', 'StoreController@storeDel');
                Route::post('/update/status', 'StoreController@updateStoreStatus');
                Route::post('/update/basic', 'StoreController@updateBasicInfo');

                Route::post('/upload/contract', 'StoreController@uploadStoreContract');
                Route::any('/get/contract', 'StoreController@getStoreContract');
                Route::any('/type', 'StoreController@getStoreType'); //获取商家类型
                Route::any('/integral', 'StoreController@integral');//积分明细
                Route::any('/integral/lists', 'StoreController@integralLists');//积分明细
                Route::any('/collect', 'StoreController@collectStore'); //店铺收藏
                Route::any('/collect/list', 'StoreController@collectList'); //店铺收藏列表
                Route::any('/collect/cancel', 'StoreController@collectCancel'); //批量取消收藏

                //线下优惠券（注册礼包）CURD
                Route::any('/offline/coupons', 'OfflineCouponsController@offlineCouponsList');
                Route::any('/offline/coupons/save', 'OfflineCouponsController@saveOfflineCoupons');
                Route::any('/offline/coupons/remove', 'OfflineCouponsController@removeOfflineCoupon');

                Route::group(['prefix' => 'lease'], function (){
                    //商家租赁信息列表
                    Route::any('/list', 'StoreLeaseController@getStoreLeaseList');
                    //商家租赁信息添加
                    Route::post('/save', 'StoreLeaseController@saveStoreLease');
                    //商家租赁缴费
                    Route::post('/payment', 'StoreLeaseController@storePayment');
                    //导入商家缴费信息
                    Route::post('/import', 'StoreLeaseController@saveStoreLeaseBysExcel');
                    //获取商家租赁缴费记录
                    Route::any('/recording', 'StoreLeaseController@paymentRecording');
                });


                Route::group(['prefix' => 'chosen'], function () {
                    Route::post('/del', 'StoreController@deleteChosenStore');
                    Route::post('/update', 'StoreController@updateChosenStore');
                    Route::post('/add', 'StoreController@addChosenStore');
                });

                Route::group(['prefix' => 'class'], function () { //商户分类管理
                    Route::any('/list', 'StoreClassifityController@index');//商户分类列表
                    Route::any('/read', 'StoreClassifityController@read');//商户分类详情
                    Route::post('/update', 'StoreClassifityController@update');//商户分类修改
                    Route::post('/add', 'StoreClassifityController@add');//商户分类添加
                    Route::any('/del', 'StoreClassifityController@del');//商户分类删除
                });
                Route::group(['prefix' => 'devices'], function () { //商户服务管理
                    Route::get('/list', 'DevicesController@index');
                    Route::get('/read', 'DevicesController@read');
                    Route::post('/update', 'DevicesController@update');
                    Route::post('/add', 'DevicesController@add');
                    Route::get('/del', 'DevicesController@del');
                });
                Route::group(['prefix' => 'tags'], function () { //商户标签管理
                    Route::get('/list', 'TagsController@index');
                    Route::get('/read', 'TagsController@read');
                    Route::post('/update', 'TagsController@update');
                    Route::post('/add', 'TagsController@add');
                    Route::get('/del', 'TagsController@del');
                });

                Route::group(['prefix' => 'activity'], function () { //商户活动标签管理
                    Route::get('/tags/list', 'StoreActivityTagsController@getStoreActivityTagsList');
                    Route::post('/tags/update', 'StoreActivityTagsController@updateStoreActivityTags');
                    Route::post('/tags/add', 'StoreActivityTagsController@addStoreActivityTags');
                    Route::post('/tags/delete', 'StoreActivityTagsController@deleteStoreActivityTags');
                });

                Route::group(['prefix' => 'leisure'], function () { //休闲娱乐推荐商家
                    Route::get('/list', 'StoreController@getRecommendLeisureStore');
                    Route::post('/save', 'StoreController@recommendLeisureStoreSave');
                    Route::post('/remove', 'StoreController@recommendLeisureStoreRemove');
                });

                Route::group(['prefix' => 'brand'], function () { //品牌商家
                    Route::get('/list', 'StoreController@getStoreBrand');
                    Route::post('/save', 'StoreController@saveStoreBrand');
                    Route::post('/remove', 'StoreController@removeStoreBrand');
                });
            });

            Route::group(['prefix' => 'agent'], function () { //代理管理
                Route::any('/index', 'AgentController@index');
                Route::any('/info', 'AgentController@agentInfo');
                Route::any('/exam', 'AgentController@examAgent');
//                Route::post('/consent', 'AgentController@consentAgent');

            });

            Route::group(['prefix' => 'good'], function () { //商品管理
                Route::any('/list', 'GoodController@index');
                Route::any('/read', 'GoodController@read');//商品详情
                Route::any('/read/app', 'GoodController@readApp');//商品详情app版
                Route::post('/add', 'GoodController@create');
                Route::any('/del', 'GoodController@del');
                Route::any('/recovery', 'GoodController@recovery');//商品回收
                Route::any('/classify', 'GoodController@classify');//商品分类列表
//                Route::post('/update', 'GoodController@update');//商品修改
                Route::any('/status', 'GoodController@goodStatus');//商品状态修改

                Route::post('/save', 'GoodController@goodsSave');//商品修改

                Route::any('/copy', 'GoodController@copy');//一键复制
                Route::any('/collect', 'GoodController@collectGood'); //商品收藏
                Route::any('/collect/list', 'GoodController@collectList'); //商品收藏列表
                Route::any('/collect/cancel', 'GoodController@collectCancel'); //批量取消收藏

                Route::group(['prefix' => 'class'], function () { //商品分类管理
                    Route::any('/list', 'GoodsClassifiesController@index');//商品分类列表
                    Route::any('/read', 'GoodsClassifiesController@read');//商品分类详情
                    Route::post('/update', 'GoodsClassifiesController@update');//商品分类修改
                    Route::post('/add', 'GoodsClassifiesController@add');//商品分类添加
                    Route::any('/del', 'GoodsClassifiesController@del');//商品分类删除
                });

                Route::group(['prefix' => 'cuisine'], function () { //商品菜单
                    Route::any('/list', 'GoodsCuisineController@list');//商品菜系列表
                    Route::post('/add', 'GoodsCuisineController@add');//商品菜系添加
                    Route::post('/update', 'GoodsCuisineController@update');//商品菜系修改
                    Route::post('/delete', 'GoodsCuisineController@delete');//商品菜系删除
                });

            });
            Route::group(['prefix' => 'estimates'], function () { //商品评论管理
                Route::any('/list', 'EstimatesController@index');//商品评论列表
                Route::any('/read', 'EstimatesController@read');//商品评论详情
                Route::any('/del', 'EstimatesController@del');//商品评论删除
                Route::post('/reply ', 'EstimatesController@reply');//商品评论回复
                Route::post('/check', 'EstimatesController@checkGoodsEstimate');

                Route::group(['prefix' => 'tags'], function () { //评论关键词管理
                    Route::any('/list', 'EstimatesTagController@index');//评论关键词列表
                    Route::post('/update', 'EstimatesTagController@update');//评论关键词修改
                    Route::post('/add', 'EstimatesTagController@add');//评论关键词添加
                    Route::any('/del', 'EstimatesTagController@del');//评论关键词删除
                });
            });

            Route::group(['prefix' => 'navs'], function () { //页面导航管理
                Route::any('/list', 'NavsController@index');
                Route::post('/update', 'NavsController@update');
                Route::post('/add', 'NavsController@add');
                Route::any('/del', 'NavsController@del');
            });

            Route::post('/add/store', 'StoreController@addStoreBasic');

            /**
             * 权限管理
             */
            Route::group(['prefix' => 'user'], function () {

                Route::any('/recharge/list', 'UserRechargeController@list');//获取用户充值记录

                Route::any('/admin/role', 'PermissionController@getRole');//获取管理员的菜单列表


                Route::any('/admin/info', 'PermissionController@getAdminUserInfo');
                Route::get('/index', 'PermissionController@adminUser');  //管理员列表
                Route::any('/info', 'PermissionController@adminUserInfo');  //管理员详细信息
                Route::post('/add','PermissionController@addAdminUser');  //添加管理员
                Route::post('/update','PermissionController@updateAdminUser');  //修改管理员信息
                Route::post('/delete','PermissionController@deleteAdminUser');  //删除管理员

                Route::group(['prefix' => 'sidebar'], function () {
                    Route::get('/list', 'PermissionController@sidebarList');
                    Route::post('/', 'PermissionController@getSidebar');
                    Route::get('/user', 'PermissionController@getUserSidebar');
                    Route::post('/user/update', 'PermissionController@updateUserSidebar');
                    Route::post('/update', 'PermissionController@sidebarUpdate');
                    Route::post('/add', 'PermissionController@sidebarAdd');
                    Route::post('/del', 'PermissionController@sidebarDelete');
                });


                Route::group(['prefix' => 'url'], function () {
                    Route::get('/list', 'PermissionController@adminUrlList');  //节点列表
                    Route::get('/info', 'PermissionController@adminUrlInfo');  //节点信息
                    Route::post('/add','PermissionController@addUrl');  //添加节点
                    Route::post('/update','PermissionController@updateUrl');  //修改节点信息
                    Route::post('/delete','PermissionController@deleteUrl');  //删除节点
                });

                Route::any('/feed/back', 'FeedBackController@getUserFeedBack');//获取用户反馈意见

            });


            Route::group(['prefix' => 'grass'], function (){
                Route::any('/list', 'PlantingGrassController@grassList');
                Route::any('/info', 'PlantingGrassController@grassInfo');
                Route::post('/review', 'PlantingGrassController@grassReview');
                Route::post('/delete', 'PlantingGrassController@grassOrCommentDelete');
                Route::any('/classify', 'PlantingGrassController@getPlantingClassify');
                Route::post('/classify/save', 'PlantingGrassController@plantingClassifySave');
                Route::post('/classify/remove', 'PlantingGrassController@plantingClassifyRemove');
            });


            Route::group(['prefix' => 'integral'], function (){
                Route::any('/list', 'IntegralController@index');
                Route::post('/update', 'IntegralController@updateIntegral');
                Route::post('/delete', 'IntegralController@deleteIntegral');
            });


            Route::group(['prefix' => 'activity'], function (){
                Route::any('/list', 'ActivityController@activityList');
                Route::post('/add', 'ActivityController@addActivity');
                Route::post('/update', 'ActivityController@updateActivity');
                Route::post('/delete', 'ActivityController@deleteActivity');
            });

            Route::any('/message', 'StoreMessageController@messageList');



            Route::group(['prefix' => 'authority'], function (){
                Route::post('/tree/route', 'AuthorityController@getTreeRoute');


                Route::post('/user/info', 'AuthorityController@getUserInfo');
                Route::post('/user/add', 'AuthorityController@addAdminUser');
                Route::post('/user/delete', 'AuthorityController@deleteAdminUser');
                Route::post('/user/update', 'AuthorityController@updateAdminUser');
                Route::post('/user/list', 'AuthorityController@adminUserList');

                Route::post('/user/route', 'AuthorityController@getAdminUserRoute');

                Route::any('/menu/list', 'AuthorityController@menuList');
                Route::post('/menu/add', 'AuthorityController@menuAdd');
                Route::post('/menu/update', 'AuthorityController@menuUpdate');
                Route::post('/menu/delete', 'AuthorityController@menuDelete');

                Route::any('/roles/list', 'AuthorityController@rolesList');
                Route::post('/roles/add', 'AuthorityController@rolesAdd');
                Route::post('/roles/update', 'AuthorityController@rolesUpdate');
                Route::post('/roles/delete', 'AuthorityController@rolesDelete');

                Route::post('/user/menu/save', 'AuthorityController@updateUserRoute');
            });

            Route::group(['prefix' => 'game'], function (){
                Route::any('/draw', 'GameController@getGameDrawList');
                Route::post('/draw/save', 'GameController@saveGameDraw');
                Route::post('/draw/remove', 'GameController@removeGameDraw');

                Route::any('/draw/solve', 'GameController@getGameDrawSolve');
                Route::post('/draw/solve/save', 'GameController@saveGameDrawSolve');
                Route::post('/draw/solve/remove', 'GameController@removeGameDrawSolve');
            });


            Route::post('/user/logout', 'AuthorityController@logout');

            Route::any('/version/info', 'VersionController@getVersionInfo');
            Route::post('/version/save', 'VersionController@saveVersion');


            Route::any('/qa', 'QuestionAnswer@getQuestionList');
            Route::post('/qa/save', 'QuestionAnswer@questionAnswerSave');
            Route::post('/qa/remove', 'QuestionAnswer@questionAnswerRemove');

            Route::any('/red/envelope', 'RedEnvelopeController@redEnvelopeList');
            Route::post('/red/envelope/save', 'RedEnvelopeController@redEnvelopeSave');
            Route::post('/red/envelope/remove', 'RedEnvelopeController@redEnvelopeRemove');
            Route::any('/recharge/activity', 'RechargeActivityController@list');
            Route::post('/recharge/activity/update', 'RechargeActivityController@modification');
            Route::post('/recharge/activity/add', 'RechargeActivityController@addition');
            Route::post('/recharge/activity/delete', 'RechargeActivityController@remove');
            Route::post('/recharge/user/export', 'UserRechargeController@exportExcelData');
        });

    });


});
