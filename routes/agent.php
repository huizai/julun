<?php

Route::group(['namespace'=>'Agent', 'middleware' => ['origin']], function (){
    Route::options('/{all}', function(\Illuminate\Http\Request $request) {
        $origin = $request->header('ORIGIN', '*');
        header("Access-Control-Allow-Origin: $origin");
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, token, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie');
    })->where(['all' => '([a-zA-Z0-9-]|/)+']);
    Route::post('/login', 'AgentController@login');
    Route::post('/register', 'AgentController@agentReg');


    Route::get('/', 'AgentController@download');

    Route::get('/register', 'AgentController@registerView');

    Route::get('/download', 'AgentController@download');


    Route::post('/wx/pay/turnover/back', 'AgentTurnoverController@wxPayNotify');//微信回调
    Route::post('/ali/pay/turnover/back', 'AgentTurnoverController@aliPayNotify');//支付宝回调


    Route::group(['middleware' => ['CheckAgent']], function (){
        Route::any('/main', 'AgentController@main');
        Route::any('/set/agency', 'AgentController@setAgency');
        Route::group(['prefix' => 'info'], function () { //认证
            Route::get('/', 'AgentController@agentInfo');
            Route::post('/set/authentication', 'AgentController@setAuthentication');
            Route::post('/usave/msg', 'AgentController@agentMsgUpdate');
            Route::get('/msg', 'AgentController@agentMsg');
        });
        Route::group(['prefix' => 'mill'], function () { //申请代理
            Route::get('/list', 'AgentApplyController@millList');
            Route::post('/apply', 'AgentApplyController@applyAdd');
            Route::any('/apply/info', 'AgentApplyController@applyInfo');
            Route::any('/apply/list', 'AgentApplyController@applyList');
            Route::any('/apply/exam', 'AgentApplyController@applyExam');
            Route::any('/apply/mill', 'AgentApplyController@applyMillList');


            Route::any('/apply/employ', 'AgentApplyController@employAgentList');
            Route::any('/apply/employ/get', 'AgentApplyController@employAgentGet');
        });

        Route::group(['prefix' => 'turnover'], function () { //认证
            Route::get('/cart/list', 'AgentCartController@index');
            Route::get('/cart/del', 'AgentCartController@cartDel');
            Route::post('/cart/add', 'AgentCartController@cartAdd');
            Route::post('/cart/update', 'AgentCartController@cartUpdate');
            Route::get('/sku/list', 'AgentCartController@sukList');

            Route::post('/add', 'AgentCartController@turnoverAdd');
            Route::post('/add/now', 'AgentCartController@turnoverAddNow');
            Route::get('/list', 'AgentTurnoverController@index');
            Route::get('/info', 'AgentTurnoverController@turnoverInfo');
            Route::get('/status', 'AgentTurnoverController@statusUpdate');


            Route::get('/cancel/confirm', 'AgentTurnoverController@cancelConfirm');

            Route::post('/send/turnover', 'AgentTurnoverController@sendTurnover');


            Route::post('/pay', 'AgentTurnoverController@turnoverPay');

            Route::post('/pay/online', 'AgentTurnoverController@onlinePay');//线上支付

            Route::get('/carts/list', 'AgentCartController@cartList');

            Route::any('/confirm/turnover', 'AgentCartController@confirmTurnover');

            Route::get('/pay/type', 'AgentTurnoverController@payType');

        });

        Route::group(['prefix' => 'estimates'], function () { //商品

            Route::post('/add', 'EstimatesController@addEstimatesGoods');
            Route::any('/list', 'EstimatesController@estimatesList');
        });

        Route::group(['prefix' => 'goods'], function () { //商品

            Route::any('/list', 'GoodsController@goodsList');
            Route::any('/info', 'GoodsController@goodsInfo');
        });

        Route::group(['prefix' => 'market'], function () { //商品

            Route::any('/turnover', 'MarketController@turnoverMarket');
            Route::any('/goods', 'MarketController@goodsMarket');
            Route::any('/store', 'MarketController@storeMarket');
        });
    });

    Route::get('/bank', 'AgentController@bank');//银行列表

});
